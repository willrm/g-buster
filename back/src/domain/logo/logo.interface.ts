export interface LogoInterface {
  base64: string;
  mimeType: string;
}

import { TemporaryFileInterface } from '../../temporary-file/temporary-file.interface';
import { Logo } from '../logo';

describe('domain/logo/Logo', () => {
  describe('constructor()', () => {
    let temporaryFile: TemporaryFileInterface;

    beforeEach(() => {
      temporaryFile = {
        base64: 'base64',
        mimeType: 'mimeType',
      } as TemporaryFileInterface;
    });

    describe('base64', () => {
      it('should bind base64 from temporary file when exists', () => {
        // given
        temporaryFile.base64 = 'an-image-as-base64';

        // when
        const result: Logo = new Logo(temporaryFile);

        // then
        expect(result.base64).toBe('an-image-as-base64');
      });
      it('should be defined', () => {
        // given
        temporaryFile.base64 = undefined;

        // when
        const result = () => new Logo(temporaryFile).validate('Logo');

        // then
        expect(result).toThrow('Logo creation error:\n - A logo base64 must be defined');
      });
      it('should not be an empty string', () => {
        // given
        temporaryFile.base64 = '';

        // when
        const result = () => new Logo(temporaryFile).validate('Logo');

        // then
        expect(result).toThrow('Logo creation error:\n - A logo base64 cannot be an empty string');
      });
    });

    describe('mimeType', () => {
      it('should bind mimeType from temporary file when exists', () => {
        // given
        temporaryFile.mimeType = 'image/whatever';

        // when
        const result: Logo = new Logo(temporaryFile);

        // then
        expect(result.mimeType).toBe('image/whatever');
      });
      it('should be defined', () => {
        // given
        temporaryFile.mimeType = undefined;

        // when
        const result = () => new Logo(temporaryFile).validate('Logo');

        // then
        expect(result).toThrow('Logo creation error:\n - A logo mimeType must be defined');
      });
      it('should not be an empty string', () => {
        // given
        temporaryFile.mimeType = '';

        // when
        const result = () => new Logo(temporaryFile).validate('Logo');

        // then
        expect(result).toThrow('Logo creation error:\n - A logo mimeType cannot be an empty string');
      });
    });
  });
});

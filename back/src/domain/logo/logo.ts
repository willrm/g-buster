import { IsDefined, IsNotEmpty } from 'class-validator';
import { TemporaryFileInterface } from '../temporary-file/temporary-file.interface';
import { AbstractDomainWithValidation } from '../validation/abstract-domain-with-validation';
import { LogoInterface } from './logo.interface';

export class Logo extends AbstractDomainWithValidation implements LogoInterface {
  @IsDefined({ message: 'A logo base64 must be defined' })
  @IsNotEmpty({ message: 'A logo base64 cannot be an empty string' })
  base64: string;

  @IsDefined({ message: 'A logo mimeType must be defined' })
  @IsNotEmpty({ message: 'A logo mimeType cannot be an empty string' })
  mimeType: string;

  constructor(temporaryFile: TemporaryFileInterface) {
    super();

    this.base64 = temporaryFile.base64;
    this.mimeType = temporaryFile.mimeType;
  }
}

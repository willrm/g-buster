import { SalesforceId, UserId } from '../type-aliases';
import { UserRole } from './user-enums';

export interface UserInterface {
  id: UserId;
  role: UserRole;
  firstName: string;
  lastName: string;
  email: string;
  companyName: string;
  salesforceId: SalesforceId;
}

export interface UserRawInterface {
  id: number;
  type: string;
  subType: string;
  firstName: string;
  lastName: string;
  email: string;
  companyName: string;
  salesforceId: string;
}

import { SalesforceId, UserId } from '../type-aliases';
import { UserRole } from './user-enums';
import { UserInterface } from './user.interface';
import { UserRawInterface } from './user.raw.interface';

export class User implements UserInterface {
  id: UserId;
  role: UserRole;
  firstName: string;
  lastName: string;
  email: string;
  companyName: string;
  salesforceId: SalesforceId;

  constructor(userRaw: UserRawInterface, adminEmailAddresses: string[]) {
    this.id = String(userRaw.id);
    this.firstName = userRaw.firstName;
    this.lastName = userRaw.lastName;
    this.role = this.isAdmin(adminEmailAddresses, userRaw.email) ? UserRole.ADMIN : this.getNonAdminRole(userRaw.type, userRaw.subType);
    this.email = userRaw.email;
    this.companyName = userRaw.companyName;
    this.salesforceId = userRaw.salesforceId;
  }

  private isAdmin(adminEmails: string[], email: string): boolean {
    return adminEmails.includes(email);
  }

  private getNonAdminRole(rawType: string, rawSubType: string): UserRole {
    return rawType === 'Non-Membre' && rawSubType === 'Entreprise' ? UserRole.COMPANY : UserRole.CANDIDATE;
  }
}

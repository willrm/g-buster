import { User } from '../user';
import { UserRole } from '../user-enums';
import { UserRawInterface } from '../user.raw.interface';

describe('domain/user/User', () => {
  let userRaw: UserRawInterface;
  let adminEmailAddresses: string[];

  beforeEach(() => {
    userRaw = {} as UserRawInterface;
    adminEmailAddresses = [];
  });

  describe('constructor()', () => {
    describe('id', () => {
      it('should bind id from raw data', () => {
        // given
        userRaw.id = 123456789;

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.id).toBe('123456789');
      });
    });

    describe('firstName', () => {
      it('should bind first name from raw data', () => {
        // given
        userRaw.firstName = 'Georges';

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.firstName).toBe('Georges');
      });
    });

    describe('lastName', () => {
      it('should bind last name from raw data', () => {
        // given
        userRaw.lastName = 'Abitbol';

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.lastName).toBe('Abitbol');
      });
    });

    describe('role', () => {
      it('should bind role to CANDIDATE when raw type is empty', () => {
        // given
        userRaw.type = '';

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.role).toBe(UserRole.CANDIDATE);
      });
      it('should bind role to CANDIDATE when raw type is unknown', () => {
        // given
        userRaw.type = 'unknown';

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.role).toBe(UserRole.CANDIDATE);
      });
      it('should bind role to CANDIDATE when raw type is Membre', () => {
        // given
        userRaw.type = 'Membre';

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.role).toBe(UserRole.CANDIDATE);
      });
      it('should bind role to COMPANY when raw type is Non-Membre and raw sub type is Entreprise', () => {
        // given
        userRaw.type = 'Non-Membre';
        userRaw.subType = 'Entreprise';

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.role).toBe(UserRole.COMPANY);
      });
      it('should bind role to CANDIDATE when raw type is Non-Membre and raw sub type is not Entreprise', () => {
        // given
        userRaw.type = 'Non-Membre';
        userRaw.subType = 'another sub type';

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.role).toBe(UserRole.CANDIDATE);
      });
      it('should bind role to ADMIN when raw email is in admin emails', () => {
        // given
        userRaw.email = 'test@example.org';
        adminEmailAddresses = ['test@example.org'];

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.role).toBe(UserRole.ADMIN);
      });
      it('should not bind role to ADMIN when raw email is not in admin emails', () => {
        // given
        userRaw.email = 'test@example.org';
        adminEmailAddresses = ['another@example.org'];

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.role).toBe(UserRole.CANDIDATE);
      });
    });

    describe('email', () => {
      it('should bind email from raw data', () => {
        // given
        userRaw.email = 'test@example.org';

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.email).toBe('test@example.org');
      });
    });

    describe('companyName', () => {
      it('should bind company name from raw data', () => {
        // given
        userRaw.companyName = 'Béton Provincial';

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.companyName).toBe('Béton Provincial');
      });
    });

    describe('salesforceId', () => {
      it('should bind salesforce id from raw data', () => {
        // given
        userRaw.salesforceId = 'ABC123456789XYZ';

        // when
        const result: User = new User(userRaw, adminEmailAddresses);

        // then
        expect(result.salesforceId).toBe('ABC123456789XYZ');
      });
    });
  });
});

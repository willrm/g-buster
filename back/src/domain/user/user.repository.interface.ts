import { UserInterface } from './user.interface';

export interface UserRepository {
  logIn(user: UserInterface): Promise<void>;
  findAllAdminEmailAddresses(): Promise<string[]>;
  getCurrent(): Promise<UserInterface>;
}

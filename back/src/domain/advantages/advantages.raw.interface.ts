import { AdvantageFinancial, AdvantageHealth, AdvantageLifeBalance, AdvantageProfessional, AdvantageSocial } from './advantages-enums';

export interface AdvantagesRawInterface {
  health?: AdvantageHealth[];
  social?: AdvantageSocial[];
  lifeBalance?: AdvantageLifeBalance[];
  financial?: AdvantageFinancial[];
  professional?: AdvantageProfessional[];
}

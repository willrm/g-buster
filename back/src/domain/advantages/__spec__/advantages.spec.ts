import { Advantages } from '../advantages';
import { AdvantageFinancial, AdvantageHealth, AdvantageLifeBalance, AdvantageProfessional, AdvantageSocial } from '../advantages-enums';
import { AdvantagesRawInterface } from '../advantages.raw.interface';

describe('domain/advantages/advantages', () => {
  describe('constructor()', () => {
    let advantagesRaw: AdvantagesRawInterface;

    beforeEach(() => {
      advantagesRaw = {} as AdvantagesRawInterface;
    });
    describe('health', () => {
      it('should bind health from raw data', () => {
        // given
        advantagesRaw.health = [AdvantageHealth.HEALTH_LIFE_INSURANCE];

        // when
        const result: Advantages = new Advantages(advantagesRaw);

        // then
        expect(result.health).toStrictEqual([AdvantageHealth.HEALTH_LIFE_INSURANCE]);
      });
    });

    describe('social', () => {
      it('should bind social from raw data', () => {
        // given
        advantagesRaw.social = [AdvantageSocial.SOCIAL_FRESH_FRUIT_AND_VEGETABLES];

        // when
        const result: Advantages = new Advantages(advantagesRaw);

        // then
        expect(result.social).toStrictEqual([AdvantageSocial.SOCIAL_FRESH_FRUIT_AND_VEGETABLES]);
      });
    });

    describe('lifeBalance', () => {
      it('should bind lifeBalance from raw data', () => {
        // given
        advantagesRaw.lifeBalance = [AdvantageLifeBalance.LIFE_BALANCE_MOBILE_HOLIDAYS];

        // when
        const result: Advantages = new Advantages(advantagesRaw);

        // then
        expect(result.lifeBalance).toStrictEqual([AdvantageLifeBalance.LIFE_BALANCE_MOBILE_HOLIDAYS]);
      });
    });

    describe('financial', () => {
      it('should bind financial from raw data', () => {
        // given
        advantagesRaw.financial = [AdvantageFinancial.FINANCIAL_EAP];

        // when
        const result: Advantages = new Advantages(advantagesRaw);

        // then
        expect(result.financial).toStrictEqual([AdvantageFinancial.FINANCIAL_EAP]);
      });
    });

    describe('professional', () => {
      it('should bind professional from raw data', () => {
        // given
        advantagesRaw.professional = [AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM];

        // when
        const result: Advantages = new Advantages(advantagesRaw);

        // then
        expect(result.professional).toStrictEqual([AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM]);
      });
    });
  });
});

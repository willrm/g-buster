import { AdvantageFinancial, AdvantageHealth, AdvantageLifeBalance, AdvantageProfessional, AdvantageSocial } from './advantages-enums';
import { AdvantagesInterface } from './advantages.interface';
import { AdvantagesRawInterface } from './advantages.raw.interface';

export class Advantages implements AdvantagesInterface {
  health?: AdvantageHealth[];
  social?: AdvantageSocial[];
  lifeBalance?: AdvantageLifeBalance[];
  financial?: AdvantageFinancial[];
  professional?: AdvantageProfessional[];

  constructor(advantagesRaw: AdvantagesRawInterface) {
    this.health = advantagesRaw.health;
    this.social = advantagesRaw.social;
    this.lifeBalance = advantagesRaw.lifeBalance;
    this.financial = advantagesRaw.financial;
    this.professional = advantagesRaw.professional;
  }
}

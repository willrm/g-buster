import { AdvantageFinancial, AdvantageHealth, AdvantageLifeBalance, AdvantageProfessional, AdvantageSocial } from './advantages-enums';

export interface AdvantagesInterface {
  health?: AdvantageHealth[];
  social?: AdvantageSocial[];
  lifeBalance?: AdvantageLifeBalance[];
  financial?: AdvantageFinancial[];
  professional?: AdvantageProfessional[];
}

// General types
export type Lang = string;
export type Uuid = string;
export type SalesforceId = string;

// Editorial related types
export type HtmlMarkup = string;
export type Uid = string;

// Company related types
export type CompanyId = string;

// TemporaryFile related types
export type TemporaryFileId = string;

// JobOffer related types
export type JobOfferId = string;

// User related types
export type UserId = string;

// Candidate related types
export type CandidateId = string;

// GeniusProduct related types
export type GeniusProductId = string;

/*
Note: Ids fields are voluntarily of string type (instead of number or bigint types),
because even bigint is not big enough to handle properly ids having 18 digits (ex: 563202000000340008)
*/

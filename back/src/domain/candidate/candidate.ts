import { CandidateId, SalesforceId } from '../type-aliases';
import { UserInterface } from '../user/user.interface';
import { CandidateInterface } from './candidate.interface';

export class Candidate implements CandidateInterface {
  id: CandidateId;
  firstName: string;
  lastName: string;
  email: string;
  salesforceId: SalesforceId;

  constructor(user: UserInterface) {
    this.id = null;
    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.email = user.email;
    this.salesforceId = user.salesforceId;
  }
}

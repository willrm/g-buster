import { CandidateId, SalesforceId } from '../type-aliases';

export interface CandidateInterface {
  id: CandidateId;
  email: string;
  firstName: string;
  lastName: string;
  salesforceId: SalesforceId;
}

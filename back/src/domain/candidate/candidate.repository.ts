import { TemporaryFileInterface } from '../temporary-file/temporary-file.interface';
import { CandidateId, JobOfferId } from '../type-aliases';
import { CandidateInterface } from './candidate.interface';

export interface CandidateRepository {
  save(
    candidate: CandidateInterface,
    resumeTemporaryFile: TemporaryFileInterface,
    coverLetterTemporaryFile: TemporaryFileInterface
  ): Promise<CandidateId>;
  findByEmail(email: string): Promise<CandidateInterface>;
  applyToJobOffer(candidateId: CandidateId, jobOfferId: JobOfferId): Promise<void>;
}

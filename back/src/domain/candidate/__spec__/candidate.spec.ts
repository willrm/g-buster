import { UserInterface } from '../../user/user.interface';
import { Candidate } from '../candidate';

describe('domain/candidate/Candidate', () => {
  describe('constructor()', () => {
    it('should initialize with null id', () => {
      // given
      const user: UserInterface = { email: 'test@example.org' } as UserInterface;

      // when
      const result: Candidate = new Candidate(user);

      // then
      expect(result.id).toBeNull();
    });

    it('should bind first name from user', () => {
      // given
      const user: UserInterface = { firstName: 'User first name' } as UserInterface;

      // when
      const result: Candidate = new Candidate(user);

      // then
      expect(result.firstName).toBe('User first name');
    });

    it('should bind last name from user', () => {
      // given
      const user: UserInterface = { lastName: 'User last name' } as UserInterface;

      // when
      const result: Candidate = new Candidate(user);

      // then
      expect(result.lastName).toBe('User last name');
    });

    it('should bind email from user', () => {
      // given
      const user: UserInterface = { email: 'test@example.org' } as UserInterface;

      // when
      const result: Candidate = new Candidate(user);

      // then
      expect(result.email).toBe('test@example.org');
    });

    it('should bind salesforce id from user', () => {
      // given
      const user: UserInterface = { salesforceId: 'ABC123456789XYZ' } as UserInterface;

      // when
      const result: Candidate = new Candidate(user);

      // then
      expect(result.salesforceId).toBe('ABC123456789XYZ');
    });
  });
});

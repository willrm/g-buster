import { ActivityLocations } from '../activity-locations';
import { ActivityLocationsRawInterface } from '../activity-locations.raw.interface';

describe('domain/activity-locations/ActivityLocations', () => {
  describe('constructor()', () => {
    let activityLocationsRaw: ActivityLocationsRawInterface;
    beforeEach(() => {
      activityLocationsRaw = { quebec: 12 } as ActivityLocationsRawInterface;
    });
    describe('canada', () => {
      it('should bind canada from raw data when exists', () => {
        // given
        activityLocationsRaw.canada = 42;

        // when
        const result: ActivityLocations = new ActivityLocations(activityLocationsRaw);

        // then
        expect(result.canada).toBe(42);
      });
      it('should not throw any exception when value is zero', () => {
        // given
        activityLocationsRaw.canada = 0;

        // when
        const result = () => new ActivityLocations(activityLocationsRaw).validate('ActivityLocations');

        // then
        expect(result).not.toThrow();
      });
      it('should throw an exception when value is negative', () => {
        // given
        activityLocationsRaw.canada = -12;

        // when
        const result = () => new ActivityLocations(activityLocationsRaw).validate('ActivityLocations');

        // then
        expect(result).toThrow('ActivityLocations creation error:\n - An activity locations canada value must be a positive value');
      });
    });
    describe('international', () => {
      it('should bind international from raw data when exists', () => {
        // given
        activityLocationsRaw.international = 12;

        // when
        const result: ActivityLocations = new ActivityLocations(activityLocationsRaw);

        // then
        expect(result.international).toBe(12);
      });
      it('should not throw any exception when value is zero', () => {
        // given
        activityLocationsRaw.international = 0;

        // when
        const result = () => new ActivityLocations(activityLocationsRaw).validate('ActivityLocations');

        // then
        expect(result).not.toThrow();
      });
      it('should throw an exception when value is negative', () => {
        // given
        activityLocationsRaw.international = -12;

        // when
        const result = () => new ActivityLocations(activityLocationsRaw).validate('ActivityLocations');

        // then
        expect(result).toThrow('ActivityLocations creation error:\n - An activity locations international value must be a positive value');
      });
    });
    describe('quebec', () => {
      it('should bind quebec from raw data when exists', () => {
        // given
        activityLocationsRaw.quebec = 12345;

        // when
        const result: ActivityLocations = new ActivityLocations(activityLocationsRaw);

        // then
        expect(result.quebec).toBe(12345);
      });
      it('should throw an exception when quebec is undefined', () => {
        // given
        activityLocationsRaw.quebec = undefined;

        // when
        const result = () => new ActivityLocations(activityLocationsRaw).validate('ActivityLocations');

        // then
        expect(result).toThrow('ActivityLocations creation error:\n - A quebec value must be defined');
      });
      it('should not throw any exception when value is zero', () => {
        // given
        activityLocationsRaw.quebec = 0;

        // when
        const result = () => new ActivityLocations(activityLocationsRaw).validate('ActivityLocations');

        // then
        expect(result).not.toThrow();
      });
      it('should throw an exception when value is negative', () => {
        // given
        activityLocationsRaw.quebec = -12;

        // when
        const result = () => new ActivityLocations(activityLocationsRaw).validate('ActivityLocations');

        // then
        expect(result).toThrow('ActivityLocations creation error:\n - An activity locations quebec value must be a positive value');
      });
    });
    describe('usa', () => {
      it('should bind usa from raw data when exists', () => {
        // given
        activityLocationsRaw.usa = 12345;

        // when
        const result: ActivityLocations = new ActivityLocations(activityLocationsRaw);

        // then
        expect(result.usa).toBe(12345);
      });
      it('should not throw any exception when value is zero', () => {
        // given
        activityLocationsRaw.usa = 0;

        // when
        const result = () => new ActivityLocations(activityLocationsRaw).validate('ActivityLocations');

        // then
        expect(result).not.toThrow();
      });
      it('should throw an exception when value is negative', () => {
        // given
        activityLocationsRaw.usa = -12;

        // when
        const result = () => new ActivityLocations(activityLocationsRaw).validate('ActivityLocations');

        // then
        expect(result).toThrow('ActivityLocations creation error:\n - An activity locations usa value must be a positive value');
      });
    });
  });
});

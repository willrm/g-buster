import { IsDefined, Min } from 'class-validator';
import { AbstractDomainWithValidation } from '../validation/abstract-domain-with-validation';
import { ActivityLocationsInterface } from './activity-locations.interface';
import { ActivityLocationsRawInterface } from './activity-locations.raw.interface';

export class ActivityLocations extends AbstractDomainWithValidation implements ActivityLocationsInterface {
  @Min(0, { message: 'An activity locations quebec value must be a positive value' })
  @IsDefined({ message: 'A quebec value must be defined' })
  quebec: number;

  @Min(0, { message: 'An activity locations canada value must be a positive value' })
  canada?: number;

  @Min(0, { message: 'An activity locations international value must be a positive value' })
  international?: number;

  @Min(0, { message: 'An activity locations usa value must be a positive value' })
  usa?: number;

  constructor(activityLocationsRaw: ActivityLocationsRawInterface) {
    super();

    this.canada = activityLocationsRaw.canada;
    this.international = activityLocationsRaw.international;
    this.quebec = activityLocationsRaw.quebec;
    this.usa = activityLocationsRaw.usa;
  }
}

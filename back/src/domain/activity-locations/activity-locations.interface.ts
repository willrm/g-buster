export interface ActivityLocationsInterface {
  quebec: number;
  canada?: number;
  international?: number;
  usa?: number;
}

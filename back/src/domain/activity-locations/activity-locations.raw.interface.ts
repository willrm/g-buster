export interface ActivityLocationsRawInterface {
  quebec: number;
  canada?: number;
  international?: number;
  usa?: number;
}

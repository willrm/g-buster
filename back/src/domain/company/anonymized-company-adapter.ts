import { cloneDeep, truncate } from 'lodash';
import { CompanyInterface } from './company.interface';

type CompanyKey = keyof CompanyInterface;

export class AnonymizedCompanyAdapter {
  private readonly PRESENTATION_CHARACTERS_MAX_LENGTH: number = 500;
  private readonly FIELDS_ALLOWED_FOR_AN_ANONYMOUS: CompanyKey[] = ['id', 'logo', 'name', 'presentation', 'socialNetworks', 'website'];

  anonymize(company: CompanyInterface): CompanyInterface {
    const companyKeys: CompanyKey[] = Object.keys(company) as CompanyKey[];
    const anonymizedCompany: CompanyInterface = cloneDeep(company);

    companyKeys.forEach((companyKey: CompanyKey) => {
      if (!this.FIELDS_ALLOWED_FOR_AN_ANONYMOUS.includes(companyKey)) {
        delete anonymizedCompany[companyKey];
      }
    });

    anonymizedCompany.presentation = truncate(anonymizedCompany.presentation, {
      length: this.PRESENTATION_CHARACTERS_MAX_LENGTH,
      separator: ' ',
      omission: '',
    });

    return anonymizedCompany;
  }
}

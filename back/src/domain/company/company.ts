import { ArrayMaxSize, ArrayMinSize, IsDefined, IsEnum, IsNotEmpty, IsUrl, MaxDate, MaxLength, Min, MinDate, ValidateNested } from 'class-validator';
import { cloneDeep } from 'lodash';
import { ActivityLocations } from '../activity-locations/activity-locations';
import { Logo } from '../logo/logo';
import { LogoInterface } from '../logo/logo.interface';
import { SocialNetworks } from '../social-networks/social-networks';
import { TemporaryFileInterface } from '../temporary-file/temporary-file.interface';

import { CompanyId, UserId } from '../type-aliases';
import { UserInterface } from '../user/user.interface';
import { AbstractDomainWithValidation } from '../validation/abstract-domain-with-validation';
import { Value } from '../value/value';
import { ValueRawInterface } from '../value/value.raw.interface';
import {
  COMPANY_ADDRESS_MAX_LENGTH,
  COMPANY_CREATION_DATE_MAX_DATE,
  COMPANY_CREATION_DATE_MIN_DATE,
  COMPANY_GENIUS_TYPES_MIN,
  COMPANY_NAME_MAX_LENGTH,
  COMPANY_PRESENTATION_MAX_LENGTH,
  COMPANY_VALUES_MAX,
  COMPANY_VALUES_MIN,
  COMPANY_WEBSITE_MAX_LENGTH,
} from './company-constraints';
import { CompanyBusinessSegment, CompanyGeniusType, CompanyRevenue, CompanyRevenueForRD, CompanySize, CompanyStatus } from './company-enums';
import { CompanyInterface } from './company.interface';
import { CompanyRawInterface } from './company.raw.interface';

export class Company extends AbstractDomainWithValidation implements CompanyInterface {
  static factory: CompanyFactoryInterface = {
    validateAndCreateNewCompany(user: UserInterface, company: CompanyRawInterface, temporaryFile: TemporaryFileInterface): Company {
      return new Company(user, company, temporaryFile, true);
    },
    copy(company: CompanyInterface): Company {
      return new Company({} as UserInterface, company, {} as TemporaryFileInterface, false, true);
    },
  };

  id: CompanyId;

  @IsNotEmpty({ message: 'A company user id cannot be an empty string' })
  @IsDefined({ message: 'A company user id must be defined' })
  userId: UserId;

  @IsEnum(CompanyStatus, {
    message: 'A company status type must be a valid one',
  })
  @IsDefined({ message: 'A company status must be defined' })
  status: CompanyStatus;

  @ValidateNested()
  @IsDefined({ message: 'Activity locations must be defined' })
  activityLocations: ActivityLocations;

  @MaxLength(COMPANY_ADDRESS_MAX_LENGTH, {
    message: 'A company address must have a maximum of $constraint1 characters',
  })
  @IsNotEmpty({ message: 'A company address cannot be an empty string' })
  @IsDefined({ message: 'A company address must be defined' })
  address: string;

  @Min(0, { message: 'A bachelors of engineering number must be a positive value' })
  @IsDefined({ message: 'A bachelors of engineering number must be defined' })
  bachelorsOfEngineeringNumber: number;

  @IsEnum(CompanyBusinessSegment, {
    message: 'A company business segment must be a valid one',
  })
  @IsDefined({ message: 'A company business segment must be defined' })
  businessSegment: CompanyBusinessSegment;

  @MinDate(COMPANY_CREATION_DATE_MIN_DATE, {
    message: 'A company creation date must be after January 1st, 1800',
  })
  @MaxDate(COMPANY_CREATION_DATE_MAX_DATE, {
    message: 'A company creation date must be before current date',
  })
  @IsDefined({ message: 'A company creation date must be defined' })
  creationDate: Date;

  @IsEnum(CompanyGeniusType, {
    message: 'A company genius type must be a valid one',
    each: true,
  })
  @ArrayMinSize(COMPANY_GENIUS_TYPES_MIN, {
    message: 'A company must have at least one genius type',
  })
  @IsDefined({ message: 'A company genius type must be defined' })
  geniusTypes: CompanyGeniusType[];

  @ValidateNested()
  @IsDefined({ message: 'A logo must be defined' })
  logo: LogoInterface;

  @MaxLength(COMPANY_NAME_MAX_LENGTH, {
    message: 'A company name must have a maximum of $constraint1 characters',
  })
  @IsNotEmpty({ message: 'A company name cannot be an empty string' })
  @IsDefined({ message: 'A company name must be defined' })
  name: string;

  @MaxLength(COMPANY_PRESENTATION_MAX_LENGTH, {
    message: 'A company presentation must have a maximum of $constraint1 characters',
  })
  @IsNotEmpty({ message: 'A company presentation cannot be an empty string' })
  @IsDefined({ message: 'A company presentation must be defined' })
  presentation: string;

  @IsEnum(CompanyRevenue, {
    message: 'A company revenue must be a valid one',
  })
  @IsDefined({ message: 'A company revenue must be defined' })
  revenue: CompanyRevenue;

  @IsEnum(CompanyRevenueForRD, {
    message: 'A company revenue for R&D must be a valid one',
  })
  revenueForRD?: CompanyRevenueForRD;

  @IsEnum(CompanySize, {
    message: 'A company size must be a valid one',
  })
  @IsDefined({ message: 'A company size must be defined' })
  size: CompanySize;

  @ValidateNested()
  socialNetworks: SocialNetworks;

  @ArrayMinSize(COMPANY_VALUES_MIN, {
    message: 'A company must have at least one value',
  })
  @ArrayMaxSize(COMPANY_VALUES_MAX, {
    message: 'A company must have maximum four values',
  })
  @ValidateNested()
  @IsDefined({ message: 'A company value must be defined' })
  values: Value[];

  @IsUrl(
    {},
    {
      message: 'A company website must have a URL format',
    }
  )
  @MaxLength(COMPANY_WEBSITE_MAX_LENGTH, {
    message: 'A company website must have a maximum of $constraint1 characters',
  })
  @IsNotEmpty({ message: 'A company website cannot be an empty string' })
  @IsDefined({ message: 'A company website must be defined' })
  website: string;

  private constructor(
    user: UserInterface,
    company: CompanyRawInterface | CompanyInterface,
    temporaryFile: TemporaryFileInterface,
    shouldValidate?: boolean,
    shouldCopy?: boolean
  ) {
    super();
    if (shouldCopy) {
      this.copy(company as CompanyInterface);
    } else {
      this.id = null;
      this.userId = user.id;
      this.status = CompanyStatus.STATUS_PENDING_VALIDATION;
      this.logo = temporaryFile ? new Logo(temporaryFile) : null;
      this.bindRaw(company as CompanyRawInterface);
    }

    if (shouldValidate) {
      this.validate('Company');
    }
  }

  updateWith(companyRaw: CompanyRawInterface): void {
    this.bindRaw(companyRaw);
    this.validate('Company');
  }

  updateLogo(temporaryFile: TemporaryFileInterface): void {
    this.logo = temporaryFile ? new Logo(temporaryFile) : null;
  }

  publish(): void {
    this.status = CompanyStatus.STATUS_PUBLISHED;
  }

  private copy(otherCompany: CompanyInterface): void {
    const deepCloneOfOtherCompany: CompanyInterface = cloneDeep(otherCompany);
    for (const field in deepCloneOfOtherCompany) {
      if (deepCloneOfOtherCompany.hasOwnProperty(field)) {
        this[field] = deepCloneOfOtherCompany[field];
      }
    }
  }

  private bindRaw(companyRaw: CompanyRawInterface): void {
    this.activityLocations = companyRaw.activityLocations ? new ActivityLocations(companyRaw.activityLocations) : null;
    this.address = companyRaw.address;
    this.bachelorsOfEngineeringNumber = companyRaw.bachelorsOfEngineeringNumber;
    this.businessSegment = companyRaw.businessSegment;
    this.creationDate = companyRaw.creationDate ? new Date(companyRaw.creationDate) : null;
    this.geniusTypes = companyRaw.geniusTypes;
    this.name = companyRaw.name;
    this.presentation = companyRaw.presentation;
    this.revenue = companyRaw.revenue;
    this.revenueForRD = companyRaw.revenueForRD;
    this.size = companyRaw.size;
    this.socialNetworks = companyRaw.socialNetworks ? new SocialNetworks(companyRaw.socialNetworks) : null;
    this.values = companyRaw.values ? companyRaw.values.map((valueRawInterface: ValueRawInterface) => new Value(valueRawInterface)) : null;
    this.website = companyRaw.website;
  }
}

export interface CompanyFactoryInterface {
  validateAndCreateNewCompany(user: UserInterface, company: CompanyRawInterface, temporaryFile: TemporaryFileInterface): Company;
  copy(company: CompanyInterface): Company;
}

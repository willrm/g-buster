import { repeat } from 'lodash';
import { AnonymizedCompanyAdapter } from '../anonymized-company-adapter';
import { CompanyBusinessSegment, CompanyGeniusType, CompanyRevenue, CompanyRevenueForRD, CompanySize, CompanyStatus } from '../company-enums';
import { CompanyInterface } from '../company.interface';

describe('domain/job-offer/AnonymizedCompanyAdapter', () => {
  let anonymizedCompanyAdapter: AnonymizedCompanyAdapter;
  let companyWithAllPropertiesFilled: CompanyInterface;

  beforeEach(() => {
    companyWithAllPropertiesFilled = {
      id: '123456789',
      userId: '987654321',
      status: CompanyStatus.STATUS_PENDING_VALIDATION,
      activityLocations: { quebec: 42 },
      address: 'company address',
      bachelorsOfEngineeringNumber: 1337,
      businessSegment: CompanyBusinessSegment.BUSINESS_SEGMENT_PULP_AND_PAPER,
      creationDate: new Date('2010-10-15T00:00:00'),
      geniusTypes: [CompanyGeniusType.GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING],
      logo: { mimeType: 'image/type', base64: 'image-as-base64' },
      name: 'company name',
      presentation: 'company presentation',
      revenue: CompanyRevenue.REVENUE_BETWEEN_5M_AND_20M,
      revenueForRD: CompanyRevenueForRD.REVENUE_RD_MORE_THAN_20M,
      size: CompanySize.HUNDRED_TWENTY_TO_TWO_HUNDRED_NINETY_NINE,
      socialNetworks: { linkedIn: 'linkedIn url', facebook: 'facebook url' },
      values: [{ justification: 'value justification', meaning: 'value meaning' }],
      website: 'company website',
    };

    anonymizedCompanyAdapter = new AnonymizedCompanyAdapter();
  });

  describe('anonymize()', () => {
    it('should remove all fields that needs to be logged in to view them', () => {
      // when
      const result: CompanyInterface = anonymizedCompanyAdapter.anonymize(companyWithAllPropertiesFilled);

      // then
      expect(result.userId).toBeUndefined();
      expect(result.activityLocations).toBeUndefined();
      expect(result.address).toBeUndefined();
      expect(result.bachelorsOfEngineeringNumber).toBeUndefined();
      expect(result.businessSegment).toBeUndefined();
      expect(result.creationDate).toBeUndefined();
      expect(result.geniusTypes).toBeUndefined();
      expect(result.revenue).toBeUndefined();
      expect(result.revenueForRD).toBeUndefined();
      expect(result.size).toBeUndefined();
      expect(result.values).toBeUndefined();
    });

    it('should keep id', () => {
      // when
      const result: CompanyInterface = anonymizedCompanyAdapter.anonymize(companyWithAllPropertiesFilled);

      // then
      expect(result.id).toBeDefined();
    });

    it('should keep logo', () => {
      // when
      const result: CompanyInterface = anonymizedCompanyAdapter.anonymize(companyWithAllPropertiesFilled);

      // then
      expect(result.logo).toBeDefined();
    });

    it('should keep name', () => {
      // when
      const result: CompanyInterface = anonymizedCompanyAdapter.anonymize(companyWithAllPropertiesFilled);

      // then
      expect(result.name).toBeDefined();
    });

    it('should keep socialNetworks', () => {
      // when
      const result: CompanyInterface = anonymizedCompanyAdapter.anonymize(companyWithAllPropertiesFilled);

      // then
      expect(result.socialNetworks).toBeDefined();
    });

    it('should keep website', () => {
      // when
      const result: CompanyInterface = anonymizedCompanyAdapter.anonymize(companyWithAllPropertiesFilled);

      // then
      expect(result.website).toBeDefined();
    });

    describe('presentation', () => {
      it('should keep presentation', () => {
        // when
        const result: CompanyInterface = anonymizedCompanyAdapter.anonymize(companyWithAllPropertiesFilled);

        // then
        expect(result.presentation).toBeDefined();
      });

      it('should truncate presentation to 500 characters', () => {
        // when
        companyWithAllPropertiesFilled.presentation = repeat('*', 600);
        const result: CompanyInterface = anonymizedCompanyAdapter.anonymize(companyWithAllPropertiesFilled);

        // then
        expect(result.presentation).toHaveLength(500);
      });

      it('should truncate presentation to 500 characters with space separator', () => {
        // when
        companyWithAllPropertiesFilled.presentation = `${repeat('*', 498)} should be removed`;
        const result: CompanyInterface = anonymizedCompanyAdapter.anonymize(companyWithAllPropertiesFilled);

        // then
        expect(result.presentation).toHaveLength(498);
      });

      it('should not truncate presentation if shorter than 500', () => {
        // when
        companyWithAllPropertiesFilled.presentation = repeat('*', 400);
        const result: CompanyInterface = anonymizedCompanyAdapter.anonymize(companyWithAllPropertiesFilled);

        // then
        expect(result.presentation).toHaveLength(400);
      });
    });
  });
});

import { repeat } from 'lodash';
import { ActivityLocations } from '../../activity-locations/activity-locations';
import { ActivityLocationsRawInterface } from '../../activity-locations/activity-locations.raw.interface';
import { Logo } from '../../logo/logo';
import { LogoInterface } from '../../logo/logo.interface';
import { SocialNetworks } from '../../social-networks/social-networks';
import { SocialNetworksInterface } from '../../social-networks/social-networks.interface';
import { SocialNetworksRawInterface } from '../../social-networks/social-networks.raw.interface';
import { TemporaryFileInterface } from '../../temporary-file/temporary-file.interface';
import { CompanyId } from '../../type-aliases';
import { UserInterface } from '../../user/user.interface';
import { Value } from '../../value/value';
import { ValueInterface } from '../../value/value.interface';
import { ValueRawInterface } from '../../value/value.raw.interface';
import { Company } from '../company';
import { CompanyBusinessSegment, CompanyGeniusType, CompanyRevenue, CompanyRevenueForRD, CompanySize, CompanyStatus } from '../company-enums';
import { CompanyInterface } from '../company.interface';
import { CompanyRawInterface } from '../company.raw.interface';

jest.mock('../../social-networks/social-networks');
jest.mock('../../activity-locations/activity-locations');
jest.mock('../../logo/logo');

describe('domain/company/Company', () => {
  let user: UserInterface;
  let company: Company;
  let companyRaw: CompanyRawInterface;
  let valueRaw: ValueRawInterface;
  let temporaryFile: TemporaryFileInterface;

  beforeEach(() => {
    (SocialNetworks as jest.Mock).mockClear();
    (ActivityLocations as jest.Mock).mockClear();
    (Logo as jest.Mock).mockClear();

    user = { id: '42' } as UserInterface;
    company = { id: '12' } as Company;

    valueRaw = {
      meaning: 'A values',
      justification: 'A justification',
    };

    companyRaw = {
      activityLocations: {} as ActivityLocationsRawInterface,
      name: 'name',
      website: 'https://a.company.website',
      address: 'address',
      bachelorsOfEngineeringNumber: 1,
      creationDate: new Date(2018, 9),
      size: CompanySize.ZERO_TO_NINE,
      revenue: CompanyRevenue.REVENUE_LESS_THAN_100K,
      revenueForRD: CompanyRevenueForRD.REVENUE_RD_LESS_THAN_100K,
      businessSegment: CompanyBusinessSegment.BUSINESS_SEGMENT_GEOLOGY,
      presentation: 'presentation',
      geniusTypes: [CompanyGeniusType.GENIUS_TYPE_AEROSPACE_ENGINEERING],
      values: [valueRaw],
      logoUuid: '',
    };

    temporaryFile = { base64: 'an-image-as-base64' } as TemporaryFileInterface;
  });

  describe('factory', () => {
    describe('validateAndCreateNewCompany()', () => {
      describe('id', () => {
        it('should initialize id to null', () => {
          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.id).toBeNull();
        });
      });

      describe('userId', () => {
        it('should bind id from user', () => {
          // given
          user = { id: '123456789' } as UserInterface;

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.userId).toBe('123456789');
        });

        it('should throw an exception when user id is undefined', () => {
          // given
          user = { id: undefined } as UserInterface;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company user id must be defined');
        });

        it('should throw an exception when user id is empty', () => {
          // given
          user = { id: '' } as UserInterface;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company user id cannot be an empty string');
        });
      });

      describe('status', () => {
        it('should initialize status to unpublished', () => {
          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.status).toBe(CompanyStatus.STATUS_PENDING_VALIDATION);
        });
      });

      describe('activityLocations', () => {
        let activityLocationsRaw: ActivityLocationsRawInterface;

        it('should bind activityLocations from raw data', () => {
          // given
          activityLocationsRaw = {} as ActivityLocationsRawInterface;
          companyRaw.activityLocations = {} as ActivityLocationsRawInterface;

          const expected: ActivityLocationsRawInterface = { ...activityLocationsRaw } as ActivityLocationsRawInterface;
          (ActivityLocations as jest.Mock).mockImplementation(() => expected);

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.activityLocations).toBe(expected);
          expect(ActivityLocations).toHaveBeenCalledWith(companyRaw.activityLocations);
        });

        it('should throw an exception when activity locations is undefined', () => {
          // given
          companyRaw.activityLocations = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - Activity locations must be defined');
        });
      });

      describe('address', () => {
        it('should bind address from raw data', () => {
          // given
          companyRaw.address = 'A company address';

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.address).toBe('A company address');
        });

        it('should throw an exception when address is undefined', () => {
          // given
          companyRaw.address = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company address must be defined');
        });

        it('should throw an exception when address is empty', () => {
          // given
          companyRaw.address = '';

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company address cannot be an empty string');
        });

        it('should throw an exception when address is more than 255 characters', () => {
          // given
          companyRaw.address = repeat('*', 256);

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company address must have a maximum of 255 characters');
        });
      });

      describe('bachelorsOfEngineeringNumber', () => {
        it('should bind bachelorsOfEngineeringNumber from raw data', () => {
          // given
          companyRaw.bachelorsOfEngineeringNumber = 12;

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.bachelorsOfEngineeringNumber).toStrictEqual(12);
        });

        it('should not throw any exception when bachelorsOfEngineeringNumber is zero', () => {
          // given
          companyRaw.bachelorsOfEngineeringNumber = 0;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).not.toThrow();
        });

        it('should throw an exception when bachelorsOfEngineeringNumber is negative', () => {
          // given
          companyRaw.bachelorsOfEngineeringNumber = -1;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A bachelors of engineering number must be a positive value');
        });

        it('should throw an exception when bachelorsOfEngineeringNumber is undefined', () => {
          // given
          companyRaw.bachelorsOfEngineeringNumber = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A bachelors of engineering number must be defined');
        });
      });

      describe('businessSegment', () => {
        it('should bind business segment from raw data', () => {
          // given
          companyRaw.businessSegment = CompanyBusinessSegment.BUSINESS_SEGMENT_AGRICULTURE;

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.businessSegment).toStrictEqual(CompanyBusinessSegment.BUSINESS_SEGMENT_AGRICULTURE);
        });

        it('should bind business segment from raw data with enum value as a string', () => {
          // given
          companyRaw.businessSegment = CompanyBusinessSegment.BUSINESS_SEGMENT_CHEMISTRY;

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.businessSegment).toStrictEqual('BUSINESS_SEGMENT_CHEMISTRY');
        });

        it('should throw an exception when business segment is undefined', () => {
          // given
          companyRaw.businessSegment = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company business segment must be defined');
        });

        it('should throw an exception when business segment is invalid', () => {
          // given
          companyRaw.businessSegment = 'FAKE_BUSINESS_SEGMENT' as CompanyBusinessSegment;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company business segment must be a valid one');
        });
      });

      describe('creationDate', () => {
        it('should bind creationDate from raw data', () => {
          // given
          const expected: Date = new Date(2017, 9);
          companyRaw.creationDate = expected;

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.creationDate).toStrictEqual(expected);
        });

        it('should throw an exception when creationDate is undefined', () => {
          // given
          companyRaw.creationDate = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company creation date must be defined');
        });

        it('should throw an exception when creationDate is before January 1st, 1800', () => {
          // given
          companyRaw.creationDate = new Date(1789, 7);

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company creation date must be after January 1st, 1800');
        });

        it('should throw an exception when creationDate is after current date', () => {
          // given
          const futureDate: Date = new Date();
          futureDate.setDate(futureDate.getDate() + 1);
          companyRaw.creationDate = futureDate;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company creation date must be before current date');
        });
      });

      describe('geniusTypes', () => {
        it('should bind genius types from raw data', () => {
          // given
          companyRaw.geniusTypes = [CompanyGeniusType.GENIUS_TYPE_MATERIALS_ENGINEERING_AND_METALLURGY];

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.geniusTypes).toStrictEqual([CompanyGeniusType.GENIUS_TYPE_MATERIALS_ENGINEERING_AND_METALLURGY]);
        });

        it('should bind genius types from raw data with enum value as a string', () => {
          // given
          companyRaw.geniusTypes = [CompanyGeniusType.GENIUS_TYPE_CIVIL_ENGINEERING];

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.geniusTypes).toStrictEqual(['GENIUS_TYPE_CIVIL_ENGINEERING']);
        });

        it('should throw an exception when company genius types is undefined', () => {
          // given
          companyRaw.geniusTypes = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company genius type must be defined');
        });

        it('should throw an exception when company genius types is empty', () => {
          // given
          companyRaw.geniusTypes = [];

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company must have at least one genius type');
        });

        it('should throw an exception when company genius type is invalid', () => {
          // given
          companyRaw.geniusTypes = ['FAKE_GENIUS_TYPE' as CompanyGeniusType];

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company genius type must be a valid one');
        });
      });

      describe('logo', () => {
        it('should bind logo from temporary file', () => {
          // given
          const temporaryFileData: object = { mimeType: 'mimeType', base64: 'base64' };
          temporaryFile = { ...temporaryFileData } as TemporaryFileInterface;

          const expected: Logo = { ...temporaryFileData } as Logo;
          (Logo as jest.Mock).mockImplementation(() => expected);

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.logo).toBe(expected);
          expect(Logo).toHaveBeenCalledWith(temporaryFile);
        });

        it('should throw an exception when logo is undefined', () => {
          // given
          temporaryFile = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A logo must be defined');
        });
      });

      describe('name', () => {
        it('should bind name from raw data', () => {
          // given
          companyRaw.name = 'A Company name';

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.name).toBe('A Company name');
        });

        it('should throw an exception when name is undefined', () => {
          // given
          companyRaw.name = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company name must be defined');
        });

        it('should throw an exception when name is empty', () => {
          // given
          companyRaw.name = '';

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company name cannot be an empty string');
        });

        it('should throw an exception when name is more than 40 characters', () => {
          // given
          companyRaw.name = 'A company name with more than fourty characters';

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company name must have a maximum of 40 characters');
        });
      });

      describe('presentation', () => {
        it('should bind presentation from raw data', () => {
          // given
          companyRaw.presentation = 'A company presentation';

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.presentation).toStrictEqual('A company presentation');
        });

        it('should throw an exception when presentation is undefined', () => {
          // given
          companyRaw.presentation = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company presentation must be defined');
        });

        it('should throw an exception when presentation is empty', () => {
          // given
          companyRaw.presentation = '';

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company presentation cannot be an empty string');
        });

        it('should throw an exception when presentation is more than 500 characters', () => {
          // given
          companyRaw.presentation = repeat('*', 501);

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company presentation must have a maximum of 500 characters');
        });
      });

      describe('revenue', () => {
        it('should bind revenue from raw data', () => {
          // given
          companyRaw.revenue = CompanyRevenue.REVENUE_BETWEEN_1M_AND_5M;

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.revenue).toStrictEqual(CompanyRevenue.REVENUE_BETWEEN_1M_AND_5M);
        });

        it('should bind revenue from raw data with enum value as a string', () => {
          // given
          companyRaw.revenue = CompanyRevenue.REVENUE_BETWEEN_5M_AND_20M;

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.revenue).toStrictEqual('REVENUE_BETWEEN_5M_AND_20M');
        });

        it('should throw an exception when revenue is undefined', () => {
          // given
          companyRaw.revenue = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company revenue must be defined');
        });

        it('should throw an exception when company revenue is invalid', () => {
          // given
          companyRaw.revenue = 'FAKE_COMPANY_REVENUE' as CompanyRevenue;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company revenue must be a valid one');
        });
      });

      describe('revenueForRD', () => {
        it('should bind revenue for R&D from raw data', () => {
          // given
          companyRaw.revenueForRD = CompanyRevenueForRD.REVENUE_RD_BETWEEN_1M_AND_5M;

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.revenueForRD).toStrictEqual(CompanyRevenueForRD.REVENUE_RD_BETWEEN_1M_AND_5M);
        });

        it('should bind revenue for R&D from raw data with enum value as a string', () => {
          // given
          companyRaw.revenueForRD = CompanyRevenueForRD.REVENUE_RD_MORE_THAN_20M;

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.revenueForRD).toStrictEqual('REVENUE_RD_MORE_THAN_20M');
        });

        it('should throw an exception when company revenue for R&D is invalid', () => {
          // given
          companyRaw.revenueForRD = 'FAKE_COMPANY_REVENUE_FOR_RAND' as CompanyRevenueForRD;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company revenue for R&D must be a valid one');
        });
      });

      describe('size', () => {
        it('should bind size from raw data', () => {
          // given
          companyRaw.size = CompanySize.ZERO_TO_NINE;

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.size).toStrictEqual(CompanySize.ZERO_TO_NINE);
        });

        it('should bind size from raw data with enum value as a string', () => {
          // given
          companyRaw.size = CompanySize.EIGHTY_TO_HUNDRED_NINETEEN;

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.size).toStrictEqual('EIGHTY_TO_HUNDRED_NINETEEN');
        });

        it('should throw an exception when size is undefined', () => {
          // given
          companyRaw.size = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company size must be defined');
        });

        it('should throw an exception when company size is invalid', () => {
          // given
          companyRaw.size = 'FAKE_COMPANY_SIZE' as CompanySize;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company size must be a valid one');
        });
      });

      describe('socialNetworks', () => {
        let socialNetworksRaw: SocialNetworksRawInterface;

        it('should bind social network from raw data', () => {
          // given
          socialNetworksRaw = {} as SocialNetworksRawInterface;
          companyRaw.socialNetworks = {} as SocialNetworksRawInterface;

          const expected: SocialNetworks = { ...socialNetworksRaw } as SocialNetworks;
          (SocialNetworks as jest.Mock).mockImplementation(() => expected);

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.socialNetworks).toBe(expected);
          expect(SocialNetworks).toHaveBeenCalledWith(companyRaw.socialNetworks);
        });

        it('should initialize social network to null when no raw socialNetworks', () => {
          // given
          socialNetworksRaw = undefined;
          companyRaw.socialNetworks = socialNetworksRaw;

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.socialNetworks).toBe(null);
          expect(SocialNetworks).not.toHaveBeenCalled();
        });
      });

      describe('values', () => {
        it('should bind values from raw data', () => {
          // given
          const expected: Value[] = [new Value(valueRaw)];
          companyRaw.values = [valueRaw];

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.values).toStrictEqual(expected);
        });

        it('should throw an exception when company values is undefined', () => {
          // given
          companyRaw.values = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company value must be defined');
        });

        it('should have a minimum of one value', () => {
          // given
          companyRaw.values = [];

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company must have at least one value');
        });

        it('should have a maximum of four value', () => {
          // given
          companyRaw.values = [];
          for (let i: number = 0; i < 6; i++) {
            companyRaw.values.push(valueRaw);
          }

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company must have maximum four values');
        });
      });

      describe('website', () => {
        it('should bind website from raw data', () => {
          // given
          companyRaw.website = 'http://a.company.website';

          // when
          const result: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result.website).toBe('http://a.company.website');
        });

        it('should throw an exception when website is undefined', () => {
          // given
          companyRaw.website = undefined;

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company website must be defined');
        });

        it('should throw an exception when website is empty', () => {
          // given
          companyRaw.website = '';

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company website cannot be an empty string');
        });

        it('should throw an exception when website have not an url format', () => {
          // given
          companyRaw.website = 'A Company website';

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company website must have a URL format');
        });

        it('should throw an exception when website is more than 50 characters', () => {
          // given
          companyRaw.website = 'http://a.company.website.with.more.than.fifty.characters';

          // when
          const result = () => Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

          // then
          expect(result).toThrow('Company creation error:\n - A company website must have a maximum of 50 characters');
        });
      });
    });

    describe('copy', () => {
      it('should create a new instance as a copy of given company', () => {
        // given
        const expected: CompanyInterface = {
          id: 'CompanyId',
          userId: 'User Id',
          status: CompanyStatus.STATUS_PENDING_VALIDATION,
          activityLocations: {} as ActivityLocations,
          address: 'address',
          bachelorsOfEngineeringNumber: 12,
          businessSegment: CompanyBusinessSegment.BUSINESS_SEGMENT_AERONAUTICS_AEROSPACE,
          creationDate: new Date(2018, 9),
          geniusTypes: [CompanyGeniusType.GENIUS_TYPE_AEROSPACE_ENGINEERING],
          logo: {} as Logo,
          name: 'name',
          presentation: 'presentation',
          revenue: CompanyRevenue.REVENUE_BETWEEN_1M_AND_5M,
          revenueForRD: CompanyRevenueForRD.REVENUE_RD_BETWEEN_1M_AND_5M,
          size: CompanySize.EIGHTY_TO_HUNDRED_NINETEEN,
          socialNetworks: {} as SocialNetworksInterface,
          values: {} as ValueInterface[],
          website: 'website',
        } as CompanyInterface;

        // when
        const result: Company = Company.factory.copy(expected);

        expect(result).toMatchObject(expected);
      });
    });
  });

  describe('updateWith()', () => {
    it('should update company with given raw', () => {
      // given
      const existingCompany: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);
      const newCompanyRaw: CompanyRawInterface = {
        activityLocations: { quebec: 1 } as ActivityLocationsRawInterface,
        name: 'new name',
        website: 'http://new.com',
        address: 'new address',
        bachelorsOfEngineeringNumber: 1,
        creationDate: new Date(2000, 9),
        size: CompanySize.MORE_THAN_THREE_HUNDRED,
        revenue: CompanyRevenue.REVENUE_BETWEEN_1M_AND_5M,
        revenueForRD: CompanyRevenueForRD.REVENUE_RD_MORE_THAN_20M,
        businessSegment: CompanyBusinessSegment.BUSINESS_SEGMENT_AGRICULTURE,
        presentation: 'new presentation',
        geniusTypes: [CompanyGeniusType.GENIUS_TYPE_CHEMICAL_ENGINEERING],
        values: [valueRaw, valueRaw],
        logoUuid: '1234',
      };
      const expected: ActivityLocationsRawInterface = { quebec: 1 } as ActivityLocationsRawInterface;
      (ActivityLocations as jest.Mock).mockImplementation(() => expected);

      // when
      existingCompany.updateWith(newCompanyRaw);

      // then
      expect(existingCompany).toMatchObject({
        activityLocations: { quebec: 1 },
        name: 'new name',
        website: 'http://new.com',
        address: 'new address',
        bachelorsOfEngineeringNumber: 1,
        creationDate: new Date(2000, 9),
        size: 'MORE_THAN_THREE_HUNDRED',
        revenue: 'REVENUE_BETWEEN_1M_AND_5M',
        revenueForRD: 'REVENUE_RD_MORE_THAN_20M',
        businessSegment: 'BUSINESS_SEGMENT_AGRICULTURE',
        presentation: 'new presentation',
        geniusTypes: ['GENIUS_TYPE_CHEMICAL_ENGINEERING'],
        values: [valueRaw, valueRaw],
      } as Company);
    });

    it('should trigger same validation rules as when creating a new instance', () => {
      // given
      const existingCompany: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);
      const newCompanyRaw: CompanyRawInterface = {
        activityLocations: null,
        name: null,
        website: null,
        address: null,
        bachelorsOfEngineeringNumber: null,
        creationDate: null,
        size: null,
        revenue: null,
        revenueForRD: null,
        businessSegment: null,
        presentation: null,
        geniusTypes: null,
        values: null,
        logoUuid: null,
      };

      // when
      const result = () => existingCompany.updateWith(newCompanyRaw);

      // then
      expect(result).toThrow(
        'Company creation error:\n' +
          ' - Activity locations must be defined\n' +
          ' - A company address must be defined\n' +
          ' - A bachelors of engineering number must be defined\n' +
          ' - A company business segment must be defined\n' +
          ' - A company creation date must be defined\n' +
          ' - A company genius type must be defined\n' +
          ' - A company name must be defined\n' +
          ' - A company presentation must be defined\n' +
          ' - A company revenue must be defined\n' +
          ' - A company size must be defined\n' +
          ' - A company value must be defined\n' +
          ' - A company website must be defined'
      );
    });
  });

  describe('updateLogo()', () => {
    it('should create new logo instance when temporary file is set', () => {
      // given
      const existingCompany: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);
      const expected: LogoInterface = { base64: temporaryFile.base64 } as LogoInterface;
      (Logo as jest.Mock).mockImplementation(() => expected);

      // when
      existingCompany.updateLogo(temporaryFile);

      // then
      expect(existingCompany.logo.base64).toBe('an-image-as-base64');
    });
  });

  describe('publish()', () => {
    it('should set status of company to PUBLISHED', () => {
      // given
      const existingCompany: Company = Company.factory.validateAndCreateNewCompany(user, companyRaw, temporaryFile);

      // when
      existingCompany.publish();

      // then
      expect(existingCompany.status).toBe(CompanyStatus.STATUS_PUBLISHED);
    });
  });
});

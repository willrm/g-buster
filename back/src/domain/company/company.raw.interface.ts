import { ActivityLocationsRawInterface } from '../activity-locations/activity-locations.raw.interface';
import { SocialNetworksRawInterface } from '../social-networks/social-networks.raw.interface';
import { Uuid } from '../type-aliases';
import { ValueRawInterface } from '../value/value.raw.interface';
import { CompanyBusinessSegment, CompanyGeniusType, CompanyRevenue, CompanyRevenueForRD, CompanySize } from './company-enums';

export interface CompanyRawInterface {
  activityLocations: ActivityLocationsRawInterface;
  address: string;
  bachelorsOfEngineeringNumber?: number;
  businessSegment: CompanyBusinessSegment;
  creationDate: Date;
  geniusTypes: CompanyGeniusType[];
  logoUuid: Uuid;
  name: string;
  presentation: string;
  published?: boolean;
  revenue: CompanyRevenue;
  revenueForRD?: CompanyRevenueForRD;
  size: CompanySize;
  socialNetworks?: SocialNetworksRawInterface;
  values: ValueRawInterface[];
  website: string;
}

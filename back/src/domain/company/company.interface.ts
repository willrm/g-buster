import { ActivityLocationsInterface } from '../activity-locations/activity-locations.interface';
import { LogoInterface } from '../logo/logo.interface';
import { SocialNetworksInterface } from '../social-networks/social-networks.interface';
import { CompanyId, UserId } from '../type-aliases';
import { ValueInterface } from '../value/value.interface';
import { CompanyBusinessSegment, CompanyGeniusType, CompanyRevenue, CompanyRevenueForRD, CompanySize, CompanyStatus } from './company-enums';

export interface CompanyInterface {
  id: CompanyId;
  userId: UserId;
  status: CompanyStatus;
  activityLocations: ActivityLocationsInterface;
  address: string;
  bachelorsOfEngineeringNumber?: number;
  businessSegment: CompanyBusinessSegment;
  creationDate: Date;
  geniusTypes: CompanyGeniusType[];
  logo: LogoInterface;
  name: string;
  presentation: string;
  revenue: CompanyRevenue;
  revenueForRD?: CompanyRevenueForRD;
  size: CompanySize;
  socialNetworks?: SocialNetworksInterface;
  values: ValueInterface[];
  website: string;
}

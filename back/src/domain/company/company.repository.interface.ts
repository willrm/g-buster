import { CompanyId, UserId } from '../type-aliases';
import { CompanyInterface } from './company.interface';

export interface CompanyRepository {
  save(company: CompanyInterface): Promise<CompanyId>;
  findById(id: CompanyId): Promise<CompanyInterface>;
  findByUserId(userId: UserId): Promise<CompanyInterface>;
  findAllByIds(ids: CompanyId[]): Promise<CompanyInterface[]>;
  findAll(): Promise<CompanyInterface[]>;
}

export interface StartDateOfEmploymentInterface {
  asSoonAsPossible?: boolean;
  startDate?: Date;
}

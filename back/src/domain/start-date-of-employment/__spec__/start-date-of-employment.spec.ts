import { StartDateOfEmployment } from '../start-date-of-employment';
import { StartDateOfEmploymentRawInterface } from '../start-date-of-employment.raw.interface';

describe('domain/start-date-of-employment/start-date-of-employment', () => {
  describe('constructor()', () => {
    let startDateOfEmploymentRaw: StartDateOfEmploymentRawInterface;

    beforeEach(() => {
      startDateOfEmploymentRaw = {} as StartDateOfEmploymentRawInterface;
    });
    describe('asSoonAsPossible', () => {
      it('should bind as soon as possible from raw data', () => {
        // given
        startDateOfEmploymentRaw.asSoonAsPossible = true;

        // when
        const result: StartDateOfEmployment = new StartDateOfEmployment(startDateOfEmploymentRaw);

        // then
        expect(result.asSoonAsPossible).toBe(true);
      });
    });

    describe('startDate', () => {
      it('should initialize start date to null when no raw start date', () => {
        // when
        const result: StartDateOfEmployment = new StartDateOfEmployment(startDateOfEmploymentRaw);

        // then
        expect(result.startDate).toBeNull();
      });
      it('should bind as soon as possible from raw data', () => {
        // given
        const fixed: Date = new Date(2019, 9, 23);
        startDateOfEmploymentRaw.startDate = fixed;

        // when
        const result: StartDateOfEmployment = new StartDateOfEmployment(startDateOfEmploymentRaw);

        // then
        expect(result.startDate).toStrictEqual(fixed);
      });
      it('should throw an exception when startDate is before current date', () => {
        // given
        const pastDate: Date = new Date();
        pastDate.setDate(pastDate.getDate() - 1);
        startDateOfEmploymentRaw.startDate = pastDate;

        // when
        const result = () => new StartDateOfEmployment(startDateOfEmploymentRaw).validate('StartDateOfEmployment');

        // then
        expect(result).toThrow('StartDateOfEmployment creation error:\n - A start date of application start date must be after current date');
      });
    });
  });
});

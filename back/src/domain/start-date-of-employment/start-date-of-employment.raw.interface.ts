export interface StartDateOfEmploymentRawInterface {
  asSoonAsPossible?: boolean;
  startDate?: Date;
}

import { MinDate } from 'class-validator';
import { AbstractDomainWithValidation } from '../validation/abstract-domain-with-validation';
import { START_DATE_OF_EMPLOYMENT_START_DATE_MIN_DATE } from './start-date-of-employment-constraints';
import { StartDateOfEmploymentInterface } from './start-date-of-employment.interface';
import { StartDateOfEmploymentRawInterface } from './start-date-of-employment.raw.interface';

export class StartDateOfEmployment extends AbstractDomainWithValidation implements StartDateOfEmploymentInterface {
  asSoonAsPossible?: boolean;

  @MinDate(START_DATE_OF_EMPLOYMENT_START_DATE_MIN_DATE(), {
    message: 'A start date of application start date must be after current date',
  })
  startDate?: Date;

  constructor(startDateOfEmploymentRaw: StartDateOfEmploymentRawInterface) {
    super();

    this.asSoonAsPossible = startDateOfEmploymentRaw.asSoonAsPossible;
    this.startDate = startDateOfEmploymentRaw.startDate ? new Date(startDateOfEmploymentRaw.startDate) : null;
  }
}

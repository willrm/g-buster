import { Footer } from '../footer/footer';
import { FooterInterface } from '../footer/footer.interface';
import { Header } from '../header/header';
import { HeaderInterface } from '../header/header.interface';
import { LayoutInterface } from './layout.interface';

export class Layout implements LayoutInterface {
  footer: Footer;
  header: Header;

  constructor(header: HeaderInterface, footer: FooterInterface) {
    this.header = header;
    this.footer = footer;
  }
}

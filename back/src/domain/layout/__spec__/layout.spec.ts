import { FooterInterface } from '../../footer/footer.interface';
import { HeaderInterface } from '../../header/header.interface';
import { Layout } from '../layout';

describe('domain/layout/Layout', () => {
  describe('constructor()', () => {
    it('should bind header', () => {
      // given
      const expected: HeaderInterface = {
        title: 'A header title',
      } as HeaderInterface;

      // when
      const result: Layout = new Layout(expected, {} as FooterInterface);

      // then
      expect(result.header).toStrictEqual(expected);
    });

    it('should bind footer', () => {
      // given
      const expected: FooterInterface = {
        copyright: 'A footer copyright',
      } as FooterInterface;

      // when
      const result: Layout = new Layout({} as HeaderInterface, expected);

      // then
      expect(result.footer).toStrictEqual(expected);
    });
  });
});

import { FooterInterface } from '../footer/footer.interface';
import { HeaderInterface } from '../header/header.interface';

export interface LayoutInterface {
  footer: FooterInterface;
  header: HeaderInterface;
}

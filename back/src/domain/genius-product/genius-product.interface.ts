import { GeniusProductId } from '../type-aliases';

export interface GeniusProductInterface {
  id: GeniusProductId;
  name: string;
  price: number;
  inTheSpotlightCounter: number;
  featuredCounter: number;
}

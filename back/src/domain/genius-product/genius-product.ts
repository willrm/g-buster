import { IsDefined, IsNotEmpty, Min } from 'class-validator';
import { GeniusProductId } from '../type-aliases';
import { AbstractDomainWithValidation } from '../validation/abstract-domain-with-validation';
import { GENIUS_PRODUCT_FEATURED_COUNTER_EMTPY_VALUE, GENIUS_PRODUCT_IN_THE_SPOTLIGHT_COUNTER_EMTPY_VALUE } from './genius-product-constraints';
import { GeniusProductInterface } from './genius-product.interface';
import { GeniusProductRawInterface } from './genius-product.raw.interface';

export class GeniusProduct extends AbstractDomainWithValidation implements GeniusProductInterface {
  static factory: GeniusProductFactoryInterface = {
    create(geniusProductRawInterface: GeniusProductRawInterface): GeniusProduct {
      return new GeniusProduct(geniusProductRawInterface);
    },
  };

  @IsNotEmpty({ message: 'A genius product id cannot be an empty string' })
  @IsDefined({ message: 'A genius product id must be defined' })
  id: GeniusProductId;

  @IsNotEmpty({ message: 'A genius product name cannot be an empty string' })
  @IsDefined({ message: 'A genius product name must be defined' })
  name: string;

  @Min(0, { message: 'An in the spotlight counter value must be a positive value' })
  @IsDefined({ message: 'An in the spotlight counter value must be defined' })
  inTheSpotlightCounter: number;

  @Min(0, { message: 'A featured counter value must be a positive value' })
  @IsDefined({ message: 'A featured counter value must be defined' })
  featuredCounter: number;

  @Min(0, { message: 'A price value must be a positive value' })
  @IsDefined({ message: 'A price value must be defined' })
  price: number;

  private constructor(geniusProductRawInterface: GeniusProductRawInterface) {
    super();
    this.id = geniusProductRawInterface.id;
    this.name = geniusProductRawInterface.name;
    this.inTheSpotlightCounter =
      geniusProductRawInterface.inTheSpotlightCounter === null
        ? GENIUS_PRODUCT_IN_THE_SPOTLIGHT_COUNTER_EMTPY_VALUE
        : geniusProductRawInterface.inTheSpotlightCounter;
    this.featuredCounter =
      geniusProductRawInterface.featuredCounter === null ? GENIUS_PRODUCT_FEATURED_COUNTER_EMTPY_VALUE : geniusProductRawInterface.featuredCounter;
    this.price = geniusProductRawInterface.price;

    this.validate('GeniusProduct');
  }
}

export interface GeniusProductFactoryInterface {
  create(geniusProductRawInterface: GeniusProductRawInterface): GeniusProduct;
}

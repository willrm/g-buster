import { GeniusProductId } from '../type-aliases';

export interface GeniusProductRawInterface {
  id: GeniusProductId;
  name: string;
  price: number;
  inTheSpotlightCounter: number;
  featuredCounter: number;
}

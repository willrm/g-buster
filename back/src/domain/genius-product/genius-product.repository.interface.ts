import { GeniusProductRawInterface } from './genius-product.raw.interface';

export interface GeniusProductRepository {
  findAll(): Promise<GeniusProductRawInterface[]>;
}

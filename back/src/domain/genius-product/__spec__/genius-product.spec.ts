import { GeniusProduct } from '../genius-product';
import { GeniusProductRawInterface } from '../genius-product.raw.interface';

describe('domain/genius-product/GeniusProduct', () => {
  describe('constructor()', () => {
    let geniusProductRaw: GeniusProductRawInterface;

    beforeEach(() => {
      geniusProductRaw = {
        id: 'an-id',
        name: 'a name',
        price: 0,
        inTheSpotlightCounter: 0,
        featuredCounter: 0,
      };
    });

    describe('id', () => {
      it('should bind id from raw', () => {
        // given
        geniusProductRaw.id = '123456789';

        // when
        const result: GeniusProduct = GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result.id).toBe('123456789');
      });

      it('should throw an exception when id is empty', () => {
        // given
        geniusProductRaw.id = '';

        // when
        const result = () => GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result).toThrow('GeniusProduct creation error:\n - A genius product id cannot be an empty string');
      });

      it('should throw an exception when id is undefined', () => {
        // given
        geniusProductRaw.id = undefined;

        // when
        const result = () => GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result).toThrow('GeniusProduct creation error:\n - A genius product id must be defined');
      });
    });

    describe('name', () => {
      it('should bind name from raw', () => {
        // given
        geniusProductRaw.name = 'A Genius Product';

        // when
        const result: GeniusProduct = GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result.name).toBe('A Genius Product');
      });

      it('should throw an exception when name is empty', () => {
        // given
        geniusProductRaw.name = '';

        // when
        const result = () => GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result).toThrow('GeniusProduct creation error:\n - A genius product name cannot be an empty string');
      });

      it('should throw an exception when name is undefined', () => {
        // given
        geniusProductRaw.name = undefined;

        // when
        const result = () => GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result).toThrow('GeniusProduct creation error:\n - A genius product name must be defined');
      });
    });

    describe('inTheSpotlightCounter', () => {
      it('should bind inTheSpotlightCounter from raw', () => {
        // given
        geniusProductRaw.inTheSpotlightCounter = 123456789;

        // when
        const result: GeniusProduct = GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result.inTheSpotlightCounter).toBe(123456789);
      });

      it('should set inTheSpotlightCounter to 0 when raw is null', () => {
        // given
        geniusProductRaw.inTheSpotlightCounter = null;

        // when
        const result: GeniusProduct = GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result.inTheSpotlightCounter).toBe(0);
      });

      it('should throw an exception when inTheSpotlightCounter is empty', () => {
        // given
        geniusProductRaw.inTheSpotlightCounter = -1;

        // when
        const result = () => GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result).toThrow('GeniusProduct creation error:\n - An in the spotlight counter value must be a positive value');
      });

      it('should throw an exception when inTheSpotlightCounter is undefined', () => {
        // given
        geniusProductRaw.inTheSpotlightCounter = undefined;

        // when
        const result = () => GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result).toThrow('GeniusProduct creation error:\n - An in the spotlight counter value must be defined');
      });
    });
    describe('featuredCounter', () => {
      it('should bind featuredCounter from raw', () => {
        // given
        geniusProductRaw.featuredCounter = 123456789;

        // when
        const result: GeniusProduct = GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result.featuredCounter).toBe(123456789);
      });

      it('should set featuredCounter to 0 when raw is null', () => {
        // given
        geniusProductRaw.featuredCounter = null;

        // when
        const result: GeniusProduct = GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result.featuredCounter).toBe(0);
      });

      it('should throw an exception when featuredCounter is empty', () => {
        // given
        geniusProductRaw.featuredCounter = -1;

        // when
        const result = () => GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result).toThrow('GeniusProduct creation error:\n - A featured counter value must be a positive value');
      });

      it('should throw an exception when featuredCounter is undefined', () => {
        // given
        geniusProductRaw.featuredCounter = undefined;

        // when
        const result = () => GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result).toThrow('GeniusProduct creation error:\n - A featured counter value must be defined');
      });
    });
    describe('price', () => {
      it('should bind price from raw', () => {
        // given
        geniusProductRaw.price = 123456789;

        // when
        const result: GeniusProduct = GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result.price).toBe(123456789);
      });

      it('should throw an exception when price is empty', () => {
        // given
        geniusProductRaw.price = -1;

        // when
        const result = () => GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result).toThrow('GeniusProduct creation error:\n - A price value must be a positive value');
      });

      it('should throw an exception when price is undefined', () => {
        // given
        geniusProductRaw.price = undefined;

        // when
        const result = () => GeniusProduct.factory.create(geniusProductRaw);

        // then
        expect(result).toThrow('GeniusProduct creation error:\n - A price value must be defined');
      });
    });
  });
});

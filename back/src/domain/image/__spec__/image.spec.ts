import { Image } from '../image';
import { ImageRawInterface } from '../image.raw.interface';

describe('domain/image/Image', () => {
  describe('constructor()', () => {
    let imageRaw: ImageRawInterface;
    beforeEach(() => {
      imageRaw = { alternativeText: '', source: '' } as ImageRawInterface;
    });

    it('should bind source from raw', () => {
      // given
      imageRaw.source = 'https://awesome.png';

      // when
      const result: Image = new Image(imageRaw);

      // then
      expect(result.source).toBe('https://awesome.png');
    });

    it('should bind alternativeText from raw', () => {
      // given
      imageRaw.alternativeText = 'An image alternative text';

      // when
      const result: Image = new Image(imageRaw);

      // then
      expect(result.alternativeText).toBe('An image alternative text');
    });
  });
});

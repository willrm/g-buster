import { ImageInterface } from './image.interface';
import { ImageRawInterface } from './image.raw.interface';

export class Image implements ImageInterface {
  alternativeText: string;
  source: string;

  constructor(imageRaw: ImageRawInterface) {
    this.source = imageRaw.source;
    this.alternativeText = imageRaw.alternativeText;
  }
}

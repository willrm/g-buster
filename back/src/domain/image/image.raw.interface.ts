export interface ImageRawInterface {
  alternativeText: string;
  source: string;
}

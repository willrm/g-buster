export interface ImageInterface {
  alternativeText: string;
  source: string;
}

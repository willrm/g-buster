import { Image } from '../../image/image';
import { ImageRawInterface } from '../../image/image.raw.interface';
import { DummyPage } from '../dummy-page';
import { DummyPageRawInterface } from '../dummy-page.raw.interface';

jest.mock('../../image/image');

describe('domain/dummy-page/DummyPage', () => {
  describe('constructor()', () => {
    let dummyPageRaw: DummyPageRawInterface;
    beforeEach(() => {
      dummyPageRaw = {
        title: '',
        content: '',
        image: { source: '', alternativeText: '' },
      };
    });

    it('should bind title from raw', () => {
      // given
      dummyPageRaw.title = '<h1>A title</h1>';

      // when
      const result: DummyPage = new DummyPage(dummyPageRaw);

      // then
      expect(result.title).toBe('<h1>A title</h1>');
    });

    it('should bind content from raw', () => {
      // given
      dummyPageRaw.content = '<span>A content</span>';

      // when
      const result: DummyPage = new DummyPage(dummyPageRaw);

      // then
      expect(result.content).toBe('<span>A content</span>');
    });

    it('should bind image from raw', () => {
      // given
      const imageData: ImageRawInterface = {
        source: 'https://an-image.png',
        alternativeText: 'A wonderful image',
      } as ImageRawInterface;
      dummyPageRaw.image = imageData;

      const expected: Image = { ...imageData } as Image;
      (Image as jest.Mock).mockImplementation(() => expected);

      // when
      const result: DummyPage = new DummyPage(dummyPageRaw);

      // then
      expect(result.image).toBe(expected);
      expect(Image).toHaveBeenCalledWith(dummyPageRaw.image);
    });

    it('should set generated date with current date time', () => {
      // given
      const fixedDate: Date = new Date('2017-06-13T04:41:20');
      // @ts-ignore
      jest.spyOn(global, 'Date').mockImplementationOnce(() => fixedDate);

      // when
      const result: DummyPage = new DummyPage({} as DummyPageRawInterface);

      // then
      expect(result.generatedDate).toBe(fixedDate);
    });
  });
});

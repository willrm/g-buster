import { Image } from '../image/image';
import { HtmlMarkup } from '../type-aliases';
import { DummyPageInterface } from './dummy-page.interface';
import { DummyPageRawInterface } from './dummy-page.raw.interface';

export class DummyPage implements DummyPageInterface {
  content: HtmlMarkup;
  generatedDate: Date;
  image: Image;
  title: HtmlMarkup;

  constructor(dummyPageRaw: DummyPageRawInterface) {
    this.title = dummyPageRaw.title;
    this.content = dummyPageRaw.content;
    this.image = new Image(dummyPageRaw.image);
    this.generatedDate = new Date();
  }
}

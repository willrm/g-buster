import { ImageRawInterface } from '../image/image.raw.interface';
import { HtmlMarkup } from '../type-aliases';

export interface DummyPageRawInterface {
  content: HtmlMarkup;
  image: ImageRawInterface;
  title: HtmlMarkup;
}

import { ImageInterface } from '../image/image.interface';
import { HtmlMarkup } from '../type-aliases';

export interface DummyPageInterface {
  content: HtmlMarkup;
  generatedDate: Date;
  image: ImageInterface;
  title: HtmlMarkup;
}

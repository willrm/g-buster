import { Lang, Uid } from '../type-aliases';
import { DummyPageInterface } from './dummy-page.interface';

export interface DummyPageRepository {
  findByLangAndUid(lang: Lang, uid: Uid): Promise<DummyPageInterface>;
}

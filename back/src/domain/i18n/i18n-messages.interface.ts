export interface I18nMessagesInterface {
  [key: string]: string | { [key: string]: string };
}

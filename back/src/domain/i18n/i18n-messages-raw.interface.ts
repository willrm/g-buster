export interface I18nMessagesRawInterface {
  [key: string]: string | { [key: string]: string };
}

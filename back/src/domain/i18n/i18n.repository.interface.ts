import { Lang } from '../type-aliases';
import { I18nMessagesInterface } from './i18n-messages.interface';

export interface I18nRepository {
  findMessagesByLang: (lang: Lang) => Promise<I18nMessagesInterface>;
}

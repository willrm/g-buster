import { I18nMessagesRawInterface } from './i18n-messages-raw.interface';
import { I18nMessagesInterface } from './i18n-messages.interface';

export class I18nMessages implements I18nMessagesInterface {
  [key: string]: string | { [key: string]: string };

  constructor(i18nMessagesRaw: I18nMessagesRawInterface) {
    Object.assign(this, i18nMessagesRaw);
  }
}

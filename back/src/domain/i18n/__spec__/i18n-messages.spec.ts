import { I18nMessages } from '../i18n-messages';
import { I18nMessagesRawInterface } from '../i18n-messages-raw.interface';

describe('domain/i18n/I18nMessages', () => {
  describe('constructor()', () => {
    it('should bind value from raw', () => {
      // given
      const i18nMessagesRaw: I18nMessagesRawInterface = {
        welcome: 'A welcome message',
        thanks: {
          you: 'A thank you message',
        },
      };

      // when
      const result: I18nMessages = new I18nMessages(i18nMessagesRaw);

      // then
      expect(result).toHaveProperty('welcome', 'A welcome message');
      expect(result).toHaveProperty('thanks', {
        you: 'A thank you message',
      });
    });
  });
});

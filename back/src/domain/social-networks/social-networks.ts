import { IsUrl } from 'class-validator';
import { AbstractDomainWithValidation } from '../validation/abstract-domain-with-validation';
import { SocialNetworksInterface } from './social-networks.interface';
import { SocialNetworksRawInterface } from './social-networks.raw.interface';

export class SocialNetworks extends AbstractDomainWithValidation implements SocialNetworksInterface {
  @IsUrl(
    {},
    {
      message: 'A facebook link must have a URL format',
    }
  )
  facebook?: string;

  @IsUrl(
    {},
    {
      message: 'A linkedIn link must have a URL format',
    }
  )
  linkedIn?: string;

  constructor(socialNetworksRaw: SocialNetworksRawInterface) {
    super();

    this.linkedIn = socialNetworksRaw.linkedIn;
    this.facebook = socialNetworksRaw.facebook;
  }
}

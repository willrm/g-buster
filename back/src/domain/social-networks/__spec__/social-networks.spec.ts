import { SocialNetworks } from '../social-networks';
import { SocialNetworksRawInterface } from '../social-networks.raw.interface';

describe('domain/social-networks/SocialNetworks', () => {
  describe('constructor()', () => {
    let socialNetworksRaw: SocialNetworksRawInterface;
    beforeEach(() => {
      socialNetworksRaw = {
        linkedIn: 'https://linkedin.com/company/awesome-company',
        facebook: 'https://facebook.com/awesome-company',
      } as SocialNetworksRawInterface;
    });

    describe('linkedIn', () => {
      it('should bind linkedIn url from raw', () => {
        // given
        socialNetworksRaw.linkedIn = 'https://linkedin.com/company/awesome-company';

        // when
        const result: SocialNetworks = new SocialNetworks(socialNetworksRaw);

        // then
        expect(result.linkedIn).toBe('https://linkedin.com/company/awesome-company');
      });
      it('should throw an exception if linkedin have not an url format', () => {
        // given
        socialNetworksRaw.linkedIn = 'A linkedIn URL';

        // when
        const result = () => new SocialNetworks(socialNetworksRaw).validate('SocialNetworks');

        // then
        expect(result).toThrow('SocialNetworks creation error:\n - A linkedIn link must have a URL format');
      });
    });

    describe('facebook', () => {
      it('should bind facebook url from raw', () => {
        // given
        socialNetworksRaw.facebook = 'https://facebook.com/awesome-company';

        // when
        const result: SocialNetworks = new SocialNetworks(socialNetworksRaw);

        // then
        expect(result.facebook).toBe('https://facebook.com/awesome-company');
      });

      it('should throw an exception if facebook have not an url format', () => {
        // given
        socialNetworksRaw.facebook = 'A facebook URL';

        // when
        const result = () => new SocialNetworks(socialNetworksRaw).validate('SocialNetworks');

        // then
        expect(result).toThrow('SocialNetworks creation error:\n - A facebook link must have a URL format');
      });
    });
  });
});

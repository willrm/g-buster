import { TravelingFrequency } from './traveling-enums';

export interface TravelingRawInterface {
  isTraveling?: boolean;
  quebec?: TravelingFrequency;
  canada?: TravelingFrequency;
  international?: TravelingFrequency;
}

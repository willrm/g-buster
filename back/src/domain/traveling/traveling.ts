import { TravelingFrequency } from './traveling-enums';
import { TravelingInterface } from './traveling.interface';
import { TravelingRawInterface } from './traveling.raw.interface';

export class Traveling implements TravelingInterface {
  isTraveling?: boolean;
  canada?: TravelingFrequency;
  international?: TravelingFrequency;
  quebec?: TravelingFrequency;

  constructor(travelingRaw: TravelingRawInterface) {
    this.isTraveling = travelingRaw.isTraveling;
    if (this.isTraveling) {
      this.canada = travelingRaw.canada;
      this.international = travelingRaw.international;
      this.quebec = travelingRaw.quebec;
    }
  }
}

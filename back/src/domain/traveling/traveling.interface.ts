import { TravelingFrequency } from './traveling-enums';

export interface TravelingInterface {
  isTraveling?: boolean;
  quebec?: TravelingFrequency;
  canada?: TravelingFrequency;
  international?: TravelingFrequency;
}

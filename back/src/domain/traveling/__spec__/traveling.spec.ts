import { Traveling } from '../traveling';
import { TravelingFrequency } from '../traveling-enums';
import { TravelingRawInterface } from '../traveling.raw.interface';

describe('domain/traveling/traveling', () => {
  describe('constructor()', () => {
    let travelingRaw: TravelingRawInterface;

    beforeEach(() => {
      travelingRaw = {} as TravelingRawInterface;
    });
    describe('isTraveling', () => {
      it('should bind isTraveling from raw data', () => {
        // given
        travelingRaw.isTraveling = true;

        // when
        const result: Traveling = new Traveling(travelingRaw);

        // then
        expect(result.isTraveling).toBe(true);
      });
    });

    describe('quebec', () => {
      it('should bind quebec from raw data', () => {
        // given
        travelingRaw.isTraveling = true;
        travelingRaw.quebec = TravelingFrequency.TRAVELING_FREQUENT;

        // when
        const result: Traveling = new Traveling(travelingRaw);

        // then
        expect(result.quebec).toStrictEqual(TravelingFrequency.TRAVELING_FREQUENT);
      });
      it('should not bind quebec from raw data when isTraveling is false', () => {
        // given
        travelingRaw.isTraveling = false;
        travelingRaw.quebec = TravelingFrequency.TRAVELING_FREQUENT;

        // when
        const result: Traveling = new Traveling(travelingRaw);

        // then
        expect(result.quebec).toBeUndefined();
      });
    });

    describe('canada', () => {
      it('should bind canada from raw data', () => {
        // given
        travelingRaw.isTraveling = true;
        travelingRaw.canada = TravelingFrequency.TRAVELING_FREQUENT;

        // when
        const result: Traveling = new Traveling(travelingRaw);

        // then
        expect(result.canada).toStrictEqual(TravelingFrequency.TRAVELING_FREQUENT);
      });
      it('should not bind canada from raw data when isTraveling is false', () => {
        // given
        travelingRaw.isTraveling = false;
        travelingRaw.canada = TravelingFrequency.TRAVELING_FREQUENT;

        // when
        const result: Traveling = new Traveling(travelingRaw);

        // then
        expect(result.canada).toBeUndefined();
      });
    });

    describe('international', () => {
      it('should bind international from raw data', () => {
        // given
        travelingRaw.isTraveling = true;
        travelingRaw.international = TravelingFrequency.TRAVELING_OCCASIONAL;

        // when
        const result: Traveling = new Traveling(travelingRaw);

        // then
        expect(result.international).toStrictEqual(TravelingFrequency.TRAVELING_OCCASIONAL);
      });
      it('should not bind international from raw data when isTraveling is false', () => {
        // given
        travelingRaw.isTraveling = false;
        travelingRaw.international = TravelingFrequency.TRAVELING_OCCASIONAL;

        // when
        const result: Traveling = new Traveling(travelingRaw);

        // then
        expect(result.international).toBeUndefined();
      });
    });
  });
});

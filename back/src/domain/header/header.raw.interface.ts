import { ImageRawInterface } from '../image/image.raw.interface';

export interface HeaderRawInterface {
  logo: ImageRawInterface;
  title: string;
}

import { Image } from '../image/image';
import { HeaderInterface } from './header.interface';
import { HeaderRawInterface } from './header.raw.interface';

export class Header implements HeaderInterface {
  logo: Image;
  title: string;

  constructor(headerRaw: HeaderRawInterface) {
    this.title = headerRaw.title;
    this.logo = new Image(headerRaw.logo);
  }
}

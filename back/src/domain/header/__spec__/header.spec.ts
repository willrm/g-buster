import { Image } from '../../image/image';
import { ImageRawInterface } from '../../image/image.raw.interface';
import { Header } from '../header';
import { HeaderRawInterface } from '../header.raw.interface';

jest.mock('../../image/image');

describe('domain/header/Header', () => {
  beforeEach(() => {
    (Image as jest.Mock).mockClear();
  });

  describe('constructor()', () => {
    let headerRaw: HeaderRawInterface;
    beforeEach(() => {
      headerRaw = { title: '', logo: {} } as HeaderRawInterface;
    });

    it('should bind title from raw', () => {
      // given
      headerRaw.title = 'A header title';

      // when
      const result: Header = new Header(headerRaw);

      // then
      expect(result.title).toBe('A header title');
    });

    it('should bind image from raw', () => {
      // given
      const imageData: ImageRawInterface = {
        source: 'https://an-image.png',
        alternativeText: 'A wonderful image',
      } as ImageRawInterface;
      headerRaw.logo = imageData;

      const expected: Image = { ...imageData } as Image;
      (Image as jest.Mock).mockImplementation(() => expected);

      // when
      const result: Header = new Header(headerRaw);

      // then
      expect(result.logo).toBe(expected);
      expect(Image).toHaveBeenCalledWith(headerRaw.logo);
    });
  });
});

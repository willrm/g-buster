import { Lang } from '../type-aliases';
import { HeaderInterface } from './header.interface';

export interface HeaderRepository {
  findByLang(lang: Lang): Promise<HeaderInterface>;
}

import { ImageInterface } from '../image/image.interface';

export interface HeaderInterface {
  logo: ImageInterface;
  title: string;
}

import { AnnualSalaryRange } from '../annual-salary-range';
import { AnnualSalaryRangeRawInterface } from '../annual-salary-range.raw.interface';

describe('domain/annual-salary-range/annual-salary-range', () => {
  describe('constructor()', () => {
    let annualSalaryRangeRaw: AnnualSalaryRangeRawInterface;

    beforeEach(() => {
      annualSalaryRangeRaw = {} as AnnualSalaryRangeRawInterface;
    });
    describe('minimum salary', () => {
      it('should bind minimum salary from raw data', () => {
        // given
        annualSalaryRangeRaw.minimumSalary = 123456;

        // when
        const result: AnnualSalaryRange = new AnnualSalaryRange(annualSalaryRangeRaw);

        // then
        expect(result.minimumSalary).toBe(123456);
      });

      it('should throw an exception when minimum salary is negative', () => {
        // given
        annualSalaryRangeRaw.minimumSalary = -10;

        // when
        const result = () => new AnnualSalaryRange(annualSalaryRangeRaw).validate('AnnualSalaryRange');

        // then
        expect(result).toThrow('AnnualSalaryRange creation error:\n - A minimum salary value must be a positive value');
      });

      it('should throw an exception when minimum salary is more than maximum salary', () => {
        // given
        annualSalaryRangeRaw.minimumSalary = 1234;
        annualSalaryRangeRaw.maximumSalary = 123;

        // when
        const result = () => new AnnualSalaryRange(annualSalaryRangeRaw).validate('AnnualSalaryRange');

        // then
        expect(result).toThrow('AnnualSalaryRange creation error:\n - A maximumSalary must be greater than minimumSalary');
      });
    });
    describe('maximum salary', () => {
      it('should bind maximum salary from raw data', () => {
        // given
        annualSalaryRangeRaw.maximumSalary = 1234567;

        // when
        const result: AnnualSalaryRange = new AnnualSalaryRange(annualSalaryRangeRaw);

        // then
        expect(result.maximumSalary).toBe(1234567);
      });
      it('should throw an exception when maximum salary is abose 9 999 999', () => {
        // given
        annualSalaryRangeRaw.maximumSalary = 10000000;

        // when
        const result = () => new AnnualSalaryRange(annualSalaryRangeRaw).validate('AnnualSalaryRange');

        // then
        expect(result).toThrow('AnnualSalaryRange creation error:\n - A maximum salary value must be less than 9999999');
      });
    });
  });
});

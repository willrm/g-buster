export interface AnnualSalaryRangeRawInterface {
  minimumSalary?: number;
  maximumSalary?: number;
}

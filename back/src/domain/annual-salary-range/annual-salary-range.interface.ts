export interface AnnualSalaryRangeInterface {
  minimumSalary?: number;
  maximumSalary?: number;
}

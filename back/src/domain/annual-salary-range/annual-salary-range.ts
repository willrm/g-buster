import { Max, Min, Validate } from 'class-validator';
import { AbstractDomainWithValidation } from '../validation/abstract-domain-with-validation';
import { IsGreaterThanValidationConstraint } from '../validation/validation-constraints/is-greater-than-validation-constraint';
import { ANNUAL_SALARY_RANGE_MAXIMUM_SALARY, ANNUAL_SALARY_RANGE_MINIMUM_SALARY } from './annual-salary-range-contraints';
import { AnnualSalaryRangeInterface } from './annual-salary-range.interface';
import { AnnualSalaryRangeRawInterface } from './annual-salary-range.raw.interface';

export class AnnualSalaryRange extends AbstractDomainWithValidation implements AnnualSalaryRangeInterface {
  @Min(ANNUAL_SALARY_RANGE_MINIMUM_SALARY, { message: 'A minimum salary value must be a positive value' })
  minimumSalary?: number;

  @Validate(IsGreaterThanValidationConstraint, ['minimumSalary'])
  @Max(ANNUAL_SALARY_RANGE_MAXIMUM_SALARY, { message: 'A maximum salary value must be less than $constraint1' })
  maximumSalary?: number;

  constructor(annualSalaryRangeRaw: AnnualSalaryRangeRawInterface) {
    super();

    this.minimumSalary = annualSalaryRangeRaw.minimumSalary;
    this.maximumSalary = annualSalaryRangeRaw.maximumSalary;
  }
}

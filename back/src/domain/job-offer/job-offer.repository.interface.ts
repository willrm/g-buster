import { JobOfferId, UserId } from '../type-aliases';
import { JobOfferStatus } from './job-offer-enums';
import { JobOfferInterface } from './job-offer.interface';

export interface JobOfferRepository {
  findAll(): Promise<JobOfferInterface[]>;
  findAllByUserId(userId: UserId): Promise<JobOfferInterface[]>;
  findAllByStatus(status: JobOfferStatus): Promise<JobOfferInterface[]>;
  findById(id: JobOfferId): Promise<JobOfferInterface>;
  save(jobOffer: JobOfferInterface): Promise<JobOfferId>;
}

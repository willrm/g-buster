import { AdvantagesRawInterface } from '../advantages/advantages.raw.interface';
import { AnnualSalaryRangeRawInterface } from '../annual-salary-range/annual-salary-range.raw.interface';
import { StartDateOfEmploymentRawInterface } from '../start-date-of-employment/start-date-of-employment.raw.interface';
import { TravelingRawInterface } from '../traveling/traveling.raw.interface';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from './job-offer-enums';

export interface JobOfferRawInterface {
  draft?: boolean;
  jobStatus: JobOfferJobStatus;
  jobType: JobOfferJobType;
  title: string;
  city: string;
  region: JobOfferRegion;
  targetCustomers: JobOfferTargetCustomer[];
  requiredExperiences: JobOfferRequiredExperience[];
  positionType: JobOfferPositionType;
  requestedSpecialities: JobOfferRequestedSpeciality[];
  description: string;
  specificities?: string;
  traveling?: TravelingRawInterface;
  advantages?: AdvantagesRawInterface;
  annualSalaryRange?: AnnualSalaryRangeRawInterface;
  wantedPersonalities?: JobOfferWantedPersonality[];
  endDateOfApplication?: Date;
  startDateOfEmployment: StartDateOfEmploymentRawInterface;
  emailAddressForReceiptOfApplications: string;
  internalReferenceNumber?: string;
}

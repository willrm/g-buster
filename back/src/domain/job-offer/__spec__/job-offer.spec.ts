import { repeat } from 'lodash';
import { Advantages } from '../../advantages/advantages';
import { AdvantageFinancial, AdvantageHealth, AdvantageLifeBalance, AdvantageProfessional, AdvantageSocial } from '../../advantages/advantages-enums';
import { AdvantagesRawInterface } from '../../advantages/advantages.raw.interface';
import { AnnualSalaryRange } from '../../annual-salary-range/annual-salary-range';
import { AnnualSalaryRangeRawInterface } from '../../annual-salary-range/annual-salary-range.raw.interface';
import { CompanyInterface } from '../../company/company.interface';
import { StartDateOfEmployment } from '../../start-date-of-employment/start-date-of-employment';
import { StartDateOfEmploymentRawInterface } from '../../start-date-of-employment/start-date-of-employment.raw.interface';
import { Traveling } from '../../traveling/traveling';
import { TravelingFrequency } from '../../traveling/traveling-enums';
import { TravelingRawInterface } from '../../traveling/traveling.raw.interface';
import { UserInterface } from '../../user/user.interface';
import { JobOffer } from '../job-offer';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from '../job-offer-enums';
import { JobOfferInterface } from '../job-offer.interface';
import { JobOfferRawInterface } from '../job-offer.raw.interface';

jest.mock('../../advantages/advantages');
jest.mock('../../traveling/traveling');
jest.mock('../../annual-salary-range/annual-salary-range');
jest.mock('../../start-date-of-employment/start-date-of-employment');

describe('domain/job-offer/JobOffer', () => {
  let user: UserInterface;
  let company: CompanyInterface;
  let jobOfferRaw: JobOfferRawInterface;

  beforeEach(() => {
    (Advantages as jest.Mock).mockClear();
    (Traveling as jest.Mock).mockClear();
    (AnnualSalaryRange as jest.Mock).mockClear();
    (StartDateOfEmployment as jest.Mock).mockClear();

    user = { id: '42' } as UserInterface;
    company = { id: '1337' } as CompanyInterface;

    jobOfferRaw = {
      title: 'title',
      jobStatus: JobOfferJobStatus.JOB_STATUS_PART_TIME,
      jobType: JobOfferJobType.JOB_TYPE_INTERNSHIP,
      city: 'city',
      region: JobOfferRegion.REGION_BAS_ST_LAURENT,
      targetCustomers: [JobOfferTargetCustomer.TARGET_CUSTOMER_STUDENTS_AND_GRADUATES],
      requiredExperiences: [JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS],
      positionType: JobOfferPositionType.POSITION_TYPE_JUNIOR_ENGINEERS_CEP,
      requestedSpecialities: [JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ENVIRONMENT],
      description: 'description',
      emailAddressForReceiptOfApplications: 'test@genium360.com',
      startDateOfEmployment: { asSoonAsPossible: true } as StartDateOfEmploymentRawInterface,
    } as JobOfferRawInterface;
  });

  describe('factory', () => {
    describe('validateAndCreateNewJobOffer()', () => {
      describe('id', () => {
        it('should initialize id to null', () => {
          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.id).toBeNull();
        });
      });

      describe('userId', () => {
        it('should bind id from user', () => {
          // given
          user = { id: '123456789' } as UserInterface;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.userId).toBe('123456789');
        });

        it('should throw an exception when there is no user', () => {
          // given
          user = undefined as UserInterface;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer user id must be defined');
        });

        it('should throw an exception when user id is undefined', () => {
          // given
          user = { id: undefined } as UserInterface;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer user id must be defined');
        });

        it('should throw an exception when user id is empty', () => {
          // given
          user = { id: '' } as UserInterface;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer user id cannot be an empty string');
        });
      });

      describe('companyId', () => {
        it('should bind id from company', () => {
          // given
          company = { id: '987654321' } as CompanyInterface;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.companyId).toBe('987654321');
        });

        it('should throw an exception when there is no company', () => {
          // given
          company = undefined as CompanyInterface;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer company id must be defined');
        });

        it('should throw an exception when company id is undefined', () => {
          // given
          company = { id: undefined } as CompanyInterface;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer company id must be defined');
        });

        it('should throw an exception when company id is empty', () => {
          // given
          company = { id: '' } as CompanyInterface;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer company id cannot be an empty string');
        });
      });

      describe('status', () => {
        it('should initialize status to draft when draft is true', () => {
          // given
          jobOfferRaw.draft = true;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.status).toBe(JobOfferStatus.STATUS_DRAFT);
        });

        it('should initialize status to pending validation when draft is false', () => {
          // given
          jobOfferRaw.draft = false;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.status).toBe(JobOfferStatus.STATUS_PENDING_VALIDATION);
        });

        it('should initialize status to pending validation when draft is undefined', () => {
          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.status).toBe(JobOfferStatus.STATUS_PENDING_VALIDATION);
        });
      });

      describe('title', () => {
        it('should bind title from raw data', () => {
          // given
          jobOfferRaw.title = 'A job title';

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.title).toBe('A job title');
        });

        it('should throw an exception when title is undefined', () => {
          // given
          jobOfferRaw.title = undefined;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer title must be defined');
        });

        it('should throw an exception when title is empty', () => {
          // given
          jobOfferRaw.title = '';

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer title cannot be an empty string');
        });

        it('should throw an exception when title is more than 150 characters', () => {
          // given
          jobOfferRaw.title = repeat('*', 151);

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer title must have a maximum of 150 characters');
        });
      });

      describe('jobStatus', () => {
        it('should bind job status from raw data', () => {
          // given
          jobOfferRaw.jobStatus = JobOfferJobStatus.JOB_STATUS_FULL_TIME;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.jobStatus).toBe(JobOfferJobStatus.JOB_STATUS_FULL_TIME);
        });

        it('should bind job status from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.jobStatus = JobOfferJobStatus.JOB_STATUS_PART_TIME;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.jobStatus).toStrictEqual('JOB_STATUS_PART_TIME');
        });

        it('should throw an exception when job status is undefined', () => {
          // given
          jobOfferRaw.jobStatus = undefined;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer job status must be defined');
        });

        it('should throw an exception when job status is invalid', () => {
          // given
          jobOfferRaw.jobStatus = 'FAKE_JOB_STATUS' as JobOfferJobStatus;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer job status must be a valid one');
        });
      });

      describe('jobType', () => {
        it('should bind job type from raw data', () => {
          // given
          jobOfferRaw.jobType = JobOfferJobType.JOB_TYPE_REGULAR;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.jobType).toBe(JobOfferJobType.JOB_TYPE_REGULAR);
        });

        it('should bind job type from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.jobType = JobOfferJobType.JOB_TYPE_TEMPORARY;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.jobType).toStrictEqual('JOB_TYPE_TEMPORARY');
        });

        it('should throw an exception when job type is undefined', () => {
          // given
          jobOfferRaw.jobType = undefined;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer job type must be defined');
        });

        it('should throw an exception when job type is invalid', () => {
          // given
          jobOfferRaw.jobType = 'FAKE_JOB_TYPE' as JobOfferJobType;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer job type must be a valid one');
        });
      });

      describe('city', () => {
        it('should bind city from raw data', () => {
          // given
          jobOfferRaw.city = 'Montréal';

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.city).toBe('Montréal');
        });

        it('should throw an exception when city is undefined', () => {
          // given
          jobOfferRaw.city = undefined;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer city must be defined');
        });

        it('should throw an exception when city is empty', () => {
          // given
          jobOfferRaw.city = '';

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer city cannot be an empty string');
        });

        it('should throw an exception when city is more than 100 characters', () => {
          // given
          jobOfferRaw.city = repeat('*', 101);

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer city must have a maximum of 100 characters');
        });
      });

      describe('region', () => {
        it('should bind region from raw data', () => {
          // given
          jobOfferRaw.region = JobOfferRegion.REGION_MONTREAL;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.region).toBe(JobOfferRegion.REGION_MONTREAL);
        });

        it('should bind region from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.region = JobOfferRegion.REGION_CAPITALE_NATIONALE;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.region).toStrictEqual('REGION_CAPITALE_NATIONALE');
        });

        it('should throw an exception when region is undefined', () => {
          // given
          jobOfferRaw.region = undefined;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer region must be defined');
        });

        it('should throw an exception when region is invalid', () => {
          // given
          jobOfferRaw.region = 'FAKE_REGION' as JobOfferRegion;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer region must be a valid one');
        });
      });

      describe('targetCustomers', () => {
        it('should bind target customers from raw data', () => {
          // given
          jobOfferRaw.targetCustomers = [JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS];

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.targetCustomers).toStrictEqual([JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS]);
        });

        it('should bind target customers from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.targetCustomers = [JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP];

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.targetCustomers).toStrictEqual(['TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP']);
        });

        it('should throw an exception when job offer target customers is undefined', () => {
          // given
          jobOfferRaw.targetCustomers = undefined;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer target customer must be defined');
        });

        it('should throw an exception when job offer target customers is empty', () => {
          // given
          jobOfferRaw.targetCustomers = [];

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer must have at least one target customer');
        });

        it('should throw an exception when job offer target customer is invalid', () => {
          // given
          jobOfferRaw.targetCustomers = ['FAKE_TARGET_CUSTOMER' as JobOfferTargetCustomer];

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer target customer must be a valid one');
        });
      });

      describe('requiredExperiences', () => {
        it('should bind required experiences from raw data', () => {
          // given
          jobOfferRaw.requiredExperiences = [JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS];

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.requiredExperiences).toStrictEqual([JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS]);
        });

        it('should bind required experiences from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.requiredExperiences = [JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS];

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.requiredExperiences).toStrictEqual(['REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS']);
        });

        it('should throw an exception when job offer required experiences is undefined', () => {
          // given
          jobOfferRaw.requiredExperiences = undefined;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer required experience must be defined');
        });

        it('should throw an exception when job offer required experiences is empty', () => {
          // given
          jobOfferRaw.requiredExperiences = [];

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer must have at least one required experience');
        });

        it('should throw an exception when job offer required experience is invalid', () => {
          // given
          jobOfferRaw.requiredExperiences = ['FAKE_REQUIRED_EXPERIENCE' as JobOfferRequiredExperience];

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer required experience must be a valid one');
        });
      });

      describe('positionType', () => {
        it('should bind position type from raw data', () => {
          // given
          jobOfferRaw.positionType = JobOfferPositionType.POSITION_TYPE_ANALYST;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.positionType).toBe(JobOfferPositionType.POSITION_TYPE_ANALYST);
        });

        it('should bind position type from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.positionType = JobOfferPositionType.POSITION_TYPE_DESIGNER;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.positionType).toStrictEqual('POSITION_TYPE_DESIGNER');
        });

        it('should throw an exception when position type is undefined', () => {
          // given
          jobOfferRaw.positionType = undefined;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer position type must be defined');
        });

        it('should throw an exception when position type is invalid', () => {
          // given
          jobOfferRaw.positionType = 'FAKE_POSITION_TYPE' as JobOfferPositionType;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer position type must be a valid one');
        });
      });

      describe('requestedSpecialities', () => {
        it('should bind requested specialities from raw data', () => {
          // given
          jobOfferRaw.requestedSpecialities = [JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL];

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.requestedSpecialities).toStrictEqual([JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL]);
        });

        it('should bind requested specialities from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.requestedSpecialities = [JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION];

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.requestedSpecialities).toStrictEqual(['REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION']);
        });

        it('should throw an exception when job offer requested specialities is undefined', () => {
          // given
          jobOfferRaw.requestedSpecialities = undefined;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer requested speciality must be defined');
        });

        it('should throw an exception when job offer requested specialities is empty', () => {
          // given
          jobOfferRaw.requestedSpecialities = [];

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer must have at least one requested speciality');
        });

        it('should throw an exception when job offer required experience is invalid', () => {
          // given
          jobOfferRaw.requestedSpecialities = ['FAKE_REQUESTED_SPECIALITY' as JobOfferRequestedSpeciality];

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer requested speciality must be a valid one');
        });
      });

      describe('description', () => {
        it('should bind description from raw data', () => {
          // given
          jobOfferRaw.description = 'A job offer description';

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.description).toBe('A job offer description');
        });

        it('should throw an exception when description is undefined', () => {
          // given
          jobOfferRaw.description = undefined;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer description must be defined');
        });

        it('should throw an exception when description is empty', () => {
          // given
          jobOfferRaw.description = '';

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer description cannot be an empty string');
        });

        it('should throw an exception when description is more than 32000 characters', () => {
          // given
          jobOfferRaw.description = repeat('*', 32001);

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer description must have a maximum of 32000 characters');
        });
      });

      describe('specificities', () => {
        it('should initialize specificities to null when no raw specificities', () => {
          // given
          jobOfferRaw.specificities = undefined;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.specificities).toBeNull();
        });

        it('should bind specificities from raw data', () => {
          // given
          jobOfferRaw.specificities = 'A job offer specificities';

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.specificities).toBe('A job offer specificities');
        });

        it('should throw an exception when specificities is more than 3000 characters', () => {
          // given
          jobOfferRaw.specificities = repeat('*', 3001);

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - Job offer specificities must have a maximum of 3000 characters');
        });
      });

      describe('traveling', () => {
        let travelingRaw: TravelingRawInterface;
        it('should initialize traveling to null when no raw traveling', () => {
          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.traveling).toBeNull();
        });

        it('should bind traveling from raw data', () => {
          // given
          travelingRaw = {} as TravelingRawInterface;
          jobOfferRaw.traveling = {} as TravelingRawInterface;

          const expected: TravelingRawInterface = { ...travelingRaw } as TravelingRawInterface;
          (Traveling as jest.Mock).mockImplementation(() => expected);

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.traveling).toBe(expected);
          expect(Traveling).toHaveBeenCalledWith(travelingRaw);
        });
      });

      describe('advantages', () => {
        let advantagesRaw: AdvantagesRawInterface;

        it('should initialize advantages to null when no raw advantages', () => {
          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.advantages).toBeNull();
        });

        it('should bind advantages from raw data', () => {
          // given
          advantagesRaw = {} as AdvantagesRawInterface;
          jobOfferRaw.advantages = {} as AdvantagesRawInterface;

          const expected: AdvantagesRawInterface = { ...advantagesRaw } as AdvantagesRawInterface;
          (Advantages as jest.Mock).mockImplementation(() => expected);

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.advantages).toBe(expected);
          expect(Advantages).toHaveBeenCalledWith(advantagesRaw);
        });
      });
      describe('annual salary range', () => {
        let annualSalaryRangeRaw: AnnualSalaryRangeRawInterface;

        it('should initialize annual salary range to null when no raw annual salary range', () => {
          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.annualSalaryRange).toBeNull();
        });

        it('should bind annual salary range from raw data', () => {
          // given
          annualSalaryRangeRaw = {} as AnnualSalaryRangeRawInterface;
          jobOfferRaw.annualSalaryRange = {} as AnnualSalaryRangeRawInterface;

          const expected: AnnualSalaryRangeRawInterface = { ...annualSalaryRangeRaw } as AnnualSalaryRangeRawInterface;
          (AnnualSalaryRange as jest.Mock).mockImplementation(() => expected);

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.annualSalaryRange).toBe(expected);
          expect(AnnualSalaryRange).toHaveBeenCalledWith(annualSalaryRangeRaw);
        });
      });
      describe('wanted personalities', () => {
        it('should bind wanted personalities from raw data', () => {
          // given
          jobOfferRaw.wantedPersonalities = [JobOfferWantedPersonality.WANTED_PERSONALITY_FLEXIBLE];

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.wantedPersonalities).toStrictEqual([JobOfferWantedPersonality.WANTED_PERSONALITY_FLEXIBLE]);
        });

        it('should bind wanted personalities from raw data with enum value as a string array', () => {
          // given
          jobOfferRaw.wantedPersonalities = [JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS];

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.wantedPersonalities).toStrictEqual(['WANTED_PERSONALITY_CURIOUS']);
        });
      });
      describe('end date of application', () => {
        it('should initialize end date of application to null when no raw end date of application', () => {
          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.endDateOfApplication).toBeNull();
        });

        it('should bind end date of application from raw data', () => {
          // given
          const futureDate: Date = new Date();
          futureDate.setDate(futureDate.getDate() + 1);
          jobOfferRaw.endDateOfApplication = futureDate;

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.endDateOfApplication).toStrictEqual(futureDate);
        });
        it('should throw an exception when endDateOfApplication is after current date', () => {
          // given
          const pastDate: Date = new Date();
          pastDate.setDate(pastDate.getDate() - 1);
          jobOfferRaw.endDateOfApplication = pastDate;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer end date of application must be after current date');
        });
      });
      describe('start date of employment', () => {
        let startDateOfEmployment: StartDateOfEmploymentRawInterface;

        it('should bind start date of employment from raw data', () => {
          // given
          startDateOfEmployment = { asSoonAsPossible: true } as StartDateOfEmploymentRawInterface;
          jobOfferRaw.startDateOfEmployment = { asSoonAsPossible: true } as StartDateOfEmploymentRawInterface;

          const expected: StartDateOfEmploymentRawInterface = { ...startDateOfEmployment } as StartDateOfEmploymentRawInterface;
          (StartDateOfEmployment as jest.Mock).mockImplementation(() => expected);

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.startDateOfEmployment).toBe(expected);
          expect(StartDateOfEmployment).toHaveBeenCalledWith(startDateOfEmployment);
        });

        it('should throw an exception when start date of employment asSoonAsPossible is false and there is no date', () => {
          // given
          jobOfferRaw.startDateOfEmployment = { asSoonAsPossible: false } as StartDateOfEmploymentRawInterface;

          (StartDateOfEmployment as jest.Mock).mockImplementation(() => {});

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer start date of employment must be defined');
        });

        it('should throw an exception when start date of employment is undefined', () => {
          // given
          jobOfferRaw.startDateOfEmployment = undefined;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer start date of employment must be defined');
        });
      });

      describe('email address for receipt of applications', () => {
        it('should bind email address for receipt of applications from raw data', () => {
          // given
          jobOfferRaw.emailAddressForReceiptOfApplications = 'test@genium360.com';

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.emailAddressForReceiptOfApplications).toBe('test@genium360.com');
        });
        it('should throw an exception when email address for receipt of applications is undefined', () => {
          // given
          jobOfferRaw.emailAddressForReceiptOfApplications = undefined;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer email address for receipt of applications must be defined');
        });
        it('should throw an exception when email address for receipt of applications is empty', () => {
          // given
          jobOfferRaw.emailAddressForReceiptOfApplications = '';

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer email address for receipt of applications must not be empty');
        });
        it('should throw an exception when email address for receipt of applications have not an email format', () => {
          // given
          jobOfferRaw.emailAddressForReceiptOfApplications = 'fakeMailAddress';

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer email address for receipt of applications must have an email format');
        });
        it('should throw an exception when city is more than 100 characters', () => {
          // given
          jobOfferRaw.emailAddressForReceiptOfApplications = `${repeat('*', 25)}@${repeat('*', 25)}.com`;

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow(
            'JobOffer creation error:\n - A job offer email address for receipt of applications must have a maximum of 50 characters'
          );
        });
      });

      describe('internal reference number', () => {
        it('should bind internal reference number from raw data', () => {
          // given
          jobOfferRaw.internalReferenceNumber = 'AB1234-CD';

          // when
          const result: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.internalReferenceNumber).toBe('AB1234-CD');
        });

        it('should throw an exception when internal reference number is more than 20 characters', () => {
          // given
          jobOfferRaw.internalReferenceNumber = repeat('*', 21);

          // when
          const result = () => JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).toThrow('JobOffer creation error:\n - A job offer internal reference number must have a maximum of 20 characters');
        });
      });
    });
    describe('createDraftJobOffer()', () => {
      describe('id', () => {
        it('should initialize id to null', () => {
          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.id).toBeNull();
        });
      });

      describe('userId', () => {
        it('should bind id from user', () => {
          // given
          user = { id: '123456789' } as UserInterface;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.userId).toBe('123456789');
        });

        it('should not throw any exception when there is no user', () => {
          // given
          user = undefined as UserInterface;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when user id is undefined', () => {
          // given
          user = { id: undefined } as UserInterface;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when user id is empty', () => {
          // given
          user = { id: '' } as UserInterface;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('companyId', () => {
        it('should bind id from company', () => {
          // given
          company = { id: '987654321' } as CompanyInterface;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.companyId).toBe('987654321');
        });

        it('should not throw any exception when there is no company', () => {
          // given
          company = undefined as CompanyInterface;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when company id is undefined', () => {
          // given
          company = { id: undefined } as CompanyInterface;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when company id is empty', () => {
          // given
          company = { id: '' } as CompanyInterface;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('status', () => {
        it('should initialize status to draft when draft is true', () => {
          // given
          jobOfferRaw.draft = true;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.status).toBe(JobOfferStatus.STATUS_DRAFT);
        });

        it('should initialize status to pending validation when draft is false', () => {
          // given
          jobOfferRaw.draft = false;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.status).toBe(JobOfferStatus.STATUS_PENDING_VALIDATION);
        });

        it('should initialize status to pending validation when draft is undefined', () => {
          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.status).toBe(JobOfferStatus.STATUS_PENDING_VALIDATION);
        });
      });

      describe('title', () => {
        it('should bind title from raw data', () => {
          // given
          jobOfferRaw.title = 'A job title';

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.title).toBe('A job title');
        });

        it('should not throw any exception when title is undefined', () => {
          // given
          jobOfferRaw.title = undefined;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when title is empty', () => {
          // given
          jobOfferRaw.title = '';

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when title is more than 150 characters', () => {
          // given
          jobOfferRaw.title = repeat('*', 151);

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('jobStatus', () => {
        it('should bind job status from raw data', () => {
          // given
          jobOfferRaw.jobStatus = JobOfferJobStatus.JOB_STATUS_FULL_TIME;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.jobStatus).toBe(JobOfferJobStatus.JOB_STATUS_FULL_TIME);
        });

        it('should bind job status from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.jobStatus = JobOfferJobStatus.JOB_STATUS_PART_TIME;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.jobStatus).toStrictEqual('JOB_STATUS_PART_TIME');
        });

        it('should not throw any exception when job status is undefined', () => {
          // given
          jobOfferRaw.jobStatus = undefined;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when job status is invalid', () => {
          // given
          jobOfferRaw.jobStatus = 'FAKE_JOB_STATUS' as JobOfferJobStatus;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('jobType', () => {
        it('should bind job type from raw data', () => {
          // given
          jobOfferRaw.jobType = JobOfferJobType.JOB_TYPE_REGULAR;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.jobType).toBe(JobOfferJobType.JOB_TYPE_REGULAR);
        });

        it('should bind job type from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.jobType = JobOfferJobType.JOB_TYPE_TEMPORARY;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.jobType).toStrictEqual('JOB_TYPE_TEMPORARY');
        });

        it('should not throw any exception when job type is undefined', () => {
          // given
          jobOfferRaw.jobType = undefined;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when job type is invalid', () => {
          // given
          jobOfferRaw.jobType = 'FAKE_JOB_TYPE' as JobOfferJobType;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('city', () => {
        it('should bind city from raw data', () => {
          // given
          jobOfferRaw.city = 'Montréal';

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.city).toBe('Montréal');
        });

        it('should not throw any exception when city is undefined', () => {
          // given
          jobOfferRaw.city = undefined;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when city is empty', () => {
          // given
          jobOfferRaw.city = '';

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when city is more than 100 characters', () => {
          // given
          jobOfferRaw.city = repeat('*', 101);

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('region', () => {
        it('should bind region from raw data', () => {
          // given
          jobOfferRaw.region = JobOfferRegion.REGION_MONTREAL;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.region).toBe(JobOfferRegion.REGION_MONTREAL);
        });

        it('should bind region from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.region = JobOfferRegion.REGION_CAPITALE_NATIONALE;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.region).toStrictEqual('REGION_CAPITALE_NATIONALE');
        });

        it('should not throw any exception when region is undefined', () => {
          // given
          jobOfferRaw.region = undefined;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when region is invalid', () => {
          // given
          jobOfferRaw.region = 'FAKE_REGION' as JobOfferRegion;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('targetCustomers', () => {
        it('should bind target customers from raw data', () => {
          // given
          jobOfferRaw.targetCustomers = [JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS];

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.targetCustomers).toStrictEqual([JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS]);
        });

        it('should bind target customers from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.targetCustomers = [JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP];

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.targetCustomers).toStrictEqual(['TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP']);
        });

        it('should not throw any exception when job offer target customers is undefined', () => {
          // given
          jobOfferRaw.targetCustomers = undefined;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when job offer target customers is empty', () => {
          // given
          jobOfferRaw.targetCustomers = [];

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when job offer target customer is invalid', () => {
          // given
          jobOfferRaw.targetCustomers = ['FAKE_TARGET_CUSTOMER' as JobOfferTargetCustomer];

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('requiredExperiences', () => {
        it('should bind required experiences from raw data', () => {
          // given
          jobOfferRaw.requiredExperiences = [JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS];

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.requiredExperiences).toStrictEqual([JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS]);
        });

        it('should bind required experiences from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.requiredExperiences = [JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS];

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.requiredExperiences).toStrictEqual(['REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS']);
        });

        it('should not throw any exception when job offer required experiences is undefined', () => {
          // given
          jobOfferRaw.requiredExperiences = undefined;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when job offer required experiences is empty', () => {
          // given
          jobOfferRaw.requiredExperiences = [];

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when job offer required experience is invalid', () => {
          // given
          jobOfferRaw.requiredExperiences = ['FAKE_REQUIRED_EXPERIENCE' as JobOfferRequiredExperience];

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('positionType', () => {
        it('should bind position type from raw data', () => {
          // given
          jobOfferRaw.positionType = JobOfferPositionType.POSITION_TYPE_ANALYST;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.positionType).toBe(JobOfferPositionType.POSITION_TYPE_ANALYST);
        });

        it('should bind position type from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.positionType = JobOfferPositionType.POSITION_TYPE_DESIGNER;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.positionType).toStrictEqual('POSITION_TYPE_DESIGNER');
        });

        it('should not throw any exception when position type is undefined', () => {
          // given
          jobOfferRaw.positionType = undefined;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when position type is invalid', () => {
          // given
          jobOfferRaw.positionType = 'FAKE_POSITION_TYPE' as JobOfferPositionType;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('requestedSpecialities', () => {
        it('should bind requested specialities from raw data', () => {
          // given
          jobOfferRaw.requestedSpecialities = [JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL];

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.requestedSpecialities).toStrictEqual([JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL]);
        });

        it('should bind requested specialities from raw data with enum value as a string', () => {
          // given
          jobOfferRaw.requestedSpecialities = [JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION];

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.requestedSpecialities).toStrictEqual(['REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION']);
        });

        it('should not throw any exception when job offer requested specialities is undefined', () => {
          // given
          jobOfferRaw.requestedSpecialities = undefined;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when job offer requested specialities is empty', () => {
          // given
          jobOfferRaw.requestedSpecialities = [];

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when job offer required experience is invalid', () => {
          // given
          jobOfferRaw.requestedSpecialities = ['FAKE_REQUESTED_SPECIALITY' as JobOfferRequestedSpeciality];

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('description', () => {
        it('should bind description from raw data', () => {
          // given
          jobOfferRaw.description = 'A job offer description';

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.description).toBe('A job offer description');
        });

        it('should not throw any exception when description is undefined', () => {
          // given
          jobOfferRaw.description = undefined;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when description is empty', () => {
          // given
          jobOfferRaw.description = '';

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when description is more than 32000 characters', () => {
          // given
          jobOfferRaw.description = repeat('*', 32001);

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('specificities', () => {
        it('should initialize specificities to null when no raw specificities', () => {
          // given
          jobOfferRaw.specificities = undefined;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.specificities).toBeNull();
        });

        it('should bind specificities from raw data', () => {
          // given
          jobOfferRaw.specificities = 'A job offer specificities';

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.specificities).toBe('A job offer specificities');
        });

        it('should not throw any exception when specificities is more than 3000 characters', () => {
          // given
          jobOfferRaw.specificities = repeat('*', 3001);

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('traveling', () => {
        let travelingRaw: TravelingRawInterface;
        it('should initialize traveling to null when no raw traveling', () => {
          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.traveling).toBeNull();
        });

        it('should bind traveling from raw data', () => {
          // given
          travelingRaw = {} as TravelingRawInterface;
          jobOfferRaw.traveling = {} as TravelingRawInterface;

          const expected: TravelingRawInterface = { ...travelingRaw } as TravelingRawInterface;
          (Traveling as jest.Mock).mockImplementation(() => expected);

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.traveling).toBe(expected);
          expect(Traveling).toHaveBeenCalledWith(travelingRaw);
        });
      });

      describe('advantages', () => {
        let advantagesRaw: AdvantagesRawInterface;

        it('should initialize advantages to null when no raw advantages', () => {
          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.advantages).toBeNull();
        });

        it('should bind advantages from raw data', () => {
          // given
          advantagesRaw = {} as AdvantagesRawInterface;
          jobOfferRaw.advantages = {} as AdvantagesRawInterface;

          const expected: AdvantagesRawInterface = { ...advantagesRaw } as AdvantagesRawInterface;
          (Advantages as jest.Mock).mockImplementation(() => expected);

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.advantages).toBe(expected);
          expect(Advantages).toHaveBeenCalledWith(advantagesRaw);
        });
      });
      describe('annual salary range', () => {
        let annualSalaryRangeRaw: AnnualSalaryRangeRawInterface;

        it('should initialize annual salary range to null when no raw annual salary range', () => {
          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.annualSalaryRange).toBeNull();
        });

        it('should bind annual salary range from raw data', () => {
          // given
          annualSalaryRangeRaw = {} as AnnualSalaryRangeRawInterface;
          jobOfferRaw.annualSalaryRange = {} as AnnualSalaryRangeRawInterface;

          const expected: AnnualSalaryRangeRawInterface = { ...annualSalaryRangeRaw } as AnnualSalaryRangeRawInterface;
          (AnnualSalaryRange as jest.Mock).mockImplementation(() => expected);

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.annualSalaryRange).toBe(expected);
          expect(AnnualSalaryRange).toHaveBeenCalledWith(annualSalaryRangeRaw);
        });
      });
      describe('wanted personalities', () => {
        it('should bind wanted personalities from raw data', () => {
          // given
          jobOfferRaw.wantedPersonalities = [JobOfferWantedPersonality.WANTED_PERSONALITY_FLEXIBLE];

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.wantedPersonalities).toStrictEqual([JobOfferWantedPersonality.WANTED_PERSONALITY_FLEXIBLE]);
        });

        it('should bind wanted personalities from raw data with enum value as a string array', () => {
          // given
          jobOfferRaw.wantedPersonalities = [JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS];

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.wantedPersonalities).toStrictEqual(['WANTED_PERSONALITY_CURIOUS']);
        });
      });
      describe('end date of application', () => {
        it('should initialize end date of application to null when no raw end date of application', () => {
          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.endDateOfApplication).toBeNull();
        });

        it('should bind end date of application from raw data', () => {
          // given
          const futureDate: Date = new Date();
          futureDate.setDate(futureDate.getDate() + 1);
          jobOfferRaw.endDateOfApplication = futureDate;

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.endDateOfApplication).toStrictEqual(futureDate);
        });
        it('should not throw any exception when endDateOfApplication is after current date', () => {
          // given
          const pastDate: Date = new Date();
          pastDate.setDate(pastDate.getDate() - 1);
          jobOfferRaw.endDateOfApplication = pastDate;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });
      describe('start date of employment', () => {
        let startDateOfEmployment: StartDateOfEmploymentRawInterface;

        it('should bind start date of employment from raw data', () => {
          // given
          startDateOfEmployment = { asSoonAsPossible: true } as StartDateOfEmploymentRawInterface;
          jobOfferRaw.startDateOfEmployment = { asSoonAsPossible: true } as StartDateOfEmploymentRawInterface;

          const expected: StartDateOfEmploymentRawInterface = { ...startDateOfEmployment } as StartDateOfEmploymentRawInterface;
          (StartDateOfEmployment as jest.Mock).mockImplementation(() => expected);

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.startDateOfEmployment).toBe(expected);
          expect(StartDateOfEmployment).toHaveBeenCalledWith(startDateOfEmployment);
        });

        it('should set start date of employment to null when as soon as possible is false and there is no start date', () => {
          // given
          jobOfferRaw.startDateOfEmployment = { asSoonAsPossible: false } as StartDateOfEmploymentRawInterface;

          (StartDateOfEmployment as jest.Mock).mockImplementation(() => {});

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.startDateOfEmployment).toBeNull();
          expect(StartDateOfEmployment).not.toHaveBeenCalled();
        });

        it('should not throw any exception when start date of employment is undefined', () => {
          // given
          jobOfferRaw.startDateOfEmployment = undefined;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('email address for receipt of applications', () => {
        it('should bind email address for receipt of applications from raw data', () => {
          // given
          jobOfferRaw.emailAddressForReceiptOfApplications = 'test@genium360.com';

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.emailAddressForReceiptOfApplications).toBe('test@genium360.com');
        });
        it('should not throw any exception when email address for receipt of applications is undefined', () => {
          // given
          jobOfferRaw.emailAddressForReceiptOfApplications = undefined;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
        it('should not throw any exception when email address for receipt of applications is empty', () => {
          // given
          jobOfferRaw.emailAddressForReceiptOfApplications = '';

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
        it('should not throw any exception when email address for receipt of applications have not an email format', () => {
          // given
          jobOfferRaw.emailAddressForReceiptOfApplications = 'fakeMailAddress';

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
        it('should not throw any exception when city is more than 100 characters', () => {
          // given
          jobOfferRaw.emailAddressForReceiptOfApplications = `${repeat('*', 25)}@${repeat('*', 25)}.com`;

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('internal reference number', () => {
        it('should bind internal reference number from raw data', () => {
          // given
          jobOfferRaw.internalReferenceNumber = 'AB1234-CD';

          // when
          const result: JobOffer = JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result.internalReferenceNumber).toBe('AB1234-CD');
        });

        it('should not throw any exception when internal reference number is more than 20 characters', () => {
          // given
          jobOfferRaw.internalReferenceNumber = repeat('*', 21);

          // when
          const result = () => JobOffer.factory.createDraftJobOffer(jobOfferRaw, user, company);

          // then
          expect(result).not.toThrow();
        });
      });
    });

    describe('copy()', () => {
      it('should create a new instance as a copy of given job offer', () => {
        // given
        const expected: JobOfferInterface = {
          id: '123456789',
          userId: '987654321',
          companyId: '879135135687',
          status: JobOfferStatus.STATUS_PENDING_VALIDATION,
          jobStatus: JobOfferJobStatus.JOB_STATUS_FULL_TIME,
          jobType: JobOfferJobType.JOB_TYPE_REGULAR,
          title: 'a job title',
          city: 'a city',
          region: JobOfferRegion.REGION_GASPESIE_ILES_DE_LA_MADELEINE,
          targetCustomers: [
            JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS,
            JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP,
            JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERING_GRADUATES,
            JobOfferTargetCustomer.TARGET_CUSTOMER_STUDENTS_AND_GRADUATES,
          ],
          requiredExperiences: [
            JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS,
            JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
            JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
            JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
          ],
          positionType: JobOfferPositionType.POSITION_TYPE_FORESTRY_ENGINEER,
          requestedSpecialities: [
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NONE_IN_PARTICULAR,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OTHER_SPECIALITY,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOTECHNOLOGY,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CHEMISTRY,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CIVIL,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CONSTRUCTION,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_DOCUMENTATION,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ELECTRICAL,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ENVIRONMENT,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_GEOLOGY,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_QUALITY_MANAGEMENT,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INDUSTRIAL_MANUFACTURER,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INFORMATION_TECHNOLOGY,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INSTRUMENTATION,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_SOFTWARE,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MATERIAL_HANDLING_DISTRIBUTION,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MARITIME_NAVAL,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NON_METALLIC_MATERIALS,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MECHANICS,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_METALLURGY_METALS,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MINING,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NUCLEAR_POWER,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PULP_AND_PAPER,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PHYSICAL,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ROBOTICS,
            JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OCCUPATIONAL_HEALTH_AND_SAFETY,
          ],
          description: 'A job description',
          specificities: 'Some specificities',
          traveling: {
            isTraveling: false,
            quebec: TravelingFrequency.TRAVELING_FREQUENT,
            canada: TravelingFrequency.TRAVELING_FREQUENT,
            international: TravelingFrequency.TRAVELING_OCCASIONAL,
          },
          advantages: {
            health: [
              AdvantageHealth.HEALTH_DENTAL_INSURANCE,
              AdvantageHealth.HEALTH_MEDICAL_EXPENSES_INSURANCE,
              AdvantageHealth.HEALTH_SIGHT_INSURANCE,
              AdvantageHealth.HEALTH_LIFE_INSURANCE,
              AdvantageHealth.HEALTH_TRAVEL_INSURANCE,
              AdvantageHealth.HEALTH_SALARY_INSURANCE,
              AdvantageHealth.HEALTH_SHORT_TERM_DISABILITY_INSURANCE,
              AdvantageHealth.HEALTH_LONG_TERM_DISABILITY_INSURANCE,
            ],
            social: [
              AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES,
              AdvantageSocial.SOCIAL_TRAVEL_AND_HOLIDAYS,
              AdvantageSocial.SOCIAL_CAFETERIA,
              AdvantageSocial.SOCIAL_FRESH_FRUIT_AND_VEGETABLES,
              AdvantageSocial.SOCIAL_RELAXATION_ROOM,
              AdvantageSocial.SOCIAL_WELLNESS_PROGRAM,
              AdvantageSocial.SOCIAL_REFERENCING_PROGRAM,
            ],
            lifeBalance: [
              AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK,
              AdvantageLifeBalance.LIFE_BALANCE_PAID_HOLIDAYS,
              AdvantageLifeBalance.LIFE_BALANCE_FLEXIBLE_SCHEDULES,
              AdvantageLifeBalance.LIFE_BALANCE_SICK_LEAVE,
              AdvantageLifeBalance.LIFE_BALANCE_SUMMER_HOURS,
              AdvantageLifeBalance.LIFE_BALANCE_MOBILE_HOLIDAYS,
            ],
            financial: [
              AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT,
              AdvantageFinancial.FINANCIAL_COMPETITIVE_COMPENSATION,
              AdvantageFinancial.FINANCIAL_PENSION_PLAN,
              AdvantageFinancial.FINANCIAL_DEFERRED_PROFIT_SHARING_PLAN_DPSP,
              AdvantageFinancial.FINANCIAL_EAP,
              AdvantageFinancial.FINANCIAL_RECOGNITION_OF_SENIORITY,
              AdvantageFinancial.FINANCIAL_REFUND_OF_DUES_TO_A_PROFESSIONAL_ASSOCIATION_OR_ORDER,
              AdvantageFinancial.FINANCIAL_PERFORMANCE_BONUS,
              AdvantageFinancial.FINANCIAL_PARKING_LOT,
              AdvantageFinancial.FINANCIAL_TRANSPORT_SUBSCRIPTION,
            ],
            professional: [
              AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM,
              AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM,
            ],
          },
          annualSalaryRange: { minimumSalary: 10000, maximumSalary: 999999 },
          wantedPersonalities: [
            JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS,
            JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS,
            JobOfferWantedPersonality.WANTED_PERSONALITY_AUTONOMOUS,
            JobOfferWantedPersonality.WANTED_PERSONALITY_INNOVATIVE,
            JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS,
            JobOfferWantedPersonality.WANTED_PERSONALITY_DECISION_MAKERS,
            JobOfferWantedPersonality.WANTED_PERSONALITY_STRATEGISTS,
            JobOfferWantedPersonality.WANTED_PERSONALITY_FLEXIBLE,
            JobOfferWantedPersonality.WANTED_PERSONALITY_LEADERS,
            JobOfferWantedPersonality.WANTED_PERSONALITY_NEGOTIATORS,
            JobOfferWantedPersonality.WANTED_PERSONALITY_ORGANIZED,
            JobOfferWantedPersonality.WANTED_PERSONALITY_RIGOROUS,
            JobOfferWantedPersonality.WANTED_PERSONALITY_RESOURCEFUL,
            JobOfferWantedPersonality.WANTED_PERSONALITY_TEAMWORK,
            JobOfferWantedPersonality.WANTED_PERSONALITY_SPEAKERS_AT_THE_CONFERENCE,
            JobOfferWantedPersonality.WANTED_PERSONALITY_PONDERATOR,
            JobOfferWantedPersonality.WANTED_PERSONALITY_DIPLOMATS,
          ],
          endDateOfApplication: new Date('2020-10-15T00:00:00'),
          startDateOfEmployment: { asSoonAsPossible: true, startDate: new Date('2021-01-01T00:00:00') },
          emailAddressForReceiptOfApplications: 'test@example.org',
          internalReferenceNumber: 'ABC123XYZ',
          publishedAt: new Date('2019-03-28T00:00:00'),
        };

        // when
        const result: JobOffer = JobOffer.factory.copy(expected);

        // then
        expect(result).toMatchObject(expected);
        expect(result.archive).toBeDefined();
      });

      it('should create a new instance as a copy of given job offer when no published at date because of draft status', () => {
        // given
        const expected: JobOfferInterface = {
          id: '123456789',
          userId: '987654321',
          status: JobOfferStatus.STATUS_DRAFT,
          jobStatus: JobOfferJobStatus.JOB_STATUS_FULL_TIME,
          jobType: JobOfferJobType.JOB_TYPE_REGULAR,
          title: 'a job title',
          city: 'a city',
          region: JobOfferRegion.REGION_GASPESIE_ILES_DE_LA_MADELEINE,
          targetCustomers: [JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS],
          requiredExperiences: [JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS],
          positionType: JobOfferPositionType.POSITION_TYPE_FORESTRY_ENGINEER,
          requestedSpecialities: [JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE],
          description: 'A job description',
          specificities: 'Some specificities',
          traveling: {
            isTraveling: false,
            quebec: TravelingFrequency.TRAVELING_FREQUENT,
            canada: TravelingFrequency.TRAVELING_FREQUENT,
            international: TravelingFrequency.TRAVELING_OCCASIONAL,
          },
          advantages: {
            health: [AdvantageHealth.HEALTH_DENTAL_INSURANCE],
            social: [AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES],
            lifeBalance: [AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK],
            financial: [AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT],
            professional: [AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM],
          },
          annualSalaryRange: { minimumSalary: 10000, maximumSalary: 999999 },
          wantedPersonalities: [JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS],
          endDateOfApplication: new Date('2020-10-15T00:00:00'),
          startDateOfEmployment: { asSoonAsPossible: true, startDate: new Date('2021-01-01T00:00:00') },
          emailAddressForReceiptOfApplications: 'test@example.org',
          internalReferenceNumber: 'ABC123XYZ',
        } as JobOfferInterface;

        // when
        const result: JobOffer = JobOffer.factory.copy(expected);

        // then
        expect(result).toMatchObject(expected);
        expect(result.archive).toBeDefined();
      });

      it('should not keep any reference to the given job offer', () => {
        // given
        const initialValue: JobOfferInterface = {
          city: '',
          description: '',
          emailAddressForReceiptOfApplications: '',
          jobStatus: undefined,
          jobType: undefined,
          positionType: undefined,
          publishedAt: undefined,
          region: undefined,
          requestedSpecialities: [],
          requiredExperiences: [],
          startDateOfEmployment: undefined,
          status: undefined,
          title: '',
          userId: undefined,
          companyId: undefined,
          id: '123456789',
          targetCustomers: [JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS],
        };
        const copy: JobOffer = JobOffer.factory.copy(initialValue);

        // when
        initialValue.targetCustomers.push(JobOfferTargetCustomer.TARGET_CUSTOMER_STUDENTS_AND_GRADUATES);

        // then
        expect(copy.targetCustomers).not.toStrictEqual(initialValue.targetCustomers);
      });
    });
  });

  describe('archive()', () => {
    it('should set status to ARCHIVED', () => {
      // given
      const jobOffer: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

      // when
      jobOffer.archive();

      // then
      expect(jobOffer.status).toBe(JobOfferStatus.STATUS_ARCHIVED);
    });
  });

  describe('updateWith()', () => {
    it('should update job offer with given raw', () => {
      // given
      const existingJobOffer: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);
      const newJobOfferRaw: JobOfferRawInterface = {
        title: 'new title',
        jobStatus: JobOfferJobStatus.JOB_STATUS_FULL_TIME,
        jobType: JobOfferJobType.JOB_TYPE_TEMPORARY,
        city: 'new city',
        region: JobOfferRegion.REGION_CAPITALE_NATIONALE,
        targetCustomers: [JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP],
        requiredExperiences: [JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS],
        positionType: JobOfferPositionType.POSITION_TYPE_DIRECTOR_OF_OPERATIONS,
        requestedSpecialities: [JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MARITIME_NAVAL],
        description: 'new description',
        emailAddressForReceiptOfApplications: 'new-test@genium360.com',
        startDateOfEmployment: { asSoonAsPossible: true } as StartDateOfEmploymentRawInterface,
      };
      (StartDateOfEmployment as jest.Mock).mockImplementation(() => ({
        asSoonAsPossible: true,
      }));

      // when
      existingJobOffer.updateWith(newJobOfferRaw);

      // then
      expect(existingJobOffer).toMatchObject({
        city: 'new city',
        description: 'new description',
        emailAddressForReceiptOfApplications: 'new-test@genium360.com',
        jobStatus: 'JOB_STATUS_FULL_TIME',
        jobType: 'JOB_TYPE_TEMPORARY',
        positionType: 'POSITION_TYPE_DIRECTOR_OF_OPERATIONS',
        region: 'REGION_CAPITALE_NATIONALE',
        requestedSpecialities: ['REQUESTED_SPECIALITY_MARITIME_NAVAL'],
        requiredExperiences: ['REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS'],
        status: 'STATUS_PENDING_VALIDATION',
        targetCustomers: ['TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP'],
        title: 'new title',
        userId: '42',
        startDateOfEmployment: { asSoonAsPossible: true },
      });
    });

    it('should trigger same validation rules as when creating a new instance when job offer is not a draft', () => {
      // given
      const existingJobOffer: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);
      const newJobOfferRaw: JobOfferRawInterface = {
        draft: false,
        title: null,
        jobStatus: null,
        jobType: null,
        city: null,
        region: null,
        targetCustomers: null,
        requiredExperiences: null,
        positionType: null,
        requestedSpecialities: null,
        description: null,
        emailAddressForReceiptOfApplications: null,
        startDateOfEmployment: null,
      };

      // when
      const result = () => existingJobOffer.updateWith(newJobOfferRaw);

      // then
      expect(result).toThrow(
        'JobOffer creation error:\n' +
          ' - A job offer job status must be defined\n' +
          ' - A job offer job type must be defined\n' +
          ' - A job offer title must be defined\n' +
          ' - A job offer city must be defined\n' +
          ' - A job offer region must be defined\n' +
          ' - A job offer target customer must be defined\n' +
          ' - A job offer required experience must be defined\n' +
          ' - A job offer position type must be defined\n' +
          ' - A job offer requested speciality must be defined\n' +
          ' - A job offer description must be defined\n' +
          ' - A job offer start date of employment must be defined\n' +
          ' - A job offer email address for receipt of applications must be defined'
      );
    });

    it('should not trigger any validation when job offer is a draft', () => {
      // given
      const existingJobOffer: JobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);
      const newJobOfferRaw: JobOfferRawInterface = {
        draft: true,
        title: null,
        jobStatus: null,
        jobType: null,
        city: null,
        region: null,
        targetCustomers: null,
        requiredExperiences: null,
        positionType: null,
        requestedSpecialities: null,
        description: null,
        emailAddressForReceiptOfApplications: null,
        startDateOfEmployment: null,
      };

      // when
      const result = () => existingJobOffer.updateWith(newJobOfferRaw);

      // then
      expect(result).not.toThrow();
    });
  });

  describe('isPublished()', () => {
    let jobOffer: JobOffer;
    let fixedDate: Date;
    let fixedDateOneDayBefore: Date;
    let fixedDateOneDayAfter: Date;
    let fixedDateOneHourAfter: Date;
    beforeEach(() => {
      jobOffer = JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, user, company);

      fixedDate = new Date('2019-11-15T15:35:20');
      fixedDateOneDayBefore = new Date('2019-11-14T15:35:20');
      fixedDateOneDayAfter = new Date('2019-11-16T15:35:20');
      fixedDateOneHourAfter = new Date('2019-11-15T16:35:20');
      // @ts-ignore
      jest.spyOn(global, 'Date').mockImplementation(() => fixedDate);
    });

    it('should return false when status is not published', () => {
      // given
      jobOffer.status = JobOfferStatus.STATUS_PENDING_VALIDATION;

      // when
      const result: boolean = jobOffer.isPublished();

      // then
      expect(result).toBe(false);
    });

    it('should return false when no published date', () => {
      // given
      jobOffer.publishedAt = null;

      // when
      const result: boolean = jobOffer.isPublished();

      // then
      expect(result).toBe(false);
    });

    it('should return false when published date is in the future', () => {
      // given
      jobOffer.publishedAt = fixedDateOneDayAfter;

      // when
      const result: boolean = jobOffer.isPublished();

      // then
      expect(result).toBe(false);
    });

    it('should return true when published date is now and status is published', () => {
      // given
      jobOffer.status = JobOfferStatus.STATUS_PUBLISHED;
      jobOffer.publishedAt = fixedDate;

      // when
      const result: boolean = jobOffer.isPublished();

      // then
      expect(result).toBe(true);
    });

    it('should return true when published date is in the past and status is published', () => {
      // given
      jobOffer.status = JobOfferStatus.STATUS_PUBLISHED;
      jobOffer.publishedAt = fixedDateOneDayBefore;

      // when
      const result: boolean = jobOffer.isPublished();

      // then
      expect(result).toBe(true);
    });

    it('should return true when published date is at same day but the time in the future and status is published', () => {
      // given
      jobOffer.status = JobOfferStatus.STATUS_PUBLISHED;
      jobOffer.publishedAt = fixedDateOneHourAfter;

      // when
      const result: boolean = jobOffer.isPublished();

      // then
      expect(result).toBe(true);
    });
  });
});

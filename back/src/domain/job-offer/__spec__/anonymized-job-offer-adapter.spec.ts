import { repeat } from 'lodash';
import { AdvantageFinancial, AdvantageHealth, AdvantageLifeBalance, AdvantageProfessional, AdvantageSocial } from '../../advantages/advantages-enums';
import { TravelingFrequency } from '../../traveling/traveling-enums';
import { AnonymizedJobOfferAdapter } from '../anonymized-job-offer-adapter';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from '../job-offer-enums';
import { JobOfferInterface } from '../job-offer.interface';

describe('domain/job-offer/AnonymizedJobOfferAdapter', () => {
  let anonymousJobOfferAdapter: AnonymizedJobOfferAdapter;
  let jobOfferWithAllPropertiesFilled: JobOfferInterface;

  beforeEach(() => {
    jobOfferWithAllPropertiesFilled = {
      id: '123456789',
      userId: '987654321',
      companyId: '879135135687',
      status: JobOfferStatus.STATUS_PENDING_VALIDATION,
      jobStatus: JobOfferJobStatus.JOB_STATUS_FULL_TIME,
      jobType: JobOfferJobType.JOB_TYPE_REGULAR,
      title: 'a job title',
      city: 'a city',
      region: JobOfferRegion.REGION_GASPESIE_ILES_DE_LA_MADELEINE,
      targetCustomers: [JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS],
      requiredExperiences: [JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS],
      positionType: JobOfferPositionType.POSITION_TYPE_FORESTRY_ENGINEER,
      requestedSpecialities: [JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE],
      description: 'A job description',
      specificities: 'Some specificities',
      traveling: {
        isTraveling: false,
        quebec: TravelingFrequency.TRAVELING_FREQUENT,
        canada: TravelingFrequency.TRAVELING_FREQUENT,
        international: TravelingFrequency.TRAVELING_OCCASIONAL,
      },
      advantages: {
        health: [AdvantageHealth.HEALTH_DENTAL_INSURANCE],
        social: [AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES],
        lifeBalance: [AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK],
        financial: [AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT],
        professional: [AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM],
      },
      annualSalaryRange: { minimumSalary: 10000, maximumSalary: 999999 },
      wantedPersonalities: [JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS],
      endDateOfApplication: new Date('2020-10-15T00:00:00'),
      startDateOfEmployment: { asSoonAsPossible: true, startDate: new Date('2021-01-01T00:00:00') },
      emailAddressForReceiptOfApplications: 'test@example.org',
      internalReferenceNumber: 'ABC123XYZ',
      publishedAt: new Date('2019-03-28T00:00:00'),
    };

    anonymousJobOfferAdapter = new AnonymizedJobOfferAdapter();
  });

  describe('anonymize()', () => {
    it('should remove all fields that needs to be logged in to view them', () => {
      // when
      const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

      // then
      expect(result.userId).toBeUndefined();
      expect(result.companyId).toBeUndefined();
      expect(result.status).toBeUndefined();
      expect(result.positionType).toBeUndefined();
      expect(result.specificities).toBeUndefined();
      expect(result.traveling).toBeUndefined();
      expect(result.advantages).toBeUndefined();
      expect(result.annualSalaryRange).toBeUndefined();
      expect(result.wantedPersonalities).toBeUndefined();
      expect(result.endDateOfApplication).toBeUndefined();
      expect(result.startDateOfEmployment).toBeUndefined();
      expect(result.emailAddressForReceiptOfApplications).toBeUndefined();
      expect(result.internalReferenceNumber).toBeUndefined();
    });

    it('should keep id', () => {
      // when
      const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

      // then
      expect(result.id).toBeDefined();
    });

    it('should keep title', () => {
      // when
      const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

      // then
      expect(result.title).toBeDefined();
    });

    it('should keep publishedAt', () => {
      // when
      const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

      // then
      expect(result.publishedAt).toBeDefined();
    });

    it('should keep jobStatus', () => {
      // when
      const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

      // then
      expect(result.jobStatus).toBeDefined();
    });

    it('should keep jobType', () => {
      // when
      const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

      // then
      expect(result.jobType).toBeDefined();
    });

    it('should keep region', () => {
      // when
      const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

      // then
      expect(result.region).toBeDefined();
    });

    it('should keep target customers', () => {
      // when
      const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

      // then
      expect(result.targetCustomers).toBeDefined();
    });

    it('should keep required experiences', () => {
      // when
      const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

      // then
      expect(result.requiredExperiences).toBeDefined();
    });

    it('should keep city', () => {
      // when
      const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

      // then
      expect(result.city).toBeDefined();
    });

    it('should keep requested specialities', () => {
      // when
      const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

      // then
      expect(result.requestedSpecialities).toBeDefined();
    });

    describe('description', () => {
      it('should keep description', () => {
        // when
        const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

        // then
        expect(result.description).toBeDefined();
      });

      it('should truncate description to 500 characters', () => {
        // when
        jobOfferWithAllPropertiesFilled.description = repeat('*', 600);
        const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

        // then
        expect(result.description).toHaveLength(500);
      });

      it('should truncate description to 500 characters with space separator', () => {
        // when
        jobOfferWithAllPropertiesFilled.description = `${repeat('*', 498)} should be removed`;
        const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

        // then
        expect(result.description).toHaveLength(498);
      });

      it('should not truncate description if shorter than 500', () => {
        // when
        jobOfferWithAllPropertiesFilled.description = repeat('*', 400);
        const result: JobOfferInterface = anonymousJobOfferAdapter.anonymize(jobOfferWithAllPropertiesFilled);

        // then
        expect(result.description).toHaveLength(400);
      });
    });
  });
});

import { AdvantagesInterface } from '../advantages/advantages.interface';
import { AnnualSalaryRangeInterface } from '../annual-salary-range/annual-salary-range.interface';
import { StartDateOfEmploymentInterface } from '../start-date-of-employment/start-date-of-employment.interface';
import { TravelingInterface } from '../traveling/traveling.interface';
import { CompanyId, JobOfferId, UserId } from '../type-aliases';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from './job-offer-enums';

export interface JobOfferInterface {
  id: JobOfferId;
  userId: UserId;
  companyId: CompanyId;
  status: JobOfferStatus;
  jobStatus: JobOfferJobStatus;
  jobType: JobOfferJobType;
  title: string;
  city: string;
  region: JobOfferRegion;
  targetCustomers: JobOfferTargetCustomer[];
  requiredExperiences: JobOfferRequiredExperience[];
  positionType: JobOfferPositionType;
  requestedSpecialities: JobOfferRequestedSpeciality[];
  description: string;
  specificities?: string;
  traveling?: TravelingInterface;
  advantages?: AdvantagesInterface;
  annualSalaryRange?: AnnualSalaryRangeInterface;
  wantedPersonalities?: JobOfferWantedPersonality[];
  endDateOfApplication?: Date;
  startDateOfEmployment: StartDateOfEmploymentInterface;
  emailAddressForReceiptOfApplications: string;
  internalReferenceNumber?: string;
  publishedAt: Date;
}

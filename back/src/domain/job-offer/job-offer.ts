import { ArrayMinSize, IsDefined, IsEmail, IsEnum, IsNotEmpty, MaxLength, MinDate, ValidateNested } from 'class-validator';
import { cloneDeep } from 'lodash';
import { Advantages } from '../advantages/advantages';
import { AnnualSalaryRange } from '../annual-salary-range/annual-salary-range';
import { CompanyInterface } from '../company/company.interface';
import { StartDateOfEmployment } from '../start-date-of-employment/start-date-of-employment';
import { Traveling } from '../traveling/traveling';
import { CompanyId, JobOfferId, UserId } from '../type-aliases';
import { UserInterface } from '../user/user.interface';
import { AbstractDomainWithValidation } from '../validation/abstract-domain-with-validation';
import {
  JOB_OFFER_CITY_MAX_LENGTH,
  JOB_OFFER_DESCRIPTION_MAX_LENGTH,
  JOB_OFFER_EMAIL_ADDRESS_FOR_RECEIPT_OF_APPLICATIONS_MAX_LENGTH,
  JOB_OFFER_END_DATE_OF_APPLICATION_MIN_DATE,
  JOB_OFFER_INTERNAL_REFERENCE_NUMBER_MAX_LENGTH,
  JOB_OFFER_REQUESTED_SPECIALITIES_MIN,
  JOB_OFFER_REQUIRED_EXPERIENCES_MIN,
  JOB_OFFER_SPECIFICITIES_MAX_LENGTH,
  JOB_OFFER_TARGET_CUSTOMERS_MIN,
  JOB_OFFER_TITLE_MAX_LENGTH,
} from './job-offer-constraints';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from './job-offer-enums';
import { JobOfferInterface } from './job-offer.interface';
import { JobOfferRawInterface } from './job-offer.raw.interface';

export class JobOffer extends AbstractDomainWithValidation implements JobOfferInterface {
  static factory: JobOfferFactoryInterface = {
    copy(jobOffer: JobOfferInterface): JobOffer {
      // We have to pass an empty object of user and company parameters to respect constructor signature
      return new JobOffer(jobOffer, {} as UserInterface, {} as CompanyInterface, false, true);
    },
    createDraftJobOffer(jobOffer: JobOfferRawInterface, user: UserInterface, company: CompanyInterface): JobOffer {
      return new JobOffer(jobOffer, user, company);
    },
    validateAndCreateNewJobOffer(jobOffer: JobOfferRawInterface, user: UserInterface, company: CompanyInterface): JobOffer {
      return new JobOffer(jobOffer, user, company, true);
    },
  };

  id: JobOfferId;

  @IsNotEmpty({ message: 'A job offer user id cannot be an empty string' })
  @IsDefined({ message: 'A job offer user id must be defined' })
  userId: UserId;

  @IsNotEmpty({ message: 'A job offer company id cannot be an empty string' })
  @IsDefined({ message: 'A job offer company id must be defined' })
  companyId: CompanyId;

  status: JobOfferStatus;

  @IsEnum(JobOfferJobStatus, {
    message: 'A job offer job status must be a valid one',
  })
  @IsDefined({ message: 'A job offer job status must be defined' })
  jobStatus: JobOfferJobStatus;

  @IsEnum(JobOfferJobType, {
    message: 'A job offer job type must be a valid one',
  })
  @IsDefined({ message: 'A job offer job type must be defined' })
  jobType: JobOfferJobType;

  @MaxLength(JOB_OFFER_TITLE_MAX_LENGTH, {
    message: 'A job offer title must have a maximum of $constraint1 characters',
  })
  @IsNotEmpty({ message: 'A job offer title cannot be an empty string' })
  @IsDefined({ message: 'A job offer title must be defined' })
  title: string;

  @MaxLength(JOB_OFFER_CITY_MAX_LENGTH, {
    message: 'A job offer city must have a maximum of $constraint1 characters',
  })
  @IsNotEmpty({ message: 'A job offer city cannot be an empty string' })
  @IsDefined({ message: 'A job offer city must be defined' })
  city: string;

  @IsEnum(JobOfferRegion, {
    message: 'A job offer region must be a valid one',
  })
  @IsDefined({ message: 'A job offer region must be defined' })
  region: JobOfferRegion;

  @IsEnum(JobOfferTargetCustomer, {
    message: 'A job offer target customer must be a valid one',
    each: true,
  })
  @ArrayMinSize(JOB_OFFER_TARGET_CUSTOMERS_MIN, {
    message: 'A job offer must have at least one target customer',
  })
  @IsDefined({ message: 'A job offer target customer must be defined' })
  targetCustomers: JobOfferTargetCustomer[];

  @IsEnum(JobOfferRequiredExperience, {
    message: 'A job offer required experience must be a valid one',
    each: true,
  })
  @ArrayMinSize(JOB_OFFER_REQUIRED_EXPERIENCES_MIN, {
    message: 'A job offer must have at least one required experience',
  })
  @IsDefined({ message: 'A job offer required experience must be defined' })
  requiredExperiences: JobOfferRequiredExperience[];

  @IsEnum(JobOfferPositionType, {
    message: 'A job offer position type must be a valid one',
  })
  @IsDefined({ message: 'A job offer position type must be defined' })
  positionType: JobOfferPositionType;

  @IsEnum(JobOfferRequestedSpeciality, {
    message: 'A job offer requested speciality must be a valid one',
    each: true,
  })
  @ArrayMinSize(JOB_OFFER_REQUESTED_SPECIALITIES_MIN, {
    message: 'A job offer must have at least one requested speciality',
  })
  @IsDefined({ message: 'A job offer requested speciality must be defined' })
  requestedSpecialities: JobOfferRequestedSpeciality[];

  @MaxLength(JOB_OFFER_DESCRIPTION_MAX_LENGTH, {
    message: 'A job offer description must have a maximum of $constraint1 characters',
  })
  @IsNotEmpty({ message: 'A job offer description cannot be an empty string' })
  @IsDefined({ message: 'A job offer description must be defined' })
  description: string;

  @MaxLength(JOB_OFFER_SPECIFICITIES_MAX_LENGTH, {
    message: 'Job offer specificities must have a maximum of $constraint1 characters',
  })
  specificities?: string;

  traveling?: Traveling;

  advantages?: Advantages;

  @ValidateNested()
  annualSalaryRange?: AnnualSalaryRange;

  wantedPersonalities?: JobOfferWantedPersonality[];

  @MinDate(JOB_OFFER_END_DATE_OF_APPLICATION_MIN_DATE(), {
    message: 'A job offer end date of application must be after current date',
  })
  endDateOfApplication?: Date;

  @ValidateNested()
  @IsDefined({ message: 'A job offer start date of employment must be defined' })
  startDateOfEmployment: StartDateOfEmployment;

  @IsEmail(
    {},
    {
      message: 'A job offer email address for receipt of applications must have an email format',
    }
  )
  @MaxLength(JOB_OFFER_EMAIL_ADDRESS_FOR_RECEIPT_OF_APPLICATIONS_MAX_LENGTH, {
    message: 'A job offer email address for receipt of applications must have a maximum of $constraint1 characters',
  })
  @IsDefined({ message: 'A job offer email address for receipt of applications must be defined' })
  @IsNotEmpty({ message: 'A job offer email address for receipt of applications must not be empty' })
  emailAddressForReceiptOfApplications: string;

  @MaxLength(JOB_OFFER_INTERNAL_REFERENCE_NUMBER_MAX_LENGTH, {
    message: 'A job offer internal reference number must have a maximum of $constraint1 characters',
  })
  internalReferenceNumber?: string;

  publishedAt: Date;

  private constructor(
    jobOffer: JobOfferRawInterface | JobOfferInterface,
    user: UserInterface = {} as UserInterface,
    company: CompanyInterface = {} as CompanyInterface,
    shouldValidate?: boolean,
    shouldCopy?: boolean
  ) {
    super();
    if (shouldCopy) {
      this.copy(jobOffer as JobOfferInterface);
    } else {
      this.id = null;
      this.userId = user.id;
      this.companyId = company.id;
      this.bindRaw(jobOffer as JobOfferRawInterface);
    }

    if (shouldValidate) {
      this.validate('JobOffer');
    }
  }

  archive(): void {
    this.status = JobOfferStatus.STATUS_ARCHIVED;
  }

  updateWith(jobOfferRaw: JobOfferRawInterface): void {
    this.bindRaw(jobOfferRaw);
    if (!jobOfferRaw.draft) {
      this.validate('JobOffer');
    }
  }

  isPublished(): boolean {
    if (this.status === JobOfferStatus.STATUS_PUBLISHED && this.publishedAt) {
      const now: Date = new Date();
      const iso8601DateWithoutTimeLength: number = 10;

      const publishedAtDateWithoutTime: string = this.publishedAt.toISOString().substring(0, iso8601DateWithoutTimeLength);
      const nowDateWithoutTime: string = now.toISOString().substring(0, iso8601DateWithoutTimeLength);

      return publishedAtDateWithoutTime <= nowDateWithoutTime;
    }

    return false;
  }

  private copy(otherJobOffer: JobOfferInterface): void {
    const deepCloneOfOtherJobOffer: JobOfferInterface = cloneDeep(otherJobOffer);
    for (const field in deepCloneOfOtherJobOffer) {
      if (deepCloneOfOtherJobOffer.hasOwnProperty(field)) {
        this[field] = deepCloneOfOtherJobOffer[field];
      }
    }
  }

  private bindRaw(jobOfferRaw: JobOfferRawInterface): void {
    this.status = jobOfferRaw.draft ? JobOfferStatus.STATUS_DRAFT : JobOfferStatus.STATUS_PENDING_VALIDATION;
    this.title = jobOfferRaw.title;
    this.jobStatus = jobOfferRaw.jobStatus;
    this.jobType = jobOfferRaw.jobType;
    this.city = jobOfferRaw.city;
    this.region = jobOfferRaw.region;
    this.targetCustomers = jobOfferRaw.targetCustomers;
    this.requiredExperiences = jobOfferRaw.requiredExperiences;
    this.positionType = jobOfferRaw.positionType;
    this.requestedSpecialities = jobOfferRaw.requestedSpecialities;
    this.description = jobOfferRaw.description;
    this.specificities = jobOfferRaw.specificities || null;
    this.traveling = jobOfferRaw.traveling ? new Traveling(jobOfferRaw.traveling) : null;
    this.advantages = jobOfferRaw.advantages ? new Advantages(jobOfferRaw.advantages) : null;
    this.annualSalaryRange = jobOfferRaw.annualSalaryRange ? new AnnualSalaryRange(jobOfferRaw.annualSalaryRange) : null;
    this.wantedPersonalities = jobOfferRaw.wantedPersonalities;
    this.endDateOfApplication = jobOfferRaw.endDateOfApplication ? new Date(jobOfferRaw.endDateOfApplication) : null;
    this.startDateOfEmployment =
      jobOfferRaw.startDateOfEmployment && (jobOfferRaw.startDateOfEmployment.asSoonAsPossible || jobOfferRaw.startDateOfEmployment.startDate)
        ? new StartDateOfEmployment(jobOfferRaw.startDateOfEmployment)
        : null;
    this.emailAddressForReceiptOfApplications = jobOfferRaw.emailAddressForReceiptOfApplications;
    this.internalReferenceNumber = jobOfferRaw.internalReferenceNumber;
  }
}

export interface JobOfferFactoryInterface {
  copy(jobOffer: JobOfferInterface): JobOffer;
  createDraftJobOffer(jobOffer: JobOfferRawInterface, user: UserInterface, company: CompanyInterface): JobOffer;
  validateAndCreateNewJobOffer(jobOffer: JobOfferRawInterface, user: UserInterface, company: CompanyInterface): JobOffer;
}

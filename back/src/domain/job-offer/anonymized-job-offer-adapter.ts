import { cloneDeep, truncate } from 'lodash';
import { JobOfferInterface } from './job-offer.interface';

type JobOfferKey = keyof JobOfferInterface;

export class AnonymizedJobOfferAdapter {
  private readonly DESCRIPTION_CHARACTERS_MAX_LENGTH: number = 500;
  private readonly FIELDS_ALLOWED_FOR_AN_ANONYMOUS: JobOfferKey[] = [
    'id',
    'title',
    'jobStatus',
    'jobType',
    'publishedAt',
    'region',
    'targetCustomers',
    'requiredExperiences',
    'city',
    'requestedSpecialities',
    'description',
  ];

  anonymize(jobOffer: JobOfferInterface): JobOfferInterface {
    const jobOfferKeys: JobOfferKey[] = Object.keys(jobOffer) as JobOfferKey[];
    const anonymizedJobOffer: JobOfferInterface = cloneDeep(jobOffer);

    jobOfferKeys.forEach((jobOfferKey: JobOfferKey) => {
      if (!this.FIELDS_ALLOWED_FOR_AN_ANONYMOUS.includes(jobOfferKey)) {
        delete anonymizedJobOffer[jobOfferKey];
      }
    });

    anonymizedJobOffer.description = truncate(anonymizedJobOffer.description, {
      length: this.DESCRIPTION_CHARACTERS_MAX_LENGTH,
      separator: ' ',
      omission: '',
    });

    return anonymizedJobOffer;
  }
}

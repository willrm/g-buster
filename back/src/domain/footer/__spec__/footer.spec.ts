import { Footer } from '../footer';
import { FooterRawInterface } from '../footer.raw.interface';

describe('domain/footer/Footer', () => {
  describe('constructor()', () => {
    let footerRaw: FooterRawInterface;
    beforeEach(() => {
      footerRaw = { copyright: '' } as FooterRawInterface;
    });

    it('should bind copyright from raw', () => {
      // given
      footerRaw.copyright = 'A footer copyright';

      // when
      const result: Footer = new Footer(footerRaw);

      // then
      expect(result.copyright).toBe('A footer copyright');
    });
  });
});

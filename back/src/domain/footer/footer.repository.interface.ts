import { Lang } from '../type-aliases';
import { FooterInterface } from './footer.interface';

export interface FooterRepository {
  findByLang(lang: Lang): Promise<FooterInterface>;
}

import { FooterInterface } from './footer.interface';
import { FooterRawInterface } from './footer.raw.interface';

export class Footer implements FooterInterface {
  copyright: string;

  constructor(footerRaw: FooterRawInterface) {
    this.copyright = footerRaw.copyright;
  }
}

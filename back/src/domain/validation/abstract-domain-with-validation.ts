import { ValidationError, Validator } from 'class-validator';
import { isEmpty } from 'lodash';
import { DomainValidationError } from './domain-validation.error';

export abstract class AbstractDomainWithValidation {
  validate(className: string): void {
    const validator: Validator = new Validator();
    const errors: ValidationError[] = validator.validateSync(className, this, {
      skipMissingProperties: true,
    });

    if (!isEmpty(errors)) {
      throw new DomainValidationError(className, 'creation error', errors);
    }
  }
}

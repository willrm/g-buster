import { ValidationError } from 'class-validator';
import { DomainFieldErrorsInterface } from '../domain-field-errors.interface';
import { DomainValidationError } from '../domain-validation.error';

describe('domain/validation/DomainValidationError', () => {
  describe('constructor()', () => {
    let validationErrors: ValidationError[];
    beforeEach(() => {
      validationErrors = [
        {
          target: {},
          property: 'aProperty',
          children: [],
          constraints: { isNotValid: 'A property is not valid' },
        },
      ];
    });

    it('should bind error instance name', () => {
      // given
      const expected: string = 'TestClass';

      // when
      const result: DomainValidationError = new DomainValidationError('TestClass', 'creation error', []);

      // then
      expect(result.errorInstanceName).toBe(expected);
    });
    it('should bind error type', () => {
      // given
      const expected: string = 'creation error';

      // when
      const result: DomainValidationError = new DomainValidationError('TestClass', 'creation error', []);

      // then
      expect(result.errorType).toBe(expected);
    });
    describe('extractFieldAndI18nMessageKey()', () => {
      it('should initialize fields and i18n message keys', () => {
        // given
        const expected: { [fieldName: string]: DomainFieldErrorsInterface } = {
          aProperty: {
            i18nMessageKeys: ['testClass.aPropertyIsNotValidError'],
            messages: ['A property is not valid'],
          },
        };

        // when
        const result: DomainValidationError = new DomainValidationError('TestClass', 'creation error', validationErrors);

        // then
        expect(result.domainErrors).toStrictEqual(expected);
      });
      it('should initialize fields and i18n message keys for children', () => {
        // given
        validationErrors[0].children = [
          { target: {}, property: 'aChildProperty', children: [], constraints: { isNotValid: 'A child property is not valid' } },
        ];
        const expected: { [fieldName: string]: DomainFieldErrorsInterface } = {
          aProperty: {
            i18nMessageKeys: ['testClass.aPropertyIsNotValidError'],
            messages: ['A property is not valid'],
          },
          aChildProperty: {
            i18nMessageKeys: ['testClass.aChildPropertyIsNotValidError'],
            messages: ['A child property is not valid'],
          },
        };

        // when
        const result: DomainValidationError = new DomainValidationError('TestClass', 'creation error', validationErrors);

        // then
        expect(result.domainErrors).toStrictEqual(expected);
      });
      it('should not return fields and i18n message keys if there is no constraint message', () => {
        // given
        validationErrors = [{ target: {}, property: 'aChildProperty', children: [], constraints: {} }];

        // when
        const result: DomainValidationError = new DomainValidationError('TestClass', 'creation error', validationErrors);

        // then
        expect(result.domainErrors).toStrictEqual({});
      });
    });

    describe('serializeConstraints()', () => {
      it('should initialize message', () => {
        // given
        const expected: string = 'TestClass creation error:\n - A property is not valid';

        // when
        const result: DomainValidationError = new DomainValidationError('TestClass', 'creation error', validationErrors);

        // then
        expect(result.message).toStrictEqual(expected);
      });
      it('should initialize message for children', () => {
        // given
        validationErrors[0].children = [
          { target: {}, property: 'aChildProperty', children: [], constraints: { isNotValid: 'A child property is not valid' } },
        ];
        const expected: string = 'TestClass creation error:\n - A property is not valid\n - A child property is not valid';

        // when
        const result: DomainValidationError = new DomainValidationError('TestClass', 'creation error', validationErrors);

        // then
        expect(result.message).toStrictEqual(expected);
      });
      it('should not return message if there is no constraint message', () => {
        // given
        validationErrors = [{ target: {}, property: 'aChildProperty', children: [], constraints: {} }];

        const expected: string = 'TestClass creation error:';

        // when
        const result: DomainValidationError = new DomainValidationError('TestClass', 'creation error', validationErrors);

        // then
        expect(result.message).toStrictEqual(expected);
      });
    });
  });
});

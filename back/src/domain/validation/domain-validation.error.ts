import { ValidationError } from 'class-validator';
import { camelCase, isEmpty, keys, upperFirst } from 'lodash';
import { DomainErrorsInterface } from './domain-errors.interface';
import { DomainFieldErrorsInterface } from './domain-field-errors.interface';

export class DomainValidationError extends Error {
  errorInstanceName: string;
  domainErrors: DomainErrorsInterface;
  errorType: string;

  constructor(errorInstanceName: string, errorType: string, errors: ValidationError[]) {
    super();
    const domainPrefixMessage: string = `${errorInstanceName} ${errorType}`;
    this.name = 'DomainValidationError';
    this.errorInstanceName = errorInstanceName;
    this.errorType = errorType;
    this.domainErrors = DomainValidationError.extractFieldsAndI18nMessageKeys(errorInstanceName, errors);

    this.message = DomainValidationError.serializeConstraints(domainPrefixMessage, this.domainErrors);
  }

  private static serializeConstraints(domainPrefixMessage: string, errors: DomainErrorsInterface): string {
    if (!errors) return null;
    const serializeErrorMessage = (errorMessage: string) => `\n - ${errorMessage}`;

    return keys(errors).reduce(
      (previousErrorMessage: string, errorKey: string) => `${previousErrorMessage}${errors[errorKey].messages.map(serializeErrorMessage)}`,
      `${domainPrefixMessage}:`
    );
  }

  private static extractFieldsAndI18nMessageKeys(
    errorInstanceName: string,
    errors: ValidationError[],
    previousFieldsAndI18nMessageKeys: DomainFieldErrorsInterface[] = []
  ): DomainErrorsInterface {
    const domainErrors: DomainErrorsInterface = {};

    DomainValidationError.recursiveErrorsLooper(errors, previousFieldsAndI18nMessageKeys, (error: ValidationError) => {
      if (isEmpty(error.constraints)) return null;

      const domainErrorI18nMessage: DomainFieldErrorsInterface = {
        i18nMessageKeys: [],
        messages: [],
      };

      keys(error.constraints).forEach((key: string) => {
        domainErrorI18nMessage.i18nMessageKeys.push(`${camelCase(errorInstanceName)}.${error.property}${upperFirst(key)}Error`);
        domainErrorI18nMessage.messages.push(error.constraints[key]);
      });

      domainErrors[error.property] = domainErrorI18nMessage;
    });

    return domainErrors;
  }

  private static recursiveErrorsLooper(
    errors: ValidationError[],
    previousResultArray: DomainFieldErrorsInterface[],
    actionToPerform: (error: ValidationError) => DomainFieldErrorsInterface
  ): DomainFieldErrorsInterface[] {
    errors.forEach((error: ValidationError) => {
      const domainErrorI18nMessage: DomainFieldErrorsInterface = actionToPerform(error);
      if (domainErrorI18nMessage) {
        previousResultArray.push(domainErrorI18nMessage);
      }

      if (error.children) {
        DomainValidationError.recursiveErrorsLooper(error.children, previousResultArray, actionToPerform);
      }
    });

    return previousResultArray;
  }
}

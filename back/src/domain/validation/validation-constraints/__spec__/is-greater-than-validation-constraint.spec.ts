import { ValidationArguments } from 'class-validator';
import { IsGreaterThanValidationConstraint } from '../is-greater-than-validation-constraint';

describe('domain/validation/validation-constraints/is-greater-than-validation-constraint', () => {
  describe('validate()', () => {
    it('should return false if parametrized arguments of object is greater than value', () => {
      // when
      const result: boolean = new IsGreaterThanValidationConstraint().validate(123, ({
        object: { greaterValue: 1234 },
        constraints: ['greaterValue'],
      } as unknown) as ValidationArguments);

      // then
      expect(result).toBe(false);
    });

    it('should return true if parametrized arguments of object is lower than value', () => {
      // when
      const result: boolean = new IsGreaterThanValidationConstraint().validate(1234, ({
        object: { lowerValue: 123 },
        constraints: ['lowerValue'],
      } as unknown) as ValidationArguments);

      // then
      expect(result).toBe(true);
    });
  });
  describe('defaultMessage()', () => {
    it('should return a default message with property name and constraint name in it', () => {
      // given
      const expected: string = `A greater value must be greater than lower value`;

      const result: string = new IsGreaterThanValidationConstraint().defaultMessage(({
        property: 'greater value',
        constraints: ['lower value'],
      } as unknown) as ValidationArguments);

      // then
      expect(result).toBe(expected);
    });
  });
});

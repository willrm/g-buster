import { ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

@ValidatorConstraint({ name: 'isGreaterThan', async: false })
export class IsGreaterThanValidationConstraint implements ValidatorConstraintInterface {
  defaultMessage(validationArguments?: ValidationArguments): string {
    return `A ${validationArguments.property} must be greater than ${validationArguments.constraints[0]}`;
  }

  validate(value: number, validationArguments?: ValidationArguments): boolean {
    return value > validationArguments.object[validationArguments.constraints[0]];
  }
}

import { DomainFieldErrorsInterface } from './domain-field-errors.interface';

export interface DomainErrorsInterface {
  [fieldName: string]: DomainFieldErrorsInterface;
}

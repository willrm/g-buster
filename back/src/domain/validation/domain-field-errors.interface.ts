export interface DomainFieldErrorsInterface {
  i18nMessageKeys: string[];
  messages: string[];
}

import { repeat } from 'lodash';
import { Value } from '../value';
import { ValueRawInterface } from '../value.raw.interface';

describe('domain/value/Value', () => {
  describe('constructor()', () => {
    let valueRaw: ValueRawInterface;

    beforeEach(() => {
      valueRaw = {
        meaning: 'A Value',
        justification: 'A Justification',
      };
    });

    describe('justification', () => {
      it('should bind justification from raw data when exists', () => {
        // given
        valueRaw.justification = 'A justification';

        // when
        const result: Value = new Value(valueRaw);

        // then
        expect(result.justification).toBe('A justification');
      });
      it('should be defined', () => {
        // given
        valueRaw.justification = undefined;

        // when
        const result = () => new Value(valueRaw).validate('Value');

        // then
        expect(result).toThrow('Value creation error:\n - A value justification must be defined');
      });
      it('should not be an empty string', () => {
        // given
        valueRaw.justification = '';

        // when
        const result = () => new Value(valueRaw).validate('Value');

        // then
        expect(result).toThrow('Value creation error:\n - A value justification cannot be an empty string');
      });
      it('should throw an exception when justification is more than 200 characters', () => {
        // given
        valueRaw.justification = repeat('*', 201);

        // when
        const result = () => new Value(valueRaw).validate('Value');

        // then
        expect(result).toThrow('Value creation error:\n - A value justification must have a maximum of 200 characters');
      });
    });

    describe('meaning', () => {
      it('should bind meaning from raw data when exists', () => {
        // given
        valueRaw.meaning = 'A Meaning';

        // when
        const result: Value = new Value(valueRaw);

        // then
        expect(result.meaning).toBe('A Meaning');
      });
      it('should be defined', () => {
        // given
        valueRaw.meaning = undefined;

        // when
        const result = () => new Value(valueRaw).validate('Value');

        // then
        expect(result).toThrow('Value creation error:\n - A value meaning must be defined');
      });
      it('should not be an empty string', () => {
        // given
        valueRaw.meaning = '';

        // when
        const result = () => new Value(valueRaw).validate('Value');

        // then
        expect(result).toThrow('Value creation error:\n - A value meaning cannot be an empty string');
      });
      it('should throw an exception when value name is more than 50 characters', () => {
        // given
        valueRaw.meaning = repeat('*', 51);

        // when
        const result = () => new Value(valueRaw).validate('Value');

        // then
        expect(result).toThrow('Value creation error:\n - A value meaning must have a maximum of 50 characters');
      });
    });
  });
});

export interface ValueInterface {
  meaning: string;
  justification: string;
}

export interface ValueRawInterface {
  meaning: string;
  justification: string;
}

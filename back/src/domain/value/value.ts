import { IsDefined, IsNotEmpty, MaxLength } from 'class-validator';
import { AbstractDomainWithValidation } from '../validation/abstract-domain-with-validation';
import { VALUE_JUSTIFICATION_MAX_LENGTH, VALUE_MEANING_MAX_LENGTH } from './value-constraints';
import { ValueInterface } from './value.interface';
import { ValueRawInterface } from './value.raw.interface';

export class Value extends AbstractDomainWithValidation implements ValueInterface {
  @IsDefined({ message: 'A value justification must be defined' })
  @IsNotEmpty({ message: 'A value justification cannot be an empty string' })
  @MaxLength(VALUE_JUSTIFICATION_MAX_LENGTH, {
    message: 'A value justification must have a maximum of $constraint1 characters',
  })
  justification: string;

  @IsDefined({ message: 'A value meaning must be defined' })
  @IsNotEmpty({ message: 'A value meaning cannot be an empty string' })
  @MaxLength(VALUE_MEANING_MAX_LENGTH, {
    message: 'A value meaning must have a maximum of $constraint1 characters',
  })
  meaning: string;

  constructor(valueRaw: ValueRawInterface) {
    super();

    this.meaning = valueRaw.meaning;
    this.justification = valueRaw.justification;
  }
}

export interface TemporaryFileRawInterface {
  sizeInBytes: number;
  mimeType: string;
  data: Buffer;
  filename: string;
}

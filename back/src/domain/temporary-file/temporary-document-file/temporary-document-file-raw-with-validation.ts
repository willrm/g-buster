import { IsIn, Matches, Max } from 'class-validator';
import { AbstractDomainWithValidation } from '../../validation/abstract-domain-with-validation';
import { TemporaryFileRawInterface } from '../temporary-file.raw.interface';
import {
  TEMPORARY_DOCUMENT_FILE_ACCEPTED_EXTENSIONS,
  TEMPORARY_DOCUMENT_FILE_ACCEPTED_MIME_TYPES,
  TEMPORARY_DOCUMENT_FILE_MAX_SIZE_IN_BYTES,
} from './temporary-document-file-constraints';

export class TemporaryDocumentFileRawWithValidation extends AbstractDomainWithValidation implements TemporaryFileRawInterface {
  data: Buffer;

  @Matches(new RegExp(`\\.(${TEMPORARY_DOCUMENT_FILE_ACCEPTED_EXTENSIONS.join('|')})$`, 'i'), {
    message: `Document file extension should be one of ${TEMPORARY_DOCUMENT_FILE_ACCEPTED_EXTENSIONS}`,
  })
  filename: string;

  @IsIn(TEMPORARY_DOCUMENT_FILE_ACCEPTED_MIME_TYPES, {
    message: 'Document file mime type should be one of $constraint1',
  })
  mimeType: string;

  @Max(TEMPORARY_DOCUMENT_FILE_MAX_SIZE_IN_BYTES, {
    message: 'Document file size should be $constraint1 bytes maximum',
  })
  sizeInBytes: number;
}

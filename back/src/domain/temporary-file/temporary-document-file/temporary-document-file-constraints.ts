export const TEMPORARY_DOCUMENT_FILE_ACCEPTED_MIME_TYPES: string[] = [
  'application/msword',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'application/vnd.oasis.opendocument.text',
  'application/pdf',
  'application/rtf',
];
export const TEMPORARY_DOCUMENT_FILE_ACCEPTED_EXTENSIONS: string[] = ['doc', 'docx', 'odt', 'pdf', 'rtf'];
export const TEMPORARY_DOCUMENT_FILE_MAX_SIZE_IN_BYTES: number = 10000 * 1024;

import { v4 } from 'uuid';
import { TemporaryFileId, Uuid } from '../type-aliases';
import { AbstractDomainWithValidation } from '../validation/abstract-domain-with-validation';
import { TemporaryDocumentFileRawWithValidation } from './temporary-document-file/temporary-document-file-raw-with-validation';
import { TemporaryFileInterface } from './temporary-file.interface';
import { TemporaryFileRawInterface } from './temporary-file.raw.interface';
import { TemporaryImageFileRawWithValidation } from './temporary-image-file/temporary-image-file-raw-with-validation';

export class TemporaryFile extends AbstractDomainWithValidation implements TemporaryFileInterface {
  id: TemporaryFileId;
  uuid: Uuid;
  base64: string;
  mimeType: string;
  filename: string;
  createdAt: Date;

  constructor(temporaryFileRaw: TemporaryFileRawInterface) {
    super();

    if (temporaryFileRaw.mimeType.startsWith('image/')) {
      this.validateTemporaryImageFileRaw(temporaryFileRaw);
    } else {
      this.validateTemporaryDocumentFileRaw(temporaryFileRaw);
    }

    this.id = null;
    this.uuid = v4();
    this.base64 = temporaryFileRaw.data.toString('base64');
    this.mimeType = temporaryFileRaw.mimeType;
    this.filename = temporaryFileRaw.filename;
    this.createdAt = new Date();
  }

  private validateTemporaryImageFileRaw(temporaryFileRaw: TemporaryFileRawInterface): void {
    const temporaryImageFileRawWithValidation: TemporaryImageFileRawWithValidation = new TemporaryImageFileRawWithValidation();
    Object.assign(temporaryImageFileRawWithValidation, temporaryFileRaw);
    temporaryImageFileRawWithValidation.validate('TemporaryImageFile');
  }

  private validateTemporaryDocumentFileRaw(temporaryFileRaw: TemporaryFileRawInterface): void {
    const temporaryDocumentFileRawWithValidation: TemporaryDocumentFileRawWithValidation = new TemporaryDocumentFileRawWithValidation();
    Object.assign(temporaryDocumentFileRawWithValidation, temporaryFileRaw);
    temporaryDocumentFileRawWithValidation.validate('TemporaryDocumentFile');
  }
}

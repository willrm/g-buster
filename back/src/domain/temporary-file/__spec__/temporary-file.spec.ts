import { v4 } from 'uuid';
import { Uuid } from '../../type-aliases';
import { TemporaryFile } from '../temporary-file';
import { TemporaryFileRawInterface } from '../temporary-file.raw.interface';

jest.mock('uuid', () => ({
  v4: jest.fn(),
}));

describe('domain/temporary-file/TemporaryFile', () => {
  describe('constructor()', () => {
    let temporaryFileRaw: TemporaryFileRawInterface;
    beforeEach(() => {
      temporaryFileRaw = { mimeType: 'application/rtf', data: Buffer.alloc(1, 'a') } as TemporaryFileRawInterface;
    });

    describe('id', () => {
      it('should initialize id to null', () => {
        // when
        const result: TemporaryFile = new TemporaryFile(temporaryFileRaw);

        // then
        expect(result.id).toBeNull();
      });
    });

    describe('uuid', () => {
      it('should initialize uuid with a generated uuid', () => {
        // given
        const uuid: Uuid = 'an-uuid';
        (v4 as jest.Mock).mockReturnValue(uuid);

        // when
        const result: TemporaryFile = new TemporaryFile(temporaryFileRaw);

        // then
        expect(result.uuid).toBe(uuid);
      });
    });

    describe('mimeType', () => {
      it('should bind mimeType from raw', () => {
        // given
        temporaryFileRaw.mimeType = 'application/pdf';

        // when
        const result: TemporaryFile = new TemporaryFile(temporaryFileRaw);

        // then
        expect(result.mimeType).toBe('application/pdf');
      });
    });

    describe('base64', () => {
      it('should convert data from raw to base64', () => {
        // given
        temporaryFileRaw.data = Buffer.alloc(11, 'aGVsbG8gd29ybGQ=', 'base64');

        // when
        const result: TemporaryFile = new TemporaryFile(temporaryFileRaw);

        // then
        expect(result.base64).toBe('aGVsbG8gd29ybGQ=');
      });
    });

    describe('createdAt', () => {
      it('should set created at with current date time', () => {
        // given
        const fixedDate: Date = new Date('2019-11-15T16:35:20');
        // @ts-ignore
        jest.spyOn(global, 'Date').mockImplementationOnce(() => fixedDate);

        // when
        const result: TemporaryFile = new TemporaryFile(temporaryFileRaw);

        // then
        expect(result.createdAt).toBe(fixedDate);
      });
    });

    describe('filename', () => {
      it('should bind filename from raw', () => {
        // given
        temporaryFileRaw.filename = 'filename.pdf';

        // when
        const result: TemporaryFile = new TemporaryFile(temporaryFileRaw);

        // then
        expect(result.filename).toBe('filename.pdf');
      });
    });

    describe('when temporary file is an image', () => {
      beforeEach(() => {
        temporaryFileRaw.mimeType = 'image/png';
      });

      describe('mimeType', () => {
        it('should throw an exception when mime type is not png or jpeg', () => {
          // given
          temporaryFileRaw.mimeType = 'image/svg';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).toThrow('TemporaryImageFile creation error:\n - Image file mime type should be one of image/jpeg,image/png');
        });

        it('should not throw any exception when mime type is png', () => {
          // given
          temporaryFileRaw.mimeType = 'image/png';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when mime type is jpeg', () => {
          // given
          temporaryFileRaw.mimeType = 'image/jpeg';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('sizeInBytes', () => {
        it('should throw an exception when size in bytes is more than 300KB', () => {
          // given
          temporaryFileRaw.sizeInBytes = 300 * 1024 + 1;

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).toThrow('TemporaryImageFile creation error:\n - Image file size should be 307200 bytes maximum');
        });
      });

      describe('filename', () => {
        it('should throw an exception when filename extension is not jp(e)g or png', () => {
          // given
          temporaryFileRaw.filename = 'a-file.svg';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).toThrow('TemporaryImageFile creation error:\n - Image file extension should be one of jpg,jpeg,png');
        });

        it('should not throw any exception when filename extension is jpg case-insensitive', () => {
          // given
          temporaryFileRaw.filename = 'a-file.JpG';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when filename extension is jpeg case-insensitive', () => {
          // given
          temporaryFileRaw.filename = 'a-file.jPEg';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when filename extension is png case-insensitive', () => {
          // given
          temporaryFileRaw.filename = 'a-file.pNg';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });
      });
    });

    describe('when temporary file is not an image', () => {
      beforeEach(() => {
        temporaryFileRaw.mimeType = 'application/pdf';
      });

      describe('mimeType', () => {
        it('should throw an exception when mime type is not doc or docx or odt or pdf or rtf', () => {
          // given
          temporaryFileRaw.mimeType = 'application/unknown';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).toThrow(
            'TemporaryDocumentFile creation error:\n - Document file mime type should be one of application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.oasis.opendocument.text,application/pdf,application/rtf'
          );
        });

        it('should not throw any exception when mime type is doc', () => {
          // given
          temporaryFileRaw.mimeType = 'application/msword';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when mime type is docx', () => {
          // given
          temporaryFileRaw.mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when mime type is odt', () => {
          // given
          temporaryFileRaw.mimeType = 'application/vnd.oasis.opendocument.text';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when mime type is pdf', () => {
          // given
          temporaryFileRaw.mimeType = 'application/pdf';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when mime type is rtf', () => {
          // given
          temporaryFileRaw.mimeType = 'application/rtf';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });
      });

      describe('sizeInBytes', () => {
        it('should throw an exception when size in bytes is more than 10MB', () => {
          // given
          temporaryFileRaw.sizeInBytes = 10000 * 1024 + 1;

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).toThrow('TemporaryDocumentFile creation error:\n - Document file size should be 10240000 bytes maximum');
        });
      });

      describe('filename', () => {
        it('should throw an exception when filename extension is not doc or docx or odt or pdf or rtf', () => {
          // given
          temporaryFileRaw.filename = 'a-file.exe';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).toThrow('TemporaryDocumentFile creation error:\n - Document file extension should be one of doc,docx,odt,pdf,rtf');
        });

        it('should not throw any exception when filename extension is doc case-insensitive', () => {
          // given
          temporaryFileRaw.filename = 'a-file.DoC';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when filename extension is docx case-insensitive', () => {
          // given
          temporaryFileRaw.filename = 'a-file.dOCx';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when filename extension is odt case-insensitive', () => {
          // given
          temporaryFileRaw.filename = 'a-file.OdT';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when filename extension is pdf case-insensitive', () => {
          // given
          temporaryFileRaw.filename = 'a-file.pDf';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });

        it('should not throw any exception when filename extension is rtf case-insensitive', () => {
          // given
          temporaryFileRaw.filename = 'a-file.rtF';

          // when
          const result = () => new TemporaryFile(temporaryFileRaw);

          // then
          expect(result).not.toThrow();
        });
      });
    });
  });
});

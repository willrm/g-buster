import { TemporaryFileId, Uuid } from '../type-aliases';

export interface TemporaryFileInterface {
  id: TemporaryFileId;
  filename: string;
  uuid: Uuid;
  base64: string;
  mimeType: string;
  createdAt: Date;
}

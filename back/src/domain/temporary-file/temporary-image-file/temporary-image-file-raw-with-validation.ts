import { IsIn, Matches, Max } from 'class-validator';
import { AbstractDomainWithValidation } from '../../validation/abstract-domain-with-validation';
import {
  TEMPORARY_IMAGE_FILE_ACCEPTED_EXTENSIONS,
  TEMPORARY_IMAGE_FILE_ACCEPTED_MIME_TYPES,
  TEMPORARY_IMAGE_FILE_MAX_SIZE_IN_BYTES,
} from './temporary-image-file-constraints';
import { TemporaryFileRawInterface } from '../temporary-file.raw.interface';

export class TemporaryImageFileRawWithValidation extends AbstractDomainWithValidation implements TemporaryFileRawInterface {
  data: Buffer;

  @Matches(new RegExp(`\\.(${TEMPORARY_IMAGE_FILE_ACCEPTED_EXTENSIONS.join('|')})$`, 'i'), {
    message: `Image file extension should be one of ${TEMPORARY_IMAGE_FILE_ACCEPTED_EXTENSIONS}`,
  })
  filename: string;

  @IsIn(TEMPORARY_IMAGE_FILE_ACCEPTED_MIME_TYPES, {
    message: 'Image file mime type should be one of $constraint1',
  })
  mimeType: string;

  @Max(TEMPORARY_IMAGE_FILE_MAX_SIZE_IN_BYTES, {
    message: 'Image file size should be $constraint1 bytes maximum',
  })
  sizeInBytes: number;
}

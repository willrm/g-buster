export const TEMPORARY_IMAGE_FILE_ACCEPTED_MIME_TYPES: string[] = ['image/jpeg', 'image/png'];
export const TEMPORARY_IMAGE_FILE_ACCEPTED_EXTENSIONS: string[] = ['jpg', 'jpeg', 'png'];
export const TEMPORARY_IMAGE_FILE_MAX_SIZE_IN_BYTES: number = 300 * 1024;

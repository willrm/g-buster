import { Uuid } from '../type-aliases';
import { TemporaryFileInterface } from './temporary-file.interface';

export interface TemporaryFileRepository {
  save(temporaryFile: TemporaryFileInterface): Promise<TemporaryFileInterface>;
  findByUuid(uuid: Uuid): Promise<TemporaryFileInterface>;
  findAllByCreatedAtBefore(date: Date): Promise<TemporaryFileInterface[]>;
  delete(temporaryFiles: TemporaryFileInterface[]): Promise<void>;
}

import { TemporaryFile } from '../domain/temporary-file/temporary-file';
import { TemporaryFileInterface } from '../domain/temporary-file/temporary-file.interface';
import { TemporaryFileRawInterface } from '../domain/temporary-file/temporary-file.raw.interface';
import { TemporaryFileRepository } from '../domain/temporary-file/temporary-file.repository';

export class CreateTemporaryFile {
  constructor(private readonly temporaryFileRepository: TemporaryFileRepository) {}

  async execute(temporaryFileRaw: TemporaryFileRawInterface): Promise<TemporaryFileInterface> {
    const temporaryFile: TemporaryFile = new TemporaryFile(temporaryFileRaw);

    return await this.temporaryFileRepository.save(temporaryFile);
  }
}

import { ItemNotFoundError } from '../domain/common-errors/item-not-found.error';
import { JobOfferStatus } from '../domain/job-offer/job-offer-enums';
import { JobOfferInterface } from '../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../domain/job-offer/job-offer.repository.interface';
import { JobOfferId } from '../domain/type-aliases';
import { UserInterface } from '../domain/user/user.interface';
import { UserRepository } from '../domain/user/user.repository.interface';

export class GetUserJobOfferById {
  constructor(private readonly userRepository: UserRepository, private readonly jobOfferRepository: JobOfferRepository) {}

  async execute(id: JobOfferId): Promise<JobOfferInterface> {
    const foundJobOffer: JobOfferInterface = await this.jobOfferRepository.findById(id);
    const currentUser: UserInterface = await this.userRepository.getCurrent();

    if (currentUser.id !== foundJobOffer.userId || foundJobOffer.status !== JobOfferStatus.STATUS_DRAFT) {
      return Promise.reject(new ItemNotFoundError('JobOffer not found'));
    } else {
      return Promise.resolve(foundJobOffer);
    }
  }
}

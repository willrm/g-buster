import { AnonymizedCompanyAdapter } from '../../domain/company/anonymized-company-adapter';
import { CompanyInterface } from '../../domain/company/company.interface';
import { AnonymizedJobOfferAdapter } from '../../domain/job-offer/anonymized-job-offer-adapter';
import { JobOfferInterface } from '../../domain/job-offer/job-offer.interface';
import { JobOfferWithCompanyInterface } from './job-offer-with-company.interface';

export class JobOfferWithCompanyAdapter {
  constructor(
    private readonly anonymizedJobOfferAdapter: AnonymizedJobOfferAdapter,
    private readonly anonymizedCompanyAdapter: AnonymizedCompanyAdapter
  ) {}

  toJobOfferWithCompany(jobOffer: JobOfferInterface, company: CompanyInterface, anonymize: boolean): JobOfferWithCompanyInterface {
    if (anonymize) {
      return {
        ...this.anonymizedJobOfferAdapter.anonymize(jobOffer),
        company: this.anonymizedCompanyAdapter.anonymize(company),
      } as JobOfferWithCompanyInterface;
    } else {
      return {
        ...jobOffer,
        company: { ...company },
      } as JobOfferWithCompanyInterface;
    }
  }
}

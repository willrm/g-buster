import {
  AdvantageFinancial,
  AdvantageHealth,
  AdvantageLifeBalance,
  AdvantageProfessional,
  AdvantageSocial,
} from '../../../domain/advantages/advantages-enums';
import { AnonymizedCompanyAdapter } from '../../../domain/company/anonymized-company-adapter';
import {
  CompanyBusinessSegment,
  CompanyGeniusType,
  CompanyRevenue,
  CompanyRevenueForRD,
  CompanySize,
  CompanyStatus,
} from '../../../domain/company/company-enums';
import { CompanyInterface } from '../../../domain/company/company.interface';
import { AnonymizedJobOfferAdapter } from '../../../domain/job-offer/anonymized-job-offer-adapter';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from '../../../domain/job-offer/job-offer-enums';
import { JobOfferInterface } from '../../../domain/job-offer/job-offer.interface';
import { TravelingFrequency } from '../../../domain/traveling/traveling-enums';
import { JobOfferWithCompanyAdapter } from '../job-offer-with-company-adapter';
import { JobOfferWithCompanyInterface } from '../job-offer-with-company.interface';

describe('use_cases/adapters/JobOfferWithCompanyAdapter', () => {
  let jobOfferWithCompanyAdapter: JobOfferWithCompanyAdapter;
  let mockAnonymizedJobOfferAdapter: AnonymizedJobOfferAdapter;
  let mockAnonymizedCompanyAdapter: AnonymizedCompanyAdapter;

  beforeEach(() => {
    mockAnonymizedJobOfferAdapter = {} as AnonymizedJobOfferAdapter;
    mockAnonymizedJobOfferAdapter.anonymize = jest.fn();

    mockAnonymizedCompanyAdapter = {} as AnonymizedCompanyAdapter;
    mockAnonymizedCompanyAdapter.anonymize = jest.fn();

    jobOfferWithCompanyAdapter = new JobOfferWithCompanyAdapter(mockAnonymizedJobOfferAdapter, mockAnonymizedCompanyAdapter);
  });

  describe('toJobOfferWithCompany()', () => {
    it('should copy given job offer', () => {
      // given
      const jobOfferWithAllPropertiesFilled: JobOfferInterface = {
        id: '123456789',
        userId: '987654321',
        companyId: '879135135687',
        status: JobOfferStatus.STATUS_PENDING_VALIDATION,
        jobStatus: JobOfferJobStatus.JOB_STATUS_FULL_TIME,
        jobType: JobOfferJobType.JOB_TYPE_REGULAR,
        title: 'a job title',
        city: 'a city',
        region: JobOfferRegion.REGION_GASPESIE_ILES_DE_LA_MADELEINE,
        targetCustomers: [JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS],
        requiredExperiences: [JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS],
        positionType: JobOfferPositionType.POSITION_TYPE_FORESTRY_ENGINEER,
        requestedSpecialities: [JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE],
        description: 'A job description',
        specificities: 'Some specificities',
        traveling: {
          isTraveling: false,
          quebec: TravelingFrequency.TRAVELING_FREQUENT,
          canada: TravelingFrequency.TRAVELING_FREQUENT,
          international: TravelingFrequency.TRAVELING_OCCASIONAL,
        },
        advantages: {
          health: [AdvantageHealth.HEALTH_DENTAL_INSURANCE],
          social: [AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES],
          lifeBalance: [AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK],
          financial: [AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT],
          professional: [AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM],
        },
        annualSalaryRange: { minimumSalary: 10000, maximumSalary: 999999 },
        wantedPersonalities: [JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS],
        endDateOfApplication: new Date('2020-10-15T00:00:00'),
        startDateOfEmployment: { asSoonAsPossible: true, startDate: new Date('2021-01-01T00:00:00') },
        emailAddressForReceiptOfApplications: 'test@example.org',
        internalReferenceNumber: 'ABC123XYZ',
        publishedAt: new Date('2019-03-28T00:00:00'),
      };

      // when
      const result: JobOfferWithCompanyInterface = jobOfferWithCompanyAdapter.toJobOfferWithCompany(
        jobOfferWithAllPropertiesFilled,
        {} as CompanyInterface,
        false
      );

      // then
      expect(result).toMatchObject(jobOfferWithAllPropertiesFilled);
    });

    it('should anonymize job offer when anonymize flag is true', () => {
      // given
      const jobOffer: JobOfferInterface = { id: '123456789', description: 'A job description' } as JobOfferInterface;

      // when
      jobOfferWithCompanyAdapter.toJobOfferWithCompany(jobOffer, {} as CompanyInterface, true);

      // then
      expect(mockAnonymizedJobOfferAdapter.anonymize).toHaveBeenCalledWith(jobOffer);
    });

    it('should return anonymized job offer when anonymize flag is true', () => {
      // given
      const anonymizedJobOffer: JobOfferInterface = { id: '123456789' } as JobOfferInterface;
      (mockAnonymizedJobOfferAdapter.anonymize as jest.Mock).mockReturnValue(anonymizedJobOffer);

      // when
      const result: JobOfferWithCompanyInterface = jobOfferWithCompanyAdapter.toJobOfferWithCompany(
        {} as JobOfferInterface,
        {} as CompanyInterface,
        true
      );

      // then
      expect(result).toMatchObject(anonymizedJobOffer);
    });

    it('should copy given company to company field', () => {
      // given
      const companyWithAllPropertiesFilled: CompanyInterface = {
        id: '123456789',
        userId: '987654321',
        status: CompanyStatus.STATUS_PENDING_VALIDATION,
        activityLocations: { quebec: 42 },
        address: 'company address',
        bachelorsOfEngineeringNumber: 1337,
        businessSegment: CompanyBusinessSegment.BUSINESS_SEGMENT_PULP_AND_PAPER,
        creationDate: new Date('2010-10-15T00:00:00'),
        geniusTypes: [CompanyGeniusType.GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING],
        logo: { mimeType: 'image/type', base64: 'image-as-base64' },
        name: 'company name',
        presentation: 'company presentation',
        revenue: CompanyRevenue.REVENUE_BETWEEN_5M_AND_20M,
        revenueForRD: CompanyRevenueForRD.REVENUE_RD_MORE_THAN_20M,
        size: CompanySize.HUNDRED_TWENTY_TO_TWO_HUNDRED_NINETY_NINE,
        socialNetworks: { linkedIn: 'linkedIn url', facebook: 'facebook url' },
        values: [{ justification: 'value justification', meaning: 'value meaning' }],
        website: 'company website',
      };

      // when
      const result: JobOfferWithCompanyInterface = jobOfferWithCompanyAdapter.toJobOfferWithCompany(
        {} as JobOfferInterface,
        companyWithAllPropertiesFilled,
        false
      );

      // then
      expect(result.company).toStrictEqual(companyWithAllPropertiesFilled);
    });

    it('should anonymize company when anonymize flag is true', () => {
      // given
      const company: CompanyInterface = { id: '123456789', presentation: 'company presentation' } as CompanyInterface;

      // when
      jobOfferWithCompanyAdapter.toJobOfferWithCompany({} as JobOfferInterface, company, true);

      // then
      expect(mockAnonymizedCompanyAdapter.anonymize).toHaveBeenCalledWith(company);
    });

    it('should return anonymized company when anonymize flag is true', () => {
      // given
      const anonymizedCompany: CompanyInterface = { id: '123456789' } as CompanyInterface;
      (mockAnonymizedCompanyAdapter.anonymize as jest.Mock).mockReturnValue(anonymizedCompany);

      // when
      const result: JobOfferWithCompanyInterface = jobOfferWithCompanyAdapter.toJobOfferWithCompany(
        {} as JobOfferInterface,
        {} as CompanyInterface,
        true
      );

      // then
      expect(result.company).toStrictEqual(anonymizedCompany);
    });
  });
});

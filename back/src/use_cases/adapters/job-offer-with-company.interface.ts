import { CompanyInterface } from '../../domain/company/company.interface';
import { JobOfferInterface } from '../../domain/job-offer/job-offer.interface';

export interface JobOfferWithCompanyInterface extends JobOfferInterface {
  company: CompanyInterface;
}

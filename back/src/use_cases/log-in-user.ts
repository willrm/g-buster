import { User } from '../domain/user/user';
import { UserRawInterface } from '../domain/user/user.raw.interface';
import { UserRepository } from '../domain/user/user.repository.interface';

export class LogInUser {
  constructor(private readonly userRepository: UserRepository) {}

  async execute(userRaw: UserRawInterface): Promise<void> {
    const adminEmailAddresses: string[] = await this.userRepository.findAllAdminEmailAddresses();
    const user: User = new User(userRaw, adminEmailAddresses);

    return this.userRepository.logIn(user);
  }
}

import { JobOfferJobType, JobOfferRegion, JobOfferRequestedSpeciality, JobOfferRequiredExperience } from '../domain/job-offer/job-offer-enums';

export interface GetPublishedJobOffersFilterInterface {
  requestedSpecialities?: JobOfferRequestedSpeciality[];
  jobTypes?: JobOfferJobType[];
  requiredExperiences?: JobOfferRequiredExperience[];
  regions?: JobOfferRegion[];
}

import { I18nMessagesInterface } from '../domain/i18n/i18n-messages.interface';
import { I18nRepository } from '../domain/i18n/i18n.repository.interface';
import { Lang } from '../domain/type-aliases';

export class GetI18nMessagesForLang {
  constructor(private readonly i18nRepository: I18nRepository) {}

  execute(lang: Lang): Promise<I18nMessagesInterface> {
    return this.i18nRepository.findMessagesByLang(lang);
  }
}

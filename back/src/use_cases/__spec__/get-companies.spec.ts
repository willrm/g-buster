import { CompanyStatus } from '../../domain/company/company-enums';
import { CompanyInterface } from '../../domain/company/company.interface';
import { CompanyRepository } from '../../domain/company/company.repository.interface';
import { GetCompanies } from '../get-companies';
import { GetCompaniesFilterInterface } from '../get-companies-filter.interface';

describe('uses_cases/getCompanies', () => {
  let getCompanies: GetCompanies;
  let mockCompanyRepository: CompanyRepository;

  beforeAll(() => {
    mockCompanyRepository = {} as CompanyRepository;
    mockCompanyRepository.findAll = jest.fn();

    getCompanies = new GetCompanies(mockCompanyRepository);
  });

  describe('execute()', () => {
    it('should call repository to find published companies', async () => {
      // given
      (mockCompanyRepository.findAll = jest.fn()).mockReturnValue([
        {
          id: 'Company id',
          name: 'Company name',
          status: CompanyStatus.STATUS_PENDING_VALIDATION,
        } as CompanyInterface,
      ]);

      // when
      await getCompanies.execute();

      // then
      expect(mockCompanyRepository.findAll).toHaveBeenCalled();
    });

    it('should return found companies from repository', async () => {
      // given
      const companies: CompanyInterface[] = [
        { id: '1', name: 'A company name' } as CompanyInterface,
        { id: '2', name: 'An other company name' } as CompanyInterface,
      ];
      (mockCompanyRepository.findAll as jest.Mock).mockReturnValue(companies);

      // when
      const result: CompanyInterface[] = await getCompanies.execute();

      // then
      expect(result).toStrictEqual(companies);
    });

    describe('with filter', () => {
      let publishedCompanies: CompanyInterface[];
      let unpublishedCompanies: CompanyInterface[];

      beforeEach(() => {
        publishedCompanies = [
          {
            id: '1',
            name: 'A published company name',
            status: CompanyStatus.STATUS_PUBLISHED,
          } as CompanyInterface,
          {
            id: '2',
            name: 'An other published company name',
            status: CompanyStatus.STATUS_PUBLISHED,
          } as CompanyInterface,
        ];
        unpublishedCompanies = [
          {
            id: '3',
            name: 'An unpublished company name',
            status: CompanyStatus.STATUS_PENDING_VALIDATION,
          } as CompanyInterface,
          {
            id: '4',
            name: 'An other unpublished company name',
            status: CompanyStatus.STATUS_PENDING_VALIDATION,
          } as CompanyInterface,
        ];
        (mockCompanyRepository.findAll as jest.Mock).mockReturnValue([...publishedCompanies, ...unpublishedCompanies]);
      });

      describe('status', () => {
        it('should not filter companies when status filter is null or undefined ', async () => {
          // given
          const expected: CompanyInterface[] = [...publishedCompanies, ...unpublishedCompanies];
          const filter: GetCompaniesFilterInterface = { status: undefined };

          // when
          const result: CompanyInterface[] = await getCompanies.execute(filter);

          // then
          expect(result).toStrictEqual(expected);
        });

        it('should return published companies when status filter is set to published', async () => {
          // given
          const expected: CompanyInterface[] = publishedCompanies;
          const filter: GetCompaniesFilterInterface = { status: CompanyStatus.STATUS_PUBLISHED };

          // when
          const result: CompanyInterface[] = await getCompanies.execute(filter);

          // then
          expect(result).toStrictEqual(expected);
        });

        it('should return unpublished companies when status filter is set to unpublished', async () => {
          // given
          const expected: CompanyInterface[] = unpublishedCompanies;
          const filter: GetCompaniesFilterInterface = { status: CompanyStatus.STATUS_PENDING_VALIDATION };

          // when
          const result: CompanyInterface[] = await getCompanies.execute(filter);

          // then
          expect(result).toStrictEqual(expected);
        });
      });
    });
  });
});

import { TemporaryFileInterface } from '../../domain/temporary-file/temporary-file.interface';
import { TemporaryFileRepository } from '../../domain/temporary-file/temporary-file.repository';
import { TemporaryFileId } from '../../domain/type-aliases';
import { CleanExpiredTemporaryFiles } from '../clean-expired-temporary-files';

describe('use_cases/CleanExpiredTemporaryFiles', () => {
  let cleanExpiredTemporaryFiles: CleanExpiredTemporaryFiles;
  let mockTemporaryFileRepository: TemporaryFileRepository;
  let realDateConstructor: DateConstructor;

  beforeAll(() => {
    realDateConstructor = Date;
  });

  beforeEach(() => {
    global.Date = realDateConstructor;

    mockTemporaryFileRepository = {} as TemporaryFileRepository;
    mockTemporaryFileRepository.findAllByCreatedAtBefore = jest.fn();
    mockTemporaryFileRepository.delete = jest.fn();
    cleanExpiredTemporaryFiles = new CleanExpiredTemporaryFiles(mockTemporaryFileRepository);

    (mockTemporaryFileRepository.findAllByCreatedAtBefore as jest.Mock).mockReturnValue(Promise.resolve([]));
  });

  describe('execute()', () => {
    it('should find temporary files created more than one hour ago', async () => {
      // given
      const fixedDate: Date = new Date('2019-11-15T16:35:20');
      const fixedDateOneHourBefore: Date = new Date('2019-11-15T15:35:20');
      // @ts-ignore
      jest.spyOn(global, 'Date').mockImplementationOnce(() => fixedDate);

      // when
      await cleanExpiredTemporaryFiles.execute();

      // then
      expect(mockTemporaryFileRepository.findAllByCreatedAtBefore).toHaveBeenCalledWith(fixedDateOneHourBefore);
    });

    it('should delete found temporary files', async () => {
      // given
      const expected: TemporaryFileInterface[] = [
        { id: '42', base64: 'data as base64' } as TemporaryFileInterface,
        { id: '43', base64: 'another data as base64' } as TemporaryFileInterface,
      ];
      (mockTemporaryFileRepository.findAllByCreatedAtBefore as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      await cleanExpiredTemporaryFiles.execute();

      // then
      expect(mockTemporaryFileRepository.delete).toHaveBeenCalledWith(expected);
    });

    it('should not delete any temporary file when none found', async () => {
      // given
      (mockTemporaryFileRepository.findAllByCreatedAtBefore as jest.Mock).mockReturnValue(Promise.resolve([] as TemporaryFileInterface[]));

      // when
      await cleanExpiredTemporaryFiles.execute();

      // then
      expect(mockTemporaryFileRepository.delete).not.toHaveBeenCalled();
    });

    it('should return the ids of temporary files deleted', async () => {
      // given
      const temporaryFiles: TemporaryFileInterface[] = [
        { id: '42', base64: 'data as base64' } as TemporaryFileInterface,
        { id: '43', base64: 'another data as base64' } as TemporaryFileInterface,
      ];
      (mockTemporaryFileRepository.findAllByCreatedAtBefore as jest.Mock).mockReturnValue(Promise.resolve(temporaryFiles));

      // when
      const result: TemporaryFileId[] = await cleanExpiredTemporaryFiles.execute();

      // then
      expect(result).toStrictEqual(['42', '43']);
    });
  });
});

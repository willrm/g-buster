import { ItemNotFoundError } from '../../domain/common-errors/item-not-found.error';
import { CompanyInterface } from '../../domain/company/company.interface';
import { CompanyRepository } from '../../domain/company/company.repository.interface';
import {
  JobOfferJobType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
} from '../../domain/job-offer/job-offer-enums';
import { JobOfferInterface } from '../../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../../domain/job-offer/job-offer.repository.interface';
import { CompanyId } from '../../domain/type-aliases';
import { UserRole } from '../../domain/user/user-enums';
import { UserInterface } from '../../domain/user/user.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { JobOfferWithCompanyAdapter } from '../adapters/job-offer-with-company-adapter';
import { JobOfferWithCompanyInterface } from '../adapters/job-offer-with-company.interface';
import { GetPublishedJobOffers } from '../get-published-job-offers';
import { GetPublishedJobOffersFilterInterface } from '../get-published-job-offers-filter.interface';

describe('uses_cases/GetPublishedJobOffers', () => {
  let getPublishedJobOffers: GetPublishedJobOffers;
  let mockJobOfferRepository: JobOfferRepository;
  let mockUserRepository: UserRepository;
  let mockCompanyRepository: CompanyRepository;
  let mockJobOfferWithCompanyAdapter: JobOfferWithCompanyAdapter;

  let fixedDate: Date;
  let fixedDateOneDayBefore: Date;
  let fixedDateOneDayAfter: Date;

  beforeAll(() => {
    fixedDate = new Date('2019-11-15T16:35:20');
    fixedDateOneDayBefore = new Date('2019-11-14T16:35:20');
    fixedDateOneDayAfter = new Date('2019-11-16T16:35:20');
    // @ts-ignore
    jest.spyOn(global, 'Date').mockImplementation(() => fixedDate);
  });

  beforeEach(() => {
    mockJobOfferRepository = {} as JobOfferRepository;
    mockJobOfferRepository.findAllByStatus = jest.fn();

    mockUserRepository = {} as UserRepository;
    mockUserRepository.getCurrent = jest.fn();

    mockCompanyRepository = {} as CompanyRepository;
    mockCompanyRepository.findAllByIds = jest.fn();

    mockJobOfferWithCompanyAdapter = {} as JobOfferWithCompanyAdapter;
    mockJobOfferWithCompanyAdapter.toJobOfferWithCompany = jest.fn();

    getPublishedJobOffers = new GetPublishedJobOffers(
      mockJobOfferRepository,
      mockUserRepository,
      mockCompanyRepository,
      mockJobOfferWithCompanyAdapter
    );

    (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve([]));
    (mockCompanyRepository.findAllByIds as jest.Mock).mockReturnValue(Promise.resolve([]));

    const userWithCandidateRole: UserInterface = { firstName: 'Test', role: UserRole.CANDIDATE } as UserInterface;
    (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(userWithCandidateRole));
  });

  describe('execute()', () => {
    it('should call repository to find published job offers', async () => {
      // when
      await getPublishedJobOffers.execute();

      // then
      expect(mockJobOfferRepository.findAllByStatus).toHaveBeenCalledWith(JobOfferStatus.STATUS_PUBLISHED);
    });

    it('should find company for each found job offer', async () => {
      // given
      const jobOffer1: JobOfferInterface = createPublishedJobOffer('Job 1', '1337');
      const jobOffer2: JobOfferInterface = createPublishedJobOffer('Job 2', '1338');
      const jobOffer3: JobOfferInterface = createPublishedJobOffer('Job 3', '1337');
      (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve([jobOffer1, jobOffer2, jobOffer3]));

      // when
      await getPublishedJobOffers.execute();

      // then
      expect(mockCompanyRepository.findAllByIds).toHaveBeenCalledWith(['1337', '1338']);
    });

    it('should add company to each found job offer without anonymization when user is logged in as candidate', async () => {
      // given
      const jobOffer1: JobOfferInterface = createPublishedJobOffer('Job 1', '1337');
      const jobOffer2: JobOfferInterface = createPublishedJobOffer('Job 2', '1338');
      (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve([jobOffer1, jobOffer2]));

      const company1: CompanyInterface = { id: '1337' } as CompanyInterface;
      const company2: CompanyInterface = { id: '1338' } as CompanyInterface;
      (mockCompanyRepository.findAllByIds as jest.Mock).mockReturnValue(Promise.resolve([company1, company2]));

      // when
      await getPublishedJobOffers.execute();

      // then
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(1, jobOffer1, company1, false);
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(2, jobOffer2, company2, false);
    });

    it('should add company to each found job offer with anonymization when user is logged in as company', async () => {
      // given
      const jobOffer1: JobOfferInterface = createPublishedJobOffer('Job 1', '1337');
      const jobOffer2: JobOfferInterface = createPublishedJobOffer('Job 2', '1338');
      (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve([jobOffer1, jobOffer2]));

      const company1: CompanyInterface = { id: '1337' } as CompanyInterface;
      const company2: CompanyInterface = { id: '1338' } as CompanyInterface;
      (mockCompanyRepository.findAllByIds as jest.Mock).mockReturnValue(Promise.resolve([company1, company2]));

      const userWithCompanyRole: UserInterface = { firstName: 'Test', role: UserRole.COMPANY } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(userWithCompanyRole));

      // when
      await getPublishedJobOffers.execute();

      // then
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(1, jobOffer1, company1, true);
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(2, jobOffer2, company2, true);
    });

    it('should add company to each found job offer with anonymization when no user logged in', async () => {
      // given
      const jobOffer1: JobOfferInterface = createPublishedJobOffer('Job 1', '1337');
      const jobOffer2: JobOfferInterface = createPublishedJobOffer('Job 2', '1338');
      (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve([jobOffer1, jobOffer2]));

      const company1: CompanyInterface = { id: '1337' } as CompanyInterface;
      const company2: CompanyInterface = { id: '1338' } as CompanyInterface;
      (mockCompanyRepository.findAllByIds as jest.Mock).mockReturnValue(Promise.resolve([company1, company2]));

      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.reject(new ItemNotFoundError('User not found')));

      // when
      await getPublishedJobOffers.execute();

      // then
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(1, jobOffer1, company1, true);
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(2, jobOffer2, company2, true);
    });

    it('should return found job offers with company', async () => {
      // given
      const jobOffer1: JobOfferInterface = createPublishedJobOffer('Job 1', '1337');
      const jobOffer2: JobOfferInterface = createPublishedJobOffer('Job 2', '1338');
      (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve([jobOffer1, jobOffer2]));

      const jobOfferWithCompany1: JobOfferWithCompanyInterface = { ...jobOffer1, company: { id: '1337' } } as JobOfferWithCompanyInterface;
      const jobOfferWithCompany2: JobOfferWithCompanyInterface = { ...jobOffer2, company: { id: '1338' } } as JobOfferWithCompanyInterface;
      (mockJobOfferWithCompanyAdapter.toJobOfferWithCompany as jest.Mock)
        .mockReturnValueOnce(jobOfferWithCompany1)
        .mockReturnValueOnce(jobOfferWithCompany2);

      // when
      const result: JobOfferWithCompanyInterface[] = await getPublishedJobOffers.execute();

      // then
      expect(result).toStrictEqual([jobOfferWithCompany1, jobOfferWithCompany2]);
    });

    it('should add empty company to job offer when company has not been found', async () => {
      // given
      const jobOffer: JobOfferInterface = createPublishedJobOffer('Job 1', '1337');
      (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve([jobOffer]));

      (mockCompanyRepository.findAllByIds as jest.Mock).mockReturnValue(Promise.resolve([]));

      // when
      await getPublishedJobOffers.execute();

      // then
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledWith(jobOffer, {} as CompanyInterface, false);
    });

    it('should not handle job offers having no published date', async () => {
      // given
      const jobOfferPublishedAtFixedDate: JobOfferInterface = createPublishedJobOffer('Job 1');
      const jobOfferWithoutPublishedDate: JobOfferInterface = {
        title: 'Job 2',
        status: JobOfferStatus.STATUS_PUBLISHED,
        publishedAt: null,
      } as JobOfferInterface;
      (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(
        Promise.resolve([jobOfferPublishedAtFixedDate, jobOfferWithoutPublishedDate])
      );

      // when
      await getPublishedJobOffers.execute();

      // then
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(1);
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledWith(jobOfferPublishedAtFixedDate, {}, false);
    });

    it('should not handle job offers having a published date in the future', async () => {
      // given
      const jobOfferPublishedAtFixedDateOneDayBefore: JobOfferInterface = {
        title: 'Job 1',
        status: JobOfferStatus.STATUS_PUBLISHED,
        publishedAt: fixedDateOneDayBefore,
      } as JobOfferInterface;
      const jobOfferPublishedAtFixedDate: JobOfferInterface = createPublishedJobOffer('Job 2');
      const jobOfferPublishedAtFixedDateOneDayAfter: JobOfferInterface = {
        title: 'Job 3',
        status: JobOfferStatus.STATUS_PUBLISHED,
        publishedAt: fixedDateOneDayAfter,
      } as JobOfferInterface;
      (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(
        Promise.resolve([jobOfferPublishedAtFixedDateOneDayBefore, jobOfferPublishedAtFixedDate, jobOfferPublishedAtFixedDateOneDayAfter])
      );

      // when
      await getPublishedJobOffers.execute();

      // then
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(1, jobOfferPublishedAtFixedDateOneDayBefore, {}, false);
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(2, jobOfferPublishedAtFixedDate, {}, false);
    });

    describe('with filter', () => {
      describe('requestedSpecialities', () => {
        it('should not filter any job offers when requestedSpecialities filter is null or undefined', async () => {
          // given
          const expected: JobOfferInterface[] = [createPublishedJobOffer('Job 1'), createPublishedJobOffer('Job 2')];
          (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve(expected));

          const filter: GetPublishedJobOffersFilterInterface = {};

          // when
          await getPublishedJobOffers.execute(filter);

          // then
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
        });

        it('should not filter any job offers when requestedSpecialities filter is empty', async () => {
          // given
          const expected: JobOfferInterface[] = [createPublishedJobOffer('Job 1'), createPublishedJobOffer('Job 2')];
          (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve(expected));

          const filter: GetPublishedJobOffersFilterInterface = { requestedSpecialities: [] };

          // when
          await getPublishedJobOffers.execute(filter);

          // then
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
        });

        it('should filter job offers using requestedSpecialities filter when not empty', async () => {
          // given
          const jobOffer1: JobOfferInterface = {
            title: 'Job 1',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            requestedSpecialities: [],
          } as JobOfferInterface;
          const jobOffer2: JobOfferInterface = {
            title: 'Job 2',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            requestedSpecialities: [
              JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MARITIME_NAVAL,
              JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
            ],
          } as JobOfferInterface;
          const jobOffer3: JobOfferInterface = {
            title: 'Job 3',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            requestedSpecialities: [
              JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
              JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
            ],
          } as JobOfferInterface;
          const jobOffer4: JobOfferInterface = {
            title: 'Job 4',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            requestedSpecialities: [
              JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
              JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION,
            ],
          } as JobOfferInterface;
          (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve([jobOffer1, jobOffer2, jobOffer3, jobOffer4]));

          const filter: GetPublishedJobOffersFilterInterface = {
            requestedSpecialities: [
              JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MARITIME_NAVAL,
              JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
            ],
          };

          // when
          await getPublishedJobOffers.execute(filter);

          // then
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(1, jobOffer2, {}, false);
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(2, jobOffer3, {}, false);
        });
      });

      describe('jobTypes', () => {
        it('should not filter any job offers when jobTypes filter is null or undefined', async () => {
          // given
          const expected: JobOfferInterface[] = [createPublishedJobOffer('Job 1'), createPublishedJobOffer('Job 2')];
          (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve(expected));

          const filter: GetPublishedJobOffersFilterInterface = {};

          // when
          await getPublishedJobOffers.execute(filter);

          // then
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
        });

        it('should not filter any job offers when jobTypes filter is empty', async () => {
          // given
          const expected: JobOfferInterface[] = [createPublishedJobOffer('Job 1'), createPublishedJobOffer('Job 2')];
          (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve(expected));

          const filter: GetPublishedJobOffersFilterInterface = { jobTypes: [] };

          // when
          await getPublishedJobOffers.execute(filter);

          // then
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
        });

        it('should filter job offers using jobTypes filter when not empty', async () => {
          // given
          const jobOffer1: JobOfferInterface = {
            title: 'Job 1',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            jobType: undefined,
          } as JobOfferInterface;
          const jobOffer2: JobOfferInterface = {
            title: 'Job 2',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            jobType: JobOfferJobType.JOB_TYPE_TEMPORARY,
          } as JobOfferInterface;
          const jobOffer3: JobOfferInterface = {
            title: 'Job 3',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            jobType: JobOfferJobType.JOB_TYPE_INTERNSHIP,
          } as JobOfferInterface;
          const jobOffer4: JobOfferInterface = {
            title: 'Job 4',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            jobType: JobOfferJobType.JOB_TYPE_REGULAR,
          } as JobOfferInterface;
          (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve([jobOffer1, jobOffer2, jobOffer3, jobOffer4]));

          const filter: GetPublishedJobOffersFilterInterface = { jobTypes: [JobOfferJobType.JOB_TYPE_TEMPORARY, JobOfferJobType.JOB_TYPE_REGULAR] };

          // when
          await getPublishedJobOffers.execute(filter);

          // then
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(1, jobOffer2, {}, false);
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(2, jobOffer4, {}, false);
        });
      });

      describe('requiredExperiences', () => {
        it('should not filter any job offers when requiredExperiences filter is null or undefined', async () => {
          // given
          const expected: JobOfferInterface[] = [createPublishedJobOffer('Job 1'), createPublishedJobOffer('Job 2')];
          (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve(expected));

          const filter: GetPublishedJobOffersFilterInterface = {};

          // when
          await getPublishedJobOffers.execute(filter);

          // then
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
        });

        it('should not filter any job offers when requiredExperiences filter is empty', async () => {
          // given
          const expected: JobOfferInterface[] = [createPublishedJobOffer('Job 1'), createPublishedJobOffer('Job 2')];
          (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve(expected));

          const filter: GetPublishedJobOffersFilterInterface = { requiredExperiences: [] };

          // when
          await getPublishedJobOffers.execute(filter);

          // then
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
        });

        it('should filter job offers using requiredExperiences filter when not empty', async () => {
          // given
          const jobOffer1: JobOfferInterface = {
            title: 'Job 1',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            requiredExperiences: [],
          } as JobOfferInterface;
          const jobOffer2: JobOfferInterface = {
            title: 'Job 2',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            requiredExperiences: [
              JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS,
              JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
            ],
          } as JobOfferInterface;
          const jobOffer3: JobOfferInterface = {
            title: 'Job 3',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            requiredExperiences: [
              JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
              JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
            ],
          } as JobOfferInterface;
          const jobOffer4: JobOfferInterface = {
            title: 'Job 4',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            requiredExperiences: [
              JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
              JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
            ],
          } as JobOfferInterface;
          (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve([jobOffer1, jobOffer2, jobOffer3, jobOffer4]));

          const filter: GetPublishedJobOffersFilterInterface = {
            requiredExperiences: [
              JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS,
              JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
            ],
          };

          // when
          await getPublishedJobOffers.execute(filter);

          // then
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(1, jobOffer2, {}, false);
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(2, jobOffer3, {}, false);
        });
      });

      describe('regions', () => {
        it('should not filter any job offers when regions filter is null or undefined', async () => {
          // given
          const expected: JobOfferInterface[] = [createPublishedJobOffer('Job 1'), createPublishedJobOffer('Job 2')];
          (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve(expected));

          const filter: GetPublishedJobOffersFilterInterface = {};

          // when
          await getPublishedJobOffers.execute(filter);

          // then
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
        });

        it('should not filter any job offers when regions filter is empty', async () => {
          // given
          const expected: JobOfferInterface[] = [createPublishedJobOffer('Job 1'), createPublishedJobOffer('Job 2')];
          (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve(expected));

          const filter: GetPublishedJobOffersFilterInterface = { regions: [] };

          // when
          await getPublishedJobOffers.execute(filter);

          // then
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
        });

        it('should filter job offers using regions filter when not empty', async () => {
          // given
          const jobOffer1: JobOfferInterface = {
            title: 'Job 1',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            region: undefined,
          } as JobOfferInterface;
          const jobOffer2: JobOfferInterface = {
            title: 'Job 2',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            region: JobOfferRegion.REGION_CENTRE_DU_QUEBEC,
          } as JobOfferInterface;
          const jobOffer3: JobOfferInterface = {
            title: 'Job 3',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            region: JobOfferRegion.REGION_LAURENTIDES,
          } as JobOfferInterface;
          const jobOffer4: JobOfferInterface = {
            title: 'Job 4',
            publishedAt: fixedDate,
            status: JobOfferStatus.STATUS_PUBLISHED,
            region: JobOfferRegion.REGION_OUTAOUAIS,
          } as JobOfferInterface;
          (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve([jobOffer1, jobOffer2, jobOffer3, jobOffer4]));

          const filter: GetPublishedJobOffersFilterInterface = {
            regions: [JobOfferRegion.REGION_CENTRE_DU_QUEBEC, JobOfferRegion.REGION_LAURENTIDES],
          };

          // when
          await getPublishedJobOffers.execute(filter);

          // then
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledTimes(2);
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(1, jobOffer2, {}, false);
          expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenNthCalledWith(2, jobOffer3, {}, false);
        });
      });
    });
  });
  const createPublishedJobOffer = (title: string, companyId?: CompanyId): JobOfferInterface => {
    return { title, companyId, publishedAt: fixedDate, status: JobOfferStatus.STATUS_PUBLISHED } as JobOfferInterface;
  };
});

import { I18nMessagesInterface } from '../../domain/i18n/i18n-messages.interface';
import { I18nRepository } from '../../domain/i18n/i18n.repository.interface';
import { Lang } from '../../domain/type-aliases';
import { GetI18nMessagesForLang } from '../get-i18n-messages-for-lang';

describe('use_cases/GetI18nMessagesForLang', () => {
  let getI18nMessagesForLang: GetI18nMessagesForLang;
  let mockI18nRepository: I18nRepository;

  beforeEach(() => {
    mockI18nRepository = {} as I18nRepository;
    mockI18nRepository.findMessagesByLang = jest.fn();
    getI18nMessagesForLang = new GetI18nMessagesForLang(mockI18nRepository);
  });

  describe('execute()', () => {
    it('should call i18n repository with lang', () => {
      // given
      const lang: Lang = 'fr-ca';

      // when
      getI18nMessagesForLang.execute(lang);

      // then
      expect(mockI18nRepository.findMessagesByLang).toHaveBeenCalledWith(lang);
    });

    it('should return I18nMessages from repository', async () => {
      // given
      const expected: I18nMessagesInterface = {
        welcome: 'A welcome message',
        thanks: {
          you: 'A thank you message',
        },
      } as I18nMessagesInterface;

      (mockI18nRepository.findMessagesByLang as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: I18nMessagesInterface = await getI18nMessagesForLang.execute('fr-ca');

      // then
      expect(result).toStrictEqual(expected);
    });
  });
});

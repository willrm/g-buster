import { ItemNotFoundError } from '../../domain/common-errors/item-not-found.error';
import { JobOfferStatus } from '../../domain/job-offer/job-offer-enums';
import { JobOfferInterface } from '../../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../../domain/job-offer/job-offer.repository.interface';
import { JobOfferId } from '../../domain/type-aliases';
import { UserInterface } from '../../domain/user/user.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { GetUserJobOfferById } from '../get-user-job-offer-by-id';

describe('use_cases/GetUserJobOfferById', () => {
  let getJobOfferById: GetUserJobOfferById;
  let mockUserRepository: UserRepository;
  let mockJobOfferRepository: JobOfferRepository;

  beforeEach(() => {
    mockUserRepository = {} as UserRepository;
    mockUserRepository.getCurrent = jest.fn();

    mockJobOfferRepository = {} as JobOfferRepository;
    mockJobOfferRepository.findById = jest.fn();

    getJobOfferById = new GetUserJobOfferById(mockUserRepository, mockJobOfferRepository);
  });

  describe('execute()', () => {
    it('should call job offer repository with id', async () => {
      // given
      const id: JobOfferId = '42';
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve({ status: JobOfferStatus.STATUS_DRAFT } as JobOfferInterface));
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve({} as UserInterface));

      // when
      await getJobOfferById.execute(id);

      // then
      expect(mockJobOfferRepository.findById).toHaveBeenCalledWith(id);
    });

    it('should return found job offer from repository when found job offer is draft and user id matches with current user id', async () => {
      // given
      const id: JobOfferId = '42';
      const jobOffer: JobOfferInterface = {
        id,
        userId: '1337',
        title: 'A job offer title',
        status: JobOfferStatus.STATUS_DRAFT,
      } as JobOfferInterface;
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(jobOffer));

      const currentUser: UserInterface = { id: '1337' } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

      // when
      const result: JobOfferInterface = await getJobOfferById.execute(id);

      // then
      expect(result).toStrictEqual(jobOffer);
    });

    it('should return not found error when found job offer is not draft', async () => {
      // given
      const jobOffer: JobOfferInterface = {
        userId: '1337',
        title: 'A job offer title',
        status: JobOfferStatus.STATUS_PUBLISHED,
      } as JobOfferInterface;
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(jobOffer));

      const currentUser: UserInterface = { id: '1337' } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

      // when
      const result: Promise<JobOfferInterface> = getJobOfferById.execute('42');

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('JobOffer not found'));
    });

    it('should return not found error when found job offer user id does not match with current user id', async () => {
      // given
      const jobOffer: JobOfferInterface = { userId: '1337', title: 'A job offer title', status: JobOfferStatus.STATUS_DRAFT } as JobOfferInterface;
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(jobOffer));

      const currentUser: UserInterface = { id: '1336' } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

      // when
      const result: Promise<JobOfferInterface> = getJobOfferById.execute('42');

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('JobOffer not found'));
    });
  });
});

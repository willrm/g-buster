import { DummyPage } from '../../domain/dummy-page/dummy-page';
import { DummyPageInterface } from '../../domain/dummy-page/dummy-page.interface';
import { DummyPageRepository } from '../../domain/dummy-page/dummy-page.repository.interface';
import { GetDummyPage } from '../get-dummy-page';

describe('use_cases/GetDummyPage', () => {
  let getDummyPage: GetDummyPage;
  let mockDummyPageRepository: DummyPageRepository;

  beforeEach(() => {
    mockDummyPageRepository = {} as DummyPageRepository;
    mockDummyPageRepository.findByLangAndUid = jest.fn();
    getDummyPage = new GetDummyPage(mockDummyPageRepository);
  });
  describe('execute()', () => {
    it('should call content repository with lang and uid', () => {
      // given
      const lang: string = 'a-lang';
      const uid: string = 'dummy-page-uid';

      // when
      getDummyPage.execute(lang, uid);

      // then
      expect(mockDummyPageRepository.findByLangAndUid).toHaveBeenCalledWith(lang, uid);
    });

    it('should return DummyPage from repository', async () => {
      // given
      const expected: DummyPage = {
        title: 'Dummy title',
        content: 'Dummy content',
        image: {
          source: 'http://dummy.jpg',
          alternativeText: 'My dummy image',
        },
        generatedDate: new Date(),
      } as DummyPage;

      (mockDummyPageRepository.findByLangAndUid as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: DummyPageInterface = await getDummyPage.execute('any-lang', 'any-uid');

      // then
      expect(result).toBe(expected);
    });
  });
});

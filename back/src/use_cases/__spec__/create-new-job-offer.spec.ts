import { CompanyInterface } from '../../domain/company/company.interface';
import { CompanyRepository } from '../../domain/company/company.repository.interface';
import { JobOffer, JobOfferFactoryInterface } from '../../domain/job-offer/job-offer';
import { JobOfferRawInterface } from '../../domain/job-offer/job-offer.raw.interface';
import { JobOfferRepository } from '../../domain/job-offer/job-offer.repository.interface';
import { JobOfferId } from '../../domain/type-aliases';
import { UserInterface } from '../../domain/user/user.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { CreateNewJobOffer } from '../create-new-job-offer';

jest.mock('../../domain/job-offer/job-offer');

describe('use_cases/CreateNewJobOffer', () => {
  let createNewJobOffer: CreateNewJobOffer;
  let mockUserRepository: UserRepository;
  let mockJobOfferRepository: JobOfferRepository;
  let mockCompanyRepository: CompanyRepository;
  let jobOfferRaw: JobOfferRawInterface;

  beforeEach(() => {
    JobOffer.factory = {
      copy: jest.fn(),
      createDraftJobOffer: jest.fn(),
      validateAndCreateNewJobOffer: jest.fn(),
    } as JobOfferFactoryInterface;

    mockUserRepository = {} as UserRepository;
    mockUserRepository.getCurrent = jest.fn();

    mockJobOfferRepository = {} as JobOfferRepository;
    mockJobOfferRepository.save = jest.fn();

    mockCompanyRepository = {} as CompanyRepository;
    mockCompanyRepository.findByUserId = jest.fn();

    createNewJobOffer = new CreateNewJobOffer(mockUserRepository, mockJobOfferRepository, mockCompanyRepository);

    jobOfferRaw = {} as JobOfferRawInterface;
  });

  describe('execute()', () => {
    it('should find company using user id', async () => {
      // given
      const currentUser: UserInterface = { id: '42' } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

      // when
      await createNewJobOffer.execute(jobOfferRaw);

      // then
      expect(mockCompanyRepository.findByUserId).toHaveBeenCalledWith('42');
    });

    describe('draft', () => {
      it('should create a draft JobOffer with current user, company and raw data if draft is true', async () => {
        // given
        jobOfferRaw.title = 'A job title';
        jobOfferRaw.draft = true;

        const currentUser: UserInterface = { firstName: 'Test' } as UserInterface;
        (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

        const company: CompanyInterface = { name: 'Company name' } as CompanyInterface;
        (mockCompanyRepository.findByUserId as jest.Mock).mockReturnValue(Promise.resolve(company));

        // when
        await createNewJobOffer.execute(jobOfferRaw);

        // then
        expect(JobOffer.factory.createDraftJobOffer as jest.Mock).toHaveBeenCalledWith(jobOfferRaw, currentUser, company);
        expect(JobOffer.factory.validateAndCreateNewJobOffer as jest.Mock).not.toHaveBeenCalled();
      });

      it('should validate and create a new JobOffer with current user, company and raw data if draft is false', async () => {
        // given
        jobOfferRaw.title = 'A job title';
        jobOfferRaw.draft = false;

        const currentUser: UserInterface = { firstName: 'Test' } as UserInterface;
        (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

        const company: CompanyInterface = { name: 'Company name' } as CompanyInterface;
        (mockCompanyRepository.findByUserId as jest.Mock).mockReturnValue(Promise.resolve(company));

        // when
        await createNewJobOffer.execute(jobOfferRaw);

        // then
        expect(JobOffer.factory.createDraftJobOffer as jest.Mock).not.toHaveBeenCalled();
        expect(JobOffer.factory.validateAndCreateNewJobOffer as jest.Mock).toHaveBeenCalledWith(jobOfferRaw, currentUser, company);
      });
    });

    it('should save new JobOffer using repository', async () => {
      // given
      const jobOffer: JobOffer = { title: 'A job title' } as JobOffer;
      (JobOffer.factory.validateAndCreateNewJobOffer as jest.Mock).mockImplementation(() => jobOffer);

      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve({} as UserInterface));

      // when
      await createNewJobOffer.execute(jobOfferRaw);

      // then
      expect(mockJobOfferRepository.save).toHaveBeenCalledWith(jobOffer);
    });

    it('should return saved JobOffer id', async () => {
      // given
      const savedJobOfferId: JobOfferId = '123456789';
      (mockJobOfferRepository.save as jest.Mock).mockReturnValue(Promise.resolve(savedJobOfferId));

      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve({} as UserInterface));

      // when
      const result: JobOfferId = await createNewJobOffer.execute(jobOfferRaw);

      // then
      expect(result).toBe(savedJobOfferId);
    });
  });
});

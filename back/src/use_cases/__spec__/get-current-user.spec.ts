import { UserRole } from '../../domain/user/user-enums';
import { UserInterface } from '../../domain/user/user.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { GetCurrentUser } from '../get-current-user';

describe('use_cases/GetCurrentUser', () => {
  let getCurrentUser: GetCurrentUser;
  let mockUserRepository: UserRepository;

  beforeEach(() => {
    mockUserRepository = {} as UserRepository;
    mockUserRepository.getCurrent = jest.fn();

    getCurrentUser = new GetCurrentUser(mockUserRepository);
  });

  describe('execute()', () => {
    it('should return current user from repository', async () => {
      // given
      const expected: UserInterface = {
        id: '123456789',
        firstName: 'Test',
        role: UserRole.CANDIDATE,
      } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: UserInterface = await getCurrentUser.execute();

      // then
      expect(result).toBe(expected);
    });
  });
});

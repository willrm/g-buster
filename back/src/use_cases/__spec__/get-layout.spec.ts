import { FooterInterface } from '../../domain/footer/footer.interface';
import { FooterRepository } from '../../domain/footer/footer.repository.interface';
import { HeaderInterface } from '../../domain/header/header.interface';
import { HeaderRepository } from '../../domain/header/header.repository.interface';
import { ImageInterface } from '../../domain/image/image.interface';
import { Layout } from '../../domain/layout/layout';
import { GetLayout } from '../get-layout';

jest.mock('../../domain/layout/layout');

describe('use_cases/GetLayout', () => {
  let getLayout: GetLayout;
  let mockHeaderRepository: HeaderRepository;
  let mockFooterRepository: FooterRepository;

  beforeEach(() => {
    (Layout as jest.Mock).mockClear();

    mockHeaderRepository = {} as HeaderRepository;
    mockHeaderRepository.findByLang = jest.fn();
    mockFooterRepository = {} as FooterRepository;
    mockFooterRepository.findByLang = jest.fn();
    getLayout = new GetLayout(mockHeaderRepository, mockFooterRepository);
  });

  describe('execute()', () => {
    it('should call content repository to get header', async () => {
      // given
      const lang: string = 'a-lang';

      // when
      await getLayout.execute(lang);

      // then
      expect(mockHeaderRepository.findByLang).toHaveBeenCalledWith(lang);
    });
    it('should call content repository to get footer', async () => {
      // given
      const lang: string = 'a-lang';

      // when
      await getLayout.execute(lang);

      // then
      expect(mockFooterRepository.findByLang).toHaveBeenCalledWith(lang);
    });
    it('should return layout built using header and footer', async () => {
      // given
      const header: HeaderInterface = {
        title: 'Header title',
        logo: {} as ImageInterface,
      };
      const footer: FooterInterface = { copyright: 'Copyright 2049' };

      const expected: Layout = {
        header,
        footer,
      } as Layout;
      (Layout as jest.Mock).mockImplementation(() => expected);

      (mockHeaderRepository.findByLang as jest.Mock).mockReturnValue(Promise.resolve(header));
      (mockFooterRepository.findByLang as jest.Mock).mockReturnValue(Promise.resolve(footer));

      // when
      const result: Layout = await getLayout.execute('a-lang');

      // then
      expect(result).toBe(expected);
      expect(Layout as jest.Mock).toHaveBeenCalledWith(header, footer);
    });
  });
});

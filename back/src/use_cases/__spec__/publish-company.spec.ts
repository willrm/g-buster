import { Company, CompanyFactoryInterface } from '../../domain/company/company';
import { CompanyInterface } from '../../domain/company/company.interface';
import { CompanyRepository } from '../../domain/company/company.repository.interface';
import { CompanyId } from '../../domain/type-aliases';
import { PublishCompany } from '../publish-company';

describe('use_cases/publishCompany', () => {
  const id: CompanyId = 'companyId';

  let publishCompany: PublishCompany;
  let mockCompanyRepository: CompanyRepository;
  let companyToPublish: Company;

  beforeEach(() => {
    mockCompanyRepository = {} as CompanyRepository;
    mockCompanyRepository.findById = jest.fn();
    mockCompanyRepository.save = jest.fn();

    publishCompany = new PublishCompany(mockCompanyRepository);

    Company.factory = {
      copy: jest.fn(),
      validateAndCreateNewCompany: jest.fn(),
    } as CompanyFactoryInterface;

    companyToPublish = {
      id: 'companyId',
    } as Company;
    companyToPublish.publish = jest.fn();

    (mockCompanyRepository.findById as jest.Mock).mockReturnValue(Promise.resolve({} as CompanyInterface));
    (Company.factory.copy as jest.Mock).mockImplementation(() => companyToPublish);
  });

  describe('execute()', () => {
    it('should find existing company for given id', async () => {
      // when
      await publishCompany.execute(id);

      // then
      expect(mockCompanyRepository.findById).toHaveBeenCalledWith(id);
    });

    it('should call company publish method', async () => {
      // when
      await publishCompany.execute(id);

      // then
      expect(companyToPublish.publish as jest.Mock).toHaveBeenCalled();
    });
    it('should call repository to save company', async () => {
      // when
      await publishCompany.execute(id);

      // then
      expect(mockCompanyRepository.save).toHaveBeenCalledWith(companyToPublish);
    });
  });
});

import { ItemNotFoundError } from '../../domain/common-errors/item-not-found.error';
import { JobOffer, JobOfferFactoryInterface } from '../../domain/job-offer/job-offer';
import { JobOfferInterface } from '../../domain/job-offer/job-offer.interface';
import { JobOfferRawInterface } from '../../domain/job-offer/job-offer.raw.interface';
import { JobOfferRepository } from '../../domain/job-offer/job-offer.repository.interface';
import { JobOfferId } from '../../domain/type-aliases';
import { UserInterface } from '../../domain/user/user.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { UpdateDraftJobOffer } from '../update-draft-job-offer';

jest.mock('../../domain/job-offer/job-offer');

describe('use_cases/UpdateDraftJobOffer', () => {
  let updateJobOffer: UpdateDraftJobOffer;
  let mockUserRepository: UserRepository;
  let mockJobOfferRepository: JobOfferRepository;
  let jobOfferRaw: JobOfferRawInterface;

  beforeEach(() => {
    JobOffer.factory = {
      copy: jest.fn(),
      createDraftJobOffer: jest.fn(),
      validateAndCreateNewJobOffer: jest.fn(),
    } as JobOfferFactoryInterface;

    mockUserRepository = {} as UserRepository;
    mockUserRepository.getCurrent = jest.fn();

    mockJobOfferRepository = {} as JobOfferRepository;
    mockJobOfferRepository.findById = jest.fn();
    mockJobOfferRepository.save = jest.fn();

    updateJobOffer = new UpdateDraftJobOffer(mockUserRepository, mockJobOfferRepository);

    jobOfferRaw = {} as JobOfferRawInterface;
    (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve({} as UserInterface));
    (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve({} as JobOfferInterface));
  });

  describe('execute()', () => {
    it('should find existing JobOffer with given id', async () => {
      // given
      const id: JobOfferId = 'aJobOfferId';
      const jobOfferToUpdate: JobOffer = { id } as JobOffer;
      jobOfferToUpdate.updateWith = jest.fn();
      (JobOffer.factory.copy as jest.Mock).mockImplementation(() => jobOfferToUpdate);

      // when
      await updateJobOffer.execute(id, jobOfferRaw);

      // then
      expect(mockJobOfferRepository.findById).toHaveBeenCalledWith(id);
    });

    it('should update existing JobOffer with raw data', async () => {
      // given
      const id: JobOfferId = 'aJobOfferId';
      jobOfferRaw.title = 'New title';

      const foundJobOffer: JobOfferInterface = { id } as JobOfferInterface;
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(foundJobOffer));

      const jobOfferToUpdate: JobOffer = { id } as JobOffer;
      jobOfferToUpdate.updateWith = jest.fn();
      (JobOffer.factory.copy as jest.Mock).mockImplementation(() => jobOfferToUpdate);

      // when
      await updateJobOffer.execute(id, jobOfferRaw);

      // then
      expect(JobOffer.factory.copy as jest.Mock).toHaveBeenCalledWith(foundJobOffer);
      expect(jobOfferToUpdate.updateWith as jest.Mock).toHaveBeenCalledWith(jobOfferRaw);
    });

    it('should save updated JobOffer using repository', async () => {
      // given
      const id: JobOfferId = 'aJobOfferId';
      const jobOfferToUpdate: JobOffer = { id } as JobOffer;
      jobOfferToUpdate.updateWith = () => {
        jobOfferToUpdate.title = 'New title';
      };
      (JobOffer.factory.copy as jest.Mock).mockImplementation(() => jobOfferToUpdate);

      // when
      await updateJobOffer.execute(id, jobOfferRaw);

      // then
      expect((mockJobOfferRepository.save as jest.Mock).mock.calls[0][0]).toMatchObject({ id: 'aJobOfferId', title: 'New title' });
    });

    it('should return saved JobOffer id', async () => {
      // given
      const id: JobOfferId = '123456789';
      const jobOfferToUpdate: JobOffer = { id } as JobOffer;
      jobOfferToUpdate.updateWith = jest.fn();
      (JobOffer.factory.copy as jest.Mock).mockImplementation(() => jobOfferToUpdate);
      (mockJobOfferRepository.save as jest.Mock).mockReturnValue(Promise.resolve(id));

      // when
      const result: JobOfferId = await updateJobOffer.execute(id, jobOfferRaw);

      // then
      expect(result).toBe(id);
    });

    it('should return not found error when found job offer user id does not match with current user id', async () => {
      // given
      const jobOffer: JobOfferInterface = { userId: '1337', title: 'A job offer title' } as JobOfferInterface;
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(jobOffer));

      const currentUser: UserInterface = { id: '1336' } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

      // when
      const result: Promise<JobOfferId> = updateJobOffer.execute('42', jobOfferRaw);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('JobOffer not found'));
    });
  });
});

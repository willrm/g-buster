import { DuplicateItemError } from '../../domain/common-errors/duplicate-item.error';
import { ItemNotFoundError } from '../../domain/common-errors/item-not-found.error';
import { Company, CompanyFactoryInterface } from '../../domain/company/company';
import { CompanyInterface } from '../../domain/company/company.interface';
import { CompanyRawInterface } from '../../domain/company/company.raw.interface';
import { CompanyRepository } from '../../domain/company/company.repository.interface';
import { TemporaryFileInterface } from '../../domain/temporary-file/temporary-file.interface';
import { TemporaryFileRepository } from '../../domain/temporary-file/temporary-file.repository';
import { CompanyId } from '../../domain/type-aliases';
import { UserInterface } from '../../domain/user/user.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { CreateNewCompany } from '../create-new-company';

jest.mock('../../domain/company/company');

describe('use_cases/CreateNewCompany', () => {
  let createNewCompany: CreateNewCompany;
  let mockUserRepository: UserRepository;
  let mockCompanyRepository: CompanyRepository;
  let mockTemporaryFileRepository: TemporaryFileRepository;
  let companyRaw: CompanyRawInterface;

  beforeEach(() => {
    Company.factory = {
      copy: jest.fn(),
      validateAndCreateNewCompany: jest.fn(),
    } as CompanyFactoryInterface;

    companyRaw = {} as CompanyRawInterface;

    mockUserRepository = {} as UserRepository;
    mockUserRepository.getCurrent = jest.fn();

    mockCompanyRepository = {} as CompanyRepository;
    mockCompanyRepository.save = jest.fn();
    mockCompanyRepository.findByUserId = jest.fn();

    mockTemporaryFileRepository = {} as TemporaryFileRepository;
    mockTemporaryFileRepository.findByUuid = jest.fn();
    createNewCompany = new CreateNewCompany(mockUserRepository, mockCompanyRepository, mockTemporaryFileRepository);
  });

  describe('execute()', () => {
    it('should call temporary file repository to find temporary file by uuid', async () => {
      // given
      companyRaw.logoUuid = 'a-temporary-file-uuid';

      // when
      await createNewCompany.execute(companyRaw);

      // then
      expect(mockTemporaryFileRepository.findByUuid).toHaveBeenCalledWith('a-temporary-file-uuid');
    });

    it('should instantiate a new Company from current user, raw data and found temporary file', async () => {
      // given
      companyRaw.name = 'A company name';
      const temporaryFile: TemporaryFileInterface = { base64: 'an-image-as-base64' } as TemporaryFileInterface;
      (mockTemporaryFileRepository.findByUuid as jest.Mock).mockReturnValue(Promise.resolve(temporaryFile));

      const currentUser: UserInterface = { firstName: 'Test' } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

      // when
      await createNewCompany.execute(companyRaw);

      // then
      expect(Company.factory.validateAndCreateNewCompany as jest.Mock).toHaveBeenCalledWith(currentUser, companyRaw, temporaryFile);
    });

    it('should instantiate a new Company with null temporary file when not found', async () => {
      // given
      (mockTemporaryFileRepository.findByUuid as jest.Mock).mockReturnValue(Promise.reject(new ItemNotFoundError('TemporaryFile not found')));

      const currentUser: UserInterface = { firstName: 'Test' } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

      // when
      await createNewCompany.execute(companyRaw);

      // then
      expect(Company.factory.validateAndCreateNewCompany as jest.Mock).toHaveBeenCalledWith(currentUser, companyRaw, null);
    });

    it('should throw exception when a company already exists for user', async () => {
      // given
      (mockCompanyRepository.findByUserId as jest.Mock).mockReturnValue(Promise.resolve({ name: 'existing company' } as CompanyInterface));

      const currentUser: UserInterface = { id: '1337' } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

      // when
      const result: Promise<CompanyId> = createNewCompany.execute(companyRaw);

      // then
      await expect(result).rejects.toThrow(new DuplicateItemError('Company already exists for user'));
      expect(mockCompanyRepository.findByUserId).toHaveBeenCalledWith('1337');
    });

    it('should not throw any exception when no company already exists for user', async () => {
      // given
      (mockTemporaryFileRepository.findByUuid as jest.Mock).mockReturnValue(Promise.reject(new ItemNotFoundError('Company not found')));

      // when
      await createNewCompany.execute(companyRaw);

      // then
      expect(Company.factory.validateAndCreateNewCompany as jest.Mock).toHaveBeenCalled();
    });

    it('should call company repository to create a company', async () => {
      // given
      const expected: Company = { id: null, name: 'A company name' } as Company;
      (Company.factory.validateAndCreateNewCompany as jest.Mock).mockImplementation(() => expected);

      // when
      await createNewCompany.execute(companyRaw);

      // then
      expect(mockCompanyRepository.save).toHaveBeenCalledWith(expected);
    });

    it('should return saved company id', async () => {
      // given
      const expected: CompanyId = '12';
      (mockCompanyRepository.save as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: CompanyId = await createNewCompany.execute(companyRaw);

      // then
      expect(result).toBe(expected);
    });
  });
});

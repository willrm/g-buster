import { Company, CompanyFactoryInterface } from '../../domain/company/company';
import { CompanyInterface } from '../../domain/company/company.interface';
import { CompanyRawInterface } from '../../domain/company/company.raw.interface';
import { CompanyRepository } from '../../domain/company/company.repository.interface';
import { LogoInterface } from '../../domain/logo/logo.interface';
import { TemporaryFileRepository } from '../../domain/temporary-file/temporary-file.repository';
import { CompanyId } from '../../domain/type-aliases';
import { UpdateUnpublishedCompany } from '../update-unpublished-company';

describe('use_cases/UpdateUnpublishedCompany', () => {
  let updateCompany: UpdateUnpublishedCompany;
  let mockCompanyRepository: CompanyRepository;
  let mockTemporaryFileRepository: TemporaryFileRepository;

  let companyRaw: CompanyRawInterface;

  const logoToUpdate: LogoInterface = { base64: 'base64', mimeType: 'mimeType' } as LogoInterface;

  beforeEach(() => {
    Company.factory = {
      copy: jest.fn(),
      validateAndCreateNewCompany: jest.fn(),
    } as CompanyFactoryInterface;

    mockCompanyRepository = {} as CompanyRepository;
    mockCompanyRepository.findById = jest.fn();
    mockCompanyRepository.save = jest.fn();

    mockTemporaryFileRepository = {} as TemporaryFileRepository;
    mockTemporaryFileRepository.findByUuid = jest.fn();

    updateCompany = new UpdateUnpublishedCompany(mockCompanyRepository, mockTemporaryFileRepository);

    companyRaw = {} as CompanyRawInterface;
    (mockCompanyRepository.findById as jest.Mock).mockReturnValue(Promise.resolve({} as CompanyInterface));
    (mockTemporaryFileRepository.findByUuid as jest.Mock).mockReturnValue(Promise.resolve(logoToUpdate));
  });

  describe('execute()', () => {
    it('should find existing company for given id', async () => {
      // given
      const id: CompanyId = 'companyId';
      const companyToUpdate: Company = { id } as Company;
      companyToUpdate.updateWith = jest.fn();

      (Company.factory.copy as jest.Mock).mockImplementation(() => companyToUpdate);

      // when
      await updateCompany.execute(id, companyRaw);

      // then
      expect(mockCompanyRepository.findById).toHaveBeenCalledWith(id);
    });

    it('should call temporary file repository to find temporary file by uuid', async () => {
      // given
      const id: CompanyId = 'companyId';
      companyRaw.logoUuid = 'a-temporary-file-uuid';

      const companyToUpdate: Company = { id } as Company;
      companyToUpdate.updateWith = jest.fn();
      companyToUpdate.updateLogo = jest.fn();
      (Company.factory.copy as jest.Mock).mockImplementation(() => companyToUpdate);

      // when
      await updateCompany.execute(id, companyRaw);

      // then
      expect(mockTemporaryFileRepository.findByUuid).toHaveBeenCalledWith('a-temporary-file-uuid');
    });

    it('should update existing Company with raw data', async () => {
      // given
      const id: CompanyId = 'companyId';
      companyRaw.name = 'New name';

      const foundCompany: CompanyInterface = { id } as CompanyInterface;
      (mockCompanyRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(foundCompany));

      const companyToUpdate: Company = { id } as Company;
      companyToUpdate.updateWith = jest.fn();
      (Company.factory.copy as jest.Mock).mockImplementation(() => companyToUpdate);

      // when
      await updateCompany.execute(id, companyRaw);

      // then
      expect(Company.factory.copy as jest.Mock).toHaveBeenCalledWith(foundCompany);
      expect(companyToUpdate.updateWith as jest.Mock).toHaveBeenCalledWith(companyRaw);
    });

    it('should update company logo when raw contains logo uuid', async () => {
      // given
      const id: CompanyId = 'companyId';
      companyRaw.logoUuid = 'SomeUUID';

      const companyToUpdate: Company = { id } as Company;
      companyToUpdate.updateLogo = jest.fn();
      companyToUpdate.updateWith = jest.fn();
      (Company.factory.copy as jest.Mock).mockImplementation(() => companyToUpdate);

      // when
      await updateCompany.execute(id, companyRaw);

      // then
      expect(companyToUpdate.updateLogo as jest.Mock).toHaveBeenCalled();
    });
  });
});

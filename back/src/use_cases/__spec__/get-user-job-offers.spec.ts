import { JobOfferInterface } from '../../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../../domain/job-offer/job-offer.repository.interface';
import { UserInterface } from '../../domain/user/user.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { GetUserJobOffers } from '../get-user-job-offers';

describe('use_cases/GetUserJobOffers', () => {
  let getUserJobOffers: GetUserJobOffers;
  let mockUserRepository: UserRepository;
  let mockJobOfferRepository: JobOfferRepository;

  beforeEach(() => {
    mockUserRepository = {} as UserRepository;
    mockUserRepository.getCurrent = jest.fn();

    mockJobOfferRepository = {} as JobOfferRepository;
    mockJobOfferRepository.findAllByUserId = jest.fn();

    getUserJobOffers = new GetUserJobOffers(mockUserRepository, mockJobOfferRepository);

    (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve({} as UserInterface));
  });

  describe('execute()', () => {
    it('should find job offers having user id of current user', async () => {
      // given
      const currentUser: UserInterface = { id: '1337', firstName: 'Test' } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

      // when
      await getUserJobOffers.execute();

      // then
      expect(mockJobOfferRepository.findAllByUserId).toHaveBeenCalledWith('1337');
    });

    it('should return found job offers from repository', async () => {
      // given
      const jobOffers: JobOfferInterface[] = [
        { title: 'An awesome job' } as JobOfferInterface,
        { title: 'Another awesome job' } as JobOfferInterface,
      ];
      (mockJobOfferRepository.findAllByUserId as jest.Mock).mockReturnValue(Promise.resolve(jobOffers));

      // when
      const result: JobOfferInterface[] = await getUserJobOffers.execute();

      // then
      expect(result).toBe(jobOffers);
    });
  });
});

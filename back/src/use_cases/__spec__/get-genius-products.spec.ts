import { GeniusProduct } from '../../domain/genius-product/genius-product';
import { GeniusProductInterface } from '../../domain/genius-product/genius-product.interface';
import { GeniusProductRawInterface } from '../../domain/genius-product/genius-product.raw.interface';
import { GeniusProductRepository } from '../../domain/genius-product/genius-product.repository.interface';
import { GetGeniusProducts } from '../get-genius-products';

jest.mock('../../domain/genius-product/genius-product');

describe('use_cases/GetGeniusProducts', () => {
  let getGeniusProducts: GetGeniusProducts;
  let mockGeniusProductRepository: GeniusProductRepository;
  let geniusProductRaw: GeniusProductRawInterface;

  beforeEach(() => {
    GeniusProduct.factory = {
      create: jest.fn(),
    };

    mockGeniusProductRepository = {} as GeniusProductRepository;
    mockGeniusProductRepository.findAll = jest.fn();

    getGeniusProducts = new GetGeniusProducts(mockGeniusProductRepository);

    geniusProductRaw = {} as GeniusProductRawInterface;
  });

  describe('execute()', () => {
    it('should call genius product repository', () => {
      // given
      (mockGeniusProductRepository.findAll as jest.Mock).mockReturnValue(Promise.resolve([]));

      // when
      getGeniusProducts.execute();

      // then
      expect(mockGeniusProductRepository.findAll).toHaveBeenCalled();
    });

    it('should create new GeniusProducts for each item returned by repository', async () => {
      // given
      geniusProductRaw = {
        id: 'an-id',
        name: 'A genius product',
        price: 12,
      } as GeniusProductRawInterface;

      const geniusProductsRaw: GeniusProductRawInterface[] = [geniusProductRaw, geniusProductRaw];

      (mockGeniusProductRepository.findAll as jest.Mock).mockReturnValue(Promise.resolve(geniusProductsRaw));

      // when
      await getGeniusProducts.execute();

      // then
      expect(GeniusProduct.factory.create as jest.Mock).toHaveBeenCalledTimes(2);
      expect(GeniusProduct.factory.create as jest.Mock).toHaveBeenNthCalledWith(1, geniusProductRaw);
      expect(GeniusProduct.factory.create as jest.Mock).toHaveBeenNthCalledWith(2, geniusProductRaw);
    });

    it('should return GeniusProducts from repository', async () => {
      // given
      const geniusProduct: GeniusProductInterface = {
        id: 'an-id',
        name: 'A genius product',
        price: 12,
      } as GeniusProductInterface;

      const expected: GeniusProductInterface[] = [geniusProduct];

      (mockGeniusProductRepository.findAll as jest.Mock).mockReturnValue(Promise.resolve([{}]));
      (GeniusProduct.factory.create as jest.Mock).mockReturnValue(geniusProduct);

      // when
      const result: GeniusProductInterface[] = await getGeniusProducts.execute();

      // then
      expect(result).toStrictEqual(expected);
    });
  });
});

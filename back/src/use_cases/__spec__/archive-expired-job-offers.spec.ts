import { JobOfferStatus } from '../../domain/job-offer/job-offer-enums';
import { JobOfferInterface } from '../../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../../domain/job-offer/job-offer.repository.interface';
import { JobOfferId } from '../../domain/type-aliases';
import { ArchiveExpiredJobOffers } from '../archive-expired-job-offers';

describe('use_cases/ArchiveExpiredJobOffers', () => {
  let archiveExpiredJobOffers: ArchiveExpiredJobOffers;
  let mockJobOfferRepository: JobOfferRepository;
  let realDateConstructor: DateConstructor;

  beforeAll(() => {
    realDateConstructor = Date;
  });

  beforeEach(() => {
    global.Date = realDateConstructor;

    mockJobOfferRepository = {} as JobOfferRepository;
    mockJobOfferRepository.findAllByStatus = jest.fn();
    mockJobOfferRepository.save = jest.fn();
    archiveExpiredJobOffers = new ArchiveExpiredJobOffers(mockJobOfferRepository);

    (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve([]));
  });

  describe('execute()', () => {
    it('should find job offers with published status', async () => {
      // given

      // when
      await archiveExpiredJobOffers.execute();

      // then
      expect(mockJobOfferRepository.findAllByStatus).toHaveBeenCalledWith(JobOfferStatus.STATUS_PUBLISHED);
    });

    it('should archive and save job offers published more than 30 days ago', async () => {
      // given
      const fixedDate: Date = new Date('2019-03-15T16:35:20');
      const fixedDateThirtyDaysBefore: Date = new Date('2019-02-13T16:35:20');
      const fixedDateThirtyOneDaysBefore: Date = new Date('2019-02-12T16:35:20');

      const jobOffers: JobOfferInterface[] = [
        buildJobOffer('1', 'job offer 1', fixedDate),
        buildJobOffer('2', 'job offer 2', fixedDateThirtyDaysBefore),
        buildJobOffer('3', 'job offer 3', fixedDateThirtyOneDaysBefore),
      ];
      (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve(jobOffers));

      // @ts-ignore
      jest.spyOn(global, 'Date').mockImplementationOnce(() => fixedDate);

      // when
      await archiveExpiredJobOffers.execute();

      // then
      expect(mockJobOfferRepository.save).toHaveBeenCalledTimes(1);
      expect((mockJobOfferRepository.save as jest.Mock).mock.calls[0][0]).toMatchObject({
        id: '3',
        status: JobOfferStatus.STATUS_ARCHIVED,
        title: 'job offer 3',
      });
    });

    it('should return the ids of job offers archived', async () => {
      // given
      const fixedDate: Date = new Date('2019-03-15T16:35:20');
      const fixedDateThirtyOneDaysBefore: Date = new Date('2019-02-12T16:35:20');

      const jobOffers: JobOfferInterface[] = [
        buildJobOffer('1', 'job offer 1', fixedDateThirtyOneDaysBefore),
        buildJobOffer('2', 'job offer 2', fixedDateThirtyOneDaysBefore),
      ];
      (mockJobOfferRepository.findAllByStatus as jest.Mock).mockReturnValue(Promise.resolve(jobOffers));

      // @ts-ignore
      jest.spyOn(global, 'Date').mockImplementationOnce(() => fixedDate);

      // when
      const result: JobOfferId[] = await archiveExpiredJobOffers.execute();

      // then
      expect(result).toStrictEqual(['1', '2']);
    });
  });

  function buildJobOffer(id: string, title: string, publishedAt: Date): JobOfferInterface {
    return {
      id,
      title,
      publishedAt,
      city: '',
      description: '',
      emailAddressForReceiptOfApplications: '',
      jobStatus: undefined,
      jobType: undefined,
      positionType: undefined,
      region: undefined,
      requestedSpecialities: [],
      requiredExperiences: [],
      startDateOfEmployment: undefined,
      status: undefined,
      targetCustomers: [],
      userId: undefined,
      companyId: undefined,
    };
  }
});

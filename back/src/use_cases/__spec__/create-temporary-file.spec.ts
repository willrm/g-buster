import { TemporaryFile } from '../../domain/temporary-file/temporary-file';
import { TemporaryFileInterface } from '../../domain/temporary-file/temporary-file.interface';
import { TemporaryFileRawInterface } from '../../domain/temporary-file/temporary-file.raw.interface';
import { TemporaryFileRepository } from '../../domain/temporary-file/temporary-file.repository';
import { CreateTemporaryFile } from '../create-temporary-file';

jest.mock('../../domain/temporary-file/temporary-file');

describe('use_cases/CreateTemporaryFile', () => {
  let createTemporaryFile: CreateTemporaryFile;
  let mockTemporaryFileRepository: TemporaryFileRepository;

  beforeEach(() => {
    (TemporaryFile as jest.Mock).mockClear();

    mockTemporaryFileRepository = {} as TemporaryFileRepository;
    mockTemporaryFileRepository.save = jest.fn();
    createTemporaryFile = new CreateTemporaryFile(mockTemporaryFileRepository);
  });

  describe('execute()', () => {
    it('should instantiate a new TemporaryFile from raw data', () => {
      // given
      const temporaryFileRaw: TemporaryFileRawInterface = { mimeType: 'image/whatever' } as TemporaryFileRawInterface;

      // when
      createTemporaryFile.execute(temporaryFileRaw);

      // then
      expect(TemporaryFile as jest.Mock).toHaveBeenCalledWith(temporaryFileRaw);
    });

    it('should call temporary file repository to create an temporary file', () => {
      // given
      const expected: TemporaryFile = { id: null, base64: 'data as base64' } as TemporaryFile;
      (TemporaryFile as jest.Mock).mockImplementation(() => expected);

      // when
      createTemporaryFile.execute({} as TemporaryFileRawInterface);

      // then
      expect(mockTemporaryFileRepository.save).toHaveBeenCalledWith(expected);
    });

    it('should return saved temporary file', async () => {
      // given
      const expected: TemporaryFile = { id: '42', base64: 'data as base64' } as TemporaryFile;
      (mockTemporaryFileRepository.save as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: TemporaryFileInterface = await createTemporaryFile.execute({} as TemporaryFileRawInterface);

      // then
      expect(result).toBe(expected);
    });
  });
});

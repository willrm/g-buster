import { CompanyInterface } from '../../domain/company/company.interface';
import { CompanyRepository } from '../../domain/company/company.repository.interface';
import { JobOffer, JobOfferFactoryInterface } from '../../domain/job-offer/job-offer';
import { JobOfferRawInterface } from '../../domain/job-offer/job-offer.raw.interface';
import { UserInterface } from '../../domain/user/user.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { ValidateJobOffer } from '../validate-job-offer';

jest.mock('../../domain/job-offer/job-offer');

describe('use_cases/ValidateJobOffer', () => {
  let validateJobOffer: ValidateJobOffer;
  let mockUserRepository: UserRepository;
  let mockCompanyRepository: CompanyRepository;
  let jobOfferRaw: JobOfferRawInterface;

  beforeEach(() => {
    JobOffer.factory = {
      copy: jest.fn(),
      createDraftJobOffer: jest.fn(),
      validateAndCreateNewJobOffer: jest.fn(),
    } as JobOfferFactoryInterface;

    mockUserRepository = {} as UserRepository;
    mockUserRepository.getCurrent = jest.fn();

    mockCompanyRepository = {} as CompanyRepository;
    mockCompanyRepository.findByUserId = jest.fn();

    validateJobOffer = new ValidateJobOffer(mockUserRepository, mockCompanyRepository);

    jobOfferRaw = {} as JobOfferRawInterface;
  });

  describe('execute()', () => {
    it('should find company using user id', async () => {
      // given
      const currentUser: UserInterface = { id: '42' } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

      // when
      await validateJobOffer.execute(jobOfferRaw);

      // then
      expect(mockCompanyRepository.findByUserId).toHaveBeenCalledWith('42');
    });

    it('should validate and create a new JobOffer with current user, company and raw data', async () => {
      // given
      jobOfferRaw.title = 'A job title';

      const currentUser: UserInterface = { firstName: 'Test' } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

      const company: CompanyInterface = { name: 'Company name' } as CompanyInterface;
      (mockCompanyRepository.findByUserId as jest.Mock).mockReturnValue(Promise.resolve(company));

      // when
      await validateJobOffer.execute(jobOfferRaw);

      // then
      expect(JobOffer.factory.validateAndCreateNewJobOffer as jest.Mock).toHaveBeenCalledWith(jobOfferRaw, currentUser, company);
    });
  });
});

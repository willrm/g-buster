import { User } from '../../domain/user/user';
import { UserRawInterface } from '../../domain/user/user.raw.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { LogInUser } from '../log-in-user';

jest.mock('../../domain/user/user');

describe('use_cases/LogInUser', () => {
  let logInUser: LogInUser;
  let mockUserRepository: UserRepository;
  let userRaw: UserRawInterface;

  beforeEach(() => {
    (User as jest.Mock).mockClear();

    userRaw = {} as UserRawInterface;

    mockUserRepository = {} as UserRepository;
    mockUserRepository.findAllAdminEmailAddresses = jest.fn();
    mockUserRepository.logIn = jest.fn();

    logInUser = new LogInUser(mockUserRepository);
  });

  describe('execute()', () => {
    it('should instantiate a new User from raw data and found admin email addresses', async () => {
      // given
      userRaw.email = 'test@example.org';
      const adminEmailAddresses: string[] = ['admin@example.org'];
      (mockUserRepository.findAllAdminEmailAddresses as jest.Mock).mockReturnValue(Promise.resolve(adminEmailAddresses));

      // when
      await logInUser.execute(userRaw);

      // then
      expect(User as jest.Mock).toHaveBeenCalledWith(userRaw, adminEmailAddresses);
    });

    it('should log created user in using repository', async () => {
      // given
      const user: User = { id: '123456789' } as User;
      (User as jest.Mock).mockImplementation(() => user);

      // when
      await logInUser.execute(userRaw);

      // then
      expect(mockUserRepository.logIn).toHaveBeenCalledWith(user);
    });
  });
});

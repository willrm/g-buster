import { ItemNotFoundError } from '../../domain/common-errors/item-not-found.error';
import { CompanyInterface } from '../../domain/company/company.interface';
import { CompanyRepository } from '../../domain/company/company.repository.interface';
import { JobOfferStatus } from '../../domain/job-offer/job-offer-enums';
import { JobOfferInterface } from '../../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../../domain/job-offer/job-offer.repository.interface';
import { CompanyId, JobOfferId } from '../../domain/type-aliases';
import { UserRole } from '../../domain/user/user-enums';
import { UserInterface } from '../../domain/user/user.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { JobOfferWithCompanyAdapter } from '../adapters/job-offer-with-company-adapter';
import { JobOfferWithCompanyInterface } from '../adapters/job-offer-with-company.interface';
import { GetPublishedJobOfferById } from '../get-published-job-offer-by-id';

describe('uses_cases/GetPublishedJobOfferById', () => {
  let getPublishedJobOfferById: GetPublishedJobOfferById;
  let mockJobOfferRepository: JobOfferRepository;
  let mockUserRepository: UserRepository;
  let mockCompanyRepository: CompanyRepository;
  let mockJobOfferWithCompanyAdapter: JobOfferWithCompanyAdapter;

  let fixedDate: Date;
  let fixedDateOneDayAfter: Date;

  beforeAll(() => {
    fixedDate = new Date('2019-11-15T16:35:20');
    fixedDateOneDayAfter = new Date('2019-11-16T16:35:20');
    // @ts-ignore
    jest.spyOn(global, 'Date').mockImplementation(() => fixedDate);
  });

  beforeEach(() => {
    mockJobOfferRepository = {} as JobOfferRepository;
    mockJobOfferRepository.findById = jest.fn();

    mockUserRepository = {} as UserRepository;
    mockUserRepository.getCurrent = jest.fn();

    mockCompanyRepository = {} as CompanyRepository;
    mockCompanyRepository.findById = jest.fn();

    mockJobOfferWithCompanyAdapter = {} as JobOfferWithCompanyAdapter;
    mockJobOfferWithCompanyAdapter.toJobOfferWithCompany = jest.fn();

    getPublishedJobOfferById = new GetPublishedJobOfferById(
      mockJobOfferRepository,
      mockUserRepository,
      mockCompanyRepository,
      mockJobOfferWithCompanyAdapter
    );

    const userWithCandidateRole: UserInterface = { firstName: 'Test', role: UserRole.CANDIDATE } as UserInterface;
    (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(userWithCandidateRole));
  });

  describe('execute()', () => {
    it('should call repository to find job offer by id', async () => {
      // given
      const id: JobOfferId = '42';
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(createPublishedJobOffer(id, 'Job offer title')));

      // when
      await getPublishedJobOfferById.execute(id);

      // then
      expect(mockJobOfferRepository.findById).toHaveBeenCalledWith(id);
    });

    it('should throw not found error when job offer have no published date', async () => {
      // given
      const id: JobOfferId = '42';
      const jobOfferWithoutPublishedDate: JobOfferInterface = {
        id,
        title: 'Job with no published date',
        status: JobOfferStatus.STATUS_PUBLISHED,
      } as JobOfferInterface;
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(jobOfferWithoutPublishedDate));

      // when
      const result: Promise<JobOfferInterface> = getPublishedJobOfferById.execute(id);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('JobOffer not found'));
    });

    it('should throw not found error when job offer have a published date in the future', async () => {
      // given
      const id: JobOfferId = '42';
      const jobOfferPublishedAtFixedDateOneDayAfter: JobOfferInterface = {
        id,
        title: 'Job with published date in the future',
        status: JobOfferStatus.STATUS_PUBLISHED,
        publishedAt: fixedDateOneDayAfter,
      } as JobOfferInterface;
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(jobOfferPublishedAtFixedDateOneDayAfter));

      // when
      const result: Promise<JobOfferInterface> = getPublishedJobOfferById.execute(id);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('JobOffer not found'));
    });

    it('should throw not found error when found job offer is not published', async () => {
      // given
      const id: JobOfferId = '42';
      const jobOffer: JobOfferInterface = {
        id,
        title: 'Job with status not published',
        status: JobOfferStatus.STATUS_DRAFT,
        publishedAt: fixedDate,
      } as JobOfferInterface;
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(jobOffer));

      // when
      const result: Promise<JobOfferInterface> = getPublishedJobOfferById.execute(id);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('JobOffer not found'));
    });

    it('should find company for found job offer', async () => {
      // given
      const id: JobOfferId = '42';
      const companyId: CompanyId = '1337';
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(createPublishedJobOffer(id, 'Job offer title', companyId)));

      // when
      await getPublishedJobOfferById.execute(id);

      // then
      expect(mockCompanyRepository.findById).toHaveBeenCalledWith(companyId);
    });

    it('should add company to found job offer without anonymization when user is logged in as candidate', async () => {
      // given
      const id: JobOfferId = '42';
      const companyId: CompanyId = '1337';
      const jobOffer: JobOfferInterface = createPublishedJobOffer(id, 'Job offer title', companyId);
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(jobOffer));

      const company: CompanyInterface = { id: companyId } as CompanyInterface;
      (mockCompanyRepository.findById as jest.Mock).mockReturnValue(company);

      // when
      await getPublishedJobOfferById.execute(id);

      // then
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledWith(jobOffer, company, false);
    });

    it('should add company to found job offer with anonymization when user is logged in as company', async () => {
      // given
      const id: JobOfferId = '42';
      const companyId: CompanyId = '1337';
      const jobOffer: JobOfferInterface = createPublishedJobOffer(id, 'Job offer title', companyId);
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(jobOffer));

      const company: CompanyInterface = { id: companyId } as CompanyInterface;
      (mockCompanyRepository.findById as jest.Mock).mockReturnValue(company);

      const userWithCompanyRole: UserInterface = { firstName: 'Test', role: UserRole.COMPANY } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(userWithCompanyRole));

      // when
      await getPublishedJobOfferById.execute(id);

      // then
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledWith(jobOffer, company, true);
    });

    it('should add company to found job offer with anonymization when no user logged in', async () => {
      // given
      const id: JobOfferId = '42';
      const companyId: CompanyId = '1337';
      const jobOffer: JobOfferInterface = createPublishedJobOffer(id, 'Job offer title', companyId);
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(jobOffer));

      const company: CompanyInterface = { id: companyId } as CompanyInterface;
      (mockCompanyRepository.findById as jest.Mock).mockReturnValue(company);

      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.reject(new ItemNotFoundError('User not found')));

      // when
      await getPublishedJobOfferById.execute(id);

      // then
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledWith(jobOffer, company, true);
    });

    it('should return found job offer with company', async () => {
      // given
      const id: JobOfferId = '42';
      const companyId: CompanyId = '1337';
      const jobOffer: JobOfferInterface = createPublishedJobOffer(id, 'Job offer title', companyId);
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(jobOffer));

      const jobOfferWithCompany: JobOfferWithCompanyInterface = { ...jobOffer, company: { id: companyId } } as JobOfferWithCompanyInterface;
      (mockJobOfferWithCompanyAdapter.toJobOfferWithCompany as jest.Mock).mockReturnValue(jobOfferWithCompany);

      // when
      const result: JobOfferWithCompanyInterface = await getPublishedJobOfferById.execute(id);

      // then
      expect(result).toStrictEqual(jobOfferWithCompany);
    });

    it('should add empty company to job offer when company has not been found', async () => {
      // given
      const id: JobOfferId = '42';
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(createPublishedJobOffer(id, 'Job offer title', '1337')));

      (mockCompanyRepository.findById as jest.Mock).mockReturnValue(Promise.reject(new ItemNotFoundError('Company not found with id 1337')));

      // when
      await getPublishedJobOfferById.execute(id);

      // then
      expect(mockJobOfferWithCompanyAdapter.toJobOfferWithCompany).toHaveBeenCalledWith(
        createPublishedJobOffer(id, 'Job offer title', '1337'),
        {} as CompanyInterface,
        false
      );
    });
  });
  const createPublishedJobOffer = (id: JobOfferId, title: string, companyId?: CompanyId): JobOfferInterface => {
    return { id, title, companyId, publishedAt: fixedDate, status: JobOfferStatus.STATUS_PUBLISHED } as JobOfferInterface;
  };
});

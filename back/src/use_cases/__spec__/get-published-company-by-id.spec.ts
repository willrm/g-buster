import { ItemNotFoundError } from '../../domain/common-errors/item-not-found.error';
import { AnonymizedCompanyAdapter } from '../../domain/company/anonymized-company-adapter';
import { CompanyStatus } from '../../domain/company/company-enums';
import { CompanyInterface } from '../../domain/company/company.interface';
import { CompanyRepository } from '../../domain/company/company.repository.interface';
import { CompanyId } from '../../domain/type-aliases';
import { UserRole } from '../../domain/user/user-enums';
import { UserInterface } from '../../domain/user/user.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { GetPublishedCompanyById } from '../get-published-company-by-id';

describe('use_cases/GetPublishedCompanyById', () => {
  let getPublishedCompanyById: GetPublishedCompanyById;
  let mockUserRepository: UserRepository;
  let mockCompanyRepository: CompanyRepository;
  let mockAnonymizedCompanyAdapter: AnonymizedCompanyAdapter;

  beforeEach(() => {
    mockUserRepository = {} as UserRepository;
    mockUserRepository.getCurrent = jest.fn();

    mockCompanyRepository = {} as CompanyRepository;
    mockCompanyRepository.findById = jest.fn();

    mockAnonymizedCompanyAdapter = {} as AnonymizedCompanyAdapter;
    mockAnonymizedCompanyAdapter.anonymize = jest.fn();

    getPublishedCompanyById = new GetPublishedCompanyById(mockUserRepository, mockCompanyRepository, mockAnonymizedCompanyAdapter);

    (mockAnonymizedCompanyAdapter.anonymize as jest.Mock).mockReturnValue(Promise.resolve({} as CompanyInterface));
  });

  describe('execute()', () => {
    const existingCompanyId: CompanyId = '42';
    let company: CompanyInterface;
    let user: UserInterface;

    beforeEach(() => {
      company = {
        id: existingCompanyId,
        userId: '1337',
        status: CompanyStatus.STATUS_PUBLISHED,
      } as CompanyInterface;

      user = {
        id: '1337',
      } as UserInterface;

      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(user));
      (mockCompanyRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(company));
    });

    it('should call company repository with id', async () => {
      // when
      await getPublishedCompanyById.execute(existingCompanyId);

      // then
      expect(mockCompanyRepository.findById).toHaveBeenCalledWith(existingCompanyId);
    });

    it('should return found company', async () => {
      // when
      const result: CompanyInterface = await getPublishedCompanyById.execute(existingCompanyId);

      // then
      expect(result).toStrictEqual(result);
    });

    it('should return not found error when found company is not publish', async () => {
      // given
      company.status = CompanyStatus.STATUS_PENDING_VALIDATION;

      // when
      const result: Promise<CompanyInterface> = getPublishedCompanyById.execute(existingCompanyId);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('Company not found'));
    });

    it('should not call anonymize company adapter method when user is logged as candidate', async () => {
      // given
      user.role = UserRole.CANDIDATE;

      // when
      await getPublishedCompanyById.execute(existingCompanyId);

      // then
      expect(mockAnonymizedCompanyAdapter.anonymize).not.toHaveBeenCalled();
    });

    it('should call anonymize company adapter method when user is logged as company', async () => {
      // given
      user.role = UserRole.COMPANY;

      // when
      await getPublishedCompanyById.execute(existingCompanyId);

      // then
      expect(mockAnonymizedCompanyAdapter.anonymize).toHaveBeenCalledTimes(1);
    });

    it('should call anonymize company adapter method when no user logged in', async () => {
      // given
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.reject(new ItemNotFoundError('User not found')));

      // when
      await getPublishedCompanyById.execute(existingCompanyId);

      // then
      expect(mockAnonymizedCompanyAdapter.anonymize).toHaveBeenCalledTimes(1);
    });
  });
});

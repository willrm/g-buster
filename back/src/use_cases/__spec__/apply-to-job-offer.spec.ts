import { Candidate } from '../../domain/candidate/candidate';
import { CandidateRepository } from '../../domain/candidate/candidate.repository';
import { AccessDeniedError } from '../../domain/common-errors/access-denied.error';
import { ItemNotFoundError } from '../../domain/common-errors/item-not-found.error';
import { JobOfferStatus } from '../../domain/job-offer/job-offer-enums';
import { JobOfferInterface } from '../../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../../domain/job-offer/job-offer.repository.interface';
import { TemporaryFileInterface } from '../../domain/temporary-file/temporary-file.interface';
import { TemporaryFileRepository } from '../../domain/temporary-file/temporary-file.repository';
import { CandidateId, JobOfferId, Uuid } from '../../domain/type-aliases';
import { UserRole } from '../../domain/user/user-enums';
import { UserInterface } from '../../domain/user/user.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { ApplyToJobOffer } from '../apply-to-job-offer';

jest.mock('../../domain/candidate/candidate');

describe('use_cases/ApplyToJobOffer', () => {
  let applyToJobOffer: ApplyToJobOffer;
  let mockUserRepository: UserRepository;
  let mockJobOfferRepository: JobOfferRepository;
  let mockCandidateRepository: CandidateRepository;
  let mockTemporaryFileRepository: TemporaryFileRepository;

  let jobOfferId: JobOfferId;
  let resumeTemporaryFileUuid: Uuid;
  let coverLetterTemporaryFileUuid: Uuid;

  beforeEach(() => {
    (Candidate as jest.Mock).mockClear();

    mockUserRepository = {} as UserRepository;
    mockUserRepository.getCurrent = jest.fn();

    mockJobOfferRepository = {} as JobOfferRepository;
    mockJobOfferRepository.findById = jest.fn();

    mockCandidateRepository = {} as CandidateRepository;
    mockCandidateRepository.save = jest.fn();
    mockCandidateRepository.applyToJobOffer = jest.fn();

    mockTemporaryFileRepository = {} as TemporaryFileRepository;
    mockTemporaryFileRepository.findByUuid = jest.fn();

    applyToJobOffer = new ApplyToJobOffer(mockUserRepository, mockJobOfferRepository, mockCandidateRepository, mockTemporaryFileRepository);

    const currentUser: UserInterface = { firstName: 'Test', role: UserRole.CANDIDATE } as UserInterface;
    (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

    const publishedJobOffer: JobOfferInterface = { publishedAt: new Date(), status: JobOfferStatus.STATUS_PUBLISHED } as JobOfferInterface;
    (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(publishedJobOffer));

    jobOfferId = '42';
    resumeTemporaryFileUuid = 'resume-uuid';
    coverLetterTemporaryFileUuid = 'cover-letter-uuid';
  });

  describe('execute()', () => {
    it('should throw exception when current user is not a candidate', async () => {
      // given
      const userWithCompanyRole: UserInterface = { firstName: 'Test', role: UserRole.COMPANY } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(userWithCompanyRole));

      // when
      const result: Promise<void> = applyToJobOffer.execute(jobOfferId, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);

      // then
      await expect(result).rejects.toThrow(new AccessDeniedError('Current user is not a Candidate'));
    });

    it('should find existing job offer with given id', async () => {
      // when
      await applyToJobOffer.execute(jobOfferId, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);

      // then
      expect(mockJobOfferRepository.findById).toHaveBeenCalledWith(jobOfferId);
    });

    it('should find resume temporary file with given uuid', async () => {
      // when
      await applyToJobOffer.execute(jobOfferId, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);

      // then
      expect(mockTemporaryFileRepository.findByUuid).toHaveBeenNthCalledWith(1, resumeTemporaryFileUuid);
    });

    it('should find cover letter temporary file with given uuid', async () => {
      // when
      await applyToJobOffer.execute(jobOfferId, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);

      // then
      expect(mockTemporaryFileRepository.findByUuid).toHaveBeenNthCalledWith(2, coverLetterTemporaryFileUuid);
    });

    it('should throw exception when given job offer is not published because of no published date', async () => {
      // given
      const foundJobOffer: JobOfferInterface = { id: jobOfferId, publishedAt: null, status: JobOfferStatus.STATUS_PUBLISHED } as JobOfferInterface;
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(foundJobOffer));

      // when
      const result: Promise<void> = applyToJobOffer.execute(jobOfferId, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('JobOffer not found'));
    });

    it('should throw exception when given job offer is not published because of published date in the future', async () => {
      // given
      const foundJobOffer: JobOfferInterface = {
        id: jobOfferId,
        publishedAt: new Date('2039-03-15T16:35:20'),
        status: JobOfferStatus.STATUS_PUBLISHED,
      } as JobOfferInterface;
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(foundJobOffer));

      // when
      const result: Promise<void> = applyToJobOffer.execute(jobOfferId, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('JobOffer not found'));
    });

    it('should throw exception when given job offer is not published because of status that is not published', async () => {
      // given
      const foundJobOffer: JobOfferInterface = {
        id: jobOfferId,
        publishedAt: new Date(),
        status: JobOfferStatus.STATUS_ARCHIVED,
      } as JobOfferInterface;
      (mockJobOfferRepository.findById as jest.Mock).mockReturnValue(Promise.resolve(foundJobOffer));

      // when
      const result: Promise<void> = applyToJobOffer.execute(jobOfferId, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('JobOffer not found'));
    });

    it('should instantiate a new Candidate from current user', async () => {
      // given
      const currentUser: UserInterface = { firstName: 'Test', role: UserRole.CANDIDATE } as UserInterface;
      (mockUserRepository.getCurrent as jest.Mock).mockReturnValue(Promise.resolve(currentUser));

      // when
      await applyToJobOffer.execute(jobOfferId, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);

      // then
      expect(Candidate as jest.Mock).toHaveBeenCalledWith(currentUser);
    });

    it('should save new candidate with found resume and cover letter temporary files', async () => {
      // given
      const expectedCandidate: Candidate = { firstName: 'User first name' } as Candidate;
      (Candidate as jest.Mock).mockImplementation(() => expectedCandidate);

      const expectedResumeTemporaryFile: TemporaryFileInterface = {
        base64: 'resume-as-base64',
        filename: 'resume-filename.extension',
      } as TemporaryFileInterface;
      const expectedCoverLetterTemporaryFile: TemporaryFileInterface = {
        base64: 'cover-letter-as-base64',
        filename: 'cover-letter-filename.extension',
      } as TemporaryFileInterface;
      (mockTemporaryFileRepository.findByUuid as jest.Mock)
        .mockReturnValueOnce(expectedResumeTemporaryFile)
        .mockReturnValueOnce(expectedCoverLetterTemporaryFile);

      // when
      await applyToJobOffer.execute(jobOfferId, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);

      // then
      expect(mockCandidateRepository.save).toHaveBeenCalledWith(expectedCandidate, expectedResumeTemporaryFile, expectedCoverLetterTemporaryFile);
    });

    it('should apply created candidate to job offer', async () => {
      // given
      const candidateId: CandidateId = 'candidate-id';
      (mockCandidateRepository.save as jest.Mock).mockReturnValue(Promise.resolve(candidateId));

      // when
      await applyToJobOffer.execute(jobOfferId, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);

      // then
      expect(mockCandidateRepository.applyToJobOffer).toHaveBeenCalledWith(candidateId, jobOfferId);
    });
  });
});

import { isEmpty } from 'lodash';
import { CompanyInterface } from '../domain/company/company.interface';
import { CompanyRepository } from '../domain/company/company.repository.interface';
import { GetCompaniesFilterInterface } from './get-companies-filter.interface';

export class GetCompanies {
  constructor(private readonly companyRepository: CompanyRepository) {}

  async execute(filter: GetCompaniesFilterInterface = {}): Promise<CompanyInterface[]> {
    const companies: CompanyInterface[] = await this.companyRepository.findAll();
    const filteredCompanies: CompanyInterface[] = this.applyFilter(companies, filter);

    return Promise.resolve(filteredCompanies);
  }

  private applyFilter(companies: CompanyInterface[], filter: GetCompaniesFilterInterface): CompanyInterface[] {
    return companies.filter((company: CompanyInterface) => isEmpty(filter.status) || filter.status === company.status);
  }
}

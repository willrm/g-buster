import { Candidate } from '../domain/candidate/candidate';
import { CandidateRepository } from '../domain/candidate/candidate.repository';
import { AccessDeniedError } from '../domain/common-errors/access-denied.error';
import { ItemNotFoundError } from '../domain/common-errors/item-not-found.error';
import { JobOffer } from '../domain/job-offer/job-offer';
import { JobOfferInterface } from '../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../domain/job-offer/job-offer.repository.interface';
import { TemporaryFileInterface } from '../domain/temporary-file/temporary-file.interface';
import { TemporaryFileRepository } from '../domain/temporary-file/temporary-file.repository';
import { CandidateId, JobOfferId, Uuid } from '../domain/type-aliases';
import { UserRole } from '../domain/user/user-enums';
import { UserInterface } from '../domain/user/user.interface';
import { UserRepository } from '../domain/user/user.repository.interface';

export class ApplyToJobOffer {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly jobOfferRepository: JobOfferRepository,
    private readonly candidateRepository: CandidateRepository,
    private readonly temporaryFileRepository: TemporaryFileRepository
  ) {}

  async execute(jobOfferId: JobOfferId, resumeTemporaryFileUuid: Uuid, coverLetterTemporaryFileUuid: Uuid): Promise<void> {
    const currentUser: UserInterface = await this.userRepository.getCurrent();
    if (currentUser.role !== UserRole.CANDIDATE) {
      return Promise.reject(new AccessDeniedError('Current user is not a Candidate'));
    }

    await this.assertJobOfferIsPublished(jobOfferId);

    const candidateId: CandidateId = await this.createNewCandidate(currentUser, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);

    await this.candidateRepository.applyToJobOffer(candidateId, jobOfferId);
  }

  private async assertJobOfferIsPublished(jobOfferId: JobOfferId): Promise<void> {
    const foundJobOffer: JobOfferInterface = await this.jobOfferRepository.findById(jobOfferId);
    if (!JobOffer.factory.copy(foundJobOffer).isPublished()) {
      return Promise.reject(new ItemNotFoundError('JobOffer not found'));
    }
  }

  private async createNewCandidate(user: UserInterface, resumeTemporaryFileUuid: Uuid, coverLetterTemporaryFileUuid: Uuid): Promise<CandidateId> {
    const newCandidate: Candidate = new Candidate(user);
    const resumeTemporaryFile: TemporaryFileInterface = await this.temporaryFileRepository.findByUuid(resumeTemporaryFileUuid);
    const coverLetterTemporaryFile: TemporaryFileInterface = await this.temporaryFileRepository.findByUuid(coverLetterTemporaryFileUuid);

    return await this.candidateRepository.save(newCandidate, resumeTemporaryFile, coverLetterTemporaryFile);
  }
}

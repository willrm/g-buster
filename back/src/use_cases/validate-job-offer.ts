import { CompanyInterface } from '../domain/company/company.interface';
import { CompanyRepository } from '../domain/company/company.repository.interface';
import { JobOffer } from '../domain/job-offer/job-offer';
import { JobOfferRawInterface } from '../domain/job-offer/job-offer.raw.interface';
import { UserInterface } from '../domain/user/user.interface';
import { UserRepository } from '../domain/user/user.repository.interface';

export class ValidateJobOffer {
  constructor(private readonly userRepository: UserRepository, private readonly companyRepository: CompanyRepository) {}

  async execute(jobOfferRaw: JobOfferRawInterface): Promise<void> {
    const currentUser: UserInterface = await this.userRepository.getCurrent();
    const company: CompanyInterface = await this.companyRepository.findByUserId(currentUser.id);

    // tslint:disable-next-line:no-unused-expression
    JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, currentUser, company);
  }
}

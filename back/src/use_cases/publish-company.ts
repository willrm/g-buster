import { Company } from '../domain/company/company';
import { CompanyInterface } from '../domain/company/company.interface';
import { CompanyRepository } from '../domain/company/company.repository.interface';
import { CompanyId } from '../domain/type-aliases';

export class PublishCompany {
  constructor(private readonly companyRepository: CompanyRepository) {}

  async execute(companyId: CompanyId): Promise<void> {
    const foundCompany: CompanyInterface = await this.companyRepository.findById(companyId);
    const updateCompany: Company = Company.factory.copy(foundCompany);

    updateCompany.publish();

    await this.companyRepository.save(updateCompany);
  }
}

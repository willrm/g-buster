import { DuplicateItemError } from '../domain/common-errors/duplicate-item.error';
import { Company } from '../domain/company/company';
import { CompanyInterface } from '../domain/company/company.interface';
import { CompanyRawInterface } from '../domain/company/company.raw.interface';
import { CompanyRepository } from '../domain/company/company.repository.interface';
import { TemporaryFileInterface } from '../domain/temporary-file/temporary-file.interface';
import { TemporaryFileRepository } from '../domain/temporary-file/temporary-file.repository';
import { UserInterface } from '../domain/user/user.interface';
import { UserRepository } from '../domain/user/user.repository.interface';

export class ValidateCompany {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly companyRepository: CompanyRepository,
    private readonly temporaryFileRepository: TemporaryFileRepository
  ) {}

  async execute(companyRaw: CompanyRawInterface): Promise<void> {
    const currentUser: UserInterface = await this.userRepository.getCurrent();
    const existingCompany: CompanyInterface = await this.getCompany(currentUser);
    if (existingCompany) {
      return Promise.reject(new DuplicateItemError('Company already exists for user'));
    }

    const temporaryFile: TemporaryFileInterface = await this.getTemporaryFile(companyRaw.logoUuid);
    // tslint:disable-next-line:no-unused-expression
    Company.factory.validateAndCreateNewCompany(currentUser, companyRaw, temporaryFile);
  }

  private async getTemporaryFile(logoUuid: string): Promise<TemporaryFileInterface> {
    try {
      return await this.temporaryFileRepository.findByUuid(logoUuid);
    } catch (e) {
      return null;
    }
  }

  private async getCompany(currentUser: UserInterface): Promise<CompanyInterface> {
    try {
      return await this.companyRepository.findByUserId(currentUser.id);
    } catch (e) {
      return null;
    }
  }
}

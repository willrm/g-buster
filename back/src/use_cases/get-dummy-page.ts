import { DummyPageInterface } from '../domain/dummy-page/dummy-page.interface';
import { DummyPageRepository } from '../domain/dummy-page/dummy-page.repository.interface';
import { Lang, Uid } from '../domain/type-aliases';

export class GetDummyPage {
  constructor(private readonly dummyPageRepository: DummyPageRepository) {}

  execute(lang: Lang, uid: Uid): Promise<DummyPageInterface> {
    return this.dummyPageRepository.findByLangAndUid(lang, uid);
  }
}

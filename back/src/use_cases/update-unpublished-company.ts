import { Company } from '../domain/company/company';
import { CompanyInterface } from '../domain/company/company.interface';
import { CompanyRawInterface } from '../domain/company/company.raw.interface';
import { CompanyRepository } from '../domain/company/company.repository.interface';
import { TemporaryFileInterface } from '../domain/temporary-file/temporary-file.interface';
import { TemporaryFileRepository } from '../domain/temporary-file/temporary-file.repository';
import { CompanyId } from '../domain/type-aliases';

export class UpdateUnpublishedCompany {
  constructor(private readonly companyRepository: CompanyRepository, private readonly temporaryFileRepository: TemporaryFileRepository) {}

  async execute(companyId: CompanyId, companyRaw: CompanyRawInterface): Promise<void> {
    const foundCompany: CompanyInterface = await this.companyRepository.findById(companyId);
    const companyToUpdate: Company = Company.factory.copy(foundCompany);

    companyToUpdate.updateWith(companyRaw);

    if (companyRaw.logoUuid) {
      const temporaryFile: TemporaryFileInterface = await this.getTemporaryFile(companyRaw.logoUuid);
      companyToUpdate.updateLogo(temporaryFile);
    }

    await this.companyRepository.save(companyToUpdate);
  }

  private async getTemporaryFile(logoUuid: string): Promise<TemporaryFileInterface> {
    try {
      return await this.temporaryFileRepository.findByUuid(logoUuid);
    } catch (e) {
      return null;
    }
  }
}

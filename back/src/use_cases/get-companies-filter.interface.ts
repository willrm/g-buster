import { CompanyStatus } from '../domain/company/company-enums';

export interface GetCompaniesFilterInterface {
  status?: CompanyStatus;
}

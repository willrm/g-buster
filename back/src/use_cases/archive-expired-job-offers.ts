import { JobOffer } from '../domain/job-offer/job-offer';
import { JobOfferStatus } from '../domain/job-offer/job-offer-enums';
import { JobOfferInterface } from '../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../domain/job-offer/job-offer.repository.interface';
import { JobOfferId } from '../domain/type-aliases';

export class ArchiveExpiredJobOffers {
  constructor(private readonly jobOfferRepository: JobOfferRepository) {}

  async execute(): Promise<JobOfferId[]> {
    const publishedJobOffers: JobOfferInterface[] = await this.jobOfferRepository.findAllByStatus(JobOfferStatus.STATUS_PUBLISHED);
    const archivedJobOfferIds: JobOfferId[] = this.archiveExpiredJobOffers(publishedJobOffers);

    return Promise.resolve(archivedJobOfferIds);
  }

  private archiveExpiredJobOffers(jobOffers: JobOfferInterface[]): JobOfferId[] {
    const thirtyDaysAgo: Date = this.getCurrentDateThirtyDaysAgo();

    if (jobOffers.length > 0) {
      const jobOffersToArchive: JobOffer[] = jobOffers
        .filter((jobOffer: JobOfferInterface) => jobOffer.publishedAt < thirtyDaysAgo)
        .map((jobOfferToArchive: JobOfferInterface) => JobOffer.factory.copy(jobOfferToArchive));

      jobOffersToArchive.forEach((jobOfferToArchive: JobOffer) => this.archiveExpiredJobOffer(jobOfferToArchive));

      return jobOffersToArchive.map((jobOffer: JobOffer) => jobOffer.id);
    } else {
      return [];
    }
  }

  private getCurrentDateThirtyDaysAgo(): Date {
    const date: Date = new Date();
    date.setDate(date.getDate() - 30);

    return date;
  }

  private archiveExpiredJobOffer(expiredJobOffer: JobOffer): void {
    expiredJobOffer.archive();
    this.jobOfferRepository.save(expiredJobOffer);
  }
}

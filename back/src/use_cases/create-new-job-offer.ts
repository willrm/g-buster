import { CompanyInterface } from '../domain/company/company.interface';
import { CompanyRepository } from '../domain/company/company.repository.interface';
import { JobOffer } from '../domain/job-offer/job-offer';
import { JobOfferRawInterface } from '../domain/job-offer/job-offer.raw.interface';
import { JobOfferRepository } from '../domain/job-offer/job-offer.repository.interface';
import { JobOfferId } from '../domain/type-aliases';
import { UserInterface } from '../domain/user/user.interface';
import { UserRepository } from '../domain/user/user.repository.interface';

export class CreateNewJobOffer {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly jobOfferRepository: JobOfferRepository,
    private readonly companyRepository: CompanyRepository
  ) {}

  async execute(jobOfferRaw: JobOfferRawInterface): Promise<JobOfferId> {
    const currentUser: UserInterface = await this.userRepository.getCurrent();
    const company: CompanyInterface = await this.companyRepository.findByUserId(currentUser.id);

    const jobOffer: JobOffer = jobOfferRaw.draft
      ? JobOffer.factory.createDraftJobOffer(jobOfferRaw, currentUser, company)
      : JobOffer.factory.validateAndCreateNewJobOffer(jobOfferRaw, currentUser, company);

    return this.jobOfferRepository.save(jobOffer);
  }
}

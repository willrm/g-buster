import { TemporaryFileInterface } from '../domain/temporary-file/temporary-file.interface';
import { TemporaryFileRepository } from '../domain/temporary-file/temporary-file.repository';
import { TemporaryFileId } from '../domain/type-aliases';

export class CleanExpiredTemporaryFiles {
  constructor(private readonly temporaryFileRepository: TemporaryFileRepository) {}

  async execute(): Promise<TemporaryFileId[]> {
    const oneHourAgo: Date = this.getCurrentDateOneHourAgo();
    const expiredTemporaryFiles: TemporaryFileInterface[] = await this.temporaryFileRepository.findAllByCreatedAtBefore(oneHourAgo);
    const deletedTemporaryFileIds: TemporaryFileId[] = await this.deleteExpiredTemporaryFiles(expiredTemporaryFiles);

    return Promise.resolve(deletedTemporaryFileIds);
  }

  private deleteExpiredTemporaryFiles(expiredTemporaryFiles: TemporaryFileInterface[]): TemporaryFileId[] {
    if (expiredTemporaryFiles.length > 0) {
      const expiredTemporaryFileIds: TemporaryFileId[] = expiredTemporaryFiles.map((temporaryFile: TemporaryFileInterface) => temporaryFile.id);
      this.temporaryFileRepository.delete(expiredTemporaryFiles);

      return expiredTemporaryFileIds;
    }

    return [];
  }

  private getCurrentDateOneHourAgo(): Date {
    const date: Date = new Date();
    date.setHours(date.getHours() - 1);

    return date;
  }
}

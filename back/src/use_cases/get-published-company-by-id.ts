import { ItemNotFoundError } from '../domain/common-errors/item-not-found.error';
import { AnonymizedCompanyAdapter } from '../domain/company/anonymized-company-adapter';
import { CompanyStatus } from '../domain/company/company-enums';
import { CompanyInterface } from '../domain/company/company.interface';
import { CompanyRepository } from '../domain/company/company.repository.interface';
import { CompanyId } from '../domain/type-aliases';
import { UserRole } from '../domain/user/user-enums';
import { UserInterface } from '../domain/user/user.interface';
import { UserRepository } from '../domain/user/user.repository.interface';

export class GetPublishedCompanyById {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly companyRepository: CompanyRepository,
    private readonly anonymizedCompanyAdapter: AnonymizedCompanyAdapter
  ) {}

  async execute(id: CompanyId): Promise<CompanyInterface> {
    const foundCompany: CompanyInterface = await this.companyRepository.findById(id);

    if (foundCompany.status !== CompanyStatus.STATUS_PUBLISHED) {
      return Promise.reject(new ItemNotFoundError('Company not found'));
    }

    const isUserLoggedAsCandidate: boolean = await this.isUserLoggedAsCandidate();
    if (!isUserLoggedAsCandidate) {
      return Promise.resolve(this.anonymizedCompanyAdapter.anonymize(foundCompany));
    }

    return Promise.resolve(foundCompany);
  }

  private async isUserLoggedAsCandidate(): Promise<boolean> {
    try {
      const currentUser: UserInterface = await this.userRepository.getCurrent();

      return currentUser.role === UserRole.CANDIDATE;
    } catch (e) {
      return false;
    }
  }
}

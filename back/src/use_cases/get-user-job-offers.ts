import { JobOfferInterface } from '../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../domain/job-offer/job-offer.repository.interface';
import { UserInterface } from '../domain/user/user.interface';
import { UserRepository } from '../domain/user/user.repository.interface';

export class GetUserJobOffers {
  constructor(private readonly userRepository: UserRepository, private readonly jobOfferRepository: JobOfferRepository) {}

  async execute(): Promise<JobOfferInterface[]> {
    const currentUser: UserInterface = await this.userRepository.getCurrent();

    return this.jobOfferRepository.findAllByUserId(currentUser.id);
  }
}

import { intersection, isEmpty, uniq } from 'lodash';
import { CompanyInterface } from '../domain/company/company.interface';
import { CompanyRepository } from '../domain/company/company.repository.interface';
import { JobOffer } from '../domain/job-offer/job-offer';
import { JobOfferStatus } from '../domain/job-offer/job-offer-enums';
import { JobOfferInterface } from '../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../domain/job-offer/job-offer.repository.interface';
import { CompanyId } from '../domain/type-aliases';
import { UserRole } from '../domain/user/user-enums';
import { UserInterface } from '../domain/user/user.interface';
import { UserRepository } from '../domain/user/user.repository.interface';
import { JobOfferWithCompanyAdapter } from './adapters/job-offer-with-company-adapter';
import { JobOfferWithCompanyInterface } from './adapters/job-offer-with-company.interface';
import { GetPublishedJobOffersFilterInterface } from './get-published-job-offers-filter.interface';

export class GetPublishedJobOffers {
  constructor(
    private readonly jobOfferRepository: JobOfferRepository,
    private readonly userRepository: UserRepository,
    private readonly companyRepository: CompanyRepository,
    private readonly jobOfferWithCompanyAdapter: JobOfferWithCompanyAdapter
  ) {}

  async execute(filter: GetPublishedJobOffersFilterInterface = {}): Promise<JobOfferWithCompanyInterface[]> {
    const jobOffersPublishedInThePast: JobOfferInterface[] = await this.getJobOffersPublishedInThePast();
    const filteredJobOffersPublishedInThePast: JobOfferInterface[] = this.applyFilter(jobOffersPublishedInThePast, filter);

    const isUserLoggedAsCandidate: boolean = await this.isUserLoggedAsCandidate();

    return this.addCompanyToJobOffers(filteredJobOffersPublishedInThePast, !isUserLoggedAsCandidate);
  }

  private async getJobOffersPublishedInThePast(): Promise<JobOfferInterface[]> {
    const jobOffersWithPublishedStatus: JobOfferInterface[] = await this.jobOfferRepository.findAllByStatus(JobOfferStatus.STATUS_PUBLISHED);

    return jobOffersWithPublishedStatus.filter((jobOffer: JobOfferInterface) => JobOffer.factory.copy(jobOffer).isPublished());
  }

  private applyFilter(jobOffersToFilter: JobOfferInterface[], filter: GetPublishedJobOffersFilterInterface): JobOfferInterface[] {
    return jobOffersToFilter
      .filter(
        (jobOffer: JobOfferInterface) =>
          isEmpty(filter.requestedSpecialities) || !isEmpty(intersection(jobOffer.requestedSpecialities, filter.requestedSpecialities))
      )
      .filter(
        (jobOffer: JobOfferInterface) =>
          isEmpty(filter.requiredExperiences) || !isEmpty(intersection(jobOffer.requiredExperiences, filter.requiredExperiences))
      )
      .filter((jobOffer: JobOfferInterface) => isEmpty(filter.regions) || filter.regions.includes(jobOffer.region))
      .filter((jobOffer: JobOfferInterface) => isEmpty(filter.jobTypes) || filter.jobTypes.includes(jobOffer.jobType));
  }

  private async isUserLoggedAsCandidate(): Promise<boolean> {
    try {
      const currentUser: UserInterface = await this.userRepository.getCurrent();

      return currentUser.role === UserRole.CANDIDATE;
    } catch (e) {
      return false;
    }
  }

  private async addCompanyToJobOffers(jobOffers: JobOfferInterface[], anonymize: boolean): Promise<JobOfferWithCompanyInterface[]> {
    const jobOffersUniqueCompanyIds: CompanyId[] = uniq(jobOffers.map((jobOffer: JobOfferInterface) => jobOffer.companyId));
    const companies: CompanyInterface[] = await this.companyRepository.findAllByIds(jobOffersUniqueCompanyIds);

    return jobOffers.map((jobOffer: JobOfferInterface) =>
      this.jobOfferWithCompanyAdapter.toJobOfferWithCompany(jobOffer, this.findCompany(companies, jobOffer), anonymize)
    );
  }

  private findCompany(companies: CompanyInterface[], jobOffer: JobOfferInterface): CompanyInterface {
    return companies.find((company: CompanyInterface) => company.id === jobOffer.companyId) || ({} as CompanyInterface);
  }
}

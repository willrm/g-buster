import { ItemNotFoundError } from '../domain/common-errors/item-not-found.error';
import { CompanyInterface } from '../domain/company/company.interface';
import { CompanyRepository } from '../domain/company/company.repository.interface';
import { JobOffer } from '../domain/job-offer/job-offer';
import { JobOfferInterface } from '../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../domain/job-offer/job-offer.repository.interface';
import { JobOfferId } from '../domain/type-aliases';
import { UserRole } from '../domain/user/user-enums';
import { UserInterface } from '../domain/user/user.interface';
import { UserRepository } from '../domain/user/user.repository.interface';
import { JobOfferWithCompanyAdapter } from './adapters/job-offer-with-company-adapter';
import { JobOfferWithCompanyInterface } from './adapters/job-offer-with-company.interface';

export class GetPublishedJobOfferById {
  constructor(
    private readonly jobOfferRepository: JobOfferRepository,
    private readonly userRepository: UserRepository,
    private readonly companyRepository: CompanyRepository,
    private readonly jobOfferWithCompanyAdapter: JobOfferWithCompanyAdapter
  ) {}

  async execute(id: JobOfferId): Promise<JobOfferWithCompanyInterface> {
    const foundJobOffer: JobOfferInterface = await this.jobOfferRepository.findById(id);

    if (!JobOffer.factory.copy(foundJobOffer).isPublished()) {
      return Promise.reject(new ItemNotFoundError('JobOffer not found'));
    }

    const isUserLoggedAsCandidate: boolean = await this.isUserLoggedAsCandidate();

    return this.addCompanyToJobOffer(foundJobOffer, !isUserLoggedAsCandidate);
  }

  private async addCompanyToJobOffer(jobOffer: JobOfferInterface, anonymize: boolean): Promise<JobOfferWithCompanyInterface> {
    const company: CompanyInterface = await this.findCompany(jobOffer);

    return this.jobOfferWithCompanyAdapter.toJobOfferWithCompany(jobOffer, company, anonymize);
  }

  private async isUserLoggedAsCandidate(): Promise<boolean> {
    try {
      const currentUser: UserInterface = await this.userRepository.getCurrent();

      return currentUser.role === UserRole.CANDIDATE;
    } catch (e) {
      return false;
    }
  }

  private async findCompany(jobOffer: JobOfferInterface): Promise<CompanyInterface> {
    try {
      return await this.companyRepository.findById(jobOffer.companyId);
    } catch (e) {
      return {} as CompanyInterface;
    }
  }
}

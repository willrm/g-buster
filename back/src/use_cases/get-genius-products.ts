import { GeniusProduct } from '../domain/genius-product/genius-product';
import { GeniusProductInterface } from '../domain/genius-product/genius-product.interface';
import { GeniusProductRawInterface } from '../domain/genius-product/genius-product.raw.interface';
import { GeniusProductRepository } from '../domain/genius-product/genius-product.repository.interface';

export class GetGeniusProducts {
  constructor(private readonly geniusProductRepository: GeniusProductRepository) {}

  async execute(): Promise<GeniusProductInterface[]> {
    const geniusProductsRaw: GeniusProductRawInterface[] = await this.geniusProductRepository.findAll();

    return geniusProductsRaw.map((geniusProductRaw: GeniusProductRawInterface) => GeniusProduct.factory.create(geniusProductRaw));
  }
}

import { FooterInterface } from '../domain/footer/footer.interface';
import { FooterRepository } from '../domain/footer/footer.repository.interface';
import { HeaderInterface } from '../domain/header/header.interface';
import { HeaderRepository } from '../domain/header/header.repository.interface';
import { Layout } from '../domain/layout/layout';
import { LayoutInterface } from '../domain/layout/layout.interface';
import { Lang } from '../domain/type-aliases';

export class GetLayout {
  constructor(private readonly headerRepository: HeaderRepository, private readonly footerRepository: FooterRepository) {}

  async execute(lang: Lang): Promise<LayoutInterface> {
    const header: HeaderInterface = await this.headerRepository.findByLang(lang);
    const footer: FooterInterface = await this.footerRepository.findByLang(lang);

    return new Layout(header, footer);
  }
}

import { ItemNotFoundError } from '../domain/common-errors/item-not-found.error';
import { JobOffer } from '../domain/job-offer/job-offer';
import { JobOfferInterface } from '../domain/job-offer/job-offer.interface';
import { JobOfferRawInterface } from '../domain/job-offer/job-offer.raw.interface';
import { JobOfferRepository } from '../domain/job-offer/job-offer.repository.interface';
import { JobOfferId } from '../domain/type-aliases';
import { UserInterface } from '../domain/user/user.interface';
import { UserRepository } from '../domain/user/user.repository.interface';

export class UpdateDraftJobOffer {
  constructor(private readonly userRepository: UserRepository, private readonly jobOfferRepository: JobOfferRepository) {}

  async execute(id: JobOfferId, jobOfferRaw: JobOfferRawInterface): Promise<JobOfferId> {
    const currentUser: UserInterface = await this.userRepository.getCurrent();
    const foundJobOffer: JobOfferInterface = await this.jobOfferRepository.findById(id);
    if (currentUser.id !== foundJobOffer.userId) {
      return Promise.reject(new ItemNotFoundError('JobOffer not found'));
    }

    const jobOfferToUpdate: JobOffer = JobOffer.factory.copy(foundJobOffer);
    jobOfferToUpdate.updateWith(jobOfferRaw);

    return this.jobOfferRepository.save(jobOfferToUpdate);
  }
}

import { UserInterface } from '../domain/user/user.interface';
import { UserRepository } from '../domain/user/user.repository.interface';

export class GetCurrentUser {
  constructor(private readonly userRepository: UserRepository) {}

  async execute(): Promise<UserInterface> {
    return this.userRepository.getCurrent();
  }
}

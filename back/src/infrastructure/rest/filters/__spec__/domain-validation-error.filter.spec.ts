import { ArgumentsHost } from '@nestjs/common';
import { DomainErrorsInterface } from '../../../../domain/validation/domain-errors.interface';
import { DomainValidationError } from '../../../../domain/validation/domain-validation.error';
import { DomainValidationErrorFilter } from '../domain-validation-error.filter';
import { DomainValidationErrorResponseInterface } from '../models/domain-validation-error-response.interface';

describe('infrastructure/rest/filters/DomainValidationErrorFilter', () => {
  let mockArgumentsHost: ArgumentsHost;
  let mockStatus: jest.Mock;
  let mockJson: jest.Mock;

  beforeEach(() => {
    mockStatus = jest.fn();
    mockJson = jest.fn();

    mockStatus.mockImplementation(() => {
      return {
        json: mockJson,
      };
    });

    mockArgumentsHost = {
      switchToHttp: () => ({
        getResponse: () => ({
          status: mockStatus,
        }),
      }),
    } as ArgumentsHost;
  });

  describe('catch()', () => {
    it('should call response status method with http bad request status code', () => {
      // given
      const domainValidationError: DomainValidationError = {} as DomainValidationError;
      const expected: number = 400;

      // when
      new DomainValidationErrorFilter().catch(domainValidationError, mockArgumentsHost);

      // then
      expect(mockStatus).toHaveBeenCalledWith(expected);
    });

    it('should call response status json method with body from domain validation error', () => {
      // given
      const fixedDate: Date = new Date('2017-06-13T04:41:20');
      // @ts-ignore
      jest.spyOn(global, 'Date').mockImplementationOnce(() => fixedDate);

      const domainValidationError: DomainValidationError = {
        name: 'DomainValidationError',
        message: 'A domain validation error',
        domainErrors: {
          aFieldName: {
            i18nMessageKeys: ['An i18n message key'],
            messages: ['A message'],
          },
        } as DomainErrorsInterface,
      } as DomainValidationError;

      const expected: DomainValidationErrorResponseInterface = {
        statusCode: 400,
        timestamp: fixedDate.toISOString(),
        name: 'DomainValidationError',
        message: 'A domain validation error',
        domainErrors: {
          aFieldName: {
            i18nMessageKeys: ['An i18n message key'],
            messages: ['A message'],
          },
        },
      };

      // when
      new DomainValidationErrorFilter().catch(domainValidationError, mockArgumentsHost);

      // then
      expect(mockJson).toHaveBeenCalledWith(expected);
    });
  });
});

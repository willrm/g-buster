import { ArgumentsHost } from '@nestjs/common';
import { DuplicateItemError } from '../../../../domain/common-errors/duplicate-item.error';
import { DuplicateItemErrorFilter } from '../duplicate-item-error.filter';
import { DuplicateItemErrorResponseInterface } from '../models/duplicate-item-error-response.interface';

describe('infrastructure/rest/filters/DuplicateItemErrorFilter', () => {
  let mockArgumentsHost: ArgumentsHost;
  let mockStatus: jest.Mock;
  let mockJson: jest.Mock;

  beforeEach(() => {
    mockStatus = jest.fn();
    mockJson = jest.fn();

    mockStatus.mockImplementation(() => {
      return {
        json: mockJson,
      };
    });

    mockArgumentsHost = {
      switchToHttp: () => ({
        getResponse: () => ({
          status: mockStatus,
        }),
      }),
    } as ArgumentsHost;
  });

  describe('catch()', () => {
    it('should call response status method with http bad request status code', () => {
      // given
      const itemNotFoundError: DuplicateItemError = {} as DuplicateItemError;
      const expected: number = 400;

      // when
      new DuplicateItemErrorFilter().catch(itemNotFoundError, mockArgumentsHost);

      // then
      expect(mockStatus).toHaveBeenCalledWith(expected);
    });

    it('should call response status json method with body from duplicate item error', () => {
      // given
      const fixedDate: Date = new Date('2017-06-13T04:41:20');
      // @ts-ignore
      jest.spyOn(global, 'Date').mockImplementationOnce(() => fixedDate);

      const itemNotFoundError: DuplicateItemError = {
        name: 'DuplicateItemError',
        message: 'A duplicate item error',
      } as DuplicateItemError;

      const expected: DuplicateItemErrorResponseInterface = {
        statusCode: 400,
        timestamp: fixedDate.toISOString(),
        name: 'DuplicateItemError',
        message: 'A duplicate item error',
      };

      // when
      new DuplicateItemErrorFilter().catch(itemNotFoundError, mockArgumentsHost);

      // then
      expect(mockJson).toHaveBeenCalledWith(expected);
    });
  });
});

import { ArgumentsHost } from '@nestjs/common';
import { AccessDeniedError } from '../../../../domain/common-errors/access-denied.error';
import { AccessDeniedErrorFilter } from '../access-denied-error.filter';
import { AccessDeniedErrorResponseInterface } from '../models/access-denied-error-response.interface';

describe('infrastructure/rest/filters/AccessDeniedErrorFilter', () => {
  let mockArgumentsHost: ArgumentsHost;
  let mockStatus: jest.Mock;
  let mockJson: jest.Mock;

  beforeEach(() => {
    mockStatus = jest.fn();
    mockJson = jest.fn();

    mockStatus.mockImplementation(() => {
      return {
        json: mockJson,
      };
    });

    mockArgumentsHost = {
      switchToHttp: () => ({
        getResponse: () => ({
          status: mockStatus,
        }),
      }),
    } as ArgumentsHost;
  });

  describe('catch()', () => {
    it('should call response status method with http forbidden status code', () => {
      // given
      const accessDeniedError: AccessDeniedError = {} as AccessDeniedError;
      const expected: number = 403;

      // when
      new AccessDeniedErrorFilter().catch(accessDeniedError, mockArgumentsHost);

      // then
      expect(mockStatus).toHaveBeenCalledWith(expected);
    });

    it('should call response status json method with body from access denied error', () => {
      // given
      const fixedDate: Date = new Date('2017-06-13T04:41:20');
      // @ts-ignore
      jest.spyOn(global, 'Date').mockImplementationOnce(() => fixedDate);

      const accessDeniedError: AccessDeniedError = {
        name: 'AccessDeniedError',
        message: 'An access denied error',
      } as AccessDeniedError;

      const expected: AccessDeniedErrorResponseInterface = {
        statusCode: 403,
        timestamp: fixedDate.toISOString(),
        name: 'AccessDeniedError',
        message: 'An access denied error',
      };

      // when
      new AccessDeniedErrorFilter().catch(accessDeniedError, mockArgumentsHost);

      // then
      expect(mockJson).toHaveBeenCalledWith(expected);
    });
  });
});

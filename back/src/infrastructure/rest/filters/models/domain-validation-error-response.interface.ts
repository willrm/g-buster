import { DomainFieldErrorsInterface } from '../../../../domain/validation/domain-field-errors.interface';
import { DefaultErrorResponseInterface } from './default-error-response.interface';

export type DomainFieldErrorsResponseInterface = DomainFieldErrorsInterface;
export interface DomainErrorsResponseInterface {
  [fieldName: string]: DomainFieldErrorsResponseInterface;
}

export interface DomainValidationErrorResponseInterface extends DefaultErrorResponseInterface {
  domainErrors: DomainErrorsResponseInterface;
}

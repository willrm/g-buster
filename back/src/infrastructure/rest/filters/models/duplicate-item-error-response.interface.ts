import { DefaultErrorResponseInterface } from './default-error-response.interface';

// tslint:disable-next-line:no-empty-interface
export interface DuplicateItemErrorResponseInterface extends DefaultErrorResponseInterface {}

import { ArgumentsHost, Catch, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { HttpArgumentsHost } from '@nestjs/common/interfaces';
import { Response } from 'express';
import { DomainValidationError } from '../../../domain/validation/domain-validation.error';
import { DomainValidationErrorResponseInterface } from './models/domain-validation-error-response.interface';

@Catch(DomainValidationError)
export class DomainValidationErrorFilter implements ExceptionFilter {
  catch(exception: DomainValidationError, host: ArgumentsHost): void {
    const ctx: HttpArgumentsHost = host.switchToHttp();
    const response: Response = ctx.getResponse<Response>();
    const status: HttpStatus = HttpStatus.BAD_REQUEST;

    response.status(status).json({
      statusCode: status,
      timestamp: new Date().toISOString(),
      name: exception.name,
      message: exception.message,
      domainErrors: exception.domainErrors,
    } as DomainValidationErrorResponseInterface);
  }
}

import { ArgumentsHost, Catch, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { HttpArgumentsHost } from '@nestjs/common/interfaces';
import { Response } from 'express';
import { AccessDeniedError } from '../../../domain/common-errors/access-denied.error';
import { AccessDeniedErrorResponseInterface } from './models/access-denied-error-response.interface';

@Catch(AccessDeniedError)
export class AccessDeniedErrorFilter implements ExceptionFilter {
  catch(exception: AccessDeniedError, host: ArgumentsHost): void {
    const ctx: HttpArgumentsHost = host.switchToHttp();
    const response: Response = ctx.getResponse<Response>();
    const status: HttpStatus = HttpStatus.FORBIDDEN;

    response.status(status).json({
      statusCode: status,
      timestamp: new Date().toISOString(),
      name: exception.name,
      message: exception.message,
    } as AccessDeniedErrorResponseInterface);
  }
}

import { ArgumentsHost, Catch, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { HttpArgumentsHost } from '@nestjs/common/interfaces';
import { Response } from 'express';
import { DuplicateItemError } from '../../../domain/common-errors/duplicate-item.error';
import { DuplicateItemErrorResponseInterface } from './models/duplicate-item-error-response.interface';

@Catch(DuplicateItemError)
export class DuplicateItemErrorFilter implements ExceptionFilter {
  catch(exception: DuplicateItemError, host: ArgumentsHost): void {
    const ctx: HttpArgumentsHost = host.switchToHttp();
    const response: Response = ctx.getResponse<Response>();
    const status: HttpStatus = HttpStatus.BAD_REQUEST;

    response.status(status).json({
      statusCode: status,
      timestamp: new Date().toISOString(),
      name: exception.name,
      message: exception.message,
    } as DuplicateItemErrorResponseInterface);
  }
}

import { Controller, Get, Inject, Param } from '@nestjs/common';
import { ApiParam, ApiResponse } from '@nestjs/swagger';
import { Lang } from '../../../domain/type-aliases';
import { GetI18nMessagesForLang } from '../../../use_cases/get-i18n-messages-for-lang';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../use_cases_proxy/use-case-proxy';
import { LANG_SWAGGER } from '../swagger/lang.swagger';
import { I18nMessagesResponseInterface } from './models/i18n-messages-response.interface';
import { I18nMessagesResponseSwagger } from './swagger/i18n-messages-response.swagger';

@Controller('/messages')
export class I18nMessagesController {
  constructor(
    @Inject(ProxyServicesDynamicModule.GET_I18N_MESSAGES_FOR_LANG_PROXY_SERVICE)
    private readonly getI18nMessagesForLangProxyService: UseCaseProxy<GetI18nMessagesForLang>
  ) {}

  @Get('/:lang')
  @ApiParam({ name: 'lang', enum: LANG_SWAGGER })
  @ApiResponse({ status: 200, type: I18nMessagesResponseSwagger })
  getI18nMessages(@Param('lang') lang: Lang): Promise<I18nMessagesResponseInterface> {
    return this.getI18nMessagesForLangProxyService.getInstance().execute(lang);
  }
}

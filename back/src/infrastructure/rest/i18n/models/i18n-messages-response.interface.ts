import { I18nMessagesInterface } from '../../../../domain/i18n/i18n-messages.interface';

export type I18nMessagesResponseInterface = I18nMessagesInterface;

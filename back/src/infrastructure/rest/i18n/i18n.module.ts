import { Module } from '@nestjs/common';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { I18nMessagesController } from './i18n-messages.controller';

@Module({
  imports: [ProxyServicesDynamicModule.register()],
  controllers: [I18nMessagesController],
})
export class I18nModule {}

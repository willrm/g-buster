import { I18nMessagesResponseInterface } from '../models/i18n-messages-response.interface';

export class I18nMessagesResponseSwagger implements I18nMessagesResponseInterface {
  [key: string]: string | { [p: string]: string };
}

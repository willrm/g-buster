import { I18nMessagesInterface } from '../../../../domain/i18n/i18n-messages.interface';
import { GetI18nMessagesForLang } from '../../../../use_cases/get-i18n-messages-for-lang';
import { UseCaseProxy } from '../../../use_cases_proxy/use-case-proxy';
import { I18nMessagesController } from '../i18n-messages.controller';
import { I18nMessagesResponseInterface } from '../models/i18n-messages-response.interface';

describe('infrastructure/rest/i18n/I18nMessagesController', () => {
  let i18nMessagesController: I18nMessagesController;
  let mockGetI18nMessagesForLangProxyService: UseCaseProxy<GetI18nMessagesForLang>;

  beforeEach(() => {
    mockGetI18nMessagesForLangProxyService = {} as UseCaseProxy<GetI18nMessagesForLang>;
    mockGetI18nMessagesForLangProxyService.getInstance = jest.fn();

    i18nMessagesController = new I18nMessagesController(mockGetI18nMessagesForLangProxyService);
  });

  describe('getI18nMessages()', () => {
    it('should return result from use case execution', () => {
      // given
      const expected: Promise<I18nMessagesInterface> = Promise.resolve({
        welcome: 'A welcome message',
        thanks: {
          you: 'A thank you message',
        },
      } as I18nMessagesInterface);
      (mockGetI18nMessagesForLangProxyService.getInstance as jest.Mock).mockImplementation(() => ({
        execute: jest.fn(() => expected),
      }));

      // when
      const result: Promise<I18nMessagesResponseInterface> = i18nMessagesController.getI18nMessages('fr-ca');

      // then
      expect(result).toBe(expected);
    });

    it('should pass lang from params to use case execution', () => {
      // given
      const execute: jest.Mock = jest.fn();
      (mockGetI18nMessagesForLangProxyService.getInstance as jest.Mock).mockImplementation(() => ({
        execute,
      }));
      const lang: string = 'fr-ca';

      // when
      i18nMessagesController.getI18nMessages(lang);

      // then
      expect(execute).toHaveBeenCalledWith(lang);
    });
  });
});

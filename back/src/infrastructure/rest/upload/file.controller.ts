import { Controller, Inject, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes, ApiResponse } from '@nestjs/swagger';
import { TemporaryFileInterface } from '../../../domain/temporary-file/temporary-file.interface';
import { TemporaryFileRawInterface } from '../../../domain/temporary-file/temporary-file.raw.interface';
import { CreateTemporaryFile } from '../../../use_cases/create-temporary-file';
import { AuthenticatedWithRolesGuard } from '../../config/authentication/authenticated-with-roles.guard';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../use_cases_proxy/use-case-proxy';
import { PostTemporaryFileRequestInterface } from './models/post-temporary-file-request.interface';
import { TemporaryFileResponseInterface } from './models/temporary-file-response.interface';
import { PostTemporaryFileRequestSwagger } from './swagger/post-temporary-file-request.swagger';
import { TemporaryFileResponseSwagger } from './swagger/temporary-file-response.swagger';

@Controller('/file')
export class FileController {
  constructor(
    @Inject(ProxyServicesDynamicModule.CREATE_TEMPORARY_IMAGE_PROXY_SERVICE)
    private readonly createTemporaryImageProxyService: UseCaseProxy<CreateTemporaryFile>
  ) {}

  @UseGuards(AuthenticatedWithRolesGuard)
  @Post('/temporary')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: PostTemporaryFileRequestSwagger })
  @ApiResponse({ status: 201, type: TemporaryFileResponseSwagger })
  async uploadTemporaryFile(@UploadedFile() body: PostTemporaryFileRequestInterface): Promise<TemporaryFileResponseInterface> {
    const temporaryFileRaw: TemporaryFileRawInterface = {
      data: body.buffer,
      mimeType: body.mimetype,
      sizeInBytes: body.size,
      filename: body.originalname,
    } as TemporaryFileRawInterface;
    const temporaryFile: TemporaryFileInterface = await this.createTemporaryImageProxyService.getInstance().execute(temporaryFileRaw);

    return temporaryFile as TemporaryFileResponseInterface;
  }
}

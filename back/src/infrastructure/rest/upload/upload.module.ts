import { Module } from '@nestjs/common';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { FileController } from './file.controller';

@Module({
  imports: [ProxyServicesDynamicModule.register()],
  controllers: [FileController],
})
export class UploadModule {}

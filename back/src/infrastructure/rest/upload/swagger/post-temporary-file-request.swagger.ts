import { ApiProperty } from '@nestjs/swagger';

export class PostTemporaryFileRequestSwagger {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: unknown;
}

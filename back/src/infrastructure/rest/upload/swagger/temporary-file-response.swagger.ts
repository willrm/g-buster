import { ApiProperty } from '@nestjs/swagger';
import { CommonTypeAliases } from '../../common-models/common-type-aliases';
import { TemporaryFileResponseInterface } from '../models/temporary-file-response.interface';

export class TemporaryFileResponseSwagger implements TemporaryFileResponseInterface {
  @ApiProperty()
  base64: string;
  @ApiProperty()
  id: CommonTypeAliases.TemporaryFileId;
  @ApiProperty()
  mimeType: string;
  @ApiProperty()
  filename: string;
  @ApiProperty()
  uuid: CommonTypeAliases.Uuid;
  @ApiProperty({ type: 'string', format: 'date-time' })
  createdAt: Date;
}

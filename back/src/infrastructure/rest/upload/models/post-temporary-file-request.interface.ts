export interface PostTemporaryFileRequestInterface {
  // see MulterOptions (@nestjs/platform-express/multer/interfaces/multer-options.interface.d.ts)
  mimetype: string;
  size: number;
  buffer: Buffer;
  originalname: string;
}

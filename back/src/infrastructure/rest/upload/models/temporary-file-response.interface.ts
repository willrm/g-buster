import { TemporaryFileInterface } from '../../../../domain/temporary-file/temporary-file.interface';

export type TemporaryFileResponseInterface = TemporaryFileInterface;

import { TemporaryFile } from '../../../../domain/temporary-file/temporary-file';
import { TemporaryFileRawInterface } from '../../../../domain/temporary-file/temporary-file.raw.interface';
import { CreateTemporaryFile } from '../../../../use_cases/create-temporary-file';
import { UseCaseProxy } from '../../../use_cases_proxy/use-case-proxy';
import { FileController } from '../file.controller';
import { PostTemporaryFileRequestInterface } from '../models/post-temporary-file-request.interface';
import { TemporaryFileResponseInterface } from '../models/temporary-file-response.interface';

describe('infrastructure/rest/upload/FileController', () => {
  let imageController: FileController;
  let mockCreateTemporaryImageProxyService: UseCaseProxy<CreateTemporaryFile>;

  beforeEach(() => {
    mockCreateTemporaryImageProxyService = {} as UseCaseProxy<CreateTemporaryFile>;
    mockCreateTemporaryImageProxyService.getInstance = jest.fn();

    imageController = new FileController(mockCreateTemporaryImageProxyService);
  });

  describe('uploadTemporaryFile()', () => {
    let mockCreateTemporaryImage: CreateTemporaryFile;
    beforeEach(() => {
      mockCreateTemporaryImage = {} as CreateTemporaryFile;
      mockCreateTemporaryImage.execute = jest.fn();
      (mockCreateTemporaryImageProxyService.getInstance as jest.Mock).mockReturnValue(mockCreateTemporaryImage);
    });

    it('should call use case with raw data', () => {
      // given
      const body: PostTemporaryFileRequestInterface = {
        mimetype: 'image/whatever',
        buffer: Buffer.alloc(1, 'a'),
        size: 1337,
        originalname: 'a-file.extension',
      } as PostTemporaryFileRequestInterface;

      // when
      imageController.uploadTemporaryFile(body);

      // then
      expect(mockCreateTemporaryImage.execute).toHaveBeenCalledWith({
        mimeType: 'image/whatever',
        data: Buffer.alloc(1, 'a'),
        sizeInBytes: 1337,
        filename: 'a-file.extension',
      } as TemporaryFileRawInterface);
    });

    it('should return result from use case', async () => {
      // given
      const expected: TemporaryFile = { mimeType: 'image/whatever' } as TemporaryFile;
      (mockCreateTemporaryImage.execute as jest.Mock).mockReturnValue(expected);

      // when
      const result: TemporaryFileResponseInterface = await imageController.uploadTemporaryFile({} as PostTemporaryFileRequestInterface);

      // then
      expect(result).toBe(expected);
    });
  });
});

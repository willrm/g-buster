import { Request, Response } from 'express';
import { JobOfferInterface } from '../../../../domain/job-offer/job-offer.interface';
import { JobOfferId } from '../../../../domain/type-aliases';
import { UserRole } from '../../../../domain/user/user-enums';
import { UserInterface } from '../../../../domain/user/user.interface';
import { UserRawInterface } from '../../../../domain/user/user.raw.interface';
import { GetCurrentUser } from '../../../../use_cases/get-current-user';
import { GetUserJobOfferById } from '../../../../use_cases/get-user-job-offer-by-id';
import { GetUserJobOffers } from '../../../../use_cases/get-user-job-offers';
import { LogInUser } from '../../../../use_cases/log-in-user';
import { UseCaseProxy } from '../../../use_cases_proxy/use-case-proxy';
import { Auth0ResponseInterface, Auth0UserMetadataInterface } from '../../../vendors/auth0/auth0-response.interface';
import { Auth0UserMetadataAdapterService } from '../../../vendors/auth0/auth0-user-metadata-adapter.service';
import { JobOfferResponseInterface } from '../../career/models/job-offer-response.interface';
import { UserResponseInterface } from '../models/user-response.interface';
import { UserController } from '../user.controller';

describe('infrastructure/rest/user/UserController', () => {
  let userController: UserController;
  let mockLogInUserProxyService: UseCaseProxy<LogInUser>;
  let mockGetCurrentUserProxyService: UseCaseProxy<GetCurrentUser>;
  let mockGetUserJobOffersProxyService: UseCaseProxy<GetUserJobOffers>;
  let mockGetUserJobOfferByIdProxyService: UseCaseProxy<GetUserJobOfferById>;
  let mockAuth0UserMetadataAdapterService: Auth0UserMetadataAdapterService;
  let mockRequest: Request;

  beforeEach(() => {
    mockLogInUserProxyService = {} as UseCaseProxy<LogInUser>;
    mockLogInUserProxyService.getInstance = jest.fn();

    mockGetCurrentUserProxyService = {} as UseCaseProxy<GetCurrentUser>;
    mockGetCurrentUserProxyService.getInstance = jest.fn();

    mockGetUserJobOffersProxyService = {} as UseCaseProxy<GetUserJobOffers>;
    mockGetUserJobOffersProxyService.getInstance = jest.fn();

    mockGetUserJobOfferByIdProxyService = {} as UseCaseProxy<GetUserJobOfferById>;
    mockGetUserJobOfferByIdProxyService.getInstance = jest.fn();

    mockAuth0UserMetadataAdapterService = {} as Auth0UserMetadataAdapterService;
    mockAuth0UserMetadataAdapterService.toUserRaw = jest.fn();

    mockRequest = {} as Request;
    mockRequest.session = {} as CookieSessionInterfaces.CookieSessionOptions;
    mockRequest.route = { path: '' };
    mockRequest.res = {} as Response;
    mockRequest.res.redirect = jest.fn();

    userController = new UserController(
      mockLogInUserProxyService,
      mockGetCurrentUserProxyService,
      mockGetUserJobOffersProxyService,
      mockGetUserJobOfferByIdProxyService,
      mockAuth0UserMetadataAdapterService
    );
  });

  describe('getLogin()', () => {
    it('should add redirectUrl query param to session', async () => {
      // given
      const redirectUrlQueryParam: string = '/a/url/where/redirect';

      // when
      await userController.getLogin(mockRequest, redirectUrlQueryParam);

      // then
      expect(mockRequest.session.redirectUrl).toBe(redirectUrlQueryParam);
    });
    it('should redirect to login form', async () => {
      // given
      mockRequest.route.path = '/path/to/login';

      // when
      await userController.getLogin(mockRequest, '');

      // then
      expect(mockRequest.res.redirect).toHaveBeenCalledWith('/path/to/login/form');
    });
  });

  describe('getLoginForm()', () => {
    it('should redirect to profile when authenticated', async () => {
      // given
      mockRequest.route.path = '/path/to/login/form';

      // when
      await userController.getLoginForm(mockRequest);

      // then
      expect(mockRequest.res.redirect).toHaveBeenCalledWith('/path/to/profile');
    });
  });

  describe('getLoginCallback()', () => {
    let mockLogInUser: LogInUser;

    beforeEach(() => {
      mockLogInUser = {} as LogInUser;
      mockLogInUser.execute = jest.fn();
      (mockLogInUserProxyService.getInstance as jest.Mock).mockReturnValue(mockLogInUser);

      mockRequest.user = {} as Express.User;
    });

    it('should execute use case with raw data built from auth0 response', async () => {
      // given
      const auth0Response: Auth0ResponseInterface = {
        'https://career.genium360.ca/user_metadata': {} as Auth0UserMetadataInterface,
      };
      // @ts-ignore
      mockRequest.user._json = auth0Response;

      const userRaw: UserRawInterface = { id: 123456 } as UserRawInterface;
      (mockAuth0UserMetadataAdapterService.toUserRaw as jest.Mock).mockReturnValue(userRaw);

      // when
      await userController.getLoginCallback(mockRequest);

      // then
      expect(mockAuth0UserMetadataAdapterService.toUserRaw).toHaveBeenCalledWith(auth0Response);
      expect(mockLogInUser.execute).toHaveBeenCalledWith(userRaw);
    });
    it('should redirect to redirectUrl value from session', async () => {
      // given
      mockRequest.session.redirectUrl = '/a/url/where/redirect';

      // when
      await userController.getLoginCallback(mockRequest);

      // then
      expect(mockRequest.res.redirect).toHaveBeenCalledWith('/a/url/where/redirect');
    });
    it('should redirect to profile when no redirectUrl value in session', async () => {
      // given
      mockRequest.session.redirectUrl = '';
      mockRequest.route.path = '/path/to/login/callback';

      // when
      await userController.getLoginCallback(mockRequest);

      // then
      expect(mockRequest.res.redirect).toHaveBeenCalledWith('/path/to/profile');
    });
    it('should remove existing redirectUrl from session', async () => {
      // given
      mockRequest.session.redirectUrl = '/a/url/where/redirect';

      // when
      await userController.getLoginCallback(mockRequest);

      // then
      expect(mockRequest.session.redirectUrl).toBeUndefined();
    });
  });

  describe('getCurrentUser()', () => {
    let mockGetCurrentUser: GetCurrentUser;

    beforeEach(() => {
      mockGetCurrentUser = {} as GetCurrentUser;
      mockGetCurrentUser.execute = jest.fn();
      (mockGetCurrentUserProxyService.getInstance as jest.Mock).mockReturnValue(mockGetCurrentUser);
    });

    it('should return user retrieved from use case', async () => {
      // given
      const expected: UserInterface = {
        id: '123456789',
        firstName: 'Test',
        role: UserRole.CANDIDATE,
      } as UserInterface;
      (mockGetCurrentUser.execute as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: UserResponseInterface = await userController.getCurrentUser();

      // then
      expect(result).toBe(expected as UserResponseInterface);
    });
  });

  describe('getJobOffers()', () => {
    let mockGetUserJobOffers: GetUserJobOffers;
    beforeEach(() => {
      mockGetUserJobOffers = {} as GetUserJobOffers;
      mockGetUserJobOffers.execute = jest.fn();
      (mockGetUserJobOffers.execute as jest.Mock).mockReturnValue(Promise.resolve([]));

      (mockGetUserJobOffersProxyService.getInstance as jest.Mock).mockReturnValue(mockGetUserJobOffers);
    });

    it('should call use case', () => {
      // when
      userController.getJobOffers();

      // then
      expect(mockGetUserJobOffers.execute).toHaveBeenCalled();
    });

    it('should return result from use case mapped to response interface', async () => {
      // given
      const jobOffer1: JobOfferInterface = { title: 'An awesome job' } as JobOfferInterface;
      const jobOffer2: JobOfferInterface = { title: 'Another awesome job' } as JobOfferInterface;
      (mockGetUserJobOffers.execute as jest.Mock).mockReturnValue(Promise.resolve([jobOffer1, jobOffer2]));

      // when
      const result: JobOfferResponseInterface[] = await userController.getJobOffers();

      // then
      expect(result).toStrictEqual([jobOffer1 as JobOfferResponseInterface, jobOffer2 as JobOfferResponseInterface] as JobOfferResponseInterface[]);
    });
  });

  describe('getJobOffer()', () => {
    let mockGetJobOfferById: GetUserJobOfferById;
    beforeEach(() => {
      mockGetJobOfferById = {} as GetUserJobOfferById;
      mockGetJobOfferById.execute = jest.fn();
      (mockGetUserJobOfferByIdProxyService.getInstance as jest.Mock).mockReturnValue(mockGetJobOfferById);
    });

    it('should call use case with raw data', () => {
      // when
      userController.getJobOffer('42');

      // then
      expect(mockGetJobOfferById.execute).toHaveBeenCalledWith('42' as JobOfferId);
    });

    it('should return result from use case mapped to response interface', async () => {
      // given
      const expected: JobOfferInterface = { id: '42', title: 'A job offer title' } as JobOfferInterface;
      (mockGetJobOfferById.execute as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: JobOfferResponseInterface = await userController.getJobOffer('42');

      // then
      expect(result).toBe(expected as JobOfferResponseInterface);
    });
  });
});

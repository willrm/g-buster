import { ApiProperty } from '@nestjs/swagger';
import { CommonTypeAliases } from '../../common-models/common-type-aliases';
import { UserEnums } from '../models/user-constraints-and-enums';
import { UserResponseInterface } from '../models/user-response.interface';

export class UserResponseSwagger implements UserResponseInterface {
  @ApiProperty()
  id: CommonTypeAliases.UserId;

  @ApiProperty()
  email: string;

  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  companyName: string;

  @ApiProperty({ enum: UserEnums.UserRole })
  role: UserEnums.UserRole;

  @ApiProperty()
  salesforceId: string;
}

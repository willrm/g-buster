import { UserInterface } from '../../../../domain/user/user.interface';

export type UserResponseInterface = UserInterface;

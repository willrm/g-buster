import { Module } from '@nestjs/common';
import { AuthenticationModule } from '../../config/authentication/authentication.module';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { Auth0Module } from '../../vendors/auth0/auth0.module';
import { UserController } from './user.controller';

@Module({
  imports: [AuthenticationModule, ProxyServicesDynamicModule.register(), Auth0Module],
  controllers: [UserController],
})
export class UserModule {}

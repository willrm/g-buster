import { Controller, Get, Inject, Param, Query, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { Request } from 'express';
import { JobOfferInterface } from '../../../domain/job-offer/job-offer.interface';
import { JobOfferId } from '../../../domain/type-aliases';
import { UserRole } from '../../../domain/user/user-enums';
import { UserInterface } from '../../../domain/user/user.interface';
import { UserRawInterface } from '../../../domain/user/user.raw.interface';
import { GetCurrentUser } from '../../../use_cases/get-current-user';
import { GetUserJobOfferById } from '../../../use_cases/get-user-job-offer-by-id';
import { GetUserJobOffers } from '../../../use_cases/get-user-job-offers';
import { LogInUser } from '../../../use_cases/log-in-user';
import { Auth0Strategy } from '../../config/authentication/auth0-strategy';
import { AuthenticatedWithRolesGuard, UserRoles } from '../../config/authentication/authenticated-with-roles.guard';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../use_cases_proxy/use-case-proxy';
import { Auth0ResponseInterface } from '../../vendors/auth0/auth0-response.interface';
import { Auth0UserMetadataAdapterService } from '../../vendors/auth0/auth0-user-metadata-adapter.service';
import { JobOfferResponseInterface } from '../career/models/job-offer-response.interface';
import { JobOfferNotFoundResponseSwagger } from '../career/swagger/job-offer-not-found-response.swagger';
import { JobOfferResponseSwagger } from '../career/swagger/job-offer-response.swagger';
import { UserResponseInterface } from './models/user-response.interface';
import { UserResponseSwagger } from './swagger/user-response.swagger';

@Controller('/')
export class UserController {
  constructor(
    @Inject(ProxyServicesDynamicModule.LOG_IN_USER_PROXY_SERVICE)
    private readonly logInUserProxyService: UseCaseProxy<LogInUser>,
    @Inject(ProxyServicesDynamicModule.GET_CURRENT_USER_PROXY_SERVICE)
    private readonly getCurrentUserProxyService: UseCaseProxy<GetCurrentUser>,
    @Inject(ProxyServicesDynamicModule.GET_USER_JOB_OFFERS_PROXY_SERVICE)
    private readonly getUserJobOffersProxyService: UseCaseProxy<GetUserJobOffers>,
    @Inject(ProxyServicesDynamicModule.GET_USER_JOB_OFFER_BY_ID_PROXY_SERVICE)
    private readonly getUserJobOfferByIdUseCaseProxyService: UseCaseProxy<GetUserJobOfferById>,
    private readonly auth0UserMetadataAdapterService: Auth0UserMetadataAdapterService
  ) {}

  @Get('/login')
  @ApiQuery({
    name: 'redirectUrl',
    description: 'URL where to redirect when login is successful',
    required: false,
    type: String,
  })
  async getLogin(@Req() request: Request, @Query('redirectUrl') redirectUrlQueryParam: string): Promise<void> {
    request.session.redirectUrl = redirectUrlQueryParam;
    request.res.redirect(`${request.route.path}/form`);
  }

  @UseGuards(AuthGuard(Auth0Strategy.AUTH0_STRATEGY_NAME))
  @Get('/login/form')
  async getLoginForm(@Req() request: Request): Promise<void> {
    request.res.redirect(this.buildProfileUrl(request.route.path));
  }

  @UseGuards(AuthGuard(Auth0Strategy.AUTH0_STRATEGY_NAME))
  @Get('/login/callback')
  async getLoginCallback(@Req() request: Request): Promise<void> {
    // @ts-ignore
    const auth0Response: Auth0ResponseInterface = request.user._json as Auth0ResponseInterface;
    const userRaw: UserRawInterface = this.auth0UserMetadataAdapterService.toUserRaw(auth0Response);

    await this.logInUserProxyService.getInstance().execute(userRaw);

    const redirectUrl: string = request.session.redirectUrl;
    delete request.session.redirectUrl;
    request.res.redirect(redirectUrl ? `${redirectUrl}` : this.buildProfileUrl(request.route.path));
  }

  @UseGuards(AuthenticatedWithRolesGuard)
  @Get('/profile')
  @ApiResponse({ status: 200, type: UserResponseSwagger })
  async getCurrentUser(): Promise<UserResponseInterface> {
    const user: UserInterface = await this.getCurrentUserProxyService.getInstance().execute();

    return user as UserResponseInterface;
  }

  @UseGuards(AuthenticatedWithRolesGuard)
  @UserRoles(UserRole.COMPANY)
  @Get('/job-offers')
  @ApiResponse({ status: 200, isArray: true, type: JobOfferResponseSwagger })
  async getJobOffers(): Promise<JobOfferResponseInterface[]> {
    const jobOffers: JobOfferInterface[] = await this.getUserJobOffersProxyService.getInstance().execute();

    return jobOffers.map((jobOffer: JobOfferInterface) => {
      return jobOffer as JobOfferResponseInterface;
    });
  }

  @UseGuards(AuthenticatedWithRolesGuard)
  @UserRoles(UserRole.COMPANY)
  @Get('/job-offers/:id')
  @ApiParam({ name: 'id' })
  @ApiResponse({ status: 200, type: JobOfferResponseSwagger })
  @ApiResponse({ status: 404, type: JobOfferNotFoundResponseSwagger })
  async getJobOffer(@Param('id') id: JobOfferId): Promise<JobOfferResponseInterface> {
    const jobOffer: JobOfferInterface = await this.getUserJobOfferByIdUseCaseProxyService.getInstance().execute(id);

    return jobOffer as JobOfferResponseInterface;
  }

  private buildProfileUrl(path: string): string {
    const baseUrl: string = path.substring(0, path.indexOf('/login'));

    return `${baseUrl}/profile`;
  }
}

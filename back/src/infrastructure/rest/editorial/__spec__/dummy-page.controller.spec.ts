import { DummyPageInterface } from '../../../../domain/dummy-page/dummy-page.interface';
import { Lang, Uid } from '../../../../domain/type-aliases';
import { GetDummyPage } from '../../../../use_cases/get-dummy-page';
import { UseCaseProxy } from '../../../use_cases_proxy/use-case-proxy';
import { DummyPageController } from '../dummy-page.controller';
import { DummyPageResponseInterface } from '../models/dummy-page-response.interface';

describe('infrastructure/rest/editorial/DummyPageController', () => {
  let dummyPageController: DummyPageController;
  let mockGetDummyPageProxyService: UseCaseProxy<GetDummyPage>;

  beforeEach(() => {
    mockGetDummyPageProxyService = {} as UseCaseProxy<GetDummyPage>;
    mockGetDummyPageProxyService.getInstance = jest.fn();

    dummyPageController = new DummyPageController(mockGetDummyPageProxyService);
  });

  describe('getPage()', () => {
    it('should return result from use case execution', () => {
      // given
      const expected: Promise<DummyPageInterface> = Promise.resolve({
        content: 'A content',
      } as DummyPageInterface);
      (mockGetDummyPageProxyService.getInstance as jest.Mock).mockImplementation(() => ({
        execute: jest.fn(() => expected),
      }));

      // when
      const result: Promise<DummyPageResponseInterface> = dummyPageController.getPage('fr-ca', 'a-uid-of-dummy-page');

      // then
      expect(result).toStrictEqual(expected);
    });

    it('should pass lang and uid from params to use case execution', () => {
      // given
      const execute: jest.Mock = jest.fn();
      (mockGetDummyPageProxyService.getInstance as jest.Mock).mockImplementation(() => ({
        execute,
      }));
      const lang: Lang = 'fr-ca';
      const uid: Uid = 'a-uid-of-dummy-page';

      // when
      dummyPageController.getPage(lang, uid);

      // then
      expect(execute).toHaveBeenCalledWith(lang, uid);
    });
  });
});

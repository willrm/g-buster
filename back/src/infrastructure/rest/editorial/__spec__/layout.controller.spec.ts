import { LayoutInterface } from '../../../../domain/layout/layout.interface';
import { Lang } from '../../../../domain/type-aliases';
import { GetLayout } from '../../../../use_cases/get-layout';
import { UseCaseProxy } from '../../../use_cases_proxy/use-case-proxy';
import { LayoutController } from '../layout.controller';
import { LayoutResponseInterface } from '../models/layout-response.interface';

describe('infrastructure/rest/editorial/LayoutController', () => {
  let layoutController: LayoutController;
  let mockGetLayoutProxyService: UseCaseProxy<GetLayout>;

  beforeEach(() => {
    mockGetLayoutProxyService = {} as UseCaseProxy<GetLayout>;
    mockGetLayoutProxyService.getInstance = jest.fn();

    layoutController = new LayoutController(mockGetLayoutProxyService);
  });

  describe('getLayout()', () => {
    it('should return result from use case execution', () => {
      // given
      const expected: Promise<LayoutInterface> = Promise.resolve({
        header: {},
        footer: {},
      } as LayoutInterface);
      (mockGetLayoutProxyService.getInstance as jest.Mock).mockImplementation(() => ({
        execute: jest.fn(() => expected),
      }));

      // when
      const result: Promise<LayoutResponseInterface> = layoutController.getLayout('fr-ca');

      // then
      expect(result).toStrictEqual(expected);
    });

    it('should pass lang from params to use case execution', () => {
      // given
      const execute: jest.Mock = jest.fn();
      (mockGetLayoutProxyService.getInstance as jest.Mock).mockImplementation(() => ({
        execute,
      }));
      const lang: Lang = 'fr-ca';

      // when
      layoutController.getLayout(lang);

      // then
      expect(execute).toHaveBeenCalledWith(lang);
    });
  });
});

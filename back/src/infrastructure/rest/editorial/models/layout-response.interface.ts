import { FooterInterface } from '../../../../domain/footer/footer.interface';
import { HeaderInterface } from '../../../../domain/header/header.interface';
import { LayoutInterface } from '../../../../domain/layout/layout.interface';

export type LayoutResponseInterface = LayoutInterface;
export type FooterResponseInterface = FooterInterface;
export type HeaderResponseInterface = HeaderInterface;

import { DummyPageInterface } from '../../../../domain/dummy-page/dummy-page.interface';
import { ImageInterface } from '../../../../domain/image/image.interface';

export type DummyPageResponseInterface = DummyPageInterface;
export type ImageResponseInterface = ImageInterface;

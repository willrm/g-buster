import { Controller, Get, Inject, Param } from '@nestjs/common';
import { ApiParam, ApiResponse } from '@nestjs/swagger';
import { DummyPageInterface } from '../../../domain/dummy-page/dummy-page.interface';
import { Lang, Uid } from '../../../domain/type-aliases';
import { GetDummyPage } from '../../../use_cases/get-dummy-page';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../use_cases_proxy/use-case-proxy';
import { LANG_SWAGGER } from '../swagger/lang.swagger';
import { DummyPageResponseInterface } from './models/dummy-page-response.interface';
import { DummyPageNotFoundResponseSwagger } from './swagger/dummy-page-not-found-response.swagger';
import { DummyPageResponseSwagger } from './swagger/dummy-page-response.swagger';

@Controller('/dummy-page')
export class DummyPageController {
  constructor(
    @Inject(ProxyServicesDynamicModule.GET_DUMMY_PAGE_PROXY_SERVICE) private readonly getDummyPageProxyService: UseCaseProxy<GetDummyPage>
  ) {}

  @Get('/:uid')
  @ApiParam({ name: 'lang', enum: LANG_SWAGGER })
  @ApiParam({ name: 'uid' })
  @ApiResponse({ status: 200, type: DummyPageResponseSwagger })
  @ApiResponse({ status: 404, type: DummyPageNotFoundResponseSwagger })
  async getPage(@Param('lang') lang: Lang, @Param('uid') uid: Uid): Promise<DummyPageResponseInterface> {
    const dummyPage: DummyPageInterface = await this.getDummyPageProxyService.getInstance().execute(lang, uid);

    return dummyPage as DummyPageResponseInterface;
  }
}

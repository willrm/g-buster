import { ApiProperty } from '@nestjs/swagger';
import { CommonTypeAliases } from '../../common-models/common-type-aliases';
import { DummyPageResponseInterface } from '../models/dummy-page-response.interface';
import { ImageSwagger } from './image.swagger';

export class DummyPageResponseSwagger implements DummyPageResponseInterface {
  @ApiProperty()
  content: CommonTypeAliases.HtmlMarkup;
  @ApiProperty({ type: 'string', format: 'date-time' })
  generatedDate: Date;
  @ApiProperty()
  image: ImageSwagger;
  @ApiProperty()
  title: CommonTypeAliases.HtmlMarkup;
}

import { ApiProperty } from '@nestjs/swagger';
import { ImageResponseInterface } from '../models/dummy-page-response.interface';

export class ImageSwagger implements ImageResponseInterface {
  @ApiProperty()
  alternativeText: string;
  @ApiProperty()
  source: string;
}

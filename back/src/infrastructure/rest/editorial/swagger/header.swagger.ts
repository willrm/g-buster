import { ApiProperty } from '@nestjs/swagger';
import { HeaderResponseInterface } from '../models/layout-response.interface';
import { ImageSwagger } from './image.swagger';

export class HeaderSwagger implements HeaderResponseInterface {
  @ApiProperty()
  logo: ImageSwagger;
  @ApiProperty()
  title: string;
}

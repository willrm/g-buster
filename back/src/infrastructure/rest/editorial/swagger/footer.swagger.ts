import { ApiProperty } from '@nestjs/swagger';
import { FooterResponseInterface } from '../models/layout-response.interface';

export class FooterSwagger implements FooterResponseInterface {
  @ApiProperty()
  copyright: string;
}

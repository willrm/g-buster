import { ApiProperty } from '@nestjs/swagger';
import { LayoutResponseInterface } from '../models/layout-response.interface';
import { FooterSwagger } from './footer.swagger';
import { HeaderSwagger } from './header.swagger';

export class LayoutResponseSwagger implements LayoutResponseInterface {
  @ApiProperty()
  footer: FooterSwagger;
  @ApiProperty()
  header: HeaderSwagger;
}

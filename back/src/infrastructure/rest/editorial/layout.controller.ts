import { Controller, Get, Inject, Param } from '@nestjs/common';
import { ApiParam, ApiResponse } from '@nestjs/swagger';
import { LayoutInterface } from '../../../domain/layout/layout.interface';
import { Lang } from '../../../domain/type-aliases';

import { GetLayout } from '../../../use_cases/get-layout';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../use_cases_proxy/use-case-proxy';
import { LANG_SWAGGER } from '../swagger/lang.swagger';
import { LayoutResponseInterface } from './models/layout-response.interface';
import { LayoutResponseSwagger } from './swagger/layout-response.swagger';

@Controller('/layout')
export class LayoutController {
  constructor(@Inject(ProxyServicesDynamicModule.GET_LAYOUT_PROXY_SERVICE) private readonly getLayoutProxyService: UseCaseProxy<GetLayout>) {}

  @Get('/')
  @ApiParam({ name: 'lang', enum: LANG_SWAGGER })
  @ApiResponse({ status: 200, description: 'Dynamic layout content', type: LayoutResponseSwagger })
  async getLayout(@Param('lang') lang: Lang): Promise<LayoutResponseInterface> {
    const layoutInterface: LayoutInterface = await this.getLayoutProxyService.getInstance().execute(lang);

    return layoutInterface as LayoutResponseInterface;
  }
}

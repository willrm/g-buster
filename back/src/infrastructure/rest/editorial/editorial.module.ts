import { Module } from '@nestjs/common';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { DummyPageController } from './dummy-page.controller';
import { LayoutController } from './layout.controller';

@Module({
  imports: [ProxyServicesDynamicModule.register()],
  controllers: [DummyPageController, LayoutController],
})
export class EditorialModule {}

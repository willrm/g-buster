import { CompanyInterface } from '../../../../domain/company/company.interface';
import { CompanyId } from '../../../../domain/type-aliases';
import { GetCompanies } from '../../../../use_cases/get-companies';
import { PublishCompany } from '../../../../use_cases/publish-company';
import { UpdateUnpublishedCompany } from '../../../../use_cases/update-unpublished-company';
import { UseCaseProxy } from '../../../use_cases_proxy/use-case-proxy';
import { CompanyResponseInterface } from '../../career/models/company-response.interface';
import { PutCompanyRequestInterface } from '../../career/models/put-company-request.interface';
import { AdminController } from '../admin.controller';

describe('infrastructure/rest/admin/AdminController', () => {
  let adminController: AdminController;
  let mockGetCompaniesProxyService: UseCaseProxy<GetCompanies>;
  let mockPublishCompanyProxyService: UseCaseProxy<PublishCompany>;
  let mockUpdateUnpublishedCompanyProxyService: UseCaseProxy<UpdateUnpublishedCompany>;

  beforeEach(() => {
    mockGetCompaniesProxyService = {} as UseCaseProxy<GetCompanies>;
    mockGetCompaniesProxyService.getInstance = jest.fn();

    mockPublishCompanyProxyService = {} as UseCaseProxy<PublishCompany>;
    mockPublishCompanyProxyService.getInstance = jest.fn();

    mockUpdateUnpublishedCompanyProxyService = {} as UseCaseProxy<UpdateUnpublishedCompany>;
    mockUpdateUnpublishedCompanyProxyService.getInstance = jest.fn();

    adminController = new AdminController(mockGetCompaniesProxyService, mockPublishCompanyProxyService, mockUpdateUnpublishedCompanyProxyService);
  });

  describe('getCompanies()', () => {
    let mockGetCompanies: GetCompanies;
    beforeEach(() => {
      mockGetCompanies = {} as GetCompanies;
      mockGetCompanies.execute = jest.fn();
      (mockGetCompaniesProxyService.getInstance as jest.Mock).mockReturnValue(mockGetCompanies);
    });

    it('should call use case', () => {
      // when
      // @ts-ignore
      adminController.getCompanies(null);

      // then
      expect(mockGetCompanies.execute).toHaveBeenCalled();
    });

    it('should return result from use case mapped as response interface array', async () => {
      // given
      const expected: CompanyInterface[] = [{ name: 'A company name' } as CompanyInterface];
      (mockGetCompanies.execute as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: CompanyResponseInterface[] = await adminController.getCompanies();

      // then
      expect(result).toStrictEqual(expected as CompanyResponseInterface[]);
    });
  });

  describe('updateCompany()', () => {
    const companyId: CompanyId = 'companyId';

    let mockPublishCompany: PublishCompany;
    let mockUpdateUnpublishedCompany: UpdateUnpublishedCompany;
    let companyPayload: PutCompanyRequestInterface;

    beforeEach(() => {
      mockPublishCompany = {} as PublishCompany;
      mockUpdateUnpublishedCompany = {} as UpdateUnpublishedCompany;

      mockPublishCompany.execute = jest.fn();
      mockUpdateUnpublishedCompany.execute = jest.fn();

      (mockPublishCompanyProxyService.getInstance as jest.Mock).mockReturnValue(mockPublishCompany);
      (mockUpdateUnpublishedCompanyProxyService.getInstance as jest.Mock).mockReturnValue(mockUpdateUnpublishedCompany);

      companyPayload = {} as PutCompanyRequestInterface;
    });

    it('should call update company use case when raw do not contain publish flag', async () => {
      // when
      await adminController.updateCompany(companyId, companyPayload);

      // then
      expect(mockUpdateUnpublishedCompany.execute).toHaveBeenCalledWith(companyId, companyPayload);
    });

    it('should call publish company use case when raw contains publish flag', async () => {
      // given
      companyPayload.published = true;

      // when
      await adminController.updateCompany(companyId, companyPayload);

      // then
      expect(mockPublishCompany.execute).toHaveBeenCalledWith(companyId);
    });
  });
});

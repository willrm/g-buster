import { Module } from '@nestjs/common';
import { AuthenticationModule } from '../../config/authentication/authentication.module';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { Auth0Module } from '../../vendors/auth0/auth0.module';
import { AdminController } from './admin.controller';

@Module({
  imports: [AuthenticationModule, ProxyServicesDynamicModule.register(), Auth0Module],
  controllers: [AdminController],
})
export class AdminModule {}

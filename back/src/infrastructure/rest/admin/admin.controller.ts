import { Body, Controller, Get, Inject, Param, Put, UseGuards } from '@nestjs/common';
import { ApiBody, ApiParam, ApiResponse } from '@nestjs/swagger';
import { CompanyInterface } from '../../../domain/company/company.interface';
import { CompanyRawInterface } from '../../../domain/company/company.raw.interface';
import { CompanyId } from '../../../domain/type-aliases';
import { UserRole } from '../../../domain/user/user-enums';
import { GetCompanies } from '../../../use_cases/get-companies';
import { PublishCompany } from '../../../use_cases/publish-company';
import { UpdateUnpublishedCompany } from '../../../use_cases/update-unpublished-company';
import { AuthenticatedWithRolesGuard, UserRoles } from '../../config/authentication/authenticated-with-roles.guard';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../use_cases_proxy/use-case-proxy';
import { CompanyResponseInterface } from '../career/models/company-response.interface';
import { PutCompanyRequestInterface } from '../career/models/put-company-request.interface';
import { CompanyResponseSwagger } from '../career/swagger/company-response.swagger';
import { PutCompanyRequestSwagger } from './swagger/put-company-request.swagger';

@Controller('/')
export class AdminController {
  constructor(
    @Inject(ProxyServicesDynamicModule.GET_COMPANIES_PROXY_SERVICE)
    private readonly getCompaniesProxyService: UseCaseProxy<GetCompanies>,
    @Inject(ProxyServicesDynamicModule.PUBLISH_COMPANY_PROXY_SERVICE)
    private readonly publishCompanyProxyService: UseCaseProxy<PublishCompany>,
    @Inject(ProxyServicesDynamicModule.UPDATE_UNPUBLISHED_COMPANY_PROXY_SERVICE)
    private readonly updateUnpublishedCompanyProxyService: UseCaseProxy<UpdateUnpublishedCompany>
  ) {}

  @UseGuards(AuthenticatedWithRolesGuard)
  @UserRoles(UserRole.ADMIN)
  @Get('companies')
  @ApiResponse({ status: 200, type: CompanyResponseSwagger, isArray: true })
  @ApiResponse({ status: 403, description: 'Access forbidden' })
  async getCompanies(): Promise<CompanyResponseInterface[]> {
    const companies: CompanyInterface[] = await this.getCompaniesProxyService.getInstance().execute();

    return companies as CompanyResponseInterface[];
  }

  @UseGuards(AuthenticatedWithRolesGuard)
  @UserRoles(UserRole.ADMIN)
  @Put('companies/:id')
  @ApiParam({ name: 'id' })
  @ApiBody({ type: PutCompanyRequestSwagger })
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 403, description: 'Access forbidden' })
  async updateCompany(@Param('id') id: CompanyId, @Body() putCompanyRequest: PutCompanyRequestInterface): Promise<void> {
    if (putCompanyRequest.published) {
      await this.publishCompanyProxyService.getInstance().execute(id);
    } else {
      await this.updateUnpublishedCompanyProxyService.getInstance().execute(id, putCompanyRequest as CompanyRawInterface);
    }
  }
}

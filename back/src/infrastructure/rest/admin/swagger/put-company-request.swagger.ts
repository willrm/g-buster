import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { ActivityLocationsRawInterface } from '../../../../domain/activity-locations/activity-locations.raw.interface';
import { CompanyConstraints, CompanyEnums } from '../../career/models/company-constraints-and-enums';
import { PutCompanyRequestInterface } from '../../career/models/put-company-request.interface';
import { SocialNetworksSwagger } from '../../career/swagger/social-networks.swagger';
import { ValueSwagger } from '../../career/swagger/value.swagger';

export class PutCompanyRequestSwagger implements PutCompanyRequestInterface {
  @ApiProperty()
  activityLocations: ActivityLocationsRawInterface;

  @ApiProperty({ maxLength: CompanyConstraints.COMPANY_ADDRESS_MAX_LENGTH })
  address: string;

  @ApiPropertyOptional()
  bachelorsOfEngineeringNumber: number;

  @ApiProperty({ enum: CompanyEnums.CompanyBusinessSegment })
  businessSegment: CompanyEnums.CompanyBusinessSegment;

  @ApiProperty({ type: 'string', format: 'date-time' })
  creationDate: Date;

  @ApiProperty({ isArray: true, enum: CompanyEnums.CompanyGeniusType })
  geniusTypes: CompanyEnums.CompanyGeniusType[];

  @ApiProperty()
  logoUuid: string;

  @ApiProperty({ maxLength: CompanyConstraints.COMPANY_NAME_MAX_LENGTH })
  name: string;

  @ApiPropertyOptional({ maxLength: CompanyConstraints.COMPANY_PRESENTATION_MAX_LENGTH })
  presentation: string;

  @ApiPropertyOptional({ default: false })
  published: boolean;

  @ApiProperty({ enum: CompanyEnums.CompanyRevenue })
  revenue: CompanyEnums.CompanyRevenue;

  @ApiPropertyOptional({ enum: CompanyEnums.CompanyRevenueForRD })
  revenueForRD: CompanyEnums.CompanyRevenueForRD;

  @ApiProperty({ enum: CompanyEnums.CompanySize })
  size: CompanyEnums.CompanySize;

  @ApiPropertyOptional()
  socialNetworks: SocialNetworksSwagger;

  @ApiProperty({
    isArray: true,
    type: ValueSwagger,
    minItems: CompanyConstraints.COMPANY_VALUES_MIN,
    maxItems: CompanyConstraints.COMPANY_VALUES_MAX,
  })
  values: ValueSwagger[];

  @ApiProperty({ maxLength: CompanyConstraints.COMPANY_WEBSITE_MAX_LENGTH })
  website: string;
}

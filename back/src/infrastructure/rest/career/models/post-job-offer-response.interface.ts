import { JobOfferId } from '../../../../domain/type-aliases';

export interface PostJobOfferResponseInterface {
  id: JobOfferId;
}

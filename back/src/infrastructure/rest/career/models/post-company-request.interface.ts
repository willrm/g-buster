import { ActivityLocationsRawInterface } from '../../../../domain/activity-locations/activity-locations.raw.interface';
import { CompanyRawInterface } from '../../../../domain/company/company.raw.interface';
import { SocialNetworksRawInterface } from '../../../../domain/social-networks/social-networks.raw.interface';
import { ValueRawInterface } from '../../../../domain/value/value.raw.interface';

export type PostCompanyRequestInterface = CompanyRawInterface;
export type PostActivityLocationsRequestInterface = ActivityLocationsRawInterface;
export type PostSocialNetworksRequestInterface = SocialNetworksRawInterface;
export type PostValueRequestInterface = ValueRawInterface;

import { AdvantagesRawInterface } from '../../../../domain/advantages/advantages.raw.interface';
import { AnnualSalaryRangeRawInterface } from '../../../../domain/annual-salary-range/annual-salary-range.raw.interface';
import { JobOfferRawInterface } from '../../../../domain/job-offer/job-offer.raw.interface';
import { StartDateOfEmploymentRawInterface } from '../../../../domain/start-date-of-employment/start-date-of-employment.raw.interface';
import { TravelingRawInterface } from '../../../../domain/traveling/traveling.raw.interface';

export type PostJobOfferRequestInterface = JobOfferRawInterface;
export type PostAdvandagesRequestInterface = AdvantagesRawInterface;
export type PostTravelingRequestInterface = TravelingRawInterface;
export type PostAnnualSalaryRangeInterface = AnnualSalaryRangeRawInterface;
export type PostStartDateOfEmploymentInterface = StartDateOfEmploymentRawInterface;

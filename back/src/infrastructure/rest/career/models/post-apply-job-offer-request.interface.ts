import { CommonTypeAliases } from '../../common-models/common-type-aliases';

export interface PostApplyJobOfferRequestInterface {
  resumeUuid: CommonTypeAliases.Uuid;
  coverLetterUuid: CommonTypeAliases.Uuid;
}

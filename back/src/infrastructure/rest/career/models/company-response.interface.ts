import { ActivityLocationsInterface } from '../../../../domain/activity-locations/activity-locations.interface';
import { CompanyInterface } from '../../../../domain/company/company.interface';
import { LogoInterface } from '../../../../domain/logo/logo.interface';
import { SocialNetworksInterface } from '../../../../domain/social-networks/social-networks.interface';
import { ValueInterface } from '../../../../domain/value/value.interface';

export type CompanyResponseInterface = CompanyInterface;
export type ActivityLocationsResponseInterface = ActivityLocationsInterface;
export type SocialNetworksResponseInterface = SocialNetworksInterface;
export type ValueResponseInterface = ValueInterface;
export type LogoResponseInterface = LogoInterface;

import { GetPublishedJobOffersFilterInterface } from '../../../../use_cases/get-published-job-offers-filter.interface';

export type GetJobOffersRequestInterface = GetPublishedJobOffersFilterInterface;

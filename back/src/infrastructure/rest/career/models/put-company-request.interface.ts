import { CompanyRawInterface } from '../../../../domain/company/company.raw.interface';

export type PutCompanyRequestInterface = CompanyRawInterface;

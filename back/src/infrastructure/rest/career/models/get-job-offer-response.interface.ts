import { JobOfferInterface } from '../../../../domain/job-offer/job-offer.interface';

export type GetJobOfferResponseInterface = JobOfferInterface;

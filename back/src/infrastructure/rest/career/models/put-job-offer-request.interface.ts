import { AdvantagesRawInterface } from '../../../../domain/advantages/advantages.raw.interface';
import { AnnualSalaryRangeRawInterface } from '../../../../domain/annual-salary-range/annual-salary-range.raw.interface';
import { JobOfferRawInterface } from '../../../../domain/job-offer/job-offer.raw.interface';
import { StartDateOfEmploymentRawInterface } from '../../../../domain/start-date-of-employment/start-date-of-employment.raw.interface';
import { TravelingRawInterface } from '../../../../domain/traveling/traveling.raw.interface';

export type PutJobOfferRequestInterface = JobOfferRawInterface;
export type PutAdvandagesRequestInterface = AdvantagesRawInterface;
export type PutTravelingRequestInterface = TravelingRawInterface;
export type PutAnnualSalaryRangeInterface = AnnualSalaryRangeRawInterface;
export type PutStartDateOfEmploymentInterface = StartDateOfEmploymentRawInterface;

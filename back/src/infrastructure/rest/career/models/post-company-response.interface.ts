import { CompanyId } from '../../../../domain/type-aliases';

export interface PostCompanyResponseInterface {
  id: CompanyId;
}

import { AdvantagesInterface } from '../../../../domain/advantages/advantages.interface';
import { AnnualSalaryRangeInterface } from '../../../../domain/annual-salary-range/annual-salary-range.interface';
import { JobOfferInterface } from '../../../../domain/job-offer/job-offer.interface';
import { StartDateOfEmploymentInterface } from '../../../../domain/start-date-of-employment/start-date-of-employment.interface';
import { TravelingInterface } from '../../../../domain/traveling/traveling.interface';
import { JobOfferWithCompanyInterface } from '../../../../use_cases/adapters/job-offer-with-company.interface';

export type JobOfferResponseInterface = JobOfferInterface;
export type JobOfferWithCompanyResponseInterface = JobOfferWithCompanyInterface;
export type AdvantagesResponseInterface = AdvantagesInterface;
export type TravelingResponseInterface = TravelingInterface;
export type AnnualSalaryRangeResponseInterface = AnnualSalaryRangeInterface;
export type StartDateOfEmploymentResponseInterface = StartDateOfEmploymentInterface;

import { ApiPropertyOptional } from '@nestjs/swagger';
import { ValueConstraints } from '../models/company-constraints-and-enums';
import { ValueResponseInterface } from '../models/company-response.interface';

export class ValueSwagger implements ValueResponseInterface {
  @ApiPropertyOptional({ maxLength: ValueConstraints.VALUE_JUSTIFICATION_MAX_LENGTH })
  justification: string;

  @ApiPropertyOptional({ maxLength: ValueConstraints.VALUE_MEANING_MAX_LENGTH })
  meaning: string;
}

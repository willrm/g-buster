import { ApiPropertyOptional } from '@nestjs/swagger';
import { SocialNetworksResponseInterface } from '../models/company-response.interface';

export class SocialNetworksSwagger implements SocialNetworksResponseInterface {
  @ApiPropertyOptional()
  facebook?: string;
  @ApiPropertyOptional()
  linkedIn?: string;
}

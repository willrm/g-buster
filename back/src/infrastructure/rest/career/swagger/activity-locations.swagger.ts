import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { ActivityLocationsResponseInterface } from '../models/company-response.interface';

export class ActivityLocationsSwagger implements ActivityLocationsResponseInterface {
  @ApiProperty()
  quebec: number;

  @ApiPropertyOptional()
  canada: number;

  @ApiPropertyOptional()
  international: number;

  @ApiPropertyOptional()
  usa: number;
}

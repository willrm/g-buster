import { ApiProperty } from '@nestjs/swagger';
import { LogoResponseInterface } from '../models/company-response.interface';

export class LogoSwagger implements LogoResponseInterface {
  @ApiProperty()
  base64: string;
  @ApiProperty()
  mimeType: string;
}

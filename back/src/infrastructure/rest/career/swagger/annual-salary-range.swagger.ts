import { ApiPropertyOptional } from '@nestjs/swagger';
import { AnnualSalaryRangeConstraints } from '../models/job-offer-constraints-and-enums';
import { AnnualSalaryRangeResponseInterface } from '../models/job-offer-response.interface';

export class AnnualSalaryRangeSwagger implements AnnualSalaryRangeResponseInterface {
  @ApiPropertyOptional({ maximum: AnnualSalaryRangeConstraints.ANNUAL_SALARY_RANGE_MAXIMUM_SALARY })
  maximumSalary: number;

  @ApiPropertyOptional({ minimum: AnnualSalaryRangeConstraints.ANNUAL_SALARY_RANGE_MINIMUM_SALARY })
  minimumSalary: number;
}

import { ApiPropertyOptional } from '@nestjs/swagger';
import { StartDateOfEmploymentResponseInterface } from '../models/job-offer-response.interface';

export class StartDateOfEmploymentSwagger implements StartDateOfEmploymentResponseInterface {
  @ApiPropertyOptional()
  asSoonAsPossible: boolean;

  @ApiPropertyOptional({ type: 'string', format: 'date-time' })
  startDate: Date;
}

import { ApiPropertyOptional } from '@nestjs/swagger';
import { AdvantagesEnums } from '../models/job-offer-constraints-and-enums';
import { AdvantagesResponseInterface } from '../models/job-offer-response.interface';

export class AdvantagesSwagger implements AdvantagesResponseInterface {
  @ApiPropertyOptional({ isArray: true, enum: AdvantagesEnums.AdvantageFinancial })
  financial: AdvantagesEnums.AdvantageFinancial[];

  @ApiPropertyOptional({ isArray: true, enum: AdvantagesEnums.AdvantageFinancial })
  health: AdvantagesEnums.AdvantageHealth[];

  @ApiPropertyOptional({ isArray: true, enum: AdvantagesEnums.AdvantageLifeBalance })
  lifeBalance: AdvantagesEnums.AdvantageLifeBalance[];

  @ApiPropertyOptional({ isArray: true, enum: AdvantagesEnums.AdvantageProfessional })
  professional: AdvantagesEnums.AdvantageProfessional[];

  @ApiPropertyOptional({ isArray: true, enum: AdvantagesEnums.AdvantageSocial })
  social: AdvantagesEnums.AdvantageSocial[];
}

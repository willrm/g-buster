import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { CommonTypeAliases } from '../../common-models/common-type-aliases';
import { CompanyConstraints, CompanyEnums } from '../models/company-constraints-and-enums';
import { CompanyResponseInterface } from '../models/company-response.interface';
import { ActivityLocationsSwagger } from './activity-locations.swagger';
import { LogoSwagger } from './logo.swagger';
import { SocialNetworksSwagger } from './social-networks.swagger';
import { ValueSwagger } from './value.swagger';

export class CompanyResponseSwagger implements CompanyResponseInterface {
  @ApiProperty()
  id: CommonTypeAliases.CompanyId;

  @ApiProperty()
  userId: CommonTypeAliases.UserId;

  @ApiProperty({ enum: CompanyEnums.CompanyRevenue })
  status: CompanyEnums.CompanyStatus;

  @ApiProperty()
  activityLocations: ActivityLocationsSwagger;

  @ApiProperty({ maxLength: CompanyConstraints.COMPANY_ADDRESS_MAX_LENGTH })
  address: string;

  @ApiPropertyOptional()
  bachelorsOfEngineeringNumber: number;

  @ApiProperty({ enum: CompanyEnums.CompanyBusinessSegment })
  businessSegment: CompanyEnums.CompanyBusinessSegment;

  @ApiProperty({ type: 'string', format: 'date-time' })
  creationDate: Date;

  @ApiProperty({ isArray: true, enum: CompanyEnums.CompanyGeniusType })
  geniusTypes: CompanyEnums.CompanyGeniusType[];

  @ApiProperty()
  logo: LogoSwagger;

  @ApiProperty({ maxLength: CompanyConstraints.COMPANY_NAME_MAX_LENGTH })
  name: string;

  @ApiPropertyOptional({ maxLength: CompanyConstraints.COMPANY_PRESENTATION_MAX_LENGTH })
  presentation: string;

  @ApiProperty({ enum: CompanyEnums.CompanyRevenue })
  revenue: CompanyEnums.CompanyRevenue;

  @ApiPropertyOptional({ enum: CompanyEnums.CompanyRevenueForRD })
  revenueForRD: CompanyEnums.CompanyRevenueForRD;

  @ApiProperty({ enum: CompanyEnums.CompanySize })
  size: CompanyEnums.CompanySize;

  @ApiPropertyOptional()
  socialNetworks: SocialNetworksSwagger;

  @ApiProperty({
    isArray: true,
    type: ValueSwagger,
    minLength: CompanyConstraints.COMPANY_VALUES_MIN,
    maxLength: CompanyConstraints.COMPANY_VALUES_MAX,
  })
  values: ValueSwagger[];

  @ApiProperty({ maxLength: CompanyConstraints.COMPANY_WEBSITE_MAX_LENGTH })
  website: string;
}

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { CommonTypeAliases } from '../../common-models/common-type-aliases';
import { JobOfferEnums } from '../models/job-offer-constraints-and-enums';
import { JobOfferResponseInterface } from '../models/job-offer-response.interface';
import { AdvantagesSwagger } from './advantages.swagger';
import { AnnualSalaryRangeSwagger } from './annual-salary-range.swagger';
import { StartDateOfEmploymentSwagger } from './start-date-of-employment.swagger';
import { TravelingSwagger } from './traveling.swagger';

export class JobOfferResponseSwagger implements JobOfferResponseInterface {
  @ApiProperty()
  id: CommonTypeAliases.JobOfferId;

  @ApiProperty()
  userId: CommonTypeAliases.UserId;

  @ApiProperty()
  companyId: CommonTypeAliases.CompanyId;

  @ApiProperty({ enum: JobOfferEnums.JobOfferStatus })
  status: JobOfferEnums.JobOfferStatus;

  @ApiProperty()
  city: string;

  @ApiProperty({ enum: JobOfferEnums.JobOfferJobStatus })
  jobStatus: JobOfferEnums.JobOfferJobStatus;

  @ApiProperty({ enum: JobOfferEnums.JobOfferJobType })
  jobType: JobOfferEnums.JobOfferJobType;

  @ApiProperty()
  title: string;

  @ApiProperty({ enum: JobOfferEnums.JobOfferPositionType })
  positionType: JobOfferEnums.JobOfferPositionType;

  @ApiProperty({ enum: JobOfferEnums.JobOfferRegion })
  region: JobOfferEnums.JobOfferRegion;

  @ApiProperty({ isArray: true, enum: JobOfferEnums.JobOfferRequiredExperience })
  requiredExperiences: JobOfferEnums.JobOfferRequiredExperience[];

  @ApiProperty({ isArray: true, enum: JobOfferEnums.JobOfferTargetCustomer })
  targetCustomers: JobOfferEnums.JobOfferTargetCustomer[];

  @ApiProperty({ isArray: true, enum: JobOfferEnums.JobOfferRequestedSpeciality })
  requestedSpecialities: JobOfferEnums.JobOfferRequestedSpeciality[];

  @ApiProperty()
  description: string;

  @ApiPropertyOptional()
  specificities: string;

  @ApiPropertyOptional()
  traveling: TravelingSwagger;

  @ApiPropertyOptional()
  advantages: AdvantagesSwagger;

  @ApiPropertyOptional()
  annualSalaryRange: AnnualSalaryRangeSwagger;

  @ApiProperty()
  emailAddressForReceiptOfApplications: string;

  @ApiPropertyOptional({ type: 'string', format: 'date-time' })
  endDateOfApplication: Date;

  @ApiPropertyOptional()
  internalReferenceNumber: string;

  @ApiPropertyOptional()
  startDateOfEmployment: StartDateOfEmploymentSwagger;

  @ApiPropertyOptional({ isArray: true, enum: JobOfferEnums.JobOfferWantedPersonality })
  wantedPersonalities: JobOfferEnums.JobOfferWantedPersonality[];

  @ApiPropertyOptional({ type: 'string', format: 'date-time' })
  publishedAt: Date;
}

import { ApiProperty } from '@nestjs/swagger';
import { StatusCode } from '../../filters/models/default-error-response.interface';
import { DuplicateItemErrorResponseInterface } from '../../filters/models/duplicate-item-error-response.interface';

export class DuplicateCompanyResponseSwagger implements DuplicateItemErrorResponseInterface {
  @ApiProperty()
  message: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  statusCode: StatusCode;

  @ApiProperty()
  timestamp: string;
}

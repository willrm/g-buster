import { JobOfferId } from '../../../../domain/type-aliases';
import { PostJobOfferResponseInterface } from '../models/post-job-offer-response.interface';

export class PostJobOfferResponseSwagger implements PostJobOfferResponseInterface {
  id: JobOfferId;
}

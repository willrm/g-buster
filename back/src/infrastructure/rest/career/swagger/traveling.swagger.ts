import { ApiPropertyOptional } from '@nestjs/swagger';
import { TravelingEnums } from '../models/job-offer-constraints-and-enums';
import { TravelingResponseInterface } from '../models/job-offer-response.interface';

export class TravelingSwagger implements TravelingResponseInterface {
  @ApiPropertyOptional()
  isTraveling: boolean;

  @ApiPropertyOptional({ enum: TravelingEnums.TravelingFrequency })
  quebec: TravelingEnums.TravelingFrequency;

  @ApiPropertyOptional({ enum: TravelingEnums.TravelingFrequency })
  canada: TravelingEnums.TravelingFrequency;

  @ApiPropertyOptional({ enum: TravelingEnums.TravelingFrequency })
  international: TravelingEnums.TravelingFrequency;
}

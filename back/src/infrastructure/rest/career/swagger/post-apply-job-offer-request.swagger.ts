import { ApiProperty } from '@nestjs/swagger';
import { PostApplyJobOfferRequestInterface } from '../models/post-apply-job-offer-request.interface';

export class PostApplyJobOfferRequestSwagger implements PostApplyJobOfferRequestInterface {
  @ApiProperty()
  resumeUuid: string;

  @ApiProperty()
  coverLetterUuid: string;
}

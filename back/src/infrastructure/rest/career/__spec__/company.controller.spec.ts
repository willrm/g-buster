import { Request, Response } from 'express';
import { CompanyInterface } from '../../../../domain/company/company.interface';
import { CompanyRawInterface } from '../../../../domain/company/company.raw.interface';
import { CompanyId } from '../../../../domain/type-aliases';
import { CreateNewCompany } from '../../../../use_cases/create-new-company';
import { GetPublishedCompanyById } from '../../../../use_cases/get-published-company-by-id';
import { ValidateCompany } from '../../../../use_cases/validate-company';
import { UseCaseProxy } from '../../../use_cases_proxy/use-case-proxy';
import { CompanyController } from '../company.controller';
import { CompanyResponseInterface } from '../models/company-response.interface';
import { PostCompanyRequestInterface } from '../models/post-company-request.interface';
import { PostCompanyResponseInterface } from '../models/post-company-response.interface';

describe('infrastructure/rest/career/CompanyController', () => {
  let companyController: CompanyController;
  let mockCreateNewCompanyProxyService: UseCaseProxy<CreateNewCompany>;
  let mockValidateCompanyProxyService: UseCaseProxy<ValidateCompany>;
  let mockGetPublishedCompanyByIdProxyService: UseCaseProxy<GetPublishedCompanyById>;

  beforeEach(() => {
    mockCreateNewCompanyProxyService = {} as UseCaseProxy<CreateNewCompany>;
    mockCreateNewCompanyProxyService.getInstance = jest.fn();

    mockValidateCompanyProxyService = {} as UseCaseProxy<ValidateCompany>;
    mockValidateCompanyProxyService.getInstance = jest.fn();

    mockGetPublishedCompanyByIdProxyService = {} as UseCaseProxy<GetPublishedCompanyById>;
    mockGetPublishedCompanyByIdProxyService.getInstance = jest.fn();

    companyController = new CompanyController(
      mockCreateNewCompanyProxyService,
      mockValidateCompanyProxyService,
      mockGetPublishedCompanyByIdProxyService
    );
  });

  describe('postCompany()', () => {
    let mockCreateNewCompany: CreateNewCompany;
    let mockValidateCompany: ValidateCompany;
    let body: PostCompanyRequestInterface;
    let dryRunQueryParam: string;
    let mockRequest: Request;

    beforeEach(() => {
      mockCreateNewCompany = {} as CreateNewCompany;
      mockCreateNewCompany.execute = jest.fn();
      (mockCreateNewCompanyProxyService.getInstance as jest.Mock).mockReturnValue(mockCreateNewCompany);

      mockValidateCompany = {} as ValidateCompany;
      mockValidateCompany.execute = jest.fn();
      (mockValidateCompanyProxyService.getInstance as jest.Mock).mockReturnValue(mockValidateCompany);

      mockRequest = {} as Request;
      mockRequest.res = {} as Response;
      mockRequest.route = {};
      mockRequest.res.location = jest.fn();

      body = {} as PostCompanyRequestInterface;
    });

    describe('when dryRun is true', () => {
      beforeEach(() => {
        dryRunQueryParam = 'true';
      });

      it('should call validate company use case with raw data', () => {
        // given
        body.name = 'A company name';

        // when
        companyController.postCompany(body, dryRunQueryParam, mockRequest);

        // then
        expect(mockValidateCompany.execute).toHaveBeenCalledWith(body as CompanyRawInterface);
      });

      it('should return no company id in body', async () => {
        // when
        const result: PostCompanyResponseInterface = await companyController.postCompany(body, dryRunQueryParam, mockRequest);

        // then
        expect(result).toStrictEqual({ id: null });
      });
    });

    describe('when dryRun is false', () => {
      beforeEach(() => {
        dryRunQueryParam = 'false';
      });

      it('should call create new company use case with raw data', () => {
        // given
        body.name = 'A company name';

        // when
        companyController.postCompany(body, dryRunQueryParam, mockRequest);

        // then
        expect(mockCreateNewCompany.execute).toHaveBeenCalledWith(body as CompanyRawInterface);
      });

      it('should return response with location to created company using id', async () => {
        // given
        const expected: CompanyId = '12';
        (mockCreateNewCompany.execute as jest.Mock).mockReturnValue(Promise.resolve(expected));
        mockRequest.route.path = '/path/to/companies/endpoint';

        // when
        await companyController.postCompany(body, dryRunQueryParam, mockRequest);

        // then
        expect(mockRequest.res.location).toHaveBeenCalledWith('/path/to/companies/endpoint/12');
      });

      it('should return company id in body', async () => {
        // given
        const expected: CompanyId = '12';
        (mockCreateNewCompany.execute as jest.Mock).mockReturnValue(Promise.resolve(expected));

        // when
        const result: PostCompanyResponseInterface = await companyController.postCompany(body, dryRunQueryParam, mockRequest);

        // then
        expect(result).toStrictEqual({ id: expected });
      });
    });
  });

  describe('getCompany()', () => {
    let mockGetPublishedCompanyById: GetPublishedCompanyById;
    beforeEach(() => {
      mockGetPublishedCompanyById = {} as GetPublishedCompanyById;
      mockGetPublishedCompanyById.execute = jest.fn();
      (mockGetPublishedCompanyByIdProxyService.getInstance as jest.Mock).mockReturnValue(mockGetPublishedCompanyById);
    });

    it('should call use case with raw data', () => {
      // given
      const id: CompanyId = '42';

      // when
      companyController.getCompany(id);

      // then
      expect(mockGetPublishedCompanyById.execute).toHaveBeenCalledWith(id);
    });

    it('should return result from use case mapped to response interface', async () => {
      // given
      const id: CompanyId = '42';
      const expected: CompanyInterface = { id, name: 'A company name' } as CompanyInterface;
      (mockGetPublishedCompanyById.execute as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: CompanyResponseInterface = await companyController.getCompany(id);

      // then
      expect(result).toBe(expected as CompanyResponseInterface);
    });
  });
});

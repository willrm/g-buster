import { Request, Response } from 'express';
import { JobOfferRawInterface } from '../../../../domain/job-offer/job-offer.raw.interface';
import { JobOfferId, Uuid } from '../../../../domain/type-aliases';
import { JobOfferWithCompanyInterface } from '../../../../use_cases/adapters/job-offer-with-company.interface';
import { ApplyToJobOffer } from '../../../../use_cases/apply-to-job-offer';
import { CreateNewJobOffer } from '../../../../use_cases/create-new-job-offer';
import { GetPublishedJobOfferById } from '../../../../use_cases/get-published-job-offer-by-id';
import { GetPublishedJobOffers } from '../../../../use_cases/get-published-job-offers';
import { UpdateDraftJobOffer } from '../../../../use_cases/update-draft-job-offer';
import { ValidateJobOffer } from '../../../../use_cases/validate-job-offer';
import { UseCaseProxy } from '../../../use_cases_proxy/use-case-proxy';
import { JobOfferController } from '../job-offer.controller';
import { JobOfferEnums } from '../models/job-offer-constraints-and-enums';
import { JobOfferWithCompanyResponseInterface } from '../models/job-offer-response.interface';
import { PostApplyJobOfferRequestInterface } from '../models/post-apply-job-offer-request.interface';
import { PostJobOfferRequestInterface } from '../models/post-job-offer-request.interface';
import { PostJobOfferResponseInterface } from '../models/post-job-offer-response.interface';
import { PutJobOfferRequestInterface } from '../models/put-job-offer-request.interface';

describe('infrastructure/rest/career/JobOffer', () => {
  let jobOfferController: JobOfferController;
  let mockGetPublishedJobOffersProxyService: UseCaseProxy<GetPublishedJobOffers>;
  let mockGetPublishedJobOfferByIdProxyService: UseCaseProxy<GetPublishedJobOfferById>;
  let mockApplyToJobOfferProxyService: UseCaseProxy<ApplyToJobOffer>;
  let mockCreateNewJobOfferProxyService: UseCaseProxy<CreateNewJobOffer>;
  let mockValidateJobOfferProxyService: UseCaseProxy<ValidateJobOffer>;
  let mockUpdateDraftJobOfferProxyService: UseCaseProxy<UpdateDraftJobOffer>;

  beforeEach(() => {
    mockGetPublishedJobOffersProxyService = {} as UseCaseProxy<GetPublishedJobOffers>;
    mockGetPublishedJobOffersProxyService.getInstance = jest.fn();
    mockGetPublishedJobOfferByIdProxyService = {} as UseCaseProxy<GetPublishedJobOfferById>;
    mockGetPublishedJobOfferByIdProxyService.getInstance = jest.fn();
    mockApplyToJobOfferProxyService = {} as UseCaseProxy<ApplyToJobOffer>;
    mockApplyToJobOfferProxyService.getInstance = jest.fn();
    mockCreateNewJobOfferProxyService = {} as UseCaseProxy<CreateNewJobOffer>;
    mockCreateNewJobOfferProxyService.getInstance = jest.fn();
    mockValidateJobOfferProxyService = {} as UseCaseProxy<ValidateJobOffer>;
    mockValidateJobOfferProxyService.getInstance = jest.fn();
    mockUpdateDraftJobOfferProxyService = {} as UseCaseProxy<UpdateDraftJobOffer>;
    mockUpdateDraftJobOfferProxyService.getInstance = jest.fn();

    jobOfferController = new JobOfferController(
      mockGetPublishedJobOffersProxyService,
      mockGetPublishedJobOfferByIdProxyService,
      mockApplyToJobOfferProxyService,
      mockCreateNewJobOfferProxyService,
      mockValidateJobOfferProxyService,
      mockUpdateDraftJobOfferProxyService
    );
  });

  describe('getJobOffers()', () => {
    let mockGetPublishedJobOffers: GetPublishedJobOffers;
    beforeEach(() => {
      mockGetPublishedJobOffers = {} as GetPublishedJobOffers;
      mockGetPublishedJobOffers.execute = jest.fn();
      (mockGetPublishedJobOffersProxyService.getInstance as jest.Mock).mockReturnValue(mockGetPublishedJobOffers);
    });

    it('should call use case with filter data', () => {
      // given
      const requestedSpecialities: JobOfferEnums.JobOfferRequestedSpeciality[] = [
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION,
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
      ];
      const jobTypes: JobOfferEnums.JobOfferJobType[] = [
        JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR,
        JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP,
      ];
      const requiredExperiences: JobOfferEnums.JobOfferRequiredExperience[] = [
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
      ];
      const regions: JobOfferEnums.JobOfferRegion[] = [
        JobOfferEnums.JobOfferRegion.REGION_OUTAOUAIS,
        JobOfferEnums.JobOfferRegion.REGION_CENTRE_DU_QUEBEC,
      ];

      // when
      jobOfferController.getJobOffers(requestedSpecialities, jobTypes, requiredExperiences, regions);

      // then
      expect(mockGetPublishedJobOffers.execute).toHaveBeenCalledWith({ requestedSpecialities, jobTypes, requiredExperiences, regions });
    });

    it('should return result from use case mapped to response interface', async () => {
      // given
      const expected: JobOfferWithCompanyInterface[] = [
        { title: 'Job offer 1' } as JobOfferWithCompanyInterface,
        { title: 'Job offer 2' } as JobOfferWithCompanyInterface,
      ];
      (mockGetPublishedJobOffers.execute as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: JobOfferWithCompanyResponseInterface[] = await jobOfferController.getJobOffers();

      // then
      expect(result).toStrictEqual(expected as JobOfferWithCompanyResponseInterface[]);
    });
  });

  describe('getJobOffer()', () => {
    let mockGetPublishedJobOfferById: GetPublishedJobOfferById;
    beforeEach(() => {
      mockGetPublishedJobOfferById = {} as GetPublishedJobOfferById;
      mockGetPublishedJobOfferById.execute = jest.fn();
      (mockGetPublishedJobOfferByIdProxyService.getInstance as jest.Mock).mockReturnValue(mockGetPublishedJobOfferById);
    });

    it('should call use case with raw data', () => {
      // when
      jobOfferController.getJobOffer('42');

      // then
      expect(mockGetPublishedJobOfferById.execute).toHaveBeenCalledWith('42' as JobOfferId);
    });

    it('should return result from use case mapped to response interface', async () => {
      // given
      const expected: JobOfferWithCompanyInterface = { id: '1', title: 'Job offer 1' } as JobOfferWithCompanyInterface;
      (mockGetPublishedJobOfferById.execute as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: JobOfferWithCompanyResponseInterface = await jobOfferController.getJobOffer('42');

      // then
      expect(result).toStrictEqual(expected as JobOfferWithCompanyResponseInterface);
    });
  });

  describe('postJobOffer()', () => {
    let mockCreateNewJobOffer: CreateNewJobOffer;
    let mockValidateJobOffer: ValidateJobOffer;
    let body: PostJobOfferRequestInterface;
    let dryRunQueryParam: string;
    let mockRequest: Request;

    beforeEach(() => {
      mockCreateNewJobOffer = {} as CreateNewJobOffer;
      mockCreateNewJobOffer.execute = jest.fn();
      mockValidateJobOffer = {} as ValidateJobOffer;
      mockValidateJobOffer.execute = jest.fn();
      (mockCreateNewJobOfferProxyService.getInstance as jest.Mock).mockReturnValue(mockCreateNewJobOffer);
      (mockValidateJobOfferProxyService.getInstance as jest.Mock).mockReturnValue(mockValidateJobOffer);

      mockRequest = {} as Request;
      mockRequest.res = {} as Response;
      mockRequest.route = {};
      mockRequest.res.location = jest.fn();

      body = {} as PostJobOfferRequestInterface;
    });

    describe('when dryRun is true', () => {
      beforeEach(() => {
        dryRunQueryParam = 'true';
      });

      it('should call validate job offer use case with raw data', async () => {
        // given
        body.title = 'A job title';

        // when
        await jobOfferController.postJobOffer(body, dryRunQueryParam, mockRequest);

        // then
        expect(mockValidateJobOffer.execute).toHaveBeenCalledWith(body as JobOfferRawInterface);
      });

      it('should return no job offer id in body', async () => {
        // when
        const result: PostJobOfferResponseInterface = await jobOfferController.postJobOffer(body, dryRunQueryParam, mockRequest);

        // then
        expect(result).toStrictEqual({ id: null });
      });
    });

    describe('when dryRun is false', () => {
      beforeEach(() => {
        dryRunQueryParam = 'false';
      });

      it('should call create new job offer use case with raw data', async () => {
        // given
        body.title = 'A job title';

        // when
        await jobOfferController.postJobOffer(body, dryRunQueryParam, mockRequest);

        // then
        expect(mockCreateNewJobOffer.execute).toHaveBeenCalledWith(body as JobOfferRawInterface);
      });

      it('should return response with location to created job offer using id', async () => {
        // given
        const jobOfferId: JobOfferId = '123456789';
        (mockCreateNewJobOffer.execute as jest.Mock).mockReturnValue(Promise.resolve(jobOfferId));
        mockRequest.route.path = '/path/to/job-offers/endpoint';

        // when
        await jobOfferController.postJobOffer(body, dryRunQueryParam, mockRequest);

        // then
        expect(mockRequest.res.location).toHaveBeenCalledWith('/path/to/job-offers/endpoint/123456789');
      });

      it('should return job offer id in body', async () => {
        // given
        const expected: JobOfferId = '123456789';
        (mockCreateNewJobOffer.execute as jest.Mock).mockReturnValue(Promise.resolve(expected));

        // when
        const result: PostJobOfferResponseInterface = await jobOfferController.postJobOffer(body, dryRunQueryParam, mockRequest);

        // then
        expect(result).toStrictEqual({ id: expected });
      });
    });
  });

  describe('putJobOffer()', () => {
    let mockUpdateDraftJobOffer: UpdateDraftJobOffer;
    let body: PutJobOfferRequestInterface;
    let mockRequest: Request;
    let jobOfferId: JobOfferId;

    beforeEach(() => {
      mockUpdateDraftJobOffer = {} as UpdateDraftJobOffer;
      mockUpdateDraftJobOffer.execute = jest.fn();
      (mockUpdateDraftJobOfferProxyService.getInstance as jest.Mock).mockReturnValue(mockUpdateDraftJobOffer);

      mockRequest = {} as Request;
      mockRequest.res = {} as Response;
      mockRequest.route = {};
      mockRequest.res.location = jest.fn();

      body = {} as PutJobOfferRequestInterface;

      jobOfferId = 'aJobOfferId';
    });

    it('should call update draft job offer use case with id and raw data', () => {
      // given
      body.title = 'A job title';

      // when
      jobOfferController.putJobOffer(jobOfferId, body, mockRequest);

      // then
      expect(mockUpdateDraftJobOffer.execute).toHaveBeenCalledWith(jobOfferId, body as JobOfferRawInterface);
    });

    it('should return response with location to updated job Offer using id', async () => {
      // given
      (mockUpdateDraftJobOffer.execute as jest.Mock).mockReturnValue(Promise.resolve(jobOfferId));
      mockRequest.route.path = '/path/to/job-offers/endpoint/:id';

      // when
      await jobOfferController.putJobOffer(jobOfferId, body, mockRequest);

      // then
      expect(mockRequest.res.location).toHaveBeenCalledWith('/path/to/job-offers/endpoint/aJobOfferId');
    });
  });

  describe('applyJobOffer()', () => {
    let mockApplyToJobOffer: ApplyToJobOffer;

    beforeEach(() => {
      mockApplyToJobOffer = {} as ApplyToJobOffer;
      mockApplyToJobOffer.execute = jest.fn();
      (mockApplyToJobOfferProxyService.getInstance as jest.Mock).mockReturnValue(mockApplyToJobOffer);
    });

    it('should call apply to job offer use case with id and resume uuid from body', async () => {
      // given
      const jobOfferId: JobOfferId = 'job-offer-id';
      const resumeTemporaryFileUuid: Uuid = 'resume-uuid';
      const coverLetterTemporaryFileUuid: Uuid = 'cover-letter-uuid';
      const body: PostApplyJobOfferRequestInterface = { resumeUuid: resumeTemporaryFileUuid, coverLetterUuid: coverLetterTemporaryFileUuid };

      // when
      await jobOfferController.applyJobOffer(jobOfferId, body);

      // then
      expect(mockApplyToJobOffer.execute).toHaveBeenCalledWith(jobOfferId, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);
    });
  });
});

import { Body, Controller, Get, Inject, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBody, ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { Request } from 'express';
import { CompanyInterface } from '../../../domain/company/company.interface';
import { CompanyRawInterface } from '../../../domain/company/company.raw.interface';
import { CompanyId } from '../../../domain/type-aliases';
import { UserRole } from '../../../domain/user/user-enums';
import { CreateNewCompany } from '../../../use_cases/create-new-company';
import { GetPublishedCompanyById } from '../../../use_cases/get-published-company-by-id';
import { ValidateCompany } from '../../../use_cases/validate-company';
import { AuthenticatedWithRolesGuard, UserRoles } from '../../config/authentication/authenticated-with-roles.guard';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../use_cases_proxy/use-case-proxy';
import { CompanyResponseInterface } from './models/company-response.interface';
import { PostCompanyRequestInterface } from './models/post-company-request.interface';
import { PostCompanyResponseInterface } from './models/post-company-response.interface';
import { CompanyNotFoundResponseSwagger } from './swagger/company-not-found-response.swagger';
import { CompanyResponseSwagger } from './swagger/company-response.swagger';
import { DuplicateCompanyResponseSwagger } from './swagger/duplicate-company-response.swagger';
import { PostCompanyRequestSwagger } from './swagger/post-company-request.swagger';

@Controller('/companies')
export class CompanyController {
  constructor(
    @Inject(ProxyServicesDynamicModule.CREATE_NEW_COMPANY_PROXY_SERVICE)
    private readonly createNewCompanyProxyService: UseCaseProxy<CreateNewCompany>,
    @Inject(ProxyServicesDynamicModule.VALIDATE_COMPANY_PROXY_SERVICE)
    private readonly validateCompanyProxyService: UseCaseProxy<ValidateCompany>,
    @Inject(ProxyServicesDynamicModule.GET_PUBLISHED_COMPANY_BY_ID_PROXY_SERVICE)
    private readonly getPublishedCompanyByIdProxyService: UseCaseProxy<GetPublishedCompanyById>
  ) {}

  @UseGuards(AuthenticatedWithRolesGuard)
  @UserRoles(UserRole.COMPANY)
  @Get('/:id')
  @ApiParam({ name: 'id' })
  @ApiResponse({ status: 200, type: CompanyResponseSwagger })
  @ApiResponse({ status: 400, type: DuplicateCompanyResponseSwagger })
  @ApiResponse({ status: 404, type: CompanyNotFoundResponseSwagger })
  async getCompany(@Param('id') id: CompanyId): Promise<CompanyResponseInterface> {
    const company: CompanyInterface = await this.getPublishedCompanyByIdProxyService.getInstance().execute(id);

    return company as CompanyResponseInterface;
  }

  @UseGuards(AuthenticatedWithRolesGuard)
  @UserRoles(UserRole.COMPANY)
  @Post('/')
  @ApiQuery({
    name: 'dryRun',
    description: 'if dryRun is true, company will be validated but will not be persisted into the system',
    required: false,
    type: Boolean,
  })
  @ApiBody({ type: PostCompanyRequestSwagger })
  @ApiResponse({ status: 201 })
  async postCompany(
    @Body() postCompanyRequest: PostCompanyRequestInterface,
    @Query('dryRun') dryRunQueryParam: string,
    @Req() request: Request
  ): Promise<PostCompanyResponseInterface> {
    if (dryRunQueryParam === 'true') {
      await this.validateCompanyProxyService.getInstance().execute(postCompanyRequest as CompanyRawInterface);

      return { id: null };
    } else {
      const companyId: CompanyId = await this.createNewCompanyProxyService.getInstance().execute(postCompanyRequest as CompanyRawInterface);
      request.res.location(`${request.route.path}/${companyId}`);

      return { id: companyId };
    }
  }
}

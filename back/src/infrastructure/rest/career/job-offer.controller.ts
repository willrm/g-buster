import { Body, Controller, Get, Inject, Param, Post, Put, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBody, ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { Request } from 'express';
import { JobOfferRawInterface } from '../../../domain/job-offer/job-offer.raw.interface';
import { JobOfferId } from '../../../domain/type-aliases';
import { UserRole } from '../../../domain/user/user-enums';
import { JobOfferWithCompanyInterface } from '../../../use_cases/adapters/job-offer-with-company.interface';
import { ApplyToJobOffer } from '../../../use_cases/apply-to-job-offer';
import { CreateNewJobOffer } from '../../../use_cases/create-new-job-offer';
import { GetPublishedJobOfferById } from '../../../use_cases/get-published-job-offer-by-id';
import { GetPublishedJobOffers } from '../../../use_cases/get-published-job-offers';
import { UpdateDraftJobOffer } from '../../../use_cases/update-draft-job-offer';
import { ValidateJobOffer } from '../../../use_cases/validate-job-offer';
import { AuthenticatedWithRolesGuard, UserRoles } from '../../config/authentication/authenticated-with-roles.guard';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../use_cases_proxy/use-case-proxy';
import { JobOfferEnums } from './models/job-offer-constraints-and-enums';
import { JobOfferWithCompanyResponseInterface } from './models/job-offer-response.interface';
import { PostApplyJobOfferRequestInterface } from './models/post-apply-job-offer-request.interface';
import { PostJobOfferRequestInterface } from './models/post-job-offer-request.interface';
import { PostJobOfferResponseInterface } from './models/post-job-offer-response.interface';
import { PutJobOfferRequestInterface } from './models/put-job-offer-request.interface';
import { JobOfferResponseSwagger } from './swagger/job-offer-response.swagger';
import { PostApplyJobOfferRequestSwagger } from './swagger/post-apply-job-offer-request.swagger';
import { PostJobOfferRequestSwagger } from './swagger/post-job-offer-request.swagger';
import { PostJobOfferResponseSwagger } from './swagger/post-job-offer-response.swagger';
import { PutJobOfferRequestSwagger } from './swagger/put-job-offer-request.swagger';

@Controller('/job-offers')
export class JobOfferController {
  constructor(
    @Inject(ProxyServicesDynamicModule.GET_PUBLISHED_JOB_OFFERS_PROXY_SERVICE)
    private readonly getPublishedJobOffersProxyService: UseCaseProxy<GetPublishedJobOffers>,
    @Inject(ProxyServicesDynamicModule.GET_PUBLISHED_JOB_OFFER_BY_ID_PROXY_SERVICE)
    private readonly getPublishedJobOfferByIdProxyService: UseCaseProxy<GetPublishedJobOfferById>,
    @Inject(ProxyServicesDynamicModule.APPLY_TO_JOB_OFFER_PROXY_SERVICE)
    private readonly applyToJobOfferProxyService: UseCaseProxy<ApplyToJobOffer>,
    @Inject(ProxyServicesDynamicModule.CREATE_NEW_JOB_OFFER_PROXY_SERVICE)
    private readonly createNewJobOfferProxyService: UseCaseProxy<CreateNewJobOffer>,
    @Inject(ProxyServicesDynamicModule.VALIDATE_JOB_OFFER_PROXY_SERVICE)
    private readonly validateJobOfferProxyService: UseCaseProxy<ValidateJobOffer>,
    @Inject(ProxyServicesDynamicModule.UPDATE_DRAFT_JOB_OFFER_PROXY_SERVICE)
    private readonly updateDraftJobOfferProxyService: UseCaseProxy<UpdateDraftJobOffer>
  ) {}

  @Get('/')
  @ApiQuery({
    name: 'requestedSpecialities',
    description: 'to filter job offers by requestedSpecialities',
    isArray: true,
    required: false,
    enum: JobOfferEnums.JobOfferRequestedSpeciality,
  })
  @ApiQuery({
    name: 'jobTypes',
    description: 'to filter job offers by jobTypes',
    isArray: true,
    required: false,
    enum: JobOfferEnums.JobOfferJobType,
  })
  @ApiQuery({
    name: 'requiredExperiences',
    description: 'to filter job offers by requiredExperiences',
    isArray: true,
    required: false,
    enum: JobOfferEnums.JobOfferRequiredExperience,
  })
  @ApiQuery({
    name: 'regions',
    description: 'to filter job offers by regions',
    isArray: true,
    required: false,
    enum: JobOfferEnums.JobOfferRegion,
  })
  @ApiResponse({ status: 200, type: JobOfferResponseSwagger, isArray: true })
  async getJobOffers(
    @Query('requestedSpecialities') requestedSpecialities: JobOfferEnums.JobOfferRequestedSpeciality[] = [],
    @Query('jobTypes') jobTypes: JobOfferEnums.JobOfferJobType[] = [],
    @Query('requiredExperiences') requiredExperiences: JobOfferEnums.JobOfferRequiredExperience[] = [],
    @Query('regions') regions: JobOfferEnums.JobOfferRegion[] = []
  ): Promise<JobOfferWithCompanyResponseInterface[]> {
    const jobOffersWithCompany: JobOfferWithCompanyInterface[] = await this.getPublishedJobOffersProxyService
      .getInstance()
      .execute({ requestedSpecialities, jobTypes, requiredExperiences, regions });

    return jobOffersWithCompany.map(
      (jobOfferWithCompany: JobOfferWithCompanyInterface) => jobOfferWithCompany as JobOfferWithCompanyResponseInterface
    );
  }

  @Get('/:id')
  @ApiParam({ name: 'id' })
  @ApiResponse({ status: 200, type: JobOfferResponseSwagger })
  async getJobOffer(@Param('id') id: JobOfferId): Promise<JobOfferWithCompanyResponseInterface> {
    const jobOffer: JobOfferWithCompanyInterface = await this.getPublishedJobOfferByIdProxyService.getInstance().execute(id);

    return jobOffer as JobOfferWithCompanyResponseInterface;
  }

  @UseGuards(AuthenticatedWithRolesGuard)
  @UserRoles(UserRole.COMPANY)
  @Post('/')
  @ApiQuery({
    name: 'dryRun',
    description: 'if dryRun is true, job offer will be validated but will not be persisted into the system',
    required: false,
    type: Boolean,
  })
  @ApiBody({ type: PostJobOfferRequestSwagger })
  @ApiResponse({ status: 201, type: PostJobOfferResponseSwagger })
  async postJobOffer(
    @Body() postJobOfferRequest: PostJobOfferRequestInterface,
    @Query('dryRun') dryRunQueryParam: string,
    @Req() request: Request
  ): Promise<PostJobOfferResponseInterface> {
    if (dryRunQueryParam === 'true') {
      await this.validateJobOfferProxyService.getInstance().execute(postJobOfferRequest as JobOfferRawInterface);

      return { id: null };
    } else {
      const jobOfferId: JobOfferId = await this.createNewJobOfferProxyService.getInstance().execute(postJobOfferRequest as JobOfferRawInterface);
      request.res.location(`${request.route.path}/${jobOfferId}`);

      return { id: jobOfferId };
    }
  }

  @UseGuards(AuthenticatedWithRolesGuard)
  @UserRoles(UserRole.COMPANY)
  @Put('/:id')
  @ApiParam({ name: 'id' })
  @ApiBody({ type: PutJobOfferRequestSwagger })
  @ApiResponse({ status: 200 })
  async putJobOffer(@Param('id') id: JobOfferId, @Body() putJobOfferRequest: PutJobOfferRequestInterface, @Req() request: Request): Promise<void> {
    const jobOfferId: JobOfferId = await this.updateDraftJobOfferProxyService.getInstance().execute(id, putJobOfferRequest as JobOfferRawInterface);

    request.res.location(request.route.path.replace(':id', jobOfferId));
  }

  @UseGuards(AuthenticatedWithRolesGuard)
  @UserRoles(UserRole.CANDIDATE)
  @Post('/:id/apply')
  @ApiParam({ name: 'id' })
  @ApiBody({ type: PostApplyJobOfferRequestSwagger })
  @ApiResponse({ status: 201 })
  async applyJobOffer(@Param('id') id: JobOfferId, @Body() postApplyJobOfferRequestSwagger: PostApplyJobOfferRequestInterface): Promise<void> {
    await this.applyToJobOfferProxyService
      .getInstance()
      .execute(id, postApplyJobOfferRequestSwagger.resumeUuid, postApplyJobOfferRequestSwagger.coverLetterUuid);
  }
}

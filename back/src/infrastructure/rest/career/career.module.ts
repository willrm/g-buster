import { Module } from '@nestjs/common';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { CompanyController } from './company.controller';
import { JobOfferController } from './job-offer.controller';

@Module({
  imports: [ProxyServicesDynamicModule.register()],
  controllers: [CompanyController, JobOfferController],
})
export class CareerModule {}

import { GeniusProductInterface } from '../../../../domain/genius-product/genius-product.interface';

export type GeniusProductResponseInterface = GeniusProductInterface;

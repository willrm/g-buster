import { GeniusProductInterface } from '../../../../domain/genius-product/genius-product.interface';
import { GetGeniusProducts } from '../../../../use_cases/get-genius-products';
import { UseCaseProxy } from '../../../use_cases_proxy/use-case-proxy';
import { GeniusProductResponseInterface } from '../models/genius-product-response.interface';
import { ProductController } from '../product.controller';

describe('infrastructure/rest/career/JobOffer', () => {
  let productController: ProductController;
  let mockGetGeniusProductsProxyService: UseCaseProxy<GetGeniusProducts>;

  beforeEach(() => {
    mockGetGeniusProductsProxyService = {} as UseCaseProxy<GetGeniusProducts>;
    mockGetGeniusProductsProxyService.getInstance = jest.fn();

    productController = new ProductController(mockGetGeniusProductsProxyService);
  });

  describe('getGeniusProducts()', () => {
    let mockGetGeniusProducts: GetGeniusProducts;
    beforeEach(() => {
      mockGetGeniusProducts = {} as GetGeniusProducts;
      mockGetGeniusProducts.execute = jest.fn();
      (mockGetGeniusProductsProxyService.getInstance as jest.Mock).mockReturnValue(mockGetGeniusProducts);
    });

    it('should call use case', () => {
      // when
      productController.getGeniusProducts();

      // then
      expect(mockGetGeniusProducts.execute).toHaveBeenCalledWith();
    });

    it('should return result from use case mapped to response interface', async () => {
      // given
      const expected: GeniusProductInterface[] = [
        { name: 'Genius Product 1' } as GeniusProductInterface,
        { name: 'Genius Product 2' } as GeniusProductInterface,
      ];
      (mockGetGeniusProducts.execute as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: GeniusProductResponseInterface[] = await productController.getGeniusProducts();

      // then
      expect(result).toStrictEqual(expected as GeniusProductResponseInterface[]);
    });
  });
});

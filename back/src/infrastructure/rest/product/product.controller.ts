import { Controller, Get, Inject } from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { GeniusProductInterface } from '../../../domain/genius-product/genius-product.interface';
import { GetGeniusProducts } from '../../../use_cases/get-genius-products';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../use_cases_proxy/use-case-proxy';
import { GeniusProductResponseInterface } from './models/genius-product-response.interface';
import { GeniusProductResponseSwagger } from './swagger/genius-product-response.swagger';

@Controller('/')
export class ProductController {
  constructor(
    @Inject(ProxyServicesDynamicModule.GET_GENIUS_PRODUCTS_PROXY_SERVICE)
    private readonly getGeniusProductsProxyService: UseCaseProxy<GetGeniusProducts>
  ) {}

  @Get('/genius-products')
  @ApiResponse({ status: 200, type: GeniusProductResponseSwagger, isArray: true })
  async getGeniusProducts(): Promise<GeniusProductResponseInterface[]> {
    const geniusProduct: GeniusProductInterface[] = await this.getGeniusProductsProxyService.getInstance().execute();

    return geniusProduct as GeniusProductResponseInterface[];
  }
}

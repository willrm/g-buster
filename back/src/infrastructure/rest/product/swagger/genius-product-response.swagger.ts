import { ApiProperty } from '@nestjs/swagger';
import { CommonTypeAliases } from '../../common-models/common-type-aliases';
import { GeniusProductConstraints } from '../models/genius-product-constraints-and-enums';
import { GeniusProductResponseInterface } from '../models/genius-product-response.interface';

export class GeniusProductResponseSwagger implements GeniusProductResponseInterface {
  @ApiProperty()
  id: CommonTypeAliases.GeniusProductId;

  @ApiProperty()
  name: string;

  @ApiProperty()
  price: number;

  @ApiProperty({ default: GeniusProductConstraints.GENIUS_PRODUCT_FEATURED_COUNTER_EMTPY_VALUE })
  featuredCounter: number;

  @ApiProperty({ default: GeniusProductConstraints.GENIUS_PRODUCT_IN_THE_SPOTLIGHT_COUNTER_EMTPY_VALUE })
  inTheSpotlightCounter: number;
}

import { Module } from '@nestjs/common';
import { ProxyServicesDynamicModule } from '../../use_cases_proxy/proxy-services-dynamic.module';
import { ProductController } from './product.controller';

@Module({
  imports: [ProxyServicesDynamicModule.register()],
  controllers: [ProductController],
})
export class ProductModule {}

import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { RouterModule, Routes } from 'nest-router';
import { SwaggerModule } from '../config/swagger/swagger.module';
import { AdminModule } from './admin/admin.module';
import { CareerModule } from './career/career.module';
import { EditorialModule } from './editorial/editorial.module';
import { AccessDeniedErrorFilter } from './filters/access-denied-error.filter';
import { DomainValidationErrorFilter } from './filters/domain-validation-error.filter';
import { DuplicateItemErrorFilter } from './filters/duplicate-item-error.filter';
import { ItemNotFoundErrorFilter } from './filters/item-not-found-error.filter';
import { I18nModule } from './i18n/i18n.module';
import { ProductModule } from './product/product.module';
import { UploadModule } from './upload/upload.module';
import { UserModule } from './user/user.module';

const routes: Routes = [
  {
    path: '/',
    children: [
      {
        path: '/:lang',
        module: EditorialModule,
      },
      {
        path: '/i18n',
        module: I18nModule,
      },
      {
        path: '/career',
        module: CareerModule,
      },
      {
        path: '/upload',
        module: UploadModule,
      },
      {
        path: '/user',
        module: UserModule,
      },
      {
        path: '/admin',
        module: AdminModule,
      },
      {
        path: '/product',
        module: ProductModule,
      },
    ],
  },
];

@Module({
  imports: [
    RouterModule.forRoutes(routes),
    EditorialModule,
    I18nModule,
    CareerModule,
    UploadModule,
    UserModule,
    AdminModule,
    ProductModule,
    SwaggerModule,
  ],
  providers: [
    { provide: APP_FILTER, useClass: DomainValidationErrorFilter },
    { provide: APP_FILTER, useClass: ItemNotFoundErrorFilter },
    { provide: APP_FILTER, useClass: DuplicateItemErrorFilter },
    { provide: APP_FILTER, useClass: AccessDeniedErrorFilter },
  ],
})
export class RestModule {}

import { HttpService } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { of } from 'rxjs';
import { EnvironmentConfigService } from '../../../config/environment-config/environment-config.service';
import { Genium360ApiJobOfferTarifResponseInterface } from '../genium360-adapter.service';
import { Genium360ApiService } from '../genium360-api.service';

describe('infrastructure/vendors/genium360/Genium360ApiService', () => {
  let genium360ApiService: Genium360ApiService;
  let mockEnvironmentConfigService: EnvironmentConfigService;
  let mockHttpService: HttpService;

  beforeEach(() => {
    mockEnvironmentConfigService = {} as EnvironmentConfigService;
    mockEnvironmentConfigService.get = jest.fn();

    mockHttpService = {} as HttpService;
    mockHttpService.get = jest.fn();

    genium360ApiService = new Genium360ApiService(mockEnvironmentConfigService, mockHttpService);
  });

  describe('getGeniusProducts()', () => {
    it('should call job-offer-tarif genium360 api with url and access token from configuration', async () => {
      // given
      const expectedGenium360Url: string = 'https://api';
      const expectedGenium360ApiAccessToken: string = 'super-secret-access-token';
      (mockEnvironmentConfigService.get as jest.Mock).mockReturnValueOnce(expectedGenium360Url).mockReturnValueOnce(expectedGenium360ApiAccessToken);
      (mockHttpService.get as jest.Mock).mockReturnValue(of({ data: [] }));

      // when
      await genium360ApiService.getGeniusProducts();

      // then
      expect(mockHttpService.get).toHaveBeenCalledWith('https://api/job-offer-tarif/?g_buster=True', {
        headers: { Authorization: `Token ${expectedGenium360ApiAccessToken}` },
      });
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('GENIUM360_API_URL');
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('GENIUM360_API_ACCESS_TOKEN');
    });

    it('should return http response as Genium360ApiJobOfferTarifResponseInterface', async () => {
      // given
      const expected: object = [{ id: 123456, name: 'A job offer tarif', type_de_diffusion_c: 'Visibilité entreprise' }];

      const axiosResponse: AxiosResponse = {
        data: expected,
      } as AxiosResponse;
      (mockHttpService.get as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      const result: Genium360ApiJobOfferTarifResponseInterface[] = await genium360ApiService.getGeniusProducts();

      // then
      expect(result).toStrictEqual(expected);
    });

    it('should only return "Visibilité entreprise" "type_de_diffusion_c" products', async () => {
      // given
      const mockResponse: Genium360ApiJobOfferTarifResponseInterface[] = [
        { id: 123456, name: 'A job offer tarif', type_de_diffusion_c: 'Visibilité entreprise' } as Genium360ApiJobOfferTarifResponseInterface,
        { id: 123456, name: 'An other job offer tarif', type_de_diffusion_c: 'Affichage' } as Genium360ApiJobOfferTarifResponseInterface,
      ];

      const axiosResponse: AxiosResponse = {
        data: mockResponse,
      } as AxiosResponse;
      (mockHttpService.get as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      const result: Genium360ApiJobOfferTarifResponseInterface[] = await genium360ApiService.getGeniusProducts();

      // then
      expect(result).toStrictEqual([{ id: 123456, name: 'A job offer tarif', type_de_diffusion_c: 'Visibilité entreprise' }]);
    });
  });
});

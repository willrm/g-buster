import { GeniusProductInterface } from '../../../../domain/genius-product/genius-product.interface';
import { GeniusProductRawInterface } from '../../../../domain/genius-product/genius-product.raw.interface';
import { Genium360AdapterService, Genium360ApiJobOfferTarifResponseInterface } from '../genium360-adapter.service';

describe('infrastructure/vendors/genium360/Genium360AdapterService', () => {
  let genium360AdapterService: Genium360AdapterService;
  beforeEach(() => {
    genium360AdapterService = new Genium360AdapterService();
  });

  describe('mapGeniusProducts()', () => {
    it('should return a genius product raw object', () => {
      // given
      const expectedGeniusProduct: GeniusProductRawInterface = {
        id: '123',
        name: 'A Job Product',
        inTheSpotlightCounter: 12,
        featuredCounter: 15,
        price: 150,
      } as GeniusProductInterface;

      const genium360ApiResponse: Genium360ApiJobOfferTarifResponseInterface[] = [
        {
          sfid: '123',
          name: 'A Job Product',
          nombre_d_offre_a_la_une: 12,
          nombre_d_offre_en_vedette: 15,
          montant_du_forfait_c: 150,
        },
      ];

      // when
      const result: GeniusProductRawInterface[] = genium360AdapterService.mapGeniusProducts(genium360ApiResponse);

      // then
      expect(result).toStrictEqual([expectedGeniusProduct]);
    });
  });
});

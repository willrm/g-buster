import { HttpModule, Module } from '@nestjs/common';
import { EnvironmentConfigModule } from '../../config/environment-config/environment-config.module';
import { Genium360AdapterService } from './genium360-adapter.service';
import { Genium360ApiService } from './genium360-api.service';

@Module({
  imports: [EnvironmentConfigModule, HttpModule],
  providers: [Genium360ApiService, Genium360AdapterService],
  exports: [Genium360ApiService, Genium360AdapterService],
})
export class Genium360Module {}

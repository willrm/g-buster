import { Injectable } from '@nestjs/common';
import { GeniusProductRawInterface } from '../../../domain/genius-product/genius-product.raw.interface';

export interface Genium360ApiJobOfferTarifResponseInterface {
  id?: number;
  description_c?: string;
  duree_de_diffusion_c?: string;
  lastvieweddate?: string;
  de_reduction_autres_offres_c?: number;
  type_de_diffusion_c?: string;
  systemmodstamp?: string;
  createdbyid?: string;
  lastreferenceddate?: string;
  montant_du_forfait_c?: number;
  sfid?: string;
  lastmodifiedbyid?: string;
  de_reduction_c?: number;
  name?: string;
  lastmodifieddate?: string;
  prix_par_affichage_c?: number;
  ownerid?: string;
  statut_du_forfait_c?: string;
  lastactivitydate?: string;
  disponible_pour_contrat_d_emploi_c?: false;
  field_hc_err?: string;
  nombre_d_affichage_inclue_c?: string;
  isdeleted?: boolean;
  sous_type_de_diffusion_c?: string;
  createddate?: string;
  produit_c?: string;
  field_hc_lastop?: string;
  g_buster?: boolean;
  nombre_d_offre_a_la_une?: number;
  nombre_d_offre_en_vedette?: number;
}

@Injectable()
export class Genium360AdapterService {
  mapGeniusProducts(genium360ApiJobOfferTarifs: Genium360ApiJobOfferTarifResponseInterface[]): GeniusProductRawInterface[] {
    return genium360ApiJobOfferTarifs.map(
      (genium360ApiJobOfferTarif: Genium360ApiJobOfferTarifResponseInterface) =>
        ({
          id: genium360ApiJobOfferTarif.sfid,
          name: genium360ApiJobOfferTarif.name,
          price: genium360ApiJobOfferTarif.montant_du_forfait_c,
          featuredCounter: genium360ApiJobOfferTarif.nombre_d_offre_en_vedette,
          inTheSpotlightCounter: genium360ApiJobOfferTarif.nombre_d_offre_a_la_une,
        } as GeniusProductRawInterface)
    );
  }
}

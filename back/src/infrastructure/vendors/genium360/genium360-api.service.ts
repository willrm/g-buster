import { HttpService, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { map } from 'rxjs/operators';
import { EnvironmentConfigService } from '../../config/environment-config/environment-config.service';
import { Genium360ApiJobOfferTarifResponseInterface } from './genium360-adapter.service';

@Injectable()
export class Genium360ApiService {
  private readonly GENIUS_PRODUCT_BROADCAST_SUBTYPE: string = 'Visibilité entreprise';

  constructor(private readonly environmentConfigService: EnvironmentConfigService, private readonly httpService: HttpService) {}

  async getGeniusProducts(): Promise<Genium360ApiJobOfferTarifResponseInterface[]> {
    const GENIUM360_API_URL: string = this.environmentConfigService.get('GENIUM360_API_URL');
    const GENIUM360_API_ACCESS_TOKEN: string = this.environmentConfigService.get('GENIUM360_API_ACCESS_TOKEN');

    return this.httpService
      .get(`${GENIUM360_API_URL}/job-offer-tarif/?g_buster=True`, {
        headers: {
          Authorization: `Token ${GENIUM360_API_ACCESS_TOKEN}`,
        },
      })
      .pipe(map((response: AxiosResponse<unknown>) => response.data as Genium360ApiJobOfferTarifResponseInterface[]))
      .pipe(
        map((genium360ApiJobOfferTarifResponse: Genium360ApiJobOfferTarifResponseInterface[]) =>
          genium360ApiJobOfferTarifResponse.filter(
            (genium360ApiJobOfferTarif: Genium360ApiJobOfferTarifResponseInterface) =>
              genium360ApiJobOfferTarif.type_de_diffusion_c === this.GENIUS_PRODUCT_BROADCAST_SUBTYPE
          )
        )
      )
      .toPromise();
  }
}

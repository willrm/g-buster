import { Module } from '@nestjs/common';
import { Auth0UserMetadataAdapterService } from './auth0-user-metadata-adapter.service';

@Module({
  providers: [Auth0UserMetadataAdapterService],
  exports: [Auth0UserMetadataAdapterService],
})
export class Auth0Module {}

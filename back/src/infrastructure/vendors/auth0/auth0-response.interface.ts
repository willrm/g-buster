export interface Auth0UserMetadataInterface {
  sous_type_non_adherent: string;
  sous_type_membre: string;
  type_membre: string;
  user_id: number;
  contact_id: number;
  contact_sfid: string;
  email: string;
  first_name: string;
  last_name: string;
  company_name: string;
}

export interface Auth0ResponseInterface {
  'https://career.genium360.ca/user_metadata': Auth0UserMetadataInterface;
}

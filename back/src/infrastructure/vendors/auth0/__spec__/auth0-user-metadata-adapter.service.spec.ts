import { UserRawInterface } from '../../../../domain/user/user.raw.interface';
import { Auth0ResponseInterface } from '../auth0-response.interface';
import { Auth0UserMetadataAdapterService } from '../auth0-user-metadata-adapter.service';

describe('infrastructure/vendors/auth0/Auth0UserMetadataAdapterService', () => {
  let auth0UserMetadataAdapterService: Auth0UserMetadataAdapterService;

  beforeEach(() => {
    auth0UserMetadataAdapterService = new Auth0UserMetadataAdapterService();
  });

  describe('toUserRaw()', () => {
    it('should return user raw mapped from response data', () => {
      // given
      const auth0Response: Auth0ResponseInterface = {
        'https://career.genium360.ca/user_metadata': {
          sous_type_non_adherent: 'sous_type_non_adherent value',
          sous_type_membre: 'sous_type_membre value',
          type_membre: 'type_membre value',
          user_id: 123456,
          contact_id: 456789,
          contact_sfid: 'ABC123456789XYZ',
          email: 'test@example.org',
          first_name: 'Test first name',
          last_name: 'Test last name',
          company_name: 'Test company name',
        },
      };

      // when
      const result: UserRawInterface = auth0UserMetadataAdapterService.toUserRaw(auth0Response);

      // then
      expect(result).toStrictEqual({
        companyName: 'Test company name',
        email: 'test@example.org',
        firstName: 'Test first name',
        id: 123456,
        lastName: 'Test last name',
        subType: 'sous_type_non_adherent value',
        type: 'type_membre value',
        salesforceId: 'ABC123456789XYZ',
      } as UserRawInterface);
    });
  });
});

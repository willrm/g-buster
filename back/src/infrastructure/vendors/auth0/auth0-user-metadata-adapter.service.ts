import { Injectable } from '@nestjs/common';
import { UserRawInterface } from '../../../domain/user/user.raw.interface';
import { Auth0ResponseInterface, Auth0UserMetadataInterface } from './auth0-response.interface';

@Injectable()
export class Auth0UserMetadataAdapterService {
  toUserRaw(auth0Response: Auth0ResponseInterface): UserRawInterface {
    const userMetadata: Auth0UserMetadataInterface = auth0Response['https://career.genium360.ca/user_metadata'];

    return {
      id: userMetadata.user_id,
      firstName: userMetadata.first_name,
      lastName: userMetadata.last_name,
      email: userMetadata.email,
      companyName: userMetadata.company_name,
      type: userMetadata.type_membre,
      subType: userMetadata.sous_type_non_adherent,
      salesforceId: userMetadata.contact_sfid,
    } as UserRawInterface;
  }
}

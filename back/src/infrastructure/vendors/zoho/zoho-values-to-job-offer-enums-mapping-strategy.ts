import {
  AdvantageFinancial,
  AdvantageHealth,
  AdvantageLifeBalance,
  AdvantageProfessional,
  AdvantageSocial,
} from '../../../domain/advantages/advantages-enums';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from '../../../domain/job-offer/job-offer-enums';
import { TravelingFrequency } from '../../../domain/traveling/traveling-enums';

export const ISO_8601_DATE_WITHOUT_TIME_LENGTH: number = 10;
export const ZOHO_EMAIL_UNIQUE_SUFFIX_WITHOUT_TIMESTAMP: string = '+REMOVE-THIS-TIMESTAMPED-SUFFIX-TO-HAVE-REAL-EMAIL-';

export const ZOHO_VALUE_SEPARATOR: string = ';';

export const ZOHO_VALUE_TO_JOB_OFFER_STATUS_MAPPING_STRATEGY: { [key: string]: JobOfferStatus } = {
  Rempli: JobOfferStatus.STATUS_DRAFT,
  'En attente d’approbation': JobOfferStatus.STATUS_PENDING_VALIDATION,
  'En cours': JobOfferStatus.STATUS_PUBLISHED,
  Inactif: JobOfferStatus.STATUS_ARCHIVED,
};

export const ZOHO_VALUE_TO_JOB_OFFER_JOB_STATUS_MAPPING_STRATEGY: { [key: string]: JobOfferJobStatus } = {
  'Temps plein': JobOfferJobStatus.JOB_STATUS_FULL_TIME,
  'Temps partiel': JobOfferJobStatus.JOB_STATUS_PART_TIME,
};

export const ZOHO_VALUE_TO_JOB_OFFER_JOB_TYPE_MAPPING_STRATEGY: { [key: string]: JobOfferJobType } = {
  Stage: JobOfferJobType.JOB_TYPE_INTERNSHIP,
  Temporaire: JobOfferJobType.JOB_TYPE_TEMPORARY,
  Régulier: JobOfferJobType.JOB_TYPE_REGULAR,
};

export const ZOHO_VALUE_TO_JOB_OFFER_REGION_MAPPING_STRATEGY: { [key: string]: JobOfferRegion } = {
  'Abitibi-Témiscamingue': JobOfferRegion.REGION_ABITIBI_TEMISCAMINGUE,
  'Bas St-Laurent': JobOfferRegion.REGION_BAS_ST_LAURENT,
  'Capitale-Nationale': JobOfferRegion.REGION_CAPITALE_NATIONALE,
  'Centre du Québec': JobOfferRegion.REGION_CENTRE_DU_QUEBEC,
  'Chaudières-Appalaches': JobOfferRegion.REGION_CHAUDIERES_APPALACHES,
  'Côte-Nord': JobOfferRegion.REGION_COTE_NORD,
  Estrie: JobOfferRegion.REGION_ESTRIE,
  'Gaspésie/Îles-de-la-Madeleine': JobOfferRegion.REGION_GASPESIE_ILES_DE_LA_MADELEINE,
  Lanaudière: JobOfferRegion.REGION_LANAUDIERE,
  Laurentides: JobOfferRegion.REGION_LAURENTIDES,
  Laval: JobOfferRegion.REGION_LAVAL,
  Mauricie: JobOfferRegion.REGION_MAURICIE,
  Montérégie: JobOfferRegion.REGION_MONTEREGIE,
  Montréal: JobOfferRegion.REGION_MONTREAL,
  'Nord du Québec': JobOfferRegion.REGION_NORD_DU_QUEBEC,
  Outaouais: JobOfferRegion.REGION_OUTAOUAIS,
  'Saguenay-Lac-St-Jean': JobOfferRegion.REGION_SAGUENAY_LAC_ST_JEAN,
};

export const ZOHO_VALUE_TO_JOB_OFFER_TARGET_CUSTOMER_MAPPING_STRATEGY: { [key: string]: JobOfferTargetCustomer } = {
  Ingénieur: JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS,
  'Ingénieurs juniors / CPI': JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP,
  'Diplômés en génie': JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERING_GRADUATES,
  'Étudiants et finissants': JobOfferTargetCustomer.TARGET_CUSTOMER_STUDENTS_AND_GRADUATES,
};

export const ZOHO_VALUE_TO_JOB_OFFER_REQUIRED_EXPERIENCE_MAPPING_STRATEGY: { [key: string]: JobOfferRequiredExperience } = {
  '0 à 2 ans': JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS,
  '3 à 5 ans': JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
  '6 à 10 ans': JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
  '11 ans et plus': JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
};

export const ZOHO_VALUE_TO_JOB_OFFER_POSITION_TYPE_MAPPING_STRATEGY: { [key: string]: JobOfferPositionType } = {
  Analyste: JobOfferPositionType.POSITION_TYPE_ANALYST,
  Autres: JobOfferPositionType.POSITION_TYPE_OTHERS,
  'Chargé(e) de projet': JobOfferPositionType.POSITION_TYPE_PROJECT_MANAGER,
  'Chef de service': JobOfferPositionType.POSITION_TYPE_HEAD_OF_DEPARTMENT,
  Chercheur: JobOfferPositionType.POSITION_TYPE_RESEARCHER,
  Concepteur: JobOfferPositionType.POSITION_TYPE_DESIGNER,
  'Conseiller(e)': JobOfferPositionType.POSITION_TYPE_ADVISOR,
  'Consultante(e)': JobOfferPositionType.POSITION_TYPE_CONSULTANT,
  Coordonnateur: JobOfferPositionType.POSITION_TYPE_COORDINATOR,
  'Directeur Général': JobOfferPositionType.POSITION_TYPE_MANAGING_DIRECTOR,
  'Directeur des opérations': JobOfferPositionType.POSITION_TYPE_DIRECTOR_OF_OPERATIONS,
  'Directeur/Directrice': JobOfferPositionType.POSITION_TYPE_DIRECTOR_DIRECTOR,
  'Développeur / Programmeur': JobOfferPositionType.POSITION_TYPE_DEVELOPER_PROGRAMMER,
  'Ingénieur forestier': JobOfferPositionType.POSITION_TYPE_FORESTRY_ENGINEER,
  'Ingénieurs juniors / CPI': JobOfferPositionType.POSITION_TYPE_JUNIOR_ENGINEERS_CEP,
  'Ingénieur(e)': JobOfferPositionType.POSITION_TYPE_ENGINEER,
  'Ingénieur(e) de projet': JobOfferPositionType.POSITION_TYPE_PROJECT_ENGINEER,
  Professeur: JobOfferPositionType.POSITION_TYPE_PROFESSOR,
  'Président/Présidente': JobOfferPositionType.POSITION_TYPE_PRESIDENT_OF_THE_BOARD_OF_DIRECTORS,
  'Vice-Président(e)': JobOfferPositionType.POSITION_TYPE_VICE_CHAIRPERSON,
};

export const ZOHO_VALUE_TO_JOB_OFFER_REQUESTED_SPECIALITY_MAPPING_STRATEGY: { [key: string]: JobOfferRequestedSpeciality } = {
  'Aéronautique / aérospatiale': JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
  Agriculture: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
  'Aucune en particulier': JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NONE_IN_PARTICULAR,
  'Autre spécialité': JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OTHER_SPECIALITY,
  Biomédical: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL,
  Biotechnologie: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOTECHNOLOGY,
  Chimie: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CHEMISTRY,
  Civil: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CIVIL,
  Construction: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CONSTRUCTION,
  Documentation: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_DOCUMENTATION,
  Électrique: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ELECTRICAL,
  Environnement: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ENVIRONMENT,
  Géologie: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_GEOLOGY,
  'Gestion de la qualité': JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_QUALITY_MANAGEMENT,
  'Industriel / Manufacturier': JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INDUSTRIAL_MANUFACTURER,
  Informatique: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INFORMATION_TECHNOLOGY,
  Instrumentation: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INSTRUMENTATION,
  Logiciel: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_SOFTWARE,
  'Manutention / Distribution des matériaux': JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MATERIAL_HANDLING_DISTRIBUTION,
  'Maritime / Naval': JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MARITIME_NAVAL,
  'Matériaux non métalliques': JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NON_METALLIC_MATERIALS,
  Mécanique: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MECHANICS,
  'Métallurgie / Métaux': JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_METALLURGY_METALS,
  Minier: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MINING,
  Nucléaire: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NUCLEAR_POWER,
  'Pâtes et Papiers': JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PULP_AND_PAPER,
  Physique: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PHYSICAL,
  'Production automatisée': JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION,
  Robotique: JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ROBOTICS,
  'Santé et Sécurité du travail': JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OCCUPATIONAL_HEALTH_AND_SAFETY,
};

export const ZOHO_VALUE_TO_JOB_OFFER_TRAVELING_IS_TRAVELING_MAPPING_STRATEGY: { [key: string]: boolean } = {
  Oui: true,
  Non: false,
};

export const ZOHO_VALUE_TO_TRAVELING_FREQUENCY_MAPPING_STRATEGY: { [key: string]: TravelingFrequency } = {
  Occasionnels: TravelingFrequency.TRAVELING_OCCASIONAL,
  Fréquents: TravelingFrequency.TRAVELING_FREQUENT,
};

export const ZOHO_VALUE_TO_ADVANTAGE_HEALTH_MAPPING_STRATEGY: { [key: string]: AdvantageHealth } = {
  'Assurance dentaire': AdvantageHealth.HEALTH_DENTAL_INSURANCE,
  'Assurance frais médicaux': AdvantageHealth.HEALTH_MEDICAL_EXPENSES_INSURANCE,
  'Assurance vue': AdvantageHealth.HEALTH_SIGHT_INSURANCE,
  'Assurance vie': AdvantageHealth.HEALTH_LIFE_INSURANCE,
  'Assurance voyage': AdvantageHealth.HEALTH_TRAVEL_INSURANCE,
  'Assurance salaire': AdvantageHealth.HEALTH_SALARY_INSURANCE,
  'Assurance invalidité courte durée': AdvantageHealth.HEALTH_SHORT_TERM_DISABILITY_INSURANCE,
  'Assurance invalidité longue durée': AdvantageHealth.HEALTH_LONG_TERM_DISABILITY_INSURANCE,
};

export const ZOHO_VALUE_TO_ADVANTAGE_SOCIAL_MAPPING_STRATEGY: { [key: string]: AdvantageSocial } = {
  'Activités sociales': AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES,
  'Voyages et vacances': AdvantageSocial.SOCIAL_TRAVEL_AND_HOLIDAYS,
  Cafétéria: AdvantageSocial.SOCIAL_CAFETERIA,
  'Fruits frais': AdvantageSocial.SOCIAL_FRESH_FRUIT_AND_VEGETABLES,
  'Salle de détente': AdvantageSocial.SOCIAL_RELAXATION_ROOM,
  'Programme mieux-être': AdvantageSocial.SOCIAL_WELLNESS_PROGRAM,
  'Programme de référencement': AdvantageSocial.SOCIAL_REFERENCING_PROGRAM,
};

export const ZOHO_VALUE_TO_ADVANTAGE_LIFE_BALANCE_MAPPING_STRATEGY: { [key: string]: AdvantageLifeBalance } = {
  'Conciliation famille-travail': AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK,
  'Congés payés': AdvantageLifeBalance.LIFE_BALANCE_PAID_HOLIDAYS,
  'Horaires flexibles': AdvantageLifeBalance.LIFE_BALANCE_FLEXIBLE_SCHEDULES,
  'Congés maladie': AdvantageLifeBalance.LIFE_BALANCE_SICK_LEAVE,
  'Horaires d’été': AdvantageLifeBalance.LIFE_BALANCE_SUMMER_HOURS,
  'Congés mobiles': AdvantageLifeBalance.LIFE_BALANCE_MOBILE_HOLIDAYS,
};

export const ZOHO_VALUE_TO_ADVANTAGE_FINANCIAL_MAPPING_STRATEGY: { [key: string]: AdvantageFinancial } = {
  'Rabais employé': AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT,
  'Rémunération compétitive': AdvantageFinancial.FINANCIAL_COMPETITIVE_COMPENSATION,
  'Régime retraite': AdvantageFinancial.FINANCIAL_PENSION_PLAN,
  'Régime de participation différée aux bénéfices RPDB': AdvantageFinancial.FINANCIAL_DEFERRED_PROFIT_SHARING_PLAN_DPSP,
  PAE: AdvantageFinancial.FINANCIAL_EAP,
  'Reconnaissance ancienneté': AdvantageFinancial.FINANCIAL_RECOGNITION_OF_SENIORITY,
  'Remboursement cotisation à une association ou ordre professionnel':
    AdvantageFinancial.FINANCIAL_REFUND_OF_DUES_TO_A_PROFESSIONAL_ASSOCIATION_OR_ORDER,
  'Boni sur performance': AdvantageFinancial.FINANCIAL_PERFORMANCE_BONUS,
  Stationnement: AdvantageFinancial.FINANCIAL_PARKING_LOT,
  'Abonnement de transport': AdvantageFinancial.FINANCIAL_TRANSPORT_SUBSCRIPTION,
};

export const ZOHO_VALUE_TO_ADVANTAGE_PROFESSIONAL_MAPPING_STRATEGY: { [key: string]: AdvantageProfessional } = {
  'Programme de formation et développement': AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM,
  'Programme de reconnaissance': AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM,
};

export const ZOHO_VALUE_TO_JOB_OFFER_WANTED_PERSONALITY_MAPPING_STRATEGY: { [key: string]: JobOfferWantedPersonality } = {
  Communicant: JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS,
  Ambitieux: JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS,
  Autonome: JobOfferWantedPersonality.WANTED_PERSONALITY_AUTONOMOUS,
  Innovant: JobOfferWantedPersonality.WANTED_PERSONALITY_INNOVATIVE,
  Curieux: JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS,
  Décideur: JobOfferWantedPersonality.WANTED_PERSONALITY_DECISION_MAKERS,
  Stratège: JobOfferWantedPersonality.WANTED_PERSONALITY_STRATEGISTS,
  Flexible: JobOfferWantedPersonality.WANTED_PERSONALITY_FLEXIBLE,
  Leader: JobOfferWantedPersonality.WANTED_PERSONALITY_LEADERS,
  Négociateur: JobOfferWantedPersonality.WANTED_PERSONALITY_NEGOTIATORS,
  Organisé: JobOfferWantedPersonality.WANTED_PERSONALITY_ORGANIZED,
  Rigoureux: JobOfferWantedPersonality.WANTED_PERSONALITY_RIGOROUS,
  Débrouillard: JobOfferWantedPersonality.WANTED_PERSONALITY_RESOURCEFUL,
  "D'équipe": JobOfferWantedPersonality.WANTED_PERSONALITY_TEAMWORK,
  Orateur: JobOfferWantedPersonality.WANTED_PERSONALITY_SPEAKERS_AT_THE_CONFERENCE,
  Pondérateur: JobOfferWantedPersonality.WANTED_PERSONALITY_PONDERATOR,
  Diplomate: JobOfferWantedPersonality.WANTED_PERSONALITY_DIPLOMATS,
};

export const ZOHO_VALUE_TO_JOB_OFFER_START_DATE_OF_EMPLOYMENT_AS_SOON_AS_POSSIBLE_MAPPING_STRATEGY: { [key: string]: boolean } = {
  Oui: true,
  Non: false,
};

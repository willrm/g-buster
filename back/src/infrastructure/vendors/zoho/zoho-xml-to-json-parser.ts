import { Injectable } from '@nestjs/common';
import { isEmpty } from 'lodash';
import { parseStringPromise } from 'xml2js';
import { ZohoJsonFieldInterface } from './models/json/zoho-json-field.interface';
import { ZohoJsonGetRecordsResponseInterface } from './models/json/zoho-json-get-records-response.interface';
import { ZohoJsonRowInterface } from './models/json/zoho-json-row.interface';
import { ATTRIBUTES_KEY, CHARACTER_CONTENT_KEY } from './models/xml/node-xml2js-options';
import { ZohoXmlFieldInterface } from './models/xml/zoho-xml-field.interface';
import { ZohoXmlGetRecordsResponseInterface } from './models/xml/zoho-xml-get-records-response.interface';
import { ZohoXmlRowInterface } from './models/xml/zoho-xml-row.interface';
import { ZohoApiModule } from './zoho-api.service';

@Injectable()
export class ZohoXmlToJsonParser {
  async toJson(zohoApiModule: ZohoApiModule, xmlData: string): Promise<ZohoJsonGetRecordsResponseInterface> {
    const zohoXmlGetRecordsResponse: ZohoXmlGetRecordsResponseInterface = await parseStringPromise(xmlData, {
      attrkey: ATTRIBUTES_KEY,
      charkey: CHARACTER_CONTENT_KEY,
    });

    return Promise.resolve(this.responseToJson(zohoApiModule, zohoXmlGetRecordsResponse));
  }

  private responseToJson(
    zohoApiModule: ZohoApiModule,
    zohoXmlGetRecordsResponse: ZohoXmlGetRecordsResponseInterface
  ): ZohoJsonGetRecordsResponseInterface {
    const zohoJsonGetRecordsResponse: ZohoJsonGetRecordsResponseInterface = { response: { result: {} } };
    zohoJsonGetRecordsResponse.response.result[zohoApiModule] = {
      row: this.rowsToJson(zohoApiModule, zohoXmlGetRecordsResponse),
    };

    return zohoJsonGetRecordsResponse;
  }

  private rowsToJson(zohoApiModule: ZohoApiModule, zohoXmlGetRecordsResponse: ZohoXmlGetRecordsResponseInterface): ZohoJsonRowInterface[] {
    const zohoJsonRows: ZohoJsonRowInterface[] = [];
    if (!isEmpty(zohoXmlGetRecordsResponse.response.result)) {
      zohoXmlGetRecordsResponse.response.result[0][zohoApiModule][0].row.forEach((zohoXmlRow: ZohoXmlRowInterface) => {
        zohoJsonRows.push(this.singleRowToJson(zohoXmlRow));
      });
    }

    return zohoJsonRows;
  }

  private singleRowToJson(zohoXmlRow: ZohoXmlRowInterface): ZohoJsonRowInterface {
    return {
      no: zohoXmlRow.attributes.no,
      FL: this.fieldsToJson(zohoXmlRow),
    } as ZohoJsonRowInterface;
  }

  private fieldsToJson(zohoXmlRow: ZohoXmlRowInterface): ZohoJsonFieldInterface[] {
    const zohoJsonFields: ZohoJsonFieldInterface[] = [];
    zohoXmlRow.FL.forEach((zohoXmlField: ZohoXmlFieldInterface) => {
      zohoJsonFields.push(this.singleFieldToJson(zohoXmlField));
    });

    return zohoJsonFields;
  }

  private singleFieldToJson(zohoXmlField: ZohoXmlFieldInterface): ZohoJsonFieldInterface {
    return {
      val: zohoXmlField.attributes.val,
      content: zohoXmlField.characterContent,
    } as ZohoJsonFieldInterface;
  }
}

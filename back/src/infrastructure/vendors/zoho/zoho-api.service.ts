import { HttpService, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import FormData from 'form-data';
import { EOL } from 'os';
import { stringify } from 'qs';
import { map } from 'rxjs/operators';
import { EnvironmentConfigService } from '../../config/environment-config/environment-config.service';
import { ZohoJsonGetRecordsResponseInterface } from './models/json/zoho-json-get-records-response.interface';
import { ZohoJsonPostRecordsRequestInterface } from './models/json/zoho-json-post-records-request.interface';
import { ZohoApiInterface } from './zoho-api.interface';
import { ZohoJsonToXmlParser } from './zoho-json-to-xml-parser';
import { ZohoSearchCondition } from './zoho-search-condition';
import { ZohoXmlToJsonParser } from './zoho-xml-to-json-parser';
import { ZohoError } from './zoho.error';

@Injectable()
export class ZohoApiService implements ZohoApiInterface {
  private readonly ZOHO_API_SCOPE: string = 'recruitapi';
  private readonly ZOHO_API_VERSION: number = 2;

  constructor(
    private readonly environmentConfigService: EnvironmentConfigService,
    private readonly httpService: HttpService,
    private readonly zohoXmlToJsonParser: ZohoXmlToJsonParser,
    private readonly zohoJsonToXmlParser: ZohoJsonToXmlParser
  ) {}

  async getRecordById(zohoApiModule: ZohoApiModule, id: string): Promise<ZohoJsonGetRecordsResponseInterface> {
    const queryParams: string = this.getZohoQueryParams({ id });
    const zohoApiMethod: string = `getRecordById?${queryParams}`;

    return this.get(zohoApiModule, zohoApiMethod);
  }

  async getRecords(zohoApiModule: ZohoApiModule): Promise<ZohoJsonGetRecordsResponseInterface> {
    const queryParams: string = this.getZohoQueryParams();
    const zohoApiMethod: string = `getRecords?${queryParams}`;

    return this.get(zohoApiModule, zohoApiMethod);
  }

  async getSearchRecords<T>(zohoApiModule: ZohoApiModule, zohoSearchCondition: ZohoSearchCondition): Promise<ZohoJsonGetRecordsResponseInterface> {
    const queryParams: string = this.getZohoQueryParams({
      selectColumns: 'All',
      searchCondition: this.asQueryParam(zohoSearchCondition),
    });
    const zohoApiMethod: string = `getSearchRecords?${queryParams}`;

    return this.get(zohoApiModule, zohoApiMethod);
  }

  async addRecords(zohoApiModule: ZohoApiModule, zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface): Promise<string> {
    const queryParams: string = this.getZohoQueryParams();
    const zohoApiMethod: string = `addRecords?${queryParams}`;
    const xmlData: string = await this.zohoJsonToXmlParser.toXml(zohoApiModule, zohoJsonPostRecordsRequest);

    return await this.post(zohoApiModule, zohoApiMethod, xmlData);
  }

  async updateRecords(zohoApiModule: ZohoApiModule, id: string, zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface): Promise<string> {
    const queryParams: string = this.getZohoQueryParams({ id });
    const zohoApiMethod: string = `updateRecords?${queryParams}`;
    const xmlData: string = await this.zohoJsonToXmlParser.toXml(zohoApiModule, zohoJsonPostRecordsRequest);

    return await this.post(zohoApiModule, zohoApiMethod, xmlData);
  }

  async changeStatus(zohoApiModule: ZohoApiModule, id: string, status: string): Promise<string> {
    const queryParams: string = this.getZohoQueryParams({ id, status });
    const zohoApiMethod: string = `changeStatus?${queryParams}`;

    return await this.post(zohoApiModule, zohoApiMethod);
  }

  async associateJobOpening(zohoApiModule: ZohoApiModule, candidateId: string, jobId: string): Promise<string> {
    const queryParams: string = this.getZohoQueryParams({ candidateIds: candidateId, jobIds: jobId });
    const zohoApiMethod: string = `associateJobOpening?${queryParams}`;

    return await this.post(zohoApiModule, zohoApiMethod, null, 'candidateIds');
  }

  async uploadFile(
    zohoApiModule: ZohoApiModule,
    id: string,
    zohoFileType: ZohoFileType,
    fileAsBase64: string,
    filename: string,
    fileMimeType: string
  ): Promise<string> {
    const queryParams: string = this.getZohoQueryParams();
    const zohoApiMethod: string = `uploadFile?${queryParams}`;

    const formData: FormData = new FormData();
    formData.append('id', id);
    formData.append('type', zohoFileType);
    formData.append('content', Buffer.from(fileAsBase64, 'base64'), { filename, contentType: fileMimeType });

    const responseData: string = await this.httpService
      .post(`${this.getZohoApiUrl()}/${zohoApiModule}/${zohoApiMethod}`, formData, { headers: formData.getHeaders() })
      .pipe(map((response: AxiosResponse<string>) => response.data))
      .toPromise();

    return Promise.resolve(this.extractId(responseData, 'Id'));
  }

  private async get(zohoApiModule: ZohoApiModule, zohoApiMethod: string): Promise<ZohoJsonGetRecordsResponseInterface> {
    return await this.httpService
      .get(`${this.getZohoApiUrl()}/${zohoApiModule}/${zohoApiMethod}`)
      .pipe(map((response: AxiosResponse<string>) => response.data))
      .pipe(map((xmlData: string) => this.zohoXmlToJsonParser.toJson(zohoApiModule, xmlData)))
      .toPromise();
  }

  private async post(zohoApiModule: ZohoApiModule, zohoApiMethod: string, xmlData: string = null, fieldIdToExtract: string = 'Id'): Promise<string> {
    const postData: string = xmlData ? stringify({ xmlData }) : undefined;
    const responseData: string = await this.httpService
      .post(`${this.getZohoApiUrl()}/${zohoApiModule}/${zohoApiMethod}`, postData)
      .pipe(map((response: AxiosResponse<string>) => response.data))
      .toPromise();

    return Promise.resolve(this.extractId(responseData, fieldIdToExtract));
  }

  private getZohoQueryParams(additionalParams: object = {}): string {
    return stringify({
      authtoken: this.getZohoApiAccessToken(),
      scope: this.ZOHO_API_SCOPE,
      version: this.ZOHO_API_VERSION,
      ...additionalParams,
    });
  }

  private asQueryParam<T>(zohoSearchCondition: ZohoSearchCondition): string {
    return `(${zohoSearchCondition.field}|${zohoSearchCondition.operator}|${zohoSearchCondition.value})`;
  }

  private extractId(responseData: string, fieldIdToExtract: string): string {
    const idRegExp: RegExp = new RegExp(`.*<FL val="${fieldIdToExtract}">(\\d*)<\\/FL>.*`, 'i');
    const capturedGroups: RegExpExecArray | null = idRegExp.exec(responseData);
    if (capturedGroups && capturedGroups.length === 2) {
      return capturedGroups[1];
    }
    throw new ZohoError(`API call failed. No ${fieldIdToExtract} found in response data.${EOL}Response body: ${responseData}`);
  }

  private getZohoApiAccessToken(): string {
    return this.environmentConfigService.get('ZOHO_API_ACCESS_TOKEN');
  }

  private getZohoApiUrl(): string {
    return this.environmentConfigService.get('ZOHO_API_URL');
  }
}

export enum ZohoApiModule {
  JOB_OPENINGS = 'JobOpenings',
  CANDIDATES = 'Candidates',
}

export enum ZohoFileType {
  RESUME = 'Resume',
  COVER_LETTER = 'Cover Letter',
}

import { Injectable } from '@nestjs/common';
import { Builder } from 'xml2js';
import { ZohoJsonFieldInterface } from './models/json/zoho-json-field.interface';
import { ZohoJsonPostRecordsRequestInterface } from './models/json/zoho-json-post-records-request.interface';
import { ZohoJsonRowInterface } from './models/json/zoho-json-row.interface';
import { ATTRIBUTES_KEY, CHARACTER_CONTENT_KEY } from './models/xml/node-xml2js-options';
import { ZohoXmlFieldInterface } from './models/xml/zoho-xml-field.interface';
import { ZohoXmlPostRecordsRequestInterface } from './models/xml/zoho-xml-post-records-request.interface';
import { ZohoXmlRowInterface } from './models/xml/zoho-xml-row.interface';
import { ZohoApiModule } from './zoho-api.service';

@Injectable()
export class ZohoJsonToXmlParser {
  async toXml(zohoApiModule: ZohoApiModule, zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface): Promise<string> {
    const zohoXmlPostRecordsRequest: ZohoXmlPostRecordsRequestInterface = this.requestToXml(zohoApiModule, zohoJsonPostRecordsRequest);

    const builder: Builder = new Builder({
      attrkey: ATTRIBUTES_KEY,
      charkey: CHARACTER_CONTENT_KEY,
      cdata: true,
    });
    const xmlData: string = builder.buildObject(zohoXmlPostRecordsRequest);

    return Promise.resolve(xmlData);
  }

  private requestToXml(
    zohoApiModule: ZohoApiModule,
    zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface
  ): ZohoXmlPostRecordsRequestInterface {
    const zohoXmlPostRecordsRequest: ZohoXmlPostRecordsRequestInterface = {};
    zohoXmlPostRecordsRequest[zohoApiModule] = {
      row: this.rowsToXml(zohoApiModule, zohoJsonPostRecordsRequest),
    };

    return zohoXmlPostRecordsRequest;
  }

  private rowsToXml(zohoApiModule: ZohoApiModule, zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface): ZohoXmlRowInterface[] {
    const zohoXmlRows: ZohoXmlRowInterface[] = [];
    zohoJsonPostRecordsRequest[zohoApiModule].row.forEach((zohoJsonRow: ZohoJsonRowInterface) => {
      zohoXmlRows.push(this.singleRowToXml(zohoJsonRow));
    });

    return zohoXmlRows;
  }

  private singleRowToXml(zohoJsonRow: ZohoJsonRowInterface): ZohoXmlRowInterface {
    return {
      attributes: {
        no: zohoJsonRow.no,
      },
      FL: this.fieldsToXml(zohoJsonRow),
    } as ZohoXmlRowInterface;
  }

  private fieldsToXml(zohoJsonRow: ZohoJsonRowInterface): ZohoXmlFieldInterface[] {
    const zohoXmlFields: ZohoXmlFieldInterface[] = [];
    zohoJsonRow.FL.forEach((zohoJsonField: ZohoJsonFieldInterface) => {
      zohoXmlFields.push(this.singleFieldToXml(zohoJsonField));
    });

    return zohoXmlFields;
  }

  private singleFieldToXml(zohoJsonField: ZohoJsonFieldInterface): ZohoXmlFieldInterface {
    return {
      characterContent: zohoJsonField.content,
      attributes: {
        val: zohoJsonField.val,
      },
    } as ZohoXmlFieldInterface;
  }
}

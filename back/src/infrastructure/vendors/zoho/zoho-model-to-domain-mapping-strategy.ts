import { get, isEmpty, isNull, omitBy } from 'lodash';
import {
  AdvantageFinancial,
  AdvantageHealth,
  AdvantageLifeBalance,
  AdvantageProfessional,
  AdvantageSocial,
} from '../../../domain/advantages/advantages-enums';
import { AdvantagesInterface } from '../../../domain/advantages/advantages.interface';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from '../../../domain/job-offer/job-offer-enums';
import { TravelingFrequency } from '../../../domain/traveling/traveling-enums';
import {
  ISO_8601_DATE_WITHOUT_TIME_LENGTH,
  ZOHO_EMAIL_UNIQUE_SUFFIX_WITHOUT_TIMESTAMP,
  ZOHO_VALUE_SEPARATOR,
  ZOHO_VALUE_TO_ADVANTAGE_FINANCIAL_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_ADVANTAGE_HEALTH_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_ADVANTAGE_LIFE_BALANCE_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_ADVANTAGE_PROFESSIONAL_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_ADVANTAGE_SOCIAL_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_JOB_STATUS_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_JOB_TYPE_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_POSITION_TYPE_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_REGION_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_REQUESTED_SPECIALITY_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_REQUIRED_EXPERIENCE_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_START_DATE_OF_EMPLOYMENT_AS_SOON_AS_POSSIBLE_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_STATUS_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_TARGET_CUSTOMER_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_TRAVELING_IS_TRAVELING_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_WANTED_PERSONALITY_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_TRAVELING_FREQUENCY_MAPPING_STRATEGY,
} from './zoho-values-to-job-offer-enums-mapping-strategy';

export abstract class ZohoModelToDomainMappingStrategy {
  [val: string]: ZohoModelFieldToDomainFieldMappingStrategyInterface;

  static noOp(content: string): string {
    return content;
  }
  static toNumber(content: string): number {
    if (isEmpty(content)) {
      return null;
    } else {
      return parseInt(content.replace(/\s/g, ''), 10);
    }
  }
  static toBoolean(content: string): boolean {
    return content === 'true';
  }
  static toDate(content: string): Date {
    if (content.length === ISO_8601_DATE_WITHOUT_TIME_LENGTH) {
      return new Date(`${content}T12:00:00`);
    }

    return new Date(content);
  }
  static toEmail(content: string): string {
    return content.substring(0, content.indexOf(ZOHO_EMAIL_UNIQUE_SUFFIX_WITHOUT_TIMESTAMP));
  }
  static toStatus(content: string): JobOfferStatus {
    return ZohoModelToDomainMappingStrategy.toDomainValue<JobOfferStatus>(content, ZOHO_VALUE_TO_JOB_OFFER_STATUS_MAPPING_STRATEGY);
  }
  static toJobStatus(content: string): JobOfferJobStatus {
    return ZohoModelToDomainMappingStrategy.toDomainValue<JobOfferJobStatus>(content, ZOHO_VALUE_TO_JOB_OFFER_JOB_STATUS_MAPPING_STRATEGY);
  }
  static toJobType(content: string): JobOfferJobType {
    return ZohoModelToDomainMappingStrategy.toDomainValue<JobOfferJobType>(content, ZOHO_VALUE_TO_JOB_OFFER_JOB_TYPE_MAPPING_STRATEGY);
  }
  static toRegion(content: string): JobOfferRegion {
    return ZohoModelToDomainMappingStrategy.toDomainValue<JobOfferRegion>(content, ZOHO_VALUE_TO_JOB_OFFER_REGION_MAPPING_STRATEGY);
  }
  static toTargetCustomers(content: string): JobOfferTargetCustomer[] {
    return ZohoModelToDomainMappingStrategy.toDomainValues<JobOfferTargetCustomer>(content, ZOHO_VALUE_TO_JOB_OFFER_TARGET_CUSTOMER_MAPPING_STRATEGY);
  }
  static toRequiredExperiences(content: string): JobOfferRequiredExperience[] {
    return ZohoModelToDomainMappingStrategy.toDomainValues<JobOfferRequiredExperience>(
      content,
      ZOHO_VALUE_TO_JOB_OFFER_REQUIRED_EXPERIENCE_MAPPING_STRATEGY
    );
  }
  static toPositionType(content: string): JobOfferPositionType {
    return ZohoModelToDomainMappingStrategy.toDomainValue<JobOfferPositionType>(content, ZOHO_VALUE_TO_JOB_OFFER_POSITION_TYPE_MAPPING_STRATEGY);
  }
  static toRequestedSpecialities(content: string): JobOfferRequestedSpeciality[] {
    return ZohoModelToDomainMappingStrategy.toDomainValues<JobOfferRequestedSpeciality>(
      content,
      ZOHO_VALUE_TO_JOB_OFFER_REQUESTED_SPECIALITY_MAPPING_STRATEGY
    );
  }
  static toTravelingIsTraveling(content: string): boolean {
    return ZohoModelToDomainMappingStrategy.toDomainValue<boolean>(content, ZOHO_VALUE_TO_JOB_OFFER_TRAVELING_IS_TRAVELING_MAPPING_STRATEGY);
  }
  static toTravelingFrequency(content: string): TravelingFrequency {
    return ZohoModelToDomainMappingStrategy.toDomainValue<TravelingFrequency>(content, ZOHO_VALUE_TO_TRAVELING_FREQUENCY_MAPPING_STRATEGY);
  }
  static toAdvantages(content: string): AdvantagesInterface {
    const advantages: AdvantagesInterface = {
      health: ZohoModelToDomainMappingStrategy.toDomainValues<AdvantageHealth>(content, ZOHO_VALUE_TO_ADVANTAGE_HEALTH_MAPPING_STRATEGY),
      social: ZohoModelToDomainMappingStrategy.toDomainValues<AdvantageSocial>(content, ZOHO_VALUE_TO_ADVANTAGE_SOCIAL_MAPPING_STRATEGY),
      lifeBalance: ZohoModelToDomainMappingStrategy.toDomainValues<AdvantageLifeBalance>(
        content,
        ZOHO_VALUE_TO_ADVANTAGE_LIFE_BALANCE_MAPPING_STRATEGY
      ),
      financial: ZohoModelToDomainMappingStrategy.toDomainValues<AdvantageFinancial>(content, ZOHO_VALUE_TO_ADVANTAGE_FINANCIAL_MAPPING_STRATEGY),
      professional: ZohoModelToDomainMappingStrategy.toDomainValues<AdvantageProfessional>(
        content,
        ZOHO_VALUE_TO_ADVANTAGE_PROFESSIONAL_MAPPING_STRATEGY
      ),
    } as AdvantagesInterface;

    const result: AdvantagesInterface = omitBy(advantages, isNull);

    return isEmpty(result) ? null : result;
  }
  static toWantedPersonalities(content: string): JobOfferWantedPersonality[] {
    return ZohoModelToDomainMappingStrategy.toDomainValues<JobOfferWantedPersonality>(
      content,
      ZOHO_VALUE_TO_JOB_OFFER_WANTED_PERSONALITY_MAPPING_STRATEGY
    );
  }
  static toStartDateOfEmploymentAsSoonAsPossible(content: string): boolean {
    return ZohoModelToDomainMappingStrategy.toDomainValue<boolean>(
      content,
      ZOHO_VALUE_TO_JOB_OFFER_START_DATE_OF_EMPLOYMENT_AS_SOON_AS_POSSIBLE_MAPPING_STRATEGY
    );
  }

  private static toDomainValues<T>(content: string, mappingStrategy: { [key: string]: T }): T[] {
    if (isEmpty(content)) {
      return null;
    }

    const result: T[] = content
      .split(ZOHO_VALUE_SEPARATOR)
      .map((value: string) => ZohoModelToDomainMappingStrategy.toDomainValue<T>(value, mappingStrategy))
      .filter((parsedValue: T) => parsedValue !== null);

    return isEmpty(result) ? null : result;
  }

  private static toDomainValue<T>(content: string, mappingStrategy: { [key: string]: T }): T {
    return get(mappingStrategy, content, null);
  }
}

export interface ZohoModelFieldToDomainFieldMappingStrategyInterface {
  target: string;
  map: (content: string) => unknown;
}

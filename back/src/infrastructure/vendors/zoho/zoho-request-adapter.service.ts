import { Injectable } from '@nestjs/common';
import { forIn, get } from 'lodash';
import { DomainFieldToZohoModelFieldMappingStrategyInterface, DomainToZohoModelMappingStrategy } from './domain-to-zoho-model-mapping-strategy';
import { ZohoJsonFieldInterface } from './models/json/zoho-json-field.interface';

@Injectable()
export class ZohoRequestAdapterService {
  map<T>(domain: T, domainToZohoModelMappingStrategy: DomainToZohoModelMappingStrategy): ZohoJsonFieldInterface[] {
    const result: ZohoJsonFieldInterface[] = [];

    forIn(domainToZohoModelMappingStrategy, (mappingStrategyToApply: DomainFieldToZohoModelFieldMappingStrategyInterface, path: string) => {
      const domainValue: unknown = get(domain, path);
      if (typeof domainValue !== 'undefined' && domainValue !== null) {
        const zohoJsonField: ZohoJsonFieldInterface = this.toZohoJsonField(domainValue, mappingStrategyToApply);
        if (zohoJsonField !== null) {
          result.push(zohoJsonField);
        }
      }
    });

    return result;
  }

  private toZohoJsonField(domainValue: unknown, mappingStrategyToApply: DomainFieldToZohoModelFieldMappingStrategyInterface): ZohoJsonFieldInterface {
    const content: string = mappingStrategyToApply.map(domainValue);
    if (content === null) {
      return null;
    }

    return {
      val: mappingStrategyToApply.target,
      content,
    } as ZohoJsonFieldInterface;
  }
}

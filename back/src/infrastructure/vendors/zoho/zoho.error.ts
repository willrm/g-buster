export class ZohoError extends Error {
  constructor(message: string) {
    super(message);
  }
}

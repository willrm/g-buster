import { ZohoJsonGetRecordsResponseInterface } from './models/json/zoho-json-get-records-response.interface';
import { ZohoJsonPostRecordsRequestInterface } from './models/json/zoho-json-post-records-request.interface';
import { ZohoApiModule, ZohoFileType } from './zoho-api.service';
import { ZohoSearchCondition } from './zoho-search-condition';

export interface ZohoApiInterface {
  getRecordById(zohoApiModule: ZohoApiModule, id: string): Promise<ZohoJsonGetRecordsResponseInterface>;
  getRecords(zohoApiModule: ZohoApiModule): Promise<ZohoJsonGetRecordsResponseInterface>;
  getSearchRecords<T>(zohoApiModule: ZohoApiModule, zohoSearchCondition: ZohoSearchCondition): Promise<ZohoJsonGetRecordsResponseInterface>;
  addRecords(zohoApiModule: ZohoApiModule, zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface): Promise<string>;
  updateRecords(zohoApiModule: ZohoApiModule, id: string, zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface): Promise<string>;
  changeStatus(zohoApiModule: ZohoApiModule, id: string, status: string): Promise<string>;
  associateJobOpening(zohoApiModule: ZohoApiModule, candidateId: string, jobId: string): Promise<string>;
  uploadFile(
    zohoApiModule: ZohoApiModule,
    id: string,
    zohoFileType: ZohoFileType,
    fileAsBase64: string,
    filename: string,
    fileMimeType: string
  ): Promise<string>;
}

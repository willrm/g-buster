import { findKey, isEmpty } from 'lodash';
import {
  AdvantageFinancial,
  AdvantageHealth,
  AdvantageLifeBalance,
  AdvantageProfessional,
  AdvantageSocial,
} from '../../../domain/advantages/advantages-enums';
import { AdvantagesInterface } from '../../../domain/advantages/advantages.interface';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from '../../../domain/job-offer/job-offer-enums';
import { TravelingFrequency } from '../../../domain/traveling/traveling-enums';
import {
  ISO_8601_DATE_WITHOUT_TIME_LENGTH,
  ZOHO_EMAIL_UNIQUE_SUFFIX_WITHOUT_TIMESTAMP,
  ZOHO_VALUE_SEPARATOR,
  ZOHO_VALUE_TO_ADVANTAGE_FINANCIAL_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_ADVANTAGE_HEALTH_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_ADVANTAGE_LIFE_BALANCE_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_ADVANTAGE_PROFESSIONAL_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_ADVANTAGE_SOCIAL_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_JOB_STATUS_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_JOB_TYPE_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_POSITION_TYPE_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_REGION_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_REQUESTED_SPECIALITY_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_REQUIRED_EXPERIENCE_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_START_DATE_OF_EMPLOYMENT_AS_SOON_AS_POSSIBLE_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_STATUS_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_TARGET_CUSTOMER_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_TRAVELING_IS_TRAVELING_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_JOB_OFFER_WANTED_PERSONALITY_MAPPING_STRATEGY,
  ZOHO_VALUE_TO_TRAVELING_FREQUENCY_MAPPING_STRATEGY,
} from './zoho-values-to-job-offer-enums-mapping-strategy';

export abstract class DomainToZohoModelMappingStrategy {
  [val: string]: DomainFieldToZohoModelFieldMappingStrategyInterface;

  static noOp(value: string): string {
    return value;
  }
  static fromNumber(value: number): string {
    return `${value}`;
  }
  static fromBoolean(value: boolean): string {
    return value ? 'true' : 'false';
  }
  static fromDate(value: Date): string {
    return value.toISOString().substring(0, ISO_8601_DATE_WITHOUT_TIME_LENGTH);
  }
  static fromEmail(value: string): string {
    return `${value}${ZOHO_EMAIL_UNIQUE_SUFFIX_WITHOUT_TIMESTAMP}${new Date().getTime()}`;
  }
  static fromStatus(value: JobOfferStatus): string {
    return DomainToZohoModelMappingStrategy.toZohoValue<JobOfferStatus>(value, ZOHO_VALUE_TO_JOB_OFFER_STATUS_MAPPING_STRATEGY);
  }
  static fromJobStatus(value: JobOfferJobStatus): string {
    return DomainToZohoModelMappingStrategy.toZohoValue<JobOfferJobStatus>(value, ZOHO_VALUE_TO_JOB_OFFER_JOB_STATUS_MAPPING_STRATEGY);
  }
  static fromJobType(value: JobOfferJobType): string {
    return DomainToZohoModelMappingStrategy.toZohoValue<JobOfferJobType>(value, ZOHO_VALUE_TO_JOB_OFFER_JOB_TYPE_MAPPING_STRATEGY);
  }
  static fromRegion(value: JobOfferRegion): string {
    return DomainToZohoModelMappingStrategy.toZohoValue<JobOfferRegion>(value, ZOHO_VALUE_TO_JOB_OFFER_REGION_MAPPING_STRATEGY);
  }
  static fromTargetCustomers(values: JobOfferTargetCustomer[]): string {
    return DomainToZohoModelMappingStrategy.toZohoValues<JobOfferTargetCustomer>(values, ZOHO_VALUE_TO_JOB_OFFER_TARGET_CUSTOMER_MAPPING_STRATEGY);
  }
  static fromRequiredExperiences(values: JobOfferRequiredExperience[]): string {
    return DomainToZohoModelMappingStrategy.toZohoValues<JobOfferRequiredExperience>(
      values,
      ZOHO_VALUE_TO_JOB_OFFER_REQUIRED_EXPERIENCE_MAPPING_STRATEGY
    );
  }
  static fromPositionType(value: JobOfferPositionType): string {
    return DomainToZohoModelMappingStrategy.toZohoValue<JobOfferPositionType>(value, ZOHO_VALUE_TO_JOB_OFFER_POSITION_TYPE_MAPPING_STRATEGY);
  }
  static fromRequestedSpecialities(values: JobOfferRequestedSpeciality[]): string {
    return DomainToZohoModelMappingStrategy.toZohoValues<JobOfferRequestedSpeciality>(
      values,
      ZOHO_VALUE_TO_JOB_OFFER_REQUESTED_SPECIALITY_MAPPING_STRATEGY
    );
  }
  static fromTravelingIsTraveling(value: boolean): string {
    return DomainToZohoModelMappingStrategy.toZohoValue<boolean>(value, ZOHO_VALUE_TO_JOB_OFFER_TRAVELING_IS_TRAVELING_MAPPING_STRATEGY);
  }
  static fromTravelingFrequency(value: TravelingFrequency): string {
    return DomainToZohoModelMappingStrategy.toZohoValue<TravelingFrequency>(value, ZOHO_VALUE_TO_TRAVELING_FREQUENCY_MAPPING_STRATEGY);
  }
  static fromAdvantages(value: AdvantagesInterface): string {
    if (isEmpty(value)) {
      return null;
    }

    let result: string = null;
    result = DomainToZohoModelMappingStrategy.appendWithSeparatorIfNotEmpty(
      result,
      DomainToZohoModelMappingStrategy.toZohoValues<AdvantageHealth>(value.health, ZOHO_VALUE_TO_ADVANTAGE_HEALTH_MAPPING_STRATEGY)
    );
    result = DomainToZohoModelMappingStrategy.appendWithSeparatorIfNotEmpty(
      result,
      DomainToZohoModelMappingStrategy.toZohoValues<AdvantageSocial>(value.social, ZOHO_VALUE_TO_ADVANTAGE_SOCIAL_MAPPING_STRATEGY)
    );
    result = DomainToZohoModelMappingStrategy.appendWithSeparatorIfNotEmpty(
      result,
      DomainToZohoModelMappingStrategy.toZohoValues<AdvantageLifeBalance>(value.lifeBalance, ZOHO_VALUE_TO_ADVANTAGE_LIFE_BALANCE_MAPPING_STRATEGY)
    );
    result = DomainToZohoModelMappingStrategy.appendWithSeparatorIfNotEmpty(
      result,
      DomainToZohoModelMappingStrategy.toZohoValues<AdvantageFinancial>(value.financial, ZOHO_VALUE_TO_ADVANTAGE_FINANCIAL_MAPPING_STRATEGY)
    );
    result = DomainToZohoModelMappingStrategy.appendWithSeparatorIfNotEmpty(
      result,
      DomainToZohoModelMappingStrategy.toZohoValues<AdvantageProfessional>(value.professional, ZOHO_VALUE_TO_ADVANTAGE_PROFESSIONAL_MAPPING_STRATEGY)
    );

    return result;
  }
  static fromWantedPersonalities(values: JobOfferWantedPersonality[]): string {
    return DomainToZohoModelMappingStrategy.toZohoValues<JobOfferWantedPersonality>(
      values,
      ZOHO_VALUE_TO_JOB_OFFER_WANTED_PERSONALITY_MAPPING_STRATEGY
    );
  }
  static fromStartDateOfEmploymentAsSoonAsPossible(value: boolean): string {
    return DomainToZohoModelMappingStrategy.toZohoValue<boolean>(
      value,
      ZOHO_VALUE_TO_JOB_OFFER_START_DATE_OF_EMPLOYMENT_AS_SOON_AS_POSSIBLE_MAPPING_STRATEGY
    );
  }

  private static toZohoValues<T>(values: T[], mappingStrategy: { [key: string]: T }): string {
    if (isEmpty(values)) {
      return null;
    }

    return values
      .map((domainValue: T) => DomainToZohoModelMappingStrategy.toZohoValue<T>(domainValue, mappingStrategy))
      .filter((zohoValue: string) => zohoValue !== null)
      .join(ZOHO_VALUE_SEPARATOR);
  }

  private static toZohoValue<T>(domainValue: T, mappingStrategy: { [key: string]: T }): string {
    return findKey(mappingStrategy, (value: T) => domainValue === value) || null;
  }

  private static appendWithSeparatorIfNotEmpty(initialValue: string, valueToAppend: string): string {
    if (valueToAppend) {
      return initialValue ? `${initialValue}${ZOHO_VALUE_SEPARATOR}${valueToAppend}` : valueToAppend;
    } else {
      return initialValue;
    }
  }
}

export interface DomainFieldToZohoModelFieldMappingStrategyInterface {
  target: string;
  map: (value: unknown) => string;
}

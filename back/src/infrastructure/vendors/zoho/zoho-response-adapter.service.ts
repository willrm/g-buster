import { Injectable } from '@nestjs/common';
import { set } from 'lodash';
import { ZohoJsonFieldInterface } from './models/json/zoho-json-field.interface';
import { ZohoModelFieldToDomainFieldMappingStrategyInterface, ZohoModelToDomainMappingStrategy } from './zoho-model-to-domain-mapping-strategy';

@Injectable()
export class ZohoResponseAdapterService {
  map<T>(zohoJsonFields: ZohoJsonFieldInterface[], zohoModelToDomainMappingStrategy: ZohoModelToDomainMappingStrategy): T {
    const result: object = {};

    zohoJsonFields.forEach((zohoJsonField: ZohoJsonFieldInterface) => {
      const mappingStrategyToApply: ZohoModelFieldToDomainFieldMappingStrategyInterface = zohoModelToDomainMappingStrategy[zohoJsonField.val];
      if (mappingStrategyToApply) {
        const value: unknown = mappingStrategyToApply.map(zohoJsonField.content);
        if (value !== null) {
          set(result, mappingStrategyToApply.target, value);
        }
      }
    });

    return (result as unknown) as T;
  }
}

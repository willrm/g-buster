import { ZohoJsonGetRecordsResponseInterface } from '../models/json/zoho-json-get-records-response.interface';
import { ZohoJsonPostRecordsRequestInterface } from '../models/json/zoho-json-post-records-request.interface';
import { ZohoApiModule, ZohoApiService, ZohoFileType } from '../zoho-api.service';
import { ZohoCachedApiService } from '../zoho-cached-api.service';
import { ZohoSearchCondition, ZohoSearchConditionOperator } from '../zoho-search-condition';

describe('infrastructure/vendors/zoho/ZohoCachedApiService', () => {
  let zohoCachedApiService: ZohoCachedApiService;
  let mockZohoApiService: ZohoApiService;
  let mockCacheManager: unknown;

  beforeEach(() => {
    mockZohoApiService = {} as ZohoApiService;
    mockZohoApiService.getRecordById = jest.fn();
    mockZohoApiService.getRecords = jest.fn();
    mockZohoApiService.getSearchRecords = jest.fn();
    mockZohoApiService.addRecords = jest.fn();
    mockZohoApiService.updateRecords = jest.fn();
    mockZohoApiService.changeStatus = jest.fn();
    mockZohoApiService.associateJobOpening = jest.fn();
    mockZohoApiService.uploadFile = jest.fn();

    mockCacheManager = {
      wrap: jest.fn(),
    };

    zohoCachedApiService = new ZohoCachedApiService(mockZohoApiService, mockCacheManager);
  });

  describe('getRecordById()', () => {
    it('should cache using stringified arguments as cache key', async () => {
      // when
      await zohoCachedApiService.getRecordById(ZohoApiModule.JOB_OPENINGS, 'an-id');

      // then
      // @ts-ignore
      expect(mockCacheManager.wrap).toHaveBeenCalledWith('{"zohoApiModule":"JobOpenings","id":"an-id"}', expect.any(Function));
    });

    it('should cache getRecordById result', async () => {
      // given
      await zohoCachedApiService.getRecordById(ZohoApiModule.JOB_OPENINGS, 'an-id');
      // @ts-ignore
      const functionToCache: () => {} = mockCacheManager.wrap.mock.calls[0][1];

      // when
      await functionToCache();

      // then
      expect(mockZohoApiService.getRecordById).toHaveBeenCalledWith(ZohoApiModule.JOB_OPENINGS, 'an-id');
    });

    it('should return cached result', async () => {
      // given
      const expected: ZohoJsonGetRecordsResponseInterface = { response: { result: {} } };
      // @ts-ignore
      mockCacheManager.wrap.mockReturnValue(Promise.resolve(expected));

      // when
      const result: ZohoJsonGetRecordsResponseInterface = await zohoCachedApiService.getRecordById(ZohoApiModule.JOB_OPENINGS, 'an-id');

      // then
      expect(result).toBe(expected);
    });
  });

  describe('getRecords()', () => {
    it('should cache using stringified argument as cache key', async () => {
      // when
      await zohoCachedApiService.getRecords(ZohoApiModule.JOB_OPENINGS);

      // then
      // @ts-ignore
      expect(mockCacheManager.wrap).toHaveBeenCalledWith('{"zohoApiModule":"JobOpenings"}', expect.any(Function));
    });

    it('should cache getRecords result', async () => {
      // given
      await zohoCachedApiService.getRecords(ZohoApiModule.JOB_OPENINGS);
      // @ts-ignore
      const functionToCache: () => {} = mockCacheManager.wrap.mock.calls[0][1];

      // when
      await functionToCache();

      // then
      expect(mockZohoApiService.getRecords).toHaveBeenCalledWith(ZohoApiModule.JOB_OPENINGS);
    });

    it('should return cached result', async () => {
      // given
      const expected: ZohoJsonGetRecordsResponseInterface = { response: { result: {} } };
      // @ts-ignore
      mockCacheManager.wrap.mockReturnValue(Promise.resolve(expected));

      // when
      const result: ZohoJsonGetRecordsResponseInterface = await zohoCachedApiService.getRecords(ZohoApiModule.JOB_OPENINGS);

      // then
      expect(result).toBe(expected);
    });
  });

  describe('getSearchRecords()', () => {
    let zohoSearchCondition: ZohoSearchCondition;
    beforeEach(() => {
      zohoSearchCondition = {
        value: 'value',
        field: 'field',
        operator: ZohoSearchConditionOperator.IS,
      };
    });

    it('should cache using stringified arguments as cache key', async () => {
      // when
      await zohoCachedApiService.getSearchRecords(ZohoApiModule.JOB_OPENINGS, zohoSearchCondition);

      // then
      // @ts-ignore
      expect(mockCacheManager.wrap).toHaveBeenCalledWith(
        '{"zohoApiModule":"JobOpenings","zohoSearchCondition":{"value":"value","field":"field","operator":"="}}',
        expect.any(Function)
      );
    });

    it('should cache getSearchRecords result', async () => {
      // given
      await zohoCachedApiService.getSearchRecords(ZohoApiModule.JOB_OPENINGS, zohoSearchCondition);
      // @ts-ignore
      const functionToCache: () => {} = mockCacheManager.wrap.mock.calls[0][1];

      // when
      await functionToCache();

      // then
      expect(mockZohoApiService.getSearchRecords).toHaveBeenCalledWith(ZohoApiModule.JOB_OPENINGS, zohoSearchCondition);
    });

    it('should return cached result', async () => {
      // given
      const expected: ZohoJsonGetRecordsResponseInterface = { response: { result: {} } };
      // @ts-ignore
      mockCacheManager.wrap.mockReturnValue(Promise.resolve(expected));

      // when
      const result: ZohoJsonGetRecordsResponseInterface = await zohoCachedApiService.getSearchRecords(
        ZohoApiModule.JOB_OPENINGS,
        zohoSearchCondition
      );

      // then
      expect(result).toBe(expected);
    });
  });

  describe('addRecords()', () => {
    it('should call addRecords without cache', async () => {
      // given
      const zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface = { key: { row: [] } };

      // when
      await zohoCachedApiService.addRecords(ZohoApiModule.JOB_OPENINGS, zohoJsonPostRecordsRequest);

      // then
      expect(mockZohoApiService.addRecords).toHaveBeenCalledWith(ZohoApiModule.JOB_OPENINGS, zohoJsonPostRecordsRequest);
      // @ts-ignore
      expect(mockCacheManager.wrap).not.toHaveBeenCalled();
    });

    it('should return addRecords result', async () => {
      // given
      const zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface = { key: { row: [] } };
      const expected: string = 'expected-id';
      (mockZohoApiService.addRecords as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: string = await zohoCachedApiService.addRecords(ZohoApiModule.JOB_OPENINGS, zohoJsonPostRecordsRequest);

      // then
      expect(result).toBe(expected);
    });
  });

  describe('updateRecords()', () => {
    it('should call updateRecords without cache', async () => {
      // given
      const zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface = { key: { row: [] } };

      // when
      await zohoCachedApiService.updateRecords(ZohoApiModule.JOB_OPENINGS, 'id', zohoJsonPostRecordsRequest);

      // then
      expect(mockZohoApiService.updateRecords).toHaveBeenCalledWith(ZohoApiModule.JOB_OPENINGS, 'id', zohoJsonPostRecordsRequest);
      // @ts-ignore
      expect(mockCacheManager.wrap).not.toHaveBeenCalled();
    });

    it('should return updateRecords result', async () => {
      // given
      const zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface = { key: { row: [] } };
      const expected: string = 'expected-id';
      (mockZohoApiService.updateRecords as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: string = await zohoCachedApiService.updateRecords(ZohoApiModule.JOB_OPENINGS, 'id', zohoJsonPostRecordsRequest);

      // then
      expect(result).toBe(expected);
    });
  });

  describe('changeStatus()', () => {
    it('should call changeStatus without cache', async () => {
      // when
      await zohoCachedApiService.changeStatus(ZohoApiModule.JOB_OPENINGS, 'id', 'status');

      // then
      expect(mockZohoApiService.changeStatus).toHaveBeenCalledWith(ZohoApiModule.JOB_OPENINGS, 'id', 'status');
      // @ts-ignore
      expect(mockCacheManager.wrap).not.toHaveBeenCalled();
    });

    it('should return changeStatus result', async () => {
      // given
      const expected: string = 'expected-id';
      (mockZohoApiService.changeStatus as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: string = await zohoCachedApiService.changeStatus(ZohoApiModule.JOB_OPENINGS, 'id', 'status');

      // then
      expect(result).toBe(expected);
    });
  });

  describe('associateJobOpening()', () => {
    it('should call associateJobOpening without cache', async () => {
      // when
      await zohoCachedApiService.associateJobOpening(ZohoApiModule.CANDIDATES, 'candidateId', 'jobId');

      // then
      expect(mockZohoApiService.associateJobOpening).toHaveBeenCalledWith(ZohoApiModule.CANDIDATES, 'candidateId', 'jobId');
      // @ts-ignore
      expect(mockCacheManager.wrap).not.toHaveBeenCalled();
    });

    it('should return associateJobOpening result', async () => {
      // given
      const expected: string = 'expected-id';
      (mockZohoApiService.associateJobOpening as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: string = await zohoCachedApiService.associateJobOpening(ZohoApiModule.CANDIDATES, 'candidateId', 'jobId');

      // then
      expect(result).toBe(expected);
    });
  });

  describe('uploadFile()', () => {
    it('should call uploadFile without cache', async () => {
      // when
      await zohoCachedApiService.uploadFile(
        ZohoApiModule.CANDIDATES,
        'record-id',
        ZohoFileType.COVER_LETTER,
        'a-file-as-base64',
        'filename.extension',
        'mime/type'
      );

      // then
      expect(mockZohoApiService.uploadFile).toHaveBeenCalledWith(
        ZohoApiModule.CANDIDATES,
        'record-id',
        ZohoFileType.COVER_LETTER,
        'a-file-as-base64',
        'filename.extension',
        'mime/type'
      );
      // @ts-ignore
      expect(mockCacheManager.wrap).not.toHaveBeenCalled();
    });

    it('should return uploadFile result', async () => {
      // given
      const expected: string = 'expected-id';
      (mockZohoApiService.uploadFile as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: string = await zohoCachedApiService.uploadFile(
        ZohoApiModule.CANDIDATES,
        'record-id',
        ZohoFileType.COVER_LETTER,
        'a-file-as-base64',
        'filename.extension',
        'mime/type'
      );

      // then
      expect(result).toBe(expected);
    });
  });
});

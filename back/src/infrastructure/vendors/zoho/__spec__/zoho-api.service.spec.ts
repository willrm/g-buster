import { HttpService } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { of } from 'rxjs';
import { EnvironmentConfigService } from '../../../config/environment-config/environment-config.service';
import { ZohoJsonGetRecordsResponseInterface } from '../models/json/zoho-json-get-records-response.interface';
import { ZohoJsonPostRecordsRequestInterface } from '../models/json/zoho-json-post-records-request.interface';
import { ZohoJsonRowInterface } from '../models/json/zoho-json-row.interface';
import { ZohoApiModule, ZohoApiService, ZohoFileType } from '../zoho-api.service';
import { ZohoJsonToXmlParser } from '../zoho-json-to-xml-parser';
import { ZohoSearchCondition, ZohoSearchConditionOperator } from '../zoho-search-condition';
import { ZohoXmlToJsonParser } from '../zoho-xml-to-json-parser';
import { ZohoError } from '../zoho.error';

describe('infrastructure/vendors/zoho/ZohoApiService', () => {
  let zohoApiService: ZohoApiService;
  let mockEnvironmentConfigService: EnvironmentConfigService;
  let mockHttpService: HttpService;
  let mockZohoXmlToJsonParser: ZohoXmlToJsonParser;
  let mockZohoJsonToXmlParser: ZohoJsonToXmlParser;

  beforeEach(() => {
    mockEnvironmentConfigService = {} as EnvironmentConfigService;
    mockEnvironmentConfigService.get = jest.fn();

    mockHttpService = {} as HttpService;
    mockHttpService.get = jest.fn();
    mockHttpService.post = jest.fn();

    mockZohoXmlToJsonParser = {} as ZohoXmlToJsonParser;
    mockZohoXmlToJsonParser.toJson = jest.fn();

    mockZohoJsonToXmlParser = {} as ZohoJsonToXmlParser;
    mockZohoJsonToXmlParser.toXml = jest.fn();

    zohoApiService = new ZohoApiService(mockEnvironmentConfigService, mockHttpService, mockZohoXmlToJsonParser, mockZohoJsonToXmlParser);
  });

  describe('getRecordById()', () => {
    beforeEach(() => {
      (mockHttpService.get as jest.Mock).mockReturnValue(of({} as AxiosResponse));
    });

    it('should call getRecordById zoho api using given module with url and access token from configuration', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;
      const id: string = 'aJobOfferId';

      const expectedZohoApiUrl: string = 'https://api';
      const expectedZohoApiAccessToken: string = 'super-secret-access-token';
      (mockEnvironmentConfigService.get as jest.Mock).mockReturnValueOnce(expectedZohoApiAccessToken).mockReturnValueOnce(expectedZohoApiUrl);

      // when
      await zohoApiService.getRecordById(zohoApiModule, id);

      // then
      expect(mockHttpService.get).toHaveBeenCalledWith(
        'https://api/JobOpenings/getRecordById?authtoken=super-secret-access-token&scope=recruitapi&version=2&id=aJobOfferId'
      );
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_URL');
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_ACCESS_TOKEN');
    });

    it('should map http response xml data to json', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;
      const id: string = 'aJobOfferId';
      const xmlData: string = '<some-xml/>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.get as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      await zohoApiService.getRecordById(zohoApiModule, id);

      // then
      expect(mockZohoXmlToJsonParser.toJson).toHaveBeenCalledWith(zohoApiModule, xmlData);
    });

    it('should return mapped data', async () => {
      // given
      const id: string = 'aJobOfferId';
      const expected: ZohoJsonGetRecordsResponseInterface = {
        response: {
          result: {
            JobOpenings: {
              row: [] as ZohoJsonRowInterface[],
            },
          },
        },
      } as ZohoJsonGetRecordsResponseInterface;
      (mockZohoXmlToJsonParser.toJson as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: ZohoJsonGetRecordsResponseInterface = await zohoApiService.getRecordById(ZohoApiModule.JOB_OPENINGS, id);

      // then
      expect(result).toBe(expected);
    });
  });

  describe('getRecords()', () => {
    beforeEach(() => {
      (mockHttpService.get as jest.Mock).mockReturnValue(of({} as AxiosResponse));
    });

    it('should call getRecords zoho api using given module with url and access token from configuration', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;

      const expectedZohoApiUrl: string = 'https://api';
      const expectedZohoApiAccessToken: string = 'super-secret-access-token';
      (mockEnvironmentConfigService.get as jest.Mock).mockReturnValueOnce(expectedZohoApiAccessToken).mockReturnValueOnce(expectedZohoApiUrl);

      // when
      await zohoApiService.getRecords(zohoApiModule);

      // then
      expect(mockHttpService.get).toHaveBeenCalledWith(
        'https://api/JobOpenings/getRecords?authtoken=super-secret-access-token&scope=recruitapi&version=2'
      );
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_URL');
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_ACCESS_TOKEN');
    });

    it('should map http response xml data to json', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;
      const xmlData: string = '<some-xml/>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.get as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      await zohoApiService.getRecords(zohoApiModule);

      // then
      expect(mockZohoXmlToJsonParser.toJson).toHaveBeenCalledWith(zohoApiModule, xmlData);
    });

    it('should return mapped data', async () => {
      // given
      const expected: ZohoJsonGetRecordsResponseInterface = {
        response: {
          result: {
            JobOpenings: {
              row: [] as ZohoJsonRowInterface[],
            },
          },
        },
      } as ZohoJsonGetRecordsResponseInterface;
      (mockZohoXmlToJsonParser.toJson as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: ZohoJsonGetRecordsResponseInterface = await zohoApiService.getRecords(ZohoApiModule.JOB_OPENINGS);

      // then
      expect(result).toBe(expected);
    });
  });

  describe('getSearchRecords()', () => {
    let zohoSearchCondition: ZohoSearchCondition;

    beforeEach(() => {
      zohoSearchCondition = {} as ZohoSearchCondition;

      (mockHttpService.get as jest.Mock).mockReturnValue(of({} as AxiosResponse));
    });

    it('should call getSearchRecords zoho api using given module with url and access token from configuration', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;

      const expectedZohoApiUrl: string = 'https://api';
      const expectedZohoApiAccessToken: string = 'super-secret-access-token';
      (mockEnvironmentConfigService.get as jest.Mock).mockReturnValueOnce(expectedZohoApiAccessToken).mockReturnValueOnce(expectedZohoApiUrl);

      // when
      await zohoApiService.getSearchRecords(zohoApiModule, zohoSearchCondition);

      // then
      expect((mockHttpService.get as jest.Mock).mock.calls[0][0]).toContain(
        'https://api/JobOpenings/getSearchRecords?authtoken=super-secret-access-token&scope=recruitapi&version=2&selectColumns=All'
      );
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_URL');
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_ACCESS_TOKEN');
    });

    it('should call getSearchRecords zoho api with search condition as query string', async () => {
      // given
      zohoSearchCondition = {
        field: 'a-zoho-field',
        operator: ZohoSearchConditionOperator.IS,
        value: 'a zoho value',
      };

      // when
      await zohoApiService.getSearchRecords(ZohoApiModule.JOB_OPENINGS, zohoSearchCondition);

      // then
      expect((mockHttpService.get as jest.Mock).mock.calls[0][0]).toContain('&searchCondition=%28a-zoho-field%7C%3D%7Ca%20zoho%20value%29');
    });

    it('should map http response xml data to json', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;
      const xmlData: string = '<some-xml/>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.get as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      await zohoApiService.getSearchRecords(zohoApiModule, zohoSearchCondition);

      // then
      expect(mockZohoXmlToJsonParser.toJson).toHaveBeenCalledWith(zohoApiModule, xmlData);
    });

    it('should return mapped data', async () => {
      // given
      const expected: ZohoJsonGetRecordsResponseInterface = {
        response: {
          result: {
            JobOpenings: {
              row: [] as ZohoJsonRowInterface[],
            },
          },
        },
      } as ZohoJsonGetRecordsResponseInterface;
      (mockZohoXmlToJsonParser.toJson as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: ZohoJsonGetRecordsResponseInterface = await zohoApiService.getSearchRecords(ZohoApiModule.JOB_OPENINGS, zohoSearchCondition);

      // then
      expect(result).toBe(expected);
    });
  });

  describe('addRecords()', () => {
    beforeEach(() => {
      (mockHttpService.post as jest.Mock).mockReturnValue(
        of({
          data: '<FL val="Id">123456789</FL>',
        } as AxiosResponse)
      );
    });

    it('should call addRecords zoho api using given module with url and access token from configuration', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;

      const expectedZohoApiUrl: string = 'https://api';
      const expectedZohoApiAccessToken: string = 'super-secret-access-token';
      (mockEnvironmentConfigService.get as jest.Mock).mockReturnValueOnce(expectedZohoApiAccessToken).mockReturnValueOnce(expectedZohoApiUrl);

      // when
      await zohoApiService.addRecords(zohoApiModule, {} as ZohoJsonPostRecordsRequestInterface);

      // then
      expect((mockHttpService.post as jest.Mock).mock.calls[0][0]).toBe(
        'https://api/JobOpenings/addRecords?authtoken=super-secret-access-token&scope=recruitapi&version=2'
      );
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_URL');
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_ACCESS_TOKEN');
    });

    it('should post request mapped to xml data in form data', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;
      const zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface = { JobOpenings: { row: [] } } as ZohoJsonPostRecordsRequestInterface;

      const xmlData: string = '<some-xml/>';
      (mockZohoJsonToXmlParser.toXml as jest.Mock).mockReturnValue(Promise.resolve(xmlData));

      // when
      await zohoApiService.addRecords(zohoApiModule, zohoJsonPostRecordsRequest);

      // then
      expect(mockZohoJsonToXmlParser.toXml).toHaveBeenCalledWith(zohoApiModule, zohoJsonPostRecordsRequest);
      expect(mockHttpService.post).toHaveBeenCalledWith(expect.any(String), 'xmlData=%3Csome-xml%2F%3E');
    });

    it('should return created id from response data', async () => {
      // given
      const xmlData: string =
        '<?xml version="1.0" encoding="UTF-8" ?>\n' +
        '<response uri="/recruit/private/xml/SomeModule/addRecords">\n' +
        '    <result>\n' +
        '        <message>OK</message>\n' +
        '        <recorddetail>\n' +
        '            <FL val="Id">565241000000317013</FL>\n' +
        '            <FL val="Created Time">2019-11-11 14:29:35</FL>\n' +
        '            <FL val="Created By">\n' +
        '                <![CDATA[XYZ]]>\n' +
        '            </FL>\n' +
        '        </recorddetail>\n' +
        '    </result>\n' +
        '</response>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.post as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      const result: string = await zohoApiService.addRecords(ZohoApiModule.JOB_OPENINGS, {} as ZohoJsonPostRecordsRequestInterface);

      // then
      expect(result).toBe('565241000000317013');
    });

    it('should throw an exception when no id in response data', async () => {
      // given
      const xmlData: string =
        '<?xml version="1.0" encoding="UTF-8" ?>\n' +
        '<response uri="/recruit/private/xml/SomeModule/addRecords">\n' +
        '    <error>\n' +
        '        <code>null</code>\n' +
        '        <message>null</message>\n' +
        '    </error>\n' +
        '</response>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.post as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      const result: Promise<string> = zohoApiService.addRecords(ZohoApiModule.JOB_OPENINGS, {} as ZohoJsonPostRecordsRequestInterface);

      // then
      await expect(result).rejects.toThrow(
        new ZohoError(
          'API call failed. No Id found in response data.\n' +
            'Response body: <?xml version="1.0" encoding="UTF-8" ?>\n' +
            '<response uri="/recruit/private/xml/SomeModule/addRecords">\n' +
            '    <error>\n' +
            '        <code>null</code>\n' +
            '        <message>null</message>\n' +
            '    </error>\n' +
            '</response>'
        )
      );
    });
  });

  describe('updateRecords()', () => {
    beforeEach(() => {
      (mockHttpService.post as jest.Mock).mockReturnValue(
        of({
          data: '<FL val="Id">123456789</FL>',
        } as AxiosResponse)
      );
    });

    it('should call updateRecords zoho api using given module and id with url and access token from configuration', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;
      const id: string = '123456789';

      const expectedZohoApiUrl: string = 'https://api';
      const expectedZohoApiAccessToken: string = 'super-secret-access-token';
      (mockEnvironmentConfigService.get as jest.Mock).mockReturnValueOnce(expectedZohoApiAccessToken).mockReturnValueOnce(expectedZohoApiUrl);

      // when
      await zohoApiService.updateRecords(zohoApiModule, id, {} as ZohoJsonPostRecordsRequestInterface);

      // then
      expect((mockHttpService.post as jest.Mock).mock.calls[0][0]).toBe(
        'https://api/JobOpenings/updateRecords?authtoken=super-secret-access-token&scope=recruitapi&version=2&id=123456789'
      );
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_URL');
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_ACCESS_TOKEN');
    });

    it('should post request mapped to xml data in form data', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;
      const zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface = { JobOpenings: { row: [] } } as ZohoJsonPostRecordsRequestInterface;

      const xmlData: string = '<some-xml/>';
      (mockZohoJsonToXmlParser.toXml as jest.Mock).mockReturnValue(Promise.resolve(xmlData));

      // when
      await zohoApiService.updateRecords(zohoApiModule, '123456789', zohoJsonPostRecordsRequest);

      // then
      expect(mockZohoJsonToXmlParser.toXml).toHaveBeenCalledWith(zohoApiModule, zohoJsonPostRecordsRequest);
      expect(mockHttpService.post).toHaveBeenCalledWith(expect.any(String), 'xmlData=%3Csome-xml%2F%3E');
    });

    it('should return updated id from response data', async () => {
      // given
      const xmlData: string =
        '<?xml version="1.0" encoding="UTF-8" ?>\n' +
        '<response uri="/recruit/private/xml/JobOpenings/updateRecords">\n' +
        '    <result>\n' +
        '        <message>OK</message>\n' +
        '        <recorddetail>\n' +
        '            <FL val="Id">123456789</FL>\n' +
        '            <FL val="Created Time">2019-11-19 13:57:01</FL>\n' +
        '            <FL val="Modified Time">2019-12-02 14:27:56</FL>\n' +
        '            <FL val="Created By">\n' +
        '                <![CDATA[G-Buster]]>\n' +
        '            </FL>\n' +
        '            <FL val="Modified By">\n' +
        '                <![CDATA[G-Buster]]>\n' +
        '            </FL>\n' +
        '        </recorddetail>\n' +
        '    </result>\n' +
        '</response>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.post as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      const result: string = await zohoApiService.updateRecords(ZohoApiModule.JOB_OPENINGS, '123456789', {} as ZohoJsonPostRecordsRequestInterface);

      // then
      expect(result).toBe('123456789');
    });

    it('should throw an exception when no id in response data', async () => {
      // given
      const xmlData: string =
        '<?xml version="1.0" encoding="UTF-8" ?>\n' +
        '<response uri="/recruit/private/xml/SomeModule/updateRecords">\n' +
        '    <error>\n' +
        '        <code>null</code>\n' +
        '        <message>null</message>\n' +
        '    </error>\n' +
        '</response>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.post as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      const result: Promise<string> = zohoApiService.updateRecords(
        ZohoApiModule.JOB_OPENINGS,
        '123456789',
        {} as ZohoJsonPostRecordsRequestInterface
      );

      // then
      await expect(result).rejects.toThrow(
        new ZohoError(
          'API call failed. No Id found in response data.\n' +
            'Response body: <?xml version="1.0" encoding="UTF-8" ?>\n' +
            '<response uri="/recruit/private/xml/SomeModule/updateRecords">\n' +
            '    <error>\n' +
            '        <code>null</code>\n' +
            '        <message>null</message>\n' +
            '    </error>\n' +
            '</response>'
        )
      );
    });
  });

  describe('changeStatus()', () => {
    beforeEach(() => {
      (mockHttpService.post as jest.Mock).mockReturnValue(
        of({
          data: '<FL val="Id">123456789</FL>',
        } as AxiosResponse)
      );
    });

    it('should call changeStatus zoho api using given module and id and status with url and access token from configuration', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;
      const id: string = '123456789';
      const status: string = 'En attente d’approbation';

      const expectedZohoApiUrl: string = 'https://api';
      const expectedZohoApiAccessToken: string = 'super-secret-access-token';
      (mockEnvironmentConfigService.get as jest.Mock).mockReturnValueOnce(expectedZohoApiAccessToken).mockReturnValueOnce(expectedZohoApiUrl);

      // when
      await zohoApiService.changeStatus(zohoApiModule, id, status);

      // then
      expect(mockHttpService.post).toHaveBeenCalledWith(
        'https://api/JobOpenings/changeStatus?authtoken=super-secret-access-token&scope=recruitapi&version=2&id=123456789&status=En%20attente%20d%E2%80%99approbation',
        undefined
      );
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_URL');
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_ACCESS_TOKEN');
    });

    it('should return updated id from response data', async () => {
      // given
      const xmlData: string =
        '<?xml version="1.0" encoding="UTF-8" ?>\n' +
        '<response uri="/recruit/private/xml/SomeModule/changeStatus">\n' +
        '    <result>\n' +
        '        <message>OK</message>\n' +
        '        <recorddetail>\n' +
        '            <FL val="id">123456789</FL>\n' +
        '            <FL val="status">newStatus</FL>\n' +
        '        </recorddetail>\n' +
        '    </result>\n' +
        '</response>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.post as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      const result: string = await zohoApiService.changeStatus(ZohoApiModule.JOB_OPENINGS, '123456789', 'newStatus');

      // then
      expect(result).toBe('123456789');
    });

    it('should throw an exception when no id in response data', async () => {
      // given
      const xmlData: string =
        '<?xml version="1.0" encoding="UTF-8" ?>\n' +
        '<response uri="/recruit/private/xml/SomeModule/changeStatus">\n' +
        '    <error>\n' +
        '        <code>null</code>\n' +
        '        <message>null</message>\n' +
        '    </error>\n' +
        '</response>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.post as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      const result: Promise<string> = zohoApiService.changeStatus(ZohoApiModule.JOB_OPENINGS, '123456789', 'newStatus');

      // then
      await expect(result).rejects.toThrow(
        new ZohoError(
          'API call failed. No Id found in response data.\n' +
            'Response body: <?xml version="1.0" encoding="UTF-8" ?>\n' +
            '<response uri="/recruit/private/xml/SomeModule/changeStatus">\n' +
            '    <error>\n' +
            '        <code>null</code>\n' +
            '        <message>null</message>\n' +
            '    </error>\n' +
            '</response>'
        )
      );
    });
  });

  describe('associateJobOpening()', () => {
    beforeEach(() => {
      (mockHttpService.post as jest.Mock).mockReturnValue(
        of({
          data: '<FL val="candidateIds">123456789</FL>',
        } as AxiosResponse)
      );
    });

    it('should call associateJobOpening zoho api using given module and candidateId and jobId with url and access token from configuration', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.CANDIDATES;
      const candidateId: string = '123456789';
      const jobId: string = '987654321';

      const expectedZohoApiUrl: string = 'https://api';
      const expectedZohoApiAccessToken: string = 'super-secret-access-token';
      (mockEnvironmentConfigService.get as jest.Mock).mockReturnValueOnce(expectedZohoApiAccessToken).mockReturnValueOnce(expectedZohoApiUrl);

      // when
      await zohoApiService.associateJobOpening(zohoApiModule, candidateId, jobId);

      // then
      expect(mockHttpService.post).toHaveBeenCalledWith(
        'https://api/Candidates/associateJobOpening?authtoken=super-secret-access-token&scope=recruitapi&version=2&candidateIds=123456789&jobIds=987654321',
        undefined
      );
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_URL');
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_ACCESS_TOKEN');
    });

    it('should return updated id from response data', async () => {
      // given
      const xmlData: string =
        '<?xml version="1.0" encoding="UTF-8" ?>\n' +
        '<response uri="/recruit/private/xml/SomeModule/associateJobOpening">\n' +
        '    <result>\n' +
        '        <message>OK</message>\n' +
        '        <recorddetail>\n' +
        '            <FL val="candidateIds">123456789</FL>\n' +
        '            <FL val="jobIds">987654321</FL>\n' +
        '        </recorddetail>\n' +
        '    </result>\n' +
        '</response>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.post as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      const result: string = await zohoApiService.associateJobOpening(ZohoApiModule.CANDIDATES, '123456789', '987654321');

      // then
      expect(result).toBe('123456789');
    });

    it('should throw an exception when no id in response data', async () => {
      // given
      const xmlData: string =
        '<?xml version="1.0" encoding="UTF-8" ?>\n' +
        '<response uri="/recruit/private/xml/SomeModule/associateJobOpening">\n' +
        '    <error>\n' +
        '        <code>null</code>\n' +
        '        <message>null</message>\n' +
        '    </error>\n' +
        '</response>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.post as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      const result: Promise<string> = zohoApiService.associateJobOpening(ZohoApiModule.CANDIDATES, '123456789', '987654321');

      // then
      await expect(result).rejects.toThrow(
        new ZohoError(
          'API call failed. No candidateIds found in response data.\n' +
            'Response body: <?xml version="1.0" encoding="UTF-8" ?>\n' +
            '<response uri="/recruit/private/xml/SomeModule/associateJobOpening">\n' +
            '    <error>\n' +
            '        <code>null</code>\n' +
            '        <message>null</message>\n' +
            '    </error>\n' +
            '</response>'
        )
      );
    });
  });

  describe('uploadFile()', () => {
    beforeEach(() => {
      (mockHttpService.post as jest.Mock).mockReturnValue(
        of({
          data: '<FL val="Id">123456789</FL>',
        } as AxiosResponse)
      );
    });

    it('should call uploadFile zoho api using given module and file information with url and access token from configuration', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.CANDIDATES;

      const expectedZohoApiUrl: string = 'https://api';
      const expectedZohoApiAccessToken: string = 'super-secret-access-token';
      (mockEnvironmentConfigService.get as jest.Mock).mockReturnValueOnce(expectedZohoApiAccessToken).mockReturnValueOnce(expectedZohoApiUrl);

      // when
      await zohoApiService.uploadFile(zohoApiModule, 'record-id', ZohoFileType.RESUME, 'YS1maWxlLWFzLWJhc2U2NA==', 'filename.extension', 'mime/type');

      // then
      expect((mockHttpService.post as jest.Mock).mock.calls[0][0]).toBe(
        'https://api/Candidates/uploadFile?authtoken=super-secret-access-token&scope=recruitapi&version=2'
      );
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_URL');
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('ZOHO_API_ACCESS_TOKEN');
    });

    it('should post id, type and file to uploadFile zoho api using a FormData', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.CANDIDATES;
      const id: string = 'record-id';
      const zohoFileType: ZohoFileType = ZohoFileType.RESUME;
      const fileAsBase64: string = 'YS1maWxlLWFzLWJhc2U2NA==';
      const filename: string = 'filename.extension';
      const fileMimeType: string = 'mime/type';

      // when
      await zohoApiService.uploadFile(zohoApiModule, id, zohoFileType, fileAsBase64, filename, fileMimeType);

      // then
      const stringifiedFormData: string = JSON.stringify((mockHttpService.post as jest.Mock).mock.calls[0][1]).replace(/(\\r\\n|\\n|\\r|\\)/gm, '');
      expect(stringifiedFormData).toContain('Content-Disposition: form-data; name="id"","record-id"');
      expect(stringifiedFormData).toContain('Content-Disposition: form-data; name="type"","Resume"');
      expect(stringifiedFormData).toContain(
        'Content-Disposition: form-data; name="content"; filename="filename.extension"' +
          'Content-Type: mime/type",' +
          '{"type":"Buffer","data":[97,45,102,105,108,101,45,97,115,45,98,97,115,101,54,52]}'
      );
      expect((mockHttpService.post as jest.Mock).mock.calls[0][2].headers['content-type']).toContain(
        'multipart/form-data; boundary=--------------------------'
      );
    });

    it('should return uploaded file id from response data', async () => {
      // given
      const xmlData: string =
        '<?xml version="1.0" encoding="UTF-8" ?>\n' +
        '<response uri="/recruit/private/xml/SomeModule/uploadFile">\n' +
        '    <result>\n' +
        '        <message>OK</message>\n' +
        '        <recorddetail>\n' +
        '            <FL val="Id">123456789</FL>\n' +
        '        </recorddetail>\n' +
        '    </result>\n' +
        '</response>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.post as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      const result: string = await zohoApiService.uploadFile(
        ZohoApiModule.CANDIDATES,
        'record-id',
        ZohoFileType.RESUME,
        'YS1maWxlLWFzLWJhc2U2NA==',
        'filename.extension',
        'mime/type'
      );

      // then
      expect(result).toBe('123456789');
    });

    it('should throw an exception when no id in response data', async () => {
      // given
      const xmlData: string =
        '<?xml version="1.0" encoding="UTF-8" ?>\n' +
        '<response uri="/recruit/private/xml/SomeModule/uploadFile">\n' +
        '    <error>\n' +
        '        <code>null</code>\n' +
        '        <message>null</message>\n' +
        '    </error>\n' +
        '</response>';

      const axiosResponse: AxiosResponse = {
        data: xmlData,
      } as AxiosResponse;
      (mockHttpService.post as jest.Mock).mockReturnValue(of(axiosResponse));

      // when
      const result: Promise<string> = zohoApiService.uploadFile(
        ZohoApiModule.CANDIDATES,
        'record-id',
        ZohoFileType.RESUME,
        'YS1maWxlLWFzLWJhc2U2NA==',
        'filename.extension',
        'mime/type'
      );

      // then
      await expect(result).rejects.toThrow(
        new ZohoError(
          'API call failed. No Id found in response data.\n' +
            'Response body: <?xml version="1.0" encoding="UTF-8" ?>\n' +
            '<response uri="/recruit/private/xml/SomeModule/uploadFile">\n' +
            '    <error>\n' +
            '        <code>null</code>\n' +
            '        <message>null</message>\n' +
            '    </error>\n' +
            '</response>'
        )
      );
    });
  });
});

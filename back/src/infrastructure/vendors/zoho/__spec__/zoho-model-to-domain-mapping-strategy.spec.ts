import {
  AdvantageFinancial,
  AdvantageHealth,
  AdvantageLifeBalance,
  AdvantageProfessional,
  AdvantageSocial,
} from '../../../../domain/advantages/advantages-enums';
import { AdvantagesInterface } from '../../../../domain/advantages/advantages.interface';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from '../../../../domain/job-offer/job-offer-enums';
import { TravelingFrequency } from '../../../../domain/traveling/traveling-enums';
import { ZohoModelToDomainMappingStrategy } from '../zoho-model-to-domain-mapping-strategy';

describe('infrastructure/vendors/zoho/ZohoModelToDomainMappingStrategy', () => {
  describe('noOp()', () => {
    it('should return content unchanged', () => {
      // given
      const content: string = 'a content';

      // when
      const result: string = ZohoModelToDomainMappingStrategy.noOp(content);

      // then
      expect(result).toBe('a content');
    });
  });

  describe('toNumber()', () => {
    it('should return number parsed from content', () => {
      // given
      const content: string = '123456789';

      // when
      const result: number = ZohoModelToDomainMappingStrategy.toNumber(content);

      // then
      expect(result).toBe(123456789);
    });
    it('should return number parsed from content with spaces', () => {
      // given
      const content: string = '123 456 789';

      // when
      const result: number = ZohoModelToDomainMappingStrategy.toNumber(content);

      // then
      expect(result).toBe(123456789);
    });
    it('should return null when content is empty', () => {
      // given
      const content: string = '';

      // when
      const result: number = ZohoModelToDomainMappingStrategy.toNumber(content);

      // then
      expect(result).toBeNull();
    });
    it('should return null when content is undefined', () => {
      // given
      const content: string = undefined;

      // when
      const result: number = ZohoModelToDomainMappingStrategy.toNumber(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toBoolean()', () => {
    it('should return true when content is the string true', () => {
      // given
      const content: string = 'true';

      // when
      const result: boolean = ZohoModelToDomainMappingStrategy.toBoolean(content);

      // then
      expect(result).toBe(true);
    });
    it('should return false when content is the string false', () => {
      // given
      const content: string = 'false';

      // when
      const result: boolean = ZohoModelToDomainMappingStrategy.toBoolean(content);

      // then
      expect(result).toBe(false);
    });
    it('should return false when content is an unknown string', () => {
      // given
      const content: string = 'unknown';

      // when
      const result: boolean = ZohoModelToDomainMappingStrategy.toBoolean(content);

      // then
      expect(result).toBe(false);
    });
  });

  describe('toDate()', () => {
    it('should return date parsed using content', () => {
      // given
      const content: string = '2019-03-28T16:35:57';

      // when
      const result: Date = ZohoModelToDomainMappingStrategy.toDate(content);

      // then
      expect(result).toStrictEqual(new Date('2019-03-28T16:35:57'));
    });
    it('should return date as noon when content is an ISO8601 date without time', () => {
      // given
      const content: string = '2019-03-28';

      // when
      const result: Date = ZohoModelToDomainMappingStrategy.toDate(content);

      // then
      expect(result).toStrictEqual(new Date('2019-03-28T12:00:00'));
    });
  });

  describe('toEmail()', () => {
    it('should return string without unique timestamped suffix', () => {
      // given
      const value: string = 'test@example.org+REMOVE-THIS-TIMESTAMPED-SUFFIX-TO-HAVE-REAL-EMAIL-1580223063664';

      // when
      const result: string = ZohoModelToDomainMappingStrategy.toEmail(value);

      // then
      expect(result).toBe('test@example.org');
    });
  });

  describe('toStatus()', () => {
    it('should return DRAFT status when content is Rempli', () => {
      // given
      const content: string = 'Rempli';

      // when
      const result: JobOfferStatus = ZohoModelToDomainMappingStrategy.toStatus(content);

      // then
      expect(result).toBe(JobOfferStatus.STATUS_DRAFT);
    });
    it('should return PENDING_VALIDATION status when content is En attente d’approbation', () => {
      // given
      const content: string = 'En attente d’approbation';

      // when
      const result: JobOfferStatus = ZohoModelToDomainMappingStrategy.toStatus(content);

      // then
      expect(result).toBe(JobOfferStatus.STATUS_PENDING_VALIDATION);
    });
    it('should return PUBLISHED status when content is En cours', () => {
      // given
      const content: string = 'En cours';

      // when
      const result: JobOfferStatus = ZohoModelToDomainMappingStrategy.toStatus(content);

      // then
      expect(result).toBe(JobOfferStatus.STATUS_PUBLISHED);
    });
    it('should return ARCHIVED status when content is Inactif', () => {
      // given
      const content: string = 'Inactif';

      // when
      const result: JobOfferStatus = ZohoModelToDomainMappingStrategy.toStatus(content);

      // then
      expect(result).toBe(JobOfferStatus.STATUS_ARCHIVED);
    });
    it('should return null when content is an unknown job status', () => {
      // given
      const content: string = 'unknown';

      // when
      const result: JobOfferStatus = ZohoModelToDomainMappingStrategy.toStatus(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toJobStatus()', () => {
    it('should return FULL_TIME job status when content is Temps plein', () => {
      // given
      const content: string = 'Temps plein';

      // when
      const result: JobOfferJobStatus = ZohoModelToDomainMappingStrategy.toJobStatus(content);

      // then
      expect(result).toBe(JobOfferJobStatus.JOB_STATUS_FULL_TIME);
    });
    it('should return PART_TIME job status when content is Temps partiel', () => {
      // given
      const content: string = 'Temps partiel';

      // when
      const result: JobOfferJobStatus = ZohoModelToDomainMappingStrategy.toJobStatus(content);

      // then
      expect(result).toBe(JobOfferJobStatus.JOB_STATUS_PART_TIME);
    });
    it('should return null when content is an unknown job status', () => {
      // given
      const content: string = 'unknown';

      // when
      const result: JobOfferJobStatus = ZohoModelToDomainMappingStrategy.toJobStatus(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toJobType()', () => {
    it('should return REGULAR job type when content is Régulier', () => {
      // given
      const content: string = 'Régulier';

      // when
      const result: JobOfferJobType = ZohoModelToDomainMappingStrategy.toJobType(content);

      // then
      expect(result).toBe(JobOfferJobType.JOB_TYPE_REGULAR);
    });
    it('should return TEMPORARY job type when content is Temporaire', () => {
      // given
      const content: string = 'Temporaire';

      // when
      const result: JobOfferJobType = ZohoModelToDomainMappingStrategy.toJobType(content);

      // then
      expect(result).toBe(JobOfferJobType.JOB_TYPE_TEMPORARY);
    });
    it('should return INTERNSHIP job type when content is Stage', () => {
      // given
      const content: string = 'Stage';

      // when
      const result: JobOfferJobType = ZohoModelToDomainMappingStrategy.toJobType(content);

      // then
      expect(result).toBe(JobOfferJobType.JOB_TYPE_INTERNSHIP);
    });
    it('should return null when content is an unknown job type', () => {
      // given
      const content: string = 'unknown';

      // when
      const result: JobOfferJobType = ZohoModelToDomainMappingStrategy.toJobType(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toRegion()', () => {
    it('should return ABITIBI_TEMISCAMINGUE region when content is Abitibi-Témiscamingue', () => {
      // given
      const content: string = 'Abitibi-Témiscamingue';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_ABITIBI_TEMISCAMINGUE);
    });
    it('should return BAS_ST_LAURENT region when content is Bas St-Laurent', () => {
      // given
      const content: string = 'Bas St-Laurent';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_BAS_ST_LAURENT);
    });
    it('should return CAPITALE_NATIONALE region when content is Capitale-Nationale', () => {
      // given
      const content: string = 'Capitale-Nationale';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_CAPITALE_NATIONALE);
    });
    it('should return CENTRE_DU_QUEBEC region when content is Centre du Québec', () => {
      // given
      const content: string = 'Centre du Québec';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_CENTRE_DU_QUEBEC);
    });
    it('should return CHAUDIERES_APPALACHES region when content is Chaudières-Appalaches', () => {
      // given
      const content: string = 'Chaudières-Appalaches';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_CHAUDIERES_APPALACHES);
    });
    it('should return COTE_NORD region when content is Côte-Nord', () => {
      // given
      const content: string = 'Côte-Nord';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_COTE_NORD);
    });
    it('should return ESTRIE region when content is Estrie', () => {
      // given
      const content: string = 'Estrie';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_ESTRIE);
    });
    it('should return GASPESIE_ILES_DE_LA_MADELEINE region when content is Gaspésie/Îles-de-la-Madeleine', () => {
      // given
      const content: string = 'Gaspésie/Îles-de-la-Madeleine';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_GASPESIE_ILES_DE_LA_MADELEINE);
    });
    it('should return LANAUDIERE region when content is Lanaudière', () => {
      // given
      const content: string = 'Lanaudière';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_LANAUDIERE);
    });
    it('should return LAURENTIDES region when content is Laurentides', () => {
      // given
      const content: string = 'Laurentides';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_LAURENTIDES);
    });
    it('should return LAVAL region when content is Laval', () => {
      // given
      const content: string = 'Laval';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_LAVAL);
    });
    it('should return MAURICIE region when content is Mauricie', () => {
      // given
      const content: string = 'Mauricie';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_MAURICIE);
    });
    it('should return MONTEREGIE region when content is Montérégie', () => {
      // given
      const content: string = 'Montérégie';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_MONTEREGIE);
    });
    it('should return MONTREAL region when content is Montréal', () => {
      // given
      const content: string = 'Montréal';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_MONTREAL);
    });
    it('should return NORD_DU_QUEBEC region when content is Nord du Québec', () => {
      // given
      const content: string = 'Nord du Québec';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_NORD_DU_QUEBEC);
    });
    it('should return OUTAOUAIS region when content is Outaouais', () => {
      // given
      const content: string = 'Outaouais';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_OUTAOUAIS);
    });
    it('should return SAGUENAY_LAC_ST_JEAN region when content is Saguenay-Lac-St-Jean', () => {
      // given
      const content: string = 'Saguenay-Lac-St-Jean';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBe(JobOfferRegion.REGION_SAGUENAY_LAC_ST_JEAN);
    });
    it('should return null when content is an unknown region', () => {
      // given
      const content: string = 'unknown';

      // when
      const result: JobOfferRegion = ZohoModelToDomainMappingStrategy.toRegion(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toTargetCustomers()', () => {
    it('should return list of target customers when content is values separated by semicolons', () => {
      // given
      const content: string = 'Ingénieur;Ingénieurs juniors / CPI;Diplômés en génie;Étudiants et finissants';

      // when
      const result: JobOfferTargetCustomer[] = ZohoModelToDomainMappingStrategy.toTargetCustomers(content);

      // then
      expect(result).toStrictEqual([
        JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS,
        JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP,
        JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERING_GRADUATES,
        JobOfferTargetCustomer.TARGET_CUSTOMER_STUDENTS_AND_GRADUATES,
      ]);
    });
    it('should return null when content is unknown', () => {
      // given
      const content: string = 'unknown1;unknown2';

      // when
      const result: JobOfferTargetCustomer[] = ZohoModelToDomainMappingStrategy.toTargetCustomers(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toRequiredExperiences()', () => {
    it('should return list of required experiences when content is values separated by semicolons', () => {
      // given
      const content: string = '0 à 2 ans;3 à 5 ans;6 à 10 ans;11 ans et plus';

      // when
      const result: JobOfferRequiredExperience[] = ZohoModelToDomainMappingStrategy.toRequiredExperiences(content);

      // then
      expect(result).toStrictEqual([
        JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS,
        JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
        JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
        JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
      ]);
    });
    it('should return null when content is unknown', () => {
      // given
      const content: string = 'unknown1;unknown2';

      // when
      const result: JobOfferRequiredExperience[] = ZohoModelToDomainMappingStrategy.toRequiredExperiences(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toPositionType()', () => {
    it('should return ANALYST position type when content is Analyste', () => {
      // given
      const content: string = 'Analyste';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_ANALYST);
    });
    it('should return OTHERS position type when content is Autres', () => {
      // given
      const content: string = 'Autres';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_OTHERS);
    });
    it('should return PROJECT_MANAGER position type when content is Chargé(e) de projet', () => {
      // given
      const content: string = 'Chargé(e) de projet';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_PROJECT_MANAGER);
    });
    it('should return HEAD_OF_DEPARTMENT position type when content is Chef de service', () => {
      // given
      const content: string = 'Chef de service';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_HEAD_OF_DEPARTMENT);
    });
    it('should return RESEARCHER position type when content is Chercheur', () => {
      // given
      const content: string = 'Chercheur';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_RESEARCHER);
    });
    it('should return DESIGNER position type when content is Concepteur', () => {
      // given
      const content: string = 'Concepteur';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_DESIGNER);
    });
    it('should return ADVISOR position type when content is Conseiller(e)', () => {
      // given
      const content: string = 'Conseiller(e)';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_ADVISOR);
    });
    it('should return CONSULTANT position type when content is Consultante(e)', () => {
      // given
      const content: string = 'Consultante(e)';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_CONSULTANT);
    });
    it('should return COORDINATOR position type when content is Coordonnateur', () => {
      // given
      const content: string = 'Coordonnateur';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_COORDINATOR);
    });
    it('should return MANAGING_DIRECTOR position type when content is Directeur Général', () => {
      // given
      const content: string = 'Directeur Général';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_MANAGING_DIRECTOR);
    });
    it('should return DIRECTOR_OF_OPERATIONS position type when content is Directeur des opérations', () => {
      // given
      const content: string = 'Directeur des opérations';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_DIRECTOR_OF_OPERATIONS);
    });
    it('should return DIRECTOR_DIRECTOR position type when content is Directeur/Directrice', () => {
      // given
      const content: string = 'Directeur/Directrice';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_DIRECTOR_DIRECTOR);
    });
    it('should return DEVELOPER_PROGRAMMER position type when content is Développeur / Programmeur', () => {
      // given
      const content: string = 'Développeur / Programmeur';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_DEVELOPER_PROGRAMMER);
    });
    it('should return FORESTRY_ENGINEER position type when content is Ingénieur forestier', () => {
      // given
      const content: string = 'Ingénieur forestier';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_FORESTRY_ENGINEER);
    });
    it('should return JUNIOR_ENGINEERS_CEP position type when content is Ingénieurs juniors / CPI', () => {
      // given
      const content: string = 'Ingénieurs juniors / CPI';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_JUNIOR_ENGINEERS_CEP);
    });
    it('should return ENGINEER position type when content is Ingénieur(e)', () => {
      // given
      const content: string = 'Ingénieur(e)';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_ENGINEER);
    });
    it('should return PROJECT_ENGINEER position type when content is Ingénieur(e) de projet', () => {
      // given
      const content: string = 'Ingénieur(e) de projet';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_PROJECT_ENGINEER);
    });
    it('should return PROFESSOR position type when content is Professeur', () => {
      // given
      const content: string = 'Professeur';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_PROFESSOR);
    });
    it('should return PRESIDENT_OF_THE_BOARD_OF_DIRECTORS position type when content is Président/Présidente', () => {
      // given
      const content: string = 'Président/Présidente';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_PRESIDENT_OF_THE_BOARD_OF_DIRECTORS);
    });
    it('should return VICE_CHAIRPERSON position type when content is Vice-Président(e)', () => {
      // given
      const content: string = 'Vice-Président(e)';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBe(JobOfferPositionType.POSITION_TYPE_VICE_CHAIRPERSON);
    });
    it('should return null when content is an unknown job position', () => {
      // given
      const content: string = 'unknown';

      // when
      const result: JobOfferPositionType = ZohoModelToDomainMappingStrategy.toPositionType(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toRequestedSpecialities()', () => {
    it('should return list of requested specialities when content is values separated by semicolons', () => {
      // given
      const content: string =
        'Aéronautique / aérospatiale;Agriculture;Aucune en particulier;Autre spécialité;Biomédical;Biotechnologie;Chimie;Civil;Construction;Documentation;Électrique;Environnement;Géologie;Gestion de la qualité;Industriel / Manufacturier;Informatique;Instrumentation;Logiciel;Manutention / Distribution des matériaux;Maritime / Naval;Matériaux non métalliques;Mécanique;Métallurgie / Métaux;Minier;Nucléaire;Pâtes et Papiers;Physique;Production automatisée;Robotique;Santé et Sécurité du travail';

      // when
      const result: JobOfferRequestedSpeciality[] = ZohoModelToDomainMappingStrategy.toRequestedSpecialities(content);

      // then
      expect(result).toStrictEqual([
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NONE_IN_PARTICULAR,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OTHER_SPECIALITY,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOTECHNOLOGY,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CHEMISTRY,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CIVIL,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CONSTRUCTION,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_DOCUMENTATION,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ELECTRICAL,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ENVIRONMENT,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_GEOLOGY,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_QUALITY_MANAGEMENT,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INDUSTRIAL_MANUFACTURER,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INFORMATION_TECHNOLOGY,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INSTRUMENTATION,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_SOFTWARE,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MATERIAL_HANDLING_DISTRIBUTION,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MARITIME_NAVAL,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NON_METALLIC_MATERIALS,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MECHANICS,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_METALLURGY_METALS,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MINING,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NUCLEAR_POWER,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PULP_AND_PAPER,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PHYSICAL,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ROBOTICS,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OCCUPATIONAL_HEALTH_AND_SAFETY,
      ]);
    });
    it('should return null when content is unknown', () => {
      // given
      const content: string = 'unknown1;unknown2';

      // when
      const result: JobOfferRequestedSpeciality[] = ZohoModelToDomainMappingStrategy.toRequestedSpecialities(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toTravelingIsTraveling()', () => {
    it('should return true when content is Oui', () => {
      // given
      const content: string = 'Oui';

      // when
      const result: boolean = ZohoModelToDomainMappingStrategy.toTravelingIsTraveling(content);

      // then
      expect(result).toBe(true);
    });
    it('should return false when content is Non', () => {
      // given
      const content: string = 'Non';

      // when
      const result: boolean = ZohoModelToDomainMappingStrategy.toTravelingIsTraveling(content);

      // then
      expect(result).toBe(false);
    });
    it('should return null when content is unknown', () => {
      // given
      const content: string = 'unknown';

      // when
      const result: boolean = ZohoModelToDomainMappingStrategy.toTravelingIsTraveling(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toTravelingFrequency()', () => {
    it('should return OCCASIONAL when content is Occasionnels', () => {
      // given
      const content: string = 'Occasionnels';

      // when
      const result: TravelingFrequency = ZohoModelToDomainMappingStrategy.toTravelingFrequency(content);

      // then
      expect(result).toBe(TravelingFrequency.TRAVELING_OCCASIONAL);
    });
    it('should return FREQUENT when content is Fréquents', () => {
      // given
      const content: string = 'Fréquents';

      // when
      const result: TravelingFrequency = ZohoModelToDomainMappingStrategy.toTravelingFrequency(content);

      // then
      expect(result).toBe(TravelingFrequency.TRAVELING_FREQUENT);
    });
    it('should return null when content is unknown', () => {
      // given
      const content: string = 'unknown';

      // when
      const result: TravelingFrequency = ZohoModelToDomainMappingStrategy.toTravelingFrequency(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toTravelingFrequency()', () => {
    it('should return OCCASIONAL when content is Occasionnels', () => {
      // given
      const content: string = 'Occasionnels';

      // when
      const result: TravelingFrequency = ZohoModelToDomainMappingStrategy.toTravelingFrequency(content);

      // then
      expect(result).toBe(TravelingFrequency.TRAVELING_OCCASIONAL);
    });
    it('should return FREQUENT when content is Fréquents', () => {
      // given
      const content: string = 'Fréquents';

      // when
      const result: TravelingFrequency = ZohoModelToDomainMappingStrategy.toTravelingFrequency(content);

      // then
      expect(result).toBe(TravelingFrequency.TRAVELING_FREQUENT);
    });
    it('should return null when content is unknown', () => {
      // given
      const content: string = 'unknown';

      // when
      const result: TravelingFrequency = ZohoModelToDomainMappingStrategy.toTravelingFrequency(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toTravelingFrequency()', () => {
    it('should return OCCASIONAL when content is Occasionnels', () => {
      // given
      const content: string = 'Occasionnels';

      // when
      const result: TravelingFrequency = ZohoModelToDomainMappingStrategy.toTravelingFrequency(content);

      // then
      expect(result).toBe(TravelingFrequency.TRAVELING_OCCASIONAL);
    });
    it('should return FREQUENT when content is Fréquents', () => {
      // given
      const content: string = 'Fréquents';

      // when
      const result: TravelingFrequency = ZohoModelToDomainMappingStrategy.toTravelingFrequency(content);

      // then
      expect(result).toBe(TravelingFrequency.TRAVELING_FREQUENT);
    });
    it('should return null when content is unknown', () => {
      // given
      const content: string = 'unknown';

      // when
      const result: TravelingFrequency = ZohoModelToDomainMappingStrategy.toTravelingFrequency(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toAdvantages()', () => {
    it('should return advantages when content is values separated by semicolons', () => {
      // given
      const content: string =
        'Assurance dentaire;Assurance frais médicaux;Assurance vue;Assurance vie;Assurance voyage;Assurance salaire;Assurance invalidité courte durée;Assurance invalidité longue durée' +
        ';' +
        'Activités sociales;Voyages et vacances;Cafétéria;Fruits frais;Salle de détente;Programme mieux-être;Programme de référencement' +
        ';' +
        'Conciliation famille-travail;Congés payés;Horaires flexibles;Congés maladie;Horaires d’été;Congés mobiles' +
        ';' +
        'Rabais employé;Rémunération compétitive;Régime retraite;Régime de participation différée aux bénéfices RPDB;PAE;Reconnaissance ancienneté;Remboursement cotisation à une association ou ordre professionnel;Boni sur performance;Stationnement;Abonnement de transport' +
        ';' +
        'Programme de formation et développement;Programme de reconnaissance';

      // when
      const result: AdvantagesInterface = ZohoModelToDomainMappingStrategy.toAdvantages(content);

      // then
      expect(result).toStrictEqual({
        health: [
          AdvantageHealth.HEALTH_DENTAL_INSURANCE,
          AdvantageHealth.HEALTH_MEDICAL_EXPENSES_INSURANCE,
          AdvantageHealth.HEALTH_SIGHT_INSURANCE,
          AdvantageHealth.HEALTH_LIFE_INSURANCE,
          AdvantageHealth.HEALTH_TRAVEL_INSURANCE,
          AdvantageHealth.HEALTH_SALARY_INSURANCE,
          AdvantageHealth.HEALTH_SHORT_TERM_DISABILITY_INSURANCE,
          AdvantageHealth.HEALTH_LONG_TERM_DISABILITY_INSURANCE,
        ],
        social: [
          AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES,
          AdvantageSocial.SOCIAL_TRAVEL_AND_HOLIDAYS,
          AdvantageSocial.SOCIAL_CAFETERIA,
          AdvantageSocial.SOCIAL_FRESH_FRUIT_AND_VEGETABLES,
          AdvantageSocial.SOCIAL_RELAXATION_ROOM,
          AdvantageSocial.SOCIAL_WELLNESS_PROGRAM,
          AdvantageSocial.SOCIAL_REFERENCING_PROGRAM,
        ],
        lifeBalance: [
          AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK,
          AdvantageLifeBalance.LIFE_BALANCE_PAID_HOLIDAYS,
          AdvantageLifeBalance.LIFE_BALANCE_FLEXIBLE_SCHEDULES,
          AdvantageLifeBalance.LIFE_BALANCE_SICK_LEAVE,
          AdvantageLifeBalance.LIFE_BALANCE_SUMMER_HOURS,
          AdvantageLifeBalance.LIFE_BALANCE_MOBILE_HOLIDAYS,
        ],
        financial: [
          AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT,
          AdvantageFinancial.FINANCIAL_COMPETITIVE_COMPENSATION,
          AdvantageFinancial.FINANCIAL_PENSION_PLAN,
          AdvantageFinancial.FINANCIAL_DEFERRED_PROFIT_SHARING_PLAN_DPSP,
          AdvantageFinancial.FINANCIAL_EAP,
          AdvantageFinancial.FINANCIAL_RECOGNITION_OF_SENIORITY,
          AdvantageFinancial.FINANCIAL_REFUND_OF_DUES_TO_A_PROFESSIONAL_ASSOCIATION_OR_ORDER,
          AdvantageFinancial.FINANCIAL_PERFORMANCE_BONUS,
          AdvantageFinancial.FINANCIAL_PARKING_LOT,
          AdvantageFinancial.FINANCIAL_TRANSPORT_SUBSCRIPTION,
        ],
        professional: [AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM, AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM],
      } as AdvantagesInterface);
    });
    it('should return advantages with only one field when only one value', () => {
      // given
      const content: string = 'Conciliation famille-travail';

      // when
      const result: AdvantagesInterface = ZohoModelToDomainMappingStrategy.toAdvantages(content);

      // then
      expect(result).toStrictEqual({ lifeBalance: [AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK] } as AdvantagesInterface);
    });
    it('should return null when content is unknown', () => {
      // given
      const content: string = 'unknown1;unknown2';

      // when
      const result: AdvantagesInterface = ZohoModelToDomainMappingStrategy.toAdvantages(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toWantedPersonalities()', () => {
    it('should return list of wanted personalities when content is values separated by semicolons', () => {
      // given
      const content: string =
        "Communicant;Ambitieux;Autonome;Innovant;Curieux;Décideur;Stratège;Flexible;Leader;Négociateur;Organisé;Rigoureux;Débrouillard;D'équipe;Orateur;Pondérateur;Diplomate";

      // when
      const result: JobOfferWantedPersonality[] = ZohoModelToDomainMappingStrategy.toWantedPersonalities(content);

      // then
      expect(result).toStrictEqual([
        JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_AUTONOMOUS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_INNOVATIVE,
        JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_DECISION_MAKERS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_STRATEGISTS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_FLEXIBLE,
        JobOfferWantedPersonality.WANTED_PERSONALITY_LEADERS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_NEGOTIATORS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_ORGANIZED,
        JobOfferWantedPersonality.WANTED_PERSONALITY_RIGOROUS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_RESOURCEFUL,
        JobOfferWantedPersonality.WANTED_PERSONALITY_TEAMWORK,
        JobOfferWantedPersonality.WANTED_PERSONALITY_SPEAKERS_AT_THE_CONFERENCE,
        JobOfferWantedPersonality.WANTED_PERSONALITY_PONDERATOR,
        JobOfferWantedPersonality.WANTED_PERSONALITY_DIPLOMATS,
      ]);
    });
    it('should return null when content is unknown', () => {
      // given
      const content: string = 'unknown1;unknown2';

      // when
      const result: JobOfferWantedPersonality[] = ZohoModelToDomainMappingStrategy.toWantedPersonalities(content);

      // then
      expect(result).toBeNull();
    });
  });

  describe('toStartDateOfEmploymentAsSoonAsPossible()', () => {
    it('should return true when content is Oui', () => {
      // given
      const content: string = 'Oui';

      // when
      const result: boolean = ZohoModelToDomainMappingStrategy.toStartDateOfEmploymentAsSoonAsPossible(content);

      // then
      expect(result).toBe(true);
    });
    it('should return false when content is Non', () => {
      // given
      const content: string = 'Non';

      // when
      const result: boolean = ZohoModelToDomainMappingStrategy.toStartDateOfEmploymentAsSoonAsPossible(content);

      // then
      expect(result).toBe(false);
    });
    it('should return null when content is unknown', () => {
      // given
      const content: string = 'unknown';

      // when
      const result: boolean = ZohoModelToDomainMappingStrategy.toStartDateOfEmploymentAsSoonAsPossible(content);

      // then
      expect(result).toBeNull();
    });
  });
});

import { DomainToZohoModelMappingStrategy } from '../domain-to-zoho-model-mapping-strategy';
import { ZohoJsonFieldInterface } from '../models/json/zoho-json-field.interface';
import { ZohoRequestAdapterService } from '../zoho-request-adapter.service';

describe('infrastructure/vendors/zoho/ZohoRequestAdapterService', () => {
  let zohoRequestAdapterService: ZohoRequestAdapterService;

  beforeEach(() => {
    zohoRequestAdapterService = new ZohoRequestAdapterService();
  });

  describe('map()', () => {
    let testDomain: TestTypeInterface;
    let val1MapMock: jest.Mock;
    let val2MapMock: jest.Mock;
    let val3MapMock: jest.Mock;
    let domainToZohoModelMappingStrategy: DomainToZohoModelMappingStrategy;

    beforeEach(() => {
      testDomain = {
        a: 1337,
        b: {
          c: 'content',
        },
        d: false,
      } as TestTypeInterface;

      val1MapMock = jest.fn();
      val1MapMock.mockReturnValue('1337');

      val2MapMock = jest.fn();
      val2MapMock.mockReturnValue('content mapped');

      val3MapMock = jest.fn();
      val3MapMock.mockReturnValue('No');

      domainToZohoModelMappingStrategy = {
        a: {
          target: 'val1',
          map: val1MapMock,
        },
        'b.c': {
          target: 'val2',
          map: val2MapMock,
        },
        d: {
          target: 'val3',
          map: val3MapMock,
        },
      };
    });

    it('should call map function of mapping strategy for each domain value', () => {
      // when
      zohoRequestAdapterService.map<TestTypeInterface>(testDomain, domainToZohoModelMappingStrategy);

      // then
      expect(val1MapMock).toHaveBeenCalledWith(1337);
      expect(val2MapMock).toHaveBeenCalledWith('content');
      expect(val3MapMock).toHaveBeenCalledWith(false);
    });

    it('should return an array result of each mapped domain value to corresponding target path', () => {
      // when
      const result: ZohoJsonFieldInterface[] = zohoRequestAdapterService.map<TestTypeInterface>(testDomain, domainToZohoModelMappingStrategy);

      // then
      expect(result).toStrictEqual([
        {
          val: 'val1',
          content: '1337',
        },
        {
          val: 'val2',
          content: 'content mapped',
        },
        {
          val: 'val3',
          content: 'No',
        },
      ] as ZohoJsonFieldInterface[]);
    });

    it('should ignore domain fields that are unknown to mapping strategy', () => {
      // given
      testDomain = {
        a: 1337,
        e: 'another content',
      } as TestTypeInterface;

      // when
      const result: ZohoJsonFieldInterface[] = zohoRequestAdapterService.map<TestTypeInterface>(testDomain, domainToZohoModelMappingStrategy);

      // then
      expect(result).toStrictEqual([
        {
          val: 'val1',
          content: '1337',
        },
      ] as ZohoJsonFieldInterface[]);
    });

    it('should ignore mapped domain value when null', () => {
      // given
      val1MapMock.mockReturnValue(null);
      val3MapMock.mockReturnValue(null);

      // when
      const result: ZohoJsonFieldInterface[] = zohoRequestAdapterService.map<TestTypeInterface>(testDomain, domainToZohoModelMappingStrategy);

      // then
      expect(result).toStrictEqual([
        {
          val: 'val2',
          content: 'content mapped',
        },
      ] as ZohoJsonFieldInterface[]);
    });

    it('should return empty array when all fields are unknown to mapping strategy', () => {
      // given
      testDomain = {
        e: 'another content',
      } as TestTypeInterface;

      // when
      const result: ZohoJsonFieldInterface[] = zohoRequestAdapterService.map<TestTypeInterface>(testDomain, domainToZohoModelMappingStrategy);

      // then
      expect(result).toStrictEqual([] as ZohoJsonFieldInterface[]);
    });
  });
});

interface TestTypeInterface {
  a: number;
  b: {
    c: string;
  };
  d: boolean;
  e: string;
}

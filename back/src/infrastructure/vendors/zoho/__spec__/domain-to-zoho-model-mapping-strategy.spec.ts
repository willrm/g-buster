import {
  AdvantageFinancial,
  AdvantageHealth,
  AdvantageLifeBalance,
  AdvantageProfessional,
  AdvantageSocial,
} from '../../../../domain/advantages/advantages-enums';
import { AdvantagesInterface } from '../../../../domain/advantages/advantages.interface';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from '../../../../domain/job-offer/job-offer-enums';
import { TravelingFrequency } from '../../../../domain/traveling/traveling-enums';
import { DomainToZohoModelMappingStrategy } from '../domain-to-zoho-model-mapping-strategy';

describe('infrastructure/vendors/zoho/DomainToZohoModelMappingStrategy', () => {
  describe('noOp()', () => {
    it('should return value unchanged', () => {
      // given
      const value: string = 'a value';

      // when
      const result: string = DomainToZohoModelMappingStrategy.noOp(value);

      // then
      expect(result).toBe('a value');
    });
  });

  describe('fromNumber()', () => {
    it('should return value number as string', () => {
      // given
      const value: number = 123456789;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromNumber(value);

      // then
      expect(result).toBe('123456789');
    });
  });

  describe('fromBoolean()', () => {
    it('should return string true when value is true', () => {
      // given
      const value: boolean = true;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromBoolean(value);

      // then
      expect(result).toBe('true');
    });
    it('should return string false when value is false', () => {
      // given
      const value: boolean = false;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromBoolean(value);

      // then
      expect(result).toBe('false');
    });
    it('should return string false when value is unknown', () => {
      // given
      const value: boolean = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromBoolean(value);

      // then
      expect(result).toBe('false');
    });
  });

  describe('fromDate()', () => {
    it('should return date as iso string without time', () => {
      // given
      const value: Date = new Date('2019-03-28T20:35:57.000Z');

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromDate(value);

      // then
      expect(result).toBe('2019-03-28');
    });
  });

  describe('fromEmail()', () => {
    it('should return email with unique timestamped suffix', () => {
      // given
      const content: string = 'test@example.org';

      const fixedDate: Date = new Date('2020-06-13T04:41:20');
      // @ts-ignore
      jest.spyOn(global, 'Date').mockImplementationOnce(() => fixedDate);

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromEmail(content);

      // then
      expect(result).toBe('test@example.org+REMOVE-THIS-TIMESTAMPED-SUFFIX-TO-HAVE-REAL-EMAIL-1592037680000');
    });
  });

  describe('fromStatus()', () => {
    it('should return Rempli when value is DRAFT status', () => {
      // given
      const value: JobOfferStatus = JobOfferStatus.STATUS_DRAFT;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromStatus(value);

      // then
      expect(result).toBe('Rempli');
    });
    it('should return En attente d’approbation when value is PENDING_VALIDATION status', () => {
      // given
      const value: JobOfferStatus = JobOfferStatus.STATUS_PENDING_VALIDATION;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromStatus(value);

      // then
      expect(result).toBe('En attente d’approbation');
    });
    it('should return En cours when value is PUBLISHED status', () => {
      // given
      const value: JobOfferStatus = JobOfferStatus.STATUS_PUBLISHED;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromStatus(value);

      // then
      expect(result).toBe('En cours');
    });
    it('should return Inactif when value is ARCHIVED status', () => {
      // given
      const value: JobOfferStatus = JobOfferStatus.STATUS_ARCHIVED;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromStatus(value);

      // then
      expect(result).toBe('Inactif');
    });
    it('should return null when value is an unknown status', () => {
      // given
      const value: JobOfferStatus = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromStatus(value);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromJobStatus()', () => {
    it('should return Temps plein when value is FULL_TIME job status', () => {
      // given
      const value: JobOfferJobStatus = JobOfferJobStatus.JOB_STATUS_FULL_TIME;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromJobStatus(value);

      // then
      expect(result).toBe('Temps plein');
    });
    it('should return Temps partiel when value is PART_TIME job status', () => {
      // given
      const value: JobOfferJobStatus = JobOfferJobStatus.JOB_STATUS_PART_TIME;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromJobStatus(value);

      // then
      expect(result).toBe('Temps partiel');
    });
    it('should return null when value is an unknown job status', () => {
      // given
      const value: JobOfferJobStatus = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromJobStatus(value);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromJobType()', () => {
    it('should return Régulier when value is REGULAR job type', () => {
      // given
      const value: JobOfferJobType = JobOfferJobType.JOB_TYPE_REGULAR;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromJobType(value);

      // then
      expect(result).toBe('Régulier');
    });
    it('should return Stage when value is INTERNSHIP job type', () => {
      // given
      const value: JobOfferJobType = JobOfferJobType.JOB_TYPE_INTERNSHIP;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromJobType(value);

      // then
      expect(result).toBe('Stage');
    });
    it('should return Stage when value is INTERNSHIP job type', () => {
      // given
      const value: JobOfferJobType = JobOfferJobType.JOB_TYPE_TEMPORARY;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromJobType(value);

      // then
      expect(result).toBe('Temporaire');
    });
    it('should return null when value is an unknown job type', () => {
      // given
      const value: JobOfferJobType = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromJobType(value);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromRegion()', () => {
    it('should return Abitibi-Témiscamingue when value is ABITIBI_TEMISCAMINGUE region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_ABITIBI_TEMISCAMINGUE;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Abitibi-Témiscamingue');
    });
    it('should return Bas St-Laurent when value is BAS_ST_LAURENT region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_BAS_ST_LAURENT;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Bas St-Laurent');
    });
    it('should return Capitale-Nationale when value is CAPITALE_NATIONALE region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_CAPITALE_NATIONALE;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Capitale-Nationale');
    });
    it('should return Centre du Québec when value is CENTRE_DU_QUEBEC region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_CENTRE_DU_QUEBEC;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Centre du Québec');
    });
    it('should return Chaudières-Appalaches when value is CHAUDIERES_APPALACHES region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_CHAUDIERES_APPALACHES;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Chaudières-Appalaches');
    });
    it('should return Côte-Nord when value is COTE_NORD region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_COTE_NORD;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Côte-Nord');
    });
    it('should return Estrie when value is ESTRIE region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_ESTRIE;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Estrie');
    });
    it('should return Gaspésie/Îles-de-la-Madeleine when value is GASPESIE_ILES_DE_LA_MADELEINE region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_GASPESIE_ILES_DE_LA_MADELEINE;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Gaspésie/Îles-de-la-Madeleine');
    });
    it('should return Lanaudière when value is LANAUDIERE region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_LANAUDIERE;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Lanaudière');
    });
    it('should return Laurentides when value is LAURENTIDES region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_LAURENTIDES;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Laurentides');
    });
    it('should return Laval when value is LAVAL region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_LAVAL;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Laval');
    });
    it('should return Mauricie when value is MAURICIE region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_MAURICIE;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Mauricie');
    });
    it('should return Montérégie when value is MONTEREGIE region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_MONTEREGIE;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Montérégie');
    });
    it('should return Montréal when value is MONTREAL region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_MONTREAL;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Montréal');
    });
    it('should return Nord du Québec when value is NORD_DU_QUEBEC region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_NORD_DU_QUEBEC;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Nord du Québec');
    });
    it('should return Outaouais when value is OUTAOUAIS region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_OUTAOUAIS;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Outaouais');
    });
    it('should return Saguenay-Lac-St-Jean when value is SAGUENAY_LAC_ST_JEAN region', () => {
      // given
      const value: JobOfferRegion = JobOfferRegion.REGION_SAGUENAY_LAC_ST_JEAN;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBe('Saguenay-Lac-St-Jean');
    });
    it('should return null when value is an unknown region', () => {
      // given
      const value: JobOfferRegion = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRegion(value);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromTargetCustomers()', () => {
    it('should return zoho values separated by semicolons when value is a list of target customers', () => {
      // given
      const values: JobOfferTargetCustomer[] = [
        JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS,
        JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP,
        JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERING_GRADUATES,
        JobOfferTargetCustomer.TARGET_CUSTOMER_STUDENTS_AND_GRADUATES,
      ];

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTargetCustomers(values);

      // then
      expect(result).toBe('Ingénieur;Ingénieurs juniors / CPI;Diplômés en génie;Étudiants et finissants');
    });
    it('should return null when value is an empty list', () => {
      // given
      const values: JobOfferTargetCustomer[] = [];

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTargetCustomers(values);

      // then
      expect(result).toBeNull();
    });
    it('should return null when value is undefined', () => {
      // given
      const values: JobOfferTargetCustomer[] = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTargetCustomers(values);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromRequiredExperiences()', () => {
    it('should return zoho values separated by semicolons when value is a list of required experiences', () => {
      // given
      const values: JobOfferRequiredExperience[] = [
        JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS,
        JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
        JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
        JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
      ];

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRequiredExperiences(values);

      // then
      expect(result).toBe('0 à 2 ans;3 à 5 ans;6 à 10 ans;11 ans et plus');
    });
    it('should return null when value is an empty list', () => {
      // given
      const values: JobOfferRequiredExperience[] = [];

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRequiredExperiences(values);

      // then
      expect(result).toBeNull();
    });
    it('should return null when value is undefined', () => {
      // given
      const values: JobOfferRequiredExperience[] = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRequiredExperiences(values);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromPositionType()', () => {
    it('should return Analyste when value is ANALYST position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_ANALYST;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Analyste');
    });
    it('should return Autres when value is OTHERS position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_OTHERS;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Autres');
    });
    it('should return Chargé(e) de projet when value is PROJECT_MANAGER position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_PROJECT_MANAGER;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Chargé(e) de projet');
    });
    it('should return Chef de service when value is HEAD_OF_DEPARTMENT position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_HEAD_OF_DEPARTMENT;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Chef de service');
    });
    it('should return Chercheur when value is RESEARCHER position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_RESEARCHER;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Chercheur');
    });
    it('should return Concepteur when value is DESIGNER position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_DESIGNER;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Concepteur');
    });
    it('should return Conseiller(e) when value is ADVISOR position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_ADVISOR;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Conseiller(e)');
    });
    it('should return Consultante(e) when value is CONSULTANT position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_CONSULTANT;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Consultante(e)');
    });
    it('should return Coordonnateur when value is COORDINATOR position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_COORDINATOR;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Coordonnateur');
    });
    it('should return Directeur Général when value is MANAGING_DIRECTOR position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_MANAGING_DIRECTOR;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Directeur Général');
    });
    it('should return Directeur des opérations when value is DIRECTOR_OF_OPERATIONS position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_DIRECTOR_OF_OPERATIONS;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Directeur des opérations');
    });
    it('should return Directeur/Directrice when value is DIRECTOR_DIRECTOR position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_DIRECTOR_DIRECTOR;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Directeur/Directrice');
    });
    it('should return Développeur / Programmeur when value is DEVELOPER_PROGRAMMER position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_DEVELOPER_PROGRAMMER;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Développeur / Programmeur');
    });
    it('should return Ingénieur forestier when value is FORESTRY_ENGINEER position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_FORESTRY_ENGINEER;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Ingénieur forestier');
    });
    it('should return Ingénieurs juniors / CPI when value is JUNIOR_ENGINEERS_CEP position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_JUNIOR_ENGINEERS_CEP;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Ingénieurs juniors / CPI');
    });
    it('should return Ingénieur(e) when value is ENGINEER position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_ENGINEER;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Ingénieur(e)');
    });
    it('should return Ingénieur(e) de projet when value is PROJECT_ENGINEER position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_PROJECT_ENGINEER;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Ingénieur(e) de projet');
    });
    it('should return Professeur when value is PROFESSOR position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_PROFESSOR;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Professeur');
    });
    it('should return Président/Présidente when value is PRESIDENT_OF_THE_BOARD_OF_DIRECTORS position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_PRESIDENT_OF_THE_BOARD_OF_DIRECTORS;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Président/Présidente');
    });
    it('should return Vice-Président(e) when value is VICE_CHAIRPERSON position type', () => {
      // given
      const value: JobOfferPositionType = JobOfferPositionType.POSITION_TYPE_VICE_CHAIRPERSON;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBe('Vice-Président(e)');
    });
    it('should return null when value is an unknown position type', () => {
      // given
      const value: JobOfferPositionType = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromPositionType(value);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromRequestedSpecialities()', () => {
    it('should return zoho values separated by semicolons when value is a list of requested specialities', () => {
      // given
      const values: JobOfferRequestedSpeciality[] = [
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NONE_IN_PARTICULAR,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OTHER_SPECIALITY,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOTECHNOLOGY,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CHEMISTRY,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CIVIL,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CONSTRUCTION,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_DOCUMENTATION,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ELECTRICAL,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ENVIRONMENT,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_GEOLOGY,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_QUALITY_MANAGEMENT,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INDUSTRIAL_MANUFACTURER,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INFORMATION_TECHNOLOGY,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INSTRUMENTATION,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_SOFTWARE,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MATERIAL_HANDLING_DISTRIBUTION,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MARITIME_NAVAL,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NON_METALLIC_MATERIALS,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MECHANICS,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_METALLURGY_METALS,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MINING,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NUCLEAR_POWER,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PULP_AND_PAPER,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PHYSICAL,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ROBOTICS,
        JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OCCUPATIONAL_HEALTH_AND_SAFETY,
      ];

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRequestedSpecialities(values);

      // then
      expect(result).toBe(
        'Aéronautique / aérospatiale;Agriculture;Aucune en particulier;Autre spécialité;Biomédical;Biotechnologie;Chimie;Civil;Construction;Documentation;Électrique;Environnement;Géologie;Gestion de la qualité;Industriel / Manufacturier;Informatique;Instrumentation;Logiciel;Manutention / Distribution des matériaux;Maritime / Naval;Matériaux non métalliques;Mécanique;Métallurgie / Métaux;Minier;Nucléaire;Pâtes et Papiers;Physique;Production automatisée;Robotique;Santé et Sécurité du travail'
      );
    });
    it('should return null when value is an empty list', () => {
      // given
      const values: JobOfferRequestedSpeciality[] = [];

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRequestedSpecialities(values);

      // then
      expect(result).toBeNull();
    });
    it('should return null when value is undefined', () => {
      // given
      const values: JobOfferRequestedSpeciality[] = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromRequestedSpecialities(values);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromTravelingIsTraveling()', () => {
    it('should return Oui when value is true', () => {
      // given
      const value: boolean = true;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTravelingIsTraveling(value);

      // then
      expect(result).toBe('Oui');
    });
    it('should return Non when value is false', () => {
      // given
      const value: boolean = false;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTravelingIsTraveling(value);

      // then
      expect(result).toBe('Non');
    });
    it('should return null when value is an unknown value', () => {
      // given
      const value: boolean = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTravelingIsTraveling(value);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromTravelingFrequency()', () => {
    it('should return Occasionnels when value is OCCASIONAL traveling', () => {
      // given
      const value: TravelingFrequency = TravelingFrequency.TRAVELING_OCCASIONAL;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTravelingFrequency(value);

      // then
      expect(result).toBe('Occasionnels');
    });
    it('should return Fréquents when value is FREQUENT traveling', () => {
      // given
      const value: TravelingFrequency = TravelingFrequency.TRAVELING_FREQUENT;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTravelingFrequency(value);

      // then
      expect(result).toBe('Fréquents');
    });
    it('should return null when value is an unknown job type', () => {
      // given
      const value: TravelingFrequency = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTravelingFrequency(value);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromTravelingFrequency()', () => {
    it('should return Occasionnels when value is OCCASIONAL traveling', () => {
      // given
      const value: TravelingFrequency = TravelingFrequency.TRAVELING_OCCASIONAL;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTravelingFrequency(value);

      // then
      expect(result).toBe('Occasionnels');
    });
    it('should return Fréquents when value is FREQUENT traveling', () => {
      // given
      const value: TravelingFrequency = TravelingFrequency.TRAVELING_FREQUENT;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTravelingFrequency(value);

      // then
      expect(result).toBe('Fréquents');
    });
    it('should return null when value is an unknown job type', () => {
      // given
      const value: TravelingFrequency = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTravelingFrequency(value);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromTravelingFrequency()', () => {
    it('should return Occasionnels when value is OCCASIONAL traveling', () => {
      // given
      const value: TravelingFrequency = TravelingFrequency.TRAVELING_OCCASIONAL;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTravelingFrequency(value);

      // then
      expect(result).toBe('Occasionnels');
    });
    it('should return Fréquents when value is FREQUENT traveling', () => {
      // given
      const value: TravelingFrequency = TravelingFrequency.TRAVELING_FREQUENT;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTravelingFrequency(value);

      // then
      expect(result).toBe('Fréquents');
    });
    it('should return null when value is an unknown job type', () => {
      // given
      const value: TravelingFrequency = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromTravelingFrequency(value);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromAdvantages()', () => {
    it('should return zoho values separated by semicolons when value is advantages', () => {
      // given
      const value: AdvantagesInterface = {
        health: [
          AdvantageHealth.HEALTH_DENTAL_INSURANCE,
          AdvantageHealth.HEALTH_MEDICAL_EXPENSES_INSURANCE,
          AdvantageHealth.HEALTH_SIGHT_INSURANCE,
          AdvantageHealth.HEALTH_LIFE_INSURANCE,
          AdvantageHealth.HEALTH_TRAVEL_INSURANCE,
          AdvantageHealth.HEALTH_SALARY_INSURANCE,
          AdvantageHealth.HEALTH_SHORT_TERM_DISABILITY_INSURANCE,
          AdvantageHealth.HEALTH_LONG_TERM_DISABILITY_INSURANCE,
        ],
        social: [
          AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES,
          AdvantageSocial.SOCIAL_TRAVEL_AND_HOLIDAYS,
          AdvantageSocial.SOCIAL_CAFETERIA,
          AdvantageSocial.SOCIAL_FRESH_FRUIT_AND_VEGETABLES,
          AdvantageSocial.SOCIAL_RELAXATION_ROOM,
          AdvantageSocial.SOCIAL_WELLNESS_PROGRAM,
          AdvantageSocial.SOCIAL_REFERENCING_PROGRAM,
        ],
        lifeBalance: [
          AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK,
          AdvantageLifeBalance.LIFE_BALANCE_PAID_HOLIDAYS,
          AdvantageLifeBalance.LIFE_BALANCE_FLEXIBLE_SCHEDULES,
          AdvantageLifeBalance.LIFE_BALANCE_SICK_LEAVE,
          AdvantageLifeBalance.LIFE_BALANCE_SUMMER_HOURS,
          AdvantageLifeBalance.LIFE_BALANCE_MOBILE_HOLIDAYS,
        ],
        financial: [
          AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT,
          AdvantageFinancial.FINANCIAL_COMPETITIVE_COMPENSATION,
          AdvantageFinancial.FINANCIAL_PENSION_PLAN,
          AdvantageFinancial.FINANCIAL_DEFERRED_PROFIT_SHARING_PLAN_DPSP,
          AdvantageFinancial.FINANCIAL_EAP,
          AdvantageFinancial.FINANCIAL_RECOGNITION_OF_SENIORITY,
          AdvantageFinancial.FINANCIAL_REFUND_OF_DUES_TO_A_PROFESSIONAL_ASSOCIATION_OR_ORDER,
          AdvantageFinancial.FINANCIAL_PERFORMANCE_BONUS,
          AdvantageFinancial.FINANCIAL_PARKING_LOT,
          AdvantageFinancial.FINANCIAL_TRANSPORT_SUBSCRIPTION,
        ],
        professional: [AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM, AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM],
      } as AdvantagesInterface;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromAdvantages(value);

      // then
      expect(result).toBe(
        'Assurance dentaire;Assurance frais médicaux;Assurance vue;Assurance vie;Assurance voyage;Assurance salaire;Assurance invalidité courte durée;Assurance invalidité longue durée' +
          ';' +
          'Activités sociales;Voyages et vacances;Cafétéria;Fruits frais;Salle de détente;Programme mieux-être;Programme de référencement' +
          ';' +
          'Conciliation famille-travail;Congés payés;Horaires flexibles;Congés maladie;Horaires d’été;Congés mobiles' +
          ';' +
          'Rabais employé;Rémunération compétitive;Régime retraite;Régime de participation différée aux bénéfices RPDB;PAE;Reconnaissance ancienneté;Remboursement cotisation à une association ou ordre professionnel;Boni sur performance;Stationnement;Abonnement de transport' +
          ';' +
          'Programme de formation et développement;Programme de reconnaissance'
      );
    });
    it('should return null when value is null', () => {
      // given
      const value: AdvantagesInterface = null;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromAdvantages(value);

      // then
      expect(result).toBeNull();
    });
    it('should return null when value is undefined', () => {
      // given
      const value: AdvantagesInterface = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromAdvantages(value);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromWantedPersonalities()', () => {
    it('should return zoho values separated by semicolons when value is a list of wanted personalities', () => {
      // given
      const values: JobOfferWantedPersonality[] = [
        JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_AUTONOMOUS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_INNOVATIVE,
        JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_DECISION_MAKERS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_STRATEGISTS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_FLEXIBLE,
        JobOfferWantedPersonality.WANTED_PERSONALITY_LEADERS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_NEGOTIATORS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_ORGANIZED,
        JobOfferWantedPersonality.WANTED_PERSONALITY_RIGOROUS,
        JobOfferWantedPersonality.WANTED_PERSONALITY_RESOURCEFUL,
        JobOfferWantedPersonality.WANTED_PERSONALITY_TEAMWORK,
        JobOfferWantedPersonality.WANTED_PERSONALITY_SPEAKERS_AT_THE_CONFERENCE,
        JobOfferWantedPersonality.WANTED_PERSONALITY_PONDERATOR,
        JobOfferWantedPersonality.WANTED_PERSONALITY_DIPLOMATS,
      ];

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromWantedPersonalities(values);

      // then
      expect(result).toBe(
        "Communicant;Ambitieux;Autonome;Innovant;Curieux;Décideur;Stratège;Flexible;Leader;Négociateur;Organisé;Rigoureux;Débrouillard;D'équipe;Orateur;Pondérateur;Diplomate"
      );
    });
    it('should return null when value is an empty list', () => {
      // given
      const values: JobOfferWantedPersonality[] = [];

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromWantedPersonalities(values);

      // then
      expect(result).toBeNull();
    });
    it('should return null when value is undefined', () => {
      // given
      const values: JobOfferWantedPersonality[] = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromWantedPersonalities(values);

      // then
      expect(result).toBeNull();
    });
  });

  describe('fromStartDateOfEmploymentAsSoonAsPossible()', () => {
    it('should return Oui when value is true', () => {
      // given
      const value: boolean = true;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromStartDateOfEmploymentAsSoonAsPossible(value);

      // then
      expect(result).toBe('Oui');
    });
    it('should return Non when value is false', () => {
      // given
      const value: boolean = false;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromStartDateOfEmploymentAsSoonAsPossible(value);

      // then
      expect(result).toBe('Non');
    });
    it('should return null when value is an unknown value', () => {
      // given
      const value: boolean = undefined;

      // when
      const result: string = DomainToZohoModelMappingStrategy.fromStartDateOfEmploymentAsSoonAsPossible(value);

      // then
      expect(result).toBeNull();
    });
  });
});

import { ZohoJsonPostRecordsRequestInterface } from '../models/json/zoho-json-post-records-request.interface';
import { ZohoApiModule } from '../zoho-api.service';
import { ZohoJsonToXmlParser } from '../zoho-json-to-xml-parser';

describe('infrastructure/vendors/zoho/ZohoJsonToXmlParser', () => {
  let zohoJsonToXmlParser: ZohoJsonToXmlParser;
  beforeEach(() => {
    zohoJsonToXmlParser = new ZohoJsonToXmlParser();
  });

  describe('toXml()', () => {
    it('should convert json request data to xml', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;
      const zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface = {
        JobOpenings: {
          row: [
            {
              no: '1',
              FL: [
                { val: 'A first val', content: '<span>A content that should be wrapped in CDATA because of span tag</span>' },
                { val: 'A second val', content: 'Another content' },
              ],
            },
          ],
        },
      };

      // when
      const result: string = await zohoJsonToXmlParser.toXml(zohoApiModule, zohoJsonPostRecordsRequest);

      // then
      expect(result).toBe(
        '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' +
          '<JobOpenings>\n' +
          '  <row no="1">\n' +
          '    <FL val="A first val"><![CDATA[<span>A content that should be wrapped in CDATA because of span tag</span>]]></FL>\n' +
          '    <FL val="A second val">Another content</FL>\n' +
          '  </row>\n' +
          '</JobOpenings>'
      );
    });
  });
});

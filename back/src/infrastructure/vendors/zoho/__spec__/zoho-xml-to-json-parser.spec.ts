import { ZohoJsonGetRecordsResponseInterface } from '../models/json/zoho-json-get-records-response.interface';
import { ZohoApiModule } from '../zoho-api.service';
import { ZohoXmlToJsonParser } from '../zoho-xml-to-json-parser';

describe('infrastructure/vendors/zoho/ZohoXmlToJsonParser', () => {
  let zohoXmlToJsonParser: ZohoXmlToJsonParser;
  beforeEach(() => {
    zohoXmlToJsonParser = new ZohoXmlToJsonParser();
  });

  describe('toJson()', () => {
    it('should convert xml response data to json', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;
      const xmlData: string =
        '<?xml version="1.0"?>\n' +
        '<response uri="/recruit/private/xml/JobOpenings/getRecords">\n' +
        '  <result>\n' +
        '    <JobOpenings>\n' +
        '      <row no="1">\n' +
        '        <FL val="A first val"><![CDATA[First value of first row]]></FL>\n' +
        '        <FL val="A second val"><![CDATA[Second value of first row]]></FL>\n' +
        '      </row>\n' +
        '      <row no="2">\n' +
        '        <FL val="A first val"><![CDATA[First value of second row]]></FL>\n' +
        '        <FL val="A second val"><![CDATA[Second value of second row]]></FL>\n' +
        '      </row>\n' +
        '    </JobOpenings>\n' +
        '  </result>\n' +
        '</response>';

      // when
      const result: ZohoJsonGetRecordsResponseInterface = await zohoXmlToJsonParser.toJson(zohoApiModule, xmlData);

      // then
      expect(result).toStrictEqual({
        response: {
          result: {
            JobOpenings: {
              row: [
                {
                  no: '1',
                  FL: [
                    {
                      content: 'First value of first row',
                      val: 'A first val',
                    },
                    {
                      content: 'Second value of first row',
                      val: 'A second val',
                    },
                  ],
                },
                {
                  no: '2',
                  FL: [
                    {
                      content: 'First value of second row',
                      val: 'A first val',
                    },
                    {
                      content: 'Second value of second row',
                      val: 'A second val',
                    },
                  ],
                },
              ],
            },
          },
        },
      });
    });

    it('should return result with empty row when no records', async () => {
      // given
      const zohoApiModule: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;
      const xmlData: string =
        '<?xml version="1.0" encoding="UTF-8" ?>\n' +
        '<response uri="/recruit/private/xml/JobOpenings/getSearchRecords">\n' +
        '    <nodata>\n' +
        '        <code>4422</code>\n' +
        '        <message>There is no data to show</message>\n' +
        '    </nodata>\n' +
        '</response>';

      // when
      const result: ZohoJsonGetRecordsResponseInterface = await zohoXmlToJsonParser.toJson(zohoApiModule, xmlData);

      // then
      expect(result).toStrictEqual({ response: { result: { JobOpenings: { row: [] } } } });
    });
  });
});

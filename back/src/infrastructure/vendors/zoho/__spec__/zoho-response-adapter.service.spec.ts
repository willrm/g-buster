import { ZohoJsonFieldInterface } from '../models/json/zoho-json-field.interface';
import { ZohoModelToDomainMappingStrategy } from '../zoho-model-to-domain-mapping-strategy';
import { ZohoResponseAdapterService } from '../zoho-response-adapter.service';

describe('infrastructure/vendors/zoho/ZohoResponseAdapterService', () => {
  let zohoResponseAdapterService: ZohoResponseAdapterService;

  beforeEach(() => {
    zohoResponseAdapterService = new ZohoResponseAdapterService();
  });

  describe('map()', () => {
    let zohoApiRecordsFields: ZohoJsonFieldInterface[];
    let val1MapMock: jest.Mock;
    let val2MapMock: jest.Mock;
    let val3MapMock: jest.Mock;
    let zohoModelToDomainMappingStrategy: ZohoModelToDomainMappingStrategy;

    beforeEach(() => {
      zohoApiRecordsFields = [
        { val: 'val1', content: '1337' },
        { val: 'val2', content: 'content' },
        { val: 'val3', content: 'No' },
      ];

      val1MapMock = jest.fn();
      val1MapMock.mockReturnValue(1337);

      val2MapMock = jest.fn();
      val2MapMock.mockReturnValue('content mapped');

      val3MapMock = jest.fn();
      val3MapMock.mockReturnValue(false);

      zohoModelToDomainMappingStrategy = {
        val1: {
          target: 'a',
          map: val1MapMock,
        },
        val2: {
          target: 'b.c',
          map: val2MapMock,
        },
        val3: {
          target: 'd',
          map: val3MapMock,
        },
      };
    });

    it('should call map function of mapping strategy for each zoho field content', () => {
      // when
      zohoResponseAdapterService.map<TestTypeInterface>(zohoApiRecordsFields, zohoModelToDomainMappingStrategy);

      // then
      expect(val1MapMock).toHaveBeenCalledWith('1337');
      expect(val2MapMock).toHaveBeenCalledWith('content');
      expect(val3MapMock).toHaveBeenCalledWith('No');
    });

    it('should return an object result of each mapped zoho field content to corresponding target path', () => {
      // when
      const result: TestTypeInterface = zohoResponseAdapterService.map<TestTypeInterface>(zohoApiRecordsFields, zohoModelToDomainMappingStrategy);

      // then
      expect(result).toStrictEqual({
        a: 1337,
        b: {
          c: 'content mapped',
        },
        d: false,
      } as TestTypeInterface);
    });

    it('should ignore zoho fields that are unknown to mapping strategy', () => {
      // given
      zohoApiRecordsFields = [
        { val: 'val1', content: '1337' },
        { val: 'val4', content: 'another content' },
      ];

      // when
      const result: TestTypeInterface = zohoResponseAdapterService.map<TestTypeInterface>(zohoApiRecordsFields, zohoModelToDomainMappingStrategy);

      // then
      expect(result).toStrictEqual({
        a: 1337,
      } as TestTypeInterface);
    });

    it('should ignore mapped zoho field content when null', () => {
      // given
      val1MapMock.mockReturnValue(null);
      val3MapMock.mockReturnValue(null);

      // when
      const result: TestTypeInterface = zohoResponseAdapterService.map<TestTypeInterface>(zohoApiRecordsFields, zohoModelToDomainMappingStrategy);

      // then
      expect(result).toStrictEqual({
        b: {
          c: 'content mapped',
        },
      } as TestTypeInterface);
    });

    it('should return empty object when all fields are unknown to mapping strategy', () => {
      // given
      zohoApiRecordsFields = [
        { val: 'val4', content: 'another content' },
        { val: 'val5', content: 'yet another content' },
      ];

      // when
      const result: TestTypeInterface = zohoResponseAdapterService.map<TestTypeInterface>(zohoApiRecordsFields, zohoModelToDomainMappingStrategy);

      // then
      expect(result).toStrictEqual({} as TestTypeInterface);
    });
  });
});

interface TestTypeInterface {
  a: number;
  b: {
    c: string;
  };
  d: boolean;
  e: string;
}

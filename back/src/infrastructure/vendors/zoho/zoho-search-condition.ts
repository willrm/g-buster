export enum ZohoSearchConditionOperator {
  IS = '=',
  IS_NOT = '<>',
  IS_BEFORE = '<',
  IS_BEFORE_OR_EQUAL = '<=',
  IS_AFTER = '>',
  IS_AFTER_OR_EQUAL = '>=',
}

export interface ZohoSearchCondition {
  field: string;
  operator: ZohoSearchConditionOperator;
  value: string;
}

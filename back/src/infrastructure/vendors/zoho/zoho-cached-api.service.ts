import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { ZohoJsonGetRecordsResponseInterface } from './models/json/zoho-json-get-records-response.interface';
import { ZohoJsonPostRecordsRequestInterface } from './models/json/zoho-json-post-records-request.interface';
import { ZohoApiInterface } from './zoho-api.interface';
import { ZohoApiModule, ZohoApiService, ZohoFileType } from './zoho-api.service';
import { ZohoSearchCondition } from './zoho-search-condition';

@Injectable()
export class ZohoCachedApiService implements ZohoApiInterface {
  constructor(private readonly zohoApiService: ZohoApiService, @Inject(CACHE_MANAGER) private readonly cacheManager: unknown) {}

  async getRecordById(zohoApiModule: ZohoApiModule, id: string): Promise<ZohoJsonGetRecordsResponseInterface> {
    const cacheKey: string = JSON.stringify({ zohoApiModule, id });

    // @ts-ignore
    return this.cacheManager.wrap(cacheKey, () => this.zohoApiService.getRecordById(zohoApiModule, id));
  }

  async getRecords(zohoApiModule: ZohoApiModule): Promise<ZohoJsonGetRecordsResponseInterface> {
    const cacheKey: string = JSON.stringify({ zohoApiModule });

    // @ts-ignore
    return this.cacheManager.wrap(cacheKey, () => this.zohoApiService.getRecords(zohoApiModule));
  }

  async getSearchRecords<T>(zohoApiModule: ZohoApiModule, zohoSearchCondition: ZohoSearchCondition): Promise<ZohoJsonGetRecordsResponseInterface> {
    const cacheKey: string = JSON.stringify({ zohoApiModule, zohoSearchCondition });

    // @ts-ignore
    return this.cacheManager.wrap(cacheKey, () => this.zohoApiService.getSearchRecords(zohoApiModule, zohoSearchCondition));
  }

  async addRecords(zohoApiModule: ZohoApiModule, zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface): Promise<string> {
    return await this.zohoApiService.addRecords(zohoApiModule, zohoJsonPostRecordsRequest);
  }

  async updateRecords(zohoApiModule: ZohoApiModule, id: string, zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface): Promise<string> {
    return await this.zohoApiService.updateRecords(zohoApiModule, id, zohoJsonPostRecordsRequest);
  }

  async changeStatus(zohoApiModule: ZohoApiModule, id: string, status: string): Promise<string> {
    return await this.zohoApiService.changeStatus(zohoApiModule, id, status);
  }

  async associateJobOpening(zohoApiModule: ZohoApiModule, candidateId: string, jobId: string): Promise<string> {
    return await this.zohoApiService.associateJobOpening(zohoApiModule, candidateId, jobId);
  }

  async uploadFile(
    zohoApiModule: ZohoApiModule,
    id: string,
    zohoFileType: ZohoFileType,
    fileAsBase64: string,
    filename: string,
    fileMimeType: string
  ): Promise<string> {
    return await this.zohoApiService.uploadFile(zohoApiModule, id, zohoFileType, fileAsBase64, filename, fileMimeType);
  }
}

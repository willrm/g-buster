import { ZohoXmlRowInterface } from './zoho-xml-row.interface';

export interface ZohoXmlGetRecordsResponseInterface {
  response: {
    attributes: {
      uri: string;
    };
    result: [
      {
        [key: string]: [
          {
            row: ZohoXmlRowInterface[];
          }
        ];
      }
    ];
  };
}

import { ZohoXmlFieldInterface } from './zoho-xml-field.interface';

export interface ZohoXmlRowInterface {
  attributes: { no: string };
  FL: ZohoXmlFieldInterface[];
}

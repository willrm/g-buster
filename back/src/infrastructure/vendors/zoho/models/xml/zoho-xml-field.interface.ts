export interface ZohoXmlFieldInterface {
  characterContent: string;
  attributes: {
    val: string;
  };
}

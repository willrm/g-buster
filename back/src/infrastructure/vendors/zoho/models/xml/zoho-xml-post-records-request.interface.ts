import { ZohoXmlRowInterface } from './zoho-xml-row.interface';

export interface ZohoXmlPostRecordsRequestInterface {
  [key: string]: {
    row: ZohoXmlRowInterface[];
  };
}

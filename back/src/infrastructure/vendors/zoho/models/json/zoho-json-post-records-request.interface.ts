import { ZohoJsonRowInterface } from './zoho-json-row.interface';

export interface ZohoJsonPostRecordsRequestInterface {
  [key: string]: {
    row: ZohoJsonRowInterface[];
  };
}

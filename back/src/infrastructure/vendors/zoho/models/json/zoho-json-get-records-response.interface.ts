import { ZohoJsonRowInterface } from './zoho-json-row.interface';

export interface ZohoJsonGetRecordsResponseInterface {
  response: {
    result: {
      [key: string]: {
        row: ZohoJsonRowInterface[];
      };
    };
  };
}

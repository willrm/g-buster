export interface ZohoJsonFieldInterface {
  val: string;
  content: string;
}

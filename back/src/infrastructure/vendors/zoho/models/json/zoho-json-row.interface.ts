import { ZohoJsonFieldInterface } from './zoho-json-field.interface';

export interface ZohoJsonRowInterface {
  no: string;
  FL: ZohoJsonFieldInterface[];
}

import { CacheModule, HttpModule, Module } from '@nestjs/common';
import { EnvironmentConfigModule } from '../../config/environment-config/environment-config.module';
import { EnvironmentConfigService } from '../../config/environment-config/environment-config.service';
import { ZohoApiService } from './zoho-api.service';
import { ZohoCachedApiService } from './zoho-cached-api.service';
import { ZohoJsonToXmlParser } from './zoho-json-to-xml-parser';
import { ZohoRequestAdapterService } from './zoho-request-adapter.service';
import { ZohoResponseAdapterService } from './zoho-response-adapter.service';
import { ZohoXmlToJsonParser } from './zoho-xml-to-json-parser';

@Module({
  imports: [
    EnvironmentConfigModule,
    HttpModule,
    CacheModule.registerAsync({
      imports: [EnvironmentConfigModule],
      inject: [EnvironmentConfigService],
      useFactory: async (environmentConfigService: EnvironmentConfigService) => ({
        ttl: environmentConfigService.get('ZOHO_API_CACHE_TTL_IN_SECONDS'),
      }),
    }),
  ],
  providers: [ZohoApiService, ZohoRequestAdapterService, ZohoResponseAdapterService, ZohoXmlToJsonParser, ZohoJsonToXmlParser, ZohoCachedApiService],
  exports: [ZohoCachedApiService, ZohoRequestAdapterService, ZohoResponseAdapterService],
})
export class ZohoModule {}

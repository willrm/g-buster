import { Injectable } from '@nestjs/common';
import { get } from 'lodash';
import * as PrismicDOM from 'prismic-dom';
import { Document } from 'prismic-javascript/d.ts/documents';
import { HtmlMarkup } from '../../../domain/type-aliases';

@Injectable()
export class PrismicDocumentAdapterService {
  private readonly EMPTY_STRING: string = '';

  getRichTextAsHtml(document: Document, field: string): HtmlMarkup {
    return !document.data[field] ? this.EMPTY_STRING : PrismicDOM.RichText.asHtml(document.data[field]);
  }

  getValue(document: Document, path: string): string {
    return get(document.data, path, this.EMPTY_STRING);
  }
}

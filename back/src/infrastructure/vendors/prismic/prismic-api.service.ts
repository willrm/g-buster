import { Injectable } from '@nestjs/common';
import * as Prismic from 'prismic-javascript';
import { Document } from 'prismic-javascript/d.ts/documents';
import ResolvedApi from 'prismic-javascript/d.ts/ResolvedApi';
import { ItemNotFoundError } from '../../../domain/common-errors/item-not-found.error';
import { EnvironmentConfigService } from '../../config/environment-config/environment-config.service';

@Injectable()
export class PrismicApiService {
  constructor(private readonly environmentConfigService: EnvironmentConfigService) {}

  async getByUID(type: string, uid: string, lang: string): Promise<Document> {
    const resolvedApi: ResolvedApi = await this.openApiConnection();
    const document: Document = await resolvedApi.getByUID(type, uid, { lang });

    if (!document) {
      return Promise.reject(new ItemNotFoundError(`Document not found with type "${type}", uid "${uid}" and lang "${lang}"`));
    }

    return Promise.resolve(document);
  }

  async getSingle(type: string, lang: string): Promise<Document> {
    const resolvedApi: ResolvedApi = await this.openApiConnection();
    const document: Document = await resolvedApi.getSingle(type, { lang });

    if (!document) {
      return Promise.reject(new ItemNotFoundError(`Document not found with type "${type}" and lang "${lang}"`));
    }

    return Promise.resolve(document);
  }

  // PRIVATE METHODS

  private async openApiConnection(): Promise<ResolvedApi> {
    return await Prismic.api(this.environmentConfigService.get('PRISMIC_API_URL'), {
      accessToken: this.environmentConfigService.get('PRISMIC_API_ACCESS_TOKEN'),
    });
  }
}

import { Module } from '@nestjs/common';
import { EnvironmentConfigModule } from '../../config/environment-config/environment-config.module';
import { PrismicApiService } from './prismic-api.service';
import { PrismicDocumentAdapterService } from './prismic-document-adapter.service';

@Module({
  imports: [EnvironmentConfigModule],
  providers: [PrismicApiService, PrismicDocumentAdapterService],
  exports: [PrismicApiService, PrismicDocumentAdapterService],
})
export class PrismicModule {}

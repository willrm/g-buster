import { Document } from 'prismic-javascript/d.ts/documents';
import { PrismicDocumentAdapterService } from '../prismic-document-adapter.service';

describe('infrastructure/vendors/prismic/PrismicDocumentAdapterService', () => {
  let prismicDocumentAdapterService: PrismicDocumentAdapterService;
  beforeEach(() => {
    prismicDocumentAdapterService = new PrismicDocumentAdapterService();
  });

  describe('getRichTextAsHtml()', () => {
    it('should return field as html markup', () => {
      // given
      const document: Document = {
        data: {
          title: [
            {
              type: 'heading1',
              text: 'An awesome title',
              spans: [],
            },
          ],
        },
      } as Document;

      // when
      const result: string = prismicDocumentAdapterService.getRichTextAsHtml(document, 'title');

      // then
      const expected: string = '<h1>An awesome title</h1>';
      expect(result).toBe(expected);
    });

    it('should return an empty string when field does not exists in document', () => {
      // given
      const document: Document = {
        data: {
          title: [],
        },
      } as Document;

      // when
      const result: string = prismicDocumentAdapterService.getRichTextAsHtml(document, 'title');

      // then
      expect(result).toBe('');
    });
  });

  describe('getValue()', () => {
    it('should return field value following path', () => {
      // given
      const document: Document = {
        data: {
          logo: {
            url: 'https://an-awesome.png',
          },
        },
      } as Document;

      // when
      const result: string = prismicDocumentAdapterService.getValue(document, 'logo.url');

      // then
      expect(result).toBe('https://an-awesome.png');
    });

    it('should return an empty string when field does not exists in document', () => {
      // given
      const document: Document = {
        data: {
          logo: {},
        },
      } as Document;

      // when
      const result: string = prismicDocumentAdapterService.getValue(document, 'logo.url');

      // then
      expect(result).toBe('');
    });
  });
});

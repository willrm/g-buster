import * as Prismic from 'prismic-javascript';
import { Document } from 'prismic-javascript/d.ts/documents';
import ResolvedApi from 'prismic-javascript/d.ts/ResolvedApi';
import { ItemNotFoundError } from '../../../../domain/common-errors/item-not-found.error';
import { EnvironmentConfigService } from '../../../config/environment-config/environment-config.service';
import { PrismicApiService } from '../prismic-api.service';

jest.mock('prismic-javascript');

describe('infrastructure/vendors/prismic/PrismicApiService', () => {
  let prismicApiService: PrismicApiService;
  let mockEnvironmentConfigService: EnvironmentConfigService;
  let mockResolvedApi: ResolvedApi;
  let type: string;
  let lang: string;
  beforeEach(() => {
    mockEnvironmentConfigService = {} as EnvironmentConfigService;
    mockEnvironmentConfigService.get = jest.fn();
    prismicApiService = new PrismicApiService(mockEnvironmentConfigService);
    mockResolvedApi = {} as ResolvedApi;
    mockResolvedApi.getByUID = jest.fn();
    mockResolvedApi.getSingle = jest.fn();
    (Prismic.api as jest.Mock).mockReturnValue(mockResolvedApi);

    type = 'a-type';
    lang = 'a-lang';
  });

  describe('getByUID()', () => {
    let uid: string;
    beforeEach(() => {
      uid = 'a-uid';

      (mockResolvedApi.getByUID as jest.Mock).mockReturnValue(Promise.resolve({} as Document));
    });

    it('should open prismic connection with endpoint and access token', async () => {
      // given
      const expectedPrismicApiUrl: string = 'https://api';
      const expectedPrismicApiAccessToken: string = 'super-secret-access-token';
      (mockEnvironmentConfigService.get as jest.Mock).mockReturnValueOnce(expectedPrismicApiUrl).mockReturnValueOnce(expectedPrismicApiAccessToken);

      // when
      await prismicApiService.getByUID(type, uid, lang);

      // then
      expect(Prismic.api).toHaveBeenCalledWith(expectedPrismicApiUrl, {
        accessToken: expectedPrismicApiAccessToken,
      });
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('PRISMIC_API_URL');
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('PRISMIC_API_ACCESS_TOKEN');
    });

    it('should call prismic api with type, uid and lang', async () => {
      // when
      await prismicApiService.getByUID(type, uid, lang);

      // then
      expect(mockResolvedApi.getByUID).toHaveBeenCalledWith(type, uid, {
        lang,
      });
    });

    it('should return found document', async () => {
      // given
      const expected: Document = {} as Document;
      (mockResolvedApi.getByUID as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: Document = await prismicApiService.getByUID(type, uid, lang);

      // then
      expect(result).toBe(expected);
    });

    it('should throw exception when document not found', async () => {
      // given
      (mockResolvedApi.getByUID as jest.Mock).mockReturnValue(Promise.resolve(null));

      // when
      const result: Promise<Document> = prismicApiService.getByUID(type, uid, lang);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('Document not found with type "a-type", uid "a-uid" and lang "a-lang"'));
    });
  });

  describe('getSingle()', () => {
    beforeEach(() => {
      (mockResolvedApi.getSingle as jest.Mock).mockReturnValue(Promise.resolve({} as Document));
    });

    it('should open prismic connection with endpoint and access token', async () => {
      // given
      const expectedPrismicApiUrl: string = 'https://api';
      const expectedPrismicApiAccessToken: string = 'super-secret-access-token';
      (mockEnvironmentConfigService.get as jest.Mock).mockReturnValueOnce(expectedPrismicApiUrl).mockReturnValueOnce(expectedPrismicApiAccessToken);

      // when
      await prismicApiService.getSingle(type, lang);

      // then
      expect(Prismic.api).toHaveBeenCalledWith(expectedPrismicApiUrl, {
        accessToken: expectedPrismicApiAccessToken,
      });
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('PRISMIC_API_URL');
      expect(mockEnvironmentConfigService.get).toHaveBeenCalledWith('PRISMIC_API_ACCESS_TOKEN');
    });

    it('should call prismic api with type, uid and lang', async () => {
      // when
      await prismicApiService.getSingle(type, lang);

      // then
      expect(mockResolvedApi.getSingle).toHaveBeenCalledWith(type, { lang });
    });

    it('should return found document', async () => {
      // given
      const expected: Document = {} as Document;
      (mockResolvedApi.getSingle as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: Document = await prismicApiService.getSingle(type, lang);

      // then
      expect(result).toBe(expected);
    });

    it('should throw exception when document not found', async () => {
      // given
      (mockResolvedApi.getSingle as jest.Mock).mockReturnValue(Promise.resolve(null));

      // when
      const result: Promise<Document> = prismicApiService.getSingle(type, lang);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('Document not found with type "a-type" and lang "a-lang"'));
    });
  });
});

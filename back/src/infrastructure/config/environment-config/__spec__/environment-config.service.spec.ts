import { repeat } from 'lodash';
import { EnvironmentConfigError } from '../environment-config.error';
import { EnvironmentConfigService } from '../environment-config.service';
import ProcessEnv = NodeJS.ProcessEnv;

describe('infrastructure/config/environment-config/EnvironmentConfigService', () => {
  beforeEach(() => {
    const env: ProcessEnv = {
      PORT: '1111',
      PRISMIC_API_URL: 'https://an-uri',
      PRISMIC_API_ACCESS_TOKEN: 'a-token',
      ZOHO_API_URL: 'https://an-uri',
      ZOHO_API_ACCESS_TOKEN: 'a-token',
      DATABASE_TYPE: 'database-type',
      DATABASE_HOST: 'database-host',
      DATABASE_PORT: '1234',
      DATABASE_USERNAME: 'database-username',
      DATABASE_PASSWORD: 'database-password',
      DATABASE_NAME: 'database-name',
      GBUSTER_ADMIN_EMAIL_ADDRESSES: 'admin@example.org',
      GBUSTER_SESSION_SECRET: 'ffffffffffffffffffffffffffffffff',
      AUTH0_DOMAIN: 'subdomain.auth0.com',
      AUTH0_CLIENT_ID: 'auth0-client-id',
      AUTH0_CLIENT_SECRET: 'auth0-client-secret',
      AUTH0_CALLBACK_URL: 'http://auth0.callback/url',
      ZOHO_API_CACHE_TTL_IN_SECONDS: '60',
      GENIUM360_API_URL: 'https://www.genium360.api',
      GENIUM360_API_ACCESS_TOKEN: 'genium360-token',
    } as ProcessEnv;

    process.env = {
      ...process.env,
      ...env,
    };
  });

  describe('constructor()', () => {
    it('should allow unknown variables', () => {
      // given
      process.env.UNKNOWN = 'any-value';

      // when
      const result = () => new EnvironmentConfigService();

      // then
      expect(result).not.toThrow();
    });

    describe('PORT', () => {
      it('should fail when not a number', () => {
        // given
        process.env.PORT = 'not-a-number';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "PORT" must be a number'));
      });
      it('should be defaulted to 3001', () => {
        // given
        process.env.PORT = undefined;

        // when
        const result: EnvironmentConfigService = new EnvironmentConfigService();

        // then
        expect(result.get('PORT')).toBe(3001);
      });
    });

    describe('PRISMIC_API_URL', () => {
      it('should fail when empty', () => {
        // given
        process.env.PRISMIC_API_URL = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "PRISMIC_API_URL" is not allowed to be empty'));
      });
      it('should fail when invalid uri', () => {
        // given
        process.env.PRISMIC_API_URL = 'not-an-uri';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(
          new EnvironmentConfigError('Config validation error: "PRISMIC_API_URL" must be a valid uri with a scheme matching the https pattern')
        );
      });
      it('should fail when uri does not start with https', () => {
        // given
        process.env.PRISMIC_API_URL = 'http://unsecured-api';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(
          new EnvironmentConfigError('Config validation error: "PRISMIC_API_URL" must be a valid uri with a scheme matching the https pattern')
        );
      });
    });

    describe('PRISMIC_API_ACCESS_TOKEN', () => {
      it('should fail when empty', () => {
        // given
        process.env.PRISMIC_API_ACCESS_TOKEN = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "PRISMIC_API_ACCESS_TOKEN" is not allowed to be empty'));
      });
    });

    describe('ZOHO_API_URL', () => {
      it('should fail when empty', () => {
        // given
        process.env.ZOHO_API_URL = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "ZOHO_API_URL" is not allowed to be empty'));
      });
      it('should fail when invalid uri', () => {
        // given
        process.env.ZOHO_API_URL = 'not-an-uri';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(
          new EnvironmentConfigError('Config validation error: "ZOHO_API_URL" must be a valid uri with a scheme matching the https pattern')
        );
      });
      it('should fail when uri does not start with https', () => {
        // given
        process.env.ZOHO_API_URL = 'http://unsecured-api';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(
          new EnvironmentConfigError('Config validation error: "ZOHO_API_URL" must be a valid uri with a scheme matching the https pattern')
        );
      });
    });

    describe('ZOHO_API_ACCESS_TOKEN', () => {
      it('should fail when empty', () => {
        // given
        process.env.ZOHO_API_ACCESS_TOKEN = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "ZOHO_API_ACCESS_TOKEN" is not allowed to be empty'));
      });
    });

    describe('ZOHO_API_CACHE_TTL_IN_SECONDS', () => {
      it('should fail when not a number', () => {
        // given
        process.env.ZOHO_API_CACHE_TTL_IN_SECONDS = 'not-a-number';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "ZOHO_API_CACHE_TTL_IN_SECONDS" must be a number'));
      });
      it('should be defaulted to 60', () => {
        // given
        process.env.ZOHO_API_CACHE_TTL_IN_SECONDS = undefined;

        // when
        const result: EnvironmentConfigService = new EnvironmentConfigService();

        // then
        expect(result.get('ZOHO_API_CACHE_TTL_IN_SECONDS')).toBe(60);
      });
    });

    describe('DATABASE_TYPE', () => {
      it('should fail when empty', () => {
        // given
        process.env.DATABASE_TYPE = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "DATABASE_TYPE" is not allowed to be empty'));
      });
    });

    describe('DATABASE_NAME', () => {
      it('should fail when empty', () => {
        // given
        process.env.DATABASE_NAME = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "DATABASE_NAME" is not allowed to be empty'));
      });
    });

    describe('when DATABASE_TYPE is postgres', () => {
      beforeEach(() => {
        process.env.DATABASE_TYPE = 'postgres';
      });

      describe('DATABASE_HOST', () => {
        it('should fail when empty', () => {
          // given
          process.env.DATABASE_HOST = '';

          // when
          const result = () => new EnvironmentConfigService();

          // then
          expect(result).toThrow(new EnvironmentConfigError('Config validation error: "DATABASE_HOST" is not allowed to be empty'));
        });
      });

      describe('DATABASE_PORT', () => {
        it('should fail when not a number', () => {
          // given
          process.env.DATABASE_PORT = 'not-a-number';

          // when
          const result = () => new EnvironmentConfigService();

          // then
          expect(result).toThrow(new EnvironmentConfigError('Config validation error: "DATABASE_PORT" must be a number'));
        });
      });

      describe('DATABASE_USERNAME', () => {
        it('should fail when empty', () => {
          // given
          process.env.DATABASE_USERNAME = '';

          // when
          const result = () => new EnvironmentConfigService();

          // then
          expect(result).toThrow(new EnvironmentConfigError('Config validation error: "DATABASE_USERNAME" is not allowed to be empty'));
        });
      });

      describe('DATABASE_PASSWORD', () => {
        it('should fail when empty', () => {
          // given
          process.env.DATABASE_PASSWORD = '';

          // when
          const result = () => new EnvironmentConfigService();

          // then
          expect(result).toThrow(new EnvironmentConfigError('Config validation error: "DATABASE_PASSWORD" is not allowed to be empty'));
        });
      });
    });

    describe('when DATABASE_TYPE is sqlite', () => {
      beforeEach(() => {
        process.env.DATABASE_TYPE = 'sqlite';
      });

      describe('DATABASE_HOST', () => {
        it('should be optional', () => {
          // given
          process.env.DATABASE_HOST = undefined;

          // when
          const result = () => new EnvironmentConfigService();

          // then
          expect(result).not.toThrow();
        });
      });

      describe('DATABASE_PORT', () => {
        it('should be optional', () => {
          // given
          process.env.DATABASE_PORT = undefined;

          // when
          const result = () => new EnvironmentConfigService();

          // then
          expect(result).not.toThrow();
        });
      });

      describe('DATABASE_USERNAME', () => {
        it('should be optional', () => {
          // given
          process.env.DATABASE_USERNAME = undefined;

          // when
          const result = () => new EnvironmentConfigService();

          // then
          expect(result).not.toThrow();
        });
      });

      describe('DATABASE_PASSWORD', () => {
        it('should be optional', () => {
          // given
          process.env.DATABASE_PASSWORD = undefined;

          // when
          const result = () => new EnvironmentConfigService();

          // then
          expect(result).not.toThrow();
        });
      });
    });

    describe('GBUSTER_ADMIN_EMAIL_ADDRESSES', () => {
      it('should fail when empty', () => {
        // given
        process.env.GBUSTER_ADMIN_EMAIL_ADDRESSES = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "GBUSTER_ADMIN_EMAIL_ADDRESSES" is not allowed to be empty'));
      });
    });

    describe('GBUSTER_SESSION_SECRET', () => {
      it('should fail when empty', () => {
        // given
        process.env.GBUSTER_SESSION_SECRET = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "GBUSTER_SESSION_SECRET" is not allowed to be empty'));
      });
      it('should fail when less than 32 characters', () => {
        // given
        process.env.GBUSTER_SESSION_SECRET = repeat('*', 31);

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(
          new EnvironmentConfigError('Config validation error: "GBUSTER_SESSION_SECRET" length must be at least 32 characters long')
        );
      });
      it('should fail when not having hexadecimal characters', () => {
        // given
        process.env.GBUSTER_SESSION_SECRET = repeat('X', 32);

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(
          new EnvironmentConfigError('Config validation error: "GBUSTER_SESSION_SECRET" must only contain hexadecimal characters')
        );
      });
    });

    describe('AUTH0_DOMAIN', () => {
      it('should fail when empty', () => {
        // given
        process.env.AUTH0_DOMAIN = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "AUTH0_DOMAIN" is not allowed to be empty'));
      });
    });

    describe('AUTH0_CLIENT_ID', () => {
      it('should fail when empty', () => {
        // given
        process.env.AUTH0_CLIENT_ID = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "AUTH0_CLIENT_ID" is not allowed to be empty'));
      });
    });

    describe('AUTH0_CLIENT_SECRET', () => {
      it('should fail when empty', () => {
        // given
        process.env.AUTH0_CLIENT_SECRET = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "AUTH0_CLIENT_SECRET" is not allowed to be empty'));
      });
    });

    describe('AUTH0_CALLBACK_URL', () => {
      it('should fail when empty', () => {
        // given
        process.env.AUTH0_CALLBACK_URL = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "AUTH0_CALLBACK_URL" is not allowed to be empty'));
      });
      it('should fail when invalid uri', () => {
        // given
        process.env.AUTH0_CALLBACK_URL = 'not-an-uri';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "AUTH0_CALLBACK_URL" must be a valid uri'));
      });
    });

    describe('GENIUM360_API_URL', () => {
      it('should fail when empty', () => {
        // given
        process.env.GENIUM360_API_URL = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "GENIUM360_API_URL" is not allowed to be empty'));
      });
      it('should fail when invalid uri', () => {
        // given
        process.env.GENIUM360_API_URL = 'not-an-uri';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(
          new EnvironmentConfigError('Config validation error: "GENIUM360_API_URL" must be a valid uri with a scheme matching the https pattern')
        );
      });
      it('should fail when uri does not start with https', () => {
        // given
        process.env.GENIUM360_API_URL = 'http://unsecured-api';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(
          new EnvironmentConfigError('Config validation error: "GENIUM360_API_URL" must be a valid uri with a scheme matching the https pattern')
        );
      });
    });

    describe('GENIUM360_API_ACCESS_TOKEN', () => {
      it('should fail when empty', () => {
        // given
        process.env.GENIUM360_API_ACCESS_TOKEN = '';

        // when
        const result = () => new EnvironmentConfigService();

        // then
        expect(result).toThrow(new EnvironmentConfigError('Config validation error: "GENIUM360_API_ACCESS_TOKEN" is not allowed to be empty'));
      });
    });
  });

  describe('get()', () => {
    it('should return variable from process.env', () => {
      // given
      const expected: string = 'any value';
      process.env.TEST_ENV_VAR = expected;
      const environmentConfigService: EnvironmentConfigService = new EnvironmentConfigService();

      // when
      const result: string = environmentConfigService.get('TEST_ENV_VAR');

      // then
      expect(result).toBe(expected);
    });
  });
});

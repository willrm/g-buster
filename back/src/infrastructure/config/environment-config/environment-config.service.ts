import * as Joi from '@hapi/joi';
import { ValidationResult } from '@hapi/joi';
import { Injectable } from '@nestjs/common';
import { EnvironmentConfigError } from './environment-config.error';

export interface EnvironmentConfig {
  [key: string]: string;
}

@Injectable()
export class EnvironmentConfigService {
  private readonly environmentConfig: EnvironmentConfig;

  constructor() {
    this.environmentConfig = EnvironmentConfigService.validateInput({ ...process.env });
  }

  private static validateInput(environmentConfig: EnvironmentConfig): EnvironmentConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      PORT: Joi.number().default(3001),
      PRISMIC_API_URL: Joi.string()
        .uri({ scheme: 'https' })
        .required(),
      PRISMIC_API_ACCESS_TOKEN: Joi.string().required(),
      ZOHO_API_URL: Joi.string()
        .uri({ scheme: 'https' })
        .required(),
      ZOHO_API_ACCESS_TOKEN: Joi.string().required(),
      ZOHO_API_CACHE_TTL_IN_SECONDS: Joi.number().default(60),
      DATABASE_TYPE: Joi.string().required(),
      DATABASE_HOST: Joi.string().when('DATABASE_TYPE', { is: 'sqlite', then: Joi.optional(), otherwise: Joi.required() }),
      DATABASE_PORT: Joi.number().when('DATABASE_TYPE', { is: 'sqlite', then: Joi.optional(), otherwise: Joi.required() }),
      DATABASE_USERNAME: Joi.string().when('DATABASE_TYPE', { is: 'sqlite', then: Joi.optional(), otherwise: Joi.required() }),
      DATABASE_PASSWORD: Joi.string().when('DATABASE_TYPE', { is: 'sqlite', then: Joi.optional(), otherwise: Joi.required() }),
      DATABASE_NAME: Joi.string().required(),
      GBUSTER_ADMIN_EMAIL_ADDRESSES: Joi.string().required(),
      GBUSTER_SESSION_SECRET: Joi.string()
        .required()
        .min(32)
        .hex(),
      AUTH0_DOMAIN: Joi.string().required(),
      AUTH0_CLIENT_ID: Joi.string().required(),
      AUTH0_CLIENT_SECRET: Joi.string().required(),
      AUTH0_CALLBACK_URL: Joi.string()
        .required()
        .uri(),
      GENIUM360_API_URL: Joi.string()
        .uri({ scheme: 'https' })
        .required(),
      GENIUM360_API_ACCESS_TOKEN: Joi.string().required(),
    }).unknown(true);

    const { error, value: validatedEnvironmentConfig }: ValidationResult = envVarsSchema.validate(environmentConfig);
    if (error) {
      throw new EnvironmentConfigError(`Config validation error: ${error.message}`);
    }

    return validatedEnvironmentConfig;
  }

  get(key: string): string {
    return this.environmentConfig[key];
  }
}

import { ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { UserRole } from '../../../../domain/user/user-enums';
import { UserInterface } from '../../../../domain/user/user.interface';
import { AuthenticatedWithRolesGuard } from '../authenticated-with-roles.guard';

describe('infrastructure/config/authentication/AuthenticatedWithRolesGuard', () => {
  let authenticatedWithRolesGuard: AuthenticatedWithRolesGuard;
  let mockReflector: Reflector;
  let mockExecutionContext: ExecutionContext;
  let mockRequest: Request;

  beforeEach(() => {
    mockReflector = {} as Reflector;
    mockReflector.get = jest.fn();
    authenticatedWithRolesGuard = new AuthenticatedWithRolesGuard(mockReflector);

    mockRequest = {} as Request;
    mockRequest.session = {} as CookieSessionInterfaces.CookieSessionObject;
    mockExecutionContext = {
      switchToHttp: () => ({
        getRequest: () => mockRequest,
      }),
    } as ExecutionContext;
    mockExecutionContext.getHandler = jest.fn();
  });

  describe('canActivate()', () => {
    it('should return false when no user in session', () => {
      // given
      mockRequest.session.user = null;

      // when
      const result: boolean = authenticatedWithRolesGuard.canActivate(mockExecutionContext);

      // then
      expect(result).toBe(false);
    });

    it('should return true when no specific roles defined', () => {
      // given
      mockRequest.session.user = { id: '123456' } as UserInterface;

      const someExecutionContextHandler = () => 'something';
      (mockExecutionContext.getHandler as jest.Mock).mockReturnValue(someExecutionContextHandler);
      (mockReflector.get as jest.Mock).mockReturnValue([]);

      // when
      const result: boolean = authenticatedWithRolesGuard.canActivate(mockExecutionContext);

      // then
      expect(mockReflector.get).toHaveBeenCalledWith('userRoles', someExecutionContextHandler);
      expect(result).toBe(true);
    });

    it('should return true when user has the defined role', () => {
      // given
      mockRequest.session.user = { id: '123456', role: UserRole.COMPANY } as UserInterface;
      (mockReflector.get as jest.Mock).mockReturnValue([UserRole.CANDIDATE, UserRole.COMPANY]);

      // when
      const result: boolean = authenticatedWithRolesGuard.canActivate(mockExecutionContext);

      // then
      expect(result).toBe(true);
    });

    it('should return false when user does not have the defined role', () => {
      // given
      mockRequest.session.user = { id: '123456', role: UserRole.ADMIN } as UserInterface;
      (mockReflector.get as jest.Mock).mockReturnValue([UserRole.CANDIDATE, UserRole.COMPANY]);

      // when
      const result: boolean = authenticatedWithRolesGuard.canActivate(mockExecutionContext);

      // then
      expect(result).toBe(false);
    });
  });
});

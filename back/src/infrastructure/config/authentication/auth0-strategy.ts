import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtraVerificationParams, Profile, Strategy } from 'passport-auth0';
import { EnvironmentConfigService } from '../environment-config/environment-config.service';

@Injectable()
export class Auth0Strategy extends PassportStrategy(Strategy) {
  static readonly AUTH0_STRATEGY_NAME: string = 'auth0';
  constructor(private readonly environmentConfigService: EnvironmentConfigService) {
    super(
      {
        domain: environmentConfigService.get('AUTH0_DOMAIN'),
        clientID: environmentConfigService.get('AUTH0_CLIENT_ID'),
        clientSecret: environmentConfigService.get('AUTH0_CLIENT_SECRET'),
        callbackURL: environmentConfigService.get('AUTH0_CALLBACK_URL'),
        scope: 'openid email profile',
      },
      (
        accessToken: string,
        refreshToken: string,
        extraParams: ExtraVerificationParams,
        profile: Profile,
        done: (error: unknown, user?: unknown, info?: unknown) => void
      ) => {
        return done(null, profile);
      }
    );
  }
}

import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { CookieSessionModule } from 'nestjs-cookie-session';
import { EnvironmentConfigModule } from '../environment-config/environment-config.module';
import { EnvironmentConfigService } from '../environment-config/environment-config.service';
import { Auth0Strategy } from './auth0-strategy';

@Module({
  imports: [
    EnvironmentConfigModule,
    CookieSessionModule.forRootAsync({
      imports: [EnvironmentConfigModule],
      inject: [EnvironmentConfigService],
      useFactory: (environmentConfigService: EnvironmentConfigService) => ({
        session: { secret: environmentConfigService.get('GBUSTER_SESSION_SECRET') },
      }),
    }),
    PassportModule.register({}),
  ],
  providers: [Auth0Strategy],
})
export class AuthenticationModule {}

import { CanActivate, ExecutionContext, Injectable, SetMetadata } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { isEmpty } from 'lodash';
import { UserRole } from '../../../domain/user/user-enums';
import { UserInterface } from '../../../domain/user/user.interface';

@Injectable()
export class AuthenticatedWithRolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(executionContext: ExecutionContext): boolean {
    const request: Request = executionContext.switchToHttp().getRequest();
    const currentUser: UserInterface = request.session.user;
    if (isEmpty(currentUser)) {
      return false;
    }

    const userRoles: UserRole[] = this.reflector.get<UserRole[]>('userRoles', executionContext.getHandler());

    return isEmpty(userRoles) ? true : userRoles.includes(currentUser.role);
  }
}

// tslint:disable-next-line:variable-name
export const UserRoles = (...userRoles: UserRole[]) => SetMetadata('userRoles', userRoles);

import { INestApplication, Injectable } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { OpenAPIObject } from '@nestjs/swagger/dist/interfaces';
import { EnvironmentConfigService } from '../environment-config/environment-config.service';

@Injectable()
export class SwaggerService {
  activate(app: INestApplication): void {
    const options: Omit<OpenAPIObject, 'components' | 'paths'> = new DocumentBuilder()
      .setTitle('G-Buster API')
      .setDescription('The G-Buster project API')
      .setVersion(app.get(EnvironmentConfigService).get('npm_package_version'))
      .build();
    const document: OpenAPIObject = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('api/doc', app, document);
  }
}

import { Module } from '@nestjs/common';
import { BatchModule } from './batch/batch.module';
import { RestModule } from './rest/rest.module';

@Module({
  imports: [RestModule, BatchModule],
})
export class AppModule {}

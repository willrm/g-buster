import { DynamicModule, Module } from '@nestjs/common';
import { AnonymizedCompanyAdapter } from '../../domain/company/anonymized-company-adapter';
import { AnonymizedJobOfferAdapter } from '../../domain/job-offer/anonymized-job-offer-adapter';
import { JobOfferWithCompanyAdapter } from '../../use_cases/adapters/job-offer-with-company-adapter';
import { ApplyToJobOffer } from '../../use_cases/apply-to-job-offer';
import { ArchiveExpiredJobOffers } from '../../use_cases/archive-expired-job-offers';
import { CleanExpiredTemporaryFiles } from '../../use_cases/clean-expired-temporary-files';
import { CreateNewCompany } from '../../use_cases/create-new-company';
import { CreateNewJobOffer } from '../../use_cases/create-new-job-offer';
import { CreateTemporaryFile } from '../../use_cases/create-temporary-file';
import { GetCompanies } from '../../use_cases/get-companies';
import { GetCurrentUser } from '../../use_cases/get-current-user';
import { GetDummyPage } from '../../use_cases/get-dummy-page';
import { GetGeniusProducts } from '../../use_cases/get-genius-products';
import { GetI18nMessagesForLang } from '../../use_cases/get-i18n-messages-for-lang';
import { GetLayout } from '../../use_cases/get-layout';
import { GetPublishedCompanyById } from '../../use_cases/get-published-company-by-id';
import { GetPublishedJobOfferById } from '../../use_cases/get-published-job-offer-by-id';
import { GetPublishedJobOffers } from '../../use_cases/get-published-job-offers';
import { GetUserJobOfferById } from '../../use_cases/get-user-job-offer-by-id';
import { GetUserJobOffers } from '../../use_cases/get-user-job-offers';
import { LogInUser } from '../../use_cases/log-in-user';
import { PublishCompany } from '../../use_cases/publish-company';
import { UpdateDraftJobOffer } from '../../use_cases/update-draft-job-offer';
import { UpdateUnpublishedCompany } from '../../use_cases/update-unpublished-company';
import { ValidateCompany } from '../../use_cases/validate-company';
import { ValidateJobOffer } from '../../use_cases/validate-job-offer';
import { DatabaseCompanyRepository } from '../repositories/database-company.repository';
import { DatabaseTemporaryFileRepository } from '../repositories/database-temporary-file.repository';
import { FileI18nRepository } from '../repositories/file-i18n.repository';
import { Genium360GeniusProductRepository } from '../repositories/genium360-genius-product.repository';
import { LocalUserRepository } from '../repositories/local-user.repository';
import { PrismicDummyPageRepository } from '../repositories/prismic-dummy-page.repository';
import { PrismicFooterRepository } from '../repositories/prismic-footer.repository';
import { PrismicHeaderRepository } from '../repositories/prismic-header.repository';
import { RepositoriesModule } from '../repositories/repositories.module';
import { ZohoCandidateRepository } from '../repositories/zoho-candidate.repository';
import { ZohoJobOfferRepository } from '../repositories/zoho-job-offer.repository';
import { UseCaseProxy } from './use-case-proxy';

@Module({
  imports: [RepositoriesModule],
})
export class ProxyServicesDynamicModule {
  static CREATE_NEW_COMPANY_PROXY_SERVICE: string = 'CreateNewCompanyProxyService';
  static VALIDATE_COMPANY_PROXY_SERVICE: string = 'ValidateCompanyProxyService';
  static GET_PUBLISHED_COMPANY_BY_ID_PROXY_SERVICE: string = 'GetPublishedCompanyByIdProxyService';
  static GET_DUMMY_PAGE_PROXY_SERVICE: string = 'GetDummyPageProxyService';
  static GET_I18N_MESSAGES_FOR_LANG_PROXY_SERVICE: string = 'GetI18nMessagesForLangProxyService';
  static GET_LAYOUT_PROXY_SERVICE: string = 'GetLayoutProxyService';
  static CREATE_TEMPORARY_IMAGE_PROXY_SERVICE: string = 'CreateTemporaryImageProxyService';
  static GET_USER_JOB_OFFERS_PROXY_SERVICE: string = 'GetUserJobOffersProxyService';
  static GET_USER_JOB_OFFER_BY_ID_PROXY_SERVICE: string = 'GetUserJobOfferByIdProxyService';
  static CREATE_NEW_JOB_OFFER_PROXY_SERVICE: string = 'CreateNewJobOfferProxyService';
  static VALIDATE_JOB_OFFER_PROXY_SERVICE: string = 'ValidateJobOfferProxyService';
  static UPDATE_DRAFT_JOB_OFFER_PROXY_SERVICE: string = 'UpdateDraftJobOfferProxyService';
  static CLEAN_EXPIRED_TEMPORARY_FILES_PROXY_SERVICE: string = 'CleanExpiredTemporaryFilesProxyService';
  static ARCHIVE_EXPIRED_JOB_OFFERS_PROXY_SERVICE: string = 'ArchiveExpiredJobOffersProxyService';
  static LOG_IN_USER_PROXY_SERVICE: string = 'LogInUserProxyService';
  static GET_CURRENT_USER_PROXY_SERVICE: string = 'GetCurrentUserProxyService';
  static GET_PUBLISHED_JOB_OFFERS_PROXY_SERVICE: string = 'GetPublishedJobOffersProxyService';
  static GET_PUBLISHED_JOB_OFFER_BY_ID_PROXY_SERVICE: string = 'GetPublishedJobOfferByIdProxyService';
  static GET_COMPANIES_PROXY_SERVICE: string = 'GetCompaniesProxyService';
  static APPLY_TO_JOB_OFFER_PROXY_SERVICE: string = 'ApplyToJobOfferProxyService';
  static PUBLISH_COMPANY_PROXY_SERVICE: string = 'PublishCompanyProxyService';
  static UPDATE_UNPUBLISHED_COMPANY_PROXY_SERVICE: string = 'UpdateUnpublishedCompanyProxyService';
  static GET_GENIUS_PRODUCTS_PROXY_SERVICE: string = 'GetGeniusProductsProxyService';

  static register(): DynamicModule {
    const anonymizedCompanyAdapter: AnonymizedCompanyAdapter = new AnonymizedCompanyAdapter();
    const jobOfferWithCompanyAdapter: JobOfferWithCompanyAdapter = new JobOfferWithCompanyAdapter(
      new AnonymizedJobOfferAdapter(),
      anonymizedCompanyAdapter
    );

    return {
      module: ProxyServicesDynamicModule,
      providers: [
        {
          inject: [LocalUserRepository, DatabaseCompanyRepository, DatabaseTemporaryFileRepository],
          provide: ProxyServicesDynamicModule.CREATE_NEW_COMPANY_PROXY_SERVICE,
          useFactory: (
            localUserRepository: LocalUserRepository,
            databaseCompanyRepository: DatabaseCompanyRepository,
            databaseTemporaryFileRepository: DatabaseTemporaryFileRepository
          ) => new UseCaseProxy(new CreateNewCompany(localUserRepository, databaseCompanyRepository, databaseTemporaryFileRepository)),
        },
        {
          inject: [LocalUserRepository, DatabaseCompanyRepository, DatabaseTemporaryFileRepository],
          provide: ProxyServicesDynamicModule.VALIDATE_COMPANY_PROXY_SERVICE,
          useFactory: (
            localUserRepository: LocalUserRepository,
            databaseCompanyRepository: DatabaseCompanyRepository,
            databaseTemporaryFileRepository: DatabaseTemporaryFileRepository
          ) => new UseCaseProxy(new ValidateCompany(localUserRepository, databaseCompanyRepository, databaseTemporaryFileRepository)),
        },
        {
          inject: [LocalUserRepository, DatabaseCompanyRepository],
          provide: ProxyServicesDynamicModule.GET_PUBLISHED_COMPANY_BY_ID_PROXY_SERVICE,
          useFactory: (localUserRepository: LocalUserRepository, databaseCompanyRepository: DatabaseCompanyRepository) =>
            new UseCaseProxy(new GetPublishedCompanyById(localUserRepository, databaseCompanyRepository, anonymizedCompanyAdapter)),
        },
        {
          inject: [DatabaseCompanyRepository],
          provide: ProxyServicesDynamicModule.GET_COMPANIES_PROXY_SERVICE,
          useFactory: (databaseCompanyRepository: DatabaseCompanyRepository) => new UseCaseProxy(new GetCompanies(databaseCompanyRepository)),
        },
        {
          inject: [PrismicDummyPageRepository],
          provide: ProxyServicesDynamicModule.GET_DUMMY_PAGE_PROXY_SERVICE,
          useFactory: (prismicDummyPageRepository: PrismicDummyPageRepository) => new UseCaseProxy(new GetDummyPage(prismicDummyPageRepository)),
        },
        {
          inject: [FileI18nRepository],
          provide: ProxyServicesDynamicModule.GET_I18N_MESSAGES_FOR_LANG_PROXY_SERVICE,
          useFactory: (fileI18nRepository: FileI18nRepository) => new UseCaseProxy(new GetI18nMessagesForLang(fileI18nRepository)),
        },
        {
          inject: [PrismicHeaderRepository, PrismicFooterRepository],
          provide: ProxyServicesDynamicModule.GET_LAYOUT_PROXY_SERVICE,
          useFactory: (prismicHeaderRepository: PrismicHeaderRepository, prismicFooterRepository: PrismicFooterRepository) =>
            new UseCaseProxy(new GetLayout(prismicHeaderRepository, prismicFooterRepository)),
        },
        {
          inject: [DatabaseTemporaryFileRepository],
          provide: ProxyServicesDynamicModule.CREATE_TEMPORARY_IMAGE_PROXY_SERVICE,
          useFactory: (databaseTemporaryFileRepository: DatabaseTemporaryFileRepository) =>
            new UseCaseProxy(new CreateTemporaryFile(databaseTemporaryFileRepository)),
        },
        {
          inject: [LocalUserRepository, ZohoJobOfferRepository],
          provide: ProxyServicesDynamicModule.GET_USER_JOB_OFFERS_PROXY_SERVICE,
          useFactory: (localUserRepository: LocalUserRepository, zohoJobOfferRepository: ZohoJobOfferRepository) =>
            new UseCaseProxy(new GetUserJobOffers(localUserRepository, zohoJobOfferRepository)),
        },
        {
          inject: [LocalUserRepository, ZohoJobOfferRepository],
          provide: ProxyServicesDynamicModule.GET_USER_JOB_OFFER_BY_ID_PROXY_SERVICE,
          useFactory: (localUserRepository: LocalUserRepository, zohoJobOfferRepository: ZohoJobOfferRepository) =>
            new UseCaseProxy(new GetUserJobOfferById(localUserRepository, zohoJobOfferRepository)),
        },
        {
          inject: [LocalUserRepository, ZohoJobOfferRepository, DatabaseCompanyRepository],
          provide: ProxyServicesDynamicModule.CREATE_NEW_JOB_OFFER_PROXY_SERVICE,
          useFactory: (
            localUserRepository: LocalUserRepository,
            zohoJobOfferRepository: ZohoJobOfferRepository,
            databaseCompanyRepository: DatabaseCompanyRepository
          ) => new UseCaseProxy(new CreateNewJobOffer(localUserRepository, zohoJobOfferRepository, databaseCompanyRepository)),
        },
        {
          inject: [LocalUserRepository, DatabaseCompanyRepository],
          provide: ProxyServicesDynamicModule.VALIDATE_JOB_OFFER_PROXY_SERVICE,
          useFactory: (localUserRepository: LocalUserRepository, databaseCompanyRepository: DatabaseCompanyRepository) =>
            new UseCaseProxy(new ValidateJobOffer(localUserRepository, databaseCompanyRepository)),
        },
        {
          inject: [LocalUserRepository, ZohoJobOfferRepository],
          provide: ProxyServicesDynamicModule.UPDATE_DRAFT_JOB_OFFER_PROXY_SERVICE,
          useFactory: (localUserRepository: LocalUserRepository, zohoJobOfferRepository: ZohoJobOfferRepository) =>
            new UseCaseProxy(new UpdateDraftJobOffer(localUserRepository, zohoJobOfferRepository)),
        },
        {
          inject: [DatabaseTemporaryFileRepository],
          provide: ProxyServicesDynamicModule.CLEAN_EXPIRED_TEMPORARY_FILES_PROXY_SERVICE,
          useFactory: (databaseTemporaryFileRepository: DatabaseTemporaryFileRepository) =>
            new UseCaseProxy(new CleanExpiredTemporaryFiles(databaseTemporaryFileRepository)),
        },
        {
          inject: [ZohoJobOfferRepository],
          provide: ProxyServicesDynamicModule.ARCHIVE_EXPIRED_JOB_OFFERS_PROXY_SERVICE,
          useFactory: (zohoJobOfferRepository: ZohoJobOfferRepository) => new UseCaseProxy(new ArchiveExpiredJobOffers(zohoJobOfferRepository)),
        },
        {
          inject: [LocalUserRepository],
          provide: ProxyServicesDynamicModule.LOG_IN_USER_PROXY_SERVICE,
          useFactory: (localUserRepository: LocalUserRepository) => new UseCaseProxy(new LogInUser(localUserRepository)),
        },
        {
          inject: [LocalUserRepository],
          provide: ProxyServicesDynamicModule.GET_CURRENT_USER_PROXY_SERVICE,
          useFactory: (localUserRepository: LocalUserRepository) => new UseCaseProxy(new GetCurrentUser(localUserRepository)),
        },
        {
          inject: [ZohoJobOfferRepository, LocalUserRepository, DatabaseCompanyRepository],
          provide: ProxyServicesDynamicModule.GET_PUBLISHED_JOB_OFFERS_PROXY_SERVICE,
          useFactory: (
            zohoJobOfferRepository: ZohoJobOfferRepository,
            localUserRepository: LocalUserRepository,
            databaseCompanyRepository: DatabaseCompanyRepository
          ) =>
            new UseCaseProxy(
              new GetPublishedJobOffers(zohoJobOfferRepository, localUserRepository, databaseCompanyRepository, jobOfferWithCompanyAdapter)
            ),
        },
        {
          inject: [ZohoJobOfferRepository, LocalUserRepository, DatabaseCompanyRepository],
          provide: ProxyServicesDynamicModule.GET_PUBLISHED_JOB_OFFER_BY_ID_PROXY_SERVICE,
          useFactory: (
            zohoJobOfferRepository: ZohoJobOfferRepository,
            localUserRepository: LocalUserRepository,
            databaseCompanyRepository: DatabaseCompanyRepository
          ) =>
            new UseCaseProxy(
              new GetPublishedJobOfferById(zohoJobOfferRepository, localUserRepository, databaseCompanyRepository, jobOfferWithCompanyAdapter)
            ),
        },
        {
          inject: [LocalUserRepository, ZohoJobOfferRepository, ZohoCandidateRepository, DatabaseTemporaryFileRepository],
          provide: ProxyServicesDynamicModule.APPLY_TO_JOB_OFFER_PROXY_SERVICE,
          useFactory: (
            localUserRepository: LocalUserRepository,
            zohoJobOfferRepository: ZohoJobOfferRepository,
            zohoCandidateRepository: ZohoCandidateRepository,
            databaseTemporaryFileRepository: DatabaseTemporaryFileRepository
          ) =>
            new UseCaseProxy(
              new ApplyToJobOffer(localUserRepository, zohoJobOfferRepository, zohoCandidateRepository, databaseTemporaryFileRepository)
            ),
        },
        {
          inject: [DatabaseCompanyRepository, DatabaseTemporaryFileRepository],
          provide: ProxyServicesDynamicModule.UPDATE_UNPUBLISHED_COMPANY_PROXY_SERVICE,
          useFactory: (databaseCompanyRepository: DatabaseCompanyRepository, databaseTemporaryFileRepository: DatabaseTemporaryFileRepository) =>
            new UseCaseProxy(new UpdateUnpublishedCompany(databaseCompanyRepository, databaseTemporaryFileRepository)),
        },
        {
          inject: [DatabaseCompanyRepository],
          provide: ProxyServicesDynamicModule.PUBLISH_COMPANY_PROXY_SERVICE,
          useFactory: (databaseCompanyRepository: DatabaseCompanyRepository) => new UseCaseProxy(new PublishCompany(databaseCompanyRepository)),
        },

        {
          inject: [Genium360GeniusProductRepository],
          provide: ProxyServicesDynamicModule.GET_GENIUS_PRODUCTS_PROXY_SERVICE,
          useFactory: (genium360GeniusProductRepository: Genium360GeniusProductRepository) =>
            new UseCaseProxy(new GetGeniusProducts(genium360GeniusProductRepository)),
        },
      ],
      exports: [
        ProxyServicesDynamicModule.APPLY_TO_JOB_OFFER_PROXY_SERVICE,
        ProxyServicesDynamicModule.ARCHIVE_EXPIRED_JOB_OFFERS_PROXY_SERVICE,
        ProxyServicesDynamicModule.CLEAN_EXPIRED_TEMPORARY_FILES_PROXY_SERVICE,
        ProxyServicesDynamicModule.CREATE_NEW_COMPANY_PROXY_SERVICE,
        ProxyServicesDynamicModule.CREATE_NEW_JOB_OFFER_PROXY_SERVICE,
        ProxyServicesDynamicModule.CREATE_TEMPORARY_IMAGE_PROXY_SERVICE,
        ProxyServicesDynamicModule.GET_COMPANIES_PROXY_SERVICE,
        ProxyServicesDynamicModule.GET_CURRENT_USER_PROXY_SERVICE,
        ProxyServicesDynamicModule.GET_DUMMY_PAGE_PROXY_SERVICE,
        ProxyServicesDynamicModule.GET_GENIUS_PRODUCTS_PROXY_SERVICE,
        ProxyServicesDynamicModule.GET_I18N_MESSAGES_FOR_LANG_PROXY_SERVICE,
        ProxyServicesDynamicModule.GET_LAYOUT_PROXY_SERVICE,
        ProxyServicesDynamicModule.GET_PUBLISHED_COMPANY_BY_ID_PROXY_SERVICE,
        ProxyServicesDynamicModule.GET_PUBLISHED_JOB_OFFER_BY_ID_PROXY_SERVICE,
        ProxyServicesDynamicModule.GET_PUBLISHED_JOB_OFFERS_PROXY_SERVICE,
        ProxyServicesDynamicModule.GET_USER_JOB_OFFER_BY_ID_PROXY_SERVICE,
        ProxyServicesDynamicModule.GET_USER_JOB_OFFERS_PROXY_SERVICE,
        ProxyServicesDynamicModule.LOG_IN_USER_PROXY_SERVICE,
        ProxyServicesDynamicModule.PUBLISH_COMPANY_PROXY_SERVICE,
        ProxyServicesDynamicModule.UPDATE_DRAFT_JOB_OFFER_PROXY_SERVICE,
        ProxyServicesDynamicModule.UPDATE_UNPUBLISHED_COMPANY_PROXY_SERVICE,
        ProxyServicesDynamicModule.VALIDATE_COMPANY_PROXY_SERVICE,
        ProxyServicesDynamicModule.VALIDATE_JOB_OFFER_PROXY_SERVICE,
      ],
    };
  }
}

import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';
import { I18nMessages } from '../../domain/i18n/i18n-messages';
import { I18nMessagesRawInterface } from '../../domain/i18n/i18n-messages-raw.interface';
import { I18nMessagesInterface } from '../../domain/i18n/i18n-messages.interface';
import { I18nRepository } from '../../domain/i18n/i18n.repository.interface';
import { Lang } from '../../domain/type-aliases';
import { I18nError } from './errors/i18n.error';

@Injectable()
export class FileI18nRepository implements I18nRepository {
  async findMessagesByLang(lang: Lang): Promise<I18nMessagesInterface> {
    const i18nMessagesRaw: I18nMessagesRawInterface = await new Promise(
      (resolve: (arg: I18nMessagesRawInterface) => void, reject: (reason: Error) => void) => {
        fs.readFile(path.join(__dirname, `../../../i18n/${lang}.json`), 'utf-8', this.handleReadDataToJSON(reject, resolve));
      }
    );

    return new I18nMessages(i18nMessagesRaw);
  }

  private handleReadDataToJSON(
    reject: (reason: Error) => void,
    resolve: (arg: I18nMessagesRawInterface) => void
  ): (err: NodeJS.ErrnoException | null, data: string) => void {
    return (err: NodeJS.ErrnoException | null, data: string) => {
      if (err) {
        reject(new I18nError('Error during JSON data reading', err));
      }
      resolve(JSON.parse(data));
    };
  }
}

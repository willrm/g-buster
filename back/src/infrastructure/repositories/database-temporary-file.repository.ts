import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ItemNotFoundError } from '../../domain/common-errors/item-not-found.error';
import { TemporaryFileInterface } from '../../domain/temporary-file/temporary-file.interface';
import { TemporaryFileRepository } from '../../domain/temporary-file/temporary-file.repository';
import { Uuid } from '../../domain/type-aliases';
import { TemporaryFileEntityTransformer } from './entities/temporary-file-entity.transformer';
import { TemporaryFileEntity } from './entities/temporary-file.entity';

@Injectable()
export class DatabaseTemporaryFileRepository implements TemporaryFileRepository {
  private readonly LIMIT: number = 100;

  constructor(
    @InjectRepository(TemporaryFileEntity) private readonly temporaryFileEntityRepository: Repository<TemporaryFileEntity>,
    private readonly temporaryFileEntityTransformer: TemporaryFileEntityTransformer
  ) {}

  async save(temporaryFile: TemporaryFileInterface): Promise<TemporaryFileInterface> {
    const temporaryFileEntity: TemporaryFileEntity = this.temporaryFileEntityTransformer.to(temporaryFile);
    const savedTemporaryFileEntity: TemporaryFileEntity = await this.temporaryFileEntityRepository.save(temporaryFileEntity);
    const result: TemporaryFileInterface = this.temporaryFileEntityTransformer.from(savedTemporaryFileEntity);

    return Promise.resolve(result);
  }

  async findByUuid(uuid: Uuid): Promise<TemporaryFileInterface> {
    const foundTemporaryFileEntity: TemporaryFileEntity = await this.temporaryFileEntityRepository.findOne({ where: { uuid } });
    if (!foundTemporaryFileEntity) {
      return Promise.reject(new ItemNotFoundError(`TemporaryFile not found with uuid "${uuid}"`));
    }
    const result: TemporaryFileInterface = this.temporaryFileEntityTransformer.from(foundTemporaryFileEntity);

    return Promise.resolve(result);
  }

  async findAllByCreatedAtBefore(date: Date): Promise<TemporaryFileInterface[]> {
    const foundTemporaryFileEntities: TemporaryFileEntity[] = await this.temporaryFileEntityRepository.find({
      take: this.LIMIT,
      order: { createdAt: 'ASC' },
    });
    const temporaryFileEntitiesBeforeDate: TemporaryFileEntity[] = foundTemporaryFileEntities.filter((temporaryFileEntity: TemporaryFileEntity) => {
      return temporaryFileEntity.createdAt.getTime() < date.getTime();
    });
    const result: TemporaryFileInterface[] = temporaryFileEntitiesBeforeDate.map((temporaryFileEntity: TemporaryFileEntity) =>
      this.temporaryFileEntityTransformer.from(temporaryFileEntity)
    );

    return Promise.resolve(result);
  }

  async delete(temporaryFiles: TemporaryFileInterface[]): Promise<void> {
    temporaryFiles
      .map((temporaryFile: TemporaryFileInterface) => this.temporaryFileEntityTransformer.to(temporaryFile))
      .forEach(async (temporaryFileEntity: TemporaryFileEntity) => {
        await this.temporaryFileEntityRepository.delete(temporaryFileEntity);
      });
  }
}

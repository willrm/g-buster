import { Injectable } from '@nestjs/common';
import { GeniusProductInterface } from '../../domain/genius-product/genius-product.interface';
import { GeniusProductRepository } from '../../domain/genius-product/genius-product.repository.interface';
import { Genium360AdapterService } from '../vendors/genium360/genium360-adapter.service';
import { Genium360ApiService } from '../vendors/genium360/genium360-api.service';

@Injectable()
export class Genium360GeniusProductRepository implements GeniusProductRepository {
  constructor(private genium360ApiService: Genium360ApiService, private genium360AdapterService: Genium360AdapterService) {}

  async findAll(): Promise<GeniusProductInterface[]> {
    return this.genium360ApiService.getGeniusProducts().then(this.genium360AdapterService.mapGeniusProducts);
  }
}

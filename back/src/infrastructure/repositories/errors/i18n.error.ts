export class I18nError extends Error {
  constructor(message: string, cause: NodeJS.ErrnoException) {
    super(`${message} caused by: ${cause.message}`);
  }
}

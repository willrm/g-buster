import { GeniusProductInterface } from '../../../domain/genius-product/genius-product.interface';
import { Genium360AdapterService, Genium360ApiJobOfferTarifResponseInterface } from '../../vendors/genium360/genium360-adapter.service';
import { Genium360ApiService } from '../../vendors/genium360/genium360-api.service';
import { Genium360GeniusProductRepository } from '../genium360-genius-product.repository';

describe('infrastructure/repositories/Genium360GeniusProductRepository', () => {
  let genium360GeniusProductRepository: Genium360GeniusProductRepository;
  let mockGenium360ApiService: Genium360ApiService;
  let mockGenium360AdapterService: Genium360AdapterService;

  beforeEach(() => {
    mockGenium360ApiService = {} as Genium360ApiService;
    mockGenium360ApiService.getGeniusProducts = jest.fn();

    mockGenium360AdapterService = {} as Genium360AdapterService;
    mockGenium360AdapterService.mapGeniusProducts = jest.fn();

    genium360GeniusProductRepository = new Genium360GeniusProductRepository(mockGenium360ApiService, mockGenium360AdapterService);
  });

  describe('findAll()', () => {
    it('should call genium360 api service', async () => {
      // given
      (mockGenium360ApiService.getGeniusProducts as jest.Mock).mockReturnValue(
        Promise.resolve([{ id: 123 } as Genium360ApiJobOfferTarifResponseInterface])
      );

      // when
      await genium360GeniusProductRepository.findAll();

      // then
      expect(mockGenium360ApiService.getGeniusProducts).toHaveBeenCalled();
    });

    it('should call genium360 adapter service', async () => {
      // given
      const expected: GeniusProductInterface[] = [{ id: '123' } as GeniusProductInterface];

      (mockGenium360ApiService.getGeniusProducts as jest.Mock).mockReturnValue(
        Promise.resolve([{ id: 123 } as Genium360ApiJobOfferTarifResponseInterface])
      );
      (mockGenium360AdapterService.mapGeniusProducts as jest.Mock).mockReturnValue(Promise.resolve(expected));

      // when
      const result: GeniusProductInterface[] = await genium360GeniusProductRepository.findAll();

      // then
      expect(mockGenium360AdapterService.mapGeniusProducts).toHaveBeenCalled();
      expect(result).toStrictEqual(expected);
    });
  });
});

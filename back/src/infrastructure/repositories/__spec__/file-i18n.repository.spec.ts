import * as fs from 'fs';
import * as path from 'path';
import { I18nMessages } from '../../../domain/i18n/i18n-messages';
import { I18nMessagesRawInterface } from '../../../domain/i18n/i18n-messages-raw.interface';
import { I18nMessagesInterface } from '../../../domain/i18n/i18n-messages.interface';
import { FileI18nRepository } from '../file-i18n.repository';

jest.mock('fs');
jest.mock('../../../domain/i18n/i18n-messages');

describe('infrastructure/repositories/FileI18nRepository', () => {
  let fileI18nRepository: FileI18nRepository;

  beforeEach(() => {
    ((fs.readFile as unknown) as jest.Mock).mockClear();
    (I18nMessages as jest.Mock).mockClear();
    fileI18nRepository = new FileI18nRepository();
  });

  describe('findMessagesByLang()', () => {
    let promise: Promise<I18nMessagesRawInterface>;
    let callback: (err: unknown, data: string) => void;
    beforeEach(() => {
      promise = fileI18nRepository.findMessagesByLang('fr-ca');
      callback = ((fs.readFile as unknown) as jest.Mock).mock.calls[0][2];
    });
    it('should call fs.readFile with path ....', async () => {
      // given
      const expectedPath: string = path.join(__dirname, '../../../../i18n/fr-ca.json');
      const expectedCharset: string = 'utf-8';

      // when
      callback(null, '{}');
      await promise;

      // then
      expect(fs.readFile).toHaveBeenCalledWith(expectedPath, expectedCharset, expect.any(Function));
    });

    it('should build i18nMessages with read data', async () => {
      // given
      const dataFromFsReadFile: object = {
        welcome: 'A welcome message',
        thanks: {
          you: 'A thank you message',
        },
      };
      const dataFromFsReadFileAsString: string = JSON.stringify(dataFromFsReadFile);
      const expected: I18nMessagesRawInterface = { ...dataFromFsReadFile } as I18nMessagesRawInterface;

      // when
      callback(null, dataFromFsReadFileAsString);
      await promise;

      // then
      const i18nMessagesRaw: I18nMessagesRawInterface = (I18nMessages as jest.Mock).mock.calls[0][0];
      expect(i18nMessagesRaw).toStrictEqual(expected);
    });

    it('should return a resolved promise with read data when callback is executed', async () => {
      // given
      const expected: I18nMessagesInterface = {
        welcome: 'A welcome message',
        thanks: {
          you: 'A thank you message',
        },
      };
      (I18nMessages as jest.Mock).mockImplementation(() => expected);

      // when
      callback(null, '{}');
      const result: I18nMessagesInterface = await promise;

      // then
      expect(result).toStrictEqual(expected);
    });

    it('should return a rejected promise with error when callback is executed', async () => {
      // given
      const error: NodeJS.ErrnoException = { name: 'An error', message: 'An error message' };

      // when
      callback(error, null);

      // then
      await expect(promise).rejects.toThrow('Error during JSON data reading caused by: An error message');
    });
  });
});

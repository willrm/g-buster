import { ItemNotFoundError } from '../../../domain/common-errors/item-not-found.error';
import { JobOfferStatus } from '../../../domain/job-offer/job-offer-enums';
import { JobOfferInterface } from '../../../domain/job-offer/job-offer.interface';
import { JobOfferId, UserId } from '../../../domain/type-aliases';
import { DomainFieldToZohoModelFieldMappingStrategyInterface } from '../../vendors/zoho/domain-to-zoho-model-mapping-strategy';
import { ZohoJsonFieldInterface } from '../../vendors/zoho/models/json/zoho-json-field.interface';
import { ZohoJsonGetRecordsResponseInterface } from '../../vendors/zoho/models/json/zoho-json-get-records-response.interface';
import { ZohoJsonRowInterface } from '../../vendors/zoho/models/json/zoho-json-row.interface';
import { ZohoCachedApiService } from '../../vendors/zoho/zoho-cached-api.service';
import { ZohoRequestAdapterService } from '../../vendors/zoho/zoho-request-adapter.service';
import { ZohoResponseAdapterService } from '../../vendors/zoho/zoho-response-adapter.service';
import { ZohoSearchCondition, ZohoSearchConditionOperator } from '../../vendors/zoho/zoho-search-condition';
import { JobOfferDomainToJobOpeningZohoModelMappingStrategy } from '../mapping-strategies/job-offer-domain-to-job-opening-zoho-model-mapping-strategy';
import { JobOpeningZohoModelToJobOfferDomainMappingStrategy } from '../mapping-strategies/job-opening-zoho-model-to-job-offer-domain-mapping-strategy';
import { ZohoJobOfferRepository } from '../zoho-job-offer.repository';

describe('infrastructure/repositories/ZohoJobOfferRepository', () => {
  let zohoJobOfferRepository: ZohoJobOfferRepository;
  let mockZohoCachedApiService: ZohoCachedApiService;
  let mockZohoRequestAdapterService: ZohoRequestAdapterService;
  let mockZohoResponseAdapterService: ZohoResponseAdapterService;
  let mockJobOfferDomainToJobOpeningZohoModelMappingStrategy: JobOfferDomainToJobOpeningZohoModelMappingStrategy;
  let mockJobOpeningZohoModelToJobOfferDomainMappingStrategy: JobOpeningZohoModelToJobOfferDomainMappingStrategy;

  beforeEach(() => {
    mockZohoCachedApiService = {} as ZohoCachedApiService;
    mockZohoCachedApiService.addRecords = jest.fn();
    mockZohoCachedApiService.getRecordById = jest.fn();
    mockZohoCachedApiService.getRecords = jest.fn();
    mockZohoCachedApiService.getSearchRecords = jest.fn();
    mockZohoCachedApiService.updateRecords = jest.fn();
    mockZohoCachedApiService.changeStatus = jest.fn();
    mockZohoRequestAdapterService = {} as ZohoRequestAdapterService;
    mockZohoRequestAdapterService.map = jest.fn();
    mockZohoResponseAdapterService = {} as ZohoResponseAdapterService;
    mockZohoResponseAdapterService.map = jest.fn();
    mockJobOfferDomainToJobOpeningZohoModelMappingStrategy = {} as JobOfferDomainToJobOpeningZohoModelMappingStrategy;
    mockJobOpeningZohoModelToJobOfferDomainMappingStrategy = {} as JobOpeningZohoModelToJobOfferDomainMappingStrategy;

    zohoJobOfferRepository = new ZohoJobOfferRepository(
      mockZohoCachedApiService,
      mockZohoRequestAdapterService,
      mockZohoResponseAdapterService,
      mockJobOfferDomainToJobOpeningZohoModelMappingStrategy,
      mockJobOpeningZohoModelToJobOfferDomainMappingStrategy
    );
  });

  describe('findById()', () => {
    let zohoApiGetRecordsResponse: ZohoJsonGetRecordsResponseInterface;

    beforeEach(() => {
      zohoApiGetRecordsResponse = {
        response: {
          result: {
            JobOpenings: {
              row: [] as ZohoJsonRowInterface[],
            },
          },
        },
      };
      (mockZohoCachedApiService.getRecordById as jest.Mock).mockReturnValue(Promise.resolve(zohoApiGetRecordsResponse));
    });

    it('should call zoho api to get job opening record by id', async () => {
      // given
      const id: JobOfferId = 'aJobOfferId';
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[0] = { FL: [{} as ZohoJsonFieldInterface] } as ZohoJsonRowInterface;

      const jobOffer: JobOfferInterface = { title: 'An awesome job' } as JobOfferInterface;
      (mockZohoResponseAdapterService.map as jest.Mock).mockReturnValue(jobOffer);

      // when
      await zohoJobOfferRepository.findById(id);

      // then
      expect(mockZohoCachedApiService.getRecordById).toHaveBeenCalledWith('JobOpenings', id);
    });

    it('should call adapter for each row fields list with mapping strategy', async () => {
      // given
      const id: JobOfferId = 'aJobOfferId';
      const fieldsListFirstRow: ZohoJsonFieldInterface[] = [{} as ZohoJsonFieldInterface];
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[0] = { FL: fieldsListFirstRow } as ZohoJsonRowInterface;

      // when
      await zohoJobOfferRepository.findById(id);

      // then
      expect(mockZohoResponseAdapterService.map).toHaveBeenCalledWith(fieldsListFirstRow, mockJobOpeningZohoModelToJobOfferDomainMappingStrategy);
    });

    it('should return an item of row fields list mapped', async () => {
      // given
      const id: JobOfferId = 'aJobOfferId';
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[0] = { FL: [{} as ZohoJsonFieldInterface] } as ZohoJsonRowInterface;

      const jobOffer: JobOfferInterface = { title: 'An awesome job' } as JobOfferInterface;
      (mockZohoResponseAdapterService.map as jest.Mock).mockReturnValue(jobOffer);

      // when
      const result: JobOfferInterface = await zohoJobOfferRepository.findById(id);

      // then
      expect(result).toStrictEqual(jobOffer);
    });

    it('should throw exception when job offer not found in api', async () => {
      // given
      const id: JobOfferId = 'aJobOfferId';

      // when
      const result: Promise<JobOfferInterface> = zohoJobOfferRepository.findById(id);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('JobOffer not found with id "aJobOfferId"'));
    });
  });

  describe('findAll()', () => {
    let zohoApiGetRecordsResponse: ZohoJsonGetRecordsResponseInterface;

    beforeEach(() => {
      zohoApiGetRecordsResponse = {
        response: {
          result: {
            JobOpenings: {
              row: [] as ZohoJsonRowInterface[],
            },
          },
        },
      };
      (mockZohoCachedApiService.getRecords as jest.Mock).mockReturnValue(Promise.resolve(zohoApiGetRecordsResponse));
    });

    it('should call zoho api to get job openings records', async () => {
      // when
      await zohoJobOfferRepository.findAll();

      // then
      expect(mockZohoCachedApiService.getRecords).toHaveBeenCalledWith('JobOpenings');
    });

    it('should call adapter for each row fields list with mapping strategy', async () => {
      // given
      const fieldsListFirstRow: ZohoJsonFieldInterface[] = [{} as ZohoJsonFieldInterface];
      const fieldsListSecondRow: ZohoJsonFieldInterface[] = [{} as ZohoJsonFieldInterface];
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[0] = { FL: fieldsListFirstRow } as ZohoJsonRowInterface;
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[1] = { FL: fieldsListSecondRow } as ZohoJsonRowInterface;

      // when
      await zohoJobOfferRepository.findAll();

      // then
      expect(mockZohoResponseAdapterService.map).toHaveBeenCalledWith(fieldsListFirstRow, mockJobOpeningZohoModelToJobOfferDomainMappingStrategy);
      expect(mockZohoResponseAdapterService.map).toHaveBeenCalledWith(fieldsListSecondRow, mockJobOpeningZohoModelToJobOfferDomainMappingStrategy);
    });

    it('should return an array of each row fields list mapped', async () => {
      // given
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[0] = { FL: [{} as ZohoJsonFieldInterface] } as ZohoJsonRowInterface;
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[1] = { FL: [{} as ZohoJsonFieldInterface] } as ZohoJsonRowInterface;

      const jobOffer1: JobOfferInterface = { title: 'An awesome job' } as JobOfferInterface;
      const jobOffer2: JobOfferInterface = { title: 'Another awesome job' } as JobOfferInterface;
      (mockZohoResponseAdapterService.map as jest.Mock).mockReturnValueOnce(jobOffer1).mockReturnValueOnce(jobOffer2);

      // when
      const result: JobOfferInterface[] = await zohoJobOfferRepository.findAll();

      // then
      expect(result).toStrictEqual([jobOffer1, jobOffer2]);
    });
  });

  describe('findAllByUserId()', () => {
    let zohoApiGetRecordsResponse: ZohoJsonGetRecordsResponseInterface;
    let userId: UserId;

    beforeEach(() => {
      zohoApiGetRecordsResponse = {
        response: {
          result: {
            JobOpenings: {
              row: [] as ZohoJsonRowInterface[],
            },
          },
        },
      };
      mockJobOfferDomainToJobOpeningZohoModelMappingStrategy.userId = {
        target: undefined,
        map: (value: unknown) => undefined,
      } as DomainFieldToZohoModelFieldMappingStrategyInterface;
      userId = '42';
      (mockZohoCachedApiService.getSearchRecords as jest.Mock).mockReturnValue(Promise.resolve(zohoApiGetRecordsResponse));
    });

    it('should call zoho api to search zoho job openings having given user id', async () => {
      // given
      mockJobOfferDomainToJobOpeningZohoModelMappingStrategy.userId = {
        target: 'userId-zoho-field',
        map: (value: unknown) => (value === userId ? 'id value mapped to zoho value' : null),
      };

      // when
      await zohoJobOfferRepository.findAllByUserId(userId);

      // then
      expect(mockZohoCachedApiService.getSearchRecords).toHaveBeenCalledWith('JobOpenings', {
        field: 'userId-zoho-field',
        operator: ZohoSearchConditionOperator.IS,
        value: 'id value mapped to zoho value',
      } as ZohoSearchCondition);
    });

    it('should call adapter for each row fields list with mapping strategy', async () => {
      // given
      const fieldsListFirstRow: ZohoJsonFieldInterface[] = [{} as ZohoJsonFieldInterface];
      const fieldsListSecondRow: ZohoJsonFieldInterface[] = [{} as ZohoJsonFieldInterface];
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[0] = { FL: fieldsListFirstRow } as ZohoJsonRowInterface;
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[1] = { FL: fieldsListSecondRow } as ZohoJsonRowInterface;

      // when
      await zohoJobOfferRepository.findAllByUserId(userId);

      // then
      expect(mockZohoResponseAdapterService.map).toHaveBeenCalledWith(fieldsListFirstRow, mockJobOpeningZohoModelToJobOfferDomainMappingStrategy);
      expect(mockZohoResponseAdapterService.map).toHaveBeenCalledWith(fieldsListSecondRow, mockJobOpeningZohoModelToJobOfferDomainMappingStrategy);
    });

    it('should return an array of each row fields list mapped', async () => {
      // given
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[0] = { FL: [{} as ZohoJsonFieldInterface] } as ZohoJsonRowInterface;
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[1] = { FL: [{} as ZohoJsonFieldInterface] } as ZohoJsonRowInterface;

      const jobOffer1: JobOfferInterface = { title: 'An awesome job' } as JobOfferInterface;
      const jobOffer2: JobOfferInterface = { title: 'Another awesome job' } as JobOfferInterface;
      (mockZohoResponseAdapterService.map as jest.Mock).mockReturnValueOnce(jobOffer1).mockReturnValueOnce(jobOffer2);

      // when
      const result: JobOfferInterface[] = await zohoJobOfferRepository.findAllByUserId(userId);

      // then
      expect(result).toStrictEqual([jobOffer1, jobOffer2]);
    });
  });

  describe('findAllByStatus()', () => {
    let zohoApiGetRecordsResponse: ZohoJsonGetRecordsResponseInterface;
    let status: JobOfferStatus;

    beforeEach(() => {
      zohoApiGetRecordsResponse = {
        response: {
          result: {
            JobOpenings: {
              row: [] as ZohoJsonRowInterface[],
            },
          },
        },
      };
      mockJobOfferDomainToJobOpeningZohoModelMappingStrategy.status = {
        target: undefined,
        map: (value: unknown) => undefined,
      } as DomainFieldToZohoModelFieldMappingStrategyInterface;
      status = JobOfferStatus.STATUS_PENDING_VALIDATION;
      (mockZohoCachedApiService.getSearchRecords as jest.Mock).mockReturnValue(Promise.resolve(zohoApiGetRecordsResponse));
    });

    it('should call zoho api to search zoho job openings published before given date', async () => {
      // given
      mockJobOfferDomainToJobOpeningZohoModelMappingStrategy.status = {
        target: 'status-zoho-field',
        map: (value: unknown) => (value === status ? 'status value mapped to zoho value' : null),
      };

      // when
      await zohoJobOfferRepository.findAllByStatus(status);

      // then
      expect(mockZohoCachedApiService.getSearchRecords).toHaveBeenCalledWith('JobOpenings', {
        field: 'status-zoho-field',
        operator: ZohoSearchConditionOperator.IS,
        value: 'status value mapped to zoho value',
      } as ZohoSearchCondition);
    });

    it('should call adapter for each row fields list with mapping strategy', async () => {
      // given
      const fieldsListFirstRow: ZohoJsonFieldInterface[] = [{} as ZohoJsonFieldInterface];
      const fieldsListSecondRow: ZohoJsonFieldInterface[] = [{} as ZohoJsonFieldInterface];
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[0] = { FL: fieldsListFirstRow } as ZohoJsonRowInterface;
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[1] = { FL: fieldsListSecondRow } as ZohoJsonRowInterface;

      // when
      await zohoJobOfferRepository.findAllByStatus(status);

      // then
      expect(mockZohoResponseAdapterService.map).toHaveBeenCalledWith(fieldsListFirstRow, mockJobOpeningZohoModelToJobOfferDomainMappingStrategy);
      expect(mockZohoResponseAdapterService.map).toHaveBeenCalledWith(fieldsListSecondRow, mockJobOpeningZohoModelToJobOfferDomainMappingStrategy);
    });

    it('should return an array of each row fields list mapped', async () => {
      // given
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[0] = { FL: [{} as ZohoJsonFieldInterface] } as ZohoJsonRowInterface;
      zohoApiGetRecordsResponse.response.result.JobOpenings.row[1] = { FL: [{} as ZohoJsonFieldInterface] } as ZohoJsonRowInterface;

      const jobOffer1: JobOfferInterface = { title: 'An awesome job' } as JobOfferInterface;
      const jobOffer2: JobOfferInterface = { title: 'Another awesome job' } as JobOfferInterface;
      (mockZohoResponseAdapterService.map as jest.Mock).mockReturnValueOnce(jobOffer1).mockReturnValueOnce(jobOffer2);

      // when
      const result: JobOfferInterface[] = await zohoJobOfferRepository.findAllByStatus(status);

      // then
      expect(result).toStrictEqual([jobOffer1, jobOffer2]);
    });
  });

  describe('save()', () => {
    let jobOffer: JobOfferInterface;
    beforeEach(() => {
      jobOffer = { title: 'A job title' } as JobOfferInterface;
    });

    it('should call adapter for given job opening with mapping strategy', async () => {
      // when
      await zohoJobOfferRepository.save(jobOffer);

      // then
      expect(mockZohoRequestAdapterService.map).toHaveBeenCalledWith(jobOffer, mockJobOfferDomainToJobOpeningZohoModelMappingStrategy);
    });

    describe('when job offer has no id', () => {
      beforeEach(() => {
        jobOffer.id = undefined;
      });

      it('should call zoho api to add job opening record with request generated from adapter', async () => {
        // given
        const zohoJsonFields: ZohoJsonFieldInterface[] = [{} as ZohoJsonFieldInterface, {} as ZohoJsonFieldInterface];
        (mockZohoRequestAdapterService.map as jest.Mock).mockReturnValue(zohoJsonFields);

        // when
        await zohoJobOfferRepository.save(jobOffer);

        // then
        expect(mockZohoCachedApiService.addRecords).toHaveBeenCalledWith('JobOpenings', {
          JobOpenings: {
            row: [
              {
                no: '1',
                FL: zohoJsonFields,
              },
            ],
          },
        });
      });

      it('should return created job opening id', async () => {
        // given
        const expected: string = '123456789';
        (mockZohoCachedApiService.addRecords as jest.Mock).mockReturnValue(Promise.resolve(expected));

        // when
        const result: JobOfferId = await zohoJobOfferRepository.save(jobOffer);

        // then
        expect(result).toBe(expected as JobOfferId);
      });
    });

    describe('when job offer has an id', () => {
      beforeEach(() => {
        jobOffer.id = '42';

        mockJobOfferDomainToJobOpeningZohoModelMappingStrategy.status = {} as DomainFieldToZohoModelFieldMappingStrategyInterface;
        mockJobOfferDomainToJobOpeningZohoModelMappingStrategy.status.map = jest.fn();
      });

      it('should call zoho api to change job opening status with id and mapped status', async () => {
        // given
        jobOffer.status = JobOfferStatus.STATUS_PUBLISHED;
        (mockJobOfferDomainToJobOpeningZohoModelMappingStrategy.status.map as jest.Mock).mockReturnValue('mapped status');

        // when
        await zohoJobOfferRepository.save(jobOffer);

        // then
        expect(mockJobOfferDomainToJobOpeningZohoModelMappingStrategy.status.map).toHaveBeenCalledWith(JobOfferStatus.STATUS_PUBLISHED);
        expect(mockZohoCachedApiService.changeStatus).toHaveBeenCalledWith('JobOpenings', '42', 'mapped status');
      });

      it('should call zoho api to update job opening record with id and request generated from adapter', async () => {
        // given
        const zohoJsonFields: ZohoJsonFieldInterface[] = [{} as ZohoJsonFieldInterface, {} as ZohoJsonFieldInterface];
        (mockZohoRequestAdapterService.map as jest.Mock).mockReturnValue(zohoJsonFields);

        // when
        await zohoJobOfferRepository.save(jobOffer);

        // then
        expect(mockZohoCachedApiService.updateRecords).toHaveBeenCalledWith('JobOpenings', '42', {
          JobOpenings: {
            row: [
              {
                no: '1',
                FL: zohoJsonFields,
              },
            ],
          },
        });
      });

      it('should return updated job opening id', async () => {
        // given
        const expected: string = '123456789';
        (mockZohoCachedApiService.updateRecords as jest.Mock).mockReturnValue(Promise.resolve(expected));

        // when
        const result: JobOfferId = await zohoJobOfferRepository.save(jobOffer);

        // then
        expect(result).toBe(expected as JobOfferId);
      });
    });
  });
});

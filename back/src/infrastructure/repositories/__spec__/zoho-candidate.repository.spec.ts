import { CandidateInterface } from '../../../domain/candidate/candidate.interface';
import { ItemNotFoundError } from '../../../domain/common-errors/item-not-found.error';
import { TemporaryFileInterface } from '../../../domain/temporary-file/temporary-file.interface';
import { CandidateId, JobOfferId } from '../../../domain/type-aliases';
import { DomainFieldToZohoModelFieldMappingStrategyInterface } from '../../vendors/zoho/domain-to-zoho-model-mapping-strategy';
import { ZohoJsonFieldInterface } from '../../vendors/zoho/models/json/zoho-json-field.interface';
import { ZohoJsonGetRecordsResponseInterface } from '../../vendors/zoho/models/json/zoho-json-get-records-response.interface';
import { ZohoJsonRowInterface } from '../../vendors/zoho/models/json/zoho-json-row.interface';
import { ZohoApiModule, ZohoFileType } from '../../vendors/zoho/zoho-api.service';
import { ZohoCachedApiService } from '../../vendors/zoho/zoho-cached-api.service';
import { ZohoRequestAdapterService } from '../../vendors/zoho/zoho-request-adapter.service';
import { ZohoResponseAdapterService } from '../../vendors/zoho/zoho-response-adapter.service';
import { ZohoSearchCondition, ZohoSearchConditionOperator } from '../../vendors/zoho/zoho-search-condition';
import { CandidateDomainToCandidateZohoModelMappingStrategy } from '../mapping-strategies/candidate-domain-to-candidate-zoho-model-mapping-strategy';
import { CandidateZohoModelToCandidateDomainMappingStrategy } from '../mapping-strategies/candidate-zoho-model-to-candidate-domain-mapping-strategy';
import { ZohoCandidateRepository } from '../zoho-candidate.repository';

describe('infrastructure/repositories/ZohoCandidateRepository', () => {
  let zohoCandidateRepository: ZohoCandidateRepository;
  let mockZohoCachedApiService: ZohoCachedApiService;
  let mockZohoRequestAdapterService: ZohoRequestAdapterService;
  let mockZohoResponseAdapterService: ZohoResponseAdapterService;
  let mockCandidateDomainToCandidateZohoModelMappingStrategy: CandidateDomainToCandidateZohoModelMappingStrategy;
  let mockCandidateZohoModelToCandidateDomainMappingStrategy: CandidateZohoModelToCandidateDomainMappingStrategy;

  beforeEach(() => {
    mockZohoCachedApiService = {} as ZohoCachedApiService;
    mockZohoCachedApiService.addRecords = jest.fn();
    mockZohoCachedApiService.updateRecords = jest.fn();
    mockZohoCachedApiService.getSearchRecords = jest.fn();
    mockZohoCachedApiService.associateJobOpening = jest.fn();
    mockZohoCachedApiService.uploadFile = jest.fn();
    mockZohoRequestAdapterService = {} as ZohoRequestAdapterService;
    mockZohoRequestAdapterService.map = jest.fn();
    mockZohoResponseAdapterService = {} as ZohoResponseAdapterService;
    mockZohoResponseAdapterService.map = jest.fn();
    mockCandidateDomainToCandidateZohoModelMappingStrategy = {} as CandidateDomainToCandidateZohoModelMappingStrategy;
    mockCandidateZohoModelToCandidateDomainMappingStrategy = {} as CandidateZohoModelToCandidateDomainMappingStrategy;

    zohoCandidateRepository = new ZohoCandidateRepository(
      mockZohoCachedApiService,
      mockZohoRequestAdapterService,
      mockZohoResponseAdapterService,
      mockCandidateDomainToCandidateZohoModelMappingStrategy,
      mockCandidateZohoModelToCandidateDomainMappingStrategy
    );
  });

  describe('findByEmail()', () => {
    let zohoApiGetRecordsResponse: ZohoJsonGetRecordsResponseInterface;
    let email: string;

    beforeEach(() => {
      zohoApiGetRecordsResponse = {
        response: {
          result: {
            Candidates: {
              row: [{ no: '1', FL: [] as ZohoJsonFieldInterface[] }] as ZohoJsonRowInterface[],
            },
          },
        },
      };
      mockCandidateDomainToCandidateZohoModelMappingStrategy.email = {
        target: undefined,
        map: (value: unknown) => undefined,
      } as DomainFieldToZohoModelFieldMappingStrategyInterface;
      email = 'test@example.org';
      (mockZohoCachedApiService.getSearchRecords as jest.Mock).mockReturnValue(Promise.resolve(zohoApiGetRecordsResponse));
    });

    it('should call zoho api to search zoho candidates having given email', async () => {
      // given
      mockCandidateDomainToCandidateZohoModelMappingStrategy.email = {
        target: 'email-zoho-field',
        map: (value: unknown) => (value === email ? 'email value mapped to zoho value' : null),
      };

      // when
      await zohoCandidateRepository.findByEmail(email);

      // then
      expect(mockZohoCachedApiService.getSearchRecords).toHaveBeenCalledWith('Candidates', {
        field: 'email-zoho-field',
        operator: ZohoSearchConditionOperator.IS,
        value: 'email value mapped to zoho value',
      } as ZohoSearchCondition);
    });

    it('should call adapter for row result with mapping strategy', async () => {
      // given
      const fieldsListRow: ZohoJsonFieldInterface[] = [{} as ZohoJsonFieldInterface];
      zohoApiGetRecordsResponse.response.result.Candidates.row[0] = { FL: fieldsListRow } as ZohoJsonRowInterface;

      // when
      await zohoCandidateRepository.findByEmail(email);

      // then
      expect(mockZohoResponseAdapterService.map).toHaveBeenCalledWith(fieldsListRow, mockCandidateZohoModelToCandidateDomainMappingStrategy);
    });

    it('should return first row mapped', async () => {
      // given
      zohoApiGetRecordsResponse.response.result.Candidates.row[0] = { FL: [{} as ZohoJsonFieldInterface] } as ZohoJsonRowInterface;

      const candidate: CandidateInterface = { firstName: 'A first name' } as CandidateInterface;
      (mockZohoResponseAdapterService.map as jest.Mock).mockReturnValue(candidate);

      // when
      const result: CandidateInterface = await zohoCandidateRepository.findByEmail(email);

      // then
      expect(result).toBe(candidate);
    });

    it('should throw not found error when no result', async () => {
      // given
      zohoApiGetRecordsResponse.response.result.Candidates.row = [];

      // when
      const result: Promise<CandidateInterface> = zohoCandidateRepository.findByEmail(email);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('Candidate not found'));
    });
  });

  describe('save()', () => {
    let candidate: CandidateInterface;
    let resumeTemporaryFile: TemporaryFileInterface;
    let coverLetterTemporaryFile: TemporaryFileInterface;

    beforeEach(() => {
      candidate = { firstName: 'A first name' } as CandidateInterface;
      resumeTemporaryFile = {} as TemporaryFileInterface;
      coverLetterTemporaryFile = {} as TemporaryFileInterface;
    });

    it('should call adapter for given candidate with mapping strategy', async () => {
      // when
      await zohoCandidateRepository.save(candidate, resumeTemporaryFile, coverLetterTemporaryFile);

      // then
      expect(mockZohoRequestAdapterService.map).toHaveBeenCalledWith(candidate, mockCandidateDomainToCandidateZohoModelMappingStrategy);
    });

    describe('when candidate has no id', () => {
      beforeEach(() => {
        candidate.id = undefined;
      });

      it('should call zoho api to add new candidate', async () => {
        // given
        const zohoJsonFields: ZohoJsonFieldInterface[] = [{} as ZohoJsonFieldInterface, {} as ZohoJsonFieldInterface];
        (mockZohoRequestAdapterService.map as jest.Mock).mockReturnValue(zohoJsonFields);

        // when
        await zohoCandidateRepository.save(candidate, resumeTemporaryFile, coverLetterTemporaryFile);

        // then
        expect(mockZohoCachedApiService.addRecords).toHaveBeenCalledWith(ZohoApiModule.CANDIDATES, {
          Candidates: { row: [{ no: '1', FL: zohoJsonFields }] },
        });
      });

      it('should call zoho api to upload resume to created candidate', async () => {
        // given
        const createdCandidateId: string = '123456789';
        (mockZohoCachedApiService.addRecords as jest.Mock).mockReturnValue(Promise.resolve(createdCandidateId));

        resumeTemporaryFile.base64 = 'resume-as-base64';
        resumeTemporaryFile.filename = 'filename.extension';
        resumeTemporaryFile.mimeType = 'mime/type';

        // when
        await zohoCandidateRepository.save(candidate, resumeTemporaryFile, coverLetterTemporaryFile);

        // then
        expect(mockZohoCachedApiService.uploadFile).toHaveBeenNthCalledWith(
          1,
          'Candidates',
          createdCandidateId,
          ZohoFileType.RESUME,
          'resume-as-base64',
          'filename.extension',
          'mime/type'
        );
      });

      it('should call zoho api to upload cover letter to created candidate', async () => {
        // given
        const createdCandidateId: string = '123456789';
        (mockZohoCachedApiService.addRecords as jest.Mock).mockReturnValue(Promise.resolve(createdCandidateId));

        coverLetterTemporaryFile.base64 = 'cover-letter-as-base64';
        coverLetterTemporaryFile.filename = 'cover-letter-filename.extension';
        coverLetterTemporaryFile.mimeType = 'cover/letter';

        // when
        await zohoCandidateRepository.save(candidate, resumeTemporaryFile, coverLetterTemporaryFile);

        // then
        expect(mockZohoCachedApiService.uploadFile).toHaveBeenNthCalledWith(
          2,
          'Candidates',
          createdCandidateId,
          ZohoFileType.COVER_LETTER,
          'cover-letter-as-base64',
          'cover-letter-filename.extension',
          'cover/letter'
        );
      });

      it('should return created and then updated candidate id', async () => {
        // given
        const expected: string = '123456789';
        (mockZohoCachedApiService.addRecords as jest.Mock).mockReturnValue(Promise.resolve(expected));
        (mockZohoCachedApiService.uploadFile as jest.Mock).mockReturnValue(Promise.resolve('uploaded-file-id'));

        // when
        const result: CandidateId = await zohoCandidateRepository.save(candidate, resumeTemporaryFile, coverLetterTemporaryFile);

        // then
        expect(result).toBe(expected as CandidateId);
      });
    });

    describe('when candidate has an id', () => {
      beforeEach(() => {
        candidate.id = '42';
      });

      it('should call zoho api to update candidate record with id and request generated from adapter', async () => {
        // given
        const zohoJsonFields: ZohoJsonFieldInterface[] = [{} as ZohoJsonFieldInterface, {} as ZohoJsonFieldInterface];
        (mockZohoRequestAdapterService.map as jest.Mock).mockReturnValue(zohoJsonFields);

        // when
        await zohoCandidateRepository.save(candidate, resumeTemporaryFile, coverLetterTemporaryFile);

        // then
        expect(mockZohoCachedApiService.updateRecords).toHaveBeenCalledWith('Candidates', '42', {
          Candidates: { row: [{ no: '1', FL: zohoJsonFields }] },
        });
      });

      it('should return updated candidate id', async () => {
        // given
        const expected: string = '123456789';
        (mockZohoCachedApiService.updateRecords as jest.Mock).mockReturnValue(Promise.resolve(expected));

        // when
        const result: CandidateId = await zohoCandidateRepository.save(candidate, resumeTemporaryFile, coverLetterTemporaryFile);

        // then
        expect(result).toBe(expected as CandidateId);
      });
    });
  });

  describe('applyToJobOffer()', () => {
    it('should call zoho api to associate given candidate id with given job id', async () => {
      // given
      const candidateId: CandidateId = 'candidate-id';
      const jobOfferId: JobOfferId = 'job-id';

      // when
      await zohoCandidateRepository.applyToJobOffer(candidateId, jobOfferId);

      // then
      expect(mockZohoCachedApiService.associateJobOpening).toHaveBeenCalledWith(ZohoApiModule.CANDIDATES, candidateId, jobOfferId);
    });
  });
});

import { Request } from 'express';
import { ItemNotFoundError } from '../../../domain/common-errors/item-not-found.error';
import { UserRole } from '../../../domain/user/user-enums';
import { UserInterface } from '../../../domain/user/user.interface';
import { EnvironmentConfigService } from '../../config/environment-config/environment-config.service';
import { LocalUserRepository } from '../local-user.repository';

describe('infrastructure/repositories/LocalUserRepository', () => {
  let localUserRepository: LocalUserRepository;
  let mockRequest: Request;
  let mockEnvironmentConfigService: EnvironmentConfigService;

  beforeEach(() => {
    mockRequest = {
      session: {} as CookieSessionInterfaces.CookieSessionObject,
    } as Request;
    mockEnvironmentConfigService = {} as EnvironmentConfigService;
    mockEnvironmentConfigService.get = jest.fn();

    localUserRepository = new LocalUserRepository(mockRequest, mockEnvironmentConfigService);
  });

  describe('findAllAdminEmailAddresses()', () => {
    it('should return email addresses from config splitted using comma character', async () => {
      // given
      (mockEnvironmentConfigService.get as jest.Mock).mockReturnValue('admin1@example.org, admin2@example.org, admin3@example.org');

      // when
      const result: string[] = await localUserRepository.findAllAdminEmailAddresses();

      // then
      expect(mockEnvironmentConfigService.get as jest.Mock).toHaveBeenCalledWith('GBUSTER_ADMIN_EMAIL_ADDRESSES');
      expect(result).toStrictEqual(['admin1@example.org', 'admin2@example.org', 'admin3@example.org']);
    });
  });

  describe('logIn()', () => {
    it('should save given user in session', async () => {
      // given
      const user: UserInterface = {
        id: '123456789',
        firstName: 'Test',
        role: UserRole.CANDIDATE,
      } as UserInterface;

      // when
      await localUserRepository.logIn(user);

      // then
      expect(mockRequest.session.user).toBe(user);
    });
  });

  describe('getCurrent()', () => {
    it('should get current user from session', async () => {
      // given
      const expected: UserInterface = {
        id: '123456789',
        firstName: 'Test',
        role: UserRole.CANDIDATE,
      } as UserInterface;
      mockRequest.session.user = expected;

      // when
      const result: UserInterface = await localUserRepository.getCurrent();

      // then
      expect(result).toBe(expected);
    });

    it('should throw exception when no user in session', async () => {
      // given
      mockRequest.session.user = null;

      // when
      const result: Promise<UserInterface> = localUserRepository.getCurrent();

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError('User not found'));
    });
  });
});

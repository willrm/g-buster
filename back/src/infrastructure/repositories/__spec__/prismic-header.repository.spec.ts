import { Document } from 'prismic-javascript/d.ts/documents';
import { Header } from '../../../domain/header/header';
import { HeaderInterface } from '../../../domain/header/header.interface';
import { HeaderRawInterface } from '../../../domain/header/header.raw.interface';
import { PrismicApiService } from '../../vendors/prismic/prismic-api.service';
import { PrismicDocumentAdapterService } from '../../vendors/prismic/prismic-document-adapter.service';
import { PrismicHeaderRepository } from '../prismic-header.repository';

jest.mock('../../../domain/header/header');

describe('infrastructure/repositories/PrismicHeaderRepository', () => {
  let prismicHeaderRepository: PrismicHeaderRepository;
  let mockPrismicApiService: PrismicApiService;
  let mockPrismicDocumentAdapterService: PrismicDocumentAdapterService;
  let document: Document;

  beforeEach(() => {
    (Header as jest.Mock).mockClear();
    mockPrismicApiService = {} as PrismicApiService;
    mockPrismicApiService.getByUID = jest.fn();
    mockPrismicApiService.getSingle = jest.fn();

    mockPrismicDocumentAdapterService = {} as PrismicDocumentAdapterService;
    mockPrismicDocumentAdapterService.getRichTextAsHtml = jest.fn();
    mockPrismicDocumentAdapterService.getValue = jest.fn();

    prismicHeaderRepository = new PrismicHeaderRepository(mockPrismicApiService, mockPrismicDocumentAdapterService);
    document = {
      type: 'header',
      data: {
        title: '',
        logo: {
          url: '',
          alt: '',
        },
      },
    } as Document;

    (mockPrismicApiService.getSingle as jest.Mock).mockReturnValue(Promise.resolve(document));
  });

  describe('findByLang()', () => {
    it('should call prismicApiService with header type and lang', async () => {
      // given
      const lang: string = 'fr-ca';

      // when
      await prismicHeaderRepository.findByLang(lang);

      // then
      expect(mockPrismicApiService.getSingle).toHaveBeenCalledWith('header', lang);
    });

    it('should build a header containing title', async () => {
      // given
      const title: string = 'A header title';
      document.data.title = title;
      (mockPrismicDocumentAdapterService.getValue as jest.Mock).mockReturnValue(title);

      // when
      await prismicHeaderRepository.findByLang('a-lang');

      // then
      const expected: HeaderRawInterface = { title } as HeaderRawInterface;

      const headerRaw: HeaderRawInterface = (Header as jest.Mock).mock.calls[0][0];
      expect(headerRaw).toMatchObject(expected);
      expect(mockPrismicDocumentAdapterService.getValue).toHaveBeenCalledWith(document, 'title');
    });
    it('should build a header containing logo', async () => {
      // given
      const url: string = 'http://my-image.png';
      const alt: string = 'A beautiful image';
      document.data.logo = { url, alt };
      (mockPrismicDocumentAdapterService.getValue as jest.Mock)
        .mockReturnValueOnce('title')
        .mockReturnValueOnce(url)
        .mockReturnValueOnce(alt);

      // when
      await prismicHeaderRepository.findByLang('a-lang');

      // then
      const expected: HeaderRawInterface = {
        logo: { source: url, alternativeText: alt },
      } as HeaderRawInterface;

      const headerRaw: HeaderRawInterface = (Header as jest.Mock).mock.calls[0][0];
      expect(headerRaw).toMatchObject(expected);
      expect(mockPrismicDocumentAdapterService.getValue).toHaveBeenCalledWith(document, 'logo.url');
      expect(mockPrismicDocumentAdapterService.getValue).toHaveBeenCalledWith(document, 'logo.alt');
    });
    it('should return a header', async () => {
      // given
      const expected: Header = { title: 'A header title' } as Header;
      (Header as jest.Mock).mockImplementation(() => expected);

      // when
      const result: HeaderInterface = await prismicHeaderRepository.findByLang('a-lang');

      // then
      expect(result).toBe(expected);
    });
  });
});

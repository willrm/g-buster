import { Document } from 'prismic-javascript/d.ts/documents';
import { Footer } from '../../../domain/footer/footer';
import { FooterInterface } from '../../../domain/footer/footer.interface';
import { FooterRawInterface } from '../../../domain/footer/footer.raw.interface';
import { PrismicApiService } from '../../vendors/prismic/prismic-api.service';
import { PrismicDocumentAdapterService } from '../../vendors/prismic/prismic-document-adapter.service';
import { PrismicFooterRepository } from '../prismic-footer.repository';

jest.mock('../../../domain/footer/footer');

describe('infrastructure/repositories/PrismicFooterRepository', () => {
  let prismicFooterRepository: PrismicFooterRepository;
  let mockPrismicApiService: PrismicApiService;
  let mockPrismicDocumentAdapterService: PrismicDocumentAdapterService;
  let document: Document;

  beforeEach(() => {
    (Footer as jest.Mock).mockClear();
    mockPrismicApiService = {} as PrismicApiService;
    mockPrismicApiService.getByUID = jest.fn();
    mockPrismicApiService.getSingle = jest.fn();

    mockPrismicDocumentAdapterService = {} as PrismicDocumentAdapterService;
    mockPrismicDocumentAdapterService.getRichTextAsHtml = jest.fn();
    mockPrismicDocumentAdapterService.getValue = jest.fn();

    prismicFooterRepository = new PrismicFooterRepository(mockPrismicApiService, mockPrismicDocumentAdapterService);

    document = {
      type: 'footer',
      data: {
        copyright: '',
      },
    } as Document;

    (mockPrismicApiService.getSingle as jest.Mock).mockReturnValue(Promise.resolve(document));
  });

  describe('findByLang()', () => {
    it('should call prismicApiService with footer type and lang', async () => {
      // given
      const lang: string = 'fr-ca';

      // when
      await prismicFooterRepository.findByLang(lang);

      // then
      expect(mockPrismicApiService.getSingle).toHaveBeenCalledWith('footer', lang);
    });

    it('should build a footer containing copyright', async () => {
      // given
      const copyright: string = 'A copyright';
      document.data.copyright = copyright;
      (mockPrismicDocumentAdapterService.getValue as jest.Mock).mockReturnValue(copyright);

      // when
      await prismicFooterRepository.findByLang('a-lang');

      // then
      const expected: FooterRawInterface = {
        copyright,
      } as FooterRawInterface;

      const footerRaw: FooterRawInterface = (Footer as jest.Mock).mock.calls[0][0];
      expect(footerRaw).toMatchObject(expected);
      expect(mockPrismicDocumentAdapterService.getValue).toHaveBeenCalledWith(document, 'copyright');
    });

    it('should return a footer', async () => {
      // given
      const expected: Footer = { copyright: 'A copyright' } as Footer;
      (Footer as jest.Mock).mockImplementation(() => expected);

      // when
      const result: FooterInterface = await prismicFooterRepository.findByLang('a-lang');

      // then
      expect(result).toBe(expected);
    });
  });
});

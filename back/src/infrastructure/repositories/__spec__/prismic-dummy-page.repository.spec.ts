import { Document } from 'prismic-javascript/d.ts/documents';
import { DummyPage } from '../../../domain/dummy-page/dummy-page';
import { DummyPageInterface } from '../../../domain/dummy-page/dummy-page.interface';
import { DummyPageRawInterface } from '../../../domain/dummy-page/dummy-page.raw.interface';
import { PrismicApiService } from '../../vendors/prismic/prismic-api.service';
import { PrismicDocumentAdapterService } from '../../vendors/prismic/prismic-document-adapter.service';
import { PrismicDummyPageRepository } from '../prismic-dummy-page.repository';

jest.mock('../../../domain/dummy-page/dummy-page');

describe('infrastructure/repositories/PrismicDummyPageRepository', () => {
  let prismicDummyPageRepository: PrismicDummyPageRepository;
  let mockPrismicApiService: PrismicApiService;
  let mockPrismicDocumentAdapterService: PrismicDocumentAdapterService;

  beforeEach(() => {
    (DummyPage as jest.Mock).mockClear();
    mockPrismicApiService = {} as PrismicApiService;
    mockPrismicApiService.getByUID = jest.fn();
    mockPrismicApiService.getSingle = jest.fn();

    mockPrismicDocumentAdapterService = {} as PrismicDocumentAdapterService;
    mockPrismicDocumentAdapterService.getRichTextAsHtml = jest.fn();
    mockPrismicDocumentAdapterService.getValue = jest.fn();

    prismicDummyPageRepository = new PrismicDummyPageRepository(mockPrismicApiService, mockPrismicDocumentAdapterService);
  });
  describe('findByLangAndUid()', () => {
    let document: Document;

    beforeEach(() => {
      document = {
        data: {
          image: {},
        },
      } as Document;

      (mockPrismicApiService.getByUID as jest.Mock).mockReturnValue(Promise.resolve(document));
    });

    it('should call prismicApiService with dummy_page type, uid and lang', async () => {
      // given
      const uid: string = 'an-uid';
      const lang: string = 'fr-ca';

      // when
      await prismicDummyPageRepository.findByLangAndUid(lang, uid);

      // then
      expect(mockPrismicApiService.getByUID).toHaveBeenCalledWith('dummy_page', uid, lang);
    });

    it('should build a dummy page containing image', async () => {
      // given
      const url: string = 'http://my-image.png';
      const alt: string = 'A beautiful image';
      document.data.image = { url, alt };
      (mockPrismicDocumentAdapterService.getValue as jest.Mock).mockReturnValueOnce(url).mockReturnValueOnce(alt);

      // when
      await prismicDummyPageRepository.findByLangAndUid('a-lang', 'a-uid');

      // then
      const expected: DummyPageRawInterface = {
        image: { source: url, alternativeText: alt },
      } as DummyPageRawInterface;

      const dummyPageRaw: DummyPageRawInterface = (DummyPage as jest.Mock).mock.calls[0][0];

      expect(dummyPageRaw).toMatchObject(expected);
      expect(mockPrismicDocumentAdapterService.getValue).toHaveBeenCalledWith(document, 'image.url');
      expect(mockPrismicDocumentAdapterService.getValue).toHaveBeenCalledWith(document, 'image.alt');
    });

    it('should build a dummy page containing title as html', async () => {
      // given
      const expectedTitle: string = '<h1>An awesome title</h1>';
      document.data.title = {};

      (mockPrismicDocumentAdapterService.getRichTextAsHtml as jest.Mock).mockReturnValue(expectedTitle);

      // when
      await prismicDummyPageRepository.findByLangAndUid('a-lang', 'a-uid');

      // then
      const expected: DummyPageRawInterface = {
        title: expectedTitle,
      } as DummyPageRawInterface;

      const dummyPageRaw: DummyPageRawInterface = (DummyPage as jest.Mock).mock.calls[0][0];

      expect(dummyPageRaw).toMatchObject(expected);
      expect(mockPrismicDocumentAdapterService.getRichTextAsHtml).toHaveBeenCalledWith(document, 'title');
    });

    it('should build a dummy page containing content as html', async () => {
      // given
      const expectedContent: string = '<span>An awesome content</span>';
      document.data.content = {};

      (mockPrismicDocumentAdapterService.getRichTextAsHtml as jest.Mock).mockReturnValue(expectedContent);

      // when
      await prismicDummyPageRepository.findByLangAndUid('a-lang', 'a-uid');

      // then
      const expected: DummyPageRawInterface = {
        content: expectedContent,
      } as DummyPageRawInterface;

      const dummyPageRaw: DummyPageRawInterface = (DummyPage as jest.Mock).mock.calls[0][0];

      expect(dummyPageRaw).toMatchObject(expected);
      expect(mockPrismicDocumentAdapterService.getRichTextAsHtml).toHaveBeenCalledWith(document, 'content');
    });

    it('should return a dummy page', async () => {
      // given
      const expected: DummyPage = { title: 'A dummy page title' } as DummyPage;
      (DummyPage as jest.Mock).mockImplementation(() => expected);

      // when
      const result: DummyPageInterface = await prismicDummyPageRepository.findByLangAndUid('a-lang', 'a-uid');

      // then
      expect(result).toBe(expected);
    });
  });
});

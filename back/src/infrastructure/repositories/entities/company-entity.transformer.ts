import { Injectable } from '@nestjs/common';
import { ValueTransformer } from 'typeorm';
import { CompanyInterface } from '../../../domain/company/company.interface';
import { SocialNetworksInterface } from '../../../domain/social-networks/social-networks.interface';
import { ValueInterface } from '../../../domain/value/value.interface';
import { CompanyEntity } from './company.entity';

@Injectable()
export class CompanyEntityTransformer implements ValueTransformer {
  private readonly COMPANY_VALUE_SEPARATOR: string = '###';

  from(companyEntity: CompanyEntity): CompanyInterface {
    const socialNetworks: SocialNetworksInterface =
      companyEntity.socialNetworksFacebook || companyEntity.socialNetworksLinkedIn
        ? {
            facebook: companyEntity.socialNetworksFacebook,
            linkedIn: companyEntity.socialNetworksLinkedIn,
          }
        : null;

    const values: ValueInterface[] = companyEntity.values.map(
      (valueAsStringWithSeparator: string): ValueInterface => {
        const valueAsStringArray: string[] = valueAsStringWithSeparator.split(this.COMPANY_VALUE_SEPARATOR);

        return {
          meaning: valueAsStringArray[0],
          justification: valueAsStringArray[1],
        };
      }
    );

    return {
      id: String(companyEntity.id),
      userId: companyEntity.userId,
      status: companyEntity.status,
      activityLocations: {
        quebec: companyEntity.activityLocationQuebec,
        canada: companyEntity.activityLocationCanada,
        international: companyEntity.activityLocationInternational,
        usa: companyEntity.activityLocationUsa,
      },
      address: companyEntity.address,
      bachelorsOfEngineeringNumber: companyEntity.bachelorsOfEngineeringNumber,
      businessSegment: companyEntity.businessSegment,
      creationDate: companyEntity.creationDate,
      geniusTypes: companyEntity.geniusTypes,
      logo: {
        base64: companyEntity.logoBase64,
        mimeType: companyEntity.logoMimeType,
      },
      name: companyEntity.name,
      presentation: companyEntity.presentation,
      revenue: companyEntity.revenue,
      revenueForRD: companyEntity.revenueForRD,
      size: companyEntity.size,
      socialNetworks,
      values,
      website: companyEntity.website,
    } as CompanyInterface;
  }

  to(company: CompanyInterface): CompanyEntity {
    const companyEntity: CompanyEntity = new CompanyEntity();
    companyEntity.id = company.id ? parseInt(company.id, 10) : null;
    companyEntity.userId = company.userId;
    companyEntity.status = company.status;
    companyEntity.activityLocationQuebec = company.activityLocations.quebec;
    companyEntity.activityLocationCanada = company.activityLocations.canada;
    companyEntity.activityLocationInternational = company.activityLocations.international;
    companyEntity.activityLocationUsa = company.activityLocations.usa;
    companyEntity.address = company.address;
    companyEntity.bachelorsOfEngineeringNumber = company.bachelorsOfEngineeringNumber;
    companyEntity.businessSegment = company.businessSegment;
    companyEntity.creationDate = company.creationDate;
    companyEntity.geniusTypes = company.geniusTypes;
    companyEntity.logoBase64 = company.logo.base64;
    companyEntity.logoMimeType = company.logo.mimeType;
    companyEntity.name = company.name;
    companyEntity.presentation = company.presentation;
    companyEntity.revenue = company.revenue;
    companyEntity.revenueForRD = company.revenueForRD;
    companyEntity.size = company.size;
    companyEntity.socialNetworksFacebook = company.socialNetworks ? company.socialNetworks.facebook : null;
    companyEntity.socialNetworksLinkedIn = company.socialNetworks ? company.socialNetworks.linkedIn : null;
    companyEntity.values = company.values.map(
      (value: ValueInterface): string => `${value.meaning}${this.COMPANY_VALUE_SEPARATOR}${value.justification}`
    );
    companyEntity.website = company.website;

    return companyEntity;
  }
}

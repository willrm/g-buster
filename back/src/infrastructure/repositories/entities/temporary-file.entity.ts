import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Uuid } from '../../../domain/type-aliases';
import { DATE_MAX_LENGTH, dateIsoStringValueTransformer, DEFAULT_MAX_LENGTH, MIME_TYPE_MAX_LENGTH, UUID_MAX_LENGTH } from './entity.utils';

@Entity({ name: 'temporary_file' })
export class TemporaryFileEntity {
  @PrimaryGeneratedColumn('increment', { name: 'id' })
  id: number;

  @Column({ name: 'base_64', type: 'text', nullable: false })
  base64: string;

  @Column({ name: 'mime_type', type: 'varchar', nullable: false, length: MIME_TYPE_MAX_LENGTH })
  mimeType: string;

  @Column({ name: 'filename', type: 'varchar', nullable: false, length: DEFAULT_MAX_LENGTH })
  filename: string;

  @Column({ name: 'uuid', type: 'varchar', nullable: false, length: UUID_MAX_LENGTH, unique: true })
  uuid: Uuid;

  @Column({ name: 'created_at', type: 'varchar', nullable: false, length: DATE_MAX_LENGTH, transformer: dateIsoStringValueTransformer })
  createdAt: Date;
}

import {
  CompanyBusinessSegment,
  CompanyGeniusType,
  CompanyRevenue,
  CompanyRevenueForRD,
  CompanySize,
  CompanyStatus,
} from '../../../../domain/company/company-enums';
import { CompanyInterface } from '../../../../domain/company/company.interface';
import { CompanyEntityTransformer } from '../company-entity.transformer';
import { CompanyEntity } from '../company.entity';

describe('infrastructure/repositories/entities/CompanyEntityTransformer', () => {
  let companyEntityTransformer: CompanyEntityTransformer;
  beforeEach(() => {
    companyEntityTransformer = new CompanyEntityTransformer();
  });

  describe('from()', () => {
    it('should transform CompanyEntity to CompanyInterface', () => {
      // given
      const companyEntity: CompanyEntity = {
        activityLocationCanada: 2,
        activityLocationInternational: 3,
        activityLocationQuebec: 1,
        activityLocationUsa: 4,
        address: 'an address',
        bachelorsOfEngineeringNumber: 42,
        businessSegment: 'BUSINESS_SEGMENT_INFORMATION_TECHNOLOGY',
        creationDate: new Date('2019-11-15T17:35:20'),
        geniusTypes: ['GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING', 'GENIUS_TYPE_BUILDING_ENGINEERING'],
        id: 123456789,
        status: 'STATUS_PENDING_VALIDATION',
        logoBase64: 'logo-as-base64',
        logoMimeType: 'image/type',
        name: 'company name',
        presentation: 'company presentation',
        revenue: 'REVENUE_BETWEEN_100k_AND_1M',
        revenueForRD: 'REVENUE_RD_MORE_THAN_20M',
        size: 'HUNDRED_TWENTY_TO_TWO_HUNDRED_NINETY_NINE',
        socialNetworksFacebook: 'https://www.facebook.com',
        socialNetworksLinkedIn: 'https://www.linkedin.com',
        userId: '987654321',
        values: ['value 1###justification 1', 'value 2###justification 2'],
        website: 'https://company.website',
      } as CompanyEntity;

      // when
      const result: CompanyInterface = companyEntityTransformer.from(companyEntity);

      // then
      expect(result).toStrictEqual({
        id: '123456789',
        userId: '987654321',
        status: CompanyStatus.STATUS_PENDING_VALIDATION,
        activityLocations: {
          quebec: 1,
          canada: 2,
          international: 3,
          usa: 4,
        },
        address: 'an address',
        bachelorsOfEngineeringNumber: 42,
        businessSegment: CompanyBusinessSegment.BUSINESS_SEGMENT_INFORMATION_TECHNOLOGY,
        creationDate: new Date('2019-11-15T17:35:20'),
        geniusTypes: [CompanyGeniusType.GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING, CompanyGeniusType.GENIUS_TYPE_BUILDING_ENGINEERING],
        logo: {
          base64: 'logo-as-base64',
          mimeType: 'image/type',
        },
        name: 'company name',
        presentation: 'company presentation',
        revenue: CompanyRevenue.REVENUE_BETWEEN_100k_AND_1M,
        revenueForRD: CompanyRevenueForRD.REVENUE_RD_MORE_THAN_20M,
        size: CompanySize.HUNDRED_TWENTY_TO_TWO_HUNDRED_NINETY_NINE,
        socialNetworks: {
          facebook: `https://www.facebook.com`,
          linkedIn: `https://www.linkedin.com`,
        },
        values: [
          { meaning: 'value 1', justification: 'justification 1' },
          { meaning: 'value 2', justification: 'justification 2' },
        ],
        website: 'https://company.website',
      } as CompanyInterface);
    });

    it('should transform CompanyEntity to CompanyInterface when no social networks', () => {
      // given
      const companyEntity: CompanyEntity = {
        activityLocationQuebec: 1,
        address: 'an address',
        businessSegment: 'BUSINESS_SEGMENT_PULP_AND_PAPER',
        creationDate: new Date('2019-11-15T17:35:20'),
        geniusTypes: ['GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING'],
        id: 123456789,
        status: 'STATUS_PENDING_VALIDATION',
        logoBase64: 'logo-as-base64',
        logoMimeType: 'image/type',
        name: 'company name',
        presentation: 'company presentation',
        revenue: 'REVENUE_BETWEEN_5M_AND_20M',
        size: 'EIGHTY_TO_HUNDRED_NINETEEN',
        socialNetworksFacebook: null,
        socialNetworksLinkedIn: null,
        userId: '987654321',
        values: ['value###jusitification'],
        website: 'a website',
      } as CompanyEntity;

      // when
      const result: CompanyInterface = companyEntityTransformer.from(companyEntity);

      // then
      expect(result).toStrictEqual({
        activityLocations: {
          quebec: 1,
          canada: undefined,
          international: undefined,
          usa: undefined,
        },
        address: 'an address',
        bachelorsOfEngineeringNumber: undefined,
        businessSegment: 'BUSINESS_SEGMENT_PULP_AND_PAPER',
        creationDate: new Date('2019-11-15T17:35:20'),
        geniusTypes: ['GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING'],
        id: '123456789',
        status: 'STATUS_PENDING_VALIDATION',
        logo: {
          base64: 'logo-as-base64',
          mimeType: 'image/type',
        },
        name: 'company name',
        presentation: 'company presentation',
        revenue: 'REVENUE_BETWEEN_5M_AND_20M',
        revenueForRD: undefined,
        size: 'EIGHTY_TO_HUNDRED_NINETEEN',
        socialNetworks: null,
        userId: '987654321',
        values: [
          {
            justification: 'jusitification',
            meaning: 'value',
          },
        ],
        website: 'a website',
      } as CompanyInterface);
    });
  });

  describe('to()', () => {
    it('should transform CompanyInterface to CompanyEntity', () => {
      // given
      const company: CompanyInterface = {
        id: '123456789',
        userId: '987654321',
        status: CompanyStatus.STATUS_PENDING_VALIDATION,
        activityLocations: {
          quebec: 1,
          canada: 2,
          international: 3,
          usa: 4,
        },
        address: 'an address',
        bachelorsOfEngineeringNumber: 42,
        businessSegment: CompanyBusinessSegment.BUSINESS_SEGMENT_INFORMATION_TECHNOLOGY,
        creationDate: new Date('2019-11-15T17:35:20'),
        geniusTypes: [CompanyGeniusType.GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING, CompanyGeniusType.GENIUS_TYPE_BUILDING_ENGINEERING],
        logo: {
          base64: 'logo-as-base64',
          mimeType: 'image/type',
        },
        name: 'company name',
        presentation: 'company presentation',
        revenue: CompanyRevenue.REVENUE_BETWEEN_100k_AND_1M,
        revenueForRD: CompanyRevenueForRD.REVENUE_RD_MORE_THAN_20M,
        size: CompanySize.HUNDRED_TWENTY_TO_TWO_HUNDRED_NINETY_NINE,
        socialNetworks: {
          facebook: `https://www.facebook.com`,
          linkedIn: `https://www.linkedin.com`,
        },
        values: [
          { meaning: 'value 1', justification: 'justification 1' },
          { meaning: 'value 2', justification: 'justification 2' },
        ],
        website: 'https://company.website',
      };

      // when
      const result: CompanyEntity = companyEntityTransformer.to(company);

      // then
      expect(result).toMatchObject({
        activityLocationCanada: 2,
        activityLocationInternational: 3,
        activityLocationQuebec: 1,
        activityLocationUsa: 4,
        address: 'an address',
        bachelorsOfEngineeringNumber: 42,
        businessSegment: 'BUSINESS_SEGMENT_INFORMATION_TECHNOLOGY',
        creationDate: new Date('2019-11-15T17:35:20'),
        geniusTypes: ['GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING', 'GENIUS_TYPE_BUILDING_ENGINEERING'],
        id: 123456789,
        logoBase64: 'logo-as-base64',
        logoMimeType: 'image/type',
        name: 'company name',
        presentation: 'company presentation',
        revenue: 'REVENUE_BETWEEN_100k_AND_1M',
        revenueForRD: 'REVENUE_RD_MORE_THAN_20M',
        size: 'HUNDRED_TWENTY_TO_TWO_HUNDRED_NINETY_NINE',
        socialNetworksFacebook: 'https://www.facebook.com',
        socialNetworksLinkedIn: 'https://www.linkedin.com',
        userId: '987654321',
        values: ['value 1###justification 1', 'value 2###justification 2'],
        website: 'https://company.website',
      } as CompanyEntity);
    });

    it('should transform CompanyInterface to CompanyEntity when no social networks', () => {
      // given
      const company: CompanyInterface = {
        activityLocations: { quebec: 1 },
        address: 'an address',
        businessSegment: CompanyBusinessSegment.BUSINESS_SEGMENT_PULP_AND_PAPER,
        creationDate: new Date('2019-11-15T17:35:20'),
        geniusTypes: [CompanyGeniusType.GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING],
        id: '123456789',
        status: CompanyStatus.STATUS_PENDING_VALIDATION,
        logo: {
          base64: 'logo-as-base64',
          mimeType: 'image/type',
        },
        name: 'company name',
        presentation: 'company presentation',
        revenue: CompanyRevenue.REVENUE_BETWEEN_5M_AND_20M,
        size: CompanySize.EIGHTY_TO_HUNDRED_NINETEEN,
        userId: '987654321',
        values: [{ meaning: 'value', justification: 'jusitification' }],
        website: 'a website',
        socialNetworks: null,
      };

      // when
      const result: CompanyEntity = companyEntityTransformer.to(company);

      // then
      expect(result).toMatchObject({
        activityLocationQuebec: 1,
        address: 'an address',
        businessSegment: 'BUSINESS_SEGMENT_PULP_AND_PAPER',
        creationDate: new Date('2019-11-15T17:35:20'),
        geniusTypes: ['GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING'],
        id: 123456789,
        logoBase64: 'logo-as-base64',
        logoMimeType: 'image/type',
        name: 'company name',
        presentation: 'company presentation',
        revenue: 'REVENUE_BETWEEN_5M_AND_20M',
        size: 'EIGHTY_TO_HUNDRED_NINETEEN',
        socialNetworksFacebook: null,
        socialNetworksLinkedIn: null,
        userId: '987654321',
        values: ['value###jusitification'],
        website: 'a website',
      } as CompanyEntity);
    });

    it('should transform CompanyInterface to CompanyEntity when no id', () => {
      // given
      const company: CompanyInterface = {
        activityLocations: { quebec: 1 },
        address: 'an address',
        businessSegment: CompanyBusinessSegment.BUSINESS_SEGMENT_PULP_AND_PAPER,
        creationDate: new Date('2019-11-15T17:35:20'),
        geniusTypes: [CompanyGeniusType.GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING],
        id: null,
        status: CompanyStatus.STATUS_PENDING_VALIDATION,
        logo: {
          base64: 'logo-as-base64',
          mimeType: 'image/type',
        },
        name: 'company name',
        presentation: 'company presentation',
        revenue: CompanyRevenue.REVENUE_BETWEEN_5M_AND_20M,
        size: CompanySize.EIGHTY_TO_HUNDRED_NINETEEN,
        userId: '987654321',
        values: [{ meaning: 'value', justification: 'jusitification' }],
        website: 'a website',
        socialNetworks: null,
      };

      // when
      const result: CompanyEntity = companyEntityTransformer.to(company);

      // then
      expect(result).toMatchObject({
        activityLocationQuebec: 1,
        address: 'an address',
        businessSegment: 'BUSINESS_SEGMENT_PULP_AND_PAPER',
        creationDate: new Date('2019-11-15T17:35:20'),
        geniusTypes: ['GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING'],
        id: null,
        logoBase64: 'logo-as-base64',
        logoMimeType: 'image/type',
        name: 'company name',
        presentation: 'company presentation',
        revenue: 'REVENUE_BETWEEN_5M_AND_20M',
        size: 'EIGHTY_TO_HUNDRED_NINETEEN',
        socialNetworksFacebook: null,
        socialNetworksLinkedIn: null,
        userId: '987654321',
        values: ['value###jusitification'],
        website: 'a website',
      } as CompanyEntity);
    });
  });
});

import { TemporaryFileInterface } from '../../../../domain/temporary-file/temporary-file.interface';
import { TemporaryFileEntityTransformer } from '../temporary-file-entity.transformer';
import { TemporaryFileEntity } from '../temporary-file.entity';

describe('infrastructure/repositories/entities/TemporaryFileEntityTransformer', () => {
  let temporaryFileEntityTransformer: TemporaryFileEntityTransformer;
  beforeEach(() => {
    temporaryFileEntityTransformer = new TemporaryFileEntityTransformer();
  });

  describe('from()', () => {
    it('should transform TemporaryFileEntity to TemporaryFileInterface', () => {
      // given
      const temporaryFileEntity: TemporaryFileEntity = {
        id: 123456789,
        base64: 'an-image-as-base64',
        mimeType: 'image/type',
        filename: 'filename.extension',
        uuid: '336880f8-b393-4c68-a6df-07f6051263e2',
        createdAt: new Date('2019-11-15T17:35:20'),
      } as TemporaryFileEntity;

      // when
      const result: TemporaryFileInterface = temporaryFileEntityTransformer.from(temporaryFileEntity);

      // then
      expect(result).toStrictEqual({
        id: '123456789',
        base64: 'an-image-as-base64',
        mimeType: 'image/type',
        filename: 'filename.extension',
        uuid: '336880f8-b393-4c68-a6df-07f6051263e2',
        createdAt: new Date('2019-11-15T17:35:20'),
      } as TemporaryFileInterface);
    });
  });

  describe('to()', () => {
    it('should transform TemporaryFileInterface to TemporaryFileEntity', () => {
      // given
      const temporaryFile: TemporaryFileInterface = {
        id: '123456789',
        base64: 'an-image-as-base64',
        mimeType: 'image/type',
        filename: 'filename.extension',
        uuid: '336880f8-b393-4c68-a6df-07f6051263e2',
        createdAt: new Date('2019-11-15T17:35:20'),
      } as TemporaryFileInterface;

      // when
      const result: TemporaryFileEntity = temporaryFileEntityTransformer.to(temporaryFile);

      // then
      expect(result).toMatchObject({
        id: 123456789,
        base64: 'an-image-as-base64',
        mimeType: 'image/type',
        filename: 'filename.extension',
        uuid: '336880f8-b393-4c68-a6df-07f6051263e2',
        createdAt: new Date('2019-11-15T17:35:20'),
      } as TemporaryFileEntity);
    });

    it('should transform TemporaryFileInterface to TemporaryFileEntity when no id', () => {
      // given
      const temporaryFile: TemporaryFileInterface = {
        id: null,
        base64: 'an-image-as-base64',
        mimeType: 'image/type',
        filename: 'filename.extension',
        uuid: '336880f8-b393-4c68-a6df-07f6051263e2',
        createdAt: new Date('2019-11-15T17:35:20'),
      } as TemporaryFileInterface;

      // when
      const result: TemporaryFileEntity = temporaryFileEntityTransformer.to(temporaryFile);

      // then
      expect(result).toMatchObject({
        id: null,
        base64: 'an-image-as-base64',
        mimeType: 'image/type',
        filename: 'filename.extension',
        uuid: '336880f8-b393-4c68-a6df-07f6051263e2',
        createdAt: new Date('2019-11-15T17:35:20'),
      } as TemporaryFileEntity);
    });
  });
});

import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import {
  COMPANY_ADDRESS_MAX_LENGTH,
  COMPANY_NAME_MAX_LENGTH,
  COMPANY_PRESENTATION_MAX_LENGTH,
  COMPANY_WEBSITE_MAX_LENGTH,
} from '../../../domain/company/company-constraints';
import { UserId } from '../../../domain/type-aliases';
import {
  arrayValueTransformer,
  DATE_MAX_LENGTH,
  dateIsoStringValueTransformer,
  DEFAULT_MAX_LENGTH,
  ENUM_VALUE_MAX_LENGTH,
  MIME_TYPE_MAX_LENGTH,
} from './entity.utils';

@Entity({ name: 'company' })
export class CompanyEntity {
  @PrimaryGeneratedColumn('increment', { name: 'id' })
  id: number;

  @Column({ name: 'user_id', type: 'varchar', nullable: false, length: DEFAULT_MAX_LENGTH })
  userId: UserId;

  @Column({ name: 'status', type: 'varchar', nullable: false, length: ENUM_VALUE_MAX_LENGTH })
  status: string;

  @Column({ name: 'activity_location_quebec', type: 'smallint', nullable: false })
  activityLocationQuebec: number;

  @Column({ name: 'activity_location_canada', type: 'smallint', nullable: true })
  activityLocationCanada?: number;

  @Column({ name: 'activity_location_international', type: 'smallint', nullable: true })
  activityLocationInternational?: number;

  @Column({ name: 'activity_location_usa', type: 'smallint', nullable: true })
  activityLocationUsa?: number;

  @Column({ name: 'address', type: 'varchar', nullable: false, length: COMPANY_ADDRESS_MAX_LENGTH })
  address: string;

  @Column({ name: 'bachelors_of_engineering_number', type: 'smallint', nullable: true })
  bachelorsOfEngineeringNumber?: number;

  @Column({ name: 'business_segment', type: 'varchar', nullable: false, length: ENUM_VALUE_MAX_LENGTH })
  businessSegment: string;

  @Column({ name: 'creation_date', type: 'varchar', nullable: false, length: DATE_MAX_LENGTH, transformer: dateIsoStringValueTransformer })
  creationDate: Date;

  @Column({ name: 'genius_types', type: 'text', nullable: false, transformer: arrayValueTransformer })
  geniusTypes: string[];

  @Column({ name: 'logo_base_64', type: 'text', nullable: false })
  logoBase64: string;

  @Column({ name: 'logo_mime_type', type: 'varchar', nullable: false, length: MIME_TYPE_MAX_LENGTH })
  logoMimeType: string;

  @Column({ name: 'name', type: 'varchar', nullable: false, length: COMPANY_NAME_MAX_LENGTH })
  name: string;

  @Column({ name: 'presentation', type: 'varchar', nullable: false, length: COMPANY_PRESENTATION_MAX_LENGTH })
  presentation: string;

  @Column({ name: 'revenue', type: 'varchar', nullable: false, length: ENUM_VALUE_MAX_LENGTH })
  revenue: string;

  @Column({ name: 'revenue_for_rd', type: 'varchar', nullable: true, length: ENUM_VALUE_MAX_LENGTH })
  revenueForRD?: string;

  @Column({ name: 'size', type: 'varchar', nullable: false, length: ENUM_VALUE_MAX_LENGTH })
  size: string;

  @Column({ name: 'social_networks_facebook', type: 'varchar', nullable: true, length: DEFAULT_MAX_LENGTH })
  socialNetworksFacebook?: string;

  @Column({ name: 'social_networks_linked_in', type: 'varchar', nullable: true, length: DEFAULT_MAX_LENGTH })
  socialNetworksLinkedIn?: string;

  @Column({ name: 'values', type: 'text', nullable: false, transformer: arrayValueTransformer })
  values: string[];

  @Column({ name: 'website', type: 'varchar', nullable: false, length: COMPANY_WEBSITE_MAX_LENGTH })
  website: string;
}

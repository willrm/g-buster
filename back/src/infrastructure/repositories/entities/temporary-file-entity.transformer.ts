import { Injectable } from '@nestjs/common';
import { ValueTransformer } from 'typeorm';
import { TemporaryFileInterface } from '../../../domain/temporary-file/temporary-file.interface';
import { TemporaryFileEntity } from './temporary-file.entity';

@Injectable()
export class TemporaryFileEntityTransformer implements ValueTransformer {
  from(temporaryFileEntity: TemporaryFileEntity): TemporaryFileInterface {
    return {
      id: String(temporaryFileEntity.id),
      base64: temporaryFileEntity.base64,
      mimeType: temporaryFileEntity.mimeType,
      filename: temporaryFileEntity.filename,
      uuid: temporaryFileEntity.uuid,
      createdAt: temporaryFileEntity.createdAt,
    } as TemporaryFileInterface;
  }

  to(temporaryFile: TemporaryFileInterface): TemporaryFileEntity {
    const temporaryFileEntity: TemporaryFileEntity = new TemporaryFileEntity();
    temporaryFileEntity.id = temporaryFile.id ? parseInt(temporaryFile.id, 10) : null;
    temporaryFileEntity.base64 = temporaryFile.base64;
    temporaryFileEntity.mimeType = temporaryFile.mimeType;
    temporaryFileEntity.filename = temporaryFile.filename;
    temporaryFileEntity.uuid = temporaryFile.uuid;
    temporaryFileEntity.createdAt = temporaryFile.createdAt;

    return temporaryFileEntity;
  }
}

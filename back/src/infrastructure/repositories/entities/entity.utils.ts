import { isEmpty } from 'lodash';
import { ValueTransformer } from 'typeorm';

export const DATE_MAX_LENGTH: number = 24;
export const ENUM_VALUE_MAX_LENGTH: number = 64;
export const MIME_TYPE_MAX_LENGTH: number = 128;
export const UUID_MAX_LENGTH: number = 36;
export const DEFAULT_MAX_LENGTH: number = 255;

const ARRAY_ITEM_SEPARATOR: string = '|||';

export const dateIsoStringValueTransformer: ValueTransformer = {
  from: (dateAsISOString: string): Date => {
    return new Date(dateAsISOString);
  },
  to: (date: Date): string => {
    return date.toISOString();
  },
} as ValueTransformer;

export const arrayValueTransformer: ValueTransformer = {
  from: (stringWithSeparators: string): string[] => {
    return isEmpty(stringWithSeparators) ? null : stringWithSeparators.split('|||');
  },
  to: (array: string[]): string => {
    return isEmpty(array) ? null : array.join(ARRAY_ITEM_SEPARATOR);
  },
} as ValueTransformer;

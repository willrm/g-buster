import { Injectable } from '@nestjs/common';
import { CandidateInterface } from '../../domain/candidate/candidate.interface';
import { CandidateRepository } from '../../domain/candidate/candidate.repository';
import { ItemNotFoundError } from '../../domain/common-errors/item-not-found.error';
import { TemporaryFileInterface } from '../../domain/temporary-file/temporary-file.interface';
import { CandidateId, JobOfferId } from '../../domain/type-aliases';
import { DomainFieldToZohoModelFieldMappingStrategyInterface } from '../vendors/zoho/domain-to-zoho-model-mapping-strategy';
import { ZohoJsonFieldInterface } from '../vendors/zoho/models/json/zoho-json-field.interface';
import { ZohoJsonGetRecordsResponseInterface } from '../vendors/zoho/models/json/zoho-json-get-records-response.interface';
import { ZohoJsonPostRecordsRequestInterface } from '../vendors/zoho/models/json/zoho-json-post-records-request.interface';
import { ZohoJsonRowInterface } from '../vendors/zoho/models/json/zoho-json-row.interface';
import { ZohoApiModule, ZohoFileType } from '../vendors/zoho/zoho-api.service';
import { ZohoCachedApiService } from '../vendors/zoho/zoho-cached-api.service';
import { ZohoRequestAdapterService } from '../vendors/zoho/zoho-request-adapter.service';
import { ZohoResponseAdapterService } from '../vendors/zoho/zoho-response-adapter.service';
import { ZohoSearchCondition, ZohoSearchConditionOperator } from '../vendors/zoho/zoho-search-condition';
import { CandidateDomainToCandidateZohoModelMappingStrategy } from './mapping-strategies/candidate-domain-to-candidate-zoho-model-mapping-strategy';
import { CandidateZohoModelToCandidateDomainMappingStrategy } from './mapping-strategies/candidate-zoho-model-to-candidate-domain-mapping-strategy';

@Injectable()
export class ZohoCandidateRepository implements CandidateRepository {
  private readonly ZOHO_API_MODULE: ZohoApiModule = ZohoApiModule.CANDIDATES;

  constructor(
    private readonly zohoCachedApiService: ZohoCachedApiService,
    private readonly zohoRequestAdapterService: ZohoRequestAdapterService,
    private readonly zohoResponseAdapterService: ZohoResponseAdapterService,
    private readonly candidateDomainToCandidateZohoModelMappingStrategy: CandidateDomainToCandidateZohoModelMappingStrategy,
    private readonly candidateZohoModelToCandidateDomainMappingStrategy: CandidateZohoModelToCandidateDomainMappingStrategy
  ) {}

  async findByEmail(email: string): Promise<CandidateInterface> {
    const emailSearchCondition: ZohoSearchCondition = this.buildZohoSearchCondition<CandidateInterface>(
      'email',
      ZohoSearchConditionOperator.IS,
      email
    );
    const zohoJsonGetRecordsResponse: ZohoJsonGetRecordsResponseInterface = await this.zohoCachedApiService.getSearchRecords(
      this.ZOHO_API_MODULE,
      emailSearchCondition
    );

    const candidates: CandidateInterface[] = await this.toCandidates(zohoJsonGetRecordsResponse);
    if (candidates.length === 0) {
      return Promise.reject(new ItemNotFoundError('Candidate not found'));
    }

    return candidates[0];
  }

  async save(
    candidate: CandidateInterface,
    resumeTemporaryFile: TemporaryFileInterface,
    coverLetterTemporaryFile: TemporaryFileInterface
  ): Promise<CandidateId> {
    const zohoJsonFields: ZohoJsonFieldInterface[] = this.zohoRequestAdapterService.map<CandidateInterface>(
      candidate,
      this.candidateDomainToCandidateZohoModelMappingStrategy
    );

    const zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface = {
      Candidates: { row: [{ no: '1', FL: zohoJsonFields }] },
    };

    if (candidate.id) {
      return this.zohoCachedApiService.updateRecords(this.ZOHO_API_MODULE, candidate.id, zohoJsonPostRecordsRequest);
    } else {
      const createdCandidateId: string = await this.zohoCachedApiService.addRecords(this.ZOHO_API_MODULE, zohoJsonPostRecordsRequest);
      await this.uploadFiles(createdCandidateId, resumeTemporaryFile, coverLetterTemporaryFile);

      return createdCandidateId;
    }
  }

  async applyToJobOffer(candidateId: CandidateId, jobOfferId: JobOfferId): Promise<void> {
    await this.zohoCachedApiService.associateJobOpening(ZohoApiModule.CANDIDATES, candidateId, jobOfferId);
  }

  private toCandidates(zohoJsonGetRecordsResponse: ZohoJsonGetRecordsResponseInterface): Promise<CandidateInterface[]> {
    const candidates: CandidateInterface[] = zohoJsonGetRecordsResponse.response.result.Candidates.row.map((zohoJsonRow: ZohoJsonRowInterface) => {
      return this.zohoResponseAdapterService.map<CandidateInterface>(zohoJsonRow.FL, this.candidateZohoModelToCandidateDomainMappingStrategy);
    });

    return Promise.resolve(candidates);
  }

  private buildZohoSearchCondition<T>(field: keyof T, operator: ZohoSearchConditionOperator, value: unknown): ZohoSearchCondition {
    const mappingStrategy: DomainFieldToZohoModelFieldMappingStrategyInterface = this.candidateDomainToCandidateZohoModelMappingStrategy[
      field as string
    ];

    return {
      field: mappingStrategy.target,
      operator,
      value: mappingStrategy.map(value),
    } as ZohoSearchCondition;
  }

  private async uploadFiles(
    candidateId: string,
    resumeTemporaryFile: TemporaryFileInterface,
    coverLetterTemporaryFile: TemporaryFileInterface
  ): Promise<void> {
    await this.zohoCachedApiService.uploadFile(
      this.ZOHO_API_MODULE,
      candidateId,
      ZohoFileType.RESUME,
      resumeTemporaryFile.base64,
      resumeTemporaryFile.filename,
      resumeTemporaryFile.mimeType
    );

    await this.zohoCachedApiService.uploadFile(
      this.ZOHO_API_MODULE,
      candidateId,
      ZohoFileType.COVER_LETTER,
      coverLetterTemporaryFile.base64,
      coverLetterTemporaryFile.filename,
      coverLetterTemporaryFile.mimeType
    );
  }
}

import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { isEmpty } from 'lodash';
import { ItemNotFoundError } from '../../domain/common-errors/item-not-found.error';
import { UserInterface } from '../../domain/user/user.interface';
import { UserRepository } from '../../domain/user/user.repository.interface';
import { EnvironmentConfigService } from '../config/environment-config/environment-config.service';

@Injectable({ scope: Scope.REQUEST })
export class LocalUserRepository implements UserRepository {
  constructor(
    @Inject(REQUEST)
    private readonly request: Request,
    private readonly environmentConfigService: EnvironmentConfigService
  ) {}

  async findAllAdminEmailAddresses(): Promise<string[]> {
    return Promise.resolve(
      this.environmentConfigService
        .get('GBUSTER_ADMIN_EMAIL_ADDRESSES')
        .replace(/\s/g, '')
        .split(',')
    );
  }

  async logIn(user: UserInterface): Promise<void> {
    this.request.session.user = user;
  }

  async getCurrent(): Promise<UserInterface> {
    const userInSession: UserInterface = this.request.session.user;

    if (isEmpty(userInSession)) {
      return Promise.reject(new ItemNotFoundError('User not found'));
    } else {
      return Promise.resolve(userInSession);
    }
  }
}

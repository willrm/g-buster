import { Injectable } from '@nestjs/common';
import { ItemNotFoundError } from '../../domain/common-errors/item-not-found.error';
import { JobOfferStatus } from '../../domain/job-offer/job-offer-enums';
import { JobOfferInterface } from '../../domain/job-offer/job-offer.interface';
import { JobOfferRepository } from '../../domain/job-offer/job-offer.repository.interface';
import { JobOfferId, UserId } from '../../domain/type-aliases';
import { DomainFieldToZohoModelFieldMappingStrategyInterface } from '../vendors/zoho/domain-to-zoho-model-mapping-strategy';
import { ZohoJsonFieldInterface } from '../vendors/zoho/models/json/zoho-json-field.interface';
import { ZohoJsonGetRecordsResponseInterface } from '../vendors/zoho/models/json/zoho-json-get-records-response.interface';
import { ZohoJsonPostRecordsRequestInterface } from '../vendors/zoho/models/json/zoho-json-post-records-request.interface';
import { ZohoJsonRowInterface } from '../vendors/zoho/models/json/zoho-json-row.interface';
import { ZohoApiModule } from '../vendors/zoho/zoho-api.service';
import { ZohoCachedApiService } from '../vendors/zoho/zoho-cached-api.service';
import { ZohoRequestAdapterService } from '../vendors/zoho/zoho-request-adapter.service';
import { ZohoResponseAdapterService } from '../vendors/zoho/zoho-response-adapter.service';
import { ZohoSearchCondition, ZohoSearchConditionOperator } from '../vendors/zoho/zoho-search-condition';
import { JobOfferDomainToJobOpeningZohoModelMappingStrategy } from './mapping-strategies/job-offer-domain-to-job-opening-zoho-model-mapping-strategy';
import { JobOpeningZohoModelToJobOfferDomainMappingStrategy } from './mapping-strategies/job-opening-zoho-model-to-job-offer-domain-mapping-strategy';

@Injectable()
export class ZohoJobOfferRepository implements JobOfferRepository {
  private readonly ZOHO_API_MODULE: ZohoApiModule = ZohoApiModule.JOB_OPENINGS;

  constructor(
    private readonly zohoCachedApiService: ZohoCachedApiService,
    private readonly zohoRequestAdapterService: ZohoRequestAdapterService,
    private readonly zohoResponseAdapterService: ZohoResponseAdapterService,
    private readonly jobOfferDomainToJobOpeningZohoModelMappingStrategy: JobOfferDomainToJobOpeningZohoModelMappingStrategy,
    private readonly jobOpeningZohoModelToJobOfferDomainMappingStrategy: JobOpeningZohoModelToJobOfferDomainMappingStrategy
  ) {}

  async findById(id: JobOfferId): Promise<JobOfferInterface> {
    const zohoJsonGetRecordsResponse: ZohoJsonGetRecordsResponseInterface = await this.zohoCachedApiService.getRecordById(this.ZOHO_API_MODULE, id);
    const jobOfferMap: JobOfferInterface[] = await this.toJobOffers(zohoJsonGetRecordsResponse);
    if (jobOfferMap.length === 0) {
      return Promise.reject(new ItemNotFoundError(`JobOffer not found with id "${id}"`));
    }

    return Promise.resolve(jobOfferMap[0]);
  }

  async findAll(): Promise<JobOfferInterface[]> {
    const zohoJsonGetRecordsResponse: ZohoJsonGetRecordsResponseInterface = await this.zohoCachedApiService.getRecords(this.ZOHO_API_MODULE);

    return this.toJobOffers(zohoJsonGetRecordsResponse);
  }

  async findAllByUserId(userId: UserId): Promise<JobOfferInterface[]> {
    const userIdEqualSearchCondition: ZohoSearchCondition = this.buildZohoSearchCondition<JobOfferInterface>(
      'userId',
      ZohoSearchConditionOperator.IS,
      userId
    );
    const zohoJsonGetRecordsResponse: ZohoJsonGetRecordsResponseInterface = await this.zohoCachedApiService.getSearchRecords(
      this.ZOHO_API_MODULE,
      userIdEqualSearchCondition
    );

    return this.toJobOffers(zohoJsonGetRecordsResponse);
  }

  async findAllByStatus(status: JobOfferStatus): Promise<JobOfferInterface[]> {
    const statusSearchCondition: ZohoSearchCondition = this.buildZohoSearchCondition<JobOfferInterface>(
      'status',
      ZohoSearchConditionOperator.IS,
      status
    );
    const zohoJsonGetRecordsResponse: ZohoJsonGetRecordsResponseInterface = await this.zohoCachedApiService.getSearchRecords(
      this.ZOHO_API_MODULE,
      statusSearchCondition
    );

    return this.toJobOffers(zohoJsonGetRecordsResponse);
  }

  async save(jobOffer: JobOfferInterface): Promise<JobOfferId> {
    const zohoJsonFields: ZohoJsonFieldInterface[] = this.zohoRequestAdapterService.map<JobOfferInterface>(
      jobOffer,
      this.jobOfferDomainToJobOpeningZohoModelMappingStrategy
    );

    const zohoJsonPostRecordsRequest: ZohoJsonPostRecordsRequestInterface = {
      JobOpenings: { row: [{ no: '1', FL: zohoJsonFields }] },
    };

    if (jobOffer.id) {
      const zohoStatus: string = this.jobOfferDomainToJobOpeningZohoModelMappingStrategy.status.map(jobOffer.status);
      await this.zohoCachedApiService.changeStatus(this.ZOHO_API_MODULE, jobOffer.id, zohoStatus);

      return this.zohoCachedApiService.updateRecords(this.ZOHO_API_MODULE, jobOffer.id, zohoJsonPostRecordsRequest);
    } else {
      return this.zohoCachedApiService.addRecords(this.ZOHO_API_MODULE, zohoJsonPostRecordsRequest);
    }
  }

  private toJobOffers(zohoJsonGetRecordsResponse: ZohoJsonGetRecordsResponseInterface): Promise<JobOfferInterface[]> {
    const jobOffers: JobOfferInterface[] = zohoJsonGetRecordsResponse.response.result.JobOpenings.row.map((zohoJsonRow: ZohoJsonRowInterface) => {
      return this.zohoResponseAdapterService.map<JobOfferInterface>(zohoJsonRow.FL, this.jobOpeningZohoModelToJobOfferDomainMappingStrategy);
    });

    return Promise.resolve(jobOffers);
  }

  private buildZohoSearchCondition<T>(field: keyof T, operator: ZohoSearchConditionOperator, value: unknown): ZohoSearchCondition {
    const mappingStrategy: DomainFieldToZohoModelFieldMappingStrategyInterface = this.jobOfferDomainToJobOpeningZohoModelMappingStrategy[
      field as string
    ];

    return {
      field: mappingStrategy.target,
      operator,
      value: mappingStrategy.map(value),
    } as ZohoSearchCondition;
  }
}

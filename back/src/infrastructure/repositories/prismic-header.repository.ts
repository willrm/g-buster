import { Injectable } from '@nestjs/common';
import { Document } from 'prismic-javascript/d.ts/documents';
import { Header } from '../../domain/header/header';
import { HeaderInterface } from '../../domain/header/header.interface';
import { HeaderRawInterface } from '../../domain/header/header.raw.interface';
import { HeaderRepository } from '../../domain/header/header.repository.interface';
import { ImageRawInterface } from '../../domain/image/image.raw.interface';
import { Lang } from '../../domain/type-aliases';
import { PrismicApiService } from '../vendors/prismic/prismic-api.service';
import { PrismicDocumentAdapterService } from '../vendors/prismic/prismic-document-adapter.service';

@Injectable()
export class PrismicHeaderRepository implements HeaderRepository {
  private readonly HEADER_CONTENT_TYPE: string = 'header';

  constructor(private readonly prismicApiService: PrismicApiService, private readonly prismicDocumentAdapterService: PrismicDocumentAdapterService) {}

  async findByLang(lang: Lang): Promise<HeaderInterface> {
    const document: Document = await this.prismicApiService.getSingle(this.HEADER_CONTENT_TYPE, lang);

    return new Header({
      title: this.prismicDocumentAdapterService.getValue(document, 'title'),
      logo: this.getImage(document, 'logo'),
    } as HeaderRawInterface);
  }

  // PRIVATE METHODS

  private getImage(document: Document, field: string): ImageRawInterface {
    return {
      source: this.prismicDocumentAdapterService.getValue(document, `${field}.url`),
      alternativeText: this.prismicDocumentAdapterService.getValue(document, `${field}.alt`),
    };
  }
}

import { Injectable } from '@nestjs/common';
import { DomainToZohoModelMappingStrategy } from '../../vendors/zoho/domain-to-zoho-model-mapping-strategy';

@Injectable()
export class JobOfferDomainToJobOpeningZohoModelMappingStrategy extends DomainToZohoModelMappingStrategy {
  constructor() {
    super();

    this.id = {
      target: 'JOBOPENINGID',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
    this.userId = {
      target: 'Auth0 user id',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
    this.companyId = {
      target: 'G-Buster company id',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
    this.status = {
      target: 'Job Opening Status',
      map: DomainToZohoModelMappingStrategy.fromStatus,
    };
    this.title = {
      target: 'Posting Title',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
    this.jobStatus = {
      target: "Statut de l'offre d'emploi",
      map: DomainToZohoModelMappingStrategy.fromJobStatus,
    };
    this.jobType = {
      target: 'Job Type',
      map: DomainToZohoModelMappingStrategy.fromJobType,
    };
    this.city = {
      target: 'City',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
    this.region = {
      target: 'Région',
      map: DomainToZohoModelMappingStrategy.fromRegion,
    };
    this.targetCustomers = {
      target: 'Clientèle visée',
      map: DomainToZohoModelMappingStrategy.fromTargetCustomers,
    };
    this.requiredExperiences = {
      target: 'Expérience requise',
      map: DomainToZohoModelMappingStrategy.fromRequiredExperiences,
    };
    this.positionType = {
      target: 'Type de fonction',
      map: DomainToZohoModelMappingStrategy.fromPositionType,
    };
    this.requestedSpecialities = {
      target: 'Spécialité(s) souhaitée(s)',
      map: DomainToZohoModelMappingStrategy.fromRequestedSpecialities,
    };
    this.description = {
      target: 'Job Description',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
    this.specificities = {
      target: 'Spécificités',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
    this['traveling.isTraveling'] = {
      target: 'Déplacements',
      map: DomainToZohoModelMappingStrategy.fromTravelingIsTraveling,
    };
    this['traveling.quebec'] = {
      target: 'Déplacements Québec',
      map: DomainToZohoModelMappingStrategy.fromTravelingFrequency,
    };
    this['traveling.canada'] = {
      target: 'Déplacements Canada',
      map: DomainToZohoModelMappingStrategy.fromTravelingFrequency,
    };
    this['traveling.international'] = {
      target: 'Déplacements international',
      map: DomainToZohoModelMappingStrategy.fromTravelingFrequency,
    };
    this.advantages = {
      target: 'Avantages spécifiques au poste',
      map: DomainToZohoModelMappingStrategy.fromAdvantages,
    };
    this['annualSalaryRange.minimumSalary'] = {
      target: 'Minimum $',
      map: DomainToZohoModelMappingStrategy.fromNumber,
    };
    this['annualSalaryRange.maximumSalary'] = {
      target: 'Maximum $',
      map: DomainToZohoModelMappingStrategy.fromNumber,
    };
    this.wantedPersonalities = {
      target: 'Personnalité recherchée',
      map: DomainToZohoModelMappingStrategy.fromWantedPersonalities,
    };
    this.endDateOfApplication = {
      target: 'Target Date',
      map: DomainToZohoModelMappingStrategy.fromDate,
    };
    this['startDateOfEmployment.asSoonAsPossible'] = {
      target: 'Entrée en fonction dès que possible',
      map: DomainToZohoModelMappingStrategy.fromStartDateOfEmploymentAsSoonAsPossible,
    };
    this['startDateOfEmployment.startDate'] = {
      target: "Date d'entrée en fonction",
      map: DomainToZohoModelMappingStrategy.fromDate,
    };
    this.emailAddressForReceiptOfApplications = {
      target: 'Courriel de réception des candidatures',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
    this.internalReferenceNumber = {
      target: 'Numéro de référence interne',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
    this.publishedAt = {
      target: 'Date de parution',
      map: DomainToZohoModelMappingStrategy.fromDate,
    };
  }
}

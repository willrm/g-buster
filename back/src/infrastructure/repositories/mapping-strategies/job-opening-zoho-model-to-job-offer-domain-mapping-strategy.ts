import { Injectable } from '@nestjs/common';
import { ZohoModelToDomainMappingStrategy } from '../../vendors/zoho/zoho-model-to-domain-mapping-strategy';

@Injectable()
export class JobOpeningZohoModelToJobOfferDomainMappingStrategy extends ZohoModelToDomainMappingStrategy {
  constructor() {
    super();

    this.JOBOPENINGID = {
      target: 'id',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
    this['Auth0 user id'] = {
      target: 'userId',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
    this['G-Buster company id'] = {
      target: 'companyId',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
    this['Job Opening Status'] = {
      target: 'status',
      map: ZohoModelToDomainMappingStrategy.toStatus,
    };
    this['Posting Title'] = {
      target: 'title',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
    this["Statut de l'offre d'emploi"] = {
      target: 'jobStatus',
      map: ZohoModelToDomainMappingStrategy.toJobStatus,
    };
    this['Job Type'] = {
      target: 'jobType',
      map: ZohoModelToDomainMappingStrategy.toJobType,
    };
    this.City = {
      target: 'city',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
    this.Région = {
      target: 'region',
      map: ZohoModelToDomainMappingStrategy.toRegion,
    };
    this['Clientèle visée'] = {
      target: 'targetCustomers',
      map: ZohoModelToDomainMappingStrategy.toTargetCustomers,
    };
    this['Expérience requise'] = {
      target: 'requiredExperiences',
      map: ZohoModelToDomainMappingStrategy.toRequiredExperiences,
    };
    this['Type de fonction'] = {
      target: 'positionType',
      map: ZohoModelToDomainMappingStrategy.toPositionType,
    };
    this['Spécialité(s) souhaitée(s)'] = {
      target: 'requestedSpecialities',
      map: ZohoModelToDomainMappingStrategy.toRequestedSpecialities,
    };
    this['Job Description'] = {
      target: 'description',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
    this.Spécificités = {
      target: 'specificities',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
    this.Déplacements = {
      target: 'traveling.isTraveling',
      map: ZohoModelToDomainMappingStrategy.toTravelingIsTraveling,
    };
    this['Déplacements Québec'] = {
      target: 'traveling.quebec',
      map: ZohoModelToDomainMappingStrategy.toTravelingFrequency,
    };
    this['Déplacements Canada'] = {
      target: 'traveling.canada',
      map: ZohoModelToDomainMappingStrategy.toTravelingFrequency,
    };
    this['Déplacements international'] = {
      target: 'traveling.international',
      map: ZohoModelToDomainMappingStrategy.toTravelingFrequency,
    };
    this['Avantages spécifiques au poste'] = {
      target: 'advantages',
      map: ZohoModelToDomainMappingStrategy.toAdvantages,
    };
    this['Minimum $'] = {
      target: 'annualSalaryRange.minimumSalary',
      map: ZohoModelToDomainMappingStrategy.toNumber,
    };
    this['Maximum $'] = {
      target: 'annualSalaryRange.maximumSalary',
      map: ZohoModelToDomainMappingStrategy.toNumber,
    };
    this['Personnalité recherchée'] = {
      target: 'wantedPersonalities',
      map: ZohoModelToDomainMappingStrategy.toWantedPersonalities,
    };
    this['Target Date'] = {
      target: 'endDateOfApplication',
      map: ZohoModelToDomainMappingStrategy.toDate,
    };
    this['Entrée en fonction dès que possible'] = {
      target: 'startDateOfEmployment.asSoonAsPossible',
      map: ZohoModelToDomainMappingStrategy.toStartDateOfEmploymentAsSoonAsPossible,
    };
    this["Date d'entrée en fonction"] = {
      target: 'startDateOfEmployment.startDate',
      map: ZohoModelToDomainMappingStrategy.toDate,
    };
    this['Courriel de réception des candidatures'] = {
      target: 'emailAddressForReceiptOfApplications',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
    this['Numéro de référence interne'] = {
      target: 'internalReferenceNumber',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
    this['Date de parution'] = {
      target: 'publishedAt',
      map: ZohoModelToDomainMappingStrategy.toDate,
    };
  }
}

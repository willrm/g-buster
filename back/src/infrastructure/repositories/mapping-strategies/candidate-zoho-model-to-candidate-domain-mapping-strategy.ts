import { Injectable } from '@nestjs/common';
import { ZohoModelToDomainMappingStrategy } from '../../vendors/zoho/zoho-model-to-domain-mapping-strategy';

@Injectable()
export class CandidateZohoModelToCandidateDomainMappingStrategy extends ZohoModelToDomainMappingStrategy {
  constructor() {
    super();

    this.CANDIDATEID = {
      target: 'id',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
    this['First Name'] = {
      target: 'firstName',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
    this['Last Name'] = {
      target: 'lastName',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
    this.Email = {
      target: 'email',
      // We have to map to a specific unique email because when you try to add to Zoho a new Candidate entity having the same email as another Candidate entity, it won't add it but it will update the existing entity
      // And we don't want that because we want a unique Candidate entity in Zoho each time a person applies to a job offer
      // See also CandidateDomainToCandidateZohoModelMappingStrategy.ts
      map: ZohoModelToDomainMappingStrategy.toEmail,
    };
    this['Salesforce Id'] = {
      target: 'salesforceId',
      map: ZohoModelToDomainMappingStrategy.noOp,
    };
  }
}

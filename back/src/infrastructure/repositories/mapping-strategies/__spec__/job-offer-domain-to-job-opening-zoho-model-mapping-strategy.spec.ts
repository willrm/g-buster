import {
  AdvantageFinancial,
  AdvantageHealth,
  AdvantageLifeBalance,
  AdvantageProfessional,
  AdvantageSocial,
} from '../../../../domain/advantages/advantages-enums';
import { AdvantagesInterface } from '../../../../domain/advantages/advantages.interface';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from '../../../../domain/job-offer/job-offer-enums';
import { TravelingFrequency } from '../../../../domain/traveling/traveling-enums';
import { JobOfferDomainToJobOpeningZohoModelMappingStrategy } from '../job-offer-domain-to-job-opening-zoho-model-mapping-strategy';

describe('infrastructure/repositories/mapping-strategies/JobOfferDomainToJobOpeningZohoModelMappingStrategy', () => {
  describe('constructor()', () => {
    let jobOfferDomainToJobOpeningZohoModelMappingStrategy: JobOfferDomainToJobOpeningZohoModelMappingStrategy;
    beforeEach(() => {
      jobOfferDomainToJobOpeningZohoModelMappingStrategy = new JobOfferDomainToJobOpeningZohoModelMappingStrategy();
    });

    describe('id', () => {
      it('should add entry to map id field to zoho JOBOPENINGID', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.id.target).toBe('JOBOPENINGID');
      });
      it('should add entry to map string value to zoho JOBOPENINGID', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.id.map('123456789');

        // then
        expect(result).toBe('123456789');
      });
    });

    describe('userId', () => {
      it('should add entry to map userId field to zoho Auth0 user id', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.userId.target).toBe('Auth0 user id');
      });
      it('should add entry to map string value to zoho Auth0 user id', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.userId.map('987654321');

        // then
        expect(result).toBe('987654321');
      });
    });

    describe('companyId', () => {
      it('should add entry to map companyId field to zoho G-Buster company id', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.companyId.target).toBe('G-Buster company id');
      });
      it('should add entry to map string value to zoho G-Buster company id', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.companyId.map('879135135687');

        // then
        expect(result).toBe('879135135687');
      });
    });

    describe('status', () => {
      it('should add entry to map status field to zoho Job Opening Status', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.status.target).toBe('Job Opening Status');
      });
      it('should add entry to map JobOfferStatus enum value to zoho Job Opening Status', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.status.map(JobOfferStatus.STATUS_PENDING_VALIDATION);

        // then
        expect(result).toBe('En attente d’approbation');
      });
    });

    describe('title', () => {
      it('should add entry to map title field to zoho Posting Title', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.title.target).toBe('Posting Title');
      });
      it('should add entry to map string value to zoho Posting Title', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.title.map('a title');

        // then
        expect(result).toBe('a title');
      });
    });

    describe('jobStatus', () => {
      it("should add entry to map jobStatus field to zoho Statut de l'offre d'emploi", () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.jobStatus.target).toBe("Statut de l'offre d'emploi");
      });
      it("should add entry to map JobOfferJobStatus enum value to zoho Statut de l'offre d'emploi", () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.jobStatus.map(JobOfferJobStatus.JOB_STATUS_FULL_TIME);

        // then
        expect(result).toBe('Temps plein');
      });
    });

    describe('jobType', () => {
      it('should add entry to map jobType field to zoho Job Type', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.jobType.target).toBe('Job Type');
      });
      it("should add entry to map JobOfferJobType enum value to zoho Statut de l'offre d'emploi", () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.jobType.map(JobOfferJobType.JOB_TYPE_INTERNSHIP);

        // then
        expect(result).toBe('Stage');
      });
    });

    describe('city', () => {
      it('should add entry to map city field to zoho City', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.city.target).toBe('City');
      });
      it('should add entry to map string value to zoho City', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.city.map('a city');

        // then
        expect(result).toBe('a city');
      });
    });

    describe('region', () => {
      it('should add entry to map region field to zoho Région', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.region.target).toBe('Région');
      });
      it('should add entry to map JobOfferRegion enum value to zoho Région', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.region.map(JobOfferRegion.REGION_CHAUDIERES_APPALACHES);

        // then
        expect(result).toBe('Chaudières-Appalaches');
      });
    });

    describe('targetCustomers', () => {
      it('should add entry to map targetCustomers field to zoho Clientèle visée', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.targetCustomers.target).toBe('Clientèle visée');
      });
      it('should add entry to map JobOfferTargetCustomer enum values to zoho Clientèle visée', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.targetCustomers.map([
          JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERING_GRADUATES,
          JobOfferTargetCustomer.TARGET_CUSTOMER_STUDENTS_AND_GRADUATES,
        ]);

        // then
        expect(result).toBe('Diplômés en génie;Étudiants et finissants');
      });
    });

    describe('requiredExperiences', () => {
      it('should add entry to map requiredExperiences field to zoho Expérience requise', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.requiredExperiences.target).toBe('Expérience requise');
      });
      it('should add entry to map JobOfferRequiredExperience enum values to zoho Expérience requise', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.requiredExperiences.map([
          JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
          JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
        ]);

        // then
        expect(result).toBe('3 à 5 ans;6 à 10 ans');
      });
    });

    describe('positionType', () => {
      it('should add entry to map positionType field to zoho Type de fonction', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.positionType.target).toBe('Type de fonction');
      });
      it('should add entry to map JobOfferPositionType enum value to zoho Type de fonction', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.positionType.map(JobOfferPositionType.POSITION_TYPE_ANALYST);

        // then
        expect(result).toBe('Analyste');
      });
    });

    describe('requestedSpecialities', () => {
      it('should add entry to map requestedSpecialities field to zoho Spécialité(s) souhaitée(s)', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.requestedSpecialities.target).toBe('Spécialité(s) souhaitée(s)');
      });
      it('should add entry to map JobOfferRequestedSpeciality enum values to zoho Spécialité(s) souhaitée(s)', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.requestedSpecialities.map([
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOTECHNOLOGY,
        ]);

        // then
        expect(result).toBe('Biomédical;Biotechnologie');
      });
    });

    describe('description', () => {
      it('should add entry to map description field to zoho Job Description', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.description.target).toBe('Job Description');
      });
      it('should add entry to map string values to zoho Job Description', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.description.map('A job description');

        // then
        expect(result).toBe('A job description');
      });
    });

    describe('specificities', () => {
      it('should add entry to map specificities field to zoho Spécificités', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.specificities.target).toBe('Spécificités');
      });
      it('should add entry to map string values to zoho Spécificités', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.specificities.map('Job specificities');

        // then
        expect(result).toBe('Job specificities');
      });
    });

    describe('traveling.isTraveling', () => {
      it('should add entry to map traveling.isTraveling field to zoho Déplacements', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy['traveling.isTraveling'].target).toBe('Déplacements');
      });
      it('should add entry to map boolean value to zoho Déplacements', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy['traveling.isTraveling'].map(true);

        // then
        expect(result).toBe('Oui');
      });
    });

    describe('traveling.quebec', () => {
      it('should add entry to map traveling.quebec field to zoho Déplacements Québec', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy['traveling.quebec'].target).toBe('Déplacements Québec');
      });
      it('should add entry to map TravelingFrequency enum value to zoho Déplacements Québec', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy['traveling.quebec'].map(TravelingFrequency.TRAVELING_FREQUENT);

        // then
        expect(result).toBe('Fréquents');
      });
    });

    describe('traveling.canada', () => {
      it('should add entry to map traveling.canada field to zoho Déplacements Canada', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy['traveling.canada'].target).toBe('Déplacements Canada');
      });
      it('should add entry to map TravelingFrequency enum value to zoho Déplacements Canada', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy['traveling.canada'].map(TravelingFrequency.TRAVELING_OCCASIONAL);

        // then
        expect(result).toBe('Occasionnels');
      });
    });

    describe('traveling.international', () => {
      it('should add entry to map traveling.international field to zoho Déplacements international', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy['traveling.international'].target).toBe('Déplacements international');
      });
      it('should add entry to map TravelingFrequency enum value to zoho Déplacements international', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy['traveling.international'].map(
          TravelingFrequency.TRAVELING_FREQUENT
        );

        // then
        expect(result).toBe('Fréquents');
      });
    });

    describe('advantages', () => {
      it('should add entry to map advantages field to zoho Avantages spécifiques au poste', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.advantages.target).toBe('Avantages spécifiques au poste');
      });
      it('should add entry to map Advantages value to zoho Avantages spécifiques au poste', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.advantages.map({
          health: [AdvantageHealth.HEALTH_DENTAL_INSURANCE],
          social: [AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES],
          financial: [AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT],
          lifeBalance: [AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK],
          professional: [AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM],
        } as AdvantagesInterface);

        // then
        expect(result).toBe(
          'Assurance dentaire;Activités sociales;Conciliation famille-travail;Rabais employé;Programme de formation et développement'
        );
      });
    });

    describe('annualSalaryRange.minimumSalary', () => {
      it('should add entry to map annualSalaryRange.minimumSalary field to zoho Minimum $', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy['annualSalaryRange.minimumSalary'].target).toBe('Minimum $');
      });
      it('should add entry to map number value to zoho Minimum $', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy['annualSalaryRange.minimumSalary'].map(123456789);

        // then
        expect(result).toBe('123456789');
      });
    });

    describe('annualSalaryRange.maximumSalary', () => {
      it('should add entry to map annualSalaryRange.maximumSalary field to zoho Maximum $', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy['annualSalaryRange.maximumSalary'].target).toBe('Maximum $');
      });
      it('should add entry to map number value to zoho Maximum $', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy['annualSalaryRange.maximumSalary'].map(123456789);

        // then
        expect(result).toBe('123456789');
      });
    });

    describe('wantedPersonalities', () => {
      it('should add entry to map wantedPersonalities field to zoho Personnalité recherchée', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.wantedPersonalities.target).toBe('Personnalité recherchée');
      });
      it('should add entry to map JobOfferWantedPersonality enum values to zoho Personnalité recherchée', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.wantedPersonalities.map([
          JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS,
          JobOfferWantedPersonality.WANTED_PERSONALITY_DIPLOMATS,
        ]);

        // then
        expect(result).toBe('Communicant;Diplomate');
      });
    });

    describe('endDateOfApplication', () => {
      it('should add entry to map endDateOfApplication field to zoho Target Date', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.endDateOfApplication.target).toBe('Target Date');
      });
      it('should add entry to map Date value to zoho Target Date', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.endDateOfApplication.map(new Date('2019-03-28T20:35:57.000Z'));

        // then
        expect(result).toBe('2019-03-28');
      });
    });

    describe('startDateOfEmployment.asSoonAsPossible', () => {
      it('should add entry to map startDateOfEmployment.asSoonAsPossible field to zoho Entrée en fonction dès que possible', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy['startDateOfEmployment.asSoonAsPossible'].target).toBe(
          'Entrée en fonction dès que possible'
        );
      });
      it('should add entry to map boolean value to zoho Entrée en fonction dès que possible', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy['startDateOfEmployment.asSoonAsPossible'].map(false);

        // then
        expect(result).toBe('Non');
      });
    });

    describe('startDateOfEmployment.startDate', () => {
      it("should add entry to map startDateOfEmployment.startDate field to zoho Date d'entrée en fonction", () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy['startDateOfEmployment.startDate'].target).toBe("Date d'entrée en fonction");
      });
      it("should add entry to map Date value to zoho Date d'entrée en fonction", () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy['startDateOfEmployment.startDate'].map(
          new Date('2020-03-28T00:00:00.000Z')
        );

        // then
        expect(result).toBe('2020-03-28');
      });
    });

    describe('emailAddressForReceiptOfApplications', () => {
      it('should add entry to map emailAddressForReceiptOfApplications field to zoho Courriel de réception des candidatures', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.emailAddressForReceiptOfApplications.target).toBe(
          'Courriel de réception des candidatures'
        );
      });
      it('should add entry to map string values to zoho Courriel de réception des candidatures', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.emailAddressForReceiptOfApplications.map('test@example.org');

        // then
        expect(result).toBe('test@example.org');
      });
    });

    describe('internalReferenceNumber', () => {
      it('should add entry to map internalReferenceNumber field to zoho Numéro de référence interne', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.internalReferenceNumber.target).toBe('Numéro de référence interne');
      });
      it('should add entry to map string values to zoho Numéro de référence interne', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.internalReferenceNumber.map('ABC123XYZ');

        // then
        expect(result).toBe('ABC123XYZ');
      });
    });

    describe('publishedAt', () => {
      it('should add entry to map publishedAt field to zoho Date de parution', () => {
        // then
        expect(jobOfferDomainToJobOpeningZohoModelMappingStrategy.publishedAt.target).toBe('Date de parution');
      });
      it('should add entry to map Date value to zoho Date de parution', () => {
        // when
        const result: string = jobOfferDomainToJobOpeningZohoModelMappingStrategy.publishedAt.map(new Date('2020-03-28T00:00:00.000Z'));

        // then
        expect(result).toBe('2020-03-28');
      });
    });
  });
});

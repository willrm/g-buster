import { CandidateDomainToCandidateZohoModelMappingStrategy } from '../candidate-domain-to-candidate-zoho-model-mapping-strategy';

describe('infrastructure/repositories/mapping-strategies/CandidateDomainToCandidateZohoModelMappingStrategy', () => {
  describe('constructor()', () => {
    let candidateDomainToCandidateZohoModelMappingStrategy: CandidateDomainToCandidateZohoModelMappingStrategy;
    beforeEach(() => {
      candidateDomainToCandidateZohoModelMappingStrategy = new CandidateDomainToCandidateZohoModelMappingStrategy();
    });

    describe('id', () => {
      it('should add entry to map id field to zoho CANDIDATEID', () => {
        // then
        expect(candidateDomainToCandidateZohoModelMappingStrategy.id.target).toBe('CANDIDATEID');
      });
      it('should add entry to map string value to zoho CANDIDATEID', () => {
        // when
        const result: string = candidateDomainToCandidateZohoModelMappingStrategy.id.map('123456789');

        // then
        expect(result).toBe('123456789');
      });
    });

    describe('firstName', () => {
      it('should add entry to map firstName field to zoho First Name', () => {
        // then
        expect(candidateDomainToCandidateZohoModelMappingStrategy.firstName.target).toBe('First Name');
      });
      it('should add entry to map string value to zoho First Name', () => {
        // when
        const result: string = candidateDomainToCandidateZohoModelMappingStrategy.firstName.map('A first name');

        // then
        expect(result).toBe('A first name');
      });
    });

    describe('lastName', () => {
      it('should add entry to map lastName field to zoho Last Name', () => {
        // then
        expect(candidateDomainToCandidateZohoModelMappingStrategy.lastName.target).toBe('Last Name');
      });
      it('should add entry to map string value to zoho Last Name', () => {
        // when
        const result: string = candidateDomainToCandidateZohoModelMappingStrategy.lastName.map('A last name');

        // then
        expect(result).toBe('A last name');
      });
    });

    describe('email', () => {
      it('should add entry to map email field to zoho Email', () => {
        // then
        expect(candidateDomainToCandidateZohoModelMappingStrategy.email.target).toBe('Email');
      });
      it('should add entry to map string value to unique zoho Email', () => {
        // given
        const fixedDate: Date = new Date('2020-06-13T04:41:20');
        // @ts-ignore
        jest.spyOn(global, 'Date').mockImplementationOnce(() => fixedDate);

        // when
        const result: string = candidateDomainToCandidateZohoModelMappingStrategy.email.map('test@example.org');

        // then
        expect(result).toBe('test@example.org+REMOVE-THIS-TIMESTAMPED-SUFFIX-TO-HAVE-REAL-EMAIL-1592037680000');
      });
    });

    describe('salesforceId', () => {
      it('should add entry to map salesforceId field to zoho Salesforce Id', () => {
        // then
        expect(candidateDomainToCandidateZohoModelMappingStrategy.salesforceId.target).toBe('Salesforce Id');
      });
      it('should add entry to map string value to zoho Salesforce Id', () => {
        // when
        const result: string = candidateDomainToCandidateZohoModelMappingStrategy.salesforceId.map('ABC123456789XYZ');

        // then
        expect(result).toBe('ABC123456789XYZ');
      });
    });
  });
});

import { CandidateZohoModelToCandidateDomainMappingStrategy } from '../candidate-zoho-model-to-candidate-domain-mapping-strategy';

describe('infrastructure/repositories/mapping-strategies/CandidateZohoModelToCandidateDomainMappingStrategy', () => {
  describe('constructor()', () => {
    let candidateZohoModelToCandidateDomainMappingStrategy: CandidateZohoModelToCandidateDomainMappingStrategy;
    beforeEach(() => {
      candidateZohoModelToCandidateDomainMappingStrategy = new CandidateZohoModelToCandidateDomainMappingStrategy();
    });

    describe('CANDIDATEID', () => {
      it('should add entry to map zoho CANDIDATEID to id field', () => {
        // then
        expect(candidateZohoModelToCandidateDomainMappingStrategy.CANDIDATEID.target).toBe('id');
      });
      it('should add entry to map zoho CANDIDATEID to string value', () => {
        // when
        const result: unknown = candidateZohoModelToCandidateDomainMappingStrategy.CANDIDATEID.map('123456789');

        // then
        expect(result).toBe('123456789');
      });
    });

    describe('First Name', () => {
      it('should add entry to map zoho First Name to firstName field', () => {
        // then
        expect(candidateZohoModelToCandidateDomainMappingStrategy['First Name'].target).toBe('firstName');
      });
      it('should add entry to map zoho First Name to string value', () => {
        // when
        const result: unknown = candidateZohoModelToCandidateDomainMappingStrategy['First Name'].map('A first name');

        // then
        expect(result).toBe('A first name');
      });
    });

    describe('Last Name', () => {
      it('should add entry to map zoho Last Name to lastName field', () => {
        // then
        expect(candidateZohoModelToCandidateDomainMappingStrategy['Last Name'].target).toBe('lastName');
      });
      it('should add entry to map zoho Last Name to string value', () => {
        // when
        const result: unknown = candidateZohoModelToCandidateDomainMappingStrategy['Last Name'].map('A last name');

        // then
        expect(result).toBe('A last name');
      });
    });

    describe('Email', () => {
      it('should add entry to map zoho Email to email field', () => {
        // then
        expect(candidateZohoModelToCandidateDomainMappingStrategy.Email.target).toBe('email');
      });
      it('should add entry to map zoho unique Email to string value', () => {
        // when
        const result: unknown = candidateZohoModelToCandidateDomainMappingStrategy.Email.map(
          'test@example.org+REMOVE-THIS-TIMESTAMPED-SUFFIX-TO-HAVE-REAL-EMAIL-1592037680000'
        );

        // then
        expect(result).toBe('test@example.org');
      });
    });

    describe('Salesforce Id', () => {
      it('should add entry to map zoho Salesforce Id to salesforceId field', () => {
        // then
        expect(candidateZohoModelToCandidateDomainMappingStrategy['Salesforce Id'].target).toBe('salesforceId');
      });
      it('should add entry to map zoho Salesforce Id to string value', () => {
        // when
        const result: unknown = candidateZohoModelToCandidateDomainMappingStrategy['Salesforce Id'].map('ABC123456789XYZ');

        // then
        expect(result).toBe('ABC123456789XYZ');
      });
    });
  });
});

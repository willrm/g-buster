import {
  AdvantageFinancial,
  AdvantageHealth,
  AdvantageLifeBalance,
  AdvantageProfessional,
  AdvantageSocial,
} from '../../../../domain/advantages/advantages-enums';
import { AdvantagesInterface } from '../../../../domain/advantages/advantages.interface';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from '../../../../domain/job-offer/job-offer-enums';
import { TravelingFrequency } from '../../../../domain/traveling/traveling-enums';
import { JobOpeningZohoModelToJobOfferDomainMappingStrategy } from '../job-opening-zoho-model-to-job-offer-domain-mapping-strategy';

describe('infrastructure/repositories/mapping-strategies/JobOpeningZohoModelToJobOfferDomainMappingStrategy', () => {
  describe('constructor()', () => {
    let jobOpeningZohoModelToJobOfferDomainMappingStrategy: JobOpeningZohoModelToJobOfferDomainMappingStrategy;
    beforeEach(() => {
      jobOpeningZohoModelToJobOfferDomainMappingStrategy = new JobOpeningZohoModelToJobOfferDomainMappingStrategy();
    });

    describe('JOBOPENINGID', () => {
      it('should add entry to map zoho JOBOPENINGID to id field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy.JOBOPENINGID.target).toBe('id');
      });
      it('should add entry to map zoho JOBOPENINGID to string value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy.JOBOPENINGID.map('123456789');

        // then
        expect(result).toBe('123456789');
      });
    });

    describe('Auth0 user id', () => {
      it('should add entry to map zoho Auth0 user id to userId field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Auth0 user id'].target).toBe('userId');
      });
      it('should add entry to map zoho Auth0 user id to string value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Auth0 user id'].map('987654321');

        // then
        expect(result).toBe('987654321');
      });
    });

    describe('G-Buster company id', () => {
      it('should add entry to map zoho G-Buster company id to companyId field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['G-Buster company id'].target).toBe('companyId');
      });
      it('should add entry to map zoho G-Buster company id to string value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['G-Buster company id'].map('879135135687');

        // then
        expect(result).toBe('879135135687');
      });
    });

    describe('Job Opening Status', () => {
      it('should add entry to map zoho Job Opening Status to status field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Job Opening Status'].target).toBe('status');
      });
      it('should add entry to map zoho Job Opening Status to JobOfferStatus enum value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Job Opening Status'].map('En cours');

        // then
        expect(result).toBe(JobOfferStatus.STATUS_PUBLISHED);
      });
    });

    describe('Posting Title', () => {
      it('should add entry to map zoho Posting Title to title field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Posting Title'].target).toBe('title');
      });
      it('should add entry to map zoho Posting Title to string value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Posting Title'].map('a title');

        // then
        expect(result).toBe('a title');
      });
    });

    describe("Statut de l'offre d'emploi", () => {
      it("should add entry to map zoho Statut de l'offre d'emploi to jobStatus field", () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy["Statut de l'offre d'emploi"].target).toBe('jobStatus');
      });
      it("should add entry to map zoho Statut de l'offre d'emploi to JobOfferJobStatus enum value", () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy["Statut de l'offre d'emploi"].map('Temps plein');

        // then
        expect(result).toBe(JobOfferJobStatus.JOB_STATUS_FULL_TIME);
      });
    });

    describe('Job Type', () => {
      it('should add entry to map zoho Job Type to jobType field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Job Type'].target).toBe('jobType');
      });
      it('should add entry to map zoho Job Type to JobOfferJobType enum value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Job Type'].map('Temporaire');

        // then
        expect(result).toBe(JobOfferJobType.JOB_TYPE_TEMPORARY);
      });
    });

    describe('City', () => {
      it('should add entry to map zoho City to city field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy.City.target).toBe('city');
      });
      it('should add entry to map zoho City to string value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy.City.map('a city');

        // then
        expect(result).toBe('a city');
      });
    });

    describe('Région', () => {
      it('should add entry to map zoho Région to region field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy.Région.target).toBe('region');
      });
      it('should add entry to map zoho Région to JobOfferRegion enum value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy.Région.map('Chaudières-Appalaches');

        // then
        expect(result).toBe(JobOfferRegion.REGION_CHAUDIERES_APPALACHES);
      });
    });

    describe('Clientèle visée', () => {
      it('should add entry to map zoho Clientèle visée to targetCustomers field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Clientèle visée'].target).toBe('targetCustomers');
      });
      it('should add entry to map zoho Clientèle visée to JobOfferTargetCustomer enum values', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Clientèle visée'].map('Ingénieur;Ingénieurs juniors / CPI');

        // then
        expect(result).toStrictEqual([JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS, JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP]);
      });
    });

    describe('Expérience requise', () => {
      it('should add entry to map zoho Expérience requise to requiredExperiences field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Expérience requise'].target).toBe('requiredExperiences');
      });
      it('should add entry to map zoho Expérience requise to JobOfferRequiredExperience enum values', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Expérience requise'].map('3 à 5 ans;6 à 10 ans');

        // then
        expect(result).toStrictEqual([
          JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
          JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
        ]);
      });
    });

    describe('Type de fonction', () => {
      it('should add entry to map zoho Type de fonction to positionType field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Type de fonction'].target).toBe('positionType');
      });
      it('should add entry to map zoho Type de fonction to JobOfferPositionType enum value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Type de fonction'].map('Analyste');

        // then
        expect(result).toBe(JobOfferPositionType.POSITION_TYPE_ANALYST);
      });
    });

    describe('Spécialité(s) souhaitée(s)', () => {
      it('should add entry to map zoho Spécialité(s) souhaitée(s) to requestedSpecialities field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Spécialité(s) souhaitée(s)'].target).toBe('requestedSpecialities');
      });
      it('should add entry to map zoho Spécialité(s) souhaitée(s) to JobOfferRequestedSpeciality enum values', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Spécialité(s) souhaitée(s)'].map('Biomédical;Biotechnologie');

        // then
        expect(result).toStrictEqual([
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOTECHNOLOGY,
        ]);
      });
    });

    describe('Job Description', () => {
      it('should add entry to map zoho Job Description to description field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Job Description'].target).toBe('description');
      });
      it('should add entry to map zoho Job Description to string value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Job Description'].map('A job description');

        // then
        expect(result).toBe('A job description');
      });
    });

    describe('Spécificités', () => {
      it('should add entry to map zoho Spécificités to specificities field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy.Spécificités.target).toBe('specificities');
      });
      it('should add entry to map zoho Spécificités to string value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy.Spécificités.map('Job specificities');

        // then
        expect(result).toBe('Job specificities');
      });
    });

    describe('Déplacements', () => {
      it('should add entry to map zoho Déplacements to traveling.isTraveling field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy.Déplacements.target).toBe('traveling.isTraveling');
      });
      it('should add entry to map zoho Déplacements to boolean value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy.Déplacements.map('Oui');

        // then
        expect(result).toBe(true);
      });
    });

    describe('Déplacements Québec', () => {
      it('should add entry to map zoho Déplacements Québec to traveling.quebec field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Déplacements Québec'].target).toBe('traveling.quebec');
      });
      it('should add entry to map zoho Déplacements Québec to TravelingFrequency enum value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Déplacements Québec'].map('Occasionnels');

        // then
        expect(result).toBe(TravelingFrequency.TRAVELING_OCCASIONAL);
      });
    });

    describe('Déplacements Canada', () => {
      it('should add entry to map zoho Déplacements Canada to traveling.canada field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Déplacements Canada'].target).toBe('traveling.canada');
      });
      it('should add entry to map zoho Déplacements Canada to TravelingFrequency enum value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Déplacements Canada'].map('Fréquents');

        // then
        expect(result).toBe(TravelingFrequency.TRAVELING_FREQUENT);
      });
    });

    describe('Déplacements international', () => {
      it('should add entry to map zoho Déplacements international to traveling.international field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Déplacements international'].target).toBe('traveling.international');
      });
      it('should add entry to map zoho Déplacements international to TravelingFrequency enum value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Déplacements international'].map('Occasionnels');

        // then
        expect(result).toBe(TravelingFrequency.TRAVELING_OCCASIONAL);
      });
    });

    describe('Avantages spécifiques au poste', () => {
      it('should add entry to map zoho Avantages spécifiques au poste to advantages field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Avantages spécifiques au poste'].target).toBe('advantages');
      });
      it('should add entry to map zoho Avantages spécifiques au poste to Advantages value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Avantages spécifiques au poste'].map(
          'Assurance dentaire;Activités sociales;Conciliation famille-travail;Rabais employé;Programme de formation et développement'
        );

        // then
        expect(result).toStrictEqual({
          health: [AdvantageHealth.HEALTH_DENTAL_INSURANCE],
          social: [AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES],
          financial: [AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT],
          lifeBalance: [AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK],
          professional: [AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM],
        } as AdvantagesInterface);
      });
    });

    describe('Minimum $', () => {
      it('should add entry to map zoho Minimum $ to annualSalaryRange.minimumSalary field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Minimum $'].target).toBe('annualSalaryRange.minimumSalary');
      });
      it('should add entry to map zoho Minimum $ to number value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Minimum $'].map('123 456 789');

        // then
        expect(result).toBe(123456789);
      });
    });

    describe('Maximum $', () => {
      it('should add entry to map zoho Maximum $ to annualSalaryRange.maximumSalary field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Maximum $'].target).toBe('annualSalaryRange.maximumSalary');
      });
      it('should add entry to map zoho Maximum $ to number value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Maximum $'].map('987 654 321');

        // then
        expect(result).toBe(987654321);
      });
    });

    describe('Personnalité recherchée', () => {
      it('should add entry to map zoho Personnalité recherchée to wantedPersonalities field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Personnalité recherchée'].target).toBe('wantedPersonalities');
      });
      it('should add entry to map zoho Personnalité recherchée to JobOfferWantedPersonality enum values', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Personnalité recherchée'].map('Communicant;Diplomate');

        // then
        expect(result).toStrictEqual([
          JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS,
          JobOfferWantedPersonality.WANTED_PERSONALITY_DIPLOMATS,
        ]);
      });
    });

    describe('Target Date', () => {
      it('should add entry to map zoho Target Date to endDateOfApplication field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Target Date'].target).toBe('endDateOfApplication');
      });
      it('should add entry to map zoho Target Date to Date value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Target Date'].map('1995-12-17T03:24:00');

        // then
        expect(result).toStrictEqual(new Date('1995-12-17T03:24:00'));
      });
    });

    describe('Entrée en fonction dès que possible', () => {
      it('should add entry to map zoho Entrée en fonction dès que possible to startDateOfEmployment.asSoonAsPossible field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Entrée en fonction dès que possible'].target).toBe(
          'startDateOfEmployment.asSoonAsPossible'
        );
      });
      it('should add entry to map zoho Entrée en fonction dès que possible to boolean value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Entrée en fonction dès que possible'].map('Oui');

        // then
        expect(result).toBe(true);
      });
    });

    describe("Date d'entrée en fonction", () => {
      it("should add entry to map zoho Date d'entrée en fonction to startDateOfEmployment.startDate field", () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy["Date d'entrée en fonction"].target).toBe('startDateOfEmployment.startDate');
      });
      it("should add entry to map zoho Date d'entrée en fonction to Date value", () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy["Date d'entrée en fonction"].map('2019-12-01');

        // then
        expect(result).toStrictEqual(new Date('2019-12-01T12:00:00'));
      });
    });

    describe('Courriel de réception des candidatures', () => {
      it('should add entry to map zoho Courriel de réception des candidatures to internalReferenceNumber field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Courriel de réception des candidatures'].target).toBe(
          'emailAddressForReceiptOfApplications'
        );
      });
      it('should add entry to map zoho Courriel de réception des candidatures to string value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Courriel de réception des candidatures'].map('test@example.org');

        // then
        expect(result).toBe('test@example.org');
      });
    });

    describe('Numéro de référence interne', () => {
      it('should add entry to map zoho Numéro de référence interne to internalReferenceNumber field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Numéro de référence interne'].target).toBe('internalReferenceNumber');
      });
      it('should add entry to map zoho Numéro de référence interne to string value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Numéro de référence interne'].map('ABC123XYZ');

        // then
        expect(result).toBe('ABC123XYZ');
      });
    });

    describe('Date de parution', () => {
      it('should add entry to map zoho Date de parution to publishedAt field', () => {
        // then
        expect(jobOpeningZohoModelToJobOfferDomainMappingStrategy['Date de parution'].target).toBe('publishedAt');
      });
      it('should add entry to map zoho Date de parution to Date value', () => {
        // when
        const result: unknown = jobOpeningZohoModelToJobOfferDomainMappingStrategy['Date de parution'].map('2019-12-01');

        // then
        expect(result).toStrictEqual(new Date('2019-12-01T12:00:00'));
      });
    });
  });
});

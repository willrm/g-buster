import { Injectable } from '@nestjs/common';
import { DomainToZohoModelMappingStrategy } from '../../vendors/zoho/domain-to-zoho-model-mapping-strategy';

@Injectable()
export class CandidateDomainToCandidateZohoModelMappingStrategy extends DomainToZohoModelMappingStrategy {
  constructor() {
    super();

    this.id = {
      target: 'CANDIDATEID',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
    this.firstName = {
      target: 'First Name',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
    this.lastName = {
      target: 'Last Name',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
    this.email = {
      target: 'Email',
      // We have to map to a specific unique email because when you try to add to Zoho a new Candidate entity having the same email as another Candidate entity, it won't add it but it will update the existing entity
      // And we don't want that because we want a unique Candidate entity in Zoho each time a person applies to a job offer
      // See also CandidateZohoModelToCandidateDomainMappingStrategy.ts
      map: DomainToZohoModelMappingStrategy.fromEmail,
    };
    this.salesforceId = {
      target: 'Salesforce Id',
      map: DomainToZohoModelMappingStrategy.noOp,
    };
  }
}

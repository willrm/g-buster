import { Injectable } from '@nestjs/common';
import { Document } from 'prismic-javascript/d.ts/documents';
import { DummyPage } from '../../domain/dummy-page/dummy-page';
import { DummyPageInterface } from '../../domain/dummy-page/dummy-page.interface';
import { DummyPageRawInterface } from '../../domain/dummy-page/dummy-page.raw.interface';
import { DummyPageRepository } from '../../domain/dummy-page/dummy-page.repository.interface';
import { ImageRawInterface } from '../../domain/image/image.raw.interface';
import { Lang, Uid } from '../../domain/type-aliases';
import { PrismicApiService } from '../vendors/prismic/prismic-api.service';
import { PrismicDocumentAdapterService } from '../vendors/prismic/prismic-document-adapter.service';

@Injectable()
export class PrismicDummyPageRepository implements DummyPageRepository {
  private readonly DUMMY_PAGE_CONTENT_TYPE: string = 'dummy_page';

  constructor(private readonly prismicApiService: PrismicApiService, private readonly prismicDocumentAdapterService: PrismicDocumentAdapterService) {}

  async findByLangAndUid(lang: Lang, uid: Uid): Promise<DummyPageInterface> {
    const document: Document = await this.prismicApiService.getByUID(this.DUMMY_PAGE_CONTENT_TYPE, uid, lang);

    const raw: DummyPageRawInterface = {
      title: this.prismicDocumentAdapterService.getRichTextAsHtml(document, 'title'),
      content: this.prismicDocumentAdapterService.getRichTextAsHtml(document, 'content'),
      image: this.getImage(document, 'image'),
    } as DummyPageRawInterface;

    return new DummyPage(raw);
  }

  // PRIVATE METHODS

  private getImage(document: Document, field: string): ImageRawInterface {
    return {
      source: this.prismicDocumentAdapterService.getValue(document, `${field}.url`),
      alternativeText: this.prismicDocumentAdapterService.getValue(document, `${field}.alt`),
    };
  }
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ItemNotFoundError } from '../../domain/common-errors/item-not-found.error';
import { CompanyInterface } from '../../domain/company/company.interface';
import { CompanyRepository } from '../../domain/company/company.repository.interface';
import { CompanyId, UserId } from '../../domain/type-aliases';
import { CompanyEntityTransformer } from './entities/company-entity.transformer';
import { CompanyEntity } from './entities/company.entity';

@Injectable()
export class DatabaseCompanyRepository implements CompanyRepository {
  constructor(
    @InjectRepository(CompanyEntity) private readonly companyEntityRepository: Repository<CompanyEntity>,
    private readonly companyEntityTransformer: CompanyEntityTransformer
  ) {}

  async save(company: CompanyInterface): Promise<CompanyId> {
    const companyEntity: CompanyEntity = this.companyEntityTransformer.to(company);
    const savedCompanyEntity: CompanyEntity = await this.companyEntityRepository.save(companyEntity);

    return Promise.resolve(String(savedCompanyEntity.id));
  }

  async findById(id: CompanyId): Promise<CompanyInterface> {
    const foundCompanyEntity: CompanyEntity = await this.companyEntityRepository.findOne({ where: { id } });
    if (!foundCompanyEntity) {
      return Promise.reject(new ItemNotFoundError(`Company not found with id "${id}"`));
    }
    const result: CompanyInterface = this.companyEntityTransformer.from(foundCompanyEntity);

    return Promise.resolve(result);
  }

  async findByUserId(userId: UserId): Promise<CompanyInterface> {
    const foundCompanyEntity: CompanyEntity = await this.companyEntityRepository.findOne({ where: { userId } });
    if (!foundCompanyEntity) {
      return Promise.reject(new ItemNotFoundError(`Company not found with userId "${userId}"`));
    }
    const result: CompanyInterface = this.companyEntityTransformer.from(foundCompanyEntity);

    return Promise.resolve(result);
  }

  async findAllByIds(ids: CompanyId[]): Promise<CompanyInterface[]> {
    const foundCompanyEntities: CompanyEntity[] = await this.companyEntityRepository.findByIds(ids);
    const result: CompanyInterface[] = foundCompanyEntities.map((companyEntity: CompanyEntity) => this.companyEntityTransformer.from(companyEntity));

    return Promise.resolve(result);
  }

  async findAll(): Promise<CompanyInterface[]> {
    const foundCompanyEntities: CompanyEntity[] = await this.companyEntityRepository.find();
    const result: CompanyInterface[] = foundCompanyEntities.map((companyEntity: CompanyEntity) => this.companyEntityTransformer.from(companyEntity));

    return Promise.resolve(result);
  }
}

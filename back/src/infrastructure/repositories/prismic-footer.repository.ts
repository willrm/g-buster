import { Injectable } from '@nestjs/common';
import { Document } from 'prismic-javascript/d.ts/documents';
import { Footer } from '../../domain/footer/footer';
import { FooterInterface } from '../../domain/footer/footer.interface';
import { FooterRawInterface } from '../../domain/footer/footer.raw.interface';
import { FooterRepository } from '../../domain/footer/footer.repository.interface';
import { Lang } from '../../domain/type-aliases';
import { PrismicApiService } from '../vendors/prismic/prismic-api.service';
import { PrismicDocumentAdapterService } from '../vendors/prismic/prismic-document-adapter.service';

@Injectable()
export class PrismicFooterRepository implements FooterRepository {
  private readonly FOOTER_CONTENT_TYPE: string = 'footer';
  constructor(private readonly prismicApiService: PrismicApiService, private readonly prismicDocumentAdapterService: PrismicDocumentAdapterService) {}

  async findByLang(lang: Lang): Promise<FooterInterface> {
    const document: Document = await this.prismicApiService.getSingle(this.FOOTER_CONTENT_TYPE, lang);

    return new Footer({
      copyright: this.prismicDocumentAdapterService.getValue(document, 'copyright'),
    } as FooterRawInterface);
  }
}

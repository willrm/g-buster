import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EnvironmentConfigModule } from '../config/environment-config/environment-config.module';
import { TypeOrmConfigModule } from '../config/typeorm/typeorm-config.module';
import { Genium360Module } from '../vendors/genium360/genium360.module';
import { PrismicModule } from '../vendors/prismic/prismic.module';
import { ZohoModule } from '../vendors/zoho/zoho.module';
import { DatabaseCompanyRepository } from './database-company.repository';
import { DatabaseTemporaryFileRepository } from './database-temporary-file.repository';
import { CompanyEntityTransformer } from './entities/company-entity.transformer';
import { CompanyEntity } from './entities/company.entity';
import { TemporaryFileEntityTransformer } from './entities/temporary-file-entity.transformer';
import { TemporaryFileEntity } from './entities/temporary-file.entity';
import { FileI18nRepository } from './file-i18n.repository';
import { Genium360GeniusProductRepository } from './genium360-genius-product.repository';
import { LocalUserRepository } from './local-user.repository';
import { CandidateDomainToCandidateZohoModelMappingStrategy } from './mapping-strategies/candidate-domain-to-candidate-zoho-model-mapping-strategy';
import { CandidateZohoModelToCandidateDomainMappingStrategy } from './mapping-strategies/candidate-zoho-model-to-candidate-domain-mapping-strategy';
import { JobOfferDomainToJobOpeningZohoModelMappingStrategy } from './mapping-strategies/job-offer-domain-to-job-opening-zoho-model-mapping-strategy';
import { JobOpeningZohoModelToJobOfferDomainMappingStrategy } from './mapping-strategies/job-opening-zoho-model-to-job-offer-domain-mapping-strategy';
import { PrismicDummyPageRepository } from './prismic-dummy-page.repository';
import { PrismicFooterRepository } from './prismic-footer.repository';
import { PrismicHeaderRepository } from './prismic-header.repository';
import { ZohoCandidateRepository } from './zoho-candidate.repository';
import { ZohoJobOfferRepository } from './zoho-job-offer.repository';

@Module({
  imports: [
    PrismicModule,
    ZohoModule,
    TypeOrmConfigModule,
    TypeOrmModule.forFeature([TemporaryFileEntity, CompanyEntity]),
    EnvironmentConfigModule,
    Genium360Module,
  ],
  providers: [
    PrismicFooterRepository,
    PrismicHeaderRepository,
    PrismicDummyPageRepository,
    FileI18nRepository,
    DatabaseCompanyRepository,
    DatabaseTemporaryFileRepository,
    ZohoJobOfferRepository,
    JobOpeningZohoModelToJobOfferDomainMappingStrategy,
    JobOfferDomainToJobOpeningZohoModelMappingStrategy,
    LocalUserRepository,
    CompanyEntityTransformer,
    TemporaryFileEntityTransformer,
    ZohoCandidateRepository,
    CandidateZohoModelToCandidateDomainMappingStrategy,
    CandidateDomainToCandidateZohoModelMappingStrategy,
    Genium360GeniusProductRepository,
  ],
  exports: [
    PrismicFooterRepository,
    PrismicHeaderRepository,
    PrismicDummyPageRepository,
    FileI18nRepository,
    DatabaseCompanyRepository,
    DatabaseTemporaryFileRepository,
    ZohoJobOfferRepository,
    LocalUserRepository,
    ZohoCandidateRepository,
    Genium360GeniusProductRepository,
  ],
})
export class RepositoriesModule {}

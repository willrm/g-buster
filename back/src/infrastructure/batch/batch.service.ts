import { Inject, Injectable, Logger, LoggerService } from '@nestjs/common';
import { InjectSchedule, Schedule } from 'nest-schedule';
import { JobOfferId, TemporaryFileId } from '../../domain/type-aliases';
import { ArchiveExpiredJobOffers } from '../../use_cases/archive-expired-job-offers';
import { CleanExpiredTemporaryFiles } from '../../use_cases/clean-expired-temporary-files';
import { ProxyServicesDynamicModule } from '../use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../use_cases_proxy/use-case-proxy';

@Injectable()
export class BatchService {
  private readonly LOGGER: LoggerService = new Logger(BatchService.name);
  private readonly ONE_HOUR_IN_MILLISECONDS: number = 60 * 60 * 1000;

  constructor(
    @InjectSchedule() private readonly schedule: Schedule,
    @Inject(ProxyServicesDynamicModule.CLEAN_EXPIRED_TEMPORARY_FILES_PROXY_SERVICE)
    private readonly cleanExpiredTemporaryFilesProxyService: UseCaseProxy<CleanExpiredTemporaryFiles>,
    @Inject(ProxyServicesDynamicModule.ARCHIVE_EXPIRED_JOB_OFFERS_PROXY_SERVICE)
    private readonly archiveExpiredJobOffersProxyService: UseCaseProxy<ArchiveExpiredJobOffers>
  ) {}

  async scheduleJobs(): Promise<void> {
    this.scheduleCleanExpiredTemporaryFilesJob();
    this.scheduleArchiveExpiredJobOffersJob();
  }

  private scheduleCleanExpiredTemporaryFilesJob(): void {
    this.schedule.scheduleIntervalJob(
      'clean-expired-temporary-files-job',
      this.ONE_HOUR_IN_MILLISECONDS,
      async () => {
        const deletedTemporaryFileIds: TemporaryFileId[] = await this.cleanExpiredTemporaryFilesProxyService.getInstance().execute();

        this.LOGGER.log(`Number of temporary fiels deleted: ${deletedTemporaryFileIds.length}`);
        this.LOGGER.debug(`Deleted temporary file ids: ${JSON.stringify(deletedTemporaryFileIds)}`);

        return false;
      },
      { immediate: true }
    );
  }

  private scheduleArchiveExpiredJobOffersJob(): void {
    this.schedule.scheduleIntervalJob(
      'archive-expired-job-offers-job',
      this.ONE_HOUR_IN_MILLISECONDS,
      async () => {
        const archivedJobOfferIds: JobOfferId[] = await this.archiveExpiredJobOffersProxyService.getInstance().execute();

        this.LOGGER.log(`Number of job offers archived: ${archivedJobOfferIds.length}`);
        this.LOGGER.debug(`Archived job offer ids: ${JSON.stringify(archivedJobOfferIds)}`);

        return false;
      },
      { immediate: true }
    );
  }
}

import { Module } from '@nestjs/common';
import { ScheduleModule } from 'nest-schedule';
import { ProxyServicesDynamicModule } from '../use_cases_proxy/proxy-services-dynamic.module';
import { BatchService } from './batch.service';

@Module({
  imports: [ScheduleModule.register(), ProxyServicesDynamicModule.register()],
  providers: [BatchService],
  exports: [BatchService],
})
export class BatchModule {}

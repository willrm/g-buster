import { Schedule } from 'nest-schedule';
import { ArchiveExpiredJobOffers } from '../../../use_cases/archive-expired-job-offers';
import { CleanExpiredTemporaryFiles } from '../../../use_cases/clean-expired-temporary-files';
import { UseCaseProxy } from '../../use_cases_proxy/use-case-proxy';
import { BatchService } from '../batch.service';

describe('infrastructure/batch/BatchService', () => {
  let batchService: BatchService;
  let mockSchedule: Schedule;
  let mockCleanExpiredTemporaryImagesProxyService: UseCaseProxy<CleanExpiredTemporaryFiles>;
  let mockCleanExpiredTemporaryImages: CleanExpiredTemporaryFiles;
  let mockArchiveExpiredJobOffersProxyService: UseCaseProxy<ArchiveExpiredJobOffers>;
  let mockArchiveExpiredJobOffers: ArchiveExpiredJobOffers;

  beforeEach(() => {
    mockSchedule = {} as Schedule;
    mockSchedule.scheduleIntervalJob = jest.fn();

    mockCleanExpiredTemporaryImagesProxyService = {} as UseCaseProxy<CleanExpiredTemporaryFiles>;
    mockCleanExpiredTemporaryImagesProxyService.getInstance = jest.fn();

    mockArchiveExpiredJobOffersProxyService = {} as UseCaseProxy<ArchiveExpiredJobOffers>;
    mockArchiveExpiredJobOffersProxyService.getInstance = jest.fn();

    mockCleanExpiredTemporaryImages = {} as CleanExpiredTemporaryFiles;
    mockCleanExpiredTemporaryImages.execute = jest.fn();
    (mockCleanExpiredTemporaryImagesProxyService.getInstance as jest.Mock).mockReturnValue(mockCleanExpiredTemporaryImages);

    mockArchiveExpiredJobOffers = {} as ArchiveExpiredJobOffers;
    mockArchiveExpiredJobOffers.execute = jest.fn();
    (mockArchiveExpiredJobOffersProxyService.getInstance as jest.Mock).mockReturnValue(mockArchiveExpiredJobOffers);

    batchService = new BatchService(mockSchedule, mockCleanExpiredTemporaryImagesProxyService, mockArchiveExpiredJobOffersProxyService);
  });

  describe('scheduleJobs', () => {
    describe('clean-expired-temporary-images-job', () => {
      beforeEach(() => {
        (mockCleanExpiredTemporaryImages.execute as jest.Mock).mockReturnValue(Promise.resolve([]));
      });

      it('should schedule job to clean expired temporary files with one hour interval and run it immediately', async () => {
        // when
        await batchService.scheduleJobs();

        // then
        expect(mockSchedule.scheduleIntervalJob).toHaveBeenCalledWith('clean-expired-temporary-files-job', 3600000, expect.any(Function), {
          immediate: true,
        });
      });

      it('should configure callback to execute use case', async () => {
        // given
        await batchService.scheduleJobs();
        const jobCallback: JobCallback = (mockSchedule.scheduleIntervalJob as jest.Mock).mock.calls[0][2];

        // when
        await jobCallback();

        // then
        expect(mockCleanExpiredTemporaryImages.execute).toHaveBeenCalled();
      });

      it('should configure callback to return false in order to not cancel job after execution', async () => {
        // given
        await batchService.scheduleJobs();
        const jobCallback: JobCallback = (mockSchedule.scheduleIntervalJob as jest.Mock).mock.calls[0][2];

        // when
        const stop: boolean = await jobCallback();

        // then
        expect(stop).toBe(false);
      });
    });
    describe('archive-expired-job-offers-job', () => {
      beforeEach(() => {
        (mockArchiveExpiredJobOffers.execute as jest.Mock).mockReturnValue(Promise.resolve([]));
      });

      it('should schedule job to archive expired job offers with one hour interval and run it immediately', async () => {
        // when
        await batchService.scheduleJobs();

        // then
        expect(mockSchedule.scheduleIntervalJob).toHaveBeenCalledWith('archive-expired-job-offers-job', 3600000, expect.any(Function), {
          immediate: true,
        });
      });

      it('should configure callback to execute use case', async () => {
        // given
        await batchService.scheduleJobs();
        const jobCallback: JobCallback = (mockSchedule.scheduleIntervalJob as jest.Mock).mock.calls[1][2];

        // when
        await jobCallback();

        // then
        expect(mockArchiveExpiredJobOffers.execute).toHaveBeenCalled();
      });

      it('should configure callback to return false in order to not cancel job after execution', async () => {
        // given
        await batchService.scheduleJobs();
        const jobCallback: JobCallback = (mockSchedule.scheduleIntervalJob as jest.Mock).mock.calls[1][2];

        // when
        const stop: boolean = await jobCallback();

        // then
        expect(stop).toBe(false);
      });
    });
  });
});

import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request, { Response } from 'supertest';
import { GeniusProductInterface } from '../../../src/domain/genius-product/genius-product.interface';
import { Auth0Strategy } from '../../../src/infrastructure/config/authentication/auth0-strategy';
import { EnvironmentConfigService } from '../../../src/infrastructure/config/environment-config/environment-config.service';
import { GeniusProductResponseInterface } from '../../../src/infrastructure/rest/product/models/genius-product-response.interface';
import { RestModule } from '../../../src/infrastructure/rest/rest.module';
import { ProxyServicesDynamicModule } from '../../../src/infrastructure/use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../../src/infrastructure/use_cases_proxy/use-case-proxy';
import { GetGeniusProducts } from '../../../src/use_cases/get-genius-products';
import { e2eEnvironmentConfigService } from '../../e2e-config';

describe('infrastructure/rest/product/ProductController (e2e)', () => {
  let app: INestApplication;
  let mockGetGeniusProducts: GetGeniusProducts;

  beforeAll(async () => {
    mockGetGeniusProducts = {} as GetGeniusProducts;
    mockGetGeniusProducts.execute = jest.fn();
    const mockGetGeniusProductsProxyService: UseCaseProxy<GetGeniusProducts> = {
      getInstance: () => mockGetGeniusProducts,
    } as UseCaseProxy<GetGeniusProducts>;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RestModule],
    })
      .overrideProvider(ProxyServicesDynamicModule.GET_GENIUS_PRODUCTS_PROXY_SERVICE)
      .useValue(mockGetGeniusProductsProxyService)
      .overrideProvider(EnvironmentConfigService)
      .useValue(e2eEnvironmentConfigService)
      .overrideProvider(Auth0Strategy)
      .useValue({})
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('GET /product/genius-products', () => {
    it('should call use case', () => {
      // given
      (mockGetGeniusProducts.execute as jest.Mock).mockReturnValue(Promise.resolve([]));

      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/product/genius-products');

      // then
      return testRequest.expect(200).expect((response: Response) => {
        expect(mockGetGeniusProducts.execute).toHaveBeenCalled();
      });
    });

    it('should return genius products', () => {
      // given
      (mockGetGeniusProducts.execute as jest.Mock).mockReturnValue(
        Promise.resolve([{ name: 'Genius Product 1' }, { name: 'Genius Product 2' }] as GeniusProductInterface[])
      );

      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/product/genius-products');

      // then
      return testRequest.expect(200).expect([{ name: 'Genius Product 1' }, { name: 'Genius Product 2' }] as GeniusProductResponseInterface[]);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});

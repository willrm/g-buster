import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request, { Response } from 'supertest';
import { CompanyStatus } from '../../../src/domain/company/company-enums';
import { CompanyInterface } from '../../../src/domain/company/company.interface';
import { Auth0Strategy } from '../../../src/infrastructure/config/authentication/auth0-strategy';
import { EnvironmentConfigService } from '../../../src/infrastructure/config/environment-config/environment-config.service';
import { CompanyResponseInterface } from '../../../src/infrastructure/rest/career/models/company-response.interface';
import { JobOfferEnums } from '../../../src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { RestModule } from '../../../src/infrastructure/rest/rest.module';
import { ProxyServicesDynamicModule } from '../../../src/infrastructure/use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../../src/infrastructure/use_cases_proxy/use-case-proxy';
import { GetCompanies } from '../../../src/use_cases/get-companies';
import { UpdateUnpublishedCompany } from '../../../src/use_cases/update-unpublished-company';
import { e2eEnvironmentConfigService, e2eUserWithAdminRoleCookie, e2eUserWithCompanyRoleCookie } from '../../e2e-config';

describe('infrastructure/rest/admin/AdminController (e2e)', () => {
  let app: INestApplication;
  let mockGetCompanies: GetCompanies;
  let mockUpdateUnpublishedCompany: UpdateUnpublishedCompany;

  beforeAll(async () => {
    mockGetCompanies = {} as GetCompanies;
    mockGetCompanies.execute = jest.fn();
    const mockGetCompaniesProxyService: UseCaseProxy<GetCompanies> = {
      getInstance: () => mockGetCompanies,
    } as UseCaseProxy<GetCompanies>;

    mockUpdateUnpublishedCompany = {} as UpdateUnpublishedCompany;
    mockUpdateUnpublishedCompany.execute = jest.fn();
    const mockUpdateUnpublishedCompanyProxyService: UseCaseProxy<UpdateUnpublishedCompany> = {
      getInstance: () => mockUpdateUnpublishedCompany,
    } as UseCaseProxy<UpdateUnpublishedCompany>;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RestModule],
    })
      .overrideProvider(ProxyServicesDynamicModule.GET_COMPANIES_PROXY_SERVICE)
      .useValue(mockGetCompaniesProxyService)
      .overrideProvider(EnvironmentConfigService)
      .useValue(e2eEnvironmentConfigService)
      .overrideProvider(Auth0Strategy)
      .useValue({})
      .overrideProvider(ProxyServicesDynamicModule.UPDATE_UNPUBLISHED_COMPANY_PROXY_SERVICE)
      .useValue(mockUpdateUnpublishedCompanyProxyService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('GET /admin/companies', () => {
    it('should return forbidden status when not authenticated', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/admin/companies');

      // then
      return testRequest.expect(403);
    });

    it('should return forbidden status when authenticated with a different role than admin', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/admin/companies')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(403);
    });

    it('should return companies when correctly authenticated', () => {
      // given
      const companies: CompanyInterface[] = [
        { id: '1', name: 'A company name' } as CompanyInterface,
        { id: '2', name: 'An other company name' } as CompanyInterface,
      ];
      (mockGetCompanies.execute as jest.Mock).mockReturnValue(Promise.resolve(companies));

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/admin/companies')
        .set('Cookie', e2eUserWithAdminRoleCookie);

      // then
      return testRequest.expect(200).expect(companies as CompanyResponseInterface[]);
    });
  });

  describe('PUT /admin/companies/:id', () => {
    it('should return forbidden status when not authenticated', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).put('/admin/companies/12');

      // then
      return testRequest.expect(403);
    });

    it('should return forbidden status when authenticated with a different role than admin', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .put('/admin/companies/12')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(403);
    });

    it('should return 200 when company is update', () => {
      // given
      (mockGetCompanies.execute as jest.Mock).mockReturnValue(Promise.resolve());
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .put('/admin/companies/12')
        .set('Cookie', e2eUserWithAdminRoleCookie);

      // then
      return testRequest.expect(200);
    });
  });
});

import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request, { Response } from 'supertest';
import { Auth0Strategy } from '../../../src/infrastructure/config/authentication/auth0-strategy';
import { EnvironmentConfigService } from '../../../src/infrastructure/config/environment-config/environment-config.service';
import { RestModule } from '../../../src/infrastructure/rest/rest.module';
import { e2eEnvironmentConfigService } from '../../e2e-config';

describe('infrastructure/rest/i18n/I18nMessagesController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RestModule],
    })
      .overrideProvider(EnvironmentConfigService)
      .useValue(e2eEnvironmentConfigService)
      .overrideProvider(Auth0Strategy)
      .useValue({})
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('/i18n/messages/:lang', () => {
    it('GET /i18n/messages/fr-ca should return messages for locale fr-ca', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/i18n/messages/fr-ca');

      // then
      return testRequest.expect(200).expect((response: Response) => {
        expect(response.body).toMatchObject({ welcome: 'Un message de bienvenue', thanks: { you: 'Un message de remerciement' } });
      });
    });
    it('GET /i18n/messages/en-ca should return messages for locale en-ca', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/i18n/messages/en-ca');

      // then
      return testRequest.expect(200).expect((response: Response) => {
        expect(response.body).toMatchObject({ welcome: 'A welcome message', thanks: { you: 'A thank you message' } });
      });
    });
  });

  afterAll(async () => {
    await app.close();
  });
});

import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { ValidationError } from 'class-validator';
import request, { Response } from 'supertest';
import { DuplicateItemError } from '../../../src/domain/common-errors/duplicate-item.error';
import { ItemNotFoundError } from '../../../src/domain/common-errors/item-not-found.error';
import { Company } from '../../../src/domain/company/company';
import { CompanyInterface } from '../../../src/domain/company/company.interface';
import { CompanyRawInterface } from '../../../src/domain/company/company.raw.interface';
import { CompanyId } from '../../../src/domain/type-aliases';
import { DomainValidationError } from '../../../src/domain/validation/domain-validation.error';
import { Auth0Strategy } from '../../../src/infrastructure/config/authentication/auth0-strategy';
import { EnvironmentConfigService } from '../../../src/infrastructure/config/environment-config/environment-config.service';
import { CompanyResponseInterface } from '../../../src/infrastructure/rest/career/models/company-response.interface';
import { PostCompanyRequestInterface } from '../../../src/infrastructure/rest/career/models/post-company-request.interface';
import { RestModule } from '../../../src/infrastructure/rest/rest.module';
import { ProxyServicesDynamicModule } from '../../../src/infrastructure/use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../../src/infrastructure/use_cases_proxy/use-case-proxy';
import { CreateNewCompany } from '../../../src/use_cases/create-new-company';
import { GetPublishedCompanyById } from '../../../src/use_cases/get-published-company-by-id';
import { ValidateCompany } from '../../../src/use_cases/validate-company';
import { e2eEnvironmentConfigService, e2eUserWithCandidateRoleCookie, e2eUserWithCompanyRoleCookie } from '../../e2e-config';

describe('infrastructure/rest/career/CompanyController (e2e)', () => {
  let app: INestApplication;
  let mockCreateNewCompany: CreateNewCompany;
  let mockValidateCompany: ValidateCompany;
  let mockGetPublishedCompanyById: GetPublishedCompanyById;

  beforeAll(async () => {
    mockCreateNewCompany = {} as CreateNewCompany;
    mockCreateNewCompany.execute = jest.fn();
    const mockCreateNewCompanyProxyService: UseCaseProxy<CreateNewCompany> = {
      getInstance: () => mockCreateNewCompany,
    } as UseCaseProxy<CreateNewCompany>;

    mockValidateCompany = {} as ValidateCompany;
    mockValidateCompany.execute = jest.fn();
    const mockValidateCompanyProxyService: UseCaseProxy<ValidateCompany> = {
      getInstance: () => mockValidateCompany,
    } as UseCaseProxy<ValidateCompany>;

    mockGetPublishedCompanyById = {} as GetPublishedCompanyById;
    mockGetPublishedCompanyById.execute = jest.fn();
    const mockGetPublishedCompanyByIdProxyService: UseCaseProxy<GetPublishedCompanyById> = {
      getInstance: () => mockGetPublishedCompanyById,
    } as UseCaseProxy<GetPublishedCompanyById>;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RestModule],
    })
      .overrideProvider(ProxyServicesDynamicModule.CREATE_NEW_COMPANY_PROXY_SERVICE)
      .useValue(mockCreateNewCompanyProxyService)
      .overrideProvider(ProxyServicesDynamicModule.VALIDATE_COMPANY_PROXY_SERVICE)
      .useValue(mockValidateCompanyProxyService)
      .overrideProvider(ProxyServicesDynamicModule.GET_PUBLISHED_COMPANY_BY_ID_PROXY_SERVICE)
      .useValue(mockGetPublishedCompanyByIdProxyService)
      .overrideProvider(EnvironmentConfigService)
      .useValue(e2eEnvironmentConfigService)
      .overrideProvider(Auth0Strategy)
      .useValue({})
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('POST /career/companies', () => {
    it('should return created status with location to created company using id', () => {
      // given
      (mockCreateNewCompany.execute as jest.Mock).mockReturnValue(Promise.resolve('12' as CompanyId));

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/companies')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(201).expect((response: Response) => {
        expect(response.header.location).toBe('/career/companies/12');
      });
    });

    it('should return created status with company id in body', () => {
      // given
      const companyId: CompanyId = '12';
      (mockCreateNewCompany.execute as jest.Mock).mockReturnValue(Promise.resolve(companyId));

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/companies')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(201).expect((response: Response) => {
        expect(response.body).toStrictEqual({ id: companyId });
      });
    });

    it('should validate company with data from body when query param is true', () => {
      // given
      const expected: PostCompanyRequestInterface = { name: 'A company name' } as PostCompanyRequestInterface;

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/companies?dryRun=true')
        .set('Cookie', e2eUserWithCompanyRoleCookie)
        .send(expected);

      // then
      return testRequest.then((response: Response) => {
        expect(mockValidateCompany.execute).toHaveBeenCalledWith(expected as CompanyRawInterface);
      });
    });

    it('should create company with data from body when query param is false', () => {
      // given
      const expected: PostCompanyRequestInterface = { name: 'A company name' } as PostCompanyRequestInterface;

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/companies?dryRun=false')
        .set('Cookie', e2eUserWithCompanyRoleCookie)
        .send(expected);

      // then
      return testRequest.then((response: Response) => {
        expect(mockCreateNewCompany.execute).toHaveBeenCalledWith(expected as CompanyRawInterface);
      });
    });

    it('should create company with data from body when no query param', () => {
      // given
      const expected: PostCompanyRequestInterface = { name: 'A company name' } as PostCompanyRequestInterface;

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/companies')
        .set('Cookie', e2eUserWithCompanyRoleCookie)
        .send(expected);

      // then
      return testRequest.then((response: Response) => {
        expect(mockCreateNewCompany.execute).toHaveBeenCalledWith(expected as CompanyRawInterface);
      });
    });

    it('should return http bad request with domain errors when DomainValidationError is thrown', () => {
      // given
      (mockCreateNewCompany.execute as jest.Mock).mockImplementation(() => {
        const errors: ValidationError[] = [
          {
            property: 'property',
            value: 'value',
            constraints: {
              constraintKey1: 'constraintValue1',
              constraintKey2: 'constraintValue2',
            },
            children: [],
          } as ValidationError,
        ];
        throw new DomainValidationError('Company', 'creation error', errors);
      });

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/companies')
        .set('Cookie', e2eUserWithCompanyRoleCookie)
        .send({} as PostCompanyRequestInterface);

      // then
      return testRequest.expect(400).then((response: Response) => {
        expect(response.body).toMatchObject({
          statusCode: 400,
          name: 'DomainValidationError',
          message: 'Company creation error:\n - constraintValue1,\n - constraintValue2',
          domainErrors: {
            property: {
              i18nMessageKeys: ['company.propertyConstraintKey1Error', 'company.propertyConstraintKey2Error'],
              messages: ['constraintValue1', 'constraintValue2'],
            },
          },
        });
      });
    });

    it('should return http bad request when company already exists', () => {
      // given
      (mockCreateNewCompany.execute as jest.Mock).mockImplementation(() => {
        throw new DuplicateItemError('Company already exists for user');
      });

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/companies')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(400);
    });

    it('should return forbidden status when not authenticated', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).post('/career/companies');

      // then
      return testRequest.expect(403);
    });

    it('should return forbidden status when authenticated with a different role than company', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/companies')
        .set('Cookie', e2eUserWithCandidateRoleCookie);

      // then
      return testRequest.expect(403);
    });
  });

  describe('GET /career/companies/:id', () => {
    it('should return found company by id', () => {
      // given
      (mockGetPublishedCompanyById.execute as jest.Mock).mockReturnValue(Promise.resolve({ id: '12', name: 'A company name' } as CompanyInterface));

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/career/companies/12')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(200).expect({ id: '12', name: 'A company name' } as CompanyResponseInterface);
    });

    it('should find company using id from params', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/career/companies/12')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.then((response: Response) => {
        expect(mockGetPublishedCompanyById.execute).toHaveBeenCalledWith('12');
      });
    });

    it('should return http not found when company not found', () => {
      // given
      (mockGetPublishedCompanyById.execute as jest.Mock).mockImplementation(() => {
        throw new ItemNotFoundError('Company not found');
      });

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/career/companies/1234')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(404);
    });

    it('should return forbidden status when not authenticated', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/career/companies/1337');

      // then
      return testRequest.expect(403);
    });

    it('should return forbidden status when authenticated with a different role than company', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/career/companies/12345')
        .set('Cookie', e2eUserWithCandidateRoleCookie);

      // then
      return testRequest.expect(403);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});

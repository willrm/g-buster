import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { ValidationError } from 'class-validator';
import request, { Response } from 'supertest';
import { AccessDeniedError } from '../../../src/domain/common-errors/access-denied.error';
import { JobOfferRawInterface } from '../../../src/domain/job-offer/job-offer.raw.interface';
import { JobOfferId, Uuid } from '../../../src/domain/type-aliases';
import { DomainValidationError } from '../../../src/domain/validation/domain-validation.error';
import { Auth0Strategy } from '../../../src/infrastructure/config/authentication/auth0-strategy';
import { EnvironmentConfigService } from '../../../src/infrastructure/config/environment-config/environment-config.service';
import { JobOfferEnums } from '../../../src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { JobOfferWithCompanyResponseInterface } from '../../../src/infrastructure/rest/career/models/job-offer-response.interface';
import { PostApplyJobOfferRequestInterface } from '../../../src/infrastructure/rest/career/models/post-apply-job-offer-request.interface';
import { PostJobOfferRequestInterface } from '../../../src/infrastructure/rest/career/models/post-job-offer-request.interface';
import { RestModule } from '../../../src/infrastructure/rest/rest.module';
import { ProxyServicesDynamicModule } from '../../../src/infrastructure/use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../../src/infrastructure/use_cases_proxy/use-case-proxy';
import { JobOfferWithCompanyInterface } from '../../../src/use_cases/adapters/job-offer-with-company.interface';
import { ApplyToJobOffer } from '../../../src/use_cases/apply-to-job-offer';
import { CreateNewJobOffer } from '../../../src/use_cases/create-new-job-offer';
import { GetPublishedJobOfferById } from '../../../src/use_cases/get-published-job-offer-by-id';
import { GetPublishedJobOffers } from '../../../src/use_cases/get-published-job-offers';
import { UpdateDraftJobOffer } from '../../../src/use_cases/update-draft-job-offer';
import { ValidateJobOffer } from '../../../src/use_cases/validate-job-offer';
import { e2eEnvironmentConfigService, e2eUserWithCandidateRoleCookie, e2eUserWithCompanyRoleCookie } from '../../e2e-config';

describe('infrastructure/rest/career/JobOfferController (e2e)', () => {
  let app: INestApplication;
  let mockGetPublishedJobOffers: GetPublishedJobOffers;
  let mockGetPublishedJobOfferById: GetPublishedJobOfferById;
  let mockApplyToJobOffer: ApplyToJobOffer;
  let mockCreateNewJobOffer: CreateNewJobOffer;
  let mockValidateJobOffer: ValidateJobOffer;
  let mockUpdateDraftJobOffer: UpdateDraftJobOffer;

  beforeAll(async () => {
    mockGetPublishedJobOffers = {} as GetPublishedJobOffers;
    mockGetPublishedJobOffers.execute = jest.fn();
    const mockGetPublishedJobOffersProxyService: UseCaseProxy<GetPublishedJobOffers> = {
      getInstance: () => mockGetPublishedJobOffers,
    } as UseCaseProxy<GetPublishedJobOffers>;

    mockGetPublishedJobOfferById = {} as GetPublishedJobOfferById;
    mockGetPublishedJobOfferById.execute = jest.fn();
    const mockGetPublishedJobOfferByIdProxyService: UseCaseProxy<GetPublishedJobOfferById> = {
      getInstance: () => mockGetPublishedJobOfferById,
    } as UseCaseProxy<GetPublishedJobOfferById>;

    mockApplyToJobOffer = {} as ApplyToJobOffer;
    mockApplyToJobOffer.execute = jest.fn();
    const mockApplyToJobOfferProxyService: UseCaseProxy<ApplyToJobOffer> = {
      getInstance: () => mockApplyToJobOffer,
    } as UseCaseProxy<ApplyToJobOffer>;

    mockCreateNewJobOffer = {} as CreateNewJobOffer;
    mockCreateNewJobOffer.execute = jest.fn();
    const mockCreateNewJobOfferProxyService: UseCaseProxy<CreateNewJobOffer> = {
      getInstance: () => mockCreateNewJobOffer,
    } as UseCaseProxy<CreateNewJobOffer>;

    mockValidateJobOffer = {} as ValidateJobOffer;
    mockValidateJobOffer.execute = jest.fn();
    const mockValidateJobOfferProxyService: UseCaseProxy<ValidateJobOffer> = {
      getInstance: () => mockValidateJobOffer,
    } as UseCaseProxy<ValidateJobOffer>;

    mockUpdateDraftJobOffer = {} as UpdateDraftJobOffer;
    mockUpdateDraftJobOffer.execute = jest.fn();
    const mockUpdateDraftJobOfferProxyService: UseCaseProxy<UpdateDraftJobOffer> = {
      getInstance: () => mockUpdateDraftJobOffer,
    } as UseCaseProxy<UpdateDraftJobOffer>;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RestModule],
    })
      .overrideProvider(ProxyServicesDynamicModule.GET_PUBLISHED_JOB_OFFERS_PROXY_SERVICE)
      .useValue(mockGetPublishedJobOffersProxyService)
      .overrideProvider(ProxyServicesDynamicModule.GET_PUBLISHED_JOB_OFFER_BY_ID_PROXY_SERVICE)
      .useValue(mockGetPublishedJobOfferByIdProxyService)
      .overrideProvider(ProxyServicesDynamicModule.APPLY_TO_JOB_OFFER_PROXY_SERVICE)
      .useValue(mockApplyToJobOfferProxyService)
      .overrideProvider(ProxyServicesDynamicModule.CREATE_NEW_JOB_OFFER_PROXY_SERVICE)
      .useValue(mockCreateNewJobOfferProxyService)
      .overrideProvider(ProxyServicesDynamicModule.VALIDATE_JOB_OFFER_PROXY_SERVICE)
      .useValue(mockValidateJobOfferProxyService)
      .overrideProvider(ProxyServicesDynamicModule.UPDATE_DRAFT_JOB_OFFER_PROXY_SERVICE)
      .useValue(mockUpdateDraftJobOfferProxyService)
      .overrideProvider(EnvironmentConfigService)
      .useValue(e2eEnvironmentConfigService)
      .overrideProvider(Auth0Strategy)
      .useValue({})
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('GET /career/job-offers', () => {
    it('should call use case with filter data from query params', () => {
      // given
      (mockGetPublishedJobOffers.execute as jest.Mock).mockReturnValue(Promise.resolve([]));

      // when
      const testRequest: request.Test = request(app.getHttpServer()).get(
        '/career/job-offers' +
          '?requestedSpecialities=REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE&requestedSpecialities=REQUESTED_SPECIALITY_AGRICULTURE' +
          '&jobTypes=JOB_TYPE_REGULAR&jobTypes=JOB_TYPE_TEMPORARY' +
          '&requiredExperiences=REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS&requiredExperiences=REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS' +
          '&regions=REGION_ABITIBI_TEMISCAMINGUE&regions=REGION_BAS_ST_LAURENT'
      );

      // then
      return testRequest.expect(200).expect((response: Response) => {
        expect(mockGetPublishedJobOffers.execute).toHaveBeenCalledWith({
          requestedSpecialities: [
            JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
            JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
          ],
          jobTypes: [JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR, JobOfferEnums.JobOfferJobType.JOB_TYPE_TEMPORARY],
          requiredExperiences: [
            JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS,
            JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
          ],
          regions: [JobOfferEnums.JobOfferRegion.REGION_ABITIBI_TEMISCAMINGUE, JobOfferEnums.JobOfferRegion.REGION_BAS_ST_LAURENT],
        });
      });
    });

    it('should return found job offers', () => {
      // given
      (mockGetPublishedJobOffers.execute as jest.Mock).mockReturnValue(
        Promise.resolve([{ title: 'Job offer 1' }, { title: 'Job offer 2' }] as JobOfferWithCompanyInterface[])
      );

      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/career/job-offers');

      // then
      return testRequest.expect(200).expect([{ title: 'Job offer 1' }, { title: 'Job offer 2' }] as JobOfferWithCompanyResponseInterface[]);
    });
  });

  describe('GET /career/job-offers/:id', () => {
    it('should return found job offer', () => {
      // given
      (mockGetPublishedJobOfferById.execute as jest.Mock).mockReturnValue(Promise.resolve({ title: 'Job offer 1' } as JobOfferWithCompanyInterface));

      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/career/job-offers/12');

      // then
      return testRequest.expect(200).expect({ title: 'Job offer 1' } as JobOfferWithCompanyResponseInterface);
    });
  });

  describe('POST /career/job-offers', () => {
    it('should return created status with location to created job offer using id', () => {
      // given
      const jobOfferId: JobOfferId = '123456789';
      (mockCreateNewJobOffer.execute as jest.Mock).mockReturnValue(Promise.resolve(jobOfferId));

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/job-offers')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(201).expect((response: Response) => {
        expect(response.header.location).toBe('/career/job-offers/123456789');
      });
    });

    it('should return created status with job offer id in body', () => {
      // given
      const jobOfferId: JobOfferId = '123456789';
      (mockCreateNewJobOffer.execute as jest.Mock).mockReturnValue(Promise.resolve(jobOfferId));

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/job-offers')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(201).expect((response: Response) => {
        expect(response.body).toStrictEqual({ id: jobOfferId });
      });
    });

    it('should validate job offer with data from body when query param is true', () => {
      // given
      const expected: PostJobOfferRequestInterface = { title: 'A job title' } as PostJobOfferRequestInterface;

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/job-offers?dryRun=true')
        .set('Cookie', e2eUserWithCompanyRoleCookie)
        .send(expected);

      // then
      return testRequest.then((response: Response) => {
        expect(mockValidateJobOffer.execute).toHaveBeenCalledWith(expected as JobOfferRawInterface);
      });
    });

    it('should create job offer with data from body when query param is false', () => {
      // given
      const expected: PostJobOfferRequestInterface = { title: 'A job title' } as PostJobOfferRequestInterface;

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/job-offers?dryRun=false')
        .set('Cookie', e2eUserWithCompanyRoleCookie)
        .send(expected);

      // then
      return testRequest.then((response: Response) => {
        expect(mockCreateNewJobOffer.execute).toHaveBeenCalledWith(expected as JobOfferRawInterface);
      });
    });

    it('should create job offer with data from body when no query param', () => {
      // given
      const expected: PostJobOfferRequestInterface = { title: 'A job title' } as PostJobOfferRequestInterface;

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/job-offers')
        .set('Cookie', e2eUserWithCompanyRoleCookie)
        .send(expected);

      // then
      return testRequest.then((response: Response) => {
        expect(mockCreateNewJobOffer.execute).toHaveBeenCalledWith(expected as JobOfferRawInterface);
      });
    });

    it('should return http bad request with domain errors when DomainValidationError is thrown', () => {
      // given
      (mockCreateNewJobOffer.execute as jest.Mock).mockImplementation(() => {
        const errors: ValidationError[] = [
          {
            property: 'property',
            value: 'value',
            constraints: {
              constraintKey1: 'constraintValue1',
              constraintKey2: 'constraintValue2',
            },
            children: [],
          } as ValidationError,
        ];
        throw new DomainValidationError('JobOffer', 'creation error', errors);
      });

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/job-offers')
        .set('Cookie', e2eUserWithCompanyRoleCookie)
        .send({} as PostJobOfferRequestInterface);

      // then
      return testRequest.expect(400).then((response: Response) => {
        expect(response.body).toMatchObject({
          statusCode: 400,
          name: 'DomainValidationError',
          message: 'JobOffer creation error:\n - constraintValue1,\n - constraintValue2',
          domainErrors: {
            property: {
              i18nMessageKeys: ['jobOffer.propertyConstraintKey1Error', 'jobOffer.propertyConstraintKey2Error'],
              messages: ['constraintValue1', 'constraintValue2'],
            },
          },
        });
      });
    });

    it('should return forbidden status when not authenticated', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).post('/career/job-offers');

      // then
      return testRequest.expect(403);
    });

    it('should return forbidden status when authenticated with a different role than company', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/job-offers')
        .set('Cookie', e2eUserWithCandidateRoleCookie);

      // then
      return testRequest.expect(403);
    });
  });

  describe('PUT /career/job-offers/:id', () => {
    it('should return updated status with location to updated job offer using id', () => {
      // given
      const jobOfferId: JobOfferId = '123456789';
      (mockUpdateDraftJobOffer.execute as jest.Mock).mockReturnValue(Promise.resolve(jobOfferId));

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .put('/career/job-offers/' + jobOfferId)
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(200).expect((response: Response) => {
        expect(response.header.location).toBe('/career/job-offers/123456789');
      });
    });

    it('should return forbidden status when not authenticated', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).put('/career/job-offers/12');

      // then
      return testRequest.expect(403);
    });

    it('should return forbidden status when authenticated with a different role than company', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .put('/career/job-offers/12')
        .set('Cookie', e2eUserWithCandidateRoleCookie);

      // then
      return testRequest.expect(403);
    });
  });

  describe('POST /career/job-offers/:id/apply', () => {
    it('should return created status', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/job-offers/123456789/apply')
        .set('Cookie', e2eUserWithCandidateRoleCookie)
        .send({} as PostApplyJobOfferRequestInterface);

      // then
      return testRequest.expect(201);
    });

    it('should apply to job offer with id param and resume uuid from body', () => {
      // given
      const jobOfferId: JobOfferId = '123456789';
      const resumeTemporaryFileUuid: Uuid = 'resume-uuid';
      const coverLetterTemporaryFileUuid: Uuid = 'cover-letter-uuid';
      const body: PostApplyJobOfferRequestInterface = { resumeUuid: resumeTemporaryFileUuid, coverLetterUuid: coverLetterTemporaryFileUuid };

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post(`/career/job-offers/${jobOfferId}/apply`)
        .set('Cookie', e2eUserWithCandidateRoleCookie)
        .send(body);

      // then
      return testRequest.then((response: Response) => {
        expect(mockApplyToJobOffer.execute).toHaveBeenCalledWith(jobOfferId, resumeTemporaryFileUuid, coverLetterTemporaryFileUuid);
      });
    });

    it('should return forbidden when AccessDeniedError is thrown', () => {
      // given
      (mockApplyToJobOffer.execute as jest.Mock).mockImplementation(() => {
        throw new AccessDeniedError('Current user is not a Candidate');
      });

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post(`/career/job-offers/123456789/apply`)
        .set('Cookie', e2eUserWithCandidateRoleCookie)
        .send({} as PostApplyJobOfferRequestInterface);

      // then
      return testRequest.expect(403).then((response: Response) => {
        expect(response.body).toMatchObject({
          statusCode: 403,
          message: 'Current user is not a Candidate',
        });
      });
    });

    it('should return forbidden status when not authenticated', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/job-offers/123456789/apply')
        .send({} as PostApplyJobOfferRequestInterface);

      // then
      return testRequest.expect(403);
    });

    it('should return forbidden status when authenticated with a different role than candidate', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .post('/career/job-offers/123456789/apply')
        .set('Cookie', e2eUserWithCompanyRoleCookie)
        .send({} as PostApplyJobOfferRequestInterface);

      // then
      return testRequest.expect(403);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});

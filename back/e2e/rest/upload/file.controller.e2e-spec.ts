import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request from 'supertest';
import { Auth0Strategy } from '../../../src/infrastructure/config/authentication/auth0-strategy';
import { EnvironmentConfigService } from '../../../src/infrastructure/config/environment-config/environment-config.service';
import { RestModule } from '../../../src/infrastructure/rest/rest.module';
import { ProxyServicesDynamicModule } from '../../../src/infrastructure/use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../../src/infrastructure/use_cases_proxy/use-case-proxy';
import { CreateTemporaryFile } from '../../../src/use_cases/create-temporary-file';
import { e2eEnvironmentConfigService } from '../../e2e-config';

describe('infrastructure/rest/upload/FileController (e2e)', () => {
  let app: INestApplication;
  let mockCreateTemporaryImage: CreateTemporaryFile;

  beforeAll(async () => {
    mockCreateTemporaryImage = {} as CreateTemporaryFile;
    mockCreateTemporaryImage.execute = jest.fn();
    const mockCreateTemporaryImageProxyService: UseCaseProxy<CreateTemporaryFile> = {
      getInstance: () => mockCreateTemporaryImage,
    } as UseCaseProxy<CreateTemporaryFile>;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RestModule],
    })
      .overrideProvider(ProxyServicesDynamicModule.CREATE_TEMPORARY_IMAGE_PROXY_SERVICE)
      .useValue(mockCreateTemporaryImageProxyService)
      .overrideProvider(EnvironmentConfigService)
      .useValue(e2eEnvironmentConfigService)
      .overrideProvider(Auth0Strategy)
      .useValue({})
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('POST /upload/file/temporary', () => {
    it('should return forbidden status when not authenticated', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).post('/upload/file/temporary');

      // then
      return testRequest.expect(403);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});

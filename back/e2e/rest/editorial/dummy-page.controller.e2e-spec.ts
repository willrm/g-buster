import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request, { Response } from 'supertest';
import { ItemNotFoundError } from '../../../src/domain/common-errors/item-not-found.error';
import { DummyPageInterface } from '../../../src/domain/dummy-page/dummy-page.interface';
import { Auth0Strategy } from '../../../src/infrastructure/config/authentication/auth0-strategy';
import { EnvironmentConfigService } from '../../../src/infrastructure/config/environment-config/environment-config.service';
import { DummyPageResponseInterface } from '../../../src/infrastructure/rest/editorial/models/dummy-page-response.interface';
import { RestModule } from '../../../src/infrastructure/rest/rest.module';
import { ProxyServicesDynamicModule } from '../../../src/infrastructure/use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../../src/infrastructure/use_cases_proxy/use-case-proxy';
import { GetDummyPage } from '../../../src/use_cases/get-dummy-page';
import { e2eEnvironmentConfigService } from '../../e2e-config';

describe('infrastructure/rest/editorial/DummyPageController (e2e)', () => {
  let app: INestApplication;
  let mockGetDummyPage: GetDummyPage;

  beforeAll(async () => {
    mockGetDummyPage = {} as GetDummyPage;
    mockGetDummyPage.execute = jest.fn();
    const mockGetDummyPageProxyService: UseCaseProxy<GetDummyPage> = {
      getInstance: () => mockGetDummyPage,
    } as UseCaseProxy<GetDummyPage>;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RestModule],
    })
      .overrideProvider(ProxyServicesDynamicModule.GET_DUMMY_PAGE_PROXY_SERVICE)
      .useValue(mockGetDummyPageProxyService)
      .overrideProvider(EnvironmentConfigService)
      .useValue(e2eEnvironmentConfigService)
      .overrideProvider(Auth0Strategy)
      .useValue({})
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('GET /:lang/dummy-page/:uid', () => {
    it('should return found dummy page by iid', () => {
      // given
      (mockGetDummyPage.execute as jest.Mock).mockReturnValue(Promise.resolve({ title: 'A dummy page title' } as DummyPageInterface));

      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/fr-ca/dummy-page/a-dummy-page-uid');

      // then
      return testRequest.expect(200).expect({ title: 'A dummy page title' } as DummyPageResponseInterface);
    });

    it('should find dummy page using lang and uid from params', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/fr-ca/dummy-page/a-dummy-page-uid');

      // then
      return testRequest.then((response: Response) => {
        expect(mockGetDummyPage.execute).toHaveBeenCalledWith('fr-ca', 'a-dummy-page-uid');
      });
    });

    it('should return http not found when dummy page not found', () => {
      // given
      (mockGetDummyPage.execute as jest.Mock).mockImplementation(() => {
        throw new ItemNotFoundError('Document not found');
      });

      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/fr-ca/dummy-page/an-unknown-dummy-page-uid');

      // then
      return testRequest.expect(404);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});

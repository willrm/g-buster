import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request, { Response } from 'supertest';
import { ItemNotFoundError } from '../../../src/domain/common-errors/item-not-found.error';
import { JobOfferInterface } from '../../../src/domain/job-offer/job-offer.interface';
import { UserInterface } from '../../../src/domain/user/user.interface';
import { Auth0Strategy } from '../../../src/infrastructure/config/authentication/auth0-strategy';
import { EnvironmentConfigService } from '../../../src/infrastructure/config/environment-config/environment-config.service';
import { JobOfferResponseInterface } from '../../../src/infrastructure/rest/career/models/job-offer-response.interface';
import { RestModule } from '../../../src/infrastructure/rest/rest.module';
import { UserResponseInterface } from '../../../src/infrastructure/rest/user/models/user-response.interface';
import { ProxyServicesDynamicModule } from '../../../src/infrastructure/use_cases_proxy/proxy-services-dynamic.module';
import { UseCaseProxy } from '../../../src/infrastructure/use_cases_proxy/use-case-proxy';
import { GetCurrentUser } from '../../../src/use_cases/get-current-user';
import { GetUserJobOfferById } from '../../../src/use_cases/get-user-job-offer-by-id';
import { GetUserJobOffers } from '../../../src/use_cases/get-user-job-offers';
import { LogInUser } from '../../../src/use_cases/log-in-user';
import { e2eEnvironmentConfigService, e2eUserWithCandidateRoleCookie, e2eUserWithCompanyRoleCookie } from '../../e2e-config';

describe('infrastructure/rest/user/UserController (e2e)', () => {
  let app: INestApplication;
  let mockLogInUser: LogInUser;
  let mockGetCurrentUser: GetCurrentUser;
  let mockGetUserJobOffers: GetUserJobOffers;
  let mockGetUserJobOfferById: GetUserJobOfferById;

  beforeAll(async () => {
    mockLogInUser = {} as LogInUser;
    mockLogInUser.execute = jest.fn();
    const mockLogInUserProxyService: UseCaseProxy<LogInUser> = {
      getInstance: () => mockLogInUser,
    } as UseCaseProxy<LogInUser>;

    mockGetCurrentUser = {} as GetCurrentUser;
    mockGetCurrentUser.execute = jest.fn();
    const mockGetCurrentUserProxyService: UseCaseProxy<GetCurrentUser> = {
      getInstance: () => mockGetCurrentUser,
    } as UseCaseProxy<GetCurrentUser>;

    mockGetUserJobOffers = {} as GetUserJobOffers;
    mockGetUserJobOffers.execute = jest.fn();
    const mockGetUserJobOffersProxyService: UseCaseProxy<GetUserJobOffers> = {
      getInstance: () => mockGetUserJobOffers,
    } as UseCaseProxy<GetUserJobOffers>;

    mockGetUserJobOfferById = {} as GetUserJobOfferById;
    mockGetUserJobOfferById.execute = jest.fn();
    const mockGetUserJobOfferByIdProxyService: UseCaseProxy<GetUserJobOfferById> = {
      getInstance: () => mockGetUserJobOfferById,
    } as UseCaseProxy<GetUserJobOfferById>;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RestModule],
    })
      .overrideProvider(ProxyServicesDynamicModule.LOG_IN_USER_PROXY_SERVICE)
      .useValue(mockLogInUserProxyService)
      .overrideProvider(ProxyServicesDynamicModule.GET_CURRENT_USER_PROXY_SERVICE)
      .useValue(mockGetCurrentUserProxyService)
      .overrideProvider(ProxyServicesDynamicModule.GET_USER_JOB_OFFERS_PROXY_SERVICE)
      .useValue(mockGetUserJobOffersProxyService)
      .overrideProvider(ProxyServicesDynamicModule.GET_USER_JOB_OFFER_BY_ID_PROXY_SERVICE)
      .useValue(mockGetUserJobOfferByIdProxyService)
      .overrideProvider(EnvironmentConfigService)
      .useValue(e2eEnvironmentConfigService)
      .overrideProvider(Auth0Strategy)
      .useValue({})
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('GET /user/login', () => {
    it('should redirect to login form', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/user/login');

      // then
      return testRequest.expect(302).then((response: Response) => {
        expect(response.header.location).toBe('/user/login/form');
      });
    });
  });

  describe('GET /user/profile', () => {
    it('should return current user', () => {
      // given
      (mockGetCurrentUser.execute as jest.Mock).mockReturnValue(Promise.resolve({ id: '123456', firstName: 'Bob' } as UserInterface));

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/user/profile')
        .set('Cookie', e2eUserWithCandidateRoleCookie);

      // then
      return testRequest.expect(200).expect({ id: '123456', firstName: 'Bob' } as UserResponseInterface);
    });

    it('should return forbidden status when not authenticated', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/user/profile');

      // then
      return testRequest.expect(403);
    });
  });

  describe('GET /user/job-offers', () => {
    it('should return found job offers', () => {
      // given
      (mockGetUserJobOffers.execute as jest.Mock).mockReturnValue(
        Promise.resolve([{ title: 'An awesome job' } as JobOfferInterface, { title: 'Another awesome job' } as JobOfferInterface])
      );

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/user/job-offers')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(200).expect([{ title: 'An awesome job' }, { title: 'Another awesome job' }] as JobOfferResponseInterface[]);
    });

    it('should return forbidden status when not authenticated', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/user/job-offers');

      // then
      return testRequest.expect(403);
    });

    it('should return forbidden status when authenticated with a different role than company', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/user/job-offers')
        .set('Cookie', e2eUserWithCandidateRoleCookie);

      // then
      return testRequest.expect(403);
    });
  });

  describe('GET /user/job-offers/:id', () => {
    it('should return found job offer by id', () => {
      // given
      (mockGetUserJobOfferById.execute as jest.Mock).mockReturnValue(Promise.resolve({ id: '12', title: 'A job offer title' } as JobOfferInterface));

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/user/job-offers/12')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(200).expect({ id: '12', title: 'A job offer title' } as JobOfferResponseInterface);
    });

    it('should find job offer using id from params', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/user/job-offers/12')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.then((response: Response) => {
        expect(mockGetUserJobOfferById.execute).toHaveBeenCalledWith('12');
      });
    });

    it('should return cached job offer if request has already been made less than 1 minute ago', () => {
      // given
      (mockGetUserJobOfferById.execute as jest.Mock).mockReturnValue(Promise.resolve({ id: '11', title: 'A job offer title' } as JobOfferInterface));

      // then
      return request(app.getHttpServer())
        .get('/user/job-offers/11')
        .set('Cookie', e2eUserWithCompanyRoleCookie)
        .expect(200)
        .expect(() => {
          return request(app.getHttpServer())
            .get('/user/job-offers/11')
            .set('Cookie', e2eUserWithCompanyRoleCookie)
            .expect(200)
            .expect({ id: '11', title: 'A job offer title' } as JobOfferResponseInterface)
            .expect(() => {
              expect(mockGetUserJobOfferById.execute).toHaveBeenCalledTimes(1);
            });
        });
    });

    it('should return http not found when job offer not found', () => {
      // given
      (mockGetUserJobOfferById.execute as jest.Mock).mockImplementation(() => {
        throw new ItemNotFoundError('JobOffer not found');
      });

      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/user/job-offers/1234')
        .set('Cookie', e2eUserWithCompanyRoleCookie);

      // then
      return testRequest.expect(404);
    });

    it('should return forbidden status when not authenticated', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer()).get('/user/job-offers/12');

      // then
      return testRequest.expect(403);
    });

    it('should return forbidden status when authenticated with a different role than company', () => {
      // when
      const testRequest: request.Test = request(app.getHttpServer())
        .get('/user/job-offers/12')
        .set('Cookie', e2eUserWithCandidateRoleCookie);

      // then
      return testRequest.expect(403);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});

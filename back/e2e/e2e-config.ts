import { EnvironmentConfigService } from '../src/infrastructure/config/environment-config/environment-config.service';

export const e2eEnvironmentConfigService: EnvironmentConfigService = {
  get(key: string): string {
    if (key === 'DATABASE_TYPE') return 'sqlite';
    if (key === 'DATABASE_NAME') return 'e2e.sqlite';
    if (key === 'GBUSTER_SESSION_SECRET') return 'ffffffffffffffffffffffffffffffff';

    return null;
  },
} as EnvironmentConfigService;

// These cookies have been generated using a fake endpoint that set a user with specific role in session and using same GBUSTER_SESSION_SECRET as e2e config value
// When there is a call to this fake endpoint, it will return a Set-Cookie response header with the session as hashed cookie value
// We can after reuse this cookie to send it in Cookie header request in order to be authenticated with a specific role
export const e2eUserWithCandidateRoleCookie: string =
  'express:sess=eyJ1c2VyIjp7InJvbGUiOiJDQU5ESURBVEUifX0=; express:sess.sig=o3vZOPSs9ly7SgOuMtfPC65t0nI;';
export const e2eUserWithCompanyRoleCookie: string =
  'express:sess=eyJ1c2VyIjp7InJvbGUiOiJDT01QQU5ZIn19; express:sess.sig=H5drJ6YS2n4KYqk0PILVLAieb-8;';
export const e2eUserWithAdminRoleCookie: string = 'express:sess=eyJ1c2VyIjp7InJvbGUiOiJBRE1JTiJ9fQ==; express:sess.sig=IpBYwIxYWb2_XvfKUbr66kVwHbE;';

import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { ItemNotFoundError } from '../../src/domain/common-errors/item-not-found.error';
import { CompanyBusinessSegment, CompanyGeniusType, CompanyRevenue, CompanySize, CompanyStatus } from '../../src/domain/company/company-enums';
import { CompanyInterface } from '../../src/domain/company/company.interface';
import { CompanyId } from '../../src/domain/type-aliases';
import { EnvironmentConfigService } from '../../src/infrastructure/config/environment-config/environment-config.service';
import { DatabaseCompanyRepository } from '../../src/infrastructure/repositories/database-company.repository';
import { CompanyEntity } from '../../src/infrastructure/repositories/entities/company.entity';
import { RepositoriesModule } from '../../src/infrastructure/repositories/repositories.module';
import { e2eEnvironmentConfigService } from '../e2e-config';
import { runDatabaseMigrations } from './database-e2e.utils';

describe('infrastructure/repositories/DatabaseCompanyRepository', () => {
  let app: INestApplication;
  let databaseCompanyRepository: DatabaseCompanyRepository;
  let companyEntityRepository: Repository<CompanyEntity>;
  let companyWithoutId: CompanyInterface;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RepositoriesModule],
    })
      .overrideProvider(EnvironmentConfigService)
      .useValue(e2eEnvironmentConfigService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    await runDatabaseMigrations(app.get(EnvironmentConfigService));

    databaseCompanyRepository = app.get(DatabaseCompanyRepository);
    // @ts-ignore
    companyEntityRepository = databaseCompanyRepository.companyEntityRepository;
  });

  beforeEach(async () => {
    await companyEntityRepository.clear();

    companyWithoutId = {
      activityLocations: { quebec: 1 },
      address: 'an address',
      businessSegment: CompanyBusinessSegment.BUSINESS_SEGMENT_PULP_AND_PAPER,
      creationDate: new Date('2019-11-15T17:35:20'),
      geniusTypes: [CompanyGeniusType.GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING],
      logo: {
        base64: 'logo-as-base64',
        mimeType: 'image/type',
      },
      status: CompanyStatus.STATUS_PENDING_VALIDATION,
      name: 'company name',
      presentation: 'company presentation',
      revenue: CompanyRevenue.REVENUE_BETWEEN_5M_AND_20M,
      size: CompanySize.EIGHTY_TO_HUNDRED_NINETEEN,
      userId: '987654321',
      values: [{ meaning: 'value', justification: 'jusitification' }],
      website: 'a website',
    } as CompanyInterface;
  });

  describe('save()', () => {
    it('should persist company in database', async () => {
      // when
      await databaseCompanyRepository.save(companyWithoutId);

      // then
      const count: number = await companyEntityRepository.count();
      expect(count).toBe(1);
    });

    it('should return saved company with an id', async () => {
      // when
      const result: CompanyId = await databaseCompanyRepository.save(companyWithoutId);

      // then
      expect(result).toBeDefined();
    });
  });

  describe('findById()', () => {
    it('should return found company when in database', async () => {
      // given
      const savedCompanyId: CompanyId = await databaseCompanyRepository.save(companyWithoutId);

      // when
      const result: CompanyInterface = await databaseCompanyRepository.findById(savedCompanyId);

      // then
      expect(result.id).toBe(savedCompanyId);
      expect(result).toMatchObject(companyWithoutId);
    });

    it('should throw exception when company not found in database', async () => {
      // given
      const randomId: string = `an-unknown-id-${Math.random()
        .toString(36)
        .slice(2)}`;

      // when
      const result: Promise<CompanyInterface> = databaseCompanyRepository.findById(randomId);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError(`Company not found with id "${randomId}"`));
    });
  });

  describe('findByUserId()', () => {
    it('should return found company when in database', async () => {
      // given
      const savedCompanyId: CompanyId = await databaseCompanyRepository.save(companyWithoutId);

      // when
      const result: CompanyInterface = await databaseCompanyRepository.findByUserId(companyWithoutId.userId);

      // then
      expect(result.id).toBe(savedCompanyId);
      expect(result).toMatchObject(companyWithoutId);
    });

    it('should throw exception when company not found in database', async () => {
      // given
      const randomUserId: string = `an-unknown-user-id-${Math.random()
        .toString(36)
        .slice(2)}`;

      // when
      const result: Promise<CompanyInterface> = databaseCompanyRepository.findByUserId(randomUserId);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError(`Company not found with userId "${randomUserId}"`));
    });
  });

  describe('findAllByIds()', () => {
    it('should return found companies when in database', async () => {
      // given
      const savedCompanyId1: CompanyId = await databaseCompanyRepository.save(companyWithoutId);
      const companyWithoutIdAndDifferentUserId: CompanyInterface = { ...companyWithoutId, userId: 'another-user-id' };
      const savedCompanyId2: CompanyId = await databaseCompanyRepository.save(companyWithoutIdAndDifferentUserId);

      // when
      const result: CompanyInterface[] = await databaseCompanyRepository.findAllByIds([savedCompanyId1, savedCompanyId2]);

      // then
      expect(result).toHaveLength(2);
      expect(result[0]).toMatchObject(companyWithoutId);
      expect(result[1]).toMatchObject(companyWithoutIdAndDifferentUserId);
    });

    it('should return empty array when no company has been found in database', async () => {
      // given
      const randomId: string = `an-unknown-id-${Math.random()
        .toString(36)
        .slice(2)}`;

      // when
      const result: CompanyInterface[] = await databaseCompanyRepository.findAllByIds([randomId]);

      // then
      expect(result).toHaveLength(0);
    });
  });

  describe('findAll()', () => {
    it('should return found companies in database', async () => {
      // given
      const savedCompanyId1: CompanyId = await databaseCompanyRepository.save(companyWithoutId);
      const companyWithoutIdAndDifferentUserId: CompanyInterface = { ...companyWithoutId, userId: 'another-user-id' };
      const savedCompanyId2: CompanyId = await databaseCompanyRepository.save(companyWithoutIdAndDifferentUserId);

      // when
      const result: CompanyInterface[] = await databaseCompanyRepository.findAll();

      // then
      expect(result).toHaveLength(2);
      expect(result[0]).toMatchObject(companyWithoutId);
      expect(result[1]).toMatchObject(companyWithoutIdAndDifferentUserId);
    });

    it('should return empty array when no company has been found in database', async () => {
      // when
      const result: CompanyInterface[] = await databaseCompanyRepository.findAll();

      // then
      expect(result).toHaveLength(0);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});

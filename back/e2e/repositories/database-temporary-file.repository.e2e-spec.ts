import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { ItemNotFoundError } from '../../src/domain/common-errors/item-not-found.error';
import { TemporaryFileInterface } from '../../src/domain/temporary-file/temporary-file.interface';
import { EnvironmentConfigService } from '../../src/infrastructure/config/environment-config/environment-config.service';
import { DatabaseTemporaryFileRepository } from '../../src/infrastructure/repositories/database-temporary-file.repository';
import { TemporaryFileEntity } from '../../src/infrastructure/repositories/entities/temporary-file.entity';
import { RepositoriesModule } from '../../src/infrastructure/repositories/repositories.module';
import { e2eEnvironmentConfigService } from '../e2e-config';
import { runDatabaseMigrations } from './database-e2e.utils';

describe('infrastructure/repositories/DatabaseTemporaryFileRepository', () => {
  let app: INestApplication;
  let databaseTemporaryFileRepository: DatabaseTemporaryFileRepository;
  let temporaryFileEntityRepository: Repository<TemporaryFileEntity>;
  let temporaryFileWithoutId: TemporaryFileInterface;
  let defaultLimit: number;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RepositoriesModule],
    })
      .overrideProvider(EnvironmentConfigService)
      .useValue(e2eEnvironmentConfigService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    await runDatabaseMigrations(app.get(EnvironmentConfigService));

    databaseTemporaryFileRepository = app.get(DatabaseTemporaryFileRepository);
    // @ts-ignore
    temporaryFileEntityRepository = databaseTemporaryFileRepository.temporaryFileEntityRepository;
    // @ts-ignore
    defaultLimit = databaseTemporaryFileRepository.LIMIT;
  });

  beforeEach(async () => {
    await temporaryFileEntityRepository.clear();

    temporaryFileWithoutId = {
      uuid: 'an-uuid',
      mimeType: 'image/whatever',
      base64: 'a-base64',
      filename: 'filename.extension',
      createdAt: new Date(),
    } as TemporaryFileInterface;
  });

  describe('save()', () => {
    it('should persist temporary file in database', async () => {
      // when
      await databaseTemporaryFileRepository.save(temporaryFileWithoutId);

      // then
      const count: number = await temporaryFileEntityRepository.count();
      expect(count).toBe(1);
    });

    it('should return saved temporary file with an id', async () => {
      // when
      const result: TemporaryFileInterface = await databaseTemporaryFileRepository.save(temporaryFileWithoutId);

      // then
      expect(result.id).toBeDefined();
    });
  });

  describe('findByUuid()', () => {
    it('should return found temporary file when in database', async () => {
      // given
      const expected: TemporaryFileInterface = await databaseTemporaryFileRepository.save(temporaryFileWithoutId);

      // when
      const result: TemporaryFileInterface = await databaseTemporaryFileRepository.findByUuid(expected.uuid);

      // then
      expect(result).toEqual(expected);
    });

    it('should throw exception when temporary file not found in database', async () => {
      // given
      const randomUuid: string = `an-unknown-uuid-${Math.random()
        .toString(36)
        .slice(2)}`;

      // when
      const result: Promise<TemporaryFileInterface> = databaseTemporaryFileRepository.findByUuid(randomUuid);

      // then
      await expect(result).rejects.toThrow(new ItemNotFoundError(`TemporaryFile not found with uuid "${randomUuid}"`));
    });
  });

  describe('findAllByCreatedAtBefore()', () => {
    it('should return temporary file having a creation date before given date', async () => {
      // given
      const date: Date = new Date('2019-11-15T17:35:20');
      const oneHourBefore: Date = new Date('2019-11-15T16:35:20');
      const oneHourAfter: Date = new Date('2019-11-15T18:35:20');
      const temporaryFileEntityOneHourBefore: TemporaryFileInterface = await databaseTemporaryFileRepository.save({
        ...temporaryFileWithoutId,
        createdAt: oneHourBefore,
        uuid: 'an-uuid-1',
      });
      await databaseTemporaryFileRepository.save({ ...temporaryFileWithoutId, createdAt: date, uuid: 'an-uuid-2' });
      await databaseTemporaryFileRepository.save({ ...temporaryFileWithoutId, createdAt: oneHourAfter, uuid: 'an-uuid-3' });

      // when
      const result: TemporaryFileInterface[] = await databaseTemporaryFileRepository.findAllByCreatedAtBefore(date);

      // then
      expect(result).toHaveLength(1);
      expect(result[0]).toEqual(temporaryFileEntityOneHourBefore);
    });

    it('should return empty array when no temporary file having a creation date before given date', async () => {
      // given
      const date: Date = new Date('2019-11-15T17:35:20');
      const oneHourAfter: Date = new Date('2019-11-15T18:35:20');
      await databaseTemporaryFileRepository.save({ ...temporaryFileWithoutId, createdAt: date, uuid: 'an-uuid-1' });
      await databaseTemporaryFileRepository.save({ ...temporaryFileWithoutId, createdAt: oneHourAfter, uuid: 'an-uuid-2' });

      // when
      const result: TemporaryFileInterface[] = await databaseTemporaryFileRepository.findAllByCreatedAtBefore(date);

      // then
      expect(result).toHaveLength(0);
    });

    it('should order by createdAt ascending and limit search for performance', async () => {
      // given
      const date: Date = new Date('2019-11-15T17:35:20');
      const oneHourBefore: Date = new Date('2019-11-15T16:35:20');
      await databaseTemporaryFileRepository.save({ ...temporaryFileWithoutId, createdAt: date, uuid: 'an-uuid-1' });
      await databaseTemporaryFileRepository.save({ ...temporaryFileWithoutId, createdAt: oneHourBefore, uuid: 'an-uuid-2' });
      await databaseTemporaryFileRepository.save({ ...temporaryFileWithoutId, createdAt: date, uuid: 'an-uuid-3' });
      await databaseTemporaryFileRepository.save({ ...temporaryFileWithoutId, createdAt: date, uuid: 'an-uuid-4' });
      await databaseTemporaryFileRepository.save({ ...temporaryFileWithoutId, createdAt: oneHourBefore, uuid: 'an-uuid-5' });
      await databaseTemporaryFileRepository.save({ ...temporaryFileWithoutId, createdAt: date, uuid: 'an-uuid-6' });
      // @ts-ignore
      databaseTemporaryFileRepository.LIMIT = 2;

      // when
      const result: TemporaryFileInterface[] = await databaseTemporaryFileRepository.findAllByCreatedAtBefore(date);

      // then
      expect(result).toHaveLength(2);
      expect(result[0].uuid).toBe('an-uuid-2');
      expect(result[1].uuid).toBe('an-uuid-5');
    });

    it('should limit search at 100', async () => {
      // then
      expect(defaultLimit).toBe(100);
    });
  });

  describe('delete()', () => {
    it('should delete all given temporary files', async () => {
      // given
      const temporaryFileEntity1: TemporaryFileInterface = await databaseTemporaryFileRepository.save({
        ...temporaryFileWithoutId,
        uuid: 'an-uuid-1',
      });
      const temporaryFileEntity2: TemporaryFileInterface = await databaseTemporaryFileRepository.save({
        ...temporaryFileWithoutId,
        uuid: 'an-uuid-2',
      });

      // when
      await databaseTemporaryFileRepository.delete([temporaryFileEntity1, temporaryFileEntity2]);

      // then
      const count: number = await temporaryFileEntityRepository.count();
      expect(count).toBe(0);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});

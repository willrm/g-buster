import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { CandidateInterface } from '../../../src/domain/candidate/candidate.interface';
import { EnvironmentConfigService } from '../../../src/infrastructure/config/environment-config/environment-config.service';
import { CandidateDomainToCandidateZohoModelMappingStrategy } from '../../../src/infrastructure/repositories/mapping-strategies/candidate-domain-to-candidate-zoho-model-mapping-strategy';
import { CandidateZohoModelToCandidateDomainMappingStrategy } from '../../../src/infrastructure/repositories/mapping-strategies/candidate-zoho-model-to-candidate-domain-mapping-strategy';
import { RepositoriesModule } from '../../../src/infrastructure/repositories/repositories.module';
import { ZohoJsonFieldInterface } from '../../../src/infrastructure/vendors/zoho/models/json/zoho-json-field.interface';
import { ZohoRequestAdapterService } from '../../../src/infrastructure/vendors/zoho/zoho-request-adapter.service';
import { ZohoResponseAdapterService } from '../../../src/infrastructure/vendors/zoho/zoho-response-adapter.service';
import { ZohoModule } from '../../../src/infrastructure/vendors/zoho/zoho.module';
import { e2eEnvironmentConfigService } from '../../e2e-config';

describe('infrastructure/repositories/mapping-strategies/CandidateDomainToCandidateZohoModelMappingStrategy (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const fixedDate: Date = new Date('2019-03-28T16:35:57');
    // @ts-ignore
    jest.spyOn(global, 'Date').mockImplementation(() => fixedDate);

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RepositoriesModule, ZohoModule],
    })
      .overrideProvider(EnvironmentConfigService)
      .useValue(e2eEnvironmentConfigService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('map()', () => {
    let candidateDomainWithAllPropertiesFilled: CandidateInterface;
    let candidateZohoModel: ZohoJsonFieldInterface[];
    let zohoRequestAdapterService: ZohoRequestAdapterService;
    let zohoResponseAdapterService: ZohoResponseAdapterService;
    let candidateDomainToCandidateZohoModelMappingStrategy: CandidateDomainToCandidateZohoModelMappingStrategy;
    let candidateZohoModelToCandidateDomainMappingStrategy: CandidateZohoModelToCandidateDomainMappingStrategy;

    beforeEach(() => {
      candidateDomainWithAllPropertiesFilled = {
        id: '123456789',
        firstName: 'A first name',
        lastName: 'A last name',
        email: 'test@example.org',
        salesforceId: 'ABC123456789XYZ',
      };
      zohoRequestAdapterService = app.get(ZohoRequestAdapterService);
      zohoResponseAdapterService = app.get(ZohoResponseAdapterService);
      candidateDomainToCandidateZohoModelMappingStrategy = app.get(CandidateDomainToCandidateZohoModelMappingStrategy);
      candidateZohoModelToCandidateDomainMappingStrategy = app.get(CandidateZohoModelToCandidateDomainMappingStrategy);
      candidateZohoModel = zohoRequestAdapterService.map<CandidateInterface>(
        candidateDomainWithAllPropertiesFilled,
        candidateDomainToCandidateZohoModelMappingStrategy
      );
    });

    it('should match Candidate domain => Candidate Zoho Model => Candidate domain', () => {
      // when
      const candidateRemappedFromZohoModel: CandidateInterface = zohoResponseAdapterService.map<CandidateInterface>(
        candidateZohoModel,
        candidateZohoModelToCandidateDomainMappingStrategy
      );

      // then
      expect(candidateRemappedFromZohoModel).toStrictEqual(candidateDomainWithAllPropertiesFilled);
    });

    it('should match Candidate Zoho Model => Candidate domain => Candidate Zoho Model', () => {
      // when
      const candidateZohoModelRemappedFromDomain: ZohoJsonFieldInterface[] = zohoRequestAdapterService.map<CandidateInterface>(
        candidateDomainWithAllPropertiesFilled,
        candidateDomainToCandidateZohoModelMappingStrategy
      );

      // then
      expect(candidateZohoModelRemappedFromDomain).toStrictEqual(candidateZohoModel);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});

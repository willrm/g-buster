import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import {
  AdvantageFinancial,
  AdvantageHealth,
  AdvantageLifeBalance,
  AdvantageProfessional,
  AdvantageSocial,
} from '../../../src/domain/advantages/advantages-enums';
import {
  JobOfferJobStatus,
  JobOfferJobType,
  JobOfferPositionType,
  JobOfferRegion,
  JobOfferRequestedSpeciality,
  JobOfferRequiredExperience,
  JobOfferStatus,
  JobOfferTargetCustomer,
  JobOfferWantedPersonality,
} from '../../../src/domain/job-offer/job-offer-enums';
import { JobOfferInterface } from '../../../src/domain/job-offer/job-offer.interface';
import { TravelingFrequency } from '../../../src/domain/traveling/traveling-enums';
import { EnvironmentConfigService } from '../../../src/infrastructure/config/environment-config/environment-config.service';
import { JobOfferDomainToJobOpeningZohoModelMappingStrategy } from '../../../src/infrastructure/repositories/mapping-strategies/job-offer-domain-to-job-opening-zoho-model-mapping-strategy';
import { JobOpeningZohoModelToJobOfferDomainMappingStrategy } from '../../../src/infrastructure/repositories/mapping-strategies/job-opening-zoho-model-to-job-offer-domain-mapping-strategy';
import { RepositoriesModule } from '../../../src/infrastructure/repositories/repositories.module';
import { ZohoJsonFieldInterface } from '../../../src/infrastructure/vendors/zoho/models/json/zoho-json-field.interface';
import { ZohoRequestAdapterService } from '../../../src/infrastructure/vendors/zoho/zoho-request-adapter.service';
import { ZohoResponseAdapterService } from '../../../src/infrastructure/vendors/zoho/zoho-response-adapter.service';
import { ZohoModule } from '../../../src/infrastructure/vendors/zoho/zoho.module';
import { e2eEnvironmentConfigService } from '../../e2e-config';

describe('infrastructure/repositories/mapping-strategies/JobOfferDomainToJobOpeningZohoModelMappingStrategy (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RepositoriesModule, ZohoModule],
    })
      .overrideProvider(EnvironmentConfigService)
      .useValue(e2eEnvironmentConfigService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('map()', () => {
    let jobOfferDomainWithAllPropertiesFilled: JobOfferInterface;
    let jobOpeningZohoModel: ZohoJsonFieldInterface[];
    let zohoRequestAdapterService: ZohoRequestAdapterService;
    let zohoResponseAdapterService: ZohoResponseAdapterService;
    let jobOfferDomainToJobOpeningZohoModelMappingStrategy: JobOfferDomainToJobOpeningZohoModelMappingStrategy;
    let jobOpeningZohoModelToJobOfferDomainMappingStrategy: JobOpeningZohoModelToJobOfferDomainMappingStrategy;

    beforeEach(() => {
      jobOfferDomainWithAllPropertiesFilled = {
        id: '123456789',
        userId: '987654321',
        companyId: '879135135687',
        status: JobOfferStatus.STATUS_PENDING_VALIDATION,
        jobStatus: JobOfferJobStatus.JOB_STATUS_FULL_TIME,
        jobType: JobOfferJobType.JOB_TYPE_REGULAR,
        title: 'a job title',
        city: 'a city',
        region: JobOfferRegion.REGION_GASPESIE_ILES_DE_LA_MADELEINE,
        targetCustomers: [
          JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS,
          JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP,
          JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERING_GRADUATES,
          JobOfferTargetCustomer.TARGET_CUSTOMER_STUDENTS_AND_GRADUATES,
        ],
        requiredExperiences: [
          JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS,
          JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
          JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
          JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
        ],
        positionType: JobOfferPositionType.POSITION_TYPE_FORESTRY_ENGINEER,
        requestedSpecialities: [
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NONE_IN_PARTICULAR,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OTHER_SPECIALITY,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOTECHNOLOGY,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CHEMISTRY,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CIVIL,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CONSTRUCTION,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_DOCUMENTATION,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ELECTRICAL,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ENVIRONMENT,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_GEOLOGY,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_QUALITY_MANAGEMENT,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INDUSTRIAL_MANUFACTURER,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INFORMATION_TECHNOLOGY,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INSTRUMENTATION,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_SOFTWARE,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MATERIAL_HANDLING_DISTRIBUTION,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MARITIME_NAVAL,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NON_METALLIC_MATERIALS,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MECHANICS,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_METALLURGY_METALS,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MINING,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NUCLEAR_POWER,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PULP_AND_PAPER,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PHYSICAL,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ROBOTICS,
          JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OCCUPATIONAL_HEALTH_AND_SAFETY,
        ],
        description: 'A job description',
        specificities: 'Some specificities',
        traveling: {
          isTraveling: false,
          quebec: TravelingFrequency.TRAVELING_FREQUENT,
          canada: TravelingFrequency.TRAVELING_FREQUENT,
          international: TravelingFrequency.TRAVELING_OCCASIONAL,
        },
        advantages: {
          health: [
            AdvantageHealth.HEALTH_DENTAL_INSURANCE,
            AdvantageHealth.HEALTH_MEDICAL_EXPENSES_INSURANCE,
            AdvantageHealth.HEALTH_SIGHT_INSURANCE,
            AdvantageHealth.HEALTH_LIFE_INSURANCE,
            AdvantageHealth.HEALTH_TRAVEL_INSURANCE,
            AdvantageHealth.HEALTH_SALARY_INSURANCE,
            AdvantageHealth.HEALTH_SHORT_TERM_DISABILITY_INSURANCE,
            AdvantageHealth.HEALTH_LONG_TERM_DISABILITY_INSURANCE,
          ],
          social: [
            AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES,
            AdvantageSocial.SOCIAL_TRAVEL_AND_HOLIDAYS,
            AdvantageSocial.SOCIAL_CAFETERIA,
            AdvantageSocial.SOCIAL_FRESH_FRUIT_AND_VEGETABLES,
            AdvantageSocial.SOCIAL_RELAXATION_ROOM,
            AdvantageSocial.SOCIAL_WELLNESS_PROGRAM,
            AdvantageSocial.SOCIAL_REFERENCING_PROGRAM,
          ],
          lifeBalance: [
            AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK,
            AdvantageLifeBalance.LIFE_BALANCE_PAID_HOLIDAYS,
            AdvantageLifeBalance.LIFE_BALANCE_FLEXIBLE_SCHEDULES,
            AdvantageLifeBalance.LIFE_BALANCE_SICK_LEAVE,
            AdvantageLifeBalance.LIFE_BALANCE_SUMMER_HOURS,
            AdvantageLifeBalance.LIFE_BALANCE_MOBILE_HOLIDAYS,
          ],
          financial: [
            AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT,
            AdvantageFinancial.FINANCIAL_COMPETITIVE_COMPENSATION,
            AdvantageFinancial.FINANCIAL_PENSION_PLAN,
            AdvantageFinancial.FINANCIAL_DEFERRED_PROFIT_SHARING_PLAN_DPSP,
            AdvantageFinancial.FINANCIAL_EAP,
            AdvantageFinancial.FINANCIAL_RECOGNITION_OF_SENIORITY,
            AdvantageFinancial.FINANCIAL_REFUND_OF_DUES_TO_A_PROFESSIONAL_ASSOCIATION_OR_ORDER,
            AdvantageFinancial.FINANCIAL_PERFORMANCE_BONUS,
            AdvantageFinancial.FINANCIAL_PARKING_LOT,
            AdvantageFinancial.FINANCIAL_TRANSPORT_SUBSCRIPTION,
          ],
          professional: [AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM, AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM],
        },
        annualSalaryRange: { minimumSalary: 10000, maximumSalary: 999999 },
        wantedPersonalities: [
          JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS,
          JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS,
          JobOfferWantedPersonality.WANTED_PERSONALITY_AUTONOMOUS,
          JobOfferWantedPersonality.WANTED_PERSONALITY_INNOVATIVE,
          JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS,
          JobOfferWantedPersonality.WANTED_PERSONALITY_DECISION_MAKERS,
          JobOfferWantedPersonality.WANTED_PERSONALITY_STRATEGISTS,
          JobOfferWantedPersonality.WANTED_PERSONALITY_FLEXIBLE,
          JobOfferWantedPersonality.WANTED_PERSONALITY_LEADERS,
          JobOfferWantedPersonality.WANTED_PERSONALITY_NEGOTIATORS,
          JobOfferWantedPersonality.WANTED_PERSONALITY_ORGANIZED,
          JobOfferWantedPersonality.WANTED_PERSONALITY_RIGOROUS,
          JobOfferWantedPersonality.WANTED_PERSONALITY_RESOURCEFUL,
          JobOfferWantedPersonality.WANTED_PERSONALITY_TEAMWORK,
          JobOfferWantedPersonality.WANTED_PERSONALITY_SPEAKERS_AT_THE_CONFERENCE,
          JobOfferWantedPersonality.WANTED_PERSONALITY_PONDERATOR,
          JobOfferWantedPersonality.WANTED_PERSONALITY_DIPLOMATS,
        ],
        endDateOfApplication: new Date('2020-10-15T12:00:00'),
        startDateOfEmployment: { asSoonAsPossible: true, startDate: new Date('2021-01-01T12:00:00') },
        emailAddressForReceiptOfApplications: 'test@example.org',
        internalReferenceNumber: 'ABC123XYZ',
        publishedAt: new Date('2019-03-28T12:00:00'),
      };
      zohoRequestAdapterService = app.get(ZohoRequestAdapterService);
      zohoResponseAdapterService = app.get(ZohoResponseAdapterService);
      jobOfferDomainToJobOpeningZohoModelMappingStrategy = app.get(JobOfferDomainToJobOpeningZohoModelMappingStrategy);
      jobOpeningZohoModelToJobOfferDomainMappingStrategy = app.get(JobOpeningZohoModelToJobOfferDomainMappingStrategy);
      jobOpeningZohoModel = zohoRequestAdapterService.map<JobOfferInterface>(
        jobOfferDomainWithAllPropertiesFilled,
        jobOfferDomainToJobOpeningZohoModelMappingStrategy
      );
    });

    it('should match JobOffer domain => JobOpening Zoho Model => JobOffer domain', () => {
      // when
      const jobOfferDomainRemappedFromZohoModel: JobOfferInterface = zohoResponseAdapterService.map<JobOfferInterface>(
        jobOpeningZohoModel,
        jobOpeningZohoModelToJobOfferDomainMappingStrategy
      );

      // then
      expect(jobOfferDomainRemappedFromZohoModel).toStrictEqual(jobOfferDomainWithAllPropertiesFilled);
    });

    it('should match JobOpening Zoho Model => JobOffer domain => JobOpening Zoho Model', () => {
      // when
      const jobOfferZohoModelRemappedFromDomain: ZohoJsonFieldInterface[] = zohoRequestAdapterService.map<JobOfferInterface>(
        jobOfferDomainWithAllPropertiesFilled,
        jobOfferDomainToJobOpeningZohoModelMappingStrategy
      );

      // then
      expect(jobOfferZohoModelRemappedFromDomain).toStrictEqual(jobOpeningZohoModel);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});

import { MigrationInterface, QueryRunner, Table, TableIndex } from 'typeorm';

export class CreateCompanyTableMigration1578416946566 implements MigrationInterface {
  name: string = 'CreateCompanyTableMigration1578416946566';

  async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'company',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'user_id',
            type: 'varchar',
            length: '255',
            isNullable: false,
            isUnique: true,
          },
          {
            name: 'status',
            type: 'varchar',
            length: '64',
            isNullable: false,
          },
          {
            name: 'activity_location_quebec',
            type: 'smallint',
            isNullable: false,
          },
          {
            name: 'activity_location_canada',
            type: 'smallint',
            isNullable: true,
          },
          {
            name: 'activity_location_international',
            type: 'smallint',
            isNullable: true,
          },
          {
            name: 'activity_location_usa',
            type: 'smallint',
            isNullable: true,
          },
          {
            name: 'address',
            type: 'varchar',
            length: '255',
            isNullable: false,
          },
          {
            name: 'bachelors_of_engineering_number',
            type: 'smallint',
            isNullable: true,
          },
          {
            name: 'business_segment',
            type: 'varchar',
            length: '64',
            isNullable: false,
          },
          {
            name: 'creation_date',
            type: 'varchar',
            length: '24',
            isNullable: false,
          },
          {
            name: 'genius_types',
            type: 'text',
            isNullable: false,
          },
          {
            name: 'logo_base_64',
            type: 'text',
            isNullable: false,
          },
          {
            name: 'logo_mime_type',
            type: 'varchar',
            length: '10',
            isNullable: false,
          },
          {
            name: 'name',
            type: 'varchar',
            length: '40',
            isNullable: false,
          },
          {
            name: 'presentation',
            type: 'varchar',
            length: '500',
            isNullable: false,
          },
          {
            name: 'revenue',
            type: 'varchar',
            length: '64',
            isNullable: false,
          },
          {
            name: 'revenue_for_rd',
            type: 'varchar',
            length: '64',
            isNullable: true,
          },
          {
            name: 'size',
            type: 'varchar',
            length: '64',
            isNullable: false,
          },
          {
            name: 'social_networks_facebook',
            type: 'varchar',
            length: '255',
            isNullable: true,
          },
          {
            name: 'social_networks_linked_in',
            type: 'varchar',
            length: '255',
            isNullable: true,
          },
          {
            name: 'values',
            type: 'text',
            isNullable: false,
          },
          {
            name: 'website',
            type: 'varchar',
            length: '50',
            isNullable: false,
          },
        ],
      }),
      true
    );
    await queryRunner.createIndex(
      'company',
      new TableIndex({
        name: 'idx_company_user_id',
        columnNames: ['user_id'],
        isUnique: true,
      })
    );
  }

  async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropIndex('company', 'idx_company_user_id');
    await queryRunner.dropTable('company');
  }
}

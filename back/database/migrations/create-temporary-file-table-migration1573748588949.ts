import { MigrationInterface, QueryRunner, Table, TableIndex } from 'typeorm';

export class CreateTemporaryFileTableMigration1573748588949 implements MigrationInterface {
  name: string = 'CreateTemporaryFileTableMigration1573748588949';

  async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'temporary_file',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'base_64',
            type: 'text',
            isNullable: false,
          },
          {
            name: 'mime_type',
            type: 'varchar',
            length: '128',
            isNullable: false,
          },
          {
            name: 'filename',
            type: 'varchar',
            length: '255',
            isNullable: false,
          },
          {
            name: 'uuid',
            type: 'varchar',
            length: '36',
            isNullable: false,
            isUnique: true,
          },
          {
            name: 'created_at',
            type: 'varchar',
            length: '24',
            isNullable: false,
          },
        ],
      }),
      true
    );
    await queryRunner.createIndex(
      'temporary_file',
      new TableIndex({
        name: 'temporary_file_uuid',
        columnNames: ['uuid'],
        isUnique: true,
      })
    );
  }

  async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropIndex('temporary_file', 'temporary_file_uuid');
    await queryRunner.dropTable('temporary_file');
  }
}

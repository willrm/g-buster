export enum Locale {
  CANADIAN_FRENCH = 'fr-ca',
  CANADIAN_ENGLISH = 'en-ca',
}

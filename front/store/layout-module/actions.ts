import { ActionContext } from 'vuex';
import ApiService from '~/services/api.service';
import { LayoutResponseInterface } from '../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';
import { LayoutModuleStateInterface } from './index';

export interface SetLayoutActionPayload {
  apiService: ApiService;
  params: {
    [key: string]: string;
  };
}

export default {
  async setLayout(actionContext: ActionContext<LayoutModuleStateInterface, object>, setLayoutActionPayload: SetLayoutActionPayload): Promise<void> {
    const {
      apiService,
      params: { lang },
    }: SetLayoutActionPayload = setLayoutActionPayload;

    const layout: LayoutResponseInterface = await apiService.getLayout(lang);
    actionContext.commit('SET_LAYOUT', layout);
  },
};

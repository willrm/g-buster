import { LayoutResponseInterface } from '../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';
import { LayoutModuleStateInterface } from './index';

export default {
  SET_LAYOUT(currentState: LayoutModuleStateInterface, layout: LayoutResponseInterface): void {
    currentState.layout = layout;
  },
};

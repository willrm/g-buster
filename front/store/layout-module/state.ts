import {
  FooterResponseInterface,
  HeaderResponseInterface,
  LayoutResponseInterface,
} from '../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';
import { LayoutModuleStateInterface } from './index';

export default () =>
  ({
    layout: {
      header: {} as HeaderResponseInterface,
      footer: {} as FooterResponseInterface,
    } as LayoutResponseInterface,
  } as LayoutModuleStateInterface);

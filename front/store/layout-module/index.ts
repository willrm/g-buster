import { LayoutResponseInterface } from '../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import state from './state';

export interface LayoutModuleStateInterface {
  layout: LayoutResponseInterface;
}

export default { state, getters, mutations, actions };

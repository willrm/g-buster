import { LayoutModuleStateInterface } from '~/store/layout-module/index';
import { FooterResponseInterface, HeaderResponseInterface } from '../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';

export default {
  getHeader(state: LayoutModuleStateInterface): HeaderResponseInterface {
    return state.layout.header;
  },
  getFooter(state: LayoutModuleStateInterface): FooterResponseInterface {
    return state.layout.footer;
  },
};

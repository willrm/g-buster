import { LayoutModuleStateInterface } from '~/store/layout-module';
import {
  FooterResponseInterface,
  HeaderResponseInterface,
} from '../../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';
import getters from '../getters';

describe('store/layout-module/getters', () => {
  let state: LayoutModuleStateInterface;
  beforeEach(() => {
    state = {
      layout: {
        header: { title: 'A header' } as HeaderResponseInterface,
        footer: { copyright: 'A copyright' },
      },
    };
  });
  describe('getHeader', () => {
    it('should return header from state', () => {
      // when
      const result: HeaderResponseInterface = getters.getHeader(state);

      // then
      expect(result).toStrictEqual({ title: 'A header' });
    });
  });

  describe('getCompanyFooter', () => {
    it('should return footer from state', () => {
      // when
      const result: FooterResponseInterface = getters.getFooter(state);

      // then
      expect(result).toStrictEqual({ copyright: 'A copyright' });
    });
  });
});

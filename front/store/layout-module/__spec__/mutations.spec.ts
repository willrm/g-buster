import {
  FooterResponseInterface,
  LayoutResponseInterface,
} from '../../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';
import { LayoutModuleStateInterface } from '../index';
import mutations from '../mutations';

describe('store/layout-module/mutations', () => {
  describe('SET_LAYOUT()', () => {
    it('should set layout property on state', () => {
      // given
      const state: LayoutModuleStateInterface = { layout: {} as LayoutResponseInterface };
      const layout: LayoutResponseInterface = {
        footer: { copyright: 'A copyright' } as FooterResponseInterface,
      } as LayoutResponseInterface;

      // when
      mutations.SET_LAYOUT(state, layout);

      // then
      expect(state).toHaveProperty('layout', layout);
    });
  });
});

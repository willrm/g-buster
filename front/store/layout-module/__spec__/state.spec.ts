import { LayoutModuleStateInterface } from '~/store/layout-module';
import state from '~/store/layout-module/state';
import {
  FooterResponseInterface,
  HeaderResponseInterface,
  LayoutResponseInterface,
} from '../../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';

describe('store/layout-module/state()', () => {
  it('should return default state', () => {
    // given
    const expected: LayoutModuleStateInterface = {
      layout: {
        header: {} as HeaderResponseInterface,
        footer: {} as FooterResponseInterface,
      } as LayoutResponseInterface,
    } as LayoutModuleStateInterface;

    // when
    const result: LayoutModuleStateInterface = state();

    // then
    expect(result).toStrictEqual(expected);
  });
});

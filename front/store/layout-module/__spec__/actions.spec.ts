import { ActionContext } from 'vuex';

import ApiService from '~/services/api.service';
import {
  FooterResponseInterface,
  HeaderResponseInterface,
  LayoutResponseInterface,
} from '../../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';
import actions, { SetLayoutActionPayload } from '../actions';

import { LayoutModuleStateInterface } from '../index';

describe('store/layout-module/actions', () => {
  describe('setLayout()', () => {
    let mockApiService: ApiService;
    let mockActionContext: ActionContext<LayoutModuleStateInterface, object>;
    beforeEach(() => {
      mockApiService = {} as ApiService;
      mockApiService.getLayout = jest.fn();
      mockActionContext = {} as ActionContext<LayoutModuleStateInterface, object>;

      mockActionContext.commit = jest.fn();
    });
    it('should call apiService with lang from payload params', () => {
      // given
      (mockApiService.getLayout as jest.Mock).mockReturnValue(
        Promise.resolve({
          header: {} as HeaderResponseInterface,
          footer: {} as FooterResponseInterface,
        } as LayoutResponseInterface)
      );
      const actionPayload: SetLayoutActionPayload = {
        apiService: mockApiService,
        params: {
          lang: 'a-lang',
        },
      } as SetLayoutActionPayload;

      // when
      actions.setLayout(mockActionContext, actionPayload);

      // then
      expect(mockApiService.getLayout).toHaveBeenCalledWith('a-lang');
    });

    it('should call commit from context with mutation and layout', async () => {
      // given
      const expected: LayoutResponseInterface = {
        footer: { copyright: 'A copyright' } as FooterResponseInterface,
      } as LayoutResponseInterface;
      (mockApiService.getLayout as jest.Mock).mockReturnValue(Promise.resolve(expected));

      const actionPayload: SetLayoutActionPayload = {
        apiService: mockApiService,
        params: {},
      } as SetLayoutActionPayload;

      // when
      await actions.setLayout(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_LAYOUT', expected);
    });
  });
});

import { JobOffersModuleStateInterface } from '~/store/job-offers-module/index';
import { GetJobOffersRequestInterface } from '../../../back/src/infrastructure/rest/career/models/get-job-offers-request.interface';
import { JobOfferWithCompanyResponseInterface } from '../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';

export default {
  getJobOffers(state: JobOffersModuleStateInterface): JobOfferWithCompanyResponseInterface[] {
    return state.jobOffers;
  },
  getJobOffersFilter(state: JobOffersModuleStateInterface): GetJobOffersRequestInterface {
    return state.jobOffersFilter;
  },
  getJobOffer(state: JobOffersModuleStateInterface): JobOfferWithCompanyResponseInterface {
    return state.currentJobOffer;
  },
};

import Vue from 'vue';
import { JobOffersModuleStateInterface } from '~/store/job-offers-module/index';
import { GetJobOffersRequestInterface } from '../../../back/src/infrastructure/rest/career/models/get-job-offers-request.interface';
import { JobOfferWithCompanyResponseInterface } from '../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import state from './state';

type GetJobOffersRequestFieldName = keyof GetJobOffersRequestInterface;
export interface SetJobOffersFilterMutationPayload {
  fieldName: GetJobOffersRequestFieldName;
  fieldValue: GetJobOffersRequestInterface[GetJobOffersRequestFieldName];
}

export default {
  SET_JOB_OFFERS(currentState: JobOffersModuleStateInterface, jobOffers: JobOfferWithCompanyResponseInterface[]): void {
    Vue.set(currentState, 'jobOffers', jobOffers);
  },

  SET_JOB_OFFERS_FILTER(currentState: JobOffersModuleStateInterface, field: SetJobOffersFilterMutationPayload): void {
    Vue.set(currentState.jobOffersFilter, field.fieldName, field.fieldValue);
  },

  SET_JOB_OFFER(currentState: JobOffersModuleStateInterface, jobOffer: JobOfferWithCompanyResponseInterface): void {
    Vue.set(currentState, 'currentJobOffer', jobOffer);
  },

  RESET(currentState: JobOffersModuleStateInterface): void {
    Object.assign(currentState, state());
  },
};

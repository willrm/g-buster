import { JobOffersModuleStateInterface } from '~/store/job-offers-module/index';
import { GetJobOffersRequestInterface } from '../../../back/src/infrastructure/rest/career/models/get-job-offers-request.interface';
import { JobOfferEnums } from '../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { JobOfferWithCompanyResponseInterface } from '../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';

export default () =>
  ({
    jobOffers: [] as JobOfferWithCompanyResponseInterface[],
    jobOffersFilter: {
      requestedSpecialities: [] as JobOfferEnums.JobOfferRequestedSpeciality[],
      jobTypes: [] as JobOfferEnums.JobOfferJobType[],
      requiredExperiences: [] as JobOfferEnums.JobOfferRequiredExperience[],
      regions: [] as JobOfferEnums.JobOfferRegion[],
    } as GetJobOffersRequestInterface,
    currentJobOffer: {} as JobOfferWithCompanyResponseInterface,
  } as JobOffersModuleStateInterface);

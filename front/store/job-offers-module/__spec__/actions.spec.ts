import { ActionContext } from 'vuex';
import ApiService from '~/services/api.service';
import { JobOffersModuleStateInterface } from '~/store/job-offers-module';
import actions, { SetCurrentJobOfferActionPayload, SetJobOffersActionPayload } from '~/store/job-offers-module/actions';
import { GetJobOffersRequestInterface } from '../../../../back/src/infrastructure/rest/career/models/get-job-offers-request.interface';
import { JobOfferEnums } from '../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { JobOfferWithCompanyResponseInterface } from '../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';

describe('store/job-offers-module/actions', () => {
  let mockActionContext: ActionContext<JobOffersModuleStateInterface, object>;
  let $apiService: ApiService;
  let getJobOffersMock: jest.Mock;
  let getJobOfferMock: jest.Mock;

  beforeEach(() => {
    mockActionContext = {
      state: {
        jobOffers: [] as JobOfferWithCompanyResponseInterface[],
      },
    } as ActionContext<JobOffersModuleStateInterface, object>;
    mockActionContext.commit = jest.fn();

    getJobOffersMock = jest.fn();
    getJobOfferMock = jest.fn();

    $apiService = ({
      getJobOffers: getJobOffersMock,
      getJobOffer: getJobOfferMock,
    } as unknown) as ApiService;
  });

  describe('setJobOffers()', () => {
    it('should call api service to fetch job offers with filter from state', async () => {
      // given
      const filter: GetJobOffersRequestInterface = {
        jobTypes: [JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP],
      };
      mockActionContext.state.jobOffersFilter = filter;
      const actionPayload: SetJobOffersActionPayload = { apiService: $apiService };

      // when
      await actions.setJobOffers(mockActionContext, actionPayload);

      // then
      expect(getJobOffersMock).toHaveBeenCalledWith(filter);
    });

    it('should make a mutation in store', async () => {
      // given
      const actionPayload: SetJobOffersActionPayload = { apiService: $apiService };
      const expected: JobOfferWithCompanyResponseInterface[] = [{ title: 'A job offer' } as JobOfferWithCompanyResponseInterface];
      getJobOffersMock.mockReturnValue(expected);

      // when
      await actions.setJobOffers(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_JOB_OFFERS', expected);
    });
  });

  describe('setCurrentJobOffer()', () => {
    it('should call api service to fetch job offer by id if there is no job offers in state', async () => {
      // given
      const actionPayload: SetCurrentJobOfferActionPayload = { apiService: $apiService, id: '42' };

      // when
      await actions.setCurrentJobOffer(mockActionContext, actionPayload);

      // then
      expect(getJobOfferMock).toHaveBeenCalledWith('42');
    });

    it('should call api service to fetch job offer by id if there is no job offer with this id in state', async () => {
      // given
      mockActionContext.state.jobOffers = [
        { id: '41', title: 'A job offer title' } as JobOfferWithCompanyResponseInterface,
        { id: '43', title: 'Ab other job offer title' } as JobOfferWithCompanyResponseInterface,
      ];
      const actionPayload: SetCurrentJobOfferActionPayload = { apiService: $apiService, id: '42' };

      // when
      await actions.setCurrentJobOffer(mockActionContext, actionPayload);

      // then
      expect(getJobOfferMock).toHaveBeenCalledWith('42');
    });

    it('should not call api service to fetch job offer by id if there is a job offer with this id in state', async () => {
      // given
      mockActionContext.state.jobOffers = [
        { id: '42', title: 'A job offer title' } as JobOfferWithCompanyResponseInterface,
        { id: '43', title: 'Ab other job offer title' } as JobOfferWithCompanyResponseInterface,
      ];
      const actionPayload: SetCurrentJobOfferActionPayload = { apiService: $apiService, id: '42' };

      // when
      await actions.setCurrentJobOffer(mockActionContext, actionPayload);

      // then
      expect(getJobOfferMock).not.toHaveBeenCalled();
    });

    it('should make a mutation in store', async () => {
      // given
      const actionPayload: SetCurrentJobOfferActionPayload = { apiService: $apiService, id: '42' };
      const expected: JobOfferWithCompanyResponseInterface = { title: 'A job offer' } as JobOfferWithCompanyResponseInterface;
      getJobOfferMock.mockReturnValue(expected);

      // when
      await actions.setCurrentJobOffer(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_JOB_OFFER', expected);
    });
  });

  describe('reset()', () => {
    it('should make a mutation to clean store', async () => {
      // when
      await actions.reset(mockActionContext);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('RESET');
    });
  });
});

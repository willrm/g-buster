import { JobOffersModuleStateInterface } from '~/store/job-offers-module';
import state from '~/store/job-offers-module/state';
import { GetJobOffersRequestInterface } from '../../../../back/src/infrastructure/rest/career/models/get-job-offers-request.interface';
import { JobOfferEnums } from '../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { JobOfferWithCompanyResponseInterface } from '../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';

describe('store/job-offers-module/state()', () => {
  it('should return default state', () => {
    // given
    const expected: JobOffersModuleStateInterface = {
      jobOffers: [] as JobOfferWithCompanyResponseInterface[],
      jobOffersFilter: {
        requestedSpecialities: [] as JobOfferEnums.JobOfferRequestedSpeciality[],
        jobTypes: [] as JobOfferEnums.JobOfferJobType[],
        requiredExperiences: [] as JobOfferEnums.JobOfferRequiredExperience[],
        regions: [] as JobOfferEnums.JobOfferRegion[],
      } as GetJobOffersRequestInterface,
      currentJobOffer: {} as JobOfferWithCompanyResponseInterface,
    } as JobOffersModuleStateInterface;

    // when
    const result: JobOffersModuleStateInterface = state();

    // then
    expect(result).toStrictEqual(expected);
  });
});

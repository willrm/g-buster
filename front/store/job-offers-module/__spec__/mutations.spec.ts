import { JobOffersModuleStateInterface } from '~/store/job-offers-module';
import mutations, { SetJobOffersFilterMutationPayload } from '~/store/job-offers-module/mutations';
import { GetJobOffersRequestInterface } from '../../../../back/src/infrastructure/rest/career/models/get-job-offers-request.interface';
import { JobOfferEnums } from '../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { JobOfferWithCompanyResponseInterface } from '../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';

describe('store/job-offers-module/mutations', () => {
  let state: JobOffersModuleStateInterface;
  beforeEach(() => {
    state = {} as JobOffersModuleStateInterface;
  });

  describe('SET_JOB_OFFERS()', () => {
    it('should set job offers', () => {
      // given
      state.jobOffers = [] as JobOfferWithCompanyResponseInterface[];
      const expected: JobOfferWithCompanyResponseInterface[] = [{ title: 'A job offer' } as JobOfferWithCompanyResponseInterface];

      // when
      mutations.SET_JOB_OFFERS(state, expected);

      // then
      expect(state.jobOffers).toStrictEqual(expected);
    });
  });

  describe('SET_JOB_OFFERS_FILTER()', () => {
    it('should set job offers requestedSpecialities filter when fieldName is requestedSpecialities', () => {
      // given
      state.jobOffersFilter = {} as GetJobOffersRequestInterface;
      const expected: JobOfferEnums.JobOfferRequestedSpeciality[] = [
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION,
      ];
      const field: SetJobOffersFilterMutationPayload = { fieldName: 'requestedSpecialities', fieldValue: expected };

      // when
      mutations.SET_JOB_OFFERS_FILTER(state, field);

      // then
      expect(state.jobOffersFilter.requestedSpecialities).toStrictEqual(expected);
    });

    it('should set job offers jobTypes filter when fieldName is jobTypes', () => {
      // given
      state.jobOffersFilter = {} as GetJobOffersRequestInterface;
      const expected: JobOfferEnums.JobOfferJobType[] = [
        JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP,
        JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR,
      ];
      const field: SetJobOffersFilterMutationPayload = { fieldName: 'jobTypes', fieldValue: expected };

      // when
      mutations.SET_JOB_OFFERS_FILTER(state, field);

      // then
      expect(state.jobOffersFilter.jobTypes).toStrictEqual(expected);
    });

    it('should set job offers requiredExperiences filter when fieldName is requiredExperiences', () => {
      // given
      state.jobOffersFilter = {} as GetJobOffersRequestInterface;
      const expected: JobOfferEnums.JobOfferRequiredExperience[] = [
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
      ];
      const field: SetJobOffersFilterMutationPayload = { fieldName: 'requiredExperiences', fieldValue: expected };

      // when
      mutations.SET_JOB_OFFERS_FILTER(state, field);

      // then
      expect(state.jobOffersFilter.requiredExperiences).toStrictEqual(expected);
    });

    it('should set job offers regions filter when fieldName is regions', () => {
      // given
      state.jobOffersFilter = {} as GetJobOffersRequestInterface;
      const expected: JobOfferEnums.JobOfferRegion[] = [
        JobOfferEnums.JobOfferRegion.REGION_CENTRE_DU_QUEBEC,
        JobOfferEnums.JobOfferRegion.REGION_OUTAOUAIS,
      ];
      const field: SetJobOffersFilterMutationPayload = { fieldName: 'regions', fieldValue: expected };

      // when
      mutations.SET_JOB_OFFERS_FILTER(state, field);

      // then
      expect(state.jobOffersFilter.regions).toStrictEqual(expected);
    });
  });

  describe('SET_JOB_OFFER()', () => {
    it('should set job offer', () => {
      // given
      state.currentJobOffer = {} as JobOfferWithCompanyResponseInterface;
      const expected: JobOfferWithCompanyResponseInterface = { title: 'A job offer' } as JobOfferWithCompanyResponseInterface;

      // when
      mutations.SET_JOB_OFFER(state, expected);

      // then
      expect(state.currentJobOffer).toStrictEqual(expected);
    });
  });

  describe('RESET', () => {
    it('should reset store to default state', () => {
      // given
      state.jobOffers = [{ title: 'A job title' } as JobOfferWithCompanyResponseInterface];
      state.jobOffersFilter = { jobTypes: [JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP] } as GetJobOffersRequestInterface;
      state.currentJobOffer = { title: 'An other job title' } as JobOfferWithCompanyResponseInterface;

      // when
      mutations.RESET(state);

      // then
      expect(state).toStrictEqual({
        jobOffers: [],
        jobOffersFilter: {
          requestedSpecialities: [],
          jobTypes: [],
          requiredExperiences: [],
          regions: [],
        },
        currentJobOffer: {},
      });
    });
  });
});

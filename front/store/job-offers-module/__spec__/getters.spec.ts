import { JobOffersModuleStateInterface } from '~/store/job-offers-module';
import { GetJobOffersRequestInterface } from '../../../../back/src/infrastructure/rest/career/models/get-job-offers-request.interface';
import { JobOfferEnums } from '../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { JobOfferWithCompanyResponseInterface } from '../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import getters from '../getters';

describe('store/job-offers-module/getters', () => {
  let state: JobOffersModuleStateInterface;
  beforeEach(() => {
    state = {
      jobOffers: [{ title: 'A job title' }, { title: 'An other job Title' }] as JobOfferWithCompanyResponseInterface[],
      jobOffersFilter: { jobTypes: [JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP] },
      currentJobOffer: {} as JobOfferWithCompanyResponseInterface,
    };
  });

  describe('getJobOffers()', () => {
    it('should return jobOffers from state', () => {
      // when
      const result: JobOfferWithCompanyResponseInterface[] = getters.getJobOffers(state);

      // then
      expect(result).toStrictEqual([{ title: 'A job title' }, { title: 'An other job Title' }]);
    });
  });

  describe('getJobOffersFilter()', () => {
    it('should return jobOffersFilter from state', () => {
      // when
      const result: GetJobOffersRequestInterface = getters.getJobOffersFilter(state);

      // then
      expect(result).toStrictEqual({ jobTypes: [JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP] });
    });
  });

  describe('getJobOffer()', () => {
    it('should return currentJobOffer from state', () => {
      // given
      state.currentJobOffer = { title: 'A job offer' } as JobOfferWithCompanyResponseInterface;

      // when
      const result: JobOfferWithCompanyResponseInterface = getters.getJobOffer(state);

      // then
      expect(result).toStrictEqual({ title: 'A job offer' });
    });
  });
});

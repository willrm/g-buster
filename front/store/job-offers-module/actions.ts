import { ActionContext } from 'vuex';
import ApiService from '~/services/api.service';
import { JobOffersModuleStateInterface } from '~/store/job-offers-module/index';
import { SetJobOffersFilterMutationPayload } from '~/store/job-offers-module/mutations';
import { JobOfferWithCompanyResponseInterface } from '../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import { CommonTypeAliases } from '../../../back/src/infrastructure/rest/common-models/common-type-aliases';

export interface SetJobOffersActionPayload {
  apiService: ApiService;
}

export interface SetCurrentJobOfferActionPayload {
  apiService: ApiService;
  id: CommonTypeAliases.JobOfferId;
}
export type SetJobOffersFilterActionPayload = SetJobOffersFilterMutationPayload;

export default {
  async setJobOffers(actionContext: ActionContext<JobOffersModuleStateInterface, object>, actionPayload: SetJobOffersActionPayload): Promise<void> {
    const jobOffers: JobOfferWithCompanyResponseInterface[] = await actionPayload.apiService.getJobOffers(actionContext.state.jobOffersFilter);

    return Promise.resolve(actionContext.commit('SET_JOB_OFFERS', jobOffers));
  },

  async setJobOffersFilter(
    actionContext: ActionContext<JobOffersModuleStateInterface, object>,
    actionPayload: SetJobOffersFilterActionPayload
  ): Promise<void> {
    return Promise.resolve(actionContext.commit('SET_JOB_OFFERS_FILTER', actionPayload));
  },

  async setCurrentJobOffer(
    actionContext: ActionContext<JobOffersModuleStateInterface, object>,
    actionPayload: SetCurrentJobOfferActionPayload
  ): Promise<void> {
    const jobOfferIndex: number = actionContext.state.jobOffers.findIndex(
      (jobOffer: JobOfferWithCompanyResponseInterface) => jobOffer.id === actionPayload.id
    );

    const currentJobOffer: JobOfferWithCompanyResponseInterface =
      jobOfferIndex >= 0 ? actionContext.state.jobOffers[jobOfferIndex] : await actionPayload.apiService.getJobOffer(actionPayload.id);

    return Promise.resolve(actionContext.commit('SET_JOB_OFFER', currentJobOffer));
  },

  reset(actionContext: ActionContext<JobOffersModuleStateInterface, object>): Promise<void> {
    return Promise.resolve(actionContext.commit('RESET'));
  },
};

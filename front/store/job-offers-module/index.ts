import { GetJobOffersRequestInterface } from '../../../back/src/infrastructure/rest/career/models/get-job-offers-request.interface';
import { JobOfferWithCompanyResponseInterface } from '../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import state from './state';

export interface JobOffersModuleStateInterface {
  jobOffers: JobOfferWithCompanyResponseInterface[];
  jobOffersFilter: GetJobOffersRequestInterface;
  currentJobOffer: JobOfferWithCompanyResponseInterface;
}

export default { state };

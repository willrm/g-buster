import { CompanyCreateModuleStateInterface } from '~/store/company-create-module/index';
import { CompanyEnums } from '../../../back/src/infrastructure/rest/career/models/company-constraints-and-enums';
import {
  PostActivityLocationsRequestInterface,
  PostCompanyRequestInterface,
  PostSocialNetworksRequestInterface,
  PostValueRequestInterface,
} from '../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { TemporaryFileResponseInterface } from '../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';

export default () =>
  ({
    company: {
      activityLocations: {} as PostActivityLocationsRequestInterface,
      geniusTypes: [] as CompanyEnums.CompanyGeniusType[],
      socialNetworks: {} as PostSocialNetworksRequestInterface,
      values: [{} as PostValueRequestInterface],
    } as PostCompanyRequestInterface,
    companyLogo: {} as TemporaryFileResponseInterface,
  } as CompanyCreateModuleStateInterface);

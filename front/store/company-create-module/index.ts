import { PostCompanyRequestInterface } from '../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { TemporaryFileResponseInterface } from '../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import state from './state';

export interface CompanyCreateModuleStateInterface {
  company: PostCompanyRequestInterface;
  companyLogo: TemporaryFileResponseInterface;
}

export default { state, getters, mutations, actions };

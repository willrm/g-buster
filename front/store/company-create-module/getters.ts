import { CompanyCreateModuleStateInterface } from '~/store/company-create-module/index';
import { PostCompanyRequestInterface } from '../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { TemporaryFileResponseInterface } from '../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';

export default {
  getCompany(state: CompanyCreateModuleStateInterface): PostCompanyRequestInterface {
    return state.company;
  },

  getCompanyLogo(state: CompanyCreateModuleStateInterface): TemporaryFileResponseInterface {
    return state.companyLogo;
  },
};

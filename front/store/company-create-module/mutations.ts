import Vue from 'vue';
import { CompanyCreateModuleStateInterface } from '~/store/company-create-module/index';
import { PostCompanyRequestInterface } from '../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { TemporaryFileResponseInterface } from '../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';
import state from './state';

type PostCompanyRequestFieldName = keyof PostCompanyRequestInterface;

export interface SetCompanyFieldMutationPayload {
  fieldName: PostCompanyRequestFieldName;
  fieldValue: PostCompanyRequestInterface[PostCompanyRequestFieldName];
}

export default {
  SET_COMPANY_FIELD(currentState: CompanyCreateModuleStateInterface, field: SetCompanyFieldMutationPayload): void {
    // RISK: This method allows to path any value regardless of type.
    // It's an accepted risk as the backend should throw an error if value is not permitted.
    // Example: company.businessSegment that is an Enum can take 'FAKE_BUSINESS_SEGMENT' as value
    if (field.fieldValue instanceof Array) {
      Vue.set(currentState.company, field.fieldName, [...field.fieldValue]);
    } else {
      Vue.set(currentState.company, field.fieldName, field.fieldValue);
    }
  },

  SET_COMPANY_LOGO(currentState: CompanyCreateModuleStateInterface, companyLogo: TemporaryFileResponseInterface): void {
    Vue.set(currentState, 'companyLogo', companyLogo);
  },

  RESET(currentState: CompanyCreateModuleStateInterface): void {
    Object.assign(currentState, state());
  },
};

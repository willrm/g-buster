import { ActionContext } from 'vuex';
import ApiService from '~/services/api.service';
import { CompanyCreateModuleStateInterface } from '~/store/company-create-module';
import actions, { SetCompanyActionPayload, ValidateAndSubmitCompanyActionPayload } from '~/store/company-create-module/actions';
import {
  PostCompanyRequestInterface,
  PostSocialNetworksRequestInterface,
  PostValueRequestInterface,
} from '../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { TemporaryFileResponseInterface } from '../../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';

describe('store/company-create-module/actions', () => {
  let mockActionContext: ActionContext<CompanyCreateModuleStateInterface, object>;
  let $apiService: ApiService;

  beforeEach(() => {
    mockActionContext = {} as ActionContext<CompanyCreateModuleStateInterface, object>;
    mockActionContext.commit = jest.fn();
    mockActionContext.dispatch = jest.fn();

    $apiService = {} as ApiService;
    $apiService.createNewCompany = jest.fn();
  });

  describe('setCompany()', () => {
    it('should make a mutation in store', () => {
      // given
      const actionPayload: SetCompanyActionPayload = { fieldName: 'name', fieldValue: 'A field Value' };

      // when
      actions.setCompany(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_COMPANY_FIELD', actionPayload);
    });
  });

  describe('setCompanyLogo()', () => {
    it('should make a mutation in store', () => {
      // given
      const actionPayload: TemporaryFileResponseInterface = { uuid: 'an-uuid' } as TemporaryFileResponseInterface;

      // when
      actions.setCompanyLogo(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_COMPANY_LOGO', actionPayload);
    });
  });

  describe('validateCompany()', () => {
    it('should call clean values action', () => {
      // given
      mockActionContext.state = {
        company: {},
      } as CompanyCreateModuleStateInterface;
      const actionPayload: ValidateAndSubmitCompanyActionPayload = { apiService: $apiService };

      // when
      actions.validateCompany(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.dispatch).toHaveBeenCalledWith('cleanCompanyValues');
    });

    it('should call clean social networks action if there is a social networks property', async () => {
      // given
      mockActionContext.state = {
        company: {
          socialNetworks: {},
        },
      } as CompanyCreateModuleStateInterface;
      const actionPayload: ValidateAndSubmitCompanyActionPayload = { apiService: $apiService };

      // when
      await actions.validateCompany(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.dispatch).toHaveBeenLastCalledWith('cleanSocialNetworks', {});
    });

    it('should not call clean social networks action if there is a social networks property', () => {
      // given
      mockActionContext.state = {
        company: {},
      } as CompanyCreateModuleStateInterface;
      const actionPayload: ValidateAndSubmitCompanyActionPayload = { apiService: $apiService };

      // when
      actions.validateCompany(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.dispatch).not.toHaveBeenLastCalledWith('cleanSocialNetworks');
    });

    it('should call api to validate company with dryRun flag', async () => {
      // given
      const expected: PostCompanyRequestInterface = { name: 'A company name' } as PostCompanyRequestInterface;
      mockActionContext.state = {
        company: expected,
      } as CompanyCreateModuleStateInterface;
      const actionPayload: ValidateAndSubmitCompanyActionPayload = { apiService: $apiService };

      // when
      await actions.validateCompany(mockActionContext, actionPayload);

      // then
      expect($apiService.createNewCompany).toHaveBeenCalledWith(expected, true);
    });
  });

  describe('submitCompany()', () => {
    it('should call api to create company when submitting form', async () => {
      // given
      const expected: PostCompanyRequestInterface = { name: 'A company name' } as PostCompanyRequestInterface;
      mockActionContext.state = {
        company: expected,
      } as CompanyCreateModuleStateInterface;
      const actionPayload: ValidateAndSubmitCompanyActionPayload = { apiService: $apiService };

      // when
      await actions.submitCompany(mockActionContext, actionPayload);

      // then
      expect($apiService.createNewCompany).toHaveBeenCalledWith(expected);
    });
  });

  describe('cleanCompanyValues()', () => {
    it('should clean empty value objects', () => {
      // given
      const expected: PostValueRequestInterface[] = [{ meaning: 'A meaning', justification: 'A justification' }];
      mockActionContext.state = {
        company: {
          values: [
            { meaning: 'A meaning', justification: 'A justification' },
            { meaning: '', justification: 'A justification' },
            { meaning: 'A meaning', justification: '' },
          ],
        },
      } as CompanyCreateModuleStateInterface;

      // when
      actions.cleanCompanyValues(mockActionContext);

      // then
      const actionPayload: SetCompanyActionPayload = { fieldName: 'values', fieldValue: expected };
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_COMPANY_FIELD', actionPayload);
    });
  });

  describe('cleanSocialNetworks()', () => {
    it('should add facebook property if not empty', () => {
      // given
      const expected: PostSocialNetworksRequestInterface = {
        facebook: 'http://a.facebook.link',
      };
      mockActionContext.state = {
        company: {},
      } as CompanyCreateModuleStateInterface;

      // when
      actions.cleanSocialNetworks(mockActionContext, expected);

      // then
      const actionPayload: SetCompanyActionPayload = { fieldName: 'socialNetworks', fieldValue: expected };
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_COMPANY_FIELD', actionPayload);
    });

    it('should remove linkedIn property if empty', () => {
      // given
      const expected: PostSocialNetworksRequestInterface = {
        linkedIn: 'http://a.linkedIn.link',
      };
      mockActionContext.state = {
        company: {},
      } as CompanyCreateModuleStateInterface;

      // when
      actions.cleanSocialNetworks(mockActionContext, expected);

      // then
      const actionPayload: SetCompanyActionPayload = { fieldName: 'socialNetworks', fieldValue: expected };
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_COMPANY_FIELD', actionPayload);
    });
  });

  describe('reset()', () => {
    it('should make a mutation in store', () => {
      // when
      actions.reset(mockActionContext);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('RESET');
    });
  });
});

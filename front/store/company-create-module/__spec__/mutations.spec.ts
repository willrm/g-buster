import { CompanyCreateModuleStateInterface } from '~/store/company-create-module';
import mutations, { SetCompanyFieldMutationPayload } from '~/store/company-create-module/mutations';
import { CompanyEnums } from '../../../../back/src/infrastructure/rest/career/models/company-constraints-and-enums';
import {
  PostCompanyRequestInterface,
  PostSocialNetworksRequestInterface,
} from '../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { TemporaryFileResponseInterface } from '../../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';

describe('store/company-create-module/mutations', () => {
  let state: CompanyCreateModuleStateInterface;
  beforeEach(() => {
    state = {} as CompanyCreateModuleStateInterface;
  });

  describe('SET_COMPANY_FIELD()', () => {
    it('should set string company field', () => {
      // given
      state.company = {} as PostCompanyRequestInterface;
      const nameSet: SetCompanyFieldMutationPayload = {
        fieldName: 'name',
        fieldValue: 'A Company name',
      };

      // when
      mutations.SET_COMPANY_FIELD(state, nameSet);

      // then
      expect(state).toHaveProperty('company.name', 'A Company name');
    });

    it('should set number company field', () => {
      // given
      state.company = {} as PostCompanyRequestInterface;
      const sizeSet: SetCompanyFieldMutationPayload = {
        fieldName: 'size',
        fieldValue: 12,
      };

      // when
      mutations.SET_COMPANY_FIELD(state, sizeSet);

      // then
      expect(state).toHaveProperty('company.size', 12);
    });

    it('should set enum company field', () => {
      // given
      state.company = {} as PostCompanyRequestInterface;
      const businessSegmentSet: SetCompanyFieldMutationPayload = {
        fieldName: 'businessSegment',
        fieldValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_ENVIRONMENT,
      };

      // when
      mutations.SET_COMPANY_FIELD(state, businessSegmentSet);

      // then
      expect(state).toHaveProperty('company.businessSegment', 'BUSINESS_SEGMENT_ENVIRONMENT');
    });

    it('should set array company field', () => {
      // given
      state.company = {} as PostCompanyRequestInterface;
      const geniusTypesSet: SetCompanyFieldMutationPayload = {
        fieldName: 'geniusTypes',
        fieldValue: [CompanyEnums.CompanyGeniusType.GENIUS_TYPE_CIVIL_ENGINEERING, CompanyEnums.CompanyGeniusType.GENIUS_TYPE_AEROSPACE_ENGINEERING],
      };

      // when
      mutations.SET_COMPANY_FIELD(state, geniusTypesSet);

      // then
      expect(state).toHaveProperty('company.geniusTypes', ['GENIUS_TYPE_CIVIL_ENGINEERING', 'GENIUS_TYPE_AEROSPACE_ENGINEERING']);
    });

    it('should set date company field', () => {
      // given
      state.company = {} as PostCompanyRequestInterface;
      const fixedDate: Date = new Date();
      const creationDateSet: SetCompanyFieldMutationPayload = {
        fieldName: 'creationDate',
        fieldValue: fixedDate,
      };

      // when
      mutations.SET_COMPANY_FIELD(state, creationDateSet);

      // then
      expect(state).toHaveProperty('company.creationDate', fixedDate);
    });

    it('should set object company field', () => {
      // given
      state.company = {} as PostCompanyRequestInterface;
      const expected: PostSocialNetworksRequestInterface = { facebook: 'http://a.facebook.link' } as PostSocialNetworksRequestInterface;
      const socialNetworksSet: SetCompanyFieldMutationPayload = {
        fieldName: 'socialNetworks',
        fieldValue: expected,
      };

      // when
      mutations.SET_COMPANY_FIELD(state, socialNetworksSet);

      // then
      expect(state).toHaveProperty('company.socialNetworks', expected);
    });
  });
  describe('SET_COMPANY_LOGO', () => {
    it('should set companyLogo in state', () => {
      // given
      const payload: TemporaryFileResponseInterface = { uuid: 'an-uuid' } as TemporaryFileResponseInterface;
      state.companyLogo = {} as TemporaryFileResponseInterface;

      // when
      mutations.SET_COMPANY_LOGO(state, payload);

      // then
      expect(state).toHaveProperty('companyLogo', payload);
    });
  });

  describe('RESET', () => {
    it('should reset store to default state', () => {
      // given
      state.companyLogo = { uuid: 'an-uuid' } as TemporaryFileResponseInterface;
      state.company = { name: 'A company name', socialNetworks: { linkedIn: 'a linkedIn link' } } as PostCompanyRequestInterface;

      // when
      mutations.RESET(state);

      // then
      expect(state).toStrictEqual({
        company: {
          activityLocations: {},
          geniusTypes: [],
          socialNetworks: {},
          values: [{}],
        },
        companyLogo: {},
      });
    });
  });
});

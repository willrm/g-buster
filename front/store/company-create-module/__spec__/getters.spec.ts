import { CompanyCreateModuleStateInterface } from '~/store/company-create-module';
import { PostCompanyRequestInterface } from '../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { TemporaryFileResponseInterface } from '../../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';
import getters from '../getters';

describe('store/company-create-module/getter', () => {
  let state: CompanyCreateModuleStateInterface;
  beforeEach(() => {
    state = {
      company: { name: 'A company name' } as PostCompanyRequestInterface,
      companyLogo: {
        id: '1',
        base64: 'an-image-as-base64',
        mimeType: 'mime/type',
        filename: 'filename.extension',
        uuid: 'an-uuid',
        createdAt: new Date('2019-11-15T16:35:20'),
      },
    };
  });
  describe('getCompany', () => {
    it('should return company from state', () => {
      // when
      const result: PostCompanyRequestInterface = getters.getCompany(state);

      // then
      expect(result).toStrictEqual({ name: 'A company name' });
    });
  });

  describe('getCompanyLogo', () => {
    it('should return company logo from state', () => {
      // when
      const result: TemporaryFileResponseInterface = getters.getCompanyLogo(state);

      // then
      expect(result).toStrictEqual({
        id: '1',
        base64: 'an-image-as-base64',
        mimeType: 'mime/type',
        filename: 'filename.extension',
        uuid: 'an-uuid',
        createdAt: new Date('2019-11-15T16:35:20'),
      });
    });
  });
});

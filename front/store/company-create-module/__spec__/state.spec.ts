import { CompanyCreateModuleStateInterface } from '~/store/company-create-module';
import state from '~/store/company-create-module/state';
import { CompanyEnums } from '../../../../back/src/infrastructure/rest/career/models/company-constraints-and-enums';
import {
  PostActivityLocationsRequestInterface,
  PostCompanyRequestInterface,
  PostSocialNetworksRequestInterface,
  PostValueRequestInterface,
} from '../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { TemporaryFileResponseInterface } from '../../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';

describe('store/company-create-module/state()', () => {
  it('should return default state', () => {
    // given
    const expected: CompanyCreateModuleStateInterface = {
      company: {
        activityLocations: {} as PostActivityLocationsRequestInterface,
        geniusTypes: [] as CompanyEnums.CompanyGeniusType[],
        socialNetworks: {} as PostSocialNetworksRequestInterface,
        values: [{} as PostValueRequestInterface],
      } as PostCompanyRequestInterface,
      companyLogo: {} as TemporaryFileResponseInterface,
    } as CompanyCreateModuleStateInterface;

    // when
    const result: CompanyCreateModuleStateInterface = state();

    // then
    expect(result).toStrictEqual(expected);
  });
});

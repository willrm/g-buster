import { isEmpty } from 'lodash';
import { ActionContext } from 'vuex';
import ApiService from '~/services/api.service';
import { CompanyCreateModuleStateInterface } from '~/store/company-create-module/index';
import { SetCompanyFieldMutationPayload } from '~/store/company-create-module/mutations';
import {
  PostSocialNetworksRequestInterface,
  PostValueRequestInterface,
} from '../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { TemporaryFileResponseInterface } from '../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';

export type SetCompanyActionPayload = SetCompanyFieldMutationPayload;
export interface ValidateAndSubmitCompanyActionPayload {
  apiService: ApiService;
}

export default {
  setCompany(actionContext: ActionContext<CompanyCreateModuleStateInterface, object>, actionPayload: SetCompanyActionPayload): Promise<void> {
    return Promise.resolve(actionContext.commit('SET_COMPANY_FIELD', actionPayload));
  },

  setCompanyLogo(
    actionContext: ActionContext<CompanyCreateModuleStateInterface, object>,
    actionPayload: TemporaryFileResponseInterface
  ): Promise<void> {
    return Promise.resolve(actionContext.commit('SET_COMPANY_LOGO', actionPayload));
  },

  async validateCompany(
    actionContext: ActionContext<CompanyCreateModuleStateInterface, object>,
    actionPayload: ValidateAndSubmitCompanyActionPayload
  ): Promise<void> {
    await actionContext.dispatch('cleanCompanyValues');

    if (actionContext.state.company.socialNetworks) {
      await actionContext.dispatch('cleanSocialNetworks', actionContext.state.company.socialNetworks);
    }

    await actionPayload.apiService.createNewCompany(actionContext.state.company, true);
  },

  async submitCompany(
    actionContext: ActionContext<CompanyCreateModuleStateInterface, object>,
    actionPayload: ValidateAndSubmitCompanyActionPayload
  ): Promise<void> {
    await actionPayload.apiService.createNewCompany(actionContext.state.company);
  },

  cleanCompanyValues(actionContext: ActionContext<CompanyCreateModuleStateInterface, object>): Promise<void> {
    const { values }: { values: PostValueRequestInterface[] } = actionContext.state.company;
    const cleanedValues: PostValueRequestInterface[] = values.filter(
      (value: PostValueRequestInterface) => !isEmpty(value.meaning) && !isEmpty(value.justification)
    );

    const actionPayload: SetCompanyActionPayload = { fieldName: 'values', fieldValue: cleanedValues };

    return Promise.resolve(actionContext.commit('SET_COMPANY_FIELD', actionPayload));
  },

  cleanSocialNetworks(
    actionContext: ActionContext<CompanyCreateModuleStateInterface, object>,
    socialNetworks: PostSocialNetworksRequestInterface
  ): Promise<void> {
    const cleanedSocialNetworks: PostSocialNetworksRequestInterface = {};

    if (!isEmpty(socialNetworks.facebook)) {
      cleanedSocialNetworks.facebook = socialNetworks.facebook;
    }
    if (!isEmpty(socialNetworks.linkedIn)) {
      cleanedSocialNetworks.linkedIn = socialNetworks.linkedIn;
    }
    const actionPayload: SetCompanyActionPayload = { fieldName: 'socialNetworks', fieldValue: cleanedSocialNetworks };

    return Promise.resolve(actionContext.commit('SET_COMPANY_FIELD', actionPayload));
  },

  reset(actionContext: ActionContext<CompanyCreateModuleStateInterface, object>): Promise<void> {
    return Promise.resolve(actionContext.commit('RESET'));
  },
};

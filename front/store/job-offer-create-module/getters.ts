import { JobOfferCreateModuleStateInterface } from '~/store/job-offer-create-module/index';
import { PutJobOfferRequestInterface } from '../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';

export default {
  getJobOffer(state: JobOfferCreateModuleStateInterface): PutJobOfferRequestInterface {
    return state.jobOffer;
  },

  isDraftJobOffer(state: JobOfferCreateModuleStateInterface): boolean {
    return !!state.jobOffer.draft;
  },
};

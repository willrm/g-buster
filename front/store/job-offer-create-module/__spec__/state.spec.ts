import { JobOfferCreateModuleStateInterface } from '~/store/job-offer-create-module';
import { JobOfferEnums } from '../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import {
  PutAdvandagesRequestInterface,
  PutAnnualSalaryRangeInterface,
  PutTravelingRequestInterface,
} from '../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import state from '../state';

describe('store/job-offer-create-module/state()', () => {
  it('should return default state', () => {
    // given
    const expected: JobOfferCreateModuleStateInterface = {
      id: '',
      jobOffer: {
        targetCustomers: [] as JobOfferEnums.JobOfferTargetCustomer[],
        requiredExperiences: [] as JobOfferEnums.JobOfferRequiredExperience[],
        requestedSpecialities: [] as JobOfferEnums.JobOfferRequestedSpeciality[],
        wantedPersonalities: [] as JobOfferEnums.JobOfferWantedPersonality[],
        advantages: {} as PutAdvandagesRequestInterface,
        traveling: {} as PutTravelingRequestInterface,
        annualSalaryRange: {} as PutAnnualSalaryRangeInterface,
      },
    } as JobOfferCreateModuleStateInterface;

    // when
    const result: JobOfferCreateModuleStateInterface = state();

    // then
    expect(result).toStrictEqual(expected);
  });
});

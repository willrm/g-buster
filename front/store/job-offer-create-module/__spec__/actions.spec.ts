import { ActionContext } from 'vuex';
import ApiService from '~/services/api.service';
import { JobOfferCreateModuleStateInterface } from '~/store/job-offer-create-module';
import {
  GetJobOfferActionPayload,
  SetJobOfferActionPayload,
  SubmitAsDraftJobOfferActionPayload,
  SubmitJobOfferActionPayload,
  ValidateJobOfferActionPayload,
} from '~/store/job-offer-create-module/actions';
import { JobOfferEnums } from '../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { JobOfferResponseInterface } from '../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import { PostJobOfferResponseInterface } from '../../../../back/src/infrastructure/rest/career/models/post-job-offer-response.interface';
import { PutJobOfferRequestInterface } from '../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import { CommonTypeAliases } from '../../../../back/src/infrastructure/rest/common-models/common-type-aliases';
import actions from '../actions';

describe('store/job-offer-create-module/actions', () => {
  let mockActionContext: ActionContext<JobOfferCreateModuleStateInterface, object>;
  let $apiService: ApiService;
  let createNewJobOfferMock: jest.Mock;
  let updateDraftJobOfferMock: jest.Mock;
  let getUserJobOfferMock: jest.Mock;
  let setJobOfferMock: jest.Mock;

  beforeEach(() => {
    mockActionContext = {} as ActionContext<JobOfferCreateModuleStateInterface, object>;
    mockActionContext.commit = jest.fn();
    mockActionContext.dispatch = jest.fn();

    createNewJobOfferMock = jest.fn();
    updateDraftJobOfferMock = jest.fn();
    getUserJobOfferMock = jest.fn();
    setJobOfferMock = jest.fn();

    $apiService = ({
      createNewJobOffer: createNewJobOfferMock,
      updateDraftJobOffer: updateDraftJobOfferMock,
      getUserJobOffer: getUserJobOfferMock,
      setJobOffer: setJobOfferMock,
    } as unknown) as ApiService;
  });

  describe('setJobOfferId()', () => {
    it('should make a mutation in store', () => {
      // given
      const actionPayload: CommonTypeAliases.JobOfferId = 'aJobOfferId';

      // when
      actions.setJobOfferId(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_JOB_OFFER_ID', actionPayload);
    });
  });

  describe('setJobOfferField()', () => {
    it('should make a mutation in store', () => {
      // given
      const actionPayload: SetJobOfferActionPayload = { fieldName: 'title', fieldValue: 'A field Value' };

      // when
      actions.setJobOfferField(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_JOB_OFFER_FIELD', actionPayload);
    });
  });

  describe('setJobOffer()', () => {
    it('should make a mutation in store', () => {
      // given
      const actionPayload: PostJobOfferResponseInterface = ({ title: 'A job offer title' } as unknown) as PostJobOfferResponseInterface;

      // when
      actions.setJobOffer(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_JOB_OFFER', actionPayload);
    });
  });

  describe('validateJobOffer()', () => {
    it('should call api to validate job offer as non-draft with dryRun flag', async () => {
      // given
      const jobOffer: PutJobOfferRequestInterface = { title: 'A job title' } as PutJobOfferRequestInterface;
      mockActionContext.state = { jobOffer } as JobOfferCreateModuleStateInterface;
      const actionPayload: ValidateJobOfferActionPayload = { apiService: $apiService };

      // when
      await actions.validateJobOffer(mockActionContext, actionPayload);

      // then
      expect($apiService.createNewJobOffer).toHaveBeenCalledWith({ ...jobOffer, draft: false }, true);
    });

    it('should call clean email address for receipt applications action if it is an empty string', async () => {
      // given
      mockActionContext.state = { jobOffer: { emailAddressForReceiptOfApplications: '' } } as JobOfferCreateModuleStateInterface;
      const actionPayload: ValidateJobOfferActionPayload = { apiService: $apiService };

      // when
      await actions.validateJobOffer(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.dispatch).toHaveBeenCalledWith('cleanEmailAddressForReceiptOfApplications');
    });
  });

  describe('submitJobOffer()', () => {
    it('should call api to create job offer as non-draft when submitting form and there is no id in state', async () => {
      // given
      const expected: PutJobOfferRequestInterface = { title: 'A job title' } as PutJobOfferRequestInterface;
      mockActionContext.state = { id: '', jobOffer: expected } as JobOfferCreateModuleStateInterface;
      const actionPayload: SubmitJobOfferActionPayload = { apiService: $apiService };

      // when
      await actions.submitJobOffer(mockActionContext, actionPayload);

      // then
      expect($apiService.createNewJobOffer).toHaveBeenCalledWith({ ...expected, draft: false });
    });
    it('should call api to update job offer as non-draft when submitting form and there is an id in state', async () => {
      // given
      const id: CommonTypeAliases.JobOfferId = 'aJobOfferId';
      const expected: PutJobOfferRequestInterface = { title: 'A job title' } as PutJobOfferRequestInterface;
      mockActionContext.state = { id, jobOffer: expected } as JobOfferCreateModuleStateInterface;
      const actionPayload: SubmitJobOfferActionPayload = { apiService: $apiService };

      // when
      await actions.submitJobOffer(mockActionContext, actionPayload);

      // then
      expect($apiService.updateDraftJobOffer).toHaveBeenCalledWith(id, { ...expected, draft: false });
    });

    it('should set false draft status on store when successful creation', async () => {
      // given
      const expected: SetJobOfferActionPayload = { fieldName: 'draft', fieldValue: false };

      mockActionContext.state = { jobOffer: {} } as JobOfferCreateModuleStateInterface;
      const actionPayload: SubmitJobOfferActionPayload = { apiService: $apiService };
      createNewJobOfferMock.mockReturnValue({ id: 'aJobOfferId' });

      // when
      await actions.submitJobOffer(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.dispatch).toHaveBeenCalledWith('setJobOfferField', expected);
    });

    it('should not set any draft status when rejected creation', async () => {
      // given
      mockActionContext.state = { jobOffer: {} } as JobOfferCreateModuleStateInterface;

      createNewJobOfferMock.mockImplementation(() => {
        return Promise.reject(new Error());
      });
      const actionPayload: SubmitJobOfferActionPayload = { apiService: $apiService };

      // when
      const result: () => Promise<unknown> = () => actions.submitJobOffer(mockActionContext, actionPayload);

      // then
      await expect(result()).rejects.toThrow();
      expect(mockActionContext.dispatch).not.toHaveBeenCalled();
    });
  });

  describe('submitAsDraftJobOffer()', () => {
    it('should call api to create draft job offer if there is no id in store', async () => {
      // given
      const expected: PutJobOfferRequestInterface = { title: 'A job title' } as PutJobOfferRequestInterface;
      mockActionContext.state = { id: '', jobOffer: expected } as JobOfferCreateModuleStateInterface;
      const actionPayload: SubmitAsDraftJobOfferActionPayload = { apiService: $apiService };
      createNewJobOfferMock.mockReturnValue({ id: 'aJobOfferId' });

      // when
      await actions.submitAsDraftJobOffer(mockActionContext, actionPayload);

      // then
      expect($apiService.createNewJobOffer).toHaveBeenCalledWith({ ...expected, draft: true });
    });

    it('should call api to update draft job offer if there is an id in store', async () => {
      // given
      const id: CommonTypeAliases.JobOfferId = 'aJobOfferId';
      const expected: PutJobOfferRequestInterface = { title: 'A job title' } as PutJobOfferRequestInterface;
      mockActionContext.state = { id, jobOffer: expected } as JobOfferCreateModuleStateInterface;
      const actionPayload: SubmitAsDraftJobOfferActionPayload = { apiService: $apiService };

      // when
      await actions.submitAsDraftJobOffer(mockActionContext, actionPayload);

      // then
      expect($apiService.updateDraftJobOffer).toHaveBeenCalledWith(id, { ...expected, draft: true });
    });

    it('should set id in store on successful creation', async () => {
      // given
      mockActionContext.state = { id: '', jobOffer: {} } as JobOfferCreateModuleStateInterface;
      const actionPayload: SubmitAsDraftJobOfferActionPayload = { apiService: $apiService };
      createNewJobOfferMock.mockReturnValue({ id: 'aJobOfferId' });

      // when
      await actions.submitAsDraftJobOffer(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.dispatch).toHaveBeenCalledWith('setJobOfferId', 'aJobOfferId');
    });

    it('should set true draft status on store when successful creation', async () => {
      // given
      const expected: SetJobOfferActionPayload = { fieldName: 'draft', fieldValue: true };

      mockActionContext.state = { jobOffer: {} } as JobOfferCreateModuleStateInterface;
      const actionPayload: SubmitAsDraftJobOfferActionPayload = { apiService: $apiService };
      createNewJobOfferMock.mockReturnValue({ id: 'aJobOfferId' });

      // when
      await actions.submitAsDraftJobOffer(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.dispatch).toHaveBeenCalledWith('setJobOfferField', expected);
    });

    it('should not set any draft status when rejected creation', async () => {
      // given
      mockActionContext.state = { jobOffer: {} } as JobOfferCreateModuleStateInterface;

      createNewJobOfferMock.mockImplementation(() => {
        return Promise.reject(new Error());
      });
      const actionPayload: SubmitAsDraftJobOfferActionPayload = { apiService: $apiService };

      // when
      const result: () => Promise<unknown> = () => actions.submitAsDraftJobOffer(mockActionContext, actionPayload);

      // then
      await expect(result()).rejects.toThrow();
      expect(mockActionContext.dispatch).not.toHaveBeenCalled();
    });
  });

  describe('getJobOffer()', () => {
    it('should call api to get job offer by id', async () => {
      // given
      mockActionContext.state = { id: 'aJobOfferId' } as JobOfferCreateModuleStateInterface;
      const jobOfferResponse: JobOfferResponseInterface = { title: 'A job offer' } as JobOfferResponseInterface;
      getUserJobOfferMock.mockReturnValue(jobOfferResponse);
      const actionPayload: GetJobOfferActionPayload = { apiService: $apiService };

      // when
      await actions.getJobOffer(mockActionContext, actionPayload);

      // then
      expect($apiService.getUserJobOffer).toHaveBeenCalledWith('aJobOfferId');
    });

    it('should make a mutation in store', async () => {
      // given
      mockActionContext.state = { id: 'aJobOfferId' } as JobOfferCreateModuleStateInterface;
      const actionPayload: GetJobOfferActionPayload = { apiService: $apiService };
      const jobOfferResponse: JobOfferResponseInterface = {
        title: 'A job offer',
        status: JobOfferEnums.JobOfferStatus.STATUS_PENDING_VALIDATION,
      } as JobOfferResponseInterface;
      getUserJobOfferMock.mockReturnValue(jobOfferResponse);

      // when
      await actions.getJobOffer(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.dispatch).toHaveBeenCalledWith('setJobOffer', { title: 'A job offer', draft: false });
    });

    it('should make a mutation with draft flag to true if status is draft', async () => {
      // given
      mockActionContext.state = { id: 'aJobOfferId' } as JobOfferCreateModuleStateInterface;
      const actionPayload: GetJobOfferActionPayload = { apiService: $apiService };
      const jobOfferResponse: JobOfferResponseInterface = {
        title: 'A job offer',
        status: JobOfferEnums.JobOfferStatus.STATUS_DRAFT,
      } as JobOfferResponseInterface;
      getUserJobOfferMock.mockReturnValue(jobOfferResponse);

      // when
      await actions.getJobOffer(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.dispatch).toHaveBeenCalledWith('setJobOffer', { title: 'A job offer', draft: true });
    });

    it('should make a mutation with draft flag to false if status is not draft', async () => {
      // given
      mockActionContext.state = { id: 'aJobOfferId' } as JobOfferCreateModuleStateInterface;
      const actionPayload: GetJobOfferActionPayload = { apiService: $apiService };
      const jobOfferResponse: JobOfferResponseInterface = {
        title: 'A job offer',
        status: JobOfferEnums.JobOfferStatus.STATUS_PENDING_VALIDATION,
      } as JobOfferResponseInterface;
      getUserJobOfferMock.mockReturnValue(jobOfferResponse);

      // when
      await actions.getJobOffer(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.dispatch).toHaveBeenCalledWith('setJobOffer', { title: 'A job offer', draft: false });
    });
  });

  describe('cleanEmailAddressForReceiptOfApplications()', () => {
    it('should set emailAddressForReceiptOfApplications property to null if value is an empty string', async () => {
      // given
      mockActionContext.state = { jobOffer: { emailAddressForReceiptOfApplications: '' } } as JobOfferCreateModuleStateInterface;

      // when
      await actions.cleanEmailAddressForReceiptOfApplications(mockActionContext);

      // then
      const actionPayload: SetJobOfferActionPayload = { fieldName: 'emailAddressForReceiptOfApplications', fieldValue: undefined };
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_JOB_OFFER_FIELD', actionPayload);
    });
  });

  describe('reset()', () => {
    it('should make a mutation in store', () => {
      // when
      actions.reset(mockActionContext);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('RESET');
    });
  });
});

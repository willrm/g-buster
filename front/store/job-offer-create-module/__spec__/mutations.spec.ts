import { JobOfferCreateModuleStateInterface } from '~/store/job-offer-create-module';
import { JobOfferEnums } from '../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { PostJobOfferRequestInterface } from '../../../../back/src/infrastructure/rest/career/models/post-job-offer-request.interface';
import { PutJobOfferRequestInterface } from '../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import { CommonTypeAliases } from '../../../../back/src/infrastructure/rest/common-models/common-type-aliases';
import mutations, { SetJobOfferFieldMutationPayload } from '../mutations';

describe('store/job-offer-create-module/mutations', () => {
  let state: JobOfferCreateModuleStateInterface;
  beforeEach(() => {
    state = {} as JobOfferCreateModuleStateInterface;
  });

  describe('SET_JOB_OFFER_ID()', () => {
    it('should set job offer id', () => {
      // given
      state.id = '';
      const id: CommonTypeAliases.JobOfferId = 'aJobOfferId';

      // when
      mutations.SET_JOB_OFFER_ID(state, id);

      // then
      expect(state).toHaveProperty('id', id);
    });
  });

  describe('SET_JOB_OFFER()', () => {
    it('should set job offer', () => {
      // given
      state.jobOffer = {} as PostJobOfferRequestInterface;
      const expected: PostJobOfferRequestInterface = { title: 'A job offer title' } as PostJobOfferRequestInterface;

      // when
      mutations.SET_JOB_OFFER(state, expected);

      // then
      expect(state).toHaveProperty('jobOffer', expected);
    });
    it('should merge job offer for undefined fields', () => {
      // given
      state.jobOffer = {
        annualSalaryRange: {},
      } as PostJobOfferRequestInterface;

      const jobOffer: PostJobOfferRequestInterface = { title: 'A job offer title' } as PostJobOfferRequestInterface;

      // when
      mutations.SET_JOB_OFFER(state, jobOffer);

      // then
      expect(state).toHaveProperty('jobOffer', { title: 'A job offer title', annualSalaryRange: {} });
    });
  });

  describe('SET_JOB_OFFER_FIELD()', () => {
    it('should set string job offer field', () => {
      // given
      state.jobOffer = {} as PutJobOfferRequestInterface;
      const nameSet: SetJobOfferFieldMutationPayload = {
        fieldName: 'title',
        fieldValue: 'A job offer title',
      };

      // when
      mutations.SET_JOB_OFFER_FIELD(state, nameSet);

      // then
      expect(state).toHaveProperty('jobOffer.title', 'A job offer title');
    });

    it('should set enum job offer field', () => {
      // given
      state.jobOffer = {} as PutJobOfferRequestInterface;
      const jobTypeSet: SetJobOfferFieldMutationPayload = {
        fieldName: 'jobType',
        fieldValue: JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR,
      };

      // when
      mutations.SET_JOB_OFFER_FIELD(state, jobTypeSet);

      // then
      expect(state).toHaveProperty('jobOffer.jobType', 'JOB_TYPE_REGULAR');
    });
  });

  describe('RESET', () => {
    it('should reset store to default state', () => {
      // given
      state.jobOffer = { title: 'A job title', jobType: JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR } as PutJobOfferRequestInterface;

      // when
      mutations.RESET(state);

      // then
      expect(state).toStrictEqual({
        id: '',
        jobOffer: {
          targetCustomers: [],
          requiredExperiences: [],
          requestedSpecialities: [],
          wantedPersonalities: [],
          advantages: {},
          traveling: {},
          annualSalaryRange: {},
        },
      });
    });
  });
});

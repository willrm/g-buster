import { JobOfferCreateModuleStateInterface } from '~/store/job-offer-create-module';
import { PutJobOfferRequestInterface } from '../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import getters from '../getters';

describe('store/job-offer-create-module/getters', () => {
  let state: JobOfferCreateModuleStateInterface;
  beforeEach(() => {
    state = {
      id: '',
      jobOffer: { title: 'A job title' } as PutJobOfferRequestInterface,
    };
  });
  describe('getJobOffer()', () => {
    it('should return jobOffer from state', () => {
      // when
      const result: PutJobOfferRequestInterface = getters.getJobOffer(state);

      // then
      expect(result).toStrictEqual({ title: 'A job title' });
    });
  });

  describe('isDraftJobOffer()', () => {
    it('should return false if status is not defined', () => {
      // when
      const result: boolean = getters.isDraftJobOffer(state);

      // then
      expect(result).toBe(false);
    });

    it('should return false if status is not draft', () => {
      // given
      state.jobOffer.draft = false;

      // when
      const result: boolean = getters.isDraftJobOffer(state);

      // then
      expect(result).toBe(false);
    });

    it('should return true if status is draft', () => {
      // given
      state.jobOffer.draft = true;

      // when
      const result: boolean = getters.isDraftJobOffer(state);

      // then
      expect(result).toBe(true);
    });
  });
});

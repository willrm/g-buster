import { JobOfferCreateModuleStateInterface } from '~/store/job-offer-create-module/index';
import { JobOfferEnums } from '../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import {
  PutAdvandagesRequestInterface,
  PutAnnualSalaryRangeInterface,
  PutJobOfferRequestInterface,
  PutTravelingRequestInterface,
} from '../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';

export default () =>
  ({
    id: '',
    jobOffer: {
      targetCustomers: [] as JobOfferEnums.JobOfferTargetCustomer[],
      requiredExperiences: [] as JobOfferEnums.JobOfferRequiredExperience[],
      requestedSpecialities: [] as JobOfferEnums.JobOfferRequestedSpeciality[],
      wantedPersonalities: [] as JobOfferEnums.JobOfferWantedPersonality[],
      advantages: {} as PutAdvandagesRequestInterface,
      traveling: {} as PutTravelingRequestInterface,
      annualSalaryRange: {} as PutAnnualSalaryRangeInterface,
    } as PutJobOfferRequestInterface,
  } as JobOfferCreateModuleStateInterface);

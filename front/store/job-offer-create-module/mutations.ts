import { cloneDeep, merge } from 'lodash';
import Vue from 'vue';
import { JobOfferCreateModuleStateInterface } from '~/store/job-offer-create-module/index';
import state from '~/store/job-offer-create-module/state';
import { PostJobOfferRequestInterface } from '../../../back/src/infrastructure/rest/career/models/post-job-offer-request.interface';
import { PutJobOfferRequestInterface } from '../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import { CommonTypeAliases } from '../../../back/src/infrastructure/rest/common-models/common-type-aliases';

type PostJobOfferRequestFieldName = keyof PutJobOfferRequestInterface;

export interface SetJobOfferFieldMutationPayload {
  fieldName: PostJobOfferRequestFieldName;
  fieldValue: PutJobOfferRequestInterface[PostJobOfferRequestFieldName];
}

export default {
  SET_JOB_OFFER_ID(currentState: JobOfferCreateModuleStateInterface, id: CommonTypeAliases.JobOfferId): void {
    currentState.id = id;
  },

  SET_JOB_OFFER(currentState: JobOfferCreateModuleStateInterface, jobOffer: PostJobOfferRequestInterface): void {
    const mutatedJobOffer: PostJobOfferRequestInterface = cloneDeep(currentState.jobOffer);

    // Use merge method to keep properties of mutatedJobOffer if it is undefined in jobOffer
    // Example: if jobOffer.annualSalaryRange is undefined, it will keep mutatedJobOffer.annualSalaryRange
    merge(mutatedJobOffer, jobOffer);
    Vue.set(currentState, 'jobOffer', mutatedJobOffer);
  },

  SET_JOB_OFFER_FIELD(currentState: JobOfferCreateModuleStateInterface, field: SetJobOfferFieldMutationPayload): void {
    // RISK: This method allows to path any value regardless of type.
    // It's an accepted risk as the backend should throw an error if value is not permitted.
    // Example: jobOffer.jobOfferJobStatus that is an Enum can take 'FAKE_JOB_STATUS' as value
    Vue.set(currentState.jobOffer, field.fieldName, field.fieldValue);
  },

  RESET(currentState: JobOfferCreateModuleStateInterface): void {
    Object.assign(currentState, state());
  },
};

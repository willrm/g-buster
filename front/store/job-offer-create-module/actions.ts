import { isEmpty, omit } from 'lodash';
import { ActionContext } from 'vuex';
import ApiService from '~/services/api.service';
import { JobOfferCreateModuleStateInterface } from '~/store/job-offer-create-module/index';
import { SetJobOfferFieldMutationPayload } from '~/store/job-offer-create-module/mutations';
import { GetJobOfferResponseInterface } from '../../../back/src/infrastructure/rest/career/models/get-job-offer-response.interface';
import { JobOfferEnums } from '../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { PostJobOfferRequestInterface } from '../../../back/src/infrastructure/rest/career/models/post-job-offer-request.interface';
import { PostJobOfferResponseInterface } from '../../../back/src/infrastructure/rest/career/models/post-job-offer-response.interface';
import { CommonTypeAliases } from '../../../back/src/infrastructure/rest/common-models/common-type-aliases';

export type SetJobOfferActionPayload = SetJobOfferFieldMutationPayload;
export interface JobOfferCreateActionPayload {
  apiService: ApiService;
}

export type ValidateJobOfferActionPayload = JobOfferCreateActionPayload;
export type SubmitJobOfferActionPayload = JobOfferCreateActionPayload;
export type SubmitAsDraftJobOfferActionPayload = JobOfferCreateActionPayload;
export type GetJobOfferActionPayload = JobOfferCreateActionPayload;

export default {
  setJobOfferId(
    actionContext: ActionContext<JobOfferCreateModuleStateInterface, object>,
    actionPayload: CommonTypeAliases.JobOfferId
  ): Promise<void> {
    return Promise.resolve(actionContext.commit('SET_JOB_OFFER_ID', actionPayload));
  },

  setJobOffer(actionContext: ActionContext<JobOfferCreateModuleStateInterface, object>, actionPayload: PostJobOfferResponseInterface): Promise<void> {
    return Promise.resolve(actionContext.commit('SET_JOB_OFFER', actionPayload));
  },

  setJobOfferField(actionContext: ActionContext<JobOfferCreateModuleStateInterface, object>, actionPayload: SetJobOfferActionPayload): Promise<void> {
    return Promise.resolve(actionContext.commit('SET_JOB_OFFER_FIELD', actionPayload));
  },

  async validateJobOffer(
    actionContext: ActionContext<JobOfferCreateModuleStateInterface, object>,
    actionPayload: ValidateJobOfferActionPayload
  ): Promise<void> {
    const jobOffer: PostJobOfferRequestInterface = { ...actionContext.state.jobOffer, draft: false };

    if (isEmpty(jobOffer.emailAddressForReceiptOfApplications)) {
      await actionContext.dispatch('cleanEmailAddressForReceiptOfApplications');
    }

    await actionPayload.apiService.createNewJobOffer(jobOffer, true);
  },

  async submitJobOffer(
    actionContext: ActionContext<JobOfferCreateModuleStateInterface, object>,
    actionPayload: SubmitJobOfferActionPayload
  ): Promise<void> {
    const jobOffer: PostJobOfferRequestInterface = { ...actionContext.state.jobOffer, draft: false };

    isEmpty(actionContext.state.id)
      ? await actionPayload.apiService.createNewJobOffer(jobOffer)
      : await actionPayload.apiService.updateDraftJobOffer(actionContext.state.id, jobOffer);

    return actionContext.dispatch('setJobOfferField', { fieldName: 'draft', fieldValue: false });
  },

  async submitAsDraftJobOffer(
    actionContext: ActionContext<JobOfferCreateModuleStateInterface, object>,
    actionPayload: SubmitAsDraftJobOfferActionPayload
  ): Promise<void> {
    const jobOffer: PostJobOfferRequestInterface = { ...actionContext.state.jobOffer, draft: true };

    if (isEmpty(actionContext.state.id)) {
      const { id }: PostJobOfferResponseInterface = await actionPayload.apiService.createNewJobOffer(jobOffer);
      await actionContext.dispatch('setJobOfferId', id);
    } else {
      await actionPayload.apiService.updateDraftJobOffer(actionContext.state.id, jobOffer);
    }

    return actionContext.dispatch('setJobOfferField', { fieldName: 'draft', fieldValue: true });
  },

  async getJobOffer(
    actionContext: ActionContext<JobOfferCreateModuleStateInterface, object>,
    actionPayload: GetJobOfferActionPayload
  ): Promise<void> {
    const jobOffer: GetJobOfferResponseInterface = await actionPayload.apiService.getUserJobOffer(actionContext.state.id);

    const jobOfferToSet: PostJobOfferRequestInterface = { ...jobOffer, draft: JobOfferEnums.JobOfferStatus.STATUS_DRAFT === jobOffer.status };

    return actionContext.dispatch('setJobOffer', omit(jobOfferToSet, 'status'));
  },

  cleanEmailAddressForReceiptOfApplications(actionContext: ActionContext<JobOfferCreateModuleStateInterface, object>): Promise<void> {
    const actionPayload: SetJobOfferActionPayload = { fieldName: 'emailAddressForReceiptOfApplications', fieldValue: undefined };

    return Promise.resolve(actionContext.commit('SET_JOB_OFFER_FIELD', actionPayload));
  },

  reset(actionContext: ActionContext<JobOfferCreateModuleStateInterface, object>): Promise<void> {
    return Promise.resolve(actionContext.commit('RESET'));
  },
};

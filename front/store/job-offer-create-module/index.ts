import { PutJobOfferRequestInterface } from '../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import { CommonTypeAliases } from '../../../back/src/infrastructure/rest/common-models/common-type-aliases';
import state from './state';

export interface JobOfferCreateModuleStateInterface {
  id: CommonTypeAliases.JobOfferId;
  jobOffer: PutJobOfferRequestInterface;
}

export default { state };

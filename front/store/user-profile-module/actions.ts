import { ActionContext } from 'vuex';
import ApiService from '~/services/api.service';
import { UserProfileModuleStateInterface } from '~/store/user-profile-module/index';
import { UserResponseInterface } from '../../../back/src/infrastructure/rest/user/models/user-response.interface';

export interface SetUserProfileActionPayload {
  apiService: ApiService;
}

export default {
  async setUserProfile(
    actionContext: ActionContext<UserProfileModuleStateInterface, object>,
    actionPayload: SetUserProfileActionPayload
  ): Promise<void> {
    try {
      const user: UserResponseInterface = await actionPayload.apiService.getUserProfile();

      return Promise.resolve(actionContext.commit('SET_USER_PROFILE', user));
    } catch (e) {
      return Promise.resolve(actionContext.commit('SET_USER_PROFILE', {} as UserResponseInterface));
    }
  },

  reset(actionContext: ActionContext<UserProfileModuleStateInterface, object>): Promise<void> {
    return Promise.resolve(actionContext.commit('RESET'));
  },
};

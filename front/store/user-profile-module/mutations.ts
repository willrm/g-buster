import Vue from 'vue';
import { UserProfileModuleStateInterface } from '~/store/user-profile-module/index';
import { UserResponseInterface } from '../../../back/src/infrastructure/rest/user/models/user-response.interface';
import state from './state';

export default {
  SET_USER_PROFILE(currentState: UserProfileModuleStateInterface, userProfile: UserResponseInterface): void {
    Vue.set(currentState, 'userProfile', userProfile);
  },

  RESET(currentState: UserProfileModuleStateInterface): void {
    Object.assign(currentState, state());
  },
};

import { UserProfileModuleStateInterface } from '~/store/user-profile-module/index';
import { UserEnums } from '../../../back/src/infrastructure/rest/user/models/user-constraints-and-enums';
import { UserResponseInterface } from '../../../back/src/infrastructure/rest/user/models/user-response.interface';

export default {
  getUserProfile(state: UserProfileModuleStateInterface): UserResponseInterface {
    return state.userProfile;
  },

  isLoggedIn(state: UserProfileModuleStateInterface): boolean {
    return Object.keys(state.userProfile).length !== 0;
  },

  isLoggedAsCandidate(state: UserProfileModuleStateInterface): boolean {
    return state.userProfile.role === UserEnums.UserRole.CANDIDATE;
  },

  isLoggedAsCompany(state: UserProfileModuleStateInterface): boolean {
    return state.userProfile.role === UserEnums.UserRole.COMPANY;
  },
};

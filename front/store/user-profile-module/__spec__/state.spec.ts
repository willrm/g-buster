import { UserProfileModuleStateInterface } from '~/store/user-profile-module';
import state from '~/store/user-profile-module/state';

describe('store/user-profile-module/state', () => {
  it('should return default value', () => {
    // given
    const expected: UserProfileModuleStateInterface = {
      userProfile: {},
    } as UserProfileModuleStateInterface;

    // when
    const result: UserProfileModuleStateInterface = state();

    // then
    expect(result).toStrictEqual(expected);
  });
});

import { ActionContext } from 'vuex';
import ApiService from '~/services/api.service';
import { UserProfileModuleStateInterface } from '~/store/user-profile-module';

import actions, { SetUserProfileActionPayload } from '~/store/user-profile-module/actions';
import { UserResponseInterface } from '../../../../back/src/infrastructure/rest/user/models/user-response.interface';

describe('store/user-profile-module/actions', () => {
  let mockActionContext: ActionContext<UserProfileModuleStateInterface, object>;
  let $apiService: ApiService;
  let getUserProfileMock: jest.Mock;

  beforeEach(() => {
    mockActionContext = {} as ActionContext<UserProfileModuleStateInterface, object>;
    mockActionContext.commit = jest.fn();

    getUserProfileMock = jest.fn();

    $apiService = ({
      getUserProfile: getUserProfileMock,
    } as unknown) as ApiService;
  });

  describe('setUserProfile()', () => {
    it('should call api service to fetch user profile', async () => {
      // given
      const actionPayload: SetUserProfileActionPayload = { apiService: $apiService };

      // when
      await actions.setUserProfile(mockActionContext, actionPayload);

      // then
      expect(getUserProfileMock).toHaveBeenCalled();
    });

    it('should make a mutation in store with fetched user profile', async () => {
      // given
      const expected: UserResponseInterface = { firstName: 'First Name' } as UserResponseInterface;
      getUserProfileMock.mockReturnValue(Promise.resolve(expected));

      // when
      await actions.setUserProfile(mockActionContext, { apiService: $apiService });

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_USER_PROFILE', expected);
    });

    it('should make a mutation in store with empty user profile when user is not logged in', async () => {
      // given
      getUserProfileMock.mockReturnValue(Promise.reject(new Error('403 Forbidden')));

      // when
      await actions.setUserProfile(mockActionContext, { apiService: $apiService });

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_USER_PROFILE', {} as UserResponseInterface);
    });
  });

  describe('reset()', () => {
    it('should make a mutation to clean store', async () => {
      // when
      await actions.reset(mockActionContext);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('RESET');
    });
  });
});

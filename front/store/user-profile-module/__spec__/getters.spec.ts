import { UserProfileModuleStateInterface } from '~/store/user-profile-module';
import getters from '~/store/user-profile-module/getters';
import { UserEnums } from '../../../../back/src/infrastructure/rest/user/models/user-constraints-and-enums';
import { UserResponseInterface } from '../../../../back/src/infrastructure/rest/user/models/user-response.interface';

describe('store/user-profile-module/getters', () => {
  let state: UserProfileModuleStateInterface;
  beforeEach(() => {
    state = {
      userProfile: { firstName: 'User firstname' } as UserResponseInterface,
    };
  });
  describe('getUserProfile()', () => {
    it('should return user profile from state', () => {
      // when
      const result: UserResponseInterface = getters.getUserProfile(state);

      // then
      expect(result).toStrictEqual({ firstName: 'User firstname' });
    });
  });
  describe('isLoggedIn()', () => {
    it('should return false when user is not logged', () => {
      // given
      state.userProfile = {} as UserResponseInterface;

      // when
      const result: boolean = getters.isLoggedIn(state);

      // then
      expect(result).toBe(false);
    });
    it('should return true when user is logged', () => {
      // given
      state.userProfile.role = UserEnums.UserRole.CANDIDATE;

      // when
      const result: boolean = getters.isLoggedIn(state);

      // then
      expect(result).toBe(true);
    });
  });
  describe('isLoggedAsCandidate()', () => {
    it('should return false when user is not logged', () => {
      // when
      const result: boolean = getters.isLoggedAsCandidate(state);

      // then
      expect(result).toBe(false);
    });
    it('should return true when user is logged as candidate', () => {
      // given
      state.userProfile.role = UserEnums.UserRole.CANDIDATE;

      // when
      const result: boolean = getters.isLoggedAsCandidate(state);

      // then
      expect(result).toBe(true);
    });
    it('should return true when user is logged as company', () => {
      // given
      state.userProfile.role = UserEnums.UserRole.COMPANY;

      // when
      const result: boolean = getters.isLoggedAsCandidate(state);

      // then
      expect(result).toBe(false);
    });
    it('should return true when user is logged as admin', () => {
      // given
      state.userProfile.role = UserEnums.UserRole.ADMIN;

      // when
      const result: boolean = getters.isLoggedAsCandidate(state);

      // then
      expect(result).toBe(false);
    });
  });
  describe('isLoggedAsCompany()', () => {
    it('should return false when user is not logged', () => {
      // when
      const result: boolean = getters.isLoggedAsCompany(state);

      // then
      expect(result).toBe(false);
    });
    it('should return true when user is logged as company', () => {
      // given
      state.userProfile.role = UserEnums.UserRole.COMPANY;

      // when
      const result: boolean = getters.isLoggedAsCompany(state);

      // then
      expect(result).toBe(true);
    });
    it('should return false when user is logged as candidate', () => {
      // given
      state.userProfile.role = UserEnums.UserRole.CANDIDATE;

      // when
      const result: boolean = getters.isLoggedAsCompany(state);

      // then
      expect(result).toBe(false);
    });
    it('should return true when user is logged as admin', () => {
      // given
      state.userProfile.role = UserEnums.UserRole.ADMIN;

      // when
      const result: boolean = getters.isLoggedAsCompany(state);

      // then
      expect(result).toBe(false);
    });
  });
});

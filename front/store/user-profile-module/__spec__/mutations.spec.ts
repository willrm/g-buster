import { UserProfileModuleStateInterface } from '~/store/user-profile-module';
import mutations from '~/store/user-profile-module/mutations';
import { UserResponseInterface } from '../../../../back/src/infrastructure/rest/user/models/user-response.interface';

describe('store/user-profile-module/mutations', () => {
  let state: UserProfileModuleStateInterface;
  beforeEach(() => {
    state = {} as UserProfileModuleStateInterface;
  });

  describe('SET_USER_PROFILE()', () => {
    it('should set user profile', () => {
      // given
      state.userProfile = {} as UserResponseInterface;
      const expected: UserResponseInterface = { firstName: 'First Name' } as UserResponseInterface;

      // when
      mutations.SET_USER_PROFILE(state, expected);

      // then
      expect(state.userProfile).toStrictEqual(expected);
    });
  });
});

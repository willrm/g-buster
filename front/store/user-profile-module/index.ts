import { UserResponseInterface } from '../../../back/src/infrastructure/rest/user/models/user-response.interface';
import state from './state';

export interface UserProfileModuleStateInterface {
  userProfile: UserResponseInterface;
}

export default { state };

import { UserProfileModuleStateInterface } from '~/store/user-profile-module/index';

export default () =>
  ({
    userProfile: {},
  } as UserProfileModuleStateInterface);

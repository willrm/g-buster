import { Context, NuxtAppOptions } from '@nuxt/types';
import { Route } from 'vue-router';
import { ActionContext } from 'vuex';
import ApiService from '~/services/api.service';
import { actions } from '~/store';
import { LayoutModuleStateInterface } from '~/store/layout-module';
import { SetLayoutActionPayload } from '~/store/layout-module/actions';
import { SetUserProfileActionPayload } from '~/store/user-profile-module/actions';

describe('store/index', () => {
  describe('actions', () => {
    describe('nuxtServerInit()', () => {
      let mockActionContext: ActionContext<LayoutModuleStateInterface, object>;
      let mockContext: Context;
      let mockSetLocale: jest.Mock;

      beforeEach(() => {
        mockActionContext = {} as ActionContext<LayoutModuleStateInterface, object>;

        mockActionContext.dispatch = jest.fn();
        mockContext = {} as Context;
        mockContext.app = {} as NuxtAppOptions;
        mockSetLocale = jest.fn();
        Object.assign(mockContext.app, {
          ...mockContext.app,
          i18n: {
            setLocale: mockSetLocale,
          },
          route: {
            params: {},
          },
        });
        mockContext.route = {} as Route;
        mockContext.route.params = {} as { [key: string]: string };
      });

      describe('dispatch()', () => {
        it('should be called with setLayout action and ApiService and default lang as payload when no lang', async () => {
          // given
          const mockApiService: ApiService = {} as ApiService;
          mockContext.app.$apiService = mockApiService;
          mockContext.route.params.lang = '';

          // when
          // @ts-ignore
          await actions.nuxtServerInit(mockActionContext, mockContext);

          // then
          const expectedPayload: SetLayoutActionPayload = {
            apiService: mockApiService,
            params: {
              lang: 'fr-ca',
            },
          };
          expect(mockActionContext.dispatch).toHaveBeenCalledWith('layout-module/setLayout', expectedPayload);
        });

        it('should be called with setLayout action and ApiService and lang from route params', async () => {
          // given
          const mockApiService: ApiService = {} as ApiService;
          mockContext.app.$apiService = mockApiService;
          mockContext.route.params.lang = 'en-ca';

          // when
          // @ts-ignore
          await actions.nuxtServerInit(mockActionContext, mockContext);

          // then
          const expectedPayload: SetLayoutActionPayload = {
            apiService: mockApiService,
            params: {
              lang: 'en-ca',
            },
          };
          expect(mockActionContext.dispatch).toHaveBeenCalledWith('layout-module/setLayout', expectedPayload);
        });

        it('should be called with setLayout action and ApiService and default lang when lang is not fr-ca or en-ca', async () => {
          // given
          const mockApiService: ApiService = {} as ApiService;
          mockContext.app.$apiService = mockApiService;
          mockContext.route.params.lang = 'an-unknown-lang';

          // when
          // @ts-ignore
          await actions.nuxtServerInit(mockActionContext, mockContext);

          // then
          const expectedPayload: SetLayoutActionPayload = {
            apiService: mockApiService,
            params: {
              lang: 'fr-ca',
            },
          };
          expect(mockActionContext.dispatch).toHaveBeenCalledWith('layout-module/setLayout', expectedPayload);
        });

        it('should be called with setUserProfile action and ApiService', async () => {
          // given
          const mockApiService: ApiService = {} as ApiService;
          mockContext.app.$apiService = mockApiService;

          // when
          // @ts-ignore
          await actions.nuxtServerInit(mockActionContext, mockContext);

          // then
          const expectedPayload: SetUserProfileActionPayload = {
            apiService: mockApiService,
          };
          expect(mockActionContext.dispatch).toHaveBeenCalledWith('user-profile-module/setUserProfile', expectedPayload);
        });
      });
    });
  });
});

import { CompanyDashboardModuleStateInterface } from '~/store/company-dashboard-module';
import state from '../state';

describe('store/company-dashboard-module/state()', () => {
  it('should return default state', () => {
    // given
    const expected: CompanyDashboardModuleStateInterface = {
      jobOffers: [],
    } as CompanyDashboardModuleStateInterface;

    // when
    const result: CompanyDashboardModuleStateInterface = state();

    // then
    expect(result).toStrictEqual(expected);
  });
});

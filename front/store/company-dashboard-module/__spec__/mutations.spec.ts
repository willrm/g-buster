import { CompanyDashboardModuleStateInterface } from '~/store/company-dashboard-module';
import { JobOfferResponseInterface } from '../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import mutations from '../mutations';

describe('store/company-dashboard-module/mutations', () => {
  let state: CompanyDashboardModuleStateInterface;
  beforeEach(() => {
    state = {} as CompanyDashboardModuleStateInterface;
  });

  describe('SET_JOB_OFFERS()', () => {
    it('should set job offers', () => {
      // given
      state.jobOffers = [] as JobOfferResponseInterface[];
      const expected: JobOfferResponseInterface[] = [{ title: 'A job offer' } as JobOfferResponseInterface];

      // when
      mutations.SET_JOB_OFFERS(state, expected);

      // then
      expect(state.jobOffers).toStrictEqual(expected);
    });
  });

  describe('RESET', () => {
    it('should reset store to default state', () => {
      // given
      state.jobOffers = [{ title: 'A job title' } as JobOfferResponseInterface];

      // when
      mutations.RESET(state);

      // then
      expect(state).toStrictEqual({
        jobOffers: [],
      });
    });
  });
});

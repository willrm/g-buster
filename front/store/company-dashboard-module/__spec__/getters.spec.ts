import { CompanyDashboardModuleStateInterface } from '~/store/company-dashboard-module';
import { JobOfferEnums } from '../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { JobOfferResponseInterface } from '../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import getters from '../getters';

describe('store/company-dashboard-module/getters', () => {
  let state: CompanyDashboardModuleStateInterface;
  beforeEach(() => {
    state = {
      jobOffers: [] as JobOfferResponseInterface[],
    };
  });
  describe('getPublishedJobOffers()', () => {
    it('should return an empty array if there is no job offers', () => {
      // when
      const result: JobOfferResponseInterface[] = getters.getPublishedJobOffers(state);

      // then
      expect(result).toStrictEqual([]);
    });

    it('should return job offers with published status', () => {
      const expected: JobOfferResponseInterface[] = [
        { title: 'A published job offer 1', status: JobOfferEnums.JobOfferStatus.STATUS_PUBLISHED } as JobOfferResponseInterface,
        { title: 'A published job offer 2', status: JobOfferEnums.JobOfferStatus.STATUS_PUBLISHED } as JobOfferResponseInterface,
        { title: 'A published job offer 3', status: JobOfferEnums.JobOfferStatus.STATUS_PUBLISHED } as JobOfferResponseInterface,
      ];
      state.jobOffers = [
        ...expected,
        { title: 'A not published job offer 1', status: JobOfferEnums.JobOfferStatus.STATUS_DRAFT } as JobOfferResponseInterface,
        { title: 'A not published job offer 2', status: JobOfferEnums.JobOfferStatus.STATUS_PENDING_VALIDATION } as JobOfferResponseInterface,
        { title: 'A not published job offer 3', status: JobOfferEnums.JobOfferStatus.STATUS_ARCHIVED } as JobOfferResponseInterface,
      ];

      // when
      const result: JobOfferResponseInterface[] = getters.getPublishedJobOffers(state);

      // then
      expect(result).toStrictEqual(expected);
    });
  });

  describe('getArchivedJobOffers()', () => {
    it('should return an empty array if there is no job offers', () => {
      // when
      const result: JobOfferResponseInterface[] = getters.getArchivedJobOffers(state);

      // then
      expect(result).toStrictEqual([]);
    });

    it('should return job offers with archived status', () => {
      const expected: JobOfferResponseInterface[] = [
        { title: 'A published job offer 1', status: JobOfferEnums.JobOfferStatus.STATUS_ARCHIVED } as JobOfferResponseInterface,
        { title: 'A published job offer 2', status: JobOfferEnums.JobOfferStatus.STATUS_ARCHIVED } as JobOfferResponseInterface,
        { title: 'A published job offer 3', status: JobOfferEnums.JobOfferStatus.STATUS_ARCHIVED } as JobOfferResponseInterface,
      ];
      state.jobOffers = [
        ...expected,
        { title: 'A not published job offer 1', status: JobOfferEnums.JobOfferStatus.STATUS_DRAFT } as JobOfferResponseInterface,
        { title: 'A not published job offer 2', status: JobOfferEnums.JobOfferStatus.STATUS_PENDING_VALIDATION } as JobOfferResponseInterface,
        { title: 'A not published job offer 3', status: JobOfferEnums.JobOfferStatus.STATUS_PUBLISHED } as JobOfferResponseInterface,
      ];

      // when
      const result: JobOfferResponseInterface[] = getters.getArchivedJobOffers(state);

      // then
      expect(result).toStrictEqual(expected);
    });
  });

  describe('getPendingValidationJobOffers()', () => {
    it('should return an empty array if there is no job offers', () => {
      // when
      const result: JobOfferResponseInterface[] = getters.getPendingValidationJobOffers(state);

      // then
      expect(result).toStrictEqual([]);
    });

    it('should return job offers with pending validation status', () => {
      const expected: JobOfferResponseInterface[] = [
        { title: 'A published job offer 1', status: JobOfferEnums.JobOfferStatus.STATUS_PENDING_VALIDATION } as JobOfferResponseInterface,
        { title: 'A published job offer 2', status: JobOfferEnums.JobOfferStatus.STATUS_PENDING_VALIDATION } as JobOfferResponseInterface,
        { title: 'A published job offer 3', status: JobOfferEnums.JobOfferStatus.STATUS_PENDING_VALIDATION } as JobOfferResponseInterface,
      ];
      state.jobOffers = [
        ...expected,
        { title: 'A not published job offer 1', status: JobOfferEnums.JobOfferStatus.STATUS_DRAFT } as JobOfferResponseInterface,
        { title: 'A not published job offer 2', status: JobOfferEnums.JobOfferStatus.STATUS_ARCHIVED } as JobOfferResponseInterface,
        { title: 'A not published job offer 3', status: JobOfferEnums.JobOfferStatus.STATUS_PUBLISHED } as JobOfferResponseInterface,
      ];

      // when
      const result: JobOfferResponseInterface[] = getters.getPendingValidationJobOffers(state);

      // then
      expect(result).toStrictEqual(expected);
    });
  });

  describe('getDraftJobOffers()', () => {
    it('should return an empty array if there is no job offers', () => {
      // when
      const result: JobOfferResponseInterface[] = getters.getDraftJobOffers(state);

      // then
      expect(result).toStrictEqual([]);
    });

    it('should return job offers with draft status', () => {
      const expected: JobOfferResponseInterface[] = [
        { title: 'A published job offer 1', status: JobOfferEnums.JobOfferStatus.STATUS_DRAFT } as JobOfferResponseInterface,
        { title: 'A published job offer 2', status: JobOfferEnums.JobOfferStatus.STATUS_DRAFT } as JobOfferResponseInterface,
        { title: 'A published job offer 3', status: JobOfferEnums.JobOfferStatus.STATUS_DRAFT } as JobOfferResponseInterface,
      ];
      state.jobOffers = [
        ...expected,
        { title: 'A not published job offer 1', status: JobOfferEnums.JobOfferStatus.STATUS_PENDING_VALIDATION } as JobOfferResponseInterface,
        { title: 'A not published job offer 2', status: JobOfferEnums.JobOfferStatus.STATUS_ARCHIVED } as JobOfferResponseInterface,
        { title: 'A not published job offer 3', status: JobOfferEnums.JobOfferStatus.STATUS_PUBLISHED } as JobOfferResponseInterface,
      ];

      // when
      const result: JobOfferResponseInterface[] = getters.getDraftJobOffers(state);

      // then
      expect(result).toStrictEqual(expected);
    });
  });
});

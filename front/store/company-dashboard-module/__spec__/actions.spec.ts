import { ActionContext } from 'vuex';
import ApiService from '~/services/api.service';
import { CompanyDashboardModuleStateInterface } from '~/store/company-dashboard-module';
import { JobOfferResponseInterface } from '../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import actions, { GetJobOffersActionPayload } from '../actions';

describe('store/company-dashboard-module/actions', () => {
  let mockActionContext: ActionContext<CompanyDashboardModuleStateInterface, object>;
  let $apiService: ApiService;
  let getUserJobOffersMock: jest.Mock;

  beforeEach(() => {
    mockActionContext = {} as ActionContext<CompanyDashboardModuleStateInterface, object>;
    mockActionContext.commit = jest.fn();
    mockActionContext.dispatch = jest.fn();

    getUserJobOffersMock = jest.fn();

    $apiService = ({
      getUserJobOffers: getUserJobOffersMock,
    } as unknown) as ApiService;
  });

  describe('getUserJobOffers()', () => {
    it('should call api to get job offers', async () => {
      // given
      const actionPayload: GetJobOffersActionPayload = { apiService: $apiService };

      // when
      await actions.getUserJobOffers(mockActionContext, actionPayload);

      // then
      expect($apiService.getUserJobOffers).toHaveBeenCalledWith();
    });

    it('should make a mutation in store', async () => {
      // given
      const actionPayload: GetJobOffersActionPayload = { apiService: $apiService };
      const expected: JobOfferResponseInterface[] = [{ title: 'A job offer' } as JobOfferResponseInterface];
      getUserJobOffersMock.mockReturnValue(expected);

      // when
      await actions.getUserJobOffers(mockActionContext, actionPayload);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('SET_JOB_OFFERS', expected);
    });
  });

  describe('reset()', () => {
    it('should make a mutation in store', () => {
      // when
      actions.reset(mockActionContext);

      // then
      expect(mockActionContext.commit).toHaveBeenCalledWith('RESET');
    });
  });
});

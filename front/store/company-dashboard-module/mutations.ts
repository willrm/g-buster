import Vue from 'vue';
import { CompanyDashboardModuleStateInterface } from '~/store/company-dashboard-module/index';
import { JobOfferResponseInterface } from '../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import state from './state';

export default {
  SET_JOB_OFFERS(currentState: CompanyDashboardModuleStateInterface, jobOffers: JobOfferResponseInterface[]): void {
    Vue.set(currentState, 'jobOffers', jobOffers);
  },

  RESET(currentState: CompanyDashboardModuleStateInterface): void {
    Object.assign(currentState, state());
  },
};

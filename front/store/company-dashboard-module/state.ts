import { CompanyDashboardModuleStateInterface } from '~/store/company-dashboard-module/index';

export default () =>
  ({
    jobOffers: [],
  } as CompanyDashboardModuleStateInterface);

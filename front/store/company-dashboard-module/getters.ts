import { CompanyDashboardModuleStateInterface } from '~/store/company-dashboard-module/index';
import { JobOfferEnums } from '../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { JobOfferResponseInterface } from '../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';

export default {
  getPublishedJobOffers(state: CompanyDashboardModuleStateInterface): JobOfferResponseInterface[] {
    return state.jobOffers.filter((jobOffer: JobOfferResponseInterface) => JobOfferEnums.JobOfferStatus.STATUS_PUBLISHED === jobOffer.status);
  },
  getArchivedJobOffers(state: CompanyDashboardModuleStateInterface): JobOfferResponseInterface[] {
    return state.jobOffers.filter((jobOffer: JobOfferResponseInterface) => JobOfferEnums.JobOfferStatus.STATUS_ARCHIVED === jobOffer.status);
  },
  getPendingValidationJobOffers(state: CompanyDashboardModuleStateInterface): JobOfferResponseInterface[] {
    return state.jobOffers.filter(
      (jobOffer: JobOfferResponseInterface) => JobOfferEnums.JobOfferStatus.STATUS_PENDING_VALIDATION === jobOffer.status
    );
  },
  getDraftJobOffers(state: CompanyDashboardModuleStateInterface): JobOfferResponseInterface[] {
    return state.jobOffers.filter((jobOffer: JobOfferResponseInterface) => JobOfferEnums.JobOfferStatus.STATUS_DRAFT === jobOffer.status);
  },
};

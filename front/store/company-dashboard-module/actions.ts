import { ActionContext } from 'vuex';
import ApiService from '~/services/api.service';
import { CompanyDashboardModuleStateInterface } from '~/store/company-dashboard-module/index';
import { JobOfferResponseInterface } from '../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';

export interface GetJobOffersActionPayload {
  apiService: ApiService;
}

export default {
  async getUserJobOffers(
    actionContext: ActionContext<CompanyDashboardModuleStateInterface, object>,
    actionPayload: GetJobOffersActionPayload
  ): Promise<void> {
    const jobOffers: JobOfferResponseInterface[] = await actionPayload.apiService.getUserJobOffers();

    return Promise.resolve(actionContext.commit('SET_JOB_OFFERS', jobOffers));
  },

  reset(actionContext: ActionContext<CompanyDashboardModuleStateInterface, object>): Promise<void> {
    return Promise.resolve(actionContext.commit('RESET'));
  },
};

import { JobOfferResponseInterface } from '../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import state from './state';

export interface CompanyDashboardModuleStateInterface {
  jobOffers: JobOfferResponseInterface[];
}

export default { state };

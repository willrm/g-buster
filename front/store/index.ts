import { Context } from '@nuxt/types';
import { ActionContext, ActionTree } from 'vuex';
import { CompanyCreateModuleStateInterface } from '~/store/company-create-module';
import { CompanyDashboardModuleStateInterface } from '~/store/company-dashboard-module';
import { JobOfferCreateModuleStateInterface } from '~/store/job-offer-create-module';
import { JobOffersModuleStateInterface } from '~/store/job-offers-module';
import { LayoutModuleStateInterface } from '~/store/layout-module';
import { UserProfileModuleStateInterface } from '~/store/user-profile-module';
import { sanitizeLang } from '~/utils/lang.utils';
import { CommonTypeAliases } from '../../back/src/infrastructure/rest/common-models/common-type-aliases';

export interface RootStateInterface {
  'layout-module': LayoutModuleStateInterface;
  'company-create-module': CompanyCreateModuleStateInterface;
  'job-offer-create-module': JobOfferCreateModuleStateInterface;
  'company-dashboard-module': CompanyDashboardModuleStateInterface;
  'job-offers-module': JobOffersModuleStateInterface;
  'user-profile-module': UserProfileModuleStateInterface;
}

export const actions: ActionTree<RootStateInterface, object> = {
  async nuxtServerInit(actionContext: ActionContext<RootStateInterface, object>, context: Context): Promise<void> {
    const lang: CommonTypeAliases.Lang = sanitizeLang(context.route.params.lang);
    await actionContext.dispatch('layout-module/setLayout', { apiService: context.app.$apiService, params: { lang } });
    await actionContext.dispatch('user-profile-module/setUserProfile', { apiService: context.app.$apiService });
  },
};

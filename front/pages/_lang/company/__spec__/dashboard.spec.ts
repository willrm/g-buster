import { shallowMount, Stubs, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import CompanyDashboard from '../dashboard.vue';

describe('pages/_lang/company/dashboard', () => {
  let $t: jest.Mock;
  let stubs: Stubs;

  beforeEach(() => {
    $t = jest.fn();
    stubs = { 'nuxt-child': true };
  });

  describe('layout', () => {
    it('should have company-dashboard layout', () => {
      // @ts-ignore
      expect(CompanyDashboard.options.layout).toBe('company-dashboard');
    });
  });

  describe('middleware', () => {
    it('should use redirectToPage middleware', () => {
      // when
      const companyDashboardWrapper: Wrapper<Vue> = shallowMount(CompanyDashboard, { stubs, mocks: { $t } });

      // then
      expect(companyDashboardWrapper.vm.$options.middleware).toBe('redirectToPage.middleware');
    });
  });

  describe('banner', () => {
    beforeEach(() => {
      $t.mockReturnValue('Job offers');
    });
    it('should contain banner', () => {
      // given
      const companyDashboardWrapper: Wrapper<Vue> = shallowMount(CompanyDashboard, { stubs, mocks: { $t } });

      // when
      const result: Wrapper<Vue> = companyDashboardWrapper.find('.company-dashboard--banner');

      // then
      expect($t).toHaveBeenCalledWith('companyDashboard.jobOffers');
      expect(result.text()).toBe('Job offers');
    });
  });
});

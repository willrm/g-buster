import { Context } from '@nuxt/types';
import { createLocalVue, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import Vuex, { ModuleTree, Store } from 'vuex';
import GbJobOffersArchived from '~/components/company/dashboard/gb-job-offers-archived.vue';
import GbJobOffersDraft from '~/components/company/dashboard/gb-job-offers-draft.vue';
import GbJobOffersPendingValidation from '~/components/company/dashboard/gb-job-offers-pending-validation.vue';
import GbJobOffersPublished from '~/components/company/dashboard/gb-job-offers-published.vue';
import ApiService from '~/services/api.service';
import { RootStateInterface } from '~/store';
import { JobOfferResponseInterface } from '../../../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import DashboardJobOffers from '../job-offers.vue';

describe('pages/_lang/company/dashboard/job-offers', () => {
  let localVue: typeof Vue;
  let $t: jest.Mock;

  let ctx: Context;
  let getPublishedJobOffersMock: jest.Mock;
  let getArchivedJobOffersMock: jest.Mock;
  let getPendingValidationJobOffersMock: jest.Mock;
  let getDraftJobOffersMock: jest.Mock;

  let dispatchMock: jest.Mock;

  let resetMock: jest.Mock;

  let $apiService: ApiService;

  let store: Store<RootStateInterface>;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
  });

  beforeEach(() => {
    $apiService = {} as ApiService;
    $t = jest.fn();
    ctx = {
      app: {},
    } as Context;

    getPublishedJobOffersMock = jest.fn();
    getArchivedJobOffersMock = jest.fn();
    getPendingValidationJobOffersMock = jest.fn();
    getDraftJobOffersMock = jest.fn();

    dispatchMock = jest.fn();

    resetMock = jest.fn();

    store = new Vuex.Store({
      modules: {
        'company-dashboard-module': {
          namespaced: true,
          getters: {
            getPublishedJobOffers: getPublishedJobOffersMock,
            getArchivedJobOffers: getArchivedJobOffersMock,
            getPendingValidationJobOffers: getPendingValidationJobOffersMock,
            getDraftJobOffers: getDraftJobOffersMock,
          },
          actions: {
            reset: resetMock,
          },
        },
      } as ModuleTree<RootStateInterface>,
    });
  });

  describe('job offers header', () => {
    it('should contain a job offers header', () => {
      // given
      $t.mockReturnValueOnce('Job Offer Title')
        .mockReturnValueOnce('Publication')
        .mockReturnValueOnce('Location')
        .mockReturnValueOnce('Applicants')
        .mockReturnValueOnce('Days left')
        .mockReturnValueOnce('Visibility');
      const dashboardJobOffersWrapper: Wrapper<Vue> = shallowMount(DashboardJobOffers, { localVue, store, mocks: { $t } });

      // when
      const result: Wrapper<Vue> = dashboardJobOffersWrapper.find('.dashboard-job-offers--header');

      // then
      expect(result.exists()).toBe(true);

      expect($t).toHaveBeenNthCalledWith(1, 'companyDashboardJobOffers.jobOfferTitle');
      expect($t).toHaveBeenNthCalledWith(2, 'companyDashboardJobOffers.publication');
      expect($t).toHaveBeenNthCalledWith(3, 'companyDashboardJobOffers.location');
      expect($t).toHaveBeenNthCalledWith(4, 'companyDashboardJobOffers.applicants');
      expect($t).toHaveBeenNthCalledWith(5, 'companyDashboardJobOffers.daysLeft');
      expect($t).toHaveBeenNthCalledWith(6, 'companyDashboardJobOffers.visibility');
    });
  });

  describe('asyncData()', () => {
    it('should call store action to retrieve job offers asynchronously', async () => {
      // given
      ctx.app.$apiService = $apiService;
      ctx.store = ({ dispatch: dispatchMock } as unknown) as Store<unknown>;
      const dashboardJobOffersWrapper: Wrapper<Vue> = shallowMount(DashboardJobOffers, { localVue, store, mocks: { $t } });

      // when
      // @ts-ignore
      dashboardJobOffersWrapper.vm.$options.asyncData(ctx);

      // then
      expect(dispatchMock).toHaveBeenCalledWith('company-dashboard-module/getUserJobOffers', { apiService: $apiService });
    });
  });

  describe('published job offers', () => {
    it('should display gb job offer published component with job offers from getter', () => {
      // given
      const expected: JobOfferResponseInterface = { title: 'A job offer title' } as JobOfferResponseInterface;
      getPublishedJobOffersMock.mockReturnValue(expected);

      // when
      const dashboardJobOffersWrapper: Wrapper<Vue> = shallowMount(DashboardJobOffers, { localVue, store, mocks: { $t } });

      // then
      // @ts-ignore
      expect(dashboardJobOffersWrapper.find(GbJobOffersPublished).vm.jobOffers).toStrictEqual(expected);
    });
  });

  describe('draft job offers', () => {
    it('should display gb job offer draft component with job offers from getter', () => {
      // given
      const expected: JobOfferResponseInterface = { title: 'A job offer title' } as JobOfferResponseInterface;
      getDraftJobOffersMock.mockReturnValue(expected);

      // when
      const dashboardJobOffersWrapper: Wrapper<Vue> = shallowMount(DashboardJobOffers, { localVue, store, mocks: { $t } });

      // then
      // @ts-ignore
      expect(dashboardJobOffersWrapper.find(GbJobOffersDraft).vm.jobOffers).toStrictEqual(expected);
    });
  });

  describe('pending validation job offers', () => {
    it('should display gb job offer pending validation component with job offers from getter', () => {
      // given
      const expected: JobOfferResponseInterface = { title: 'A job offer title' } as JobOfferResponseInterface;
      getPendingValidationJobOffersMock.mockReturnValue(expected);

      // when
      const dashboardJobOffersWrapper: Wrapper<Vue> = shallowMount(DashboardJobOffers, { localVue, store, mocks: { $t } });

      // then
      // @ts-ignore
      expect(dashboardJobOffersWrapper.find(GbJobOffersPendingValidation).vm.jobOffers).toStrictEqual(expected);
    });
  });

  describe('archived job offers', () => {
    it('should display gb job offer archived component with job offers from getter', () => {
      // given
      const expected: JobOfferResponseInterface = { title: 'A job offer title' } as JobOfferResponseInterface;
      getArchivedJobOffersMock.mockReturnValue(expected);

      // when
      const dashboardJobOffersWrapper: Wrapper<Vue> = shallowMount(DashboardJobOffers, { localVue, store, mocks: { $t } });

      // then
      // @ts-ignore
      expect(dashboardJobOffersWrapper.find(GbJobOffersArchived).vm.jobOffers).toStrictEqual(expected);
    });
  });

  describe('destroyed hook', () => {
    it('should call reset action from store', () => {
      // given
      const dashboardJobOffersWrapper: Wrapper<Vue> = shallowMount(DashboardJobOffers, { localVue, store, mocks: { $t } });

      // when
      dashboardJobOffersWrapper.destroy();

      // then
      expect(resetMock).toHaveBeenCalled();
    });
  });
});

import { createLocalVue, mount, shallowMount, Stubs, Wrapper } from '@vue/test-utils';
import Vue, { VueConstructor } from 'vue';
import { TranslateResult } from 'vue-i18n';
import Vuex, { ModuleTree, Store } from 'vuex';
import GbActionBar from '~/components/base-components/gb-action-bar.vue';
import GbBreadcrumb from '~/components/base-components/gb-breadcrumb.vue';
import GbButton from '~/components/base-components/gb-button.vue';
import GbErrorsSummary from '~/components/base-components/gb-errors-summary.vue';
import GbCompanyCreateContent from '~/components/company/create/gb-company-create-content.vue';
import GbCompanyCreateHeader from '~/components/company/create/gb-company-create-header.vue';
import ApiService from '~/services/api.service';
import { RootStateInterface } from '~/store';
import {
  PostCompanyRequestInterface,
  PostValueRequestInterface,
} from '../../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { DomainErrorsResponseInterface } from '../../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import { TemporaryFileResponseInterface } from '../../../../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';
import CreateCompany from '../create.vue';

describe('pages/_lang/company/profile/create', () => {
  let localVue: VueConstructor;

  let $apiService: ApiService;
  let $t: jest.Mock;
  let $router: object;
  let $route: { path: string };

  let store: Store<RootStateInterface>;
  let pushMock: jest.Mock;
  let setCompanyMock: jest.Mock;
  let setCompanyLogoMock: jest.Mock;
  let validateCompanyMock: jest.Mock;
  let getCompanyMock: jest.Mock;
  let getCompanyLogoMock: jest.Mock;
  let stubs: Stubs;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
  });

  beforeEach(() => {
    $apiService = {} as ApiService;

    $t = jest.fn();

    pushMock = jest.fn();
    $router = {
      push: pushMock,
    };
    $route = { path: '' };

    setCompanyMock = jest.fn();
    setCompanyLogoMock = jest.fn();
    validateCompanyMock = jest.fn();
    getCompanyMock = jest.fn();
    getCompanyLogoMock = jest.fn();

    store = new Vuex.Store({
      modules: {
        'company-create-module': {
          namespaced: true,
          getters: {
            getCompany: getCompanyMock,
            getCompanyLogo: getCompanyLogoMock,
          },
          actions: {
            setCompany: setCompanyMock,
            setCompanyLogo: setCompanyLogoMock,
            validateCompany: validateCompanyMock,
          },
        },
      } as ModuleTree<RootStateInterface>,
    });

    stubs = { 'client-only': true, modal: true, 'file-upload': true };
  });

  describe('middleware', () => {
    it('should use authenticatedAsCompany middleware', () => {
      // when
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t } });

      // then
      expect(createCompanyWrapper.vm.$options.middleware).toBe('authenticatedAsCompany.middleware');
    });
  });

  describe('data()', () => {
    it('should initialize data with default values', () => {
      // given
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t } });

      // when
      const result: Record<string, unknown> = createCompanyWrapper.vm.$data;

      // then
      expect(result).toStrictEqual({
        domainErrors: {},
        breadcrumb: [
          {
            i18nLabel: 'company.breadcrumbCreateLabel',
            path: 'company/profile/create',
          },
          {
            i18nLabel: 'company.breadcrumbPreviewLabel',
            path: 'company/profile/preview',
          },
        ],
      });
    });
  });

  describe('errors summary', () => {
    it('should not be rendered if there is no domain errors', () => {
      // when
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t } });

      // then
      expect(createCompanyWrapper.find(GbErrorsSummary).exists()).toBeFalsy();
    });

    it('should be rendered if there is domain errors', async () => {
      // given
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t } });

      // when
      createCompanyWrapper.setData({ domainErrors: { name: { messages: ['A message'] } } });
      await createCompanyWrapper.vm.$nextTick();

      // then
      expect(createCompanyWrapper.find(GbErrorsSummary).exists()).toBeTruthy();
    });

    it('should have options set', async () => {
      // given
      const expected: DomainErrorsResponseInterface = { name: { messages: ['A message'], i18nMessageKeys: ['aMessage'] } };
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t } });

      // when
      createCompanyWrapper.setData({ domainErrors: expected });
      await createCompanyWrapper.vm.$nextTick();

      // then
      // @ts-ignore
      expect(createCompanyWrapper.find(GbErrorsSummary).vm.options).toStrictEqual({
        domainI18nPath: 'company',
        domainErrors: expected,
      });
    });
  });

  describe('company header', () => {
    it('should bind company property to GbCompanyCreateHeader input prop', () => {
      // given
      const expectedCompany: PostCompanyRequestInterface = { name: 'Company name' } as PostCompanyRequestInterface;
      const expectedCompanyLogo: TemporaryFileResponseInterface = { base64: 'aBase64Image' } as TemporaryFileResponseInterface;
      getCompanyMock.mockReturnValue(expectedCompany);
      getCompanyLogoMock.mockReturnValue(expectedCompanyLogo);

      // when
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t } });

      // then
      // @ts-ignore
      expect(createCompanyWrapper.find(GbCompanyCreateHeader).vm.company).toStrictEqual(expectedCompany);
      // @ts-ignore
      expect(createCompanyWrapper.find(GbCompanyCreateHeader).vm.companyLogo).toStrictEqual(expectedCompanyLogo);
    });

    it('should set GbCompanyCreateHeader options prop using domainErrors', () => {
      // given
      const domainErrors: DomainErrorsResponseInterface = {
        title: {
          messages: ['message'],
          i18nMessageKeys: ['i18n.key'],
        },
      };
      const data = () => ({ domainErrors });

      // when
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t }, data });

      // then
      // @ts-ignore
      expect(createCompanyWrapper.find(GbCompanyCreateHeader).vm.options).toStrictEqual({ domainErrors });
    });

    it('should call setCompany when input is triggered', () => {
      // given
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t } });

      const companyCreateHeader: Wrapper<Vue> = createCompanyWrapper.find(GbCompanyCreateHeader);
      const inputExpected: { fieldName: string; fieldValue: string } = {
        fieldName: 'name',
        fieldValue: 'A company name',
      };

      // when
      companyCreateHeader.vm.$emit('input', inputExpected);

      // then
      expect(setCompanyMock).toHaveBeenCalled();
      expect(setCompanyMock.mock.calls[0][1]).toMatchObject(inputExpected);
    });

    it('should call setCompanyLogo when input-logo is triggered', () => {
      // given
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t } });

      const companyCreateHeader: Wrapper<Vue> = createCompanyWrapper.find(GbCompanyCreateHeader);
      const inputExpected: TemporaryFileResponseInterface = { uuid: 'an-uuid' } as TemporaryFileResponseInterface;

      // when
      companyCreateHeader.vm.$emit('input-logo', inputExpected);

      // then
      expect(setCompanyLogoMock).toHaveBeenCalled();
      expect(setCompanyLogoMock.mock.calls[0][1]).toMatchObject(inputExpected);
    });
  });

  describe('company content', () => {
    it('should bind company property to GbCompanyCreateContent value', () => {
      // when
      const expected: PostCompanyRequestInterface = {
        name: 'Company name',
        socialNetworks: {},
        values: [] as PostValueRequestInterface[],
      } as PostCompanyRequestInterface;
      getCompanyMock.mockReturnValue(expected);

      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t } });

      // then
      // @ts-ignore
      expect(createCompanyWrapper.find(GbCompanyCreateContent).vm.value).toStrictEqual(expected);
    });

    it('should call setCompany when input is triggered', () => {
      // given
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t } });

      const companyCreateContent: Wrapper<Vue> = createCompanyWrapper.find(GbCompanyCreateContent);
      const inputExpected: { fieldName: string; fieldValue: string } = {
        fieldName: 'name',
        fieldValue: 'A company name',
      };

      // when
      companyCreateContent.vm.$emit('input', inputExpected);

      // then
      expect(setCompanyMock).toHaveBeenCalled();
      expect(setCompanyMock.mock.calls[0][1]).toMatchObject(inputExpected);
    });
  });

  describe('breadcrumb', () => {
    it('should display GbBreadcrumb in GbActionBar', () => {
      // when
      const company: PostCompanyRequestInterface = ({
        name: 'Company name',
        activityLocations: {},
        geniusTypes: [],
        socialNetworks: {},
        values: [],
      } as unknown) as PostCompanyRequestInterface;
      const companyLogo: TemporaryFileResponseInterface = { base64: 'aBase64Image' } as TemporaryFileResponseInterface;
      getCompanyMock.mockReturnValue(company);
      getCompanyLogoMock.mockReturnValue(companyLogo);
      const createCompanyWrapper: Wrapper<Vue> = mount(CreateCompany, { stubs, store, localVue, mocks: { $route, $apiService, $t } });
      const gbActionBarWrapper: Wrapper<Vue> = createCompanyWrapper.find(GbActionBar);

      // then
      // @ts-ignore
      expect(gbActionBarWrapper.find(GbBreadcrumb).vm.options).toStrictEqual({
        backgroundColor: 'COMPANY_BACKGROUND_COLOR',
        isLeftBorderRounded: false,
        steps: [
          {
            i18nLabel: 'company.breadcrumbCreateLabel',
            path: 'company/profile/create',
          },
          {
            i18nLabel: 'company.breadcrumbPreviewLabel',
            path: 'company/profile/preview',
          },
        ],
      });
    });
  });

  describe('submit button', () => {
    beforeEach(() => {
      $t.mockReturnValue('Preview');
    });

    it('should display submit button in GbActionBar', () => {
      // when
      const company: PostCompanyRequestInterface = ({
        name: 'Company name',
        activityLocations: {},
        geniusTypes: [],
        socialNetworks: {},
        values: [],
      } as unknown) as PostCompanyRequestInterface;
      const companyLogo: TemporaryFileResponseInterface = { base64: 'aBase64Image' } as TemporaryFileResponseInterface;
      getCompanyMock.mockReturnValue(company);
      getCompanyLogoMock.mockReturnValue(companyLogo);
      const createCompanyWrapper: Wrapper<Vue> = mount(CreateCompany, { stubs, store, localVue, mocks: { $route, $apiService, $t } });
      const gbActionBarWrapper: Wrapper<Vue> = createCompanyWrapper.find(GbActionBar);

      // then
      // @ts-ignore
      expect(gbActionBarWrapper.find(GbButton).vm.options).toStrictEqual({
        buttonLabelKey: 'preview',
        buttonType: 'submit',
      });
    });

    it('should call action from store to validate a company when submitting form', () => {
      // given
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t } });

      // when
      createCompanyWrapper.find('form').trigger('submit');

      // then
      expect(validateCompanyMock.mock.calls[0][1]).toMatchObject({ apiService: $apiService });
    });

    it('should call router to go to preview when no validation error', async () => {
      // given
      (validateCompanyMock as jest.Mock).mockReturnValueOnce(Promise.resolve({ name: 'Company X' }));
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t, $router } });

      // when
      createCompanyWrapper.find('form').trigger('submit');
      await nextTicks(createCompanyWrapper, 2);

      // then
      expect(pushMock).toHaveBeenCalledWith('preview');
    });

    it('should not call router to go anywhere when validation error', async () => {
      // given
      (validateCompanyMock as jest.Mock).mockReturnValueOnce(
        Promise.reject({
          domainErrors: { bachelorsOfEngineeringNumber: {} },
        })
      );
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t, $router } });

      // when
      createCompanyWrapper.find('form').trigger('submit');
      await createCompanyWrapper.vm.$nextTick();

      // then
      expect(pushMock).not.toHaveBeenCalled();
    });

    it('should clean domain errors when submitting form successfully after having an error on first submission', async () => {
      // given
      (validateCompanyMock as jest.Mock).mockReturnValueOnce(
        Promise.reject({
          domainErrors: { bachelorsOfEngineeringNumber: {} },
        })
      );
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t, $router } });
      createCompanyWrapper.find('form').trigger('submit');
      await nextTicks(createCompanyWrapper, 2);

      // when
      createCompanyWrapper.find('form').trigger('submit');
      await nextTicks(createCompanyWrapper, 2);

      // then
      expect(createCompanyWrapper.vm.$data.domainErrors).toStrictEqual({});
    });

    it('should fill domain errors data when errors', async () => {
      // given
      const expected: DomainErrorsResponseInterface = {
        bachelorsOfEngineeringNumber: {
          messages: ['A bachelors of engineering number error'],
          i18nMessageKeys: ['company.aBachelorsOfEngineeringNumberError'],
        },
      };
      (validateCompanyMock as jest.Mock).mockReturnValue(
        Promise.reject({
          domainErrors: expected,
        })
      );
      $t.mockReturnValue('Company creation failed' as TranslateResult);
      const createCompanyWrapper: Wrapper<Vue> = shallowMount(CreateCompany, { store, localVue, mocks: { $apiService, $t } });

      // when
      createCompanyWrapper.find('form').trigger('submit');
      await nextTicks(createCompanyWrapper, 2);

      // then
      expect(createCompanyWrapper.vm.$data).toHaveProperty('domainErrors', expected);
    });
  });
});

const nextTicks = async (wrapper: Wrapper<Vue>, numberOfTicksToExecute: number): Promise<void> => {
  for (let i: number = 0; i < numberOfTicksToExecute; i++) {
    await wrapper.vm.$nextTick();
  }
};

import { createLocalVue, mount, shallowMount, Wrapper } from '@vue/test-utils';
import Vue, { VueConstructor } from 'vue';
import Vuex, { ModuleTree, Store } from 'vuex';
import GbButton from '~/components/base-components/gb-button.vue';
import GbConfirmationCard from '~/components/base-components/gb-confirmation-card.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { RootStateInterface } from '~/store';
import { CompanyCreateModuleStateInterface } from '~/store/company-create-module';
import {
  PostCompanyRequestInterface,
  PostValueRequestInterface,
} from '../../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { TemporaryFileResponseInterface } from '../../../../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';
import CreationConfirmationCompany from '../creation-confirmation.vue';

describe('pages/_lang/company/profile/creation-confirmation', () => {
  let localVue: VueConstructor;

  let $t: jest.Mock;
  let $router: object;

  let pushMock: jest.Mock;
  let resetMock: jest.Mock;

  let state: CompanyCreateModuleStateInterface;
  let store: Store<RootStateInterface>;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
  });

  beforeEach(() => {
    $t = jest.fn();

    pushMock = jest.fn();
    $router = {
      push: pushMock,
    };

    resetMock = jest.fn();

    state = {
      company: {
        socialNetworks: {},
        activityLocations: {},
        values: [] as PostValueRequestInterface[],
      } as PostCompanyRequestInterface,
      companyLogo: {} as TemporaryFileResponseInterface,
    } as CompanyCreateModuleStateInterface;

    store = new Vuex.Store({
      modules: {
        'company-create-module': {
          namespaced: true,
          state,
          actions: {
            reset: resetMock,
          },
        },
      } as ModuleTree<RootStateInterface>,
    });
  });

  describe('middleware', () => {
    it('should use authenticatedAsCompany middleware', () => {
      // when
      const creationConfirmationCompanyWrapper: Wrapper<Vue> = shallowMount(CreationConfirmationCompany, { mocks: { $t, $router } });

      // then
      expect(creationConfirmationCompanyWrapper.vm.$options.middleware).toBe('authenticatedAsCompany.middleware');
    });
  });

  it('should contain a GbConfirmationCard component', () => {
    // when
    const creationConfirmationCompanyWrapper: Wrapper<Vue> = shallowMount(CreationConfirmationCompany, { mocks: { $t, $router } });

    // then
    expect(creationConfirmationCompanyWrapper.findAll(GbConfirmationCard).length).toBe(1);
  });

  it('should display title', () => {
    // given
    $t.mockReturnValue('Creation confirmation title');

    // when
    const creationConfirmationCompanyWrapper: Wrapper<Vue> = mount(CreationConfirmationCompany, { mocks: { $t, $router } });

    // then
    expect($t).toHaveBeenCalledWith('creationConfirmation.title');
    expect(creationConfirmationCompanyWrapper.find('h2').text()).toBe('Creation confirmation title');
  });

  it('should display subtitle', () => {
    // given
    $t.mockReturnValue('Creation confirmation subtitle');

    // when
    const creationConfirmationCompanyWrapper: Wrapper<Vue> = mount(CreationConfirmationCompany, { mocks: { $t, $router } });

    // then
    expect($t).toHaveBeenCalledWith('creationConfirmation.companySubtitle');
    expect(
      creationConfirmationCompanyWrapper
        .findAll('p')
        .at(0)
        .text()
    ).toBe('Creation confirmation subtitle');
  });

  it('should display explanation', () => {
    // given
    $t.mockReturnValue('Creation confirmation explanation');

    // when
    const creationConfirmationCompanyWrapper: Wrapper<Vue> = mount(CreationConfirmationCompany, { mocks: { $t, $router } });

    // then
    expect($t).toHaveBeenCalledWith('creationConfirmation.explanation');
    expect(
      creationConfirmationCompanyWrapper
        .findAll('p')
        .at(1)
        .text()
    ).toBe('Creation confirmation explanation');
  });

  describe('job offer creation button', () => {
    it('should display GbButton component with props for job offer creation', () => {
      // given
      const expected: object = { buttonLabelKey: 'creationConfirmation.createJobOffer' };

      // when
      const creationConfirmationCompanyWrapper: Wrapper<Vue> = mount(CreationConfirmationCompany, { mocks: { $t, $router } });

      // then
      // @ts-ignore
      expect(creationConfirmationCompanyWrapper.findAll(GbButton).at(0).vm.options).toMatchObject(expected);
    });

    it('should call router to redirect to job offer create page when click on it', async () => {
      // given
      const creationConfirmationCompanyWrapper: Wrapper<Vue> = mount(CreationConfirmationCompany, { mocks: { $t, $router } });

      // when
      creationConfirmationCompanyWrapper
        .findAll(GbButton)
        .at(0)
        .trigger('click');
      await creationConfirmationCompanyWrapper.vm.$nextTick();

      // then
      expect(pushMock).toHaveBeenCalledWith('../job-offer/create');
    });
  });

  it('should display GbButton component with props for premium offer', () => {
    // given
    const expected: object = { buttonLabelKey: 'creationConfirmation.goToPremium' };

    // when
    const creationConfirmationCompanyWrapper: Wrapper<Vue> = mount(CreationConfirmationCompany, { mocks: { $t, $router } });

    // then
    // @ts-ignore
    expect(creationConfirmationCompanyWrapper.findAll(GbButton).at(1).vm.options).toStrictEqual(expected);
  });

  it('should display GbIcon component with props for validation icon', () => {
    // given
    const expected: object = { iconName: 'valid' };

    // when
    const creationConfirmationCompanyWrapper: Wrapper<Vue> = mount(CreationConfirmationCompany, { mocks: { $t, $router } });

    // then
    // @ts-ignore
    expect(creationConfirmationCompanyWrapper.find(GbIcon).vm.options).toStrictEqual(expected);
  });

  describe('destroyed hook', () => {
    it('should call reset action from store', () => {
      // given
      const previewCompanyWrapper: Wrapper<Vue> = mount(CreationConfirmationCompany, { localVue, store, mocks: { $t, $router } });

      // when
      previewCompanyWrapper.destroy();

      // then
      expect(resetMock).toHaveBeenCalled();
    });
  });
});

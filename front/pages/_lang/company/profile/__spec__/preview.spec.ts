import { createLocalVue, mount, shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue, { VueConstructor } from 'vue';
import Vuex, { ModuleTree, Store } from 'vuex';
import GbActionBar from '~/components/base-components/gb-action-bar.vue';
import GbBreadcrumb from '~/components/base-components/gb-breadcrumb.vue';
import GbButton from '~/components/base-components/gb-button.vue';
import GbCompanyPreviewContent from '~/components/company/preview/gb-company-preview-content.vue';
import GbCompanyPreviewHeader from '~/components/company/preview/gb-company-preview-header.vue';
import ApiService from '~/services/api.service';
import { RootStateInterface } from '~/store';
import { PostValueRequestInterface } from '../../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import PreviewCompany from '../preview.vue';

describe('pages/_lang/company/profile/preview', () => {
  let localVue: VueConstructor;

  let $t: jest.Mock;
  let $router: object;
  let $apiService: ApiService;
  let $route: { path: string };

  let pushMock: jest.Mock;
  let submitCompanyMock: jest.Mock;
  let getCompanyMock: jest.Mock;
  let getCompanyLogoMock: jest.Mock;

  let store: Store<RootStateInterface>;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
  });

  beforeEach(() => {
    $t = jest.fn();
    $apiService = {} as ApiService;

    pushMock = jest.fn();
    $router = {
      push: pushMock,
    };
    $route = { path: '' };

    submitCompanyMock = jest.fn();
    getCompanyMock = jest.fn();
    getCompanyLogoMock = jest.fn();

    store = new Vuex.Store({
      modules: {
        'company-create-module': {
          namespaced: true,
          getters: {
            getCompany: getCompanyMock,
            getCompanyLogo: getCompanyLogoMock,
          },
          actions: {
            submitCompany: submitCompanyMock,
          },
        },
      } as ModuleTree<RootStateInterface>,
    });

    getCompanyLogoMock.mockReturnValue({ base64: 'aBase64Image', id: 1, mimeType: 'aMimeType', uuid: 'an-uuid' });
    getCompanyMock.mockReturnValue({
      name: 'A company name',
      socialNetworks: {},
      activityLocations: {},
      values: [] as PostValueRequestInterface[],
    });
  });

  describe('middleware', () => {
    it('should use authenticatedAsCompany middleware', () => {
      // when
      const previewCompanyWrapper: Wrapper<Vue> = shallowMount(PreviewCompany, { localVue, store, mocks: { $t, $router, $apiService } });

      // then
      expect(previewCompanyWrapper.vm.$options.middleware).toBe('authenticatedAsCompany.middleware');
    });
  });

  describe('breadcrumb', () => {
    it('should display GbBreadcrumb in GbActionBar', () => {
      // when
      const previewCompanyWrapper: Wrapper<Vue> = mount(PreviewCompany, { localVue, store, mocks: { $route, $t, $router, $apiService } });
      const gbActionBarWrapper: Wrapper<Vue> = previewCompanyWrapper.find(GbActionBar);

      // then
      // @ts-ignore
      expect(gbActionBarWrapper.find(GbBreadcrumb).vm.options).toStrictEqual({
        backgroundColor: 'COMPANY_BACKGROUND_COLOR',
        isLeftBorderRounded: false,
        steps: [
          {
            i18nLabel: 'company.breadcrumbCreateLabel',
            path: 'company/profile/create',
          },
          {
            i18nLabel: 'company.breadcrumbPreviewLabel',
            path: 'company/profile/preview',
          },
        ],
      });
    });
  });

  describe('update button', () => {
    beforeEach(() => {
      $t.mockReturnValue('Update');
    });

    it('should display update button in GbActionBar', () => {
      // when
      const previewCompanyWrapper: Wrapper<Vue> = mount(PreviewCompany, { localVue, store, mocks: { $route, $t, $router, $apiService } });
      const gbActionBarWrapper: Wrapper<Vue> = previewCompanyWrapper.find(GbActionBar);

      // then
      const gbButtons: WrapperArray<Vue> = gbActionBarWrapper.findAll(GbButton).filter(
        (button: Wrapper<Vue>) =>
          // @ts-ignore
          button.vm.options.buttonLabelKey === 'update'
      );

      // @ts-ignore
      expect(gbButtons.at(0).vm.options).toMatchObject({
        buttonLabelKey: 'update',
        buttonType: 'button',
      });
    });

    it('should call router to redirect to create page', async () => {
      // given
      const previewCompanyWrapper: Wrapper<Vue> = mount(PreviewCompany, { localVue, store, mocks: { $route, $t, $router, $apiService } });

      // when
      const gbButtons: WrapperArray<Vue> = previewCompanyWrapper.findAll(GbButton).filter(
        (button: Wrapper<Vue>) =>
          // @ts-ignore
          button.vm.options.buttonLabelKey === 'update'
      );
      gbButtons.at(0).trigger('click');
      await previewCompanyWrapper.vm.$nextTick();

      // then
      expect(pushMock).toHaveBeenCalledWith('create');
    });
  });

  describe('validate button', () => {
    beforeEach(() => {
      $t.mockReturnValue('Validate');
    });

    it('should display validate button in GbActionBar', () => {
      // when
      const previewCompanyWrapper: Wrapper<Vue> = mount(PreviewCompany, { localVue, store, mocks: { $route, $t, $router, $apiService } });
      const gbActionBarWrapper: Wrapper<Vue> = previewCompanyWrapper.find(GbActionBar);

      // then
      const gbButtonWrapper: WrapperArray<Vue> = gbActionBarWrapper.findAll(GbButton).filter(
        (button: Wrapper<Vue>) =>
          // @ts-ignore
          button.vm.options.buttonLabelKey === 'validate'
      );

      // @ts-ignore
      expect(gbButtonWrapper.at(0).vm.options).toMatchObject({
        buttonLabelKey: 'validate',
        buttonType: 'button',
      });
    });

    it('should call action from store to submit company when clicking on validate button', () => {
      // given
      const previewCompanyWrapper: Wrapper<Vue> = mount(PreviewCompany, { localVue, store, mocks: { $route, $t, $router, $apiService } });
      const gbButtonWrapper: WrapperArray<Vue> = previewCompanyWrapper.findAll(GbButton).filter(
        (button: Wrapper<Vue>) =>
          // @ts-ignore
          button.vm.options.buttonLabelKey === 'validate'
      );

      // when
      gbButtonWrapper.at(0).trigger('click');

      // then
      expect(submitCompanyMock.mock.calls[0][1]).toMatchObject({ apiService: $apiService });
    });

    it('should call router to redirect to creation confirmation page', async () => {
      // given
      const previewCompanyWrapper: Wrapper<Vue> = mount(PreviewCompany, { localVue, store, mocks: { $route, $t, $router, $apiService } });
      const gbButtonWrapper: WrapperArray<Vue> = previewCompanyWrapper.findAll(GbButton).filter(
        (button: Wrapper<Vue>) =>
          // @ts-ignore
          button.vm.options.buttonLabelKey === 'validate'
      );

      // when
      gbButtonWrapper.at(0).trigger('click');
      await previewCompanyWrapper.vm.$nextTick();

      // then
      expect(pushMock).toHaveBeenCalledWith('creation-confirmation');
    });
  });

  describe('GbCompanyPreviewHeader', () => {
    it('should have component with company from store through getters', () => {
      // given
      getCompanyMock.mockReturnValue({ name: 'A company name' });
      const previewCompanyWrapper: Wrapper<Vue> = shallowMount(PreviewCompany, { localVue, store, mocks: { $t, $router, $apiService } });

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = previewCompanyWrapper.find(GbCompanyPreviewHeader);

      // then
      // @ts-ignore
      expect(gbCompanyPreviewHeaderWrapper.vm.company).toStrictEqual({
        name: 'A company name',
      });
    });

    it('should have component with companyLogo from store through getters', () => {
      // given
      getCompanyLogoMock.mockReturnValue({ base64: 'aBase64Image', id: 1, mimeType: 'aMimeType', uuid: 'an-uuid' });
      const previewCompanyWrapper: Wrapper<Vue> = shallowMount(PreviewCompany, { localVue, store, mocks: { $t, $router, $apiService } });

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = previewCompanyWrapper.find(GbCompanyPreviewHeader);

      // then
      // @ts-ignore
      expect(gbCompanyPreviewHeaderWrapper.vm.companyLogo).toStrictEqual({ base64: 'aBase64Image', id: 1, mimeType: 'aMimeType', uuid: 'an-uuid' });
    });
  });

  describe('GbCompanyPreviewContent', () => {
    it('should have component with company from store through getters', () => {
      // given
      getCompanyMock.mockReturnValue({ name: 'A company name' });
      const previewCompanyWrapper: Wrapper<Vue> = shallowMount(PreviewCompany, { localVue, store, mocks: { $t } });

      // when
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = previewCompanyWrapper.find(GbCompanyPreviewContent);

      // then
      // @ts-ignore
      expect(gbCompanyPreviewContentWrapper.vm.company).toStrictEqual({
        name: 'A company name',
      });
    });
  });
});

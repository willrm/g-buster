import { Context } from '@nuxt/types';
import { createLocalVue, mount, shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue, { VueConstructor } from 'vue';
import Vuex, { ModuleTree, Store } from 'vuex';
import GbActionBar from '~/components/base-components/gb-action-bar.vue';
import GbBreadcrumb from '~/components/base-components/gb-breadcrumb.vue';
import GbButton from '~/components/base-components/gb-button.vue';
import GbJobOfferPreviewHeader from '~/components/job-offer/preview/gb-job-offer-preview-header.vue';
import ApiService from '~/services/api.service';
import { RootStateInterface } from '~/store';
import { PutJobOfferRequestInterface } from '../../../../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import { CommonTypeAliases } from '../../../../../../../back/src/infrastructure/rest/common-models/common-type-aliases';
import PreviewJobOffer from '../_id.vue';

describe('pages/_lang/company/job-offer/preview/_id', () => {
  let localVue: VueConstructor;

  let $t: jest.Mock;
  let ctx: Context;
  let $router: object;
  let $route: { path: string; params: { id: string } };
  let $apiService: ApiService;

  let pushMock: jest.Mock;
  let submitJobOfferMock: jest.Mock;
  let getJobOfferMock: jest.Mock;

  let store: Store<RootStateInterface>;
  let dispatchMock: jest.Mock;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
  });

  beforeEach(() => {
    $t = jest.fn();
    $apiService = {} as ApiService;
    ctx = {
      app: {},
      params: {},
    } as Context;

    dispatchMock = jest.fn();

    pushMock = jest.fn();
    $router = {
      push: pushMock,
    };
    $route = { path: '', params: { id: '' } };
    submitJobOfferMock = jest.fn();
    getJobOfferMock = jest.fn();

    store = new Vuex.Store({
      modules: {
        'job-offer-create-module': {
          namespaced: true,
          getters: {
            getJobOffer: getJobOfferMock,
          },
          actions: {
            submitJobOffer: submitJobOfferMock,
          },
        },
      } as ModuleTree<RootStateInterface>,
    });

    getJobOfferMock.mockReturnValue({
      title: 'A job title',
    });
  });

  describe('middleware', () => {
    it('should use authenticatedAsCompany middleware', () => {
      // when
      const previewJobOfferWrapper: Wrapper<Vue> = shallowMount(PreviewJobOffer, { localVue, store, mocks: { $t, $router, $route, $apiService } });

      // then
      expect(previewJobOfferWrapper.vm.$options.middleware).toBe('authenticatedAsCompany.middleware');
    });
  });

  describe('asyncData()', () => {
    it('should call store action to retrieve draft job offer if there is an id in route', async () => {
      // given
      const id: CommonTypeAliases.JobOfferId = 'aJobOfferId';
      ctx.app.$apiService = $apiService;
      ctx.params = { id };
      ctx.store = ({ dispatch: dispatchMock } as unknown) as Store<unknown>;
      const previewJobOfferWrapper: Wrapper<Vue> = shallowMount(PreviewJobOffer, { localVue, store, mocks: { $t, $router, $route, $apiService } });

      // when
      // @ts-ignore
      previewJobOfferWrapper.vm.$options.asyncData(ctx);

      // then
      await expect(dispatchMock).toHaveBeenNthCalledWith(1, 'job-offer-create-module/setJobOfferId', id);
      await expect(dispatchMock).toHaveBeenNthCalledWith(2, 'job-offer-create-module/getJobOffer', { apiService: $apiService });
    });

    it('should not call store action to retrieve draft job offer if there is an id in route', async () => {
      // given
      ctx.app.$apiService = $apiService;
      ctx.params = {};
      ctx.store = ({ dispatch: dispatchMock } as unknown) as Store<unknown>;
      const previewJobOfferWrapper: Wrapper<Vue> = shallowMount(PreviewJobOffer, { localVue, store, mocks: { $t, $router, $route, $apiService } });

      // when
      // @ts-ignore
      previewJobOfferWrapper.vm.$options.asyncData(ctx);

      // then
      expect(dispatchMock).not.toHaveBeenCalled();
      expect(dispatchMock).not.toHaveBeenCalled();
    });
  });

  describe('breadcrumb', () => {
    it('should display GbBreadcrumb in GbActionBar', () => {
      // when
      const jobOfferMock: PutJobOfferRequestInterface = ({
        title: 'A job offer title',
        annualSalaryRange: {},
        advantages: {},
        traveling: {},
        targetCustomers: [],
        requiredExperiences: [],
        requestedSpecialities: [],
        wantedPersonality: [],
      } as unknown) as PutJobOfferRequestInterface;
      getJobOfferMock.mockReturnValue(jobOfferMock);
      const previewJobOfferWrapper: Wrapper<Vue> = mount(PreviewJobOffer, { localVue, store, mocks: { $t, $router, $route, $apiService } });
      const gbActionBarWrapper: Wrapper<Vue> = previewJobOfferWrapper.find(GbActionBar);

      // then
      // @ts-ignore
      expect(gbActionBarWrapper.find(GbBreadcrumb).vm.options).toStrictEqual({
        backgroundColor: 'COMPANY_BACKGROUND_COLOR',
        isLeftBorderRounded: false,
        steps: [
          {
            i18nLabel: 'jobOffer.breadcrumbCreateLabel',
            path: 'job-offer/create',
          },
          {
            i18nLabel: 'jobOffer.breadcrumbPreviewLabel',
            path: 'job-offer/preview',
          },
        ],
      });
    });
  });

  describe('update button', () => {
    beforeEach(() => {
      $t.mockReturnValue('Update');
    });

    it('should display update button in GbActionBar', () => {
      // when
      const previewJobOfferWrapper: Wrapper<Vue> = mount(PreviewJobOffer, { localVue, store, mocks: { $t, $router, $route, $apiService } });
      const gbActionBarWrapper: Wrapper<Vue> = previewJobOfferWrapper.find(GbActionBar);

      // then
      const gbButtons: WrapperArray<Vue> = gbActionBarWrapper.findAll(GbButton).filter(
        (button: Wrapper<Vue>) =>
          // @ts-ignore
          button.vm.options.buttonLabelKey === 'update'
      );

      // @ts-ignore
      expect(gbButtons.at(0).vm.options).toMatchObject({
        buttonLabelKey: 'update',
        buttonType: 'button',
      });
    });

    it('should call router to redirect to create page without id if there is no id param', async () => {
      // given
      const previewJobOfferWrapper: Wrapper<Vue> = mount(PreviewJobOffer, { localVue, store, mocks: { $t, $router, $route, $apiService } });

      // when
      const gbButtons: WrapperArray<Vue> = previewJobOfferWrapper.findAll(GbButton).filter(
        (button: Wrapper<Vue>) =>
          // @ts-ignore
          button.vm.options.buttonLabelKey === 'update'
      );
      gbButtons.at(0).trigger('click');
      await previewJobOfferWrapper.vm.$nextTick();

      // then
      expect(pushMock).toHaveBeenCalledWith({ path: 'create' });
    });

    it('should call router to redirect to create page with id if there is an id param', async () => {
      // given
      $route.params.id = 'aJobOfferId';
      const previewJobOfferWrapper: Wrapper<Vue> = mount(PreviewJobOffer, { localVue, store, mocks: { $t, $router, $route, $apiService } });

      // when
      const gbButtons: WrapperArray<Vue> = previewJobOfferWrapper.findAll(GbButton).filter(
        (button: Wrapper<Vue>) =>
          // @ts-ignore
          button.vm.options.buttonLabelKey === 'update'
      );
      gbButtons.at(0).trigger('click');
      await previewJobOfferWrapper.vm.$nextTick();

      // then
      expect(pushMock).toHaveBeenCalledWith({ path: '../create/aJobOfferId' });
    });
  });

  describe('validate button', () => {
    beforeEach(() => {
      $t.mockReturnValue('Validate');
    });

    it('should display validate button in GbActionBar', () => {
      // when
      const previewJobOfferWrapper: Wrapper<Vue> = mount(PreviewJobOffer, { localVue, store, mocks: { $t, $router, $route, $apiService } });
      const gbActionBarWrapper: Wrapper<Vue> = previewJobOfferWrapper.find(GbActionBar);

      // then
      const gbButtonWrapper: WrapperArray<Vue> = gbActionBarWrapper.findAll(GbButton).filter(
        (button: Wrapper<Vue>) =>
          // @ts-ignore
          button.vm.options.buttonLabelKey === 'validate'
      );

      // @ts-ignore
      expect(gbButtonWrapper.at(0).vm.options).toMatchObject({
        buttonLabelKey: 'validate',
        buttonType: 'button',
      });
    });

    it('should call action from store to submit job offer when clicking on validate button', () => {
      // given
      const previewJobOfferWrapper: Wrapper<Vue> = mount(PreviewJobOffer, { localVue, store, mocks: { $t, $router, $route, $apiService } });
      const gbButtonWrapper: WrapperArray<Vue> = previewJobOfferWrapper.findAll(GbButton).filter(
        (button: Wrapper<Vue>) =>
          // @ts-ignore
          button.vm.options.buttonLabelKey === 'validate'
      );

      // when
      gbButtonWrapper.at(0).trigger('click');

      // then
      expect(submitJobOfferMock.mock.calls[0][1]).toMatchObject({ apiService: $apiService });
    });

    it('should call router to redirect to creation confirmation page on parent path when there is an id param', async () => {
      // given
      $route.params.id = 'aJobOfferId';
      const previewJobOfferWrapper: Wrapper<Vue> = mount(PreviewJobOffer, { localVue, store, mocks: { $t, $router, $route, $apiService } });
      const gbButtonWrapper: WrapperArray<Vue> = previewJobOfferWrapper.findAll(GbButton).filter(
        (button: Wrapper<Vue>) =>
          // @ts-ignore
          button.vm.options.buttonLabelKey === 'validate'
      );

      // when
      gbButtonWrapper.at(0).trigger('click');
      await previewJobOfferWrapper.vm.$nextTick();

      // then
      expect(pushMock).toHaveBeenCalledWith('../creation-confirmation');
    });

    it('should call router to redirect to creation confirmation page', async () => {
      // given
      const previewJobOfferWrapper: Wrapper<Vue> = mount(PreviewJobOffer, { localVue, store, mocks: { $t, $router, $route, $apiService } });
      const gbButtonWrapper: WrapperArray<Vue> = previewJobOfferWrapper.findAll(GbButton).filter(
        (button: Wrapper<Vue>) =>
          // @ts-ignore
          button.vm.options.buttonLabelKey === 'validate'
      );

      // when
      gbButtonWrapper.at(0).trigger('click');
      await previewJobOfferWrapper.vm.$nextTick();

      // then
      expect(pushMock).toHaveBeenCalledWith('creation-confirmation');
    });
  });

  describe('GbJobOfferPreviewHeader', () => {
    it('should have component with job-offer from store through getters', () => {
      // given
      getJobOfferMock.mockReturnValue({ title: 'A job title' } as PutJobOfferRequestInterface);
      const previewJobOfferWrapper: Wrapper<Vue> = shallowMount(PreviewJobOffer, { localVue, store, mocks: { $t, $router, $route, $apiService } });

      // when
      const gbJobOfferPreviewHeaderWrapper: Wrapper<Vue> = previewJobOfferWrapper.find(GbJobOfferPreviewHeader);

      // then
      // @ts-ignore
      expect(gbJobOfferPreviewHeaderWrapper.vm.jobOffer).toStrictEqual({
        title: 'A job title',
      });
    });
  });
});

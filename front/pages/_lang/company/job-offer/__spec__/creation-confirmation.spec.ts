import { createLocalVue, mount, shallowMount, Wrapper } from '@vue/test-utils';
import Vue, { VueConstructor } from 'vue';
import Vuex, { ModuleTree, Store } from 'vuex';
import GbButton from '~/components/base-components/gb-button.vue';
import GbConfirmationCard from '~/components/base-components/gb-confirmation-card.vue';
import { RootStateInterface } from '~/store';
import { JobOfferCreateModuleStateInterface } from '~/store/job-offer-create-module';
import { PutJobOfferRequestInterface } from '../../../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import CreationConfirmationJobOffer from '../creation-confirmation.vue';

describe('pages/_lang/company/job-offer/creation-confirmation', () => {
  let localVue: VueConstructor;

  let $t: jest.Mock;
  let $router: object;

  let pushMock: jest.Mock;
  let resetMock: jest.Mock;

  let state: JobOfferCreateModuleStateInterface;
  let store: Store<RootStateInterface>;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
  });

  beforeEach(() => {
    $t = jest.fn();

    pushMock = jest.fn();
    $router = {
      push: pushMock,
    };

    resetMock = jest.fn();

    state = {
      jobOffer: {} as PutJobOfferRequestInterface,
    } as JobOfferCreateModuleStateInterface;

    store = new Vuex.Store({
      modules: {
        'job-offer-create-module': {
          namespaced: true,
          state,
          actions: {
            reset: resetMock,
          },
        },
      } as ModuleTree<RootStateInterface>,
    });
  });

  describe('middleware', () => {
    it('should use authenticatedAsCompany middleware', () => {
      // when
      const creationConfirmationJobOfferWrapper: Wrapper<Vue> = shallowMount(CreationConfirmationJobOffer, { mocks: { $t, $router } });

      // then
      expect(creationConfirmationJobOfferWrapper.vm.$options.middleware).toBe('authenticatedAsCompany.middleware');
    });
  });

  it('should contain a GbConfirmationCard component', () => {
    // when
    const creationConfirmationJobOfferWrapper: Wrapper<Vue> = shallowMount(CreationConfirmationJobOffer, { mocks: { $t, $router } });

    // then
    expect(creationConfirmationJobOfferWrapper.findAll(GbConfirmationCard).length).toBe(1);
  });

  it('should display title', () => {
    // given
    $t.mockReturnValue('Creation confirmation title');

    // when
    const creationConfirmationJobOfferWrapper: Wrapper<Vue> = mount(CreationConfirmationJobOffer, { mocks: { $t, $router } });

    // then
    expect($t).toHaveBeenCalledWith('creationConfirmation.title');
    expect(creationConfirmationJobOfferWrapper.find('h2').text()).toBe('Creation confirmation title');
  });

  it('should display subtitle', () => {
    // given
    $t.mockReturnValue('Creation confirmation subtitle');

    // when
    const creationConfirmationJobOfferWrapper: Wrapper<Vue> = mount(CreationConfirmationJobOffer, { mocks: { $t, $router } });

    // then
    expect($t).toHaveBeenCalledWith('creationConfirmation.jobOfferSubtitle');
    expect(
      creationConfirmationJobOfferWrapper
        .findAll('p')
        .at(0)
        .text()
    ).toBe('Creation confirmation subtitle');
  });

  it('should display explanation', () => {
    // given
    $t.mockReturnValue('Creation confirmation explanation');

    // when
    const creationConfirmationJobOfferWrapper: Wrapper<Vue> = mount(CreationConfirmationJobOffer, { mocks: { $t, $router } });

    // then
    expect($t).toHaveBeenCalledWith('creationConfirmation.explanation');
    expect(
      creationConfirmationJobOfferWrapper
        .findAll('p')
        .at(1)
        .text()
    ).toBe('Creation confirmation explanation');
  });

  describe('boost your visibility button', () => {
    it('should display GbButton component with props for boost your visibility', () => {
      // given
      const expected: object = { buttonLabelKey: 'creationConfirmation.boostYourVisibility' };

      // when
      const creationConfirmationJobOfferWrapper: Wrapper<Vue> = mount(CreationConfirmationJobOffer, { mocks: { $t, $router } });

      // then
      // @ts-ignore
      expect(creationConfirmationJobOfferWrapper.findAll(GbButton).at(0).vm.options).toMatchObject(expected);
    });
  });

  describe('create new job offer button', () => {
    it('should display GbButton component with props for create new job offer', () => {
      // given
      const expected: object = { buttonLabelKey: 'creationConfirmation.createNewJobOffer' };

      // when
      const creationConfirmationJobOfferWrapper: Wrapper<Vue> = mount(CreationConfirmationJobOffer, { mocks: { $t, $router } });

      // then
      // @ts-ignore
      expect(creationConfirmationJobOfferWrapper.findAll(GbButton).at(1).vm.options).toMatchObject(expected);
    });

    it('should call router to redirect to job offer create page when click on it', async () => {
      // given
      const creationConfirmationJobOfferWrapper: Wrapper<Vue> = mount(CreationConfirmationJobOffer, { mocks: { $t, $router } });

      // when
      creationConfirmationJobOfferWrapper
        .findAll(GbButton)
        .at(1)
        .trigger('click');
      await creationConfirmationJobOfferWrapper.vm.$nextTick();

      // then
      expect(pushMock).toHaveBeenCalledWith('create');
    });
  });

  describe('destroyed hook', () => {
    it('should call reset action from store', () => {
      // given
      const creationConfirmationJobOfferWrapper: Wrapper<Vue> = mount(CreationConfirmationJobOffer, { localVue, store, mocks: { $t, $router } });

      // when
      creationConfirmationJobOfferWrapper.destroy();

      // then
      expect(resetMock).toHaveBeenCalled();
    });
  });
});

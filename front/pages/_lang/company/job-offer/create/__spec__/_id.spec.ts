import { Context } from '@nuxt/types';
import { createLocalVue, mount, shallowMount, Stubs, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import { TranslateResult } from 'vue-i18n';
import Datepicker from 'vuejs-datepicker';
import Vuex, { ModuleTree, Store } from 'vuex';
import GbActionBar from '~/components/base-components/gb-action-bar.vue';
import GbBadge from '~/components/base-components/gb-badge.vue';
import GbBreadcrumb from '~/components/base-components/gb-breadcrumb.vue';
import GbButton from '~/components/base-components/gb-button.vue';
import GbErrorsSummary from '~/components/base-components/gb-errors-summary.vue';
import GbJobOfferCreateAdditionalFields from '~/components/job-offer/create/gb-job-offer-create-additional-fields.vue';
import GbJobOfferCreateDescriptionAndSpecificities from '~/components/job-offer/create/gb-job-offer-create-description-and-specificities.vue';
import GbJobOfferCreateFooter from '~/components/job-offer/create/gb-job-offer-create-footer.vue';
import GbJobOfferCreateHeader from '~/components/job-offer/create/gb-job-offer-create-header.vue';
import ApiService from '~/services/api.service';
import { RootStateInterface } from '~/store';
import { AdvantagesEnums, JobOfferEnums } from '../../../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import {
  PutAdvandagesRequestInterface,
  PutJobOfferRequestInterface,
} from '../../../../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import { CommonTypeAliases } from '../../../../../../../back/src/infrastructure/rest/common-models/common-type-aliases';
import { DomainErrorsResponseInterface } from '../../../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import CreateJobOffer from '../_id.vue';

describe('pages/_lang/company/job-offer/create/_id', () => {
  let localVue: typeof Vue;

  let ctx: Context;
  let $apiService: ApiService;
  let $t: jest.Mock;
  let $router: object;
  let $route: { path: string; params: { id: string } };
  let pushMock: jest.Mock;

  let store: Store<RootStateInterface>;
  let dispatchMock: jest.Mock;

  let setJobOfferFieldMock: jest.Mock;
  let getJobOfferMock: jest.Mock;
  let validateJobOfferMock: jest.Mock;
  let submitAsDraftJobOfferMock: jest.Mock;
  let isDraftJobOfferMock: jest.Mock;

  let stubs: Stubs;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
    localVue.component('date-picker', Datepicker);
  });

  beforeEach(() => {
    $apiService = {} as ApiService;

    $t = jest.fn();

    pushMock = jest.fn();
    $router = {
      push: pushMock,
    };
    $route = { path: '', params: { id: '' } };

    ctx = {
      app: {},
      params: {},
    } as Context;

    dispatchMock = jest.fn();

    setJobOfferFieldMock = jest.fn();
    validateJobOfferMock = jest.fn();
    submitAsDraftJobOfferMock = jest.fn();

    getJobOfferMock = jest.fn();
    isDraftJobOfferMock = jest.fn();

    store = new Vuex.Store({
      modules: {
        'job-offer-create-module': {
          namespaced: true,
          getters: {
            getJobOffer: getJobOfferMock,
            isDraftJobOffer: isDraftJobOfferMock,
          },
          actions: {
            setJobOfferField: setJobOfferFieldMock,
            validateJobOffer: validateJobOfferMock,
            submitAsDraftJobOffer: submitAsDraftJobOfferMock,
          },
          state: {
            id: '',
          },
        },
      } as ModuleTree<RootStateInterface>,
    });

    stubs = { 'client-only': true };
  });

  describe('middleware', () => {
    it('should use authenticatedAsCompany middleware', () => {
      // when
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $route, $apiService, $t } });

      // then
      expect(createJobOfferWrapper.vm.$options.middleware).toBe('authenticatedAsCompany.middleware');
    });
  });

  describe('asyncData()', () => {
    it('should call store action to retrieve draft job offer if there is an id in route', async () => {
      // given
      const id: CommonTypeAliases.JobOfferId = 'aJobOfferId';
      ctx.app.$apiService = $apiService;
      ctx.params = { id };
      ctx.store = ({ dispatch: dispatchMock } as unknown) as Store<unknown>;
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { localVue, store, mocks: { $t } });

      // when
      // @ts-ignore
      createJobOfferWrapper.vm.$options.asyncData(ctx);

      // then
      await expect(dispatchMock).toHaveBeenNthCalledWith(1, 'job-offer-create-module/setJobOfferId', id);
      await expect(dispatchMock).toHaveBeenNthCalledWith(2, 'job-offer-create-module/getJobOffer', { apiService: $apiService });
    });

    it('should not call store action to retrieve draft job offer if there is an id in route', async () => {
      // given
      ctx.app.$apiService = $apiService;
      ctx.params = {};
      ctx.store = ({ dispatch: dispatchMock } as unknown) as Store<unknown>;
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { localVue, store, mocks: { $t } });

      // when
      // @ts-ignore
      createJobOfferWrapper.vm.$options.asyncData(ctx);

      // then
      expect(dispatchMock).not.toHaveBeenCalled();
      expect(dispatchMock).not.toHaveBeenCalled();
    });
  });

  describe('data()', () => {
    it('should initialize data with default values', () => {
      // given
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $route, $apiService, $t } });

      // when
      const result: Record<string, unknown> = createJobOfferWrapper.vm.$data;

      // then
      expect(result).toStrictEqual({
        domainErrors: {},
        breadcrumb: [
          {
            i18nLabel: 'jobOffer.breadcrumbCreateLabel',
            path: 'job-offer/create',
          },
          {
            i18nLabel: 'jobOffer.breadcrumbPreviewLabel',
            path: 'job-offer/preview',
          },
        ],
      });
    });
  });

  describe('breadcrumb', () => {
    it('should display GbBreadcrumb in GbActionBar', () => {
      // when
      const jobOfferMock: PutJobOfferRequestInterface = ({
        title: 'A job offer title',
        annualSalaryRange: {},
        advantages: {},
        traveling: {},
        targetCustomers: [],
        requiredExperiences: [],
        requestedSpecialities: [],
        wantedPersonality: [],
      } as unknown) as PutJobOfferRequestInterface;
      getJobOfferMock.mockReturnValue(jobOfferMock);
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t } });
      const gbActionBarWrapper: Wrapper<Vue> = createJobOfferWrapper.find(GbActionBar);

      // then
      // @ts-ignore
      expect(gbActionBarWrapper.find(GbBreadcrumb).vm.options).toStrictEqual({
        backgroundColor: 'COMPANY_BACKGROUND_COLOR',
        isLeftBorderRounded: false,
        steps: [
          {
            i18nLabel: 'jobOffer.breadcrumbCreateLabel',
            path: 'job-offer/create',
          },
          {
            i18nLabel: 'jobOffer.breadcrumbPreviewLabel',
            path: 'job-offer/preview',
          },
        ],
      });
    });
  });

  describe('draft badge', () => {
    beforeEach(() => {
      const jobOfferMock: PutJobOfferRequestInterface = ({
        title: 'A job offer title',
        annualSalaryRange: {},
        advantages: {},
        traveling: {},
        targetCustomers: [],
        requiredExperiences: [],
        requestedSpecialities: [],
        wantedPersonality: [],
        status: JobOfferEnums.JobOfferStatus.STATUS_DRAFT,
      } as unknown) as PutJobOfferRequestInterface;
      getJobOfferMock.mockReturnValue(jobOfferMock);
    });

    it('should display GbBadge in GbActionBar if status is draft', () => {
      // when
      isDraftJobOfferMock.mockReturnValue(true);

      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t } });
      const gbActionBarWrapper: Wrapper<Vue> = createJobOfferWrapper.find(GbActionBar);

      // then
      // @ts-ignore
      expect(gbActionBarWrapper.find(GbBadge).vm.options).toStrictEqual({
        badgeIsRounded: true,
        badgeColor: 'SUCCESS_COLOR',
      });
    });

    it('should not display GbBadge in GbActionBar if status is not draft', () => {
      // when
      isDraftJobOfferMock.mockReturnValue(false);
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t } });
      const gbActionBarWrapper: Wrapper<Vue> = createJobOfferWrapper.find(GbActionBar);

      // then
      // @ts-ignore
      expect(gbActionBarWrapper.findAll(GbBadge).length).toBe(0);
    });

    it('should display label in GbBadge if status is draft', () => {
      // when
      $t.mockReturnValue('Draft');
      isDraftJobOfferMock.mockReturnValue(true);

      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t } });
      const gbBadgeWrapper: Wrapper<Vue> = createJobOfferWrapper.find(GbBadge);

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.statusDraft');
      expect(gbBadgeWrapper.text()).toBe('Draft');
    });
  });

  describe('mandatory fields text', () => {
    beforeEach(() => {
      $t.mockReturnValue('Mandatory fields*');
    });
    it('should contain mandatory fields legent', () => {
      // given
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $apiService, $t } });

      // when
      const result: Wrapper<Vue> = createJobOfferWrapper.find('.job-offer-create--mandatory-fields');

      // then
      expect(result.text()).toBe('Mandatory fields*');
    });
  });

  describe('errors summary', () => {
    it('should not be rendered if there is no domain errors', () => {
      // when
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $apiService, $t } });

      // then
      expect(createJobOfferWrapper.find(GbErrorsSummary).exists()).toBeFalsy();
    });

    it('should be rendered if there is domain errors', async () => {
      // given
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $apiService, $t } });

      // when
      createJobOfferWrapper.setData({ domainErrors: { name: { messages: ['A message'] } } });
      await createJobOfferWrapper.vm.$nextTick();

      // then
      expect(createJobOfferWrapper.find(GbErrorsSummary).exists()).toBeTruthy();
    });

    it('should have options set', async () => {
      // given
      const expected: DomainErrorsResponseInterface = { name: { messages: ['A message'], i18nMessageKeys: ['aMessage'] } };
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $apiService, $t } });

      // when
      createJobOfferWrapper.setData({ domainErrors: expected });
      await createJobOfferWrapper.vm.$nextTick();

      // then
      // @ts-ignore
      expect(createJobOfferWrapper.find(GbErrorsSummary).vm.options).toStrictEqual({
        domainI18nPath: 'jobOffer',
        domainErrors: expected,
      });
    });
  });

  describe('banner', () => {
    beforeEach(() => {
      $t.mockReturnValue('New job offer');
    });
    it('should contain banner', () => {
      // given
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $apiService, $t } });

      // when
      const result: Wrapper<Vue> = createJobOfferWrapper.find('.job-offer-create--banner');

      // then
      expect(result.text()).toBe('New job offer');
    });
  });

  describe('job offer header', () => {
    it('should bind getJobOffer getter to GbJobOfferCreateHeader input prop', () => {
      // given
      const expected: PutJobOfferRequestInterface = { title: 'A job offer title' } as PutJobOfferRequestInterface;
      getJobOfferMock.mockReturnValue(expected);

      // when
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $apiService, $t } });

      // then
      // @ts-ignore
      expect(createJobOfferWrapper.find(GbJobOfferCreateHeader).vm.jobOffer).toStrictEqual(expected);
    });

    it('should set GbJobOfferCreateHeader options prop using domainErrors', () => {
      // given
      const domainErrors: DomainErrorsResponseInterface = {
        title: {
          messages: ['message'],
          i18nMessageKeys: ['i18n.key'],
        },
      };
      const data = () => ({ domainErrors });

      // when
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $apiService, $t }, data });

      // then
      // @ts-ignore
      expect(createJobOfferWrapper.find(GbJobOfferCreateHeader).vm.options).toStrictEqual({ domainErrors });
    });

    it('should call setJobOfferField when input is triggered', () => {
      // given
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $apiService, $t } });

      const jobOfferCreateHeader: Wrapper<Vue> = createJobOfferWrapper.find(GbJobOfferCreateHeader);
      const inputExpected: { fieldName: string; fieldValue: string } = {
        fieldName: 'title',
        fieldValue: 'A job offer title',
      };

      // when
      jobOfferCreateHeader.vm.$emit('input', inputExpected);

      // then
      expect(setJobOfferFieldMock).toHaveBeenCalled();
      expect(setJobOfferFieldMock.mock.calls[0][1]).toMatchObject(inputExpected);
    });
  });

  describe('job offer description and specificities', () => {
    it('should bind getJobOffer getter to GbJobOfferCreateDescriptionAndSpecificities input prop', () => {
      // given
      const expected: PutJobOfferRequestInterface = { description: 'A job offer description' } as PutJobOfferRequestInterface;

      // when
      getJobOfferMock.mockReturnValue(expected);

      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $t } });

      // then
      // @ts-ignore
      expect(createJobOfferWrapper.find(GbJobOfferCreateDescriptionAndSpecificities).vm.jobOffer).toStrictEqual(expected);
    });

    it('should set GbJobOfferCreateDescriptionAndSpecificities options prop', () => {
      // given
      const domainErrors: DomainErrorsResponseInterface = {
        title: {
          messages: ['message'],
          i18nMessageKeys: ['i18n.key'],
        },
      };
      const data = () => ({ domainErrors });

      // when
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $t }, data });

      // then
      // @ts-ignore
      expect(createJobOfferWrapper.find(GbJobOfferCreateDescriptionAndSpecificities).vm.options).toStrictEqual({ domainErrors });
    });

    it('should call setJobOfferField when input is triggered', () => {
      // given
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $t } });

      const jobOfferCreateHeader: Wrapper<Vue> = createJobOfferWrapper.find(GbJobOfferCreateDescriptionAndSpecificities);
      const inputExpected: { fieldName: string; fieldValue: string } = {
        fieldName: 'description',
        fieldValue: 'A job offer description',
      };

      // when
      jobOfferCreateHeader.vm.$emit('input', inputExpected);

      // then
      expect(setJobOfferFieldMock).toHaveBeenCalled();
      expect(setJobOfferFieldMock.mock.calls[0][1]).toMatchObject(inputExpected);
    });
  });

  describe('job offer content', () => {
    it('should bind getJobOffer getter to GbJobOfferCreateAdditionalFields input prop', () => {
      // given
      const expected: PutJobOfferRequestInterface = { description: 'A job offer description' } as PutJobOfferRequestInterface;

      // when
      getJobOfferMock.mockReturnValue(expected);

      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $t } });

      // then
      // @ts-ignore
      expect(createJobOfferWrapper.find(GbJobOfferCreateAdditionalFields).vm.jobOffer).toStrictEqual(expected);
    });

    it('should set GbJobOfferCreateAdditionalFields options prop', () => {
      // given
      const domainErrors: DomainErrorsResponseInterface = {
        title: {
          messages: ['message'],
          i18nMessageKeys: ['i18n.key'],
        },
      };
      const data = () => ({ domainErrors });

      // when
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $t }, data });

      // then
      // @ts-ignore
      expect(createJobOfferWrapper.find(GbJobOfferCreateAdditionalFields).vm.options).toStrictEqual({ domainErrors });
    });

    it('should call setJobOfferField when input is triggered', () => {
      // given
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $t } });

      const gbJobOfferCreateAdditionalFields: Wrapper<Vue> = createJobOfferWrapper.find(GbJobOfferCreateAdditionalFields);
      const inputExpected: { fieldName: string; fieldValue: PutAdvandagesRequestInterface } = {
        fieldName: 'advantages',
        fieldValue: { health: [AdvantagesEnums.AdvantageHealth.HEALTH_LIFE_INSURANCE] },
      };

      // when
      gbJobOfferCreateAdditionalFields.vm.$emit('input', inputExpected);

      // then
      expect(setJobOfferFieldMock).toHaveBeenCalled();
      expect(setJobOfferFieldMock.mock.calls[0][1]).toMatchObject(inputExpected);
    });
  });

  describe('job offer footer', () => {
    it('should bind getJobOffer getter to GbJobOfferCreateFooter input prop', () => {
      // given
      const expected: PutJobOfferRequestInterface = { internalReferenceNumber: 'AB_12345-D' } as PutJobOfferRequestInterface;
      getJobOfferMock.mockReturnValue(expected);

      // when
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $apiService, $t } });

      // then
      // @ts-ignore
      expect(createJobOfferWrapper.find(GbJobOfferCreateFooter).vm.jobOffer).toStrictEqual(expected);
    });

    it('should call setJobOfferField when input is triggered', () => {
      // given
      const createJobOfferWrapper: Wrapper<Vue> = shallowMount(CreateJobOffer, { store, localVue, mocks: { $apiService, $t } });

      const jobOfferCreateFooterWrapper: Wrapper<Vue> = createJobOfferWrapper.find(GbJobOfferCreateFooter);
      const inputExpected: { fieldName: string; fieldValue: string } = {
        fieldName: 'internalReferenceNumber',
        fieldValue: 'AB_12345-D',
      };

      // when
      jobOfferCreateFooterWrapper.vm.$emit('input', inputExpected);

      // then
      expect(setJobOfferFieldMock).toHaveBeenCalled();
      expect(setJobOfferFieldMock.mock.calls[0][1]).toMatchObject(inputExpected);
    });
  });

  describe('next button', () => {
    beforeEach(() => {
      const jobOfferMock: PutJobOfferRequestInterface = ({
        title: 'A job offer title',
        annualSalaryRange: {},
        advantages: {},
        traveling: {},
        targetCustomers: [],
        requiredExperiences: [],
        requestedSpecialities: [],
        wantedPersonality: [],
      } as unknown) as PutJobOfferRequestInterface;
      getJobOfferMock.mockReturnValue(jobOfferMock);

      $t.mockReturnValue('Preview');
    });

    it('should display submit button in GbActionBar', () => {
      // when
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t } });
      const gbActionBarWrapper: Wrapper<Vue> = createJobOfferWrapper.find(GbActionBar);

      // then
      // @ts-ignore
      expect(gbActionBarWrapper.findAll(GbButton).at(1).vm.options).toMatchObject({
        buttonLabelKey: 'next',
        buttonType: 'button',
      });
    });

    it('should call action from store to validate a job offer when submitting form', async () => {
      // given
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t } });
      createJobOfferWrapper.setMethods({ validateForm: () => true });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(1)
        .trigger('click');
      await nextTicks(createJobOfferWrapper, 2);

      // then
      expect(validateJobOfferMock.mock.calls[0][1]).toMatchObject({ apiService: $apiService });
    });

    it('should call router to go to preview with id when no validation error and an id param', async () => {
      // given
      $route.params.id = 'aJobOfferId';
      (validateJobOfferMock as jest.Mock).mockReturnValueOnce(Promise.resolve());
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, {
        stubs,
        store,
        localVue,
        mocks: { $route, $apiService, $t, $router },
      });
      createJobOfferWrapper.setMethods({ validateForm: () => true });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(1)
        .trigger('click');
      await nextTicks(createJobOfferWrapper, 2);

      // then
      expect(pushMock).toHaveBeenCalledWith({ path: '../preview/aJobOfferId' });
    });

    it('should call router to go to preview when no validation error and no id param', async () => {
      // given
      $route.params.id = '';
      (validateJobOfferMock as jest.Mock).mockReturnValueOnce(Promise.resolve());
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, {
        stubs,
        store,
        localVue,
        mocks: { $route, $apiService, $t, $router },
      });
      createJobOfferWrapper.setMethods({ validateForm: () => true });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(1)
        .trigger('click');
      await nextTicks(createJobOfferWrapper, 2);

      // then
      expect(pushMock).toHaveBeenCalledWith({ path: 'preview' });
    });

    it('should not call any action nor router to go anywhere when client validation error', async () => {
      // given
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t, $router } });
      createJobOfferWrapper.setMethods({ validateForm: () => false });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(1)
        .trigger('click');
      await createJobOfferWrapper.vm.$nextTick();

      // then
      expect(submitAsDraftJobOfferMock).not.toHaveBeenCalled();
      expect(validateJobOfferMock).not.toHaveBeenCalled();
      expect(pushMock).not.toHaveBeenCalled();
    });

    it('should not call router to go anywhere when server validation error', async () => {
      // given
      (validateJobOfferMock as jest.Mock).mockReturnValueOnce(
        Promise.reject({
          domainErrors: { title: {} },
        })
      );

      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t, $router } });
      createJobOfferWrapper.setMethods({ validateForm: () => true });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(1)
        .trigger('click');
      await createJobOfferWrapper.vm.$nextTick();

      // then
      expect(pushMock).not.toHaveBeenCalled();
    });

    it('should clean domain errors when submitting form successfully after having an error on first submission', async () => {
      // given
      (validateJobOfferMock as jest.Mock).mockReturnValueOnce(
        Promise.reject({
          domainErrors: { title: {} },
        })
      );
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t, $router } });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(1)
        .trigger('click');
      await createJobOfferWrapper.vm.$nextTick();
      createJobOfferWrapper
        .findAll('button')
        .at(1)
        .trigger('click');
      await createJobOfferWrapper.vm.$nextTick();

      // then
      expect(createJobOfferWrapper.vm.$data.domainErrors).toStrictEqual({});
    });

    it('should fill domain errors data when errors', async () => {
      // given
      const expected: DomainErrorsResponseInterface = {
        title: {
          messages: ['A job offer title error'],
          i18nMessageKeys: ['jobOffer.aJobOfferTitleError'],
        },
      };
      (validateJobOfferMock as jest.Mock).mockReturnValue(
        Promise.reject({
          domainErrors: expected,
        })
      );
      $t.mockReturnValue('Job offer creation failed' as TranslateResult);
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t } });
      createJobOfferWrapper.setMethods({ validateForm: () => true });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(1)
        .trigger('click');
      await nextTicks(createJobOfferWrapper, 2);

      // then
      expect(createJobOfferWrapper.vm.$data).toHaveProperty('domainErrors', expected);
    });
  });

  describe('save button', () => {
    beforeEach(() => {
      const jobOfferMock: PutJobOfferRequestInterface = ({
        title: 'A job offer title',
        annualSalaryRange: {},
        advantages: {},
        traveling: {},
        targetCustomers: [],
        requiredExperiences: [],
        requestedSpecialities: [],
        wantedPersonality: [],
      } as unknown) as PutJobOfferRequestInterface;
      getJobOfferMock.mockReturnValue(jobOfferMock);

      $t.mockReturnValue('Save');
    });

    it('should display submit button in GbActionBar', () => {
      // when
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t } });
      const gbActionBarWrapper: Wrapper<Vue> = createJobOfferWrapper.find(GbActionBar);

      // then
      // @ts-ignore
      expect(gbActionBarWrapper.findAll(GbButton).at(0).vm.options).toMatchObject({
        buttonLabelKey: 'save',
        buttonType: 'button',
      });
    });

    it('should call action from store to validate a job offer when saving form', () => {
      // given
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t } });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(0)
        .trigger('click');

      // then
      expect(submitAsDraftJobOfferMock.mock.calls[0][1]).toMatchObject({ apiService: $apiService });
    });

    it('should call action from store to save as draft a job offer when saving form', () => {
      // given
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t } });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(0)
        .trigger('click');

      // then
      expect(submitAsDraftJobOfferMock.mock.calls[0][1]).toMatchObject({ apiService: $apiService });
    });

    it('should redirect to create page with id', async () => {
      // given
      store.state['job-offer-create-module'].id = 'aJobOfferId';
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $router, $route, $apiService, $t } });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(0)
        .trigger('click');
      await nextTicks(createJobOfferWrapper, 2);

      // then
      expect(pushMock).toHaveBeenCalledWith({ path: 'create/aJobOfferId' });
    });

    it('should not redirect to create page with id when already on it', async () => {
      // given
      $route.params.id = 'aJobOfferId';
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $router, $route, $apiService, $t } });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(0)
        .trigger('click');
      await nextTicks(createJobOfferWrapper, 2);

      // then
      expect(pushMock).not.toHaveBeenCalled();
    });

    it('should clean domain errors when submitting form successfully after having an error on first submission', async () => {
      // given
      (submitAsDraftJobOfferMock as jest.Mock).mockReturnValueOnce(
        Promise.reject({
          domainErrors: { title: {} },
        })
      );
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t, $router } });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(0)
        .trigger('click');
      await nextTicks(createJobOfferWrapper, 2);
      createJobOfferWrapper
        .findAll('button')
        .at(0)
        .trigger('click');
      await nextTicks(createJobOfferWrapper, 2);

      // then
      expect(createJobOfferWrapper.vm.$data.domainErrors).toStrictEqual({});
    });

    it('should fill domain errors data when errors', async () => {
      // given
      const expected: DomainErrorsResponseInterface = {
        title: {
          messages: ['A job offer title error'],
          i18nMessageKeys: ['jobOffer.aJobOfferTitleError'],
        },
      };
      (submitAsDraftJobOfferMock as jest.Mock).mockReturnValue(
        Promise.reject({
          domainErrors: expected,
        })
      );
      $t.mockReturnValue('Job offer creation failed' as TranslateResult);
      const createJobOfferWrapper: Wrapper<Vue> = mount(CreateJobOffer, { stubs, store, localVue, mocks: { $route, $apiService, $t } });

      // when
      createJobOfferWrapper
        .findAll('button')
        .at(0)
        .trigger('click');
      await nextTicks(createJobOfferWrapper, 2);

      // then
      expect(createJobOfferWrapper.vm.$data).toHaveProperty('domainErrors', expected);
    });
  });
});

const nextTicks = async (wrapper: Wrapper<Vue>, numberOfTicksToExecute: number): Promise<void> => {
  for (let i: number = 0; i < numberOfTicksToExecute; i++) {
    await wrapper.vm.$nextTick();
  }
};

import { Context, NuxtAppOptions } from '@nuxt/types';
import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import ApiService from '~/services/api.service';
import { DummyPageResponseInterface } from '../../../../../back/src/infrastructure/rest/editorial/models/dummy-page-response.interface';
import UidPage from '../_uid.vue';

describe('pages/_lang/dummy-page/_uid', () => {
  let uidPageWrapper: Wrapper<Vue>;
  let dummyPageContent: DummyPageResponseInterface;
  let ctx: Context;

  beforeEach(() => {
    dummyPageContent = {
      title: '',
      content: '',
      image: {
        alternativeText: '',
        source: '',
      },
      generatedDate: new Date(),
    };
    uidPageWrapper = shallowMount(UidPage, {
      data: (): object => ({
        dummyPageContent,
      }),
    });

    ctx = {} as Context;
    ctx.app = {} as NuxtAppOptions;
    ctx.app.$apiService = {} as ApiService;
  });

  it('should retrieve dummy page asynchronously', async () => {
    // given
    const expected: object = { dummyPageContent };
    const mockGetDummyPageContent: jest.Mock = jest.fn(() => Promise.resolve(dummyPageContent));
    ctx.app.$apiService.getDummyPageContent = mockGetDummyPageContent;

    const lang: string = 'a-lang';
    const uid: string = 'an-uid';
    ctx.params = { lang, uid };

    // when
    // @ts-ignore
    const promise: Promise<object> = uidPageWrapper.vm.$options.asyncData(ctx);

    // then
    expect(mockGetDummyPageContent).toHaveBeenCalledWith(lang, uid);
    await expect(promise).resolves.toEqual(expected);
  });

  it('should contain title as html', async () => {
    // given
    dummyPageContent.title = '<h1>a-title</h1>';

    // when
    uidPageWrapper.setData({ dummyPageContent });
    await uidPageWrapper.vm.$nextTick();

    // then
    expect(uidPageWrapper.find('h1').text()).toBe('a-title');
  });

  it('should contain content as html', async () => {
    // given
    dummyPageContent.content = '<pre>a content</pre>';

    // when
    uidPageWrapper.setData({ dummyPageContent });
    await uidPageWrapper.vm.$nextTick();

    // then
    expect(uidPageWrapper.find('pre').text()).toBe('a content');
  });

  it('should contain image from data', async () => {
    // given
    dummyPageContent.image = {
      alternativeText: 'An awesome image',
      source: 'https://an-image.png',
    };

    // when
    uidPageWrapper.setData({ dummyPageContent });
    await uidPageWrapper.vm.$nextTick();

    // then
    const imgAttributes: { [name: string]: string } = uidPageWrapper.find('img').attributes();
    expect(imgAttributes).toHaveProperty('src', 'https://an-image.png');
    expect(imgAttributes).toHaveProperty('alt', 'An awesome image');
  });

  it('should contain generated date', async () => {
    // given
    dummyPageContent.generatedDate = new Date('2019-12-17T03:24:00');

    // when
    uidPageWrapper.setData({ dummyPageContent });
    await uidPageWrapper.vm.$nextTick();

    // then
    expect(uidPageWrapper.find('small').text()).toContain('Generated at Tue Dec 17 2019 03:24:00');
  });
});

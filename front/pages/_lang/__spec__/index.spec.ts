import { Context, NuxtAppOptions } from '@nuxt/types';
import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import ApiService from '~/services/api.service';

import Index from '../index.vue';

describe('pages/_lang', () => {
  let indexWrapper: Wrapper<Vue>;
  let $route: object;
  let ctx: Context;

  beforeEach(() => {
    $route = {
      params: {},
    };
    indexWrapper = shallowMount(Index, { mocks: { $route }, stubs: { 'nuxt-link': true } });

    ctx = {} as Context;
    ctx.app = {} as NuxtAppOptions;
    ctx.app.$apiService = {} as ApiService;
  });

  describe('when lang is fr-ca', () => {
    beforeEach(() => {
      $route = {
        params: {
          lang: 'fr-ca',
        },
      };
    });

    it('should contain link to english homepage', () => {
      // when
      indexWrapper = shallowMount(Index, { mocks: { $route }, stubs: { 'nuxt-link': true } });

      // then
      const linkWrapper: Wrapper<Vue> = indexWrapper.find('a');
      expect(linkWrapper.attributes()).toHaveProperty('href', '/en-ca');
      expect(linkWrapper.text()).toBe('Go to english homepage');
    });

    it('should contain nuxt-link to french content example', () => {
      // when
      indexWrapper = shallowMount(Index, { mocks: { $route }, stubs: { 'nuxt-link': true } });

      // then
      const nuxtLinkStubWrapper: Wrapper<Vue> = indexWrapper.find('nuxt-link-stub');
      expect(nuxtLinkStubWrapper.attributes()).toHaveProperty('to', '/fr-ca/dummy-page/exemple-de-contenu-cms-en-francais');
      expect(nuxtLinkStubWrapper.text()).toBe('Exemple de contenu CMS en français');
    });
  });

  describe('when lang is en-ca', () => {
    beforeEach(() => {
      $route = {
        params: {
          lang: 'en-ca',
        },
      };
    });

    it('should contain link to french homepage', () => {
      // when
      indexWrapper = shallowMount(Index, { mocks: { $route }, stubs: { 'nuxt-link': true } });

      // then
      const linkWrapper: Wrapper<Vue> = indexWrapper.find('a');
      expect(linkWrapper.attributes()).toHaveProperty('href', '/fr-ca');
      expect(linkWrapper.text()).toBe('Go to french homepage');
    });

    it('should contain nuxt-link to english content example', () => {
      // when
      indexWrapper = shallowMount(Index, { mocks: { $route }, stubs: { 'nuxt-link': true } });

      // then
      const nuxtLinkStubWrapper: Wrapper<Vue> = indexWrapper.find('nuxt-link-stub');
      expect(nuxtLinkStubWrapper.attributes()).toHaveProperty('to', '/en-ca/dummy-page/cms-content-example-in-english');
      expect(nuxtLinkStubWrapper.text()).toBe('CMS content example in english');
    });
  });
});

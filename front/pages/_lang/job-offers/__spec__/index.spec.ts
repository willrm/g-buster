import { Context } from '@nuxt/types';
import { createLocalVue, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import Vuex, { ModuleTree, Store } from 'vuex';
import GbLoginBlock from '~/components/base-components/gb-login-block.vue';
import GbJobOfferList from '~/components/job-offer/list/gb-job-offer-list.vue';
import GbJobOfferSearch from '~/components/job-offer/search/gb-job-offer-search.vue';
import ApiService from '~/services/api.service';
import { RootStateInterface } from '~/store';
import { SetJobOffersFilterActionPayload } from '~/store/job-offers-module/actions';
import { GetJobOffersRequestInterface } from '../../../../../back/src/infrastructure/rest/career/models/get-job-offers-request.interface';
import { JobOfferEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { JobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import JobOffers from '../index.vue';

describe('pages/_lang/job-offers', () => {
  let localVue: typeof Vue;
  let $t: jest.Mock;

  let ctx: Context;
  let getJobOffersMock: jest.Mock;
  let getJobOffersFilterMock: jest.Mock;
  let setJobOffersMock: jest.Mock;
  let setJobOffersFilterMock: jest.Mock;
  let isLoggedAsCandidateMock: jest.Mock;

  let dispatchMock: jest.Mock;

  let $apiService: ApiService;

  let store: Store<RootStateInterface>;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
  });

  beforeEach(() => {
    $t = jest.fn();
    $apiService = {} as ApiService;
    dispatchMock = jest.fn();

    ctx = {
      app: { $apiService },
      store: ({ dispatch: dispatchMock } as unknown) as Store<unknown>,
    } as Context;

    getJobOffersMock = jest.fn();
    getJobOffersFilterMock = jest.fn();
    setJobOffersMock = jest.fn();
    setJobOffersFilterMock = jest.fn();
    isLoggedAsCandidateMock = jest.fn();

    store = new Vuex.Store({
      modules: {
        'job-offers-module': {
          namespaced: true,
          getters: {
            getJobOffers: getJobOffersMock,
            getJobOffersFilter: getJobOffersFilterMock,
          },
          actions: {
            setJobOffers: setJobOffersMock,
            setJobOffersFilter: setJobOffersFilterMock,
          },
        },
        'user-profile-module': {
          namespaced: true,
          getters: {
            isLoggedAsCandidate: isLoggedAsCandidateMock,
          },
        },
      } as ModuleTree<RootStateInterface>,
    });
  });

  describe('layout', () => {
    it('should have b2c layout', () => {
      // @ts-ignore
      expect(JobOffers.options.layout).toBe('b2c');
    });
  });

  describe('asyncData()', () => {
    it('should call store action to retrieve job offers asynchronously', async () => {
      // given
      const jobOffersWrapper: Wrapper<Vue> = shallowMount(JobOffers, { localVue, store, mocks: { $t } });

      // when
      // @ts-ignore
      await jobOffersWrapper.vm.$options.asyncData(ctx);

      // then
      expect(dispatchMock).toHaveBeenCalledWith('job-offers-module/setJobOffers', { apiService: $apiService });
    });

    it('should call store action to retrieve user profile asynchronously', async () => {
      // given
      const jobOffersWrapper: Wrapper<Vue> = shallowMount(JobOffers, { localVue, store, mocks: { $t } });

      // when
      // @ts-ignore
      await jobOffersWrapper.vm.$options.asyncData(ctx);

      // then
      expect(dispatchMock).toHaveBeenCalledWith('user-profile-module/setUserProfile', { apiService: $apiService });
    });
  });

  describe('job offers', () => {
    it('should display GbJobOfferList component with job offers from store', () => {
      // given
      const expected: JobOfferResponseInterface = { title: 'A job offer' } as JobOfferResponseInterface;
      getJobOffersMock.mockReturnValue(expected);

      // when
      const jobOffersWrapper: Wrapper<Vue> = shallowMount(JobOffers, { localVue, store, mocks: { $t } });

      // then
      // @ts-ignore
      expect(jobOffersWrapper.find(GbJobOfferList).vm.jobOffers).toStrictEqual(expected);
    });

    it('should display GbJobOfferList component with user logged as candidate info from store', () => {
      // given
      const expected: boolean = true;
      isLoggedAsCandidateMock.mockReturnValue(expected);

      // when
      const jobOffersWrapper: Wrapper<Vue> = shallowMount(JobOffers, { localVue, store, mocks: { $t } });

      // then
      // @ts-ignore
      expect(jobOffersWrapper.find(GbJobOfferList).vm.isLoggedAsCandidate).toStrictEqual(expected);
    });
  });

  describe('login block', () => {
    it('should display GbLoginBlock when not logged as candidate', () => {
      // given
      isLoggedAsCandidateMock.mockReturnValue(false);
      const expected: JobOfferResponseInterface = { title: 'A job offer' } as JobOfferResponseInterface;
      getJobOffersMock.mockReturnValue(expected);

      // when
      const jobOffersWrapper: Wrapper<Vue> = shallowMount(JobOffers, { localVue, store, mocks: { $t } });

      // then
      // @ts-ignore
      expect(jobOffersWrapper.findAll(GbLoginBlock)).toHaveLength(1);
    });
    it('should not display GbLoginBlock when logged as candidate', () => {
      // given
      isLoggedAsCandidateMock.mockReturnValue(true);
      const expected: JobOfferResponseInterface = { title: 'A job offer' } as JobOfferResponseInterface;
      getJobOffersMock.mockReturnValue(expected);

      // when
      const jobOffersWrapper: Wrapper<Vue> = shallowMount(JobOffers, { localVue, store, mocks: { $t } });

      // then
      // @ts-ignore
      expect(jobOffersWrapper.findAll(GbLoginBlock)).toHaveLength(0);
    });
  });

  describe('filter', () => {
    it('should display GbJobOfferSearch component with job offers filter from getter', () => {
      // given
      const expected: GetJobOffersRequestInterface = {
        jobTypes: [JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP],
      } as GetJobOffersRequestInterface;
      getJobOffersFilterMock.mockReturnValue(expected);

      // when
      const jobOffersWrapper: Wrapper<Vue> = shallowMount(JobOffers, { localVue, store, mocks: { $t } });

      // then
      // @ts-ignore
      expect(jobOffersWrapper.find(GbJobOfferSearch).vm.value).toStrictEqual(expected);
    });

    it('should call store action to update job offers filter when input event from GbJobOfferSearch', async () => {
      // given
      const expected: SetJobOffersFilterActionPayload = {
        fieldName: 'requestedSpecialities',
        fieldValue: [JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION],
      };
      const jobOffersWrapper: Wrapper<Vue> = shallowMount(JobOffers, { localVue, store, mocks: { $t } });

      // when
      jobOffersWrapper.find(GbJobOfferSearch).vm.$emit('input', expected);
      await jobOffersWrapper.vm.$nextTick();

      // then
      expect(setJobOffersFilterMock).toHaveBeenCalled();
      expect(setJobOffersFilterMock.mock.calls[0][1]).toMatchObject(expected);
    });

    it('should call store action to refresh job offers when input event from GbJobOfferSearch', async () => {
      // given
      const jobOffersWrapper: Wrapper<Vue> = shallowMount(JobOffers, { localVue, store, mocks: { $t } });

      // when
      jobOffersWrapper.find(GbJobOfferSearch).vm.$emit('input', {});
      await jobOffersWrapper.vm.$nextTick();

      // then
      expect(setJobOffersMock).toHaveBeenCalled();
      expect(setJobOffersMock.mock.calls[0][1]).toMatchObject({ apiService: $apiService });
    });
  });
});

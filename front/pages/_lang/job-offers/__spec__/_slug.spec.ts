import { Context } from '@nuxt/types';
import { createLocalVue, shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue, { VueConstructor } from 'vue';
import Vuex, { ModuleTree, Store } from 'vuex';
import GbJobOfferViewHeader from '~/components/job-offer/view/gb-job-offer-view-header.vue';
import GbJobOfferViewTopBlock from '~/components/job-offer/view/gb-job-offer-view-top-block.vue';
import GbJobOfferViewBottomBlock from '~/components/job-offer/view/gb-job-offer-view-bottom-block.vue';
import ApiService from '~/services/api.service';
import { RootStateInterface } from '~/store';
import { JobOfferWithCompanyResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import JobOfferSlug from '../_slug.vue';

describe('pages/_lang/job-offers/_slug', () => {
  let localVue: VueConstructor;

  let $t: jest.Mock;
  let ctx: Context;
  let $route: { params: { slug: string } };
  let $apiService: ApiService;

  let getJobOfferMock: jest.Mock;

  let isLoggedAsCandidateMock: jest.Mock;

  let store: Store<RootStateInterface>;
  let dispatchMock: jest.Mock;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
  });

  beforeEach(() => {
    $t = jest.fn();
    $apiService = {} as ApiService;
    ctx = {
      app: {},
      params: {},
    } as Context;

    dispatchMock = jest.fn();

    $route = { params: { slug: '' } };
    getJobOfferMock = jest.fn();

    isLoggedAsCandidateMock = jest.fn();

    store = new Vuex.Store({
      modules: {
        'job-offers-module': {
          namespaced: true,
          getters: {
            getJobOffer: getJobOfferMock,
          },
        },
        'user-profile-module': {
          namespaced: true,
          getters: {
            isLoggedAsCandidate: isLoggedAsCandidateMock,
          },
        },
      } as ModuleTree<RootStateInterface>,
    });

    getJobOfferMock.mockReturnValue({
      title: 'A job title',
    });
  });

  describe('layout', () => {
    it('should have b2c layout', () => {
      // @ts-ignore
      expect(JobOfferSlug.options.layout).toBe('b2c');
    });
  });

  describe('asyncData()', () => {
    it('should call store action to retrieve user profile asynchronously', async () => {
      // given
      ctx.app.$apiService = $apiService;
      ctx.store = ({ dispatch: dispatchMock } as unknown) as Store<unknown>;
      const jobOfferSlugWrapper: Wrapper<Vue> = shallowMount(JobOfferSlug, { localVue, store, mocks: { $t, $route, $apiService } });

      // when
      // @ts-ignore
      await jobOfferSlugWrapper.vm.$options.asyncData(ctx);

      // then
      expect(dispatchMock).toHaveBeenNthCalledWith(1, 'user-profile-module/setUserProfile', { apiService: $apiService });
    });

    it('should call store action to retrieve job offer if there is a slug in route', async () => {
      // given
      ctx.app.$apiService = $apiService;
      ctx.params = { slug: 'aJobOfferId--a-job-offer-title' };
      ctx.store = ({ dispatch: dispatchMock } as unknown) as Store<unknown>;
      const jobOfferSlugWrapper: Wrapper<Vue> = shallowMount(JobOfferSlug, { localVue, store, mocks: { $t, $route, $apiService } });

      // when
      // @ts-ignore
      await jobOfferSlugWrapper.vm.$options.asyncData(ctx);

      // then
      expect(dispatchMock).toHaveBeenNthCalledWith(2, 'job-offers-module/setCurrentJobOffer', { apiService: $apiService, id: 'aJobOfferId' });
    });

    it('should not call store action to retrieve draft job offer if there is no slug in route', async () => {
      // given
      ctx.app.$apiService = $apiService;
      ctx.params = {};
      ctx.store = ({ dispatch: dispatchMock } as unknown) as Store<unknown>;
      const jobOfferSlugWrapper: Wrapper<Vue> = shallowMount(JobOfferSlug, { localVue, store, mocks: { $t, $route, $apiService } });

      // when
      // @ts-ignore
      jobOfferSlugWrapper.vm.$options.asyncData(ctx);

      // then
      expect(dispatchMock).toHaveBeenCalledTimes(1);
    });
  });

  describe('GbJobOfferViewHeader', () => {
    it('should have component with job-offer from store through getters', () => {
      // given
      getJobOfferMock.mockReturnValue({ title: 'A job title' } as JobOfferWithCompanyResponseInterface);
      const jobOfferSlugWrapper: Wrapper<Vue> = shallowMount(JobOfferSlug, { localVue, store, mocks: { $t, $route, $apiService } });

      // when
      const gbJobOfferViewHeaderWrapper: Wrapper<Vue> = jobOfferSlugWrapper.find(GbJobOfferViewHeader);

      // then
      // @ts-ignore
      expect(gbJobOfferViewHeaderWrapper.vm.jobOffer).toStrictEqual({
        title: 'A job title',
      });
    });
  });

  describe('GbJobOfferViewTopBlock', () => {
    it('should have component with job-offer from store through getters', () => {
      // given
      getJobOfferMock.mockReturnValue({ title: 'A job title' } as JobOfferWithCompanyResponseInterface);
      isLoggedAsCandidateMock.mockReturnValue(true);
      const jobOfferSlugWrapper: Wrapper<Vue> = shallowMount(JobOfferSlug, { localVue, store, mocks: { $t, $route, $apiService } });

      // when
      const gbJobOfferTopBlockWrapper: Wrapper<Vue> = jobOfferSlugWrapper.find(GbJobOfferViewTopBlock);

      // then
      // @ts-ignore
      expect(gbJobOfferTopBlockWrapper.vm.jobOffer).toStrictEqual({
        title: 'A job title',
      });
      // @ts-ignore
      expect(gbJobOfferTopBlockWrapper.vm.isLoggedAsCandidate).toBe(true);
    });

    it('should display it on 7 columns when not connected as candidate', () => {
      // given
      getJobOfferMock.mockReturnValue({ title: 'A job title' } as JobOfferWithCompanyResponseInterface);
      isLoggedAsCandidateMock.mockReturnValue(false);
      const jobOfferSlugWrapper: Wrapper<Vue> = shallowMount(JobOfferSlug, { localVue, store, mocks: { $t, $route, $apiService } });

      // when
      const jobOffersSlugContentWrapper: Wrapper<Vue> = jobOfferSlugWrapper.find('.job-offers-slug--top-block');

      // then
      expect(jobOffersSlugContentWrapper.element.classList).toContain('is-7');
    });

    it('should display it full when connected as candidate', () => {
      // given
      getJobOfferMock.mockReturnValue({ title: 'A job title' } as JobOfferWithCompanyResponseInterface);
      isLoggedAsCandidateMock.mockReturnValue(true);
      const jobOfferSlugWrapper: Wrapper<Vue> = shallowMount(JobOfferSlug, { localVue, store, mocks: { $t, $route, $apiService } });

      // when
      const jobOffersSlugContentWrapper: Wrapper<Vue> = jobOfferSlugWrapper.find('.job-offers-slug--top-block');

      // then
      expect(jobOffersSlugContentWrapper.element.classList).toContain('is-full');
    });
  });

  describe('GbJobOfferViewBottomBlock', () => {
    it('should have component with job-offer from store through getters when logged as a candidate', () => {
      // given
      getJobOfferMock.mockReturnValue({ title: 'A job title' } as JobOfferWithCompanyResponseInterface);
      isLoggedAsCandidateMock.mockReturnValue(true);
      const jobOfferSlugWrapper: Wrapper<Vue> = shallowMount(JobOfferSlug, { localVue, store, mocks: { $t, $route, $apiService } });

      // when
      const gbJobOfferViewBottomBlockWrapper: Wrapper<Vue> = jobOfferSlugWrapper.find(GbJobOfferViewBottomBlock);

      // then
      // @ts-ignore
      expect(gbJobOfferViewBottomBlockWrapper.vm.jobOffer).toStrictEqual({
        title: 'A job title',
      });
    });
    it('should not have component with job-offer from store through getters when not logged as a candidate', () => {
      // given
      getJobOfferMock.mockReturnValue({ title: 'A job title' } as JobOfferWithCompanyResponseInterface);
      isLoggedAsCandidateMock.mockReturnValue(false);
      const jobOfferSlugWrapper: Wrapper<Vue> = shallowMount(JobOfferSlug, { localVue, store, mocks: { $t, $route, $apiService } });

      // when
      const gbJobOfferViewBottomBlockWrapper: WrapperArray<Vue> = jobOfferSlugWrapper.findAll(GbJobOfferViewBottomBlock);

      // then
      expect(gbJobOfferViewBottomBlockWrapper.length).toBe(0);
    });
  });
});

import { mount, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import { HeaderResponseInterface } from '../../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';

import GbHeader from '../gb-header.vue';

describe('components/layout/gb-header', () => {
  let propsData: {
    value: HeaderResponseInterface;
  };
  beforeEach(() => {
    propsData = {
      value: {
        title: 'A header title',
        logo: {
          alternativeText: 'An awesome image',
          source: 'https://an-image.png',
        },
      } as HeaderResponseInterface,
    };
  });

  describe('should match snapshot', () => {
    it('with props', () => {
      // when
      const gbHeaderWrapper: Wrapper<Vue> = mount(GbHeader, { propsData });

      // then
      expect(gbHeaderWrapper.element).toMatchSnapshot();
    });
  });

  describe('header', () => {
    it('should contain a header logo', () => {
      // when
      const gbHeaderWrapper: Wrapper<Vue> = shallowMount(GbHeader, { propsData });

      // then
      const logoAttributes: { [name: string]: string } = gbHeaderWrapper.find('header img').attributes();
      expect(logoAttributes).toHaveProperty('src', 'https://an-image.png');
      expect(logoAttributes).toHaveProperty('alt', 'An awesome image');
    });
  });
});

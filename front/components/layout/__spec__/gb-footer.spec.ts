import { mount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import { FooterResponseInterface } from '../../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';

import GbFooter from '../gb-footer.vue';

describe('components/layout/gb-footer', () => {
  let propsData: {
    value: FooterResponseInterface;
  };
  beforeEach(() => {
    propsData = {
      value: {} as FooterResponseInterface,
    };
  });

  describe('should match snapshot', () => {
    it('with props', () => {
      // when
      const gbFooterWrapper: Wrapper<Vue> = mount(GbFooter, { propsData });

      // then
      expect(gbFooterWrapper.element).toMatchSnapshot();
    });
  });

  describe('footer', () => {
    it('should contain footer copyright', () => {
      // given
      const expected: string = 'A footer copyright';
      propsData.value.copyright = expected;

      // when
      const gbFooterWrapper: Wrapper<Vue> = mount(GbFooter, { propsData });

      // then
      expect(gbFooterWrapper.find('footer').text()).toBe(expected);
    });
  });
});

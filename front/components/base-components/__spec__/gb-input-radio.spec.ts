import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbErrorMessages from '~/components/base-components/gb-error-messages.vue';
import { DomainFieldErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbInputRadio from '../gb-input-radio.vue';

interface GbInputRadioValuePropsMock {
  optionLabelKey: string;
  optionValue: string;
}

describe('components/base-components/gb-input-radio', () => {
  let $t: jest.Mock;
  let propsData: {
    value: GbInputRadioValuePropsMock;
    options: {
      inputId: string;
      inputName: string;
      inputOptions: GbInputRadioValuePropsMock[];
      inputRequired?: boolean;
      inputLabelKey?: string;
      inputErrors?: DomainFieldErrorsResponseInterface;
    };
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: {} as GbInputRadioValuePropsMock,
      options: {
        inputId: 'field-id',
        inputName: 'field-name',
        inputOptions: [{} as GbInputRadioValuePropsMock],
      },
    };
  });

  describe('label', () => {
    it('should display legend', () => {
      // given
      propsData.options.inputLabelKey = 'message.key';
      $t.mockReturnValue('Label message value');

      // when
      const gbInputRadioWrapper: Wrapper<Vue> = shallowMount(GbInputRadio, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('message.key');
      expect(gbInputRadioWrapper.find('legend').text()).toBe('Label message value');
    });

    it('should display legend with * if required', () => {
      // given
      propsData.options.inputLabelKey = 'message.key';
      propsData.options.inputRequired = true;
      $t.mockReturnValue('Label message value');

      // when
      const gbInputRadioWrapper: Wrapper<Vue> = shallowMount(GbInputRadio, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('message.key');
      expect(gbInputRadioWrapper.find('legend').text()).toBe('Label message value*');
    });

    it('should not display legend when no label prop', () => {
      // given
      propsData.options.inputLabelKey = '';

      // when
      const gbInputRadioWrapper: Wrapper<Vue> = shallowMount(GbInputRadio, { mocks: { $t }, propsData });

      // then
      expect(gbInputRadioWrapper.findAll('legend')).toHaveLength(0);
    });
  });

  it('should display as many input tag as inputOptions', () => {
    // given
    propsData.options.inputOptions = [
      { optionLabelKey: 'labelKey1', optionValue: 'value1' },
      { optionLabelKey: 'labelKey2', optionValue: 'value2' },
    ];

    // when
    const gbInputRadioWrapper: Wrapper<Vue> = shallowMount(GbInputRadio, { mocks: { $t }, propsData });

    // then
    const inputWrappers: WrapperArray<Vue> = gbInputRadioWrapper.findAll('input');
    expect(inputWrappers).toHaveLength(2);

    expect(inputWrappers.at(0).attributes()).toHaveProperty('value', 'value1');
    expect(inputWrappers.at(1).attributes()).toHaveProperty('value', 'value2');
  });

  it('should display as many input tag as inputOptions with required attribute', () => {
    // given
    propsData.options.inputOptions = [
      { optionLabelKey: 'labelKey1', optionValue: 'value1' },
      { optionLabelKey: 'labelKey2', optionValue: 'value2' },
    ];
    propsData.options.inputRequired = true;

    // when
    const gbInputRadioWrapper: Wrapper<Vue> = shallowMount(GbInputRadio, { mocks: { $t }, propsData });

    // then
    const inputWrappers: WrapperArray<Vue> = gbInputRadioWrapper.findAll('input');
    expect(inputWrappers).toHaveLength(2);

    expect(inputWrappers.at(0).attributes()).toHaveProperty('required', 'required');
    expect(inputWrappers.at(1).attributes()).toHaveProperty('required', 'required');
  });

  it('should display as many label tag as inputOptions', () => {
    // given
    propsData.options.inputOptions = [
      { optionLabelKey: 'labelKey1', optionValue: 'value1' },
      { optionLabelKey: 'labelKey2', optionValue: 'value2' },
    ];

    $t.mockImplementation((selectLabelKey: string) => {
      return selectLabelKey === 'labelKey1' ? 'Label message value 1' : 'Label message value 2';
    });

    // when
    const gbInputRadioWrapper: Wrapper<Vue> = shallowMount(GbInputRadio, { mocks: { $t }, propsData });

    // then
    const labelWrappers: WrapperArray<Vue> = gbInputRadioWrapper.findAll('label');
    expect(labelWrappers).toHaveLength(2);

    expect(labelWrappers.at(0).text()).toBe('Label message value 1');
    expect(labelWrappers.at(1).text()).toBe('Label message value 2');

    expect($t).toHaveBeenCalledWith('labelKey1');
    expect($t).toHaveBeenCalledWith('labelKey2');
  });

  it('should set id attribute from options prop and index', () => {
    // given
    propsData.options.inputOptions = [
      { optionLabelKey: 'labelKey1', optionValue: 'value1' },
      { optionLabelKey: 'labelKey2', optionValue: 'value2' },
    ];

    // when
    const gbInputRadioWrapper: Wrapper<Vue> = shallowMount(GbInputRadio, { mocks: { $t }, propsData });

    // then
    const inputWrappers: WrapperArray<Vue> = gbInputRadioWrapper.findAll('input');
    expect(inputWrappers).toHaveLength(2);

    expect(inputWrappers.at(0).attributes()).toHaveProperty('id', 'field-id_1');
    expect(inputWrappers.at(1).attributes()).toHaveProperty('id', 'field-id_2');
  });

  it('should set name attribute from options prop', () => {
    // given
    propsData.options.inputOptions = [
      { optionLabelKey: 'labelKey1', optionValue: 'value1' },
      { optionLabelKey: 'labelKey2', optionValue: 'value2' },
    ];

    // when
    const gbInputRadioWrapper: Wrapper<Vue> = shallowMount(GbInputRadio, { mocks: { $t }, propsData });

    // then
    const inputWrappers: WrapperArray<Vue> = gbInputRadioWrapper.findAll('input');
    expect(inputWrappers).toHaveLength(2);

    expect(inputWrappers.at(0).attributes()).toHaveProperty('name', 'field-name');
    expect(inputWrappers.at(1).attributes()).toHaveProperty('name', 'field-name');
  });

  it('should bind input value to parent model when input event is triggered', () => {
    // given
    propsData.options.inputOptions = [
      { optionLabelKey: 'aLabelKey', optionValue: 'a label' },
      { optionLabelKey: 'aSecondLabelKey', optionValue: 'some text' },
    ];
    const gbInputRadioWrapper: Wrapper<Vue> = shallowMount(GbInputRadio, { mocks: { $t }, propsData });

    const inputField: Wrapper<Vue> = gbInputRadioWrapper.find('#field-id_1');
    const inputValue: string = 'a label';

    // when
    (inputField.element as HTMLInputElement).value = inputValue;
    inputField.trigger('change');

    // then
    expect(gbInputRadioWrapper.emitted().input).toStrictEqual([[inputValue]]);
  });

  it('should display GbErrorMessages component with input errors as props', () => {
    // given
    const expected: DomainFieldErrorsResponseInterface = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;
    propsData.options.inputErrors = expected;

    // when
    const gbInputRadioWrapper: Wrapper<Vue> = shallowMount(GbInputRadio, { mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputRadioWrapper.find(GbErrorMessages).vm.options).toStrictEqual({
      domainFieldErrors: expected,
    });
  });

  it('should not display any GbErrorMessages component when no errors', () => {
    // given
    propsData.options.inputErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbInputRadioWrapper: Wrapper<Vue> = shallowMount(GbInputRadio, { mocks: { $t }, propsData });

    // then
    expect(gbInputRadioWrapper.findAll(GbErrorMessages)).toHaveLength(0);
  });
});

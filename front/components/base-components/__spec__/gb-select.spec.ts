import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbErrorMessages from '~/components/base-components/gb-error-messages.vue';
import { DomainFieldErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbSelect from '../gb-select.vue';

describe('components/base-components/gb-select', () => {
  let $t: jest.Mock;
  let propsData: {
    value: string;
    options: {
      selectId: string;
      selectLabelKey: string;
      selectPlaceholderKey: string;
      selectRequired?: boolean;
      selectOptions: Array<{ optionLabelKey: string; optionValue: string }>;
      selectIsLarge?: boolean;
      selectErrors?: DomainFieldErrorsResponseInterface;
    };
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: '',
      options: {
        selectId: 'field-id',
        selectLabelKey: '',
        selectPlaceholderKey: '',
        selectOptions: [{ optionLabelKey: '', optionValue: '' }],
      },
    };
  });

  describe('label', () => {
    it('should display label for given id', () => {
      // given
      propsData.options.selectLabelKey = 'message.key';
      $t.mockReturnValue('Label message value');

      // when
      const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('message.key');
      expect(gbSelectWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should display label with asterisk when select required and no placeholder', () => {
      // given
      propsData.options.selectRequired = true;
      propsData.options.selectLabelKey = 'message.key';
      // @ts-ignore
      propsData.options.selectPlaceholderKey = undefined;
      $t.mockReturnValue('Label message value');

      // when
      const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

      // then
      expect(gbSelectWrapper.find('label[for="field-id"]').text()).toBe('Label message value*');
    });

    it('should display label without asterisk when select required and having a placeholder', () => {
      // given
      propsData.options.selectRequired = true;
      propsData.options.selectLabelKey = 'message.key';
      propsData.options.selectPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('Label message value');

      // when
      const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

      // then
      expect(gbSelectWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should not display label when no label prop', () => {
      // given
      propsData.options.selectLabelKey = '';

      // when
      const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

      // then
      expect(gbSelectWrapper.findAll('label[for="field-id"]')).toHaveLength(0);
    });
  });

  describe('placeholder', () => {
    it('should display placeholder in empty default option', () => {
      // given
      propsData.options.selectPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('A placeholder');

      // when
      const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
      expect(
        gbSelectWrapper
          .findAll('option')
          .at(0)
          .text()
      ).toBe('A placeholder');
    });

    it('should display placeholder in empty default option with asterisk if required', () => {
      // given
      propsData.options.selectRequired = true;
      propsData.options.selectPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('A placeholder');

      // when
      const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
      expect(
        gbSelectWrapper
          .findAll('option')
          .at(0)
          .text()
      ).toBe('A placeholder*');
    });
  });

  it('should display required select when required is true', () => {
    // given
    propsData.options.selectRequired = true;

    // when
    const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

    // then
    expect(gbSelectWrapper.find('select[id="field-id"]').attributes()).toHaveProperty('required', 'required');
  });

  it('should display optional select when required is false', () => {
    // given
    propsData.options.selectRequired = false;

    // when
    const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

    // then
    expect(gbSelectWrapper.find('select[id="field-id"]').attributes()).not.toHaveProperty('required');
  });

  it('should display default option with placeholder as text', () => {
    // when
    propsData.options.selectPlaceholderKey = 'i18n.placeholderKey';
    $t.mockReturnValue('A placeholder');
    const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

    // then
    const optionWrapper: Wrapper<Vue> = gbSelectWrapper.find('select[id="field-id"] option');
    expect(optionWrapper.text()).toBe('A placeholder');
    expect(optionWrapper.attributes()).toHaveProperty('value', '');
    expect(optionWrapper.attributes()).toHaveProperty('selected', 'selected');
    expect(optionWrapper.attributes()).toHaveProperty('disabled', 'disabled');
    expect(optionWrapper.attributes()).toHaveProperty('hidden', 'hidden');
  });

  it('should display as many options as given', () => {
    // given
    propsData.options.selectOptions = [
      { optionLabelKey: 'labelKey1', optionValue: 'value1' },
      { optionLabelKey: 'labelKey2', optionValue: 'value2' },
    ];

    $t.mockImplementation((selectLabelKey: string) => {
      return selectLabelKey === 'labelKey1' ? 'Label message value 1' : 'Label message value 2';
    });

    // when
    const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

    // then
    const optionWrappers: WrapperArray<Vue> = gbSelectWrapper.findAll('select[id="field-id"] option:not([selected])');
    expect(optionWrappers).toHaveLength(2);

    expect(optionWrappers.at(0).attributes()).toHaveProperty('value', 'value1');
    expect(optionWrappers.at(0).text()).toBe('Label message value 1');

    expect(optionWrappers.at(1).attributes()).toHaveProperty('value', 'value2');
    expect(optionWrappers.at(1).text()).toBe('Label message value 2');

    expect($t).toHaveBeenCalledWith('labelKey1');
    expect($t).toHaveBeenCalledWith('labelKey2');
  });

  it('should add gb-large class when large prop is true', () => {
    // given
    propsData.options.selectIsLarge = true;

    // when
    const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

    // then
    expect(gbSelectWrapper.find('select').attributes()).toHaveProperty('class', 'gb-select gb-large');
  });

  it('should add gb-error class when having errors', () => {
    // given
    propsData.options.selectErrors = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;

    // when
    const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

    // then
    expect(gbSelectWrapper.find('select').attributes()).toHaveProperty('class', 'gb-select gb-error');
  });

  it('should not add any additional class when large prop is false and having no error', () => {
    // given
    propsData.options.selectIsLarge = false;
    propsData.options.selectErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

    // then
    expect(gbSelectWrapper.find('select').attributes()).toHaveProperty('class', 'gb-select');
  });

  it('should bind value from props to select element value', () => {
    // given
    propsData.options.selectOptions = [
      { optionLabelKey: 'labelKey1', optionValue: 'value1' },
      { optionLabelKey: 'labelKey2', optionValue: 'value2' },
    ];
    propsData.value = propsData.options.selectOptions[1].optionValue;

    // when
    const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

    // then
    expect((gbSelectWrapper.find('#field-id').element as HTMLSelectElement).value).toBe('value2');
  });

  it('should emit input event with string value when select value has been updated', () => {
    // given
    propsData.options.selectOptions = [
      { optionLabelKey: 'labelKey1', optionValue: 'value1' },
      { optionLabelKey: 'labelKey2', optionValue: 'value2' },
    ];
    const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

    const selectedValue: string = propsData.options.selectOptions[0].optionValue;

    // when
    (gbSelectWrapper.find(`option[value="${selectedValue}"]`).element as HTMLOptionElement).selected = true;
    gbSelectWrapper.find('#field-id').trigger('change');

    // then
    expect(gbSelectWrapper.emitted().input).toStrictEqual([[selectedValue]]);
  });

  it('should display GbErrorMessages component with input errors as props', () => {
    // given
    const expected: DomainFieldErrorsResponseInterface = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;
    propsData.options.selectErrors = expected;

    // when
    const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbSelectWrapper.find(GbErrorMessages).vm.options).toStrictEqual({
      domainFieldErrors: expected,
    });
  });

  it('should not display any GbErrorMessages component when no errors', () => {
    // given
    propsData.options.selectErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbSelectWrapper: Wrapper<Vue> = shallowMount(GbSelect, { mocks: { $t }, propsData });

    // then
    expect(gbSelectWrapper.findAll(GbErrorMessages)).toHaveLength(0);
  });
});

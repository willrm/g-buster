import { createLocalVue, shallowMount, Stubs, Wrapper } from '@vue/test-utils';
import { VPopover } from 'v-tooltip';
import Vue from 'vue';
import GbIcon from '../gb-icon.vue';
import GbTooltip from '../gb-tooltip.vue';

describe('components/base-components/gb-tooltip', () => {
  let $t: jest.Mock;
  let propsData: {
    value: string;
    options: {
      iconName: string;
    };
  };

  let stubs: Stubs;
  let localVue: typeof Vue;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.component('v-popover', VPopover);
  });

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: '',
      options: {
        iconName: '',
      },
    };
    stubs = { 'client-only': true };
  });

  describe('GbIcon', () => {
    it('should contain a GbIcon with icon from options prop', () => {
      // given
      propsData.options.iconName = 'information';

      // when
      const gbTooltipWrapper: Wrapper<Vue> = shallowMount(GbTooltip, { localVue, stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbTooltipWrapper.find(GbIcon).vm.options).toStrictEqual({ iconName: 'information' });
    });
  });
});

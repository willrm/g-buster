import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbErrorMessages from '~/components/base-components/gb-error-messages.vue';
import { DomainFieldErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbTextarea from '../gb-textarea.vue';

describe('components/base-components/gb-textarea', () => {
  let $t: jest.Mock;
  let propsData: {
    value: string;
    options: {
      textareaId: string;
      textareaLabelKey: string;
      textareaPlaceholderKey: string;
      textareaRequired?: boolean;
      textareaMaxLength?: number;
      textareaIsLarge?: boolean;
      textareaErrors?: DomainFieldErrorsResponseInterface;
    };
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: '',
      options: {
        textareaId: 'field-id',
        textareaLabelKey: '',
        textareaPlaceholderKey: '',
      },
    };
  });

  describe('label', () => {
    it('should display label for given id', () => {
      // given
      propsData.options.textareaLabelKey = 'message.key';
      $t.mockReturnValue('Label message value');

      // when
      const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('message.key');
      expect(gbTextareaWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should display label with asterisk when input required and no placeholder', () => {
      // given
      propsData.options.textareaRequired = true;
      propsData.options.textareaLabelKey = 'message.key';
      // @ts-ignore
      propsData.options.textareaPlaceholderKey = undefined;
      $t.mockReturnValue('Label message value');

      // when
      const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

      // then
      expect(gbTextareaWrapper.find('label[for="field-id"]').text()).toBe('Label message value*');
    });

    it('should display label without asterisk when textarea required and having a placeholder', () => {
      // given
      propsData.options.textareaRequired = true;
      propsData.options.textareaLabelKey = 'message.key';
      propsData.options.textareaPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('Label message value');

      // when
      const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

      // then
      expect(gbTextareaWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should not display label when no label prop', () => {
      // given
      propsData.options.textareaLabelKey = '';

      // when
      const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

      // then
      expect(gbTextareaWrapper.findAll('label[for="field-id"]')).toHaveLength(0);
    });
  });

  describe('placeholder', () => {
    it('should display textarea with placeholder', () => {
      // given
      propsData.options.textareaPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('A placeholder');

      // when
      const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
      expect(gbTextareaWrapper.find('textarea').attributes()).toHaveProperty('placeholder', 'A placeholder');
    });

    it('should display textarea with placeholder with asterisk if required', () => {
      // given
      propsData.options.textareaRequired = true;
      propsData.options.textareaPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('A placeholder');

      // when
      const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
      expect(gbTextareaWrapper.find('textarea').attributes()).toHaveProperty('placeholder', 'A placeholder*');
    });

    it('should not display placeholder when no placeholder prop', () => {
      // given
      propsData.options.textareaPlaceholderKey = '';

      // when
      const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

      // then
      expect(gbTextareaWrapper.find('textarea').attributes()).toHaveProperty('placeholder', '');
    });
  });

  it('should display required textarea when required is true', () => {
    // given
    propsData.options.textareaRequired = true;

    // when
    const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

    // then
    expect(gbTextareaWrapper.find('textarea[id="field-id"]').attributes()).toHaveProperty('required', 'required');
  });

  it('should display optional textarea when required is false', () => {
    // given
    propsData.options.textareaRequired = false;

    // when
    const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

    // then
    expect(gbTextareaWrapper.find('textarea[id="field-id"]').attributes()).not.toHaveProperty('required');
  });

  it('should display textarea with maxlength constraint when value specified', () => {
    // given
    propsData.options.textareaMaxLength = 1337;

    // when
    const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

    // then
    expect(gbTextareaWrapper.find('textarea[id="field-id"]').attributes()).toHaveProperty('maxlength', '1337');
  });

  it('should display textarea without maxlength constraint when no value specified', () => {
    // when
    const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

    // then
    expect(gbTextareaWrapper.find('textarea[id="field-id"]').attributes()).not.toHaveProperty('maxlength');
  });

  it('should add gb-large class when large prop is true', () => {
    // given
    propsData.options.textareaIsLarge = true;

    // when
    const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

    // then
    expect(gbTextareaWrapper.find('textarea').attributes()).toHaveProperty('class', 'gb-textarea--textarea gb-large');
  });

  it('should add gb-error class when having errors', () => {
    // given
    propsData.options.textareaErrors = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;

    // when
    const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

    // then
    expect(gbTextareaWrapper.find('textarea').attributes()).toHaveProperty('class', 'gb-textarea--textarea gb-error');
  });

  it('should not add any additional class when large prop is false and having no error', () => {
    // given
    propsData.options.textareaIsLarge = false;
    propsData.options.textareaErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

    // then
    expect(gbTextareaWrapper.find('textarea').attributes()).toHaveProperty('class', 'gb-textarea--textarea');
  });

  it('should bind value from props to input element value', () => {
    // given
    propsData.value = 'existing\ndata';

    // when
    const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

    // then
    expect((gbTextareaWrapper.find('#field-id').element as HTMLInputElement).value).toBe('existing\ndata');
  });

  it('should emit input event with string value when input value has been updated', () => {
    // given
    const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

    const textareaField: Wrapper<Vue> = gbTextareaWrapper.find('#field-id');
    const textareaValue: string = 'some text\nwith multiple lines';

    // when
    (textareaField.element as HTMLTextAreaElement).value = textareaValue;
    textareaField.trigger('input');

    // then
    expect(gbTextareaWrapper.emitted().input).toStrictEqual([[textareaValue]]);
  });

  it('should display GbErrorMessages component with input errors as props', () => {
    // given
    const expected: DomainFieldErrorsResponseInterface = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;
    propsData.options.textareaErrors = expected;

    // when
    const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbTextareaWrapper.find(GbErrorMessages).vm.options).toStrictEqual({
      domainFieldErrors: expected,
    });
  });

  it('should not display any GbErrorMessages component when no errors', () => {
    // given
    propsData.options.textareaErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbTextareaWrapper: Wrapper<Vue> = shallowMount(GbTextarea, { mocks: { $t }, propsData });

    // then
    expect(gbTextareaWrapper.findAll(GbErrorMessages)).toHaveLength(0);
  });
});

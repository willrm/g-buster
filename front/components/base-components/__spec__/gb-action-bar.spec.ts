import { mount, Slots, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbActionBar from '../gb-action-bar.vue';

describe('components/base-components/gb-action-bar', () => {
  describe('should match snapshot', () => {
    it('with no configuration', () => {
      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbActionBar, {});

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });

    it('with slots', () => {
      // given
      const slots: Slots = {
        left: '<div>LEFT</div>',
        right: '<div>RIGHT</div>',
      };

      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbActionBar, { slots });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });
  });
});

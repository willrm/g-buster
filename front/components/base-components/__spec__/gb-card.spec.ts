import { mount, Slots, Wrapper } from '@vue/test-utils';
import Vue from 'vue';

import GbCard from '../gb-card.vue';

describe('components/base-components/gb-card', () => {
  let propsData: {
    options: { cardHasError: boolean };
  };
  beforeEach(() => {
    propsData = {
      options: {
        cardHasError: false,
      },
    };
  });

  describe('should match snapshot', () => {
    it('with no configuration', () => {
      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbCard, { propsData });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });

    it('with slots', () => {
      // given
      const slots: Slots = {
        header: '<div>HEADER</div>',
        subheader: '<div>SUBHEADER</div>',
        content: '<div>CONTENT</div>',
      };

      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbCard, { slots, propsData });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });

    it('with errors', () => {
      // given
      propsData.options.cardHasError = true;

      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbCard, { propsData });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });
  });
});

import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { DomainFieldErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbErrorMessages from '../gb-error-messages.vue';

describe('components/base-components/gb-error-messages', () => {
  let $t: jest.Mock;
  let propsData: {
    options: {
      domainFieldErrors: DomainFieldErrorsResponseInterface;
    };
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      options: {
        domainFieldErrors: {} as DomainFieldErrorsResponseInterface,
      },
    };
  });

  it('should display GbIcon with warning icon name', () => {
    // when
    const gbErrorMessagesWrapper: Wrapper<Vue> = shallowMount(GbErrorMessages, { mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbErrorMessagesWrapper.find(GbIcon).vm.options).toStrictEqual({
      iconName: 'warning_circle_red',
      iconAlternativeTextKey: 'error',
    });
  });

  it('should display as many translated message as items in i18nMessageKeys', () => {
    // given
    propsData.options.domainFieldErrors = {
      messages: [],
      i18nMessageKeys: ['message.key1', 'message.key2'],
    } as DomainFieldErrorsResponseInterface;
    $t.mockReturnValueOnce('Message 1').mockReturnValueOnce('Message 2');

    // when
    const gbErrorMessagesWrapper: Wrapper<Vue> = shallowMount(GbErrorMessages, { mocks: { $t }, propsData });

    // then
    expect($t).toHaveBeenCalledWith('message.key1');
    expect($t).toHaveBeenCalledWith('message.key2');

    const messages: WrapperArray<Vue> = gbErrorMessagesWrapper.findAll('span');
    expect(messages).toHaveLength(2);
    expect(messages.at(0).text()).toBe('Message 1');
    expect(messages.at(1).text()).toBe('Message 2');
  });
});

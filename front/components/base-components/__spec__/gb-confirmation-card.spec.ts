import { mount, Slots, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbConfirmationCard from '~/components/base-components/gb-confirmation-card.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';

describe('components/base-components/gb-confirmation-card', () => {
  let $t: jest.Mock;

  beforeEach(() => {
    $t = jest.fn();
  });

  describe('should match snapshot', () => {
    it('with no configuration', () => {
      // when
      const gbConfirmationCardWrapper: Wrapper<Vue> = mount(GbConfirmationCard, { mocks: { $t } });

      // then
      expect(gbConfirmationCardWrapper.element).toMatchSnapshot();
    });

    it('with slots', () => {
      // given
      const slots: Slots = {
        header: '<div>HEADER</div>',
        subheader: '<div>SUBHEADER</div>',
        content: '<div>CONTENT</div>',
        actions: '<div>ACTIONS</div>',
      };

      // when
      const gbConfirmationCardWrapper: Wrapper<Vue> = mount(GbConfirmationCard, { slots, mocks: { $t } });

      // then
      expect(gbConfirmationCardWrapper.element).toMatchSnapshot();
    });
  });

  it('should display GbIcon component with props for validation icon', () => {
    // given
    const expected: object = { iconName: 'valid' };

    // when
    const gbConfirmationCardWrapper: Wrapper<Vue> = mount(GbConfirmationCard, { mocks: { $t } });

    // then
    // @ts-ignore
    expect(gbConfirmationCardWrapper.find(GbIcon).vm.options).toStrictEqual(expected);
  });
});

import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import { DomainFieldErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbInputCheckbox from '../gb-input-checkbox.vue';

describe('components/base-components/gb-input-checkbox', () => {
  let $t: jest.Mock;
  let propsData: {
    value: boolean;
    options: {
      inputId: string;
      inputName: string;
      inputLabelKey: string;
      inputErrors?: DomainFieldErrorsResponseInterface;
    };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: false,
      options: {
        inputId: 'field-id',
        inputName: 'field-name',
        inputLabelKey: '',
      },
    };
  });

  it('should display input with type checkbox', () => {
    // when
    const gbInputCheckboxWrapper: Wrapper<Vue> = shallowMount(GbInputCheckbox, { mocks: { $t }, propsData });

    // then
    expect(gbInputCheckboxWrapper.find('input').attributes()).toHaveProperty('type', 'checkbox');
  });

  it('should display label in input text content', () => {
    // given
    propsData.options.inputLabelKey = 'message.key';
    $t.mockReturnValue('Label message value');

    // when
    const gbInputCheckboxWrapper: Wrapper<Vue> = shallowMount(GbInputCheckbox, { mocks: { $t }, propsData });

    // then
    expect($t).toHaveBeenCalledWith('message.key');
    expect(gbInputCheckboxWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
  });

  it('should set name in input', () => {
    // given
    propsData.options.inputName = 'field-name';

    // when
    const gbInputCheckboxWrapper: Wrapper<Vue> = shallowMount(GbInputCheckbox, { mocks: { $t }, propsData });

    // then
    expect(gbInputCheckboxWrapper.find('input').attributes()).toHaveProperty('name', 'field-name');
  });

  describe('on click', () => {
    it('should bind input value to parent model when click event is triggered for false input prop', () => {
      // given
      propsData.value = false;

      const gbInputCheckboxWrapper: Wrapper<Vue> = shallowMount(GbInputCheckbox, { mocks: { $t }, propsData });
      const inputField: Wrapper<Vue> = gbInputCheckboxWrapper.find('input');

      // when
      inputField.trigger('click');

      // then
      expect(gbInputCheckboxWrapper.emitted().input).toStrictEqual([[true]]);
    });

    it('should bind input value to parent model when click event is triggered for true input prop', () => {
      // given
      propsData.value = true;

      const gbInputCheckboxWrapper: Wrapper<Vue> = shallowMount(GbInputCheckbox, { mocks: { $t }, propsData });
      const inputField: Wrapper<Vue> = gbInputCheckboxWrapper.find('input');

      // when
      inputField.trigger('click');

      // then
      expect(gbInputCheckboxWrapper.vm.$data.inputValue).toBeFalsy();
      expect(gbInputCheckboxWrapper.emitted().input).toStrictEqual([[false]]);
    });
  });
});

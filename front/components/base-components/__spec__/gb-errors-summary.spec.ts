import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbErrorsSummary from '../gb-errors-summary.vue';

describe('components/base-components/gb-errors-summary', () => {
  let $t: jest.Mock;
  let propsData: {
    options: {
      domainI18nPath: string;
      domainErrors: DomainErrorsResponseInterface;
    };
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      options: {
        domainI18nPath: '',
        domainErrors: {} as DomainErrorsResponseInterface,
      },
    };
  });

  it('should display GbIcon with warning icon name', () => {
    // when
    const gbErrorsSummaryWrapper: Wrapper<Vue> = shallowMount(GbErrorsSummary, { mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbErrorsSummaryWrapper.find(GbIcon).vm.options).toStrictEqual({
      iconName: 'warning_red',
      iconAlternativeTextKey: 'error',
    });
  });

  it('should display errors summary header translation', () => {
    // given
    $t.mockReturnValue('Errors summary');

    // when
    const gbErrorsSummaryWrapper: Wrapper<Vue> = shallowMount(GbErrorsSummary, { mocks: { $t }, propsData });

    // then
    expect($t).toHaveBeenCalledWith('errorsSummary.header');
    expect(gbErrorsSummaryWrapper.text()).toContain('Errors summary');
  });

  it('should display as many translated error as keys in domainErrors', () => {
    // given
    propsData.options.domainI18nPath = 'pathPrefix';
    propsData.options.domainErrors = {
      key1: {} as DomainFieldErrorsResponseInterface,
      key2: {} as DomainFieldErrorsResponseInterface,
    } as DomainErrorsResponseInterface;

    $t.mockImplementation((domainErrorKey: string) => {
      return domainErrorKey === 'pathPrefix.key1' ? 'Field 1' : 'Field 2';
    });

    // when
    const gbErrorsSummaryWrapper: Wrapper<Vue> = shallowMount(GbErrorsSummary, { mocks: { $t }, propsData });

    // then
    expect($t).toHaveBeenCalledWith('pathPrefix.key1');
    expect($t).toHaveBeenCalledWith('pathPrefix.key2');

    const messages: WrapperArray<Vue> = gbErrorsSummaryWrapper.findAll('li');
    expect(messages).toHaveLength(2);
    expect(messages.at(0).text()).toBe('Field 1');
    expect(messages.at(1).text()).toBe('Field 2');
  });
});

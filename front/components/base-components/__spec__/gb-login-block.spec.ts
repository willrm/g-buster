import { mount, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbButton from '~/components/base-components/gb-button.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbLoginBlock from '~/components/base-components/gb-login-block.vue';

describe('components/job-offers/gb-login-block', () => {
  let $t: jest.Mock;
  let propsData: {
    options: {
      isLarge?: boolean;
    };
  };

  beforeEach(() => {
    window.location.replace = jest.fn();
    $t = jest.fn();

    propsData = {
      options: {},
    };
  });

  describe('is large', () => {
    it('should have large class apply', () => {
      // given
      propsData.options.isLarge = true;

      // when
      const gbLoginBlockWrapper: Wrapper<Vue> = shallowMount(GbLoginBlock, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbLoginBlockWrapper.find('.gb-login-block').element.classList).toContain('gb-login-block--is-large');
    });
  });

  describe('padlock logo', () => {
    it('should display padlock icon', () => {
      // when
      const gbLoginBlockWrapper: Wrapper<Vue> = shallowMount(GbLoginBlock, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbLoginBlockWrapper.findAll(GbIcon).at(0).vm.options).toStrictEqual({
        iconName: 'padlock_grey',
      });
    });
  });

  describe('information text', () => {
    it('should display i18n information text', () => {
      // given
      const expected: string = 'Connection block text';
      $t.mockReturnValue(expected);

      const gbLoginBlockWrapper: Wrapper<Vue> = shallowMount(GbLoginBlock, { mocks: { $t }, propsData });

      // when
      const result: string = gbLoginBlockWrapper.find('.gb-login-block--text').text();

      // then
      expect($t).toHaveBeenCalledWith('loginBlock');
      expect(result).toBe(expected);
    });
  });

  describe('login button', () => {
    it('should display login button', () => {
      // when
      const gbLoginBlockWrapper: Wrapper<Vue> = shallowMount(GbLoginBlock, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbLoginBlockWrapper.findAll(GbButton).at(0).vm.options).toMatchObject({
        buttonLabelKey: 'login',
      });
    });

    it('should redirect on login endpoint with current route as redirect url when click on it', () => {
      // given
      const $route: object = { fullPath: '/route/full/path' };
      const gbLoginBlockWrapper: Wrapper<Vue> = mount(GbLoginBlock, { mocks: { $t, $route }, propsData });

      // when
      gbLoginBlockWrapper.find(GbButton).trigger('click');

      // then
      expect(window.location.replace).toHaveBeenCalledWith('/api/user/login?redirectUrl=/route/full/path');
    });
  });
});

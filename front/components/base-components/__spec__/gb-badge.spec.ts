import { mount, Slots, Wrapper } from '@vue/test-utils';
import Vue from 'vue';

import GbBadge from '../gb-badge.vue';

enum GbBadgeColor {
  SUCCESS_COLOR = 'SUCCESS_COLOR',
  NEUTRAL_COLOR = 'NEUTRAL_COLOR',
  B2C_COLOR = 'B2C_COLOR',
  B2B_COLOR = 'B2B_COLOR',
}

describe('components/base-components/gb-badge', () => {
  let propsData: {
    options: {
      badgeIsRounded?: boolean;
      badgeIsHighlighted?: boolean;
      badgeColor?: GbBadgeColor;
    };
  };

  beforeEach(() => {
    propsData = { options: {} };
  });

  describe('should match snapshot', () => {
    it('with no slot', () => {
      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbBadge, { propsData });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });

    it('with slots', () => {
      // given
      const slots: Slots = {
        icon: '<img src="path/to/img.png" alt="an image"/>',
        default: '<span>Text</span>',
      };

      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbBadge, { slots, propsData });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });

    it('when rounded', () => {
      // given
      propsData.options.badgeIsRounded = true;

      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbBadge, { propsData });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });

    it('when highlighted', () => {
      // given
      propsData.options.badgeIsHighlighted = true;

      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbBadge, { propsData });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });

    it('when no color defined it should use b2c', () => {
      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbBadge, { propsData });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });

    it('when success color defined', () => {
      // given
      propsData.options.badgeColor = GbBadgeColor.SUCCESS_COLOR;

      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbBadge, { propsData });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });

    it('when neutral color defined', () => {
      // given
      propsData.options.badgeColor = GbBadgeColor.NEUTRAL_COLOR;

      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbBadge, { propsData });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });

    it('when b2c color defined', () => {
      // given
      propsData.options.badgeColor = GbBadgeColor.B2C_COLOR;

      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbBadge, { propsData });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });

    it('when b2b color defined', () => {
      // given
      propsData.options.badgeColor = GbBadgeColor.B2B_COLOR;

      // when
      const gbCardWrapper: Wrapper<Vue> = mount(GbBadge, { propsData });

      // then
      expect(gbCardWrapper.element).toMatchSnapshot();
    });
  });
});

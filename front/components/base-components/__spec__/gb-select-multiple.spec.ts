import { mount, shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import Multiselect from 'vue-multiselect';
import GbErrorMessages from '~/components/base-components/gb-error-messages.vue';
import { DomainFieldErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbSelectMultiple from '../gb-select-multiple.vue';

enum GbSelectMultipleBoxColor {
  B2C_COLOR = 'B2C_COLOR',
  B2B_COLOR = 'B2B_COLOR',
}

describe('components/base-components/gb-select-multiple', () => {
  let $t: jest.Mock;
  let propsData: {
    value: string[];
    options: {
      selectId: string;
      selectLabelKey: string;
      selectPlaceholderKey: string;
      selectRequired?: boolean;
      selectOptions: Array<{ optionLabelKey: string; optionValue: string }>;
      selectErrors?: DomainFieldErrorsResponseInterface;
      selectIsLarge?: boolean;
      selectBoxColor?: GbSelectMultipleBoxColor;
    };
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: [],
      options: {
        selectId: 'field-id',
        selectLabelKey: '',
        selectPlaceholderKey: '',
        selectOptions: [{ optionLabelKey: '', optionValue: '' }],
      },
    };
  });

  describe('label', () => {
    it('should display label for given id', () => {
      // given
      propsData.options.selectLabelKey = 'message.key';
      $t.mockReturnValue('Label message value');

      // when
      const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('message.key');
      expect(gbSelectMultipleWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should display label with asterisk when select required and no placeholder', () => {
      // given
      propsData.options.selectRequired = true;
      propsData.options.selectLabelKey = 'message.key';
      // @ts-ignore
      propsData.options.selectPlaceholderKey = undefined;
      $t.mockReturnValue('Label message value');

      // when
      const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

      // then
      expect(gbSelectMultipleWrapper.find('label[for="field-id"]').text()).toBe('Label message value*');
    });

    it('should display label without asterisk when select required and having a placeholder', () => {
      // given
      propsData.options.selectRequired = true;
      propsData.options.selectLabelKey = 'message.key';
      propsData.options.selectPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('Label message value');

      // when
      const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

      // then
      expect(gbSelectMultipleWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should not display label when no label prop', () => {
      // given
      propsData.options.selectLabelKey = '';

      // when
      const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

      // then
      expect(gbSelectMultipleWrapper.findAll('label[for="field-id"]')).toHaveLength(0);
    });
  });

  describe('GbErrorMessages', () => {
    it('should display GbErrorMessages component with input errors as props', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: [],
        messages: [],
      } as DomainFieldErrorsResponseInterface;
      propsData.options.selectErrors = expected;

      // when
      const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbSelectMultipleWrapper.find(GbErrorMessages).vm.options).toStrictEqual({
        domainFieldErrors: expected,
      });
    });

    it('should not display any GbErrorMessages component when no errors', () => {
      // given
      propsData.options.selectErrors = {} as DomainFieldErrorsResponseInterface;

      // when
      const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

      // then
      expect(gbSelectMultipleWrapper.findAll(GbErrorMessages)).toHaveLength(0);
    });
  });

  describe('multiselect', () => {
    beforeEach(() => {
      propsData.options.selectOptions = [
        { optionLabelKey: 'optionLabelKey1', optionValue: 'optionValue1' },
        { optionLabelKey: 'optionLabelKey2', optionValue: 'optionValue2' },
        { optionLabelKey: 'optionLabelKey3', optionValue: 'optionValue3' },
      ];

      $t.mockImplementation((optionLabelKey: string) => {
        if (optionLabelKey === 'optionLabelKey1') return 'Label 1';
        if (optionLabelKey === 'optionLabelKey2') return 'Label 2';
        if (optionLabelKey === 'optionLabelKey3') return 'Label 3';

        return null;
      });
    });

    it('should match snapshot', () => {
      // when
      const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

      // then
      expect(gbSelectMultipleWrapper.element).toMatchSnapshot();
    });

    it('should display Multiselect component with basic props for multi-selection', () => {
      // when
      const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

      // then
      const multiselectWrapper: Wrapper<Vue> = gbSelectMultipleWrapper.find(Multiselect);
      // @ts-ignore
      expect(multiselectWrapper.vm.trackBy).toBe('optionValue');
      // @ts-ignore
      expect(multiselectWrapper.vm.closeOnSelect).toBe(false);
      // @ts-ignore
      expect(multiselectWrapper.vm.id).toBe('field-id');
      // @ts-ignore
      expect(multiselectWrapper.vm.multiple).toBe(true);
      // @ts-ignore
      expect(multiselectWrapper.vm.preserveSearch).toBe(true);
      // @ts-ignore
      expect(multiselectWrapper.vm.showLabels).toBe(false);
    });

    it('should bind options to Multiselect component', () => {
      // when
      const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbSelectMultipleWrapper.find(Multiselect).vm.options).toStrictEqual([
        {
          optionLabelKey: 'optionLabelKey1',
          optionValue: 'optionValue1',
        },
        {
          optionLabelKey: 'optionLabelKey2',
          optionValue: 'optionValue2',
        },
        {
          optionLabelKey: 'optionLabelKey3',
          optionValue: 'optionValue3',
        },
      ]);
    });

    it('should bind selected options to Multiselect component', () => {
      // given
      propsData.value = ['optionValue1', 'optionValue3'];

      // when
      const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbSelectMultipleWrapper.find(Multiselect).vm.value).toStrictEqual([
        {
          optionLabelKey: 'optionLabelKey1',
          optionValue: 'optionValue1',
        },
        {
          optionLabelKey: 'optionLabelKey3',
          optionValue: 'optionValue3',
        },
      ]);
    });

    it('should display translated label for each option', () => {
      // when
      const gbSelectMultipleWrapper: Wrapper<Vue> = mount(GbSelectMultiple, { mocks: { $t }, propsData });

      // then
      const optionLabels: WrapperArray<Vue> = gbSelectMultipleWrapper.findAll('.gb-select-multiple--option-label');
      expect(optionLabels.at(0).text()).toBe('Label 1');
      expect(optionLabels.at(1).text()).toBe('Label 2');
      expect(optionLabels.at(2).text()).toBe('Label 3');
    });

    it('should display selected items with translated labels separated by a semicolon', () => {
      // given
      propsData.value = ['optionValue1', 'optionValue3'];

      // when
      const gbSelectMultipleWrapper: Wrapper<Vue> = mount(GbSelectMultiple, { mocks: { $t }, propsData });

      // then
      expect(gbSelectMultipleWrapper.find('.gb-select-multiple--selection').text()).toBe('Label 1 ; Label 3');
    });

    it('should configure custom label for autocompletion', () => {
      // given
      const gbSelectMultipleWrapper: Wrapper<Vue> = mount(GbSelectMultiple, { mocks: { $t }, propsData });

      // when
      // @ts-ignore
      const result: string = gbSelectMultipleWrapper.find(Multiselect).vm.customLabel({
        optionLabelKey: 'optionLabelKey2',
        optionValue: 'optionValue2',
      });

      // then
      // @ts-ignore
      expect(result).toBe('Label 2');
    });

    describe('placeholder', () => {
      it('should bind placeholder to Multiselect component', () => {
        // given
        propsData.options.selectPlaceholderKey = 'i18n.placeholderKey';
        $t.mockReturnValue('A placeholder');

        // when
        const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

        // then
        expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
        // @ts-ignore
        expect(gbSelectMultipleWrapper.find(Multiselect).vm.placeholder).toBe('A placeholder');
      });

      it('should bind placeholder to Multiselect component with asterisk if required', () => {
        // given
        propsData.options.selectRequired = true;
        propsData.options.selectPlaceholderKey = 'i18n.placeholderKey';
        $t.mockReturnValue('A placeholder');

        // when
        const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

        // then
        expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
        // @ts-ignore
        expect(gbSelectMultipleWrapper.find(Multiselect).vm.placeholder).toBe('A placeholder*');
      });
    });

    describe('selectErrors', () => {
      it('should add gb-error class when having errors', () => {
        // given
        propsData.options.selectErrors = {
          i18nMessageKeys: [],
          messages: [],
        } as DomainFieldErrorsResponseInterface;

        // when
        const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

        // then
        // @ts-ignore
        expect(gbSelectMultipleWrapper.find(Multiselect).attributes()).toHaveProperty('class', 'gb-error');
      });

      it('should not add any additional class when having no error', () => {
        // given
        propsData.options.selectErrors = {} as DomainFieldErrorsResponseInterface;

        // when
        const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

        // then
        expect(gbSelectMultipleWrapper.find(Multiselect).attributes()).toHaveProperty('class', '');
      });
    });

    describe('selectIsLarge', () => {
      it('should add gb-large class when select is large', () => {
        // given
        propsData.options.selectIsLarge = true;

        // when
        const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

        // then
        // @ts-ignore
        expect(gbSelectMultipleWrapper.find(Multiselect).attributes()).toHaveProperty('class', 'gb-large');
      });

      it('should not add any additional class when select is not large', () => {
        // given
        propsData.options.selectIsLarge = false;

        // when
        const gbSelectMultipleWrapper: Wrapper<Vue> = shallowMount(GbSelectMultiple, { mocks: { $t }, propsData });

        // then
        expect(gbSelectMultipleWrapper.find(Multiselect).attributes()).toHaveProperty('class', '');
      });
    });

    describe('input', () => {
      it('should emit input event with selected values when input event from Multiselect', () => {
        // given
        const gbSelectMultipleWrapper: Wrapper<Vue> = mount(GbSelectMultiple, { mocks: { $t }, propsData });
        const selectedOptions: Array<{ optionLabelKey: string; optionValue: string }> = [
          {
            optionLabelKey: 'optionLabelKey1',
            optionValue: 'optionValue1',
          },
          {
            optionLabelKey: 'optionLabelKey3',
            optionValue: 'optionValue3',
          },
        ];

        // when
        gbSelectMultipleWrapper.find(Multiselect).vm.$emit('input', selectedOptions);

        // then
        expect(gbSelectMultipleWrapper.emitted().input).toStrictEqual([[['optionValue1', 'optionValue3']]]);
      });
    });

    describe('option box', () => {
      it('should add gb-b2c class to option-box when selectBoxColor is B2C_COLOR', () => {
        // given
        propsData.options.selectBoxColor = GbSelectMultipleBoxColor.B2C_COLOR;

        // when
        const gbSelectMultipleWrapper: Wrapper<Vue> = mount(GbSelectMultiple, { mocks: { $t }, propsData });

        // then
        expect(gbSelectMultipleWrapper.find('.gb-select-multiple--option-box').attributes()).toHaveProperty(
          'class',
          'gb-select-multiple--option-box gb-b2c'
        );
      });

      it('should add gb-b2b class to option-box when selectBoxColor is B2B_COLOR', () => {
        // given
        propsData.options.selectBoxColor = GbSelectMultipleBoxColor.B2B_COLOR;

        // when
        const gbSelectMultipleWrapper: Wrapper<Vue> = mount(GbSelectMultiple, { mocks: { $t }, propsData });

        // then
        expect(gbSelectMultipleWrapper.find('.gb-select-multiple--option-box').attributes()).toHaveProperty(
          'class',
          'gb-select-multiple--option-box gb-b2b'
        );
      });

      it('should add gb-b2b class to option-box when no selectBoxColor', () => {
        // given
        propsData.options.selectBoxColor = undefined;

        // when
        const gbSelectMultipleWrapper: Wrapper<Vue> = mount(GbSelectMultiple, { mocks: { $t }, propsData });

        // then
        expect(gbSelectMultipleWrapper.find('.gb-select-multiple--option-box').attributes()).toHaveProperty(
          'class',
          'gb-select-multiple--option-box gb-b2b'
        );
      });
    });
  });
});

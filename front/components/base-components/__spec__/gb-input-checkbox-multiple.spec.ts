import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbErrorMessages from '~/components/base-components/gb-error-messages.vue';
import { DomainFieldErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbInputCheckboxMultiple from '../gb-input-checkbox-multiple.vue';

interface GbInputCheckboxMultipleValuePropsMock {
  optionLabelKey: string;
  optionValue: string;
}

describe('components/base-components/gb-input-checkbox-multiple', () => {
  let $t: jest.Mock;
  let propsData: {
    value: string[];
    options: {
      inputId: string;
      inputName: string;
      inputLabelKey: string;
      inputRequired?: boolean;
      inputOptions: GbInputCheckboxMultipleValuePropsMock[];
      inputErrors?: DomainFieldErrorsResponseInterface;
    };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: [],
      options: {
        inputId: 'field-id',
        inputName: 'field-name',
        inputOptions: [{} as GbInputCheckboxMultipleValuePropsMock],
        inputLabelKey: '',
      },
    };
  });

  describe('label', () => {
    it('should display legend', () => {
      // given
      propsData.options.inputLabelKey = 'message.key';
      $t.mockReturnValue('Label message value');

      // when
      const gbInputCheckboxMultipleWrapper: Wrapper<Vue> = shallowMount(GbInputCheckboxMultiple, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('message.key');
      expect(gbInputCheckboxMultipleWrapper.find('legend').text()).toBe('Label message value');
    });

    it('should display legend with * if required', () => {
      // given
      propsData.options.inputLabelKey = 'message.key';
      propsData.options.inputRequired = true;
      $t.mockReturnValue('Label message value');

      // when
      const gbInputCheckboxMultipleWrapper: Wrapper<Vue> = shallowMount(GbInputCheckboxMultiple, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('message.key');
      expect(gbInputCheckboxMultipleWrapper.find('legend').text()).toBe('Label message value*');
    });

    it('should not display legend when no label prop', () => {
      // given
      propsData.options.inputLabelKey = '';

      // when
      const gbInputCheckboxMultipleWrapper: Wrapper<Vue> = shallowMount(GbInputCheckboxMultiple, { mocks: { $t }, propsData });

      // then
      expect(gbInputCheckboxMultipleWrapper.findAll('legend')).toHaveLength(0);
    });
  });

  it('should display as many input tag as inputOptions', () => {
    // given
    propsData.options.inputOptions = [
      { optionLabelKey: 'labelKey1', optionValue: 'value1' },
      { optionLabelKey: 'labelKey2', optionValue: 'value2' },
    ];

    // when
    const gbInputCheckboxMultipleWrapper: Wrapper<Vue> = shallowMount(GbInputCheckboxMultiple, { mocks: { $t }, propsData });

    // then
    const inputWrappers: WrapperArray<Vue> = gbInputCheckboxMultipleWrapper.findAll('input');
    expect(inputWrappers).toHaveLength(2);

    expect(inputWrappers.at(0).attributes()).toHaveProperty('value', 'value1');
    expect(inputWrappers.at(1).attributes()).toHaveProperty('value', 'value2');
  });

  it('should display as many label tag as inputOptions', () => {
    // given
    propsData.options.inputOptions = [
      { optionLabelKey: 'labelKey1', optionValue: 'value1' },
      { optionLabelKey: 'labelKey2', optionValue: 'value2' },
    ];

    $t.mockImplementation((selectLabelKey: string) => {
      return selectLabelKey === 'labelKey1' ? 'Label message value 1' : 'Label message value 2';
    });

    // when
    const gbInputCheckboxMultipleWrapper: Wrapper<Vue> = shallowMount(GbInputCheckboxMultiple, { mocks: { $t }, propsData });

    // then
    const labelWrappers: WrapperArray<Vue> = gbInputCheckboxMultipleWrapper.findAll('label');
    expect(labelWrappers).toHaveLength(2);

    expect(labelWrappers.at(0).text()).toBe('Label message value 1');
    expect(labelWrappers.at(1).text()).toBe('Label message value 2');

    expect($t).toHaveBeenCalledWith('labelKey1');
    expect($t).toHaveBeenCalledWith('labelKey2');
  });

  it('should set id attribute from options prop and index', () => {
    // given
    propsData.options.inputOptions = [
      { optionLabelKey: 'labelKey1', optionValue: 'value1' },
      { optionLabelKey: 'labelKey2', optionValue: 'value2' },
    ];

    // when
    const gbInputCheckboxMultipleWrapper: Wrapper<Vue> = shallowMount(GbInputCheckboxMultiple, { mocks: { $t }, propsData });

    // then
    const inputWrappers: WrapperArray<Vue> = gbInputCheckboxMultipleWrapper.findAll('input');
    expect(inputWrappers).toHaveLength(2);

    expect(inputWrappers.at(0).attributes()).toHaveProperty('id', 'field-id_1');
    expect(inputWrappers.at(1).attributes()).toHaveProperty('id', 'field-id_2');
  });

  it('should set name attribute from options prop', () => {
    // given
    propsData.options.inputOptions = [
      { optionLabelKey: 'labelKey1', optionValue: 'value1' },
      { optionLabelKey: 'labelKey2', optionValue: 'value2' },
    ];

    // when
    const gbInputCheckboxMultipleWrapper: Wrapper<Vue> = shallowMount(GbInputCheckboxMultiple, { mocks: { $t }, propsData });

    // then
    const inputWrappers: WrapperArray<Vue> = gbInputCheckboxMultipleWrapper.findAll('input');
    expect(inputWrappers).toHaveLength(2);

    expect(inputWrappers.at(0).attributes()).toHaveProperty('name', 'field-name');
    expect(inputWrappers.at(1).attributes()).toHaveProperty('name', 'field-name');
  });

  it('should bind input value to parent model when input event is triggered on differents inputs', () => {
    // given
    propsData.options.inputOptions = [
      { optionLabelKey: 'aLabelKey', optionValue: 'a label' },
      { optionLabelKey: 'aSecondLabelKey', optionValue: 'a second label' },
    ];
    const gbInputCheckboxMultipleWrapper: Wrapper<Vue> = shallowMount(GbInputCheckboxMultiple, { mocks: { $t }, propsData });

    const inputField1: Wrapper<Vue> = gbInputCheckboxMultipleWrapper.find('#field-id_1');
    const inputField2: Wrapper<Vue> = gbInputCheckboxMultipleWrapper.find('#field-id_2');

    const firstInput: string = 'a label';
    const secondInput: string = 'a second label';

    // when
    inputField1.trigger('change');
    gbInputCheckboxMultipleWrapper.setProps({ value: [firstInput] });

    inputField2.trigger('change');
    gbInputCheckboxMultipleWrapper.setProps({ value: [firstInput, secondInput] });

    // then
    expect(gbInputCheckboxMultipleWrapper.emitted().input[0]).toStrictEqual([[firstInput]]);
    expect(gbInputCheckboxMultipleWrapper.emitted().input[1]).toStrictEqual([[firstInput, secondInput]]);
  });

  it('should bind input value to parent model when input event is triggered on same inputs', () => {
    // given
    propsData.options.inputOptions = [
      { optionLabelKey: 'aLabelKey', optionValue: 'a label' },
      { optionLabelKey: 'aSecondLabelKey', optionValue: 'a second label' },
      { optionLabelKey: 'aThirdLabelKey', optionValue: 'a third label' },
    ];
    const gbInputCheckboxMultipleWrapper: Wrapper<Vue> = shallowMount(GbInputCheckboxMultiple, { mocks: { $t }, propsData });

    const inputField1: Wrapper<Vue> = gbInputCheckboxMultipleWrapper.find('#field-id_1');
    const inputField2: Wrapper<Vue> = gbInputCheckboxMultipleWrapper.find('#field-id_2');
    const inputField3: Wrapper<Vue> = gbInputCheckboxMultipleWrapper.find('#field-id_3');

    const firstInput: string = 'a label';
    const secondInput: string = 'a second label';
    const thirdInput: string = 'a third label';

    // when
    inputField1.trigger('change');
    gbInputCheckboxMultipleWrapper.setProps({ value: [firstInput] });

    inputField3.trigger('change');
    gbInputCheckboxMultipleWrapper.setProps({ value: [firstInput, thirdInput] });

    inputField2.trigger('change');
    gbInputCheckboxMultipleWrapper.setProps({ value: [firstInput, thirdInput, secondInput] });

    inputField2.trigger('change');
    gbInputCheckboxMultipleWrapper.setProps({ value: [firstInput, thirdInput] });

    // then
    expect(gbInputCheckboxMultipleWrapper.emitted().input[0]).toStrictEqual([[firstInput]]);
    expect(gbInputCheckboxMultipleWrapper.emitted().input[1]).toStrictEqual([[firstInput, thirdInput]]);
    expect(gbInputCheckboxMultipleWrapper.emitted().input[2]).toStrictEqual([[firstInput, thirdInput, secondInput]]);
    expect(gbInputCheckboxMultipleWrapper.emitted().input[3]).toStrictEqual([[firstInput, thirdInput]]);
  });

  it('should display GbErrorMessages component with input errors as props', () => {
    // given
    const expected: DomainFieldErrorsResponseInterface = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;
    propsData.options.inputErrors = expected;

    // when
    const gbInputCheckboxMultipleWrapper: Wrapper<Vue> = shallowMount(GbInputCheckboxMultiple, { mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputCheckboxMultipleWrapper.find(GbErrorMessages).vm.options).toStrictEqual({
      domainFieldErrors: expected,
    });
  });

  it('should not display any GbErrorMessages component when no errors', () => {
    // given
    propsData.options.inputErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbInputCheckboxMultipleWrapper: Wrapper<Vue> = shallowMount(GbInputCheckboxMultiple, { mocks: { $t }, propsData });

    // then
    expect(gbInputCheckboxMultipleWrapper.findAll(GbErrorMessages)).toHaveLength(0);
  });
});

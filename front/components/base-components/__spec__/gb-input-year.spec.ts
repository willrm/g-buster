import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbErrorMessages from '~/components/base-components/gb-error-messages.vue';
import { DomainFieldErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbInputYear from '../gb-input-year.vue';

describe('components/base-components/gb-input-year', () => {
  let $t: jest.Mock;
  let propsData: {
    value: Date;
    options: {
      inputId: string;
      inputLabelKey: string;
      inputPlaceholderKey: string;
      inputRequired?: boolean;
      inputMin?: number;
      inputMax?: number;
      inputIsLarge?: boolean;
      inputErrors?: DomainFieldErrorsResponseInterface;
    };
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      // @ts-ignore
      value: undefined,
      options: {
        inputId: 'field-id',
        inputLabelKey: '',
        inputPlaceholderKey: '',
      },
    };
  });

  describe('label', () => {
    it('should display label for given id', () => {
      // given
      propsData.options.inputLabelKey = 'message.key';
      $t.mockReturnValue('Label message value');

      // when
      const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('message.key');
      expect(gbInputYearWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should display label with asterisk when input required and no placeholder', () => {
      // given
      propsData.options.inputRequired = true;
      propsData.options.inputLabelKey = 'message.key';
      // @ts-ignore
      propsData.options.inputPlaceholderKey = undefined;
      $t.mockReturnValue('Label message value');

      // when
      const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

      // then
      expect(gbInputYearWrapper.find('label[for="field-id"]').text()).toBe('Label message value*');
    });

    it('should display label without asterisk when input required and having a placeholder', () => {
      // given
      propsData.options.inputRequired = true;
      propsData.options.inputLabelKey = 'message.key';
      propsData.options.inputPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('Label message value');

      // when
      const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

      // then
      expect(gbInputYearWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should not display label when no label prop', () => {
      // given
      propsData.options.inputLabelKey = '';

      // when
      const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

      // then
      expect(gbInputYearWrapper.findAll('label[for="field-id"]')).toHaveLength(0);
    });
  });

  it('should display input with type number', () => {
    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect(gbInputYearWrapper.find('input').attributes()).toHaveProperty('type', 'number');
  });

  describe('placeholder', () => {
    it('should display input with placeholder', () => {
      // given
      propsData.options.inputPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('A placeholder');

      // when
      const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
      expect(gbInputYearWrapper.find('input').attributes()).toHaveProperty('placeholder', 'A placeholder');
    });

    it('should display input with placeholder with asterisk if required', () => {
      // given
      propsData.options.inputRequired = true;
      propsData.options.inputPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('A placeholder');

      // when
      const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
      expect(gbInputYearWrapper.find('input').attributes()).toHaveProperty('placeholder', 'A placeholder*');
    });
  });

  it('should display required input when required is true', () => {
    // given
    propsData.options.inputRequired = true;

    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect(gbInputYearWrapper.find('input[id="field-id"]').attributes()).toHaveProperty('required', 'required');
  });

  it('should display optional input when required is false', () => {
    // given
    propsData.options.inputRequired = false;

    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect(gbInputYearWrapper.find('input[id="field-id"]').attributes()).not.toHaveProperty('required');
  });

  it('should display input with min constraint when value specified', () => {
    // given
    propsData.options.inputMin = 42;

    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect(gbInputYearWrapper.find('input[id="field-id"]').attributes()).toHaveProperty('min', '42');
  });

  it('should display input without min constraint when no value specified', () => {
    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect(gbInputYearWrapper.find('input[id="field-id"]').attributes()).not.toHaveProperty('min');
  });

  it('should display input with max constraint when value specified', () => {
    // given
    propsData.options.inputMax = 1337;

    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect(gbInputYearWrapper.find('input[id="field-id"]').attributes()).toHaveProperty('max', '1337');
  });

  it('should display input without max constraint when no value specified', () => {
    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect(gbInputYearWrapper.find('input[id="field-id"]').attributes()).not.toHaveProperty('max');
  });

  it('should add gb-large class when large prop is true', () => {
    // given
    propsData.options.inputIsLarge = true;

    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect(gbInputYearWrapper.find('input').attributes()).toHaveProperty('class', 'gb-input-year gb-large');
  });

  it('should add gb-error class when having errors', () => {
    // given
    propsData.options.inputErrors = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;

    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect(gbInputYearWrapper.find('input').attributes()).toHaveProperty('class', 'gb-input-year gb-error');
  });

  it('should not add any additional class when large prop is false and having no error', () => {
    // given
    propsData.options.inputIsLarge = false;
    propsData.options.inputErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect(gbInputYearWrapper.find('input').attributes()).toHaveProperty('class', 'gb-input-year');
  });

  it('should bind value from props to input element value with empty string when prop value is undefined', () => {
    // given
    // @ts-ignore
    propsData.value = undefined;

    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect((gbInputYearWrapper.find('#field-id').element as HTMLInputElement).value).toBe('');
  });

  it('should bind value from props to input element value with year when prop value is a Date', () => {
    // given
    propsData.value = new Date(1988, 3);

    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect((gbInputYearWrapper.find('#field-id').element as HTMLInputElement).value).toBe('1988');
  });

  it('should bind value from props to input element value with year when prop value is a Date as string', () => {
    // given
    // @ts-ignore
    propsData.value = '1988-10-23T00:00:00.000Z';

    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect((gbInputYearWrapper.find('#field-id').element as HTMLInputElement).value).toBe('1988');
  });

  it('should emit input event with value as Date when input is value has been updated', () => {
    // given
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    const inputField: Wrapper<Vue> = gbInputYearWrapper.find('#field-id');
    const inputValue: string = '1988';

    // when
    (inputField.element as HTMLInputElement).value = inputValue;
    inputField.trigger('input');

    // then
    expect(gbInputYearWrapper.emitted().input).toStrictEqual([[new Date(1988, 0, 1)]]);
  });

  it('should not emit any input event when input value has not a 4 characters length', () => {
    // given
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData, data: () => ({ inputValue: undefined }) });

    const inputField: Wrapper<Vue> = gbInputYearWrapper.find('#field-id');
    const inputValue: string = '198';

    // when
    (inputField.element as HTMLInputElement).value = inputValue;
    inputField.trigger('input');

    // then
    expect(gbInputYearWrapper.emitted().input).toBeUndefined();
  });

  it('should display GbErrorMessages component with input errors as props', () => {
    // given
    const expected: DomainFieldErrorsResponseInterface = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;
    propsData.options.inputErrors = expected;

    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputYearWrapper.find(GbErrorMessages).vm.options).toStrictEqual({
      domainFieldErrors: expected,
    });
  });

  it('should not display any GbErrorMessages component when no errors', () => {
    // given
    propsData.options.inputErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbInputYearWrapper: Wrapper<Vue> = shallowMount(GbInputYear, { mocks: { $t }, propsData });

    // then
    expect(gbInputYearWrapper.findAll(GbErrorMessages)).toHaveLength(0);
  });
});

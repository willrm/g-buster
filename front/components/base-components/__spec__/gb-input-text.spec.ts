import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbErrorMessages from '~/components/base-components/gb-error-messages.vue';
import { DomainFieldErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbInputText from '../gb-input-text.vue';

describe('components/base-components/gb-input-text', () => {
  let $t: jest.Mock;
  let propsData: {
    value: string;
    options: {
      inputId: string;
      inputLabelKey: string;
      inputPlaceholderKey: string;
      inputRequired?: boolean;
      inputMaxLength?: number;
      inputIsLarge?: boolean;
      inputErrors?: DomainFieldErrorsResponseInterface;
    };
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: '',
      options: {
        inputId: 'field-id',
        inputLabelKey: '',
        inputPlaceholderKey: '',
      },
    };
  });

  describe('label', () => {
    it('should display label for given id', () => {
      // given
      propsData.options.inputLabelKey = 'message.key';
      $t.mockReturnValue('Label message value');

      // when
      const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('message.key');
      expect(gbInputTextWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should display label with asterisk when input required and no placeholder', () => {
      // given
      propsData.options.inputRequired = true;
      propsData.options.inputLabelKey = 'message.key';
      // @ts-ignore
      propsData.options.inputPlaceholderKey = undefined;
      $t.mockReturnValue('Label message value');

      // when
      const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

      // then
      expect(gbInputTextWrapper.find('label[for="field-id"]').text()).toBe('Label message value*');
    });

    it('should display label without asterisk when input required and having a placeholder', () => {
      // given
      propsData.options.inputRequired = true;
      propsData.options.inputLabelKey = 'message.key';
      propsData.options.inputPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('Label message value');

      // when
      const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

      // then
      expect(gbInputTextWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should not display label when no label prop', () => {
      // given
      propsData.options.inputLabelKey = '';

      // when
      const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

      // then
      expect(gbInputTextWrapper.findAll('label[for="field-id"]')).toHaveLength(0);
    });
  });

  it('should display input with type text', () => {
    // when
    const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

    // then
    expect(gbInputTextWrapper.find('input').attributes()).toHaveProperty('type', 'text');
  });

  describe('placeholder', () => {
    it('should display input with placeholder', () => {
      // given
      propsData.options.inputPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('A placeholder');

      // when
      const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
      expect(gbInputTextWrapper.find('input').attributes()).toHaveProperty('placeholder', 'A placeholder');
    });

    it('should display input with placeholder with asterisk if required', () => {
      // given
      propsData.options.inputRequired = true;
      propsData.options.inputPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('A placeholder');

      // when
      const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
      expect(gbInputTextWrapper.find('input').attributes()).toHaveProperty('placeholder', 'A placeholder*');
    });
  });

  it('should display required input when required is true', () => {
    // given
    propsData.options.inputRequired = true;

    // when
    const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

    // then
    expect(gbInputTextWrapper.find('input[id="field-id"]').attributes()).toHaveProperty('required', 'required');
  });

  it('should display optional input when required is false', () => {
    // given
    propsData.options.inputRequired = false;

    // when
    const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

    // then
    expect(gbInputTextWrapper.find('input[id="field-id"]').attributes()).not.toHaveProperty('required');
  });

  it('should display input with maxlength constraint when value specified', () => {
    // given
    propsData.options.inputMaxLength = 1337;

    // when
    const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

    // then
    expect(gbInputTextWrapper.find('input[id="field-id"]').attributes()).toHaveProperty('maxlength', '1337');
  });

  it('should display input without maxlength constraint when no value specified', () => {
    // when
    const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

    // then
    expect(gbInputTextWrapper.find('input[id="field-id"]').attributes()).not.toHaveProperty('maxlength');
  });

  it('should add gb-large class when large prop is true', () => {
    // given
    propsData.options.inputIsLarge = true;

    // when
    const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

    // then
    expect(gbInputTextWrapper.find('input').attributes()).toHaveProperty('class', 'gb-input-text gb-large');
  });

  it('should add gb-error class when having errors', () => {
    // given
    propsData.options.inputErrors = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;

    // when
    const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

    // then
    expect(gbInputTextWrapper.find('input').attributes()).toHaveProperty('class', 'gb-input-text gb-error');
  });

  it('should not add any additional class when large prop is false and having no error', () => {
    // given
    propsData.options.inputIsLarge = false;
    propsData.options.inputErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

    // then
    expect(gbInputTextWrapper.find('input').attributes()).toHaveProperty('class', 'gb-input-text');
  });

  it('should bind value from props to input element value', () => {
    // given
    propsData.value = 'existing data';

    // when
    const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

    // then
    expect((gbInputTextWrapper.find('#field-id').element as HTMLInputElement).value).toBe('existing data');
  });

  it('should emit input event with string value when input value has been updated', () => {
    // given
    const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

    const inputField: Wrapper<Vue> = gbInputTextWrapper.find('#field-id');
    const inputValue: string = 'some text';

    // when
    (inputField.element as HTMLInputElement).value = inputValue;
    inputField.trigger('input');

    // then
    expect(gbInputTextWrapper.emitted().input).toStrictEqual([[inputValue]]);
  });

  it('should display GbErrorMessages component with input errors as props', () => {
    // given
    const expected: DomainFieldErrorsResponseInterface = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;
    propsData.options.inputErrors = expected;

    // when
    const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputTextWrapper.find(GbErrorMessages).vm.options).toStrictEqual({
      domainFieldErrors: expected,
    });
  });

  it('should not display any GbErrorMessages component when no errors', () => {
    // given
    propsData.options.inputErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbInputTextWrapper: Wrapper<Vue> = shallowMount(GbInputText, { mocks: { $t }, propsData });

    // then
    expect(gbInputTextWrapper.findAll(GbErrorMessages)).toHaveLength(0);
  });
});

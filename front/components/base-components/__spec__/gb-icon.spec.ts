import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';

import GbIcon from '../gb-icon.vue';

describe('components/base-components/gb-icon', () => {
  let $t: jest.Mock;
  let propsData: {
    options: {
      iconName: string;
      iconAlternativeTextKey: string;
    };
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      options: {
        iconName: '',
        iconAlternativeTextKey: '',
      },
    };
  });

  it('should display img with src from static icons directory', () => {
    // given
    propsData.options.iconName = 'an-icon';

    // when
    const gbIcon: Wrapper<Vue> = shallowMount(GbIcon, { mocks: { $t }, propsData });

    // then
    expect(gbIcon.find('img').attributes()).toHaveProperty('src', '/icons/an-icon.svg');
  });

  it('should display img with alt translation', () => {
    // given
    propsData.options.iconAlternativeTextKey = 'icon.alt';
    $t.mockReturnValue('An alternative text');

    // when
    const gbIcon: Wrapper<Vue> = shallowMount(GbIcon, { mocks: { $t }, propsData });

    // then
    expect($t).toHaveBeenCalledWith('icon.alt');
    expect(gbIcon.find('img').attributes()).toHaveProperty('alt', 'An alternative text');
  });
});

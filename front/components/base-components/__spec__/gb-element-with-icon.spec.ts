import { mount, Slots, Wrapper } from '@vue/test-utils';
import Vue from 'vue';

import GbElementWithIcon from '../gb-element-with-icon.vue';

describe('components/base-components/gb-element-with-icon', () => {
  enum GbElementWithIconPosition {
    INSIDE = 'INSIDE',
    OUTSIDE = 'OUTSIDE',
  }

  let propsData: {
    options: {
      inputWithIconPosition: GbElementWithIconPosition;
      inputWithIconIsLarge: boolean;
      inputWithIconIsRounded: boolean;
    };
  };
  beforeEach(() => {
    propsData = {
      options: {
        inputWithIconPosition: GbElementWithIconPosition.INSIDE,
        inputWithIconIsLarge: false,
        inputWithIconIsRounded: false,
      },
    };
  });

  describe('should match snapshot', () => {
    it('with no configuration', () => {
      // when
      const gbElementWithIconWrapper: Wrapper<Vue> = mount(GbElementWithIcon, { propsData });

      // then
      expect(gbElementWithIconWrapper.element).toMatchSnapshot();
    });

    it('with slots', () => {
      // given
      const slots: Slots = {
        icon: '<img src="xyz" alt=""/>',
        input: '<input type="text" />',
      };

      // when
      const gbElementWithIconWrapper: Wrapper<Vue> = mount(GbElementWithIcon, { slots, propsData });

      // then
      expect(gbElementWithIconWrapper.element).toMatchSnapshot();
    });

    it('with position inside', () => {
      // given
      propsData.options.inputWithIconPosition = GbElementWithIconPosition.INSIDE;

      // when
      const gbElementWithIconWrapper: Wrapper<Vue> = mount(GbElementWithIcon, { propsData });

      // then
      expect(gbElementWithIconWrapper.element).toMatchSnapshot();
    });

    it('with position outside', () => {
      // given
      propsData.options.inputWithIconPosition = GbElementWithIconPosition.OUTSIDE;

      // when
      const gbElementWithIconWrapper: Wrapper<Vue> = mount(GbElementWithIcon, { propsData });

      // then
      expect(gbElementWithIconWrapper.element).toMatchSnapshot();
    });

    it('with large enabled', () => {
      // given
      propsData.options.inputWithIconIsLarge = true;

      // when
      const gbElementWithIconWrapper: Wrapper<Vue> = mount(GbElementWithIcon, { propsData });

      // then
      expect(gbElementWithIconWrapper.element).toMatchSnapshot();
    });

    it('with rounded icon', () => {
      // given
      propsData.options.inputWithIconIsRounded = true;

      // when
      const gbElementWithIconWrapper: Wrapper<Vue> = mount(GbElementWithIcon, { propsData });

      // then
      expect(gbElementWithIconWrapper.element).toMatchSnapshot();
    });

    it('without rounded icon', () => {
      // given
      propsData.options.inputWithIconIsRounded = false;

      // when
      const gbElementWithIconWrapper: Wrapper<Vue> = mount(GbElementWithIcon, { propsData });

      // then
      expect(gbElementWithIconWrapper.element).toMatchSnapshot();
    });
  });
});

import { createLocalVue, mount, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import VueUploadComponent from 'vue-upload-component';
import GbButton from '~/components/base-components/gb-button.vue';
import { DomainErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import { TemporaryFileResponseInterface } from '../../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';
import GbInputImageFile from '../gb-input-image-file.vue';

describe('components/base-components/gb-input-image-file', () => {
  let $t: jest.Mock;
  let propsData: {
    value: TemporaryFileResponseInterface;
    options: {
      inputFileAcceptedMimeTypes: string[];
      inputFileAcceptedExtensions: string[];
    };
  };
  let localVue: typeof Vue;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.component('file-upload', VueUploadComponent);
  });

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: {} as TemporaryFileResponseInterface,
      options: {
        inputFileAcceptedMimeTypes: [],
        inputFileAcceptedExtensions: [],
      },
    };
  });

  describe('upload button', () => {
    it('should display GbButton component with props for upload button', () => {
      // given
      const expected: object = { buttonLabelKey: 'logo.add' };

      // when
      const gbInputImageFileWrapper: Wrapper<Vue> = shallowMount(GbInputImageFile, { localVue, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbInputImageFileWrapper.find(GbButton).vm.options).toMatchObject(expected);
    });

    it('should emit upload event on click on upload button', async () => {
      // given
      const gbInputImageFileWrapper: Wrapper<Vue> = mount(GbInputImageFile, { localVue, mocks: { $t }, propsData });

      // when
      gbInputImageFileWrapper.find(GbButton).trigger('click');

      // then
      expect(gbInputImageFileWrapper.emitted()).toHaveProperty('upload');
    });
  });

  describe('VueUploadComponent', () => {
    it('should display VueUploadComponent component with api upload endpoint as post-action', () => {
      // when
      const gbInputImageFileWrapper: Wrapper<Vue> = mount(GbInputImageFile, { localVue, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbInputImageFileWrapper.find(VueUploadComponent).vm.postAction).toBe('/api/upload/file/temporary');
    });

    it('should display VueUploadComponent component that enable drop but not for directories', () => {
      // when
      const gbInputImageFileWrapper: Wrapper<Vue> = mount(GbInputImageFile, { localVue, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbInputImageFileWrapper.find(VueUploadComponent).vm.drop).toBe(true);
      // @ts-ignore
      expect(gbInputImageFileWrapper.find(VueUploadComponent).vm.dropDirectory).toBe(false);
    });

    it('should display VueUploadComponent component that disable multiple files', () => {
      // when
      const gbInputImageFileWrapper: Wrapper<Vue> = mount(GbInputImageFile, { localVue, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbInputImageFileWrapper.find(VueUploadComponent).vm.multiple).toBe(false);
    });

    it('should display VueUploadComponent component that accept only given extensions from props', () => {
      // given
      propsData.options.inputFileAcceptedExtensions = ['jpg', 'jpeg', 'png'];

      // when
      const gbInputImageFileWrapper: Wrapper<Vue> = mount(GbInputImageFile, { localVue, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbInputImageFileWrapper.find(VueUploadComponent).vm.extensions).toStrictEqual(['jpg', 'jpeg', 'png']);
    });

    it('should display VueUploadComponent component that accept only given mime types from props', () => {
      // given
      propsData.options.inputFileAcceptedMimeTypes = ['image/jpeg', 'image/png'];

      // when
      const gbInputImageFileWrapper: Wrapper<Vue> = mount(GbInputImageFile, { localVue, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbInputImageFileWrapper.find(VueUploadComponent).vm.accept).toBe('image/jpeg,image/png');
    });
    it('should emit input event on VueUploadComponent input', async () => {
      // given
      const gbInputImageFileWrapper: Wrapper<Vue> = mount(GbInputImageFile, { localVue, mocks: { $t }, propsData });

      // when
      gbInputImageFileWrapper.find(VueUploadComponent).vm.$emit('input', [{ response: {} }]);

      // then
      expect(gbInputImageFileWrapper.emitted()).toHaveProperty('input');
    });
  });

  describe('value', () => {
    it('should display value as contained background image', async () => {
      // given
      const gbInputImageFileWrapper: Wrapper<Vue> = mount(GbInputImageFile, { localVue, mocks: { $t }, propsData });
      const uploadedFile: TemporaryFileResponseInterface = {
        mimeType: 'image/whatever',
        base64: 'an-image-as-base64',
      } as TemporaryFileResponseInterface;

      // when
      gbInputImageFileWrapper.setProps({ value: uploadedFile });
      await gbInputImageFileWrapper.vm.$nextTick();

      // then
      const fileUploadElement: HTMLElement = gbInputImageFileWrapper.find('.gb-input-image-file--file-upload').element;
      expect(fileUploadElement.style.backgroundImage).toBe('url(data:image/whatever;base64,an-image-as-base64)');
      expect(fileUploadElement.style.backgroundSize).toBe('contain');
    });

    it('should not display any image when uploaded file is empty', () => {
      // given
      const gbInputImageFileWrapper: Wrapper<Vue> = mount(GbInputImageFile, { localVue, mocks: { $t }, propsData });
      const uploadedFile: TemporaryFileResponseInterface = {} as TemporaryFileResponseInterface;

      // when
      gbInputImageFileWrapper.setProps({ value: uploadedFile });

      // then
      const fileUploadElement: HTMLElement = gbInputImageFileWrapper.find('.gb-input-image-file--file-upload').element;
      expect(fileUploadElement.style.backgroundImage).toBe('');
      expect(fileUploadElement.style.backgroundSize).toBe('');
    });

    it('should emit input event with uploaded file when file is uploaded', () => {
      // given
      const gbInputImageFileWrapper: Wrapper<Vue> = mount(GbInputImageFile, { localVue, mocks: { $t }, propsData });
      const uploadedFile: TemporaryFileResponseInterface = {
        mimeType: 'image/whatever',
        base64: 'an-image-as-base64',
      } as TemporaryFileResponseInterface;

      const files: VUFile[] = [{ response: uploadedFile } as VUFile];

      // when
      gbInputImageFileWrapper.find(VueUploadComponent).vm.$emit('input', files);

      // then
      expect(gbInputImageFileWrapper.emitted().input[0]).toStrictEqual([uploadedFile]);
    });

    it('should emit error event when having domain errors in response', () => {
      // given
      const gbInputImageFileWrapper: Wrapper<Vue> = mount(GbInputImageFile, { localVue, mocks: { $t }, propsData });
      const domainErrors: DomainErrorsResponseInterface = {
        aFieldName: {
          i18nMessageKeys: ['An i18n message key'],
          messages: ['A message'],
        },
      };
      const files: VUFile[] = [{ response: { domainErrors } } as VUFile];

      // when
      gbInputImageFileWrapper.find(VueUploadComponent).vm.$emit('input', files);

      // then
      expect(gbInputImageFileWrapper.emitted().error).toStrictEqual([[domainErrors]]);
    });

    it('should not emit any event when having an incomplete response', () => {
      // given
      const gbInputImageFileWrapper: Wrapper<Vue> = mount(GbInputImageFile, { localVue, mocks: { $t }, propsData });
      // @ts-ignore
      const files: VUFile[] = [undefined];

      // when
      gbInputImageFileWrapper.find(VueUploadComponent).vm.$emit('input', files);

      // then
      expect(gbInputImageFileWrapper.emitted()).not.toHaveProperty('error');
      expect(gbInputImageFileWrapper.emitted()).not.toHaveProperty('input');
    });
  });
});

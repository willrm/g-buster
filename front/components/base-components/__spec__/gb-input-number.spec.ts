import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbErrorMessages from '~/components/base-components/gb-error-messages.vue';
import { DomainFieldErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbInputNumber from '../gb-input-number.vue';

describe('components/base-components/gb-input-number', () => {
  let $t: jest.Mock;
  let propsData: {
    value: number;
    options: {
      inputId: string;
      inputLabelKey: string;
      inputPlaceholderKey: string;
      inputMin?: number;
      inputMax?: number;
      inputRequired?: boolean;
      inputIsLarge?: boolean;
      inputErrors?: DomainFieldErrorsResponseInterface;
    };
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      // @ts-ignore
      value: undefined,
      options: { inputId: 'field-id', inputLabelKey: '', inputPlaceholderKey: '' },
    };
  });

  describe('label', () => {
    it('should display label for given id', () => {
      // given
      propsData.options.inputLabelKey = 'message.key';
      $t.mockReturnValue('Label message value');

      // when
      const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('message.key');
      expect(gbInputNumberWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should display label with asterisk when input required and no placeholder', () => {
      // given
      propsData.options.inputRequired = true;
      propsData.options.inputLabelKey = 'message.key';
      // @ts-ignore
      propsData.options.inputPlaceholderKey = undefined;
      $t.mockReturnValue('Label message value');

      // when
      const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

      // then
      expect(gbInputNumberWrapper.find('label[for="field-id"]').text()).toBe('Label message value*');
    });

    it('should display label without asterisk when input required and having a placeholder', () => {
      // given
      propsData.options.inputRequired = true;
      propsData.options.inputLabelKey = 'message.key';
      propsData.options.inputPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('Label message value');

      // when
      const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

      // then
      expect(gbInputNumberWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should not display label when no label prop', () => {
      // given
      propsData.options.inputLabelKey = '';

      // when
      const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

      // then
      expect(gbInputNumberWrapper.findAll('label[for="field-id"]')).toHaveLength(0);
    });
  });

  it('should display input with type number', () => {
    // when
    const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

    // then
    expect(gbInputNumberWrapper.find('input').attributes()).toHaveProperty('type', 'number');
  });

  describe('placeholder', () => {
    it('should display input with placeholder', () => {
      // given
      propsData.options.inputPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('A placeholder');

      // when
      const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
      expect(gbInputNumberWrapper.find('input').attributes()).toHaveProperty('placeholder', 'A placeholder');
    });

    it('should display input with placeholder with asterisk if required', () => {
      // given
      propsData.options.inputRequired = true;
      propsData.options.inputPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('A placeholder');

      // when
      const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
      expect(gbInputNumberWrapper.find('input').attributes()).toHaveProperty('placeholder', 'A placeholder*');
    });
  });

  describe('required', () => {
    it('should display required input when required is true', () => {
      // given
      propsData.options.inputRequired = true;

      // when
      const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

      // then
      expect(gbInputNumberWrapper.find('input[id="field-id"]').attributes()).toHaveProperty('required', 'required');
    });

    it('should display optional input when required is false', () => {
      // given
      propsData.options.inputRequired = false;

      // when
      const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

      // then
      expect(gbInputNumberWrapper.find('input[id="field-id"]').attributes()).not.toHaveProperty('required');
    });
  });

  describe('minimum contraint', () => {
    it('should display input with min constraint', () => {
      // given
      propsData.options.inputMin = 42;

      // when
      const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

      // then
      expect(gbInputNumberWrapper.find('input').attributes()).toHaveProperty('min', '42');
    });

    it('should display input with min constraint to 0 when no value specified', () => {
      // when
      const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

      // then
      expect(gbInputNumberWrapper.find('input').attributes()).toHaveProperty('min', '0');
    });
  });

  describe('maximum contraint', () => {
    it('should display input with max constraint', () => {
      // given
      propsData.options.inputMax = 100;

      // when
      const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

      // then
      expect(gbInputNumberWrapper.find('input').attributes()).toHaveProperty('max', '100');
    });

    it('should display input without max constraint when no value specified', () => {
      // when
      const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

      // then
      expect(gbInputNumberWrapper.find('input').attributes()).not.toHaveProperty('max');
    });
  });

  it('should add gb-large class when large prop is true', () => {
    // given
    propsData.options.inputIsLarge = true;

    // when
    const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

    // then
    expect(gbInputNumberWrapper.find('input').attributes()).toHaveProperty('class', 'gb-input-number gb-large');
  });

  it('should add gb-error class when having errors', () => {
    // given
    propsData.options.inputErrors = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;

    // when
    const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

    // then
    expect(gbInputNumberWrapper.find('input').attributes()).toHaveProperty('class', 'gb-input-number gb-error');
  });

  it('should not add any additional class when large prop is false and having no error', () => {
    // given
    propsData.options.inputIsLarge = false;
    propsData.options.inputErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

    // then
    expect(gbInputNumberWrapper.find('input').attributes()).toHaveProperty('class', 'gb-input-number');
  });

  it('should bind value from props to input element value', () => {
    // given
    propsData.value = 123456789;

    // when
    const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

    // then
    expect((gbInputNumberWrapper.find('#field-id').element as HTMLInputElement).value).toBe('123456789');
  });

  it('should bind empty value when value from props is undefined', () => {
    // given
    // @ts-ignore
    propsData.value = undefined;

    // when
    const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

    // then
    expect((gbInputNumberWrapper.find('#field-id').element as HTMLInputElement).value).toBe('');
  });

  it('should emit input event with number value when input value has been updated', () => {
    // given
    const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

    const inputField: Wrapper<Vue> = gbInputNumberWrapper.find('#field-id');
    const inputValue: number = 123456789;

    // when
    (inputField.element as HTMLInputElement).value = String(inputValue);
    inputField.trigger('input');

    // then
    expect(gbInputNumberWrapper.emitted().input).toStrictEqual([[inputValue]]);
  });

  it('should display GbErrorMessages component with input errors as props', () => {
    // given
    const expected: DomainFieldErrorsResponseInterface = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;
    propsData.options.inputErrors = expected;

    // when
    const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputNumberWrapper.find(GbErrorMessages).vm.options).toStrictEqual({
      domainFieldErrors: expected,
    });
  });

  it('should not display any GbErrorMessages component when no errors', () => {
    // given
    propsData.options.inputErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbInputNumberWrapper: Wrapper<Vue> = shallowMount(GbInputNumber, { mocks: { $t }, propsData });

    // then
    expect(gbInputNumberWrapper.findAll(GbErrorMessages)).toHaveLength(0);
  });
});

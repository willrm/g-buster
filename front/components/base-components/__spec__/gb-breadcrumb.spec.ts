import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBreadcrumb from '~/components/base-components/gb-breadcrumb.vue';

describe('components/base-components/gb-breadcrumb', () => {
  enum GbBreadcrumbBackgroundColor {
    COMPANY_BACKGROUND_COLOR = 'COMPANY_BACKGROUND_COLOR',
    CANDIDATE_BACKGROUND_COLOR = 'CANDIDATE_BACKGROUND_COLOR',
  }

  let $t: jest.Mock;
  let $route: { path: string };
  let propsData: {
    options: {
      backgroundColor: GbBreadcrumbBackgroundColor;
      isLeftBorderRounded: boolean;
      steps: Array<{
        i18nLabel: string;
        path: string;
      }>;
    };
  };

  beforeEach(() => {
    $t = jest.fn();
    $route = {
      path: '',
    };
    propsData = {
      options: {
        backgroundColor: GbBreadcrumbBackgroundColor.CANDIDATE_BACKGROUND_COLOR,
        isLeftBorderRounded: false,
        steps: [],
      },
    };
  });

  describe('options', () => {
    describe('steps', () => {
      it('should display as many div as steps', () => {
        // given
        propsData.options.steps = [
          { i18nLabel: 'label-step-1', path: 'step-1/path' },
          { i18nLabel: 'label-step-2', path: 'step-2/path' },
          { i18nLabel: 'label-step-3', path: 'step-3/path' },
        ];
        const gbBreadcrumbWrapper: Wrapper<Vue> = shallowMount(GbBreadcrumb, { mocks: { $t, $route }, propsData });

        // when
        const result: WrapperArray<Vue> = gbBreadcrumbWrapper.findAll('.gb-breadcrumb--step');

        // then
        expect(result.length).toEqual(3);
      });

      it('should not display div if there is no steps', () => {
        // given
        propsData.options.steps = [];
        const gbBreadcrumbWrapper: Wrapper<Vue> = shallowMount(GbBreadcrumb, { mocks: { $t, $route }, propsData });

        // when
        const result: WrapperArray<Vue> = gbBreadcrumbWrapper.findAll('.gb-breadcrumb--step');

        // then
        expect(result.length).toEqual(0);
      });

      it('should display translated label', () => {
        // given
        $t.mockReturnValueOnce('First step')
          .mockReturnValueOnce('Second step')
          .mockReturnValueOnce('Third step');
        propsData.options.steps = [
          { i18nLabel: 'label-step-1', path: 'step-1/path' },
          { i18nLabel: 'label-step-2', path: 'step-2/path' },
          { i18nLabel: 'label-step-3', path: 'step-3/path' },
        ];
        const gbBreadcrumbWrapper: Wrapper<Vue> = shallowMount(GbBreadcrumb, { mocks: { $t, $route }, propsData });

        // when
        const result: WrapperArray<Vue> = gbBreadcrumbWrapper.findAll('.gb-breadcrumb--step');

        // then
        expect($t).toHaveBeenNthCalledWith(1, 'label-step-1');
        expect($t).toHaveBeenNthCalledWith(2, 'label-step-2');
        expect($t).toHaveBeenNthCalledWith(3, 'label-step-3');

        expect(result.at(0).text()).toBe('First step');
        expect(result.at(1).text()).toBe('Second step');
        expect(result.at(2).text()).toBe('Third step');
      });
    });

    describe('backgroundcolor', () => {
      it('should set company background color', () => {
        // given
        propsData.options.backgroundColor = GbBreadcrumbBackgroundColor.COMPANY_BACKGROUND_COLOR;
        const gbBreadcrumbWrapper: Wrapper<Vue> = shallowMount(GbBreadcrumb, { mocks: { $t, $route }, propsData });

        // when
        const result: Wrapper<Vue> = gbBreadcrumbWrapper.find('.gb-breadcrumb');

        // then
        expect(result.element.classList.contains('gb-breadcrumb--company')).toBe(true);
      });

      it('should set candidate background color', () => {
        // given
        propsData.options.backgroundColor = GbBreadcrumbBackgroundColor.CANDIDATE_BACKGROUND_COLOR;
        const gbBreadcrumbWrapper: Wrapper<Vue> = shallowMount(GbBreadcrumb, { mocks: { $t, $route }, propsData });

        // when
        const result: Wrapper<Vue> = gbBreadcrumbWrapper.find('.gb-breadcrumb');

        // then
        expect(result.element.classList.contains('gb-breadcrumb--candidate')).toBe(true);
      });
    });
    describe('is left border rounded', () => {
      it('should set left border rounded class', () => {
        // given
        propsData.options.isLeftBorderRounded = true;
        const gbBreadcrumbWrapper: Wrapper<Vue> = shallowMount(GbBreadcrumb, { mocks: { $t, $route }, propsData });

        // when
        const result: Wrapper<Vue> = gbBreadcrumbWrapper.find('.gb-breadcrumb');

        // then
        expect(result.element.classList.contains('gb-breadcrumb--left-border-rounded')).toBe(true);
      });

      it('should not set left border rounded class', () => {
        // given
        propsData.options.isLeftBorderRounded = true;
        const gbBreadcrumbWrapper: Wrapper<Vue> = shallowMount(GbBreadcrumb, { mocks: { $t, $route }, propsData });

        // when
        const result: Wrapper<Vue> = gbBreadcrumbWrapper.find('.gb-breadcrumb');

        // then
        expect(result.element.classList.contains('gb-breadcrumb--left-border-rounded')).toBe(true);
      });
    });
  });

  describe('set active step', () => {
    it('should add current class to current step', async () => {
      // given
      propsData.options.steps = [
        { i18nLabel: 'label-step-1', path: 'step-1/path' },
        { i18nLabel: 'label-step-2', path: 'step-2/path' },
        { i18nLabel: 'label-step-3', path: 'step-3/path' },
      ];
      const gbBreadcrumbWrapper: Wrapper<Vue> = shallowMount(GbBreadcrumb, { mocks: { $t, $route }, propsData });
      const result: WrapperArray<Vue> = gbBreadcrumbWrapper.findAll('.gb-breadcrumb--step');

      // when
      $route.path = 'step-2/path';
      await gbBreadcrumbWrapper.vm.$nextTick();

      // then
      expect(result.at(1).element.classList.contains('gb-breadcrumb--step--current')).toBe(true);
    });

    it('should not add current class to other step', () => {
      // given
      propsData.options.steps = [
        { i18nLabel: 'label-step-1', path: 'step-1/path' },
        { i18nLabel: 'label-step-2', path: 'step-2/path' },
        { i18nLabel: 'label-step-3', path: 'step-3/path' },
      ];
      const gbBreadcrumbWrapper: Wrapper<Vue> = shallowMount(GbBreadcrumb, { mocks: { $t, $route }, propsData });
      const result: WrapperArray<Vue> = gbBreadcrumbWrapper.findAll('.gb-breadcrumb--step');

      // when
      $route.path = 'step-2/path';

      // then
      expect(result.at(0).element.classList.contains('gb-breadcrumb--step--current')).toBe(false);
      expect(result.at(2).element.classList.contains('gb-breadcrumb--step--current')).toBe(false);
    });
  });

  describe('set past steps', () => {
    it('should add past class to past steps', async () => {
      // given
      propsData.options.steps = [
        { i18nLabel: 'label-step-1', path: 'step-1/path' },
        { i18nLabel: 'label-step-2', path: 'step-2/path' },
        { i18nLabel: 'label-step-3', path: 'step-3/path' },
      ];
      const gbBreadcrumbWrapper: Wrapper<Vue> = shallowMount(GbBreadcrumb, { mocks: { $t, $route }, propsData });
      const result: WrapperArray<Vue> = gbBreadcrumbWrapper.findAll('.gb-breadcrumb--step');

      // when
      $route.path = 'step-2/path';
      await gbBreadcrumbWrapper.vm.$nextTick();

      // then
      expect(result.at(0).element.classList.contains('gb-breadcrumb--step--past')).toBe(true);
    });

    it('should not add past class to future steps', () => {
      // given
      propsData.options.steps = [
        { i18nLabel: 'label-step-1', path: 'step-1/path' },
        { i18nLabel: 'label-step-2', path: 'step-2/path' },
        { i18nLabel: 'label-step-3', path: 'step-3/path' },
      ];
      const gbBreadcrumbWrapper: Wrapper<Vue> = shallowMount(GbBreadcrumb, { mocks: { $t, $route }, propsData });
      const result: WrapperArray<Vue> = gbBreadcrumbWrapper.findAll('.gb-breadcrumb--step');

      // when
      $route.path = 'step-2/path';

      // then
      expect(result.at(1).element.classList.contains('gb-breadcrumb--step--past')).toBe(false);
      expect(result.at(2).element.classList.contains('gb-breadcrumb--step--past')).toBe(false);
    });
  });
});

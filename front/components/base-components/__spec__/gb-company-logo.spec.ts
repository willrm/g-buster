import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { LogoResponseInterface } from '../../../../back/src/infrastructure/rest/career/models/company-response.interface';

import GbCompanyLogo from '../gb-company-logo.vue';

describe('components/base-components/gb-company-logo', () => {
  let propsData: {
    options: {
      logo: LogoResponseInterface;
      alt: string;
      isLocked?: boolean;
      isLarge?: boolean;
    };
  };

  let $t: jest.Mock;

  beforeEach(() => {
    propsData = {
      options: {
        logo: {
          base64: 'base64Image',
          mimeType: 'mimeTypeImage',
        } as LogoResponseInterface,
        alt: 'alternative to image',
        isLocked: false,
        isLarge: false,
      },
    };

    $t = jest.fn();
  });

  describe('padlock', () => {
    it('should not display padlock icon when element is not locked', () => {
      // when
      const gbCompanyLogo: Wrapper<Vue> = shallowMount(GbCompanyLogo, { propsData, mocks: { $t } });

      // then
      // @ts-ignore
      expect(gbCompanyLogo.findAll(GbIcon).length).toBe(0);
    });

    it('should display padlock icon when element is locked', () => {
      // given
      propsData.options.isLocked = true;

      // when
      const gbCompanyLogo: Wrapper<Vue> = shallowMount(GbCompanyLogo, { propsData, mocks: { $t } });

      // then
      // @ts-ignore
      expect(gbCompanyLogo.findAll(GbIcon).at(0).vm.options).toStrictEqual({
        iconName: 'padlock_grey',
      });
    });

    it('should display large padlock icon when attribute isLarge is set', () => {
      // given
      propsData.options.isLocked = true;
      propsData.options.isLarge = true;

      // when
      const gbCompanyLogo: Wrapper<Vue> = shallowMount(GbCompanyLogo, { propsData, mocks: { $t } });

      // then
      expect(gbCompanyLogo.find('.oval').attributes('class')).toContain('is-large');
    });
  });

  describe('company logo', () => {
    it('should not display company logo when element is locked', () => {
      // given
      propsData.options.isLocked = true;

      // when
      const gbCompanyLogo: Wrapper<Vue> = shallowMount(GbCompanyLogo, { propsData, mocks: { $t } });

      // then
      expect(gbCompanyLogo.findAll('img.gb-company-logo--image').length).toBe(0);
    });

    it('should display company logo when element is not locked', () => {
      // when
      const gbCompanyLogo: Wrapper<Vue> = shallowMount(GbCompanyLogo, { propsData, mocks: { $t } });

      // then
      expect(gbCompanyLogo.findAll('img.gb-company-logo--image').length).toBe(1);
    });

    it('should display logo formatted src when element is not locked', () => {
      // given
      const expected: string = `data:${propsData.options.logo.mimeType};base64,${propsData.options.logo.base64}`;

      // when
      const gbCompanyLogo: Wrapper<Vue> = shallowMount(GbCompanyLogo, { propsData, mocks: { $t } });

      // then
      expect(gbCompanyLogo.find('img.gb-company-logo--image').attributes('src')).toStrictEqual(expected);
    });

    it('should display logo alt when element is not locked', () => {
      // given
      const expected: string = `${propsData.options.alt} logo`;

      // when
      const gbCompanyLogo: Wrapper<Vue> = shallowMount(GbCompanyLogo, { propsData, mocks: { $t } });

      // then
      expect(gbCompanyLogo.find('img.gb-company-logo--image').attributes('alt')).toStrictEqual(expected);
    });

    it('should display large company logo when attribute isLarge is set', () => {
      // given
      propsData.options.isLarge = true;

      // when
      const gbCompanyLogo: Wrapper<Vue> = shallowMount(GbCompanyLogo, { propsData, mocks: { $t } });

      // then
      expect(gbCompanyLogo.find('.oval').attributes('class')).toContain('is-large');
    });
  });
});

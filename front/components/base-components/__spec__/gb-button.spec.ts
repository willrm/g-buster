import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbButton from '../gb-button.vue';

describe('components/base-components/gb-button', () => {
  let $t: jest.Mock;
  let propsData: {
    options: {
      buttonType?: string;
      buttonLabelKey: string;
      buttonOnClick?: () => void;
    };
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      options: {
        buttonLabelKey: '',
      },
    };
  });

  it('should display button with given type', () => {
    // given
    propsData.options.buttonType = 'submit';

    // when
    const gbButton: Wrapper<Vue> = shallowMount(GbButton, { mocks: { $t }, propsData });

    // then
    expect(gbButton.find('button').attributes()).toHaveProperty('type', 'submit');
  });

  it('should display button with type button when no type given', () => {
    // given
    propsData.options.buttonType = undefined;

    // when
    const gbButton: Wrapper<Vue> = shallowMount(GbButton, { mocks: { $t }, propsData });

    // then
    expect(gbButton.find('button').attributes()).toHaveProperty('type', 'button');
  });

  it('should display button with label translation', () => {
    // given
    propsData.options.buttonLabelKey = 'button.label';
    $t.mockReturnValue('A button label');

    // when
    const gbButton: Wrapper<Vue> = shallowMount(GbButton, { mocks: { $t }, propsData });

    // then
    expect($t).toHaveBeenCalledWith('button.label');
    expect(gbButton.find('button').text()).toBe('A button label');
  });

  describe('onClick', () => {
    it('should not have any click listener when onClick callback is not defined', () => {
      // given
      propsData.options.buttonOnClick = undefined;
      const gbButton: Wrapper<Vue> = shallowMount(GbButton, { mocks: { $t }, propsData });

      // when
      gbButton.find('button');

      // then
      // @ts-ignore
      expect(gbButton.find('button').vm.onClick).toBeUndefined();
    });

    it('should execute onClick callback when click on button', () => {
      // given
      const onClickCallback: () => void = jest.fn();
      propsData.options.buttonOnClick = onClickCallback;
      const gbButton: Wrapper<Vue> = shallowMount(GbButton, { mocks: { $t }, propsData });

      // when
      gbButton.find('button').trigger('click');

      // then
      expect(onClickCallback).toHaveBeenCalled();
    });
  });
});

import { createLocalVue, shallowMount, Stubs, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import Datepicker from 'vuejs-datepicker';
import GbErrorMessages from '~/components/base-components/gb-error-messages.vue';
import { DomainFieldErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbInputDate from '../gb-input-date.vue';

describe('components/base-components/gb-input-date', () => {
  let $t: jest.Mock;
  let propsData: {
    value: Date;
    options: {
      inputId: string;
      inputLabelKey: string;
      inputPlaceholderKey: string;
      inputMin?: Date;
      inputMax?: Date;
      inputPattern?: string;
      inputRequired?: boolean;
      inputIsLarge?: boolean;
      inputErrors?: DomainFieldErrorsResponseInterface;
    };
  };
  let stubs: Stubs;

  let localVue: typeof Vue;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.component('date-picker', Datepicker);
  });

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      // @ts-ignore
      value: undefined,
      options: { inputId: 'field-id', inputLabelKey: '', inputPlaceholderKey: '' },
    };
    stubs = { 'client-only': true };
  });

  describe('label', () => {
    it('should display label for given id', () => {
      // given
      propsData.options.inputLabelKey = 'message.key';
      $t.mockReturnValue('Label message value');

      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('message.key');
      expect(gbInputDateWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should display label with asterisk when input required and no placeholder', () => {
      // given
      propsData.options.inputRequired = true;
      propsData.options.inputLabelKey = 'message.key';
      // @ts-ignore
      propsData.options.inputPlaceholderKey = undefined;
      $t.mockReturnValue('Label message value');

      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect(gbInputDateWrapper.find('label[for="field-id"]').text()).toBe('Label message value*');
    });

    it('should display label without asterisk when input required and having a placeholder', () => {
      // given
      propsData.options.inputRequired = true;
      propsData.options.inputLabelKey = 'message.key';
      propsData.options.inputPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('Label message value');

      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect(gbInputDateWrapper.find('label[for="field-id"]').text()).toBe('Label message value');
    });

    it('should not display label when no label prop', () => {
      // given
      propsData.options.inputLabelKey = '';

      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect(gbInputDateWrapper.findAll('label[for="field-id"]')).toHaveLength(0);
    });
  });

  describe('placeholder', () => {
    it('should display input with placeholder', () => {
      // given
      propsData.options.inputPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('A placeholder');

      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
      expect(gbInputDateWrapper.find(Datepicker).attributes()).toHaveProperty('placeholder', 'A placeholder');
    });

    it('should display input with placeholder with asterisk if required', () => {
      // given
      propsData.options.inputRequired = true;
      propsData.options.inputPlaceholderKey = 'i18n.placeholderKey';
      $t.mockReturnValue('A placeholder');

      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('i18n.placeholderKey');
      expect(gbInputDateWrapper.find(Datepicker).attributes()).toHaveProperty('placeholder', 'A placeholder*');
    });

    it('should not display placeholder when no placeholder prop', () => {
      // given
      propsData.options.inputPlaceholderKey = '';

      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect(gbInputDateWrapper.find(Datepicker).attributes()).toHaveProperty('placeholder', '');
    });
  });
  it('should display required input when required is true', () => {
    // given
    propsData.options.inputRequired = true;

    // when
    const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

    // then
    expect(gbInputDateWrapper.find(Datepicker).attributes()).toHaveProperty('required', 'true');
  });

  it('should display optional input when required is false', () => {
    // given
    propsData.options.inputRequired = false;

    // when
    const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

    // then
    expect(gbInputDateWrapper.find(Datepicker).attributes()).not.toHaveProperty('required');
  });

  describe('minimum constraint', () => {
    it('should display input with min constraint', () => {
      // given
      const fixedDate: Date = new Date(2019, 9, 23);
      propsData.options.inputMin = fixedDate;

      const expected: string = fixedDate.toISOString().split('T')[0];

      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect(gbInputDateWrapper.find(Datepicker).attributes()).toHaveProperty('min', expected);
    });

    it('should display input with empty min constraint when no value specified', () => {
      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect(gbInputDateWrapper.find(Datepicker).attributes()).toHaveProperty('min', '');
    });
  });

  describe('maximum constraint', () => {
    it('should display input with max constraint', () => {
      // given
      const fixedDate: Date = new Date(2019, 9, 23);
      propsData.options.inputMax = fixedDate;

      const expected: string = fixedDate.toISOString().split('T')[0];

      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect(gbInputDateWrapper.find(Datepicker).attributes()).toHaveProperty('max', expected);
    });

    it('should display input with empty max constraint when no value specified', () => {
      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect(gbInputDateWrapper.find(Datepicker).attributes()).toHaveProperty('max', '');
    });
  });

  describe('input format pattern', () => {
    it('should display input with format pattern', () => {
      // given
      propsData.options.inputPattern = 'YYYY-MM-DD';

      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect(gbInputDateWrapper.find(Datepicker).attributes()).toHaveProperty('format', 'YYYY-MM-DD');
    });

    it('should display input with empty max constraint when no value specified', () => {
      // when
      const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

      // then
      expect(gbInputDateWrapper.find(Datepicker).attributes()).not.toHaveProperty('pattern');
    });
  });

  it('should add gb-large class when large prop is true', () => {
    // given
    propsData.options.inputIsLarge = true;

    // when
    const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputDateWrapper.find(Datepicker).vm.inputClass).toStrictEqual({
      'gb-input-date--input': true,
      'gb-large': true,
      'gb-error': false,
    });
  });

  it('should add gb-error class when having errors', () => {
    // given
    propsData.options.inputErrors = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;

    // when
    const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputDateWrapper.find(Datepicker).vm.inputClass).toStrictEqual({
      'gb-input-date--input': true,
      'gb-large': undefined,
      'gb-error': true,
    });
  });

  it('should not add any additional class when large prop is false and having no error', () => {
    // given
    propsData.options.inputIsLarge = false;
    propsData.options.inputErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputDateWrapper.find(Datepicker).vm.inputClass).toStrictEqual({
      'gb-input-date--input': true,
      'gb-large': false,
      'gb-error': false,
    });
  });

  it('should configure Datepicker to use UTC date', () => {
    // when
    const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputDateWrapper.find(Datepicker).vm.useUtc).toBe(true);
  });

  it('should bind value from props to input element value with empty string when prop value is undefined', () => {
    // given
    // @ts-ignore
    propsData.value = undefined;

    // when
    const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputDateWrapper.find(Datepicker).vm.value).toBe('');
  });

  it('should bind value from props to input element value with date when prop value is a Date', () => {
    // given
    propsData.value = new Date('2019-10-23T00:00:00.000Z');

    // when
    const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputDateWrapper.find('#field-id').vm.value).toBe('2019-10-23');
  });

  it('should bind value from props to input element value with date when prop value is a Date as string', () => {
    // given
    // @ts-ignore
    propsData.value = '2019-10-23T00:00:00.000Z';

    // when
    const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputDateWrapper.find('#field-id').vm.value).toBe('2019-10-23');
  });

  it('should emit input event with value as Date without taking into account timezone difference when input value has been updated', () => {
    // given
    const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

    const inputField: Wrapper<Vue> = gbInputDateWrapper.find(Datepicker);
    const inputValue: Date = new Date(Date.UTC(2019, 10, 24, 0, 0, 0));

    // when
    inputField.vm.$emit('input', inputValue);

    // then
    expect(gbInputDateWrapper.emitted().input).toStrictEqual([[new Date('2019-11-24T05:00:00Z')]]);
  });

  it('should display GbErrorMessages component with input errors as props', () => {
    // given
    const expected: DomainFieldErrorsResponseInterface = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;
    propsData.options.inputErrors = expected;

    // when
    const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbInputDateWrapper.find(GbErrorMessages).vm.options).toStrictEqual({
      domainFieldErrors: expected,
    });
  });

  it('should not display any GbErrorMessages component when no errors', () => {
    // given
    propsData.options.inputErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbInputDateWrapper: Wrapper<Vue> = shallowMount(GbInputDate, { localVue, stubs, mocks: { $t }, propsData });

    // then
    expect(gbInputDateWrapper.findAll(GbErrorMessages)).toHaveLength(0);
  });
});

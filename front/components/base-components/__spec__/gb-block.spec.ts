import { mount, Slots, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbErrorMessages from '~/components/base-components/gb-error-messages.vue';
import { DomainFieldErrorsResponseInterface } from '../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';

import GbBlock from '../gb-block.vue';

describe('components/base-components/gb-block', () => {
  let $t: jest.Mock;
  let propsData: {
    options: {
      inputErrors?: DomainFieldErrorsResponseInterface;
    };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      options: {},
    };
  });

  describe('should match snapshot', () => {
    it('with no configuration', () => {
      // when
      const gbBlockWrapper: Wrapper<Vue> = mount(GbBlock);

      // then
      expect(gbBlockWrapper.element).toMatchSnapshot();
    });

    it('with slots', () => {
      // given
      const slots: Slots = {
        header: '<div>HEADER</div>',
        subheader: '<div>SUBHEADER</div>',
        content: '<div>CONTENT</div>',
      };

      // when
      const gbBlockWrapper: Wrapper<Vue> = mount(GbBlock, { slots });

      // then
      expect(gbBlockWrapper.element).toMatchSnapshot();
    });
  });
  it('should display GbErrorMessages component with input errors as props', () => {
    // given
    const expected: DomainFieldErrorsResponseInterface = {
      i18nMessageKeys: [],
      messages: [],
    } as DomainFieldErrorsResponseInterface;
    propsData.options.inputErrors = expected;

    // when
    const gbBlockWrapper: Wrapper<Vue> = mount(GbBlock, { mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbBlockWrapper.find(GbErrorMessages).vm.options).toStrictEqual({
      domainFieldErrors: expected,
    });
  });

  it('should not display any GbErrorMessages component when no errors', () => {
    // given
    propsData.options.inputErrors = {} as DomainFieldErrorsResponseInterface;

    // when
    const gbBlockWrapper: Wrapper<Vue> = mount(GbBlock, { mocks: { $t }, propsData });

    // then
    expect(gbBlockWrapper.findAll(GbErrorMessages)).toHaveLength(0);
  });
});

import { mount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbInputText from '~/components/base-components/gb-input-text.vue';
import { SocialNetworksResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/company-response.interface';
import { PostSocialNetworksRequestInterface } from '../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbCompanySocialNetworks from '../gb-company-create-social-networks.vue';

describe('components/company/create/gb-company-create-social-networks', () => {
  let $t: jest.Mock;
  let propsData: {
    value: PostSocialNetworksRequestInterface;
    options: {
      domainErrors?: DomainErrorsResponseInterface;
    };
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: {} as PostSocialNetworksRequestInterface,
      options: {
        domainErrors: {},
      },
    };
  });

  describe('facebook', () => {
    it('should display GbElementWithIcon component with props for company facebook', () => {
      // given
      const expected: object = { inputWithIconPosition: 'INSIDE', inputWithIconIsRounded: true };

      // when
      const companySocialNetworksWrapper: Wrapper<Vue> = mount(GbCompanySocialNetworks, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(companySocialNetworksWrapper.findAll(GbElementWithIcon).at(1).vm.options).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for company facebook', () => {
      // given
      const expected: object = { iconName: 'facebook_white' };

      // when
      const companySocialNetworksWrapper: Wrapper<Vue> = mount(GbCompanySocialNetworks, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = companySocialNetworksWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputText component with props for company facebook', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.aFacebookUrlError'],
        messages: ['A facebook url error'],
      };
      propsData.options.domainErrors = { facebook: expected };

      // when
      const companySocialNetworksWrapper: Wrapper<Vue> = mount(GbCompanySocialNetworks, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(companySocialNetworksWrapper.findAll(GbInputText).at(1).vm.options).toStrictEqual({
        inputId: 'facebook',
        inputPlaceholderKey: 'company.facebookPlaceholder',
        inputErrors: expected,
      });
    });

    it('should bind company facebook property to GbInputText v-model', () => {
      // given
      const facebook: string = 'company facebook';
      propsData.value.facebook = facebook;

      // when
      const companySocialNetworksWrapper: Wrapper<Vue> = mount(GbCompanySocialNetworks, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(companySocialNetworksWrapper.findAll(GbInputText).at(1).vm.value).toBe(facebook);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const companySocialNetworksWrapper: Wrapper<Vue> = mount(GbCompanySocialNetworks, { mocks: { $t }, propsData });

      const inputField: Wrapper<Vue> = companySocialNetworksWrapper.find('#facebook');
      const inputValue: string = 'a facebook url';

      // when
      (inputField.element as HTMLInputElement).value = inputValue;
      inputField.trigger('input');

      // then
      const expected: SocialNetworksResponseInterface = {
        facebook: inputValue,
      } as SocialNetworksResponseInterface;
      expect(companySocialNetworksWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('linkedIn', () => {
    it('should display GbElementWithIcon component with props for company linkedIn', () => {
      // given
      const expected: object = { inputWithIconPosition: 'INSIDE', inputWithIconIsRounded: true };

      // when
      const companySocialNetworksWrapper: Wrapper<Vue> = mount(GbCompanySocialNetworks, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(companySocialNetworksWrapper.findAll(GbElementWithIcon).at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for company linkedIn', () => {
      // given
      const expected: object = { iconName: 'linkedin_white' };

      // when
      const companySocialNetworksWrapper: Wrapper<Vue> = mount(GbCompanySocialNetworks, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = companySocialNetworksWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputText component with props for company linkedIn', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.aLinkedInUrlError'],
        messages: ['A linkedIn url error'],
      };
      propsData.options.domainErrors = { linkedIn: expected };

      // when
      const companySocialNetworksWrapper: Wrapper<Vue> = mount(GbCompanySocialNetworks, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(companySocialNetworksWrapper.findAll(GbInputText).at(0).vm.options).toStrictEqual({
        inputId: 'linkedIn',
        inputPlaceholderKey: 'company.linkedInPlaceholder',
        inputErrors: expected,
      });
    });

    it('should bind company linkedIn property to GbInputText v-model', () => {
      // given
      const linkedIn: string = 'company linkedIn';
      propsData.value.linkedIn = linkedIn;

      // when
      const companySocialNetworksWrapper: Wrapper<Vue> = mount(GbCompanySocialNetworks, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(companySocialNetworksWrapper.findAll(GbInputText).at(0).vm.value).toBe(linkedIn);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const companySocialNetworksWrapper: Wrapper<Vue> = mount(GbCompanySocialNetworks, { mocks: { $t }, propsData });

      const inputField: Wrapper<Vue> = companySocialNetworksWrapper.find('#linkedIn');
      const inputValue: string = 'a linkedIn url';

      // when
      (inputField.element as HTMLInputElement).value = inputValue;
      inputField.trigger('input');

      // then
      const expected: SocialNetworksResponseInterface = {
        linkedIn: inputValue,
      } as SocialNetworksResponseInterface;
      expect(companySocialNetworksWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });
});

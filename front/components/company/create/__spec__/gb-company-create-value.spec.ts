import { mount, Stubs, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBadge from '~/components/base-components/gb-badge.vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbTextarea from '~/components/base-components/gb-textarea.vue';
import GbTooltip from '~/components/base-components/gb-tooltip.vue';
import { PostValueRequestInterface } from '../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import GbCompanyCreateValue from '../gb-company-create-value.vue';

describe('components/company/create/gb-company-create-value', () => {
  let $t: jest.Mock;
  let propsData: {
    value: PostValueRequestInterface;
    options: {
      index: number;
    };
  };
  let stubs: Stubs;

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: {} as PostValueRequestInterface,
      options: {
        index: 0,
      },
    };

    stubs = { 'v-popover': true, 'client-only': true };
  });

  it('should display a GbElementWithIcon component with options', () => {
    // when
    const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

    // then
    const elementWithIcon: Wrapper<Vue> = gbCompanyCreateValueWrapper.find(GbElementWithIcon);

    // @ts-ignore
    expect(elementWithIcon.vm.options).toStrictEqual({ inputWithIconPosition: 'OUTSIDE', inputWithIconIsLarge: true });
  });

  it('should display a GbIcon component with icon name', () => {
    // when
    const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

    // then
    const icons: WrapperArray<Vue> = gbCompanyCreateValueWrapper.findAll(GbIcon);

    // @ts-ignore
    expect(icons.at(0).vm.options).toStrictEqual({ iconName: 'number_1_blue' });
  });

  describe('tooltip', () => {
    it('should contain a GbTooltip with options if index is 0', () => {
      // given
      const expected: object = { iconName: 'help_blue' };
      propsData.options.index = 0;

      // when
      const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateValueWrapper.find(GbTooltip).vm.options).toStrictEqual(expected);
    });

    it('should not contain a GbTooltip for index greater than 0', () => {
      // given
      propsData.options.index = 2;

      // when
      const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateValueWrapper.findAll(GbTooltip).length).toBe(0);
    });

    describe('example value', () => {
      it('should contain a title', () => {
        // given
        const expected: string = 'Example';
        $t.mockReturnValue(expected);
        propsData.options.index = 0;
        const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

        // when
        const gbTooltipWrapper: Wrapper<Vue> = gbCompanyCreateValueWrapper.find(GbTooltip);

        // then
        expect($t).toHaveBeenCalledWith('company.valueExampleTitle');
        expect(gbTooltipWrapper.find('span').text()).toStrictEqual(expected);
      });
      it('should contain a GbElementWithIcon with options for example', () => {
        // given
        propsData.options.index = 0;
        const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

        // when
        const gbTooltipWrapper: Wrapper<Vue> = gbCompanyCreateValueWrapper.find(GbTooltip);

        // then
        // @ts-ignore
        expect(gbTooltipWrapper.find(GbElementWithIcon).vm.options).toStrictEqual({ inputWithIconPosition: 'OUTSIDE', inputWithIconIsLarge: true });
      });
      it('should contain a GbIcon with options for example', () => {
        // given
        propsData.options.index = 0;
        const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

        // when
        const gbTooltipWrapper: Wrapper<Vue> = gbCompanyCreateValueWrapper.find(GbTooltip);

        // then
        // @ts-ignore
        expect(gbTooltipWrapper.findAll(GbIcon).at(1).vm.options).toStrictEqual({ iconName: 'number_1_blue' });
      });

      describe('GbBadge', () => {
        it('should contain a GbBadge with options for example', () => {
          // given
          propsData.options.index = 0;
          const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

          // when
          const gbTooltipWrapper: Wrapper<Vue> = gbCompanyCreateValueWrapper.find(GbTooltip);

          // then
          // @ts-ignore
          expect(gbTooltipWrapper.find(GbBadge).vm.options).toStrictEqual({ badgeIsRounded: true });
        });
        it('should contain a GbBadge with example meaning', () => {
          // given
          const expected: string = 'A meaning';
          $t.mockReturnValue(expected);
          propsData.options.index = 0;
          const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

          // when
          const gbTooltipWrapper: Wrapper<Vue> = gbCompanyCreateValueWrapper.find(GbTooltip);

          // then
          expect($t).toHaveBeenCalledWith('company.valueMeaningExample');
          expect(gbTooltipWrapper.find(GbBadge).text()).toStrictEqual(expected);
        });
      });

      it('should contain example for justification', () => {
        // given
        const expected: string = 'A justification';
        $t.mockReturnValue(expected);
        propsData.options.index = 0;
        const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

        // when
        const gbTooltipWrapper: Wrapper<Vue> = gbCompanyCreateValueWrapper.find(GbTooltip);
        const gbElementWithIconWrapper: Wrapper<Vue> = gbTooltipWrapper.find(GbElementWithIcon);

        // then
        expect($t).toHaveBeenCalledWith('company.valueJustificationExample');
        expect(gbElementWithIconWrapper.find('span').text()).toStrictEqual(expected);
      });
    });
  });

  describe('meaning', () => {
    it('should bind input value to parent model when input event is triggered', () => {
      // given
      propsData.options.index = 1;
      propsData.value = { justification: 'A justification' } as PostValueRequestInterface;
      const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

      const inputField: Wrapper<Vue> = gbCompanyCreateValueWrapper.find('#meaning-1');
      const inputValue: string = 'A meaning';

      // when
      (inputField.element as HTMLInputElement).value = inputValue;
      inputField.trigger('input');

      // then
      const expected: { index: number; value: PostValueRequestInterface } = {
        index: 1,
        value: { meaning: inputValue, justification: 'A justification' },
      };
      expect(gbCompanyCreateValueWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('justification', () => {
    it('should display a GbTextarea as value justifications', () => {
      // given
      propsData.options.index = 1;
      propsData.value = {} as PostValueRequestInterface;

      // when
      const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateValueWrapper.find(GbTextarea).vm.options).toStrictEqual({
        textareaId: 'justification-1',
        textareaPlaceholderKey: 'company.justificationPlaceholder',
        textareaRequired: true,
        textareaMaxLength: 200,
      });
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      propsData.options.index = 1;
      propsData.value = { meaning: 'a value meaning' } as PostValueRequestInterface;
      const gbCompanyCreateValueWrapper: Wrapper<Vue> = mount(GbCompanyCreateValue, { stubs, mocks: { $t }, propsData });

      const textareaField: Wrapper<Vue> = gbCompanyCreateValueWrapper.find('#justification-1');
      const textareaValue: string = 'a value justification';

      // when
      (textareaField.element as HTMLTextAreaElement).value = textareaValue;
      textareaField.trigger('input');

      // then
      const expected: { index: number; value: PostValueRequestInterface } = {
        index: 1,
        value: {
          meaning: 'a value meaning',
          justification: textareaValue,
        },
      };

      expect(gbCompanyCreateValueWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });
});

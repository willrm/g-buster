import { mount, shallowMount, Stubs, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbCard from '~/components/base-components/gb-card.vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbInputNumber from '~/components/base-components/gb-input-number.vue';
import GbSelectMultiple from '~/components/base-components/gb-select-multiple.vue';
import GbSelect from '~/components/base-components/gb-select.vue';
import GbCompanyCreateActivityLocations from '~/components/company/create/gb-company-create-activity-locations.vue';
import GbCompanyCreateValues from '~/components/company/create/gb-company-create-values.vue';
import { CompanyEnums } from '../../../../../back/src/infrastructure/rest/career/models/company-constraints-and-enums';
import {
  PostActivityLocationsRequestInterface,
  PostCompanyRequestInterface,
  PostValueRequestInterface,
} from '../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';

import GbCompanyCreateContent from '../gb-company-create-content.vue';

describe('components/company/create/gb-company-create-content', () => {
  let $t: jest.Mock;
  let propsData: {
    value: PostCompanyRequestInterface;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };
  let stubs: Stubs;

  beforeEach(() => {
    $t = jest.fn();
    propsData = {} as {
      value: PostCompanyRequestInterface;
      options: { domainErrors?: DomainErrorsResponseInterface };
    };
    propsData.value = {} as PostCompanyRequestInterface;
    propsData.value.activityLocations = {} as PostActivityLocationsRequestInterface;
    propsData.value.geniusTypes = [];
    propsData.value.socialNetworks = {};
    propsData.value.values = [{} as PostValueRequestInterface];

    propsData.options = {};

    stubs = { 'v-popover': true, 'client-only': true };
  });

  describe('revenueForRD', () => {
    it('should contain label', () => {
      // given
      $t.mockReturnValue('Revenue for R&B');

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('company.revenueForRD');
      expect(gbCompanyCreateContentWrapper.text()).toContain('Revenue for R&B');
    });

    it('should display GbElementWithIcon component with props for company revenue for R&D', () => {
      // given
      const expected: object = { inputWithIconIsLarge: true, inputWithIconPosition: 'OUTSIDE' };

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      expect(
        gbCompanyCreateContentWrapper
          .findAll('.column')
          .at(1)
          .findAll(GbElementWithIcon)
          // @ts-ignore
          .at(0).vm.options
      ).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for company revenue for R&D', () => {
      // given
      const expected: object = { iconName: 'money_blue' };

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelect component with props for company revenue for R&D', () => {
      // given
      const expected: object = {
        selectId: 'revenue-for-rd',
        selectPlaceholderKey: 'company.revenueForRDPlaceholder',
        selectOptions: [
          { optionLabelKey: 'company.revenueRdLessThan100K', optionValue: CompanyEnums.CompanyRevenueForRD.REVENUE_RD_LESS_THAN_100K },
          { optionLabelKey: 'company.revenueRdBetween100KAnd1M', optionValue: CompanyEnums.CompanyRevenueForRD.REVENUE_RD_BETWEEN_100k_AND_1M },
          { optionLabelKey: 'company.revenueRdBetween1MAnd5M', optionValue: CompanyEnums.CompanyRevenueForRD.REVENUE_RD_BETWEEN_1M_AND_5M },
          { optionLabelKey: 'company.revenueRdBetween5MAnd20M', optionValue: CompanyEnums.CompanyRevenueForRD.REVENUE_RD_BETWEEN_5M_AND_20M },
          { optionLabelKey: 'company.revenueRdMoreThan20M', optionValue: CompanyEnums.CompanyRevenueForRD.REVENUE_RD_MORE_THAN_20M },
        ],
        selectIsLarge: true,
        selectErrors: undefined,
      };

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === expected.selectId
      );

      // @ts-ignore
      expect(selects.at(0).vm.options).toStrictEqual(expected);
    });

    it('should set selectErrors when there is a domain errors for this field', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        messages: ['A revenue for R&D error'],
        i18nMessageKeys: ['company.aRevenueForRDError'],
      };
      propsData.options = { domainErrors: { revenueForRD: expected } };
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // then
      const selects: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === 'revenue-for-rd'
      );

      // @ts-ignore
      expect(selects.at(0).vm.options.selectErrors).toStrictEqual(expected);
    });

    it('should bind company revenueForRD property to GbSelect v-model', () => {
      // given
      const revenueForRD: CompanyEnums.CompanyRevenueForRD = CompanyEnums.CompanyRevenueForRD.REVENUE_RD_BETWEEN_100k_AND_1M;
      propsData.value.revenueForRD = revenueForRD;

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.value === revenueForRD
      );
      expect(selects).toHaveLength(1);
    });

    it('should set error flag for revenue for R&D card', () => {
      // given
      const domainErrors: DomainErrorsResponseInterface = {
        revenueForRD: {
          messages: ['A revenue for R&D error'],
          i18nMessageKeys: ['company.aRevenueForRDError'],
        },
      };
      propsData.options = { domainErrors };
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      const gbCardsWithError: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbCard).filter(
        (gbCard: Wrapper<Vue>) =>
          // @ts-ignore
          gbCard.vm.options.cardHasError
      );
      // @ts-ignore
      expect(gbCardsWithError.length).toBe(1);
    });

    it('should bind select value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      const selectField: Wrapper<Vue> = gbCompanyCreateContentWrapper.find('#revenue-for-rd');
      const selectValue: CompanyEnums.CompanyRevenueForRD = CompanyEnums.CompanyRevenueForRD.REVENUE_RD_BETWEEN_100k_AND_1M;

      // when
      (selectField.find(`option[value="${selectValue}"]`).element as HTMLOptionElement).selected = true;
      selectField.trigger('change');

      // then
      const inputExpected: { fieldName: string; fieldValue: CompanyEnums.CompanyRevenueForRD } = {
        fieldName: 'revenueForRD',
        fieldValue: selectValue,
      };

      expect(gbCompanyCreateContentWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });

  describe('geniusTypes', () => {
    it('should contain label', () => {
      // given
      $t.mockReturnValue('Genius types');

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('company.geniusTypes');
      expect(gbCompanyCreateContentWrapper.text()).toContain('Genius types');
    });

    it('should display GbSelectMultiple component with props for company geniusTypes', () => {
      // given
      const expected: object = {
        selectId: 'genius-types',
        selectPlaceholderKey: 'company.geniusTypesPlaceholder',
        selectRequired: true,
        selectErrors: undefined,
        selectOptions: [
          {
            optionLabelKey: 'company.geniusTypeMechanicalEngineering',
            optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_MECHANICAL_ENGINEERING,
          },
          { optionLabelKey: 'company.geniusTypeCivilEngineering', optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_CIVIL_ENGINEERING },
          { optionLabelKey: 'company.geniusTypeBuildingEngineering', optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_BUILDING_ENGINEERING },
          {
            optionLabelKey: 'company.geniusTypeConstructionEngineering',
            optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_CONSTRUCTION_ENGINEERING,
          },
          {
            optionLabelKey: 'company.geniusTypeElectricalEngineering',
            optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_ELECTRICAL_ENGINEERING,
          },
          { optionLabelKey: 'company.geniusTypeChemicalEngineering', optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_CHEMICAL_ENGINEERING },
          {
            optionLabelKey: 'company.geniusTypeIndustrialEngineering',
            optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_INDUSTRIAL_ENGINEERING,
          },
          {
            optionLabelKey: 'company.geniusTypeComputerAndSoftwareEngineering',
            optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_COMPUTER_AND_SOFTWARE_ENGINEERING,
          },
          {
            optionLabelKey: 'company.geniusTypeGeologicalEngineering',
            optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_GEOLOGICAL_ENGINEERING,
          },
          {
            optionLabelKey: 'company.geniusTypeAutomatedProductionEngineering',
            optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING,
          },
          { optionLabelKey: 'company.geniusTypeForestEngineering', optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_FOREST_ENGINEERING },
          {
            optionLabelKey: 'company.geniusTypeMaterialsEngineeringAndMetallurgy',
            optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_MATERIALS_ENGINEERING_AND_METALLURGY,
          },
          { optionLabelKey: 'company.geniusTypePhysicalEngineering', optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_PHYSICAL_ENGINEERING },
          { optionLabelKey: 'company.geniusTypeAerospaceEngineering', optionValue: CompanyEnums.CompanyGeniusType.GENIUS_TYPE_AEROSPACE_ENGINEERING },
        ],
      };

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      const selectMultiples: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbSelectMultiple).filter(
        (selectMultiple: Wrapper<Vue>) =>
          // @ts-ignore
          selectMultiple.vm.options.selectId === expected.selectId
      );
      // @ts-ignore
      expect(selectMultiples.at(0).vm.options).toStrictEqual(expected);
    });

    it('should bind company geniusTypes property to GbSelectMultiple v-model', () => {
      // given
      const geniusTypes: CompanyEnums.CompanyGeniusType[] = [
        CompanyEnums.CompanyGeniusType.GENIUS_TYPE_PHYSICAL_ENGINEERING,
        CompanyEnums.CompanyGeniusType.GENIUS_TYPE_GEOLOGICAL_ENGINEERING,
      ];
      propsData.value.geniusTypes = geniusTypes;

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      const selectMultiples: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbSelectMultiple).filter(
        (selectMultiple: Wrapper<Vue>) =>
          // @ts-ignore
          selectMultiple.vm.value === geniusTypes
      );
      expect(selectMultiples).toHaveLength(1);
    });

    it('should set selectErrors when there is a domain errors for this field', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        messages: ['A genius types error'],
        i18nMessageKeys: ['company.aGeniusTypesError'],
      };
      propsData.options = { domainErrors: { geniusTypes: expected } };
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // then
      const selectMultiples: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbSelectMultiple).filter(
        (selectMultiple: Wrapper<Vue>) =>
          // @ts-ignore
          selectMultiple.vm.options.selectId === 'genius-types'
      );

      // @ts-ignore
      expect(selectMultiples.at(0).vm.options.selectErrors).toStrictEqual(expected);
    });

    it('should set error flag for genius type card', () => {
      // given
      const domainErrors: DomainErrorsResponseInterface = {
        geniusTypes: {
          messages: ['A genius types error'],
          i18nMessageKeys: ['company.aGeniusTypesError'],
        },
      };
      propsData.options = { domainErrors };
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      const gbCardsWithError: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbCard).filter(
        (gbCard: Wrapper<Vue>) =>
          // @ts-ignore
          gbCard.vm.options.cardHasError
      );
      // @ts-ignore
      expect(gbCardsWithError.length).toBe(1);
    });

    it('should bind select value to parent model when input event is triggered', () => {
      // given
      const selectValue: CompanyEnums.CompanyGeniusType[] = [CompanyEnums.CompanyGeniusType.GENIUS_TYPE_AEROSPACE_ENGINEERING];
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });
      const geniusTypesGbSelectMultiple: Wrapper<Vue> = gbCompanyCreateContentWrapper
        .findAll(GbSelectMultiple)
        .filter(
          (selectMultiple: Wrapper<Vue>) =>
            // @ts-ignore
            selectMultiple.vm.options.selectId === 'genius-types'
        )
        .at(0);

      // when
      geniusTypesGbSelectMultiple.vm.$emit('input', selectValue);

      // then
      const inputExpected: { fieldName: string; fieldValue: CompanyEnums.CompanyGeniusType[] } = {
        fieldName: 'geniusTypes',
        fieldValue: selectValue,
      };
      expect(gbCompanyCreateContentWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });

  describe('values', () => {
    it('should display GbCompanyCreateValues component with props for company values', () => {
      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      expect(gbCompanyCreateContentWrapper.findAll(GbCompanyCreateValues)).toHaveLength(1);
    });

    it('should bind company values property to GbCompanyCreateValues v-model', () => {
      // given
      const companyValues: PostValueRequestInterface[] = [{} as PostValueRequestInterface, {} as PostValueRequestInterface];
      propsData.value.values = companyValues;

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateContentWrapper.findAll(GbCompanyCreateValues).at(0).vm.companyValues).toBe(companyValues);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });
      const gbCompanyCreateValuesWrapper: Wrapper<Vue> = gbCompanyCreateContentWrapper.find(GbCompanyCreateValues);

      const inputValue: PostValueRequestInterface[] = [{} as PostValueRequestInterface];
      // when
      gbCompanyCreateValuesWrapper.vm.$emit('input', inputValue);

      // then
      const inputExpected: { fieldName: string; fieldValue: PostValueRequestInterface[] } = {
        fieldName: 'values',
        fieldValue: inputValue,
      };

      expect(gbCompanyCreateContentWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });

  describe('bachelorsOfEngineeringNumber', () => {
    it('should contain label', () => {
      // given
      $t.mockReturnValue('Number of engineering bachelors');

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('company.bachelorsOfEngineeringNumber');
      expect(gbCompanyCreateContentWrapper.text()).toContain('Number of engineering bachelors');
    });

    it('should display GbInputNumber component with props for company bachelors of engineering number', () => {
      // given
      const expected: object = {
        inputId: 'bachelors-of-engineering-number',
        inputPlaceholderKey: 'company.bachelorsOfEngineeringNumberPlaceholder',
        inputRequired: true,
        inputIsLarge: true,
        inputErrors: undefined,
      };

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      const inputNumbers: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbInputNumber).filter(
        (inputNumber: Wrapper<Vue>) =>
          // @ts-ignore
          inputNumber.vm.options.inputId === expected.inputId
      );
      // @ts-ignore
      expect(inputNumbers.at(0).vm.options).toStrictEqual(expected);
    });

    it('should bind company bachelorsOfEngineeringNumber property to GbInputNumber v-model', () => {
      // given
      const bachelorsOfEngineeringNumber: number = 42;
      propsData.value.bachelorsOfEngineeringNumber = bachelorsOfEngineeringNumber;

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      const inputNumbers: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbInputNumber).filter(
        (inputNumber: Wrapper<Vue>) =>
          // @ts-ignore
          inputNumber.vm.value === bachelorsOfEngineeringNumber
      );
      expect(inputNumbers).toHaveLength(1);
    });
    it('should set selectErrors when there is a domain errors for this field', async () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        messages: ['A bachelors of engineering number error'],
        i18nMessageKeys: ['company.aBachelorsOfEngineeringNumberError'],
      };
      propsData.options = { domainErrors: { bachelorsOfEngineeringNumber: expected } };
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // then
      const inputNumbers: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbInputNumber).filter(
        (inputNumber: Wrapper<Vue>) =>
          // @ts-ignore
          inputNumber.vm.options.inputId === 'bachelors-of-engineering-number'
      );

      // @ts-ignore
      expect(inputNumbers.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });
    it('should set error flag for bachelors of engineering number card', async () => {
      // given
      const domainErrors: DomainErrorsResponseInterface = {
        geniusTypes: {
          messages: ['A bachelors of engineering number error'],
          i18nMessageKeys: ['company.aBachelorsOfEngineeringNumberError'],
        },
      };
      propsData.options = { domainErrors };
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      const gbCardsWithError: WrapperArray<Vue> = gbCompanyCreateContentWrapper.findAll(GbCard).filter(
        (gbCard: Wrapper<Vue>) =>
          // @ts-ignore
          gbCard.vm.options.cardHasError
      );
      // @ts-ignore
      expect(gbCardsWithError.length).toBe(1);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      const inputField: Wrapper<Vue> = gbCompanyCreateContentWrapper.find('#bachelors-of-engineering-number');
      const inputValue: string = '12';

      // when
      (inputField.element as HTMLInputElement).value = inputValue;
      inputField.trigger('input');

      // then
      const inputExpected: { fieldName: string; fieldValue: number } = {
        fieldName: 'bachelorsOfEngineeringNumber',
        fieldValue: parseInt(inputValue, 10),
      };

      expect(gbCompanyCreateContentWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });

  describe('activityLocations', () => {
    it('should display GbCompanyCreateValues component with props for company activity locations', () => {
      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      expect(gbCompanyCreateContentWrapper.findAll(GbCompanyCreateActivityLocations)).toHaveLength(1);
    });

    it('should bind company activityLocations property to GbCompanyCreateActivityLocations v-model', () => {
      // given
      const activityLocations: PostActivityLocationsRequestInterface = { quebec: 12, canada: 13 };
      propsData.value.activityLocations = activityLocations;

      // when
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateContentWrapper.findAll(GbCompanyCreateActivityLocations).at(0).vm.value).toStrictEqual(activityLocations);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateContentWrapper: Wrapper<Vue> = mount(GbCompanyCreateContent, { stubs, mocks: { $t }, propsData });
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = gbCompanyCreateContentWrapper.find(GbCompanyCreateActivityLocations);

      const inputValue: PostActivityLocationsRequestInterface = { quebec: 1 };
      // when
      gbCompanyCreateActivityLocationsWrapper.vm.$emit('input', inputValue);

      // then
      const inputExpected: { fieldName: string; fieldValue: PostActivityLocationsRequestInterface } = {
        fieldName: 'activityLocations',
        fieldValue: inputValue,
      };

      expect(gbCompanyCreateContentWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });
});

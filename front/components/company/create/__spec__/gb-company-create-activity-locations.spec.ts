import { mount, Stubs, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbCard from '~/components/base-components/gb-card.vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbInputNumber from '~/components/base-components/gb-input-number.vue';
import { ActivityLocationsResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/company-response.interface';
import { PostActivityLocationsRequestInterface } from '../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbCompanyCreateActivityLocations from '../gb-company-create-activity-locations.vue';

describe('components/company/create/gb-company-create-activity-locations', () => {
  let $t: jest.Mock;
  let propsData: {
    value: PostActivityLocationsRequestInterface;
    options: {
      domainErrors: DomainErrorsResponseInterface;
    };
  };
  let stubs: Stubs;

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: {
        quebec: 1,
      } as PostActivityLocationsRequestInterface,
      options: {
        domainErrors: {},
      },
    };
    stubs = { 'v-popover': true, 'client-only': true };
  });

  it('should display header for activity locations', () => {
    // given
    $t.mockReturnValue('Activity locations');

    // when
    const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

    // then
    expect($t).toHaveBeenCalledWith('company.activityLocations');
    expect(gbCompanyCreateActivityLocationsWrapper.find('h2').text()).toContain('Activity locations');
  });

  it('should display activity locations explanation', () => {
    // given
    $t.mockReturnValue('Activity locations explanation');

    // when
    const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

    // then
    expect($t).toHaveBeenCalledWith('company.activityLocationsExplanation');
    expect(gbCompanyCreateActivityLocationsWrapper.find('p').text()).toContain('Activity locations explanation');
  });

  it('should display card component with error flag', () => {
    // given
    propsData.options.domainErrors = {
      quebec: {
        i18nMessageKeys: ['company.aQuebecError'],
        messages: ['A quebec error'],
      },
    };

    // when
    const gbCompanyActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbCompanyActivityLocationsWrapper.findAll(GbCard).at(0).vm.options).toStrictEqual({ cardHasError: true });
  });

  describe('quebec', () => {
    it('should display GbElementWithIcon component with props for activity location quebec', () => {
      // given
      const expected: object = { inputWithIconIsLarge: true, inputWithIconPosition: 'OUTSIDE' };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateActivityLocationsWrapper.findAll(GbElementWithIcon).at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for activity location quebec', () => {
      // given
      const expected: object = { iconName: 'quebec_flag' };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyCreateActivityLocationsWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputNumber component with props for activity location quebec', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.aQuebecError'],
        messages: ['A quebec url error'],
      };
      propsData.options.domainErrors = { quebec: expected };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateActivityLocationsWrapper.findAll(GbInputNumber).at(0).vm.options).toStrictEqual({
        inputId: 'activity-location-quebec',
        inputLabelKey: 'company.activityLocationQuebec',
        inputPlaceholderKey: 'company.activityLocationQuebecPlaceholder',
        inputRequired: true,
        inputMin: 0,
        inputErrors: expected,
        inputIsLarge: true,
      });
    });

    it('should bind activity location quebec property to GbInputNumber input prop', () => {
      // given
      propsData.value = { quebec: 42 };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateActivityLocationsWrapper.findAll(GbInputNumber).at(0).vm.value).toBe(42);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      propsData.value.canada = 1;
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      const inputField: Wrapper<Vue> = gbCompanyCreateActivityLocationsWrapper.find('#activity-location-quebec');

      // when
      (inputField.element as HTMLInputElement).value = '42';
      inputField.trigger('input');

      // then
      const expected: ActivityLocationsResponseInterface = { canada: 1, quebec: 42 };
      expect(gbCompanyCreateActivityLocationsWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('canada', () => {
    it('should display GbElementWithIcon component with props for activity location canada', () => {
      // given
      const expected: object = { inputWithIconIsLarge: true, inputWithIconPosition: 'OUTSIDE' };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateActivityLocationsWrapper.findAll(GbElementWithIcon).at(1).vm.options).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for activity location canada', () => {
      // given
      const expected: object = { iconName: 'canada_flag' };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyCreateActivityLocationsWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputNumber component with props for activity location canada', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.aCanadaError'],
        messages: ['A canada url error'],
      };
      propsData.options.domainErrors = { canada: expected };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateActivityLocationsWrapper.findAll(GbInputNumber).at(1).vm.options).toStrictEqual({
        inputId: 'activity-location-canada',
        inputLabelKey: 'company.activityLocationCanada',
        inputPlaceholderKey: 'company.activityLocationCanadaPlaceholder',
        inputMin: 0,
        inputErrors: expected,
        inputIsLarge: true,
      });
    });

    it('should bind activity location canada property to GbInputNumber input prop', () => {
      // given
      propsData.value.canada = 43;

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateActivityLocationsWrapper.findAll(GbInputNumber).at(1).vm.value).toBe(43);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      propsData.value.canada = 43;
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      const inputField: Wrapper<Vue> = gbCompanyCreateActivityLocationsWrapper.find('#activity-location-canada');

      // when
      (inputField.element as HTMLInputElement).value = '45';
      inputField.trigger('input');

      // then
      const expected: ActivityLocationsResponseInterface = { quebec: 1, canada: 45 };
      expect(gbCompanyCreateActivityLocationsWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('usa', () => {
    it('should display GbElementWithIcon component with props for activity location usa', () => {
      // given
      const expected: object = { inputWithIconIsLarge: true, inputWithIconPosition: 'OUTSIDE' };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateActivityLocationsWrapper.findAll(GbElementWithIcon).at(2).vm.options).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for activity location usa', () => {
      // given
      const expected: object = { iconName: 'usa_flag' };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyCreateActivityLocationsWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputNumber component with props for activity location usa', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.aCanadaError'],
        messages: ['A usa url error'],
      };
      propsData.options.domainErrors = { usa: expected };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateActivityLocationsWrapper.findAll(GbInputNumber).at(2).vm.options).toStrictEqual({
        inputId: 'activity-location-usa',
        inputLabelKey: 'company.activityLocationUsa',
        inputPlaceholderKey: 'company.activityLocationUsaPlaceholder',
        inputMin: 0,
        inputErrors: expected,
        inputIsLarge: true,
      });
    });

    it('should bind activity location usa property to GbInputNumber input prop', () => {
      // given
      propsData.value.usa = 44;

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateActivityLocationsWrapper.findAll(GbInputNumber).at(2).vm.value).toBe(44);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      propsData.value = { quebec: 1, usa: 12 };
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      const inputField: Wrapper<Vue> = gbCompanyCreateActivityLocationsWrapper.find('#activity-location-usa');

      // when
      (inputField.element as HTMLInputElement).value = '44';
      inputField.trigger('input');

      // then
      const expected: ActivityLocationsResponseInterface = { quebec: 1, usa: 44 };
      expect(gbCompanyCreateActivityLocationsWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('international', () => {
    it('should display GbElementWithIcon component with props for activity location international', () => {
      // given
      const expected: object = { inputWithIconIsLarge: true, inputWithIconPosition: 'OUTSIDE' };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateActivityLocationsWrapper.findAll(GbElementWithIcon).at(3).vm.options).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for activity location international', () => {
      // given
      const expected: object = { iconName: 'international_flag' };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyCreateActivityLocationsWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputNumber component with props for activity location international', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.anInternationalError'],
        messages: ['An international url error'],
      };
      propsData.options.domainErrors = { international: expected };

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateActivityLocationsWrapper.findAll(GbInputNumber).at(3).vm.options).toStrictEqual({
        inputId: 'activity-location-international',
        inputLabelKey: 'company.activityLocationInternational',
        inputPlaceholderKey: 'company.activityLocationInternationalPlaceholder',
        inputMin: 0,
        inputErrors: expected,
        inputIsLarge: true,
      });
    });

    it('should bind activity location international property to GbInputNumber input prop', () => {
      // given
      propsData.value.international = 45;

      // when
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateActivityLocationsWrapper.findAll(GbInputNumber).at(3).vm.value).toBe(45);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      propsData.value = { quebec: 1 };
      const gbCompanyCreateActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyCreateActivityLocations, { stubs, mocks: { $t }, propsData });

      const inputField: Wrapper<Vue> = gbCompanyCreateActivityLocationsWrapper.find('#activity-location-international');

      // when
      (inputField.element as HTMLInputElement).value = '45';
      inputField.trigger('input');

      // then
      const expected: ActivityLocationsResponseInterface = { quebec: 1, international: 45 };
      expect(gbCompanyCreateActivityLocationsWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });
});

import { mount, Stubs, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbInputText from '~/components/base-components/gb-input-text.vue';
import GbInputYear from '~/components/base-components/gb-input-year.vue';
import GbSelect from '~/components/base-components/gb-select.vue';
import GbTextarea from '~/components/base-components/gb-textarea.vue';
import GbCompanyCreateAddLogo from '~/components/company/create/gb-company-create-add-logo.vue';
import GbCompanySocialNetworks from '~/components/company/create/gb-company-create-social-networks.vue';
import { CompanyEnums } from '../../../../../back/src/infrastructure/rest/career/models/company-constraints-and-enums';
import {
  PostCompanyRequestInterface,
  PostSocialNetworksRequestInterface,
} from '../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { CommonTypeAliases } from '../../../../../back/src/infrastructure/rest/common-models/common-type-aliases';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import { TemporaryFileResponseInterface } from '../../../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';
import GbCompanyCreateHeader from '../gb-company-create-header.vue';

describe('components/company/create/gb-company-create-header', () => {
  let $t: jest.Mock;
  let $modal: {
    show: jest.Mock;
    hide: jest.Mock;
  };
  let stubs: Stubs;
  let propsData: {
    company: PostCompanyRequestInterface;
    companyLogo: TemporaryFileResponseInterface;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };

  beforeEach(() => {
    $t = jest.fn();
    $modal = { show: jest.fn(), hide: jest.fn() };
    stubs = {
      modal: true,
      'file-upload': true,
      'client-only': true,
      'date-picker': true,
      'v-popover': true,
    };
    propsData = {
      company: {
        socialNetworks: {} as PostSocialNetworksRequestInterface,
      } as PostCompanyRequestInterface,
      companyLogo: {} as TemporaryFileResponseInterface,
      options: {
        domainErrors: {} as DomainErrorsResponseInterface,
      },
    };
  });

  describe('logo', () => {
    it('should display button for company logo', () => {
      // given
      $t.mockReturnValue('Upload a logo');

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      expect($t).toHaveBeenCalledWith('logo.add');
      expect(gbCompanyCreateHeaderWrapper.find('button').text()).toBe('Upload a logo');
    });

    it('should add gb-error class when having logo errors', () => {
      // given
      propsData.options.domainErrors = {
        logo: {
          i18nMessageKeys: [],
          messages: [],
        } as DomainFieldErrorsResponseInterface,
      } as DomainErrorsResponseInterface;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      expect(gbCompanyCreateHeaderWrapper.find('button').attributes()).toHaveProperty('class', 'gb-button-logo gb-error');
    });

    it('should not add any additional class when having no logo error', () => {
      // given
      propsData.options.domainErrors = {} as DomainErrorsResponseInterface;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      expect(gbCompanyCreateHeaderWrapper.find('button').attributes()).toHaveProperty('class', 'gb-button-logo');
    });

    it('should display GbCompanyCreateAddLogo component with props for uploaded logo', () => {
      // given
      const expected: TemporaryFileResponseInterface = { uuid: 'an-uuid' } as TemporaryFileResponseInterface;
      propsData.companyLogo = expected;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateHeaderWrapper.find(GbCompanyCreateAddLogo).vm.value).toStrictEqual(expected);
    });

    it('should show add-logo-modal when click on company logo button', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // when
      gbCompanyCreateHeaderWrapper.find('button').trigger('click');

      // then
      expect($modal.show).toHaveBeenCalledWith('add-logo-modal');
    });

    it('should hide add-logo-modal when close event is emitted', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // when
      gbCompanyCreateHeaderWrapper.find(GbCompanyCreateAddLogo).vm.$emit('close');

      // then
      expect($modal.hide).toHaveBeenCalledWith('add-logo-modal');
    });

    it('should display uploaded logo as contained background image', () => {
      // given
      propsData.companyLogo = {
        mimeType: 'image/whatever',
        base64: 'an-image-as-base64',
      } as TemporaryFileResponseInterface;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const buttonElement: HTMLElement = gbCompanyCreateHeaderWrapper.find('.gb-button-logo').element;
      expect(buttonElement.style.backgroundImage).toBe('url(data:image/whatever;base64,an-image-as-base64)');
      expect(buttonElement.style.backgroundSize).toBe('contain');
    });

    it('should add css class to button when loaded', () => {
      // given
      propsData.companyLogo = {
        base64: 'an-image-as-base64',
      } as TemporaryFileResponseInterface;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const buttonElement: HTMLElement = gbCompanyCreateHeaderWrapper.find('.gb-button-logo').element;
      expect(buttonElement.classList).toContain('gb-button-logo--loaded');
    });

    it('should not display any logo when uploaded logo is empty', () => {
      // given
      propsData.companyLogo = {} as TemporaryFileResponseInterface;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, {
        mocks: { $t, $modal },
        stubs,
        propsData,
      });

      // then
      const buttonElement: HTMLElement = gbCompanyCreateHeaderWrapper.find('.gb-button-logo').element;
      expect(buttonElement.style.backgroundImage).toBe('');
      expect(buttonElement.style.backgroundSize).toBe('');
    });

    it('should emit input event with uploaded logo uuid when input event is triggered by GbCompanyCreateAddLogo', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });
      const uuid: CommonTypeAliases.Uuid = 'an-uuid';
      const expectedUploadedLogo: TemporaryFileResponseInterface = { uuid } as TemporaryFileResponseInterface;

      // when
      gbCompanyCreateHeaderWrapper.find(GbCompanyCreateAddLogo).vm.$emit('input', expectedUploadedLogo);

      // then
      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[{ fieldName: 'logoUuid', fieldValue: uuid }]]);
    });

    it('should emit input-logo event with uploaded logo when input event is triggered by GbCompanyCreateAddLogo', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });
      const expectedUploadedLogo: TemporaryFileResponseInterface = {
        base64: 'aBase64Image',
        id: '1',
        mimeType: 'aMimeType',
        uuid: 'an-uuid',
      } as TemporaryFileResponseInterface;

      // when
      gbCompanyCreateHeaderWrapper.find(GbCompanyCreateAddLogo).vm.$emit('input', expectedUploadedLogo);

      // then
      expect(gbCompanyCreateHeaderWrapper.emitted()['input-logo']).toStrictEqual([[expectedUploadedLogo]]);
    });
  });

  describe('address', () => {
    it('should display GbElementWithIcon component with props for company address', () => {
      // given
      const expected: object = { inputWithIconPosition: 'OUTSIDE' };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      expect(
        gbCompanyCreateHeaderWrapper
          .find('.gb-company-create-header-right')
          .findAll(GbElementWithIcon)
          // @ts-ignore
          .at(0).vm.options
      ).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for company address', () => {
      // given
      const expected: object = { iconName: 'address_blue' };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputText component with props for company address', () => {
      // given
      const expected: object = {
        inputId: 'address',
        inputPlaceholderKey: 'company.addressPlaceholder',
        inputMaxLength: 255,
        inputRequired: true,
        inputErrors: undefined,
      };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.options.inputId === expected.inputId
      );
      // @ts-ignore
      expect(inputTexts.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputText component with input errors for company address', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.anAddressError'],
        messages: ['An address error'],
      };
      propsData.options.domainErrors = { address: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.options.inputId === 'address'
      );
      // @ts-ignore
      expect(inputTexts.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });

    it('should bind company address property to input prop', () => {
      // given
      const address: string = 'company address';
      propsData.company.address = address;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.value === address
      );
      expect(inputTexts).toHaveLength(1);
    });

    it('should emit input event when GbInputText input event is triggered', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      const inputField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#address');
      const inputValue: string = 'company address';

      // when
      (inputField.element as HTMLInputElement).value = inputValue;
      inputField.trigger('input');

      // then
      const expected: { fieldName: string; fieldValue: string } = {
        fieldName: 'address',
        fieldValue: inputValue,
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('businessSegment', () => {
    it('should display GbElementWithIcon component with props for company business segment', () => {
      // given
      const expected: object = { inputWithIconPosition: 'OUTSIDE' };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      expect(
        gbCompanyCreateHeaderWrapper
          .find('.gb-company-create-header-right')
          .findAll(GbElementWithIcon)
          // @ts-ignore
          .at(3).vm.options
      ).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for company business segment', () => {
      // given
      const expected: object = { iconName: 'square_blue' };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelect component with props for company business segment', () => {
      // given
      const expected: object = {
        selectId: 'business-segment',
        selectPlaceholderKey: 'company.businessSegmentPlaceholder',
        selectRequired: true,
        selectOptions: [
          {
            optionLabelKey: 'company.businessSegmentAeronauticsAerospace',
            optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_AERONAUTICS_AEROSPACE,
          },
          { optionLabelKey: 'company.businessSegmentAgriculture', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_AGRICULTURE },
          {
            optionLabelKey: 'company.businessSegmentOtherSpeciality',
            optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_OTHER_SPECIALITY,
          },
          { optionLabelKey: 'company.businessSegmentBiomedical', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_BIOMEDICAL },
          { optionLabelKey: 'company.businessSegmentChemistry', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_CHEMISTRY },
          { optionLabelKey: 'company.businessSegmentCivil', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_CIVIL },
          { optionLabelKey: 'company.businessSegmentDocumentation', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_DOCUMENTATION },
          { optionLabelKey: 'company.businessSegmentElectrical', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_ELECTRICAL },
          { optionLabelKey: 'company.businessSegmentEnvironment', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_ENVIRONMENT },
          { optionLabelKey: 'company.businessSegmentGeology', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_GEOLOGY },
          {
            optionLabelKey: 'company.businessSegmentQualityManagement',
            optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_QUALITY_MANAGEMENT,
          },
          {
            optionLabelKey: 'company.businessSegmentIndustrialManufacturer',
            optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_INDUSTRIAL_MANUFACTURER,
          },
          {
            optionLabelKey: 'company.businessSegmentInformationTechnology',
            optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_INFORMATION_TECHNOLOGY,
          },
          {
            optionLabelKey: 'company.businessSegmentInstrumentation',
            optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_INSTRUMENTATION,
          },
          {
            optionLabelKey: 'company.businessSegmentMaterialHandlingDistribution',
            optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_MATERIAL_HANDLING_DISTRIBUTION,
          },
          {
            optionLabelKey: 'company.businessSegmentMaritimeNaval',
            optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_MARITIME_NAVAL,
          },
          {
            optionLabelKey: 'company.businessSegmentNonMetallicMaterials',
            optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_NON_METALLIC_MATERIALS,
          },
          { optionLabelKey: 'company.businessSegmentMechanics', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_MECHANICS },
          {
            optionLabelKey: 'company.businessSegmentMetallurgyMetals',
            optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_METALLURGY_METALS,
          },
          { optionLabelKey: 'company.businessSegmentMining', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_MINING },
          { optionLabelKey: 'company.businessSegmentNuclearPower', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_NUCLEAR_POWER },
          { optionLabelKey: 'company.businessSegmentPulpAndPaper', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_PULP_AND_PAPER },
          { optionLabelKey: 'company.businessSegmentPhysical', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_PHYSICAL },
          { optionLabelKey: 'company.businessSegmentRobotics', optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_ROBOTICS },
          {
            optionLabelKey: 'company.businessSegmentOccupationalHealthAndSafety',
            optionValue: CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_OCCUPATIONAL_HEALTH_AND_SAFETY,
          },
        ],
        selectErrors: undefined,
      };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const selects: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === expected.selectId
      );
      // @ts-ignore
      expect(selects.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelect component with select errors for company business segment', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.aBusinessSegmentError'],
        messages: ['A business segment error'],
      };
      propsData.options.domainErrors = { businessSegment: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const selects: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === 'business-segment'
      );
      // @ts-ignore
      expect(selects.at(0).vm.options.selectErrors).toStrictEqual(expected);
    });

    it('should bind company businessSegment property to GbSelect input prop', () => {
      // given
      const businessSegment: CompanyEnums.CompanyBusinessSegment = CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_ENVIRONMENT;
      propsData.company.businessSegment = businessSegment;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const selects: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.value === businessSegment
      );
      expect(selects).toHaveLength(1);
    });
    it('should emit input event to parent when change event is triggered on select', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      const selectField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#business-segment');
      const selectValue: CompanyEnums.CompanyBusinessSegment = CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_ENVIRONMENT;

      // when
      (selectField.find(`option[value="${selectValue}"]`).element as HTMLOptionElement).selected = true;
      selectField.trigger('change');

      // then
      const expected: { fieldName: string; fieldValue: CompanyEnums.CompanyBusinessSegment } = {
        fieldName: 'businessSegment',
        fieldValue: selectValue,
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('creationDate', () => {
    it('should display GbElementWithIcon component with props for company creation date', () => {
      // given
      const expected: object = { inputWithIconPosition: 'OUTSIDE' };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      expect(
        gbCompanyCreateHeaderWrapper
          .find('.gb-company-create-header-right')
          .findAll(GbElementWithIcon)
          // @ts-ignore
          .at(1).vm.options
      ).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for company creation date', () => {
      // given
      const expected: object = { iconName: 'years_blue' };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputYear component with props for company creation date', () => {
      // given
      const currentYear: number = new Date().getFullYear();
      const expected: object = {
        inputId: 'creation-date',
        inputPlaceholderKey: 'company.creationDatePlaceholder',
        inputRequired: true,
        inputMin: 1800,
        inputMax: currentYear,
        inputErrors: undefined,
      };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const inputYears: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputYear).filter(
        (inputYear: Wrapper<Vue>) =>
          // @ts-ignore
          inputYear.vm.options.inputId === expected.inputId
      );
      // @ts-ignore
      expect(inputYears.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputYear component with input errors for company creation date', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.aCreationDateError'],
        messages: ['A creation date error'],
      };
      propsData.options.domainErrors = { creationDate: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const inputYears: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputYear).filter(
        (inputYear: Wrapper<Vue>) =>
          // @ts-ignore
          inputYear.vm.options.inputId === 'creation-date'
      );
      // @ts-ignore
      expect(inputYears.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });

    it('should bind company creationDate property to GbInputYear input prop', () => {
      // given
      const creationDate: Date = new Date(1988, 0, 1);
      propsData.company.creationDate = creationDate;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const inputYears: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputYear).filter(
        (inputYear: Wrapper<Vue>) =>
          // @ts-ignore
          inputYear.vm.value === creationDate
      );
      expect(inputYears).toHaveLength(1);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      const inputField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#creation-date');
      const inputValue: number = 2019;

      const expectedDate: Date = new Date(inputValue, 0, 1);

      // when
      // @ts-ignore
      (inputField.element as HTMLInputElement).value = inputValue;
      inputField.trigger('input');

      // then
      const expected: { fieldName: string; fieldValue: Date } = {
        fieldName: 'creationDate',
        fieldValue: expectedDate,
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('name', () => {
    it('should display GbInputText component with props for company name', () => {
      // given
      const expected: object = {
        inputId: 'name',
        inputPlaceholderKey: 'company.namePlaceholder',
        inputMaxLength: 40,
        inputRequired: true,
        inputErrors: undefined,
      };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.options.inputId === expected.inputId
      );

      // @ts-ignore
      expect(inputTexts.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputText component with input errors for company name', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.aNameError'],
        messages: ['A name error'],
      };
      propsData.options.domainErrors = { name: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.options.inputId === 'name'
      );

      // @ts-ignore
      expect(inputTexts.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });

    it('should bind company name property to GbInputText input prop', () => {
      // given
      const name: string = 'company name';
      propsData.company.name = name;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.value === name
      );
      expect(inputTexts).toHaveLength(1);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      const inputField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#name');
      const inputValue: string = 'company name';

      // when
      (inputField.element as HTMLInputElement).value = inputValue;
      inputField.trigger('input');

      // then
      const expected: { fieldName: string; fieldValue: string } = {
        fieldName: 'name',
        fieldValue: 'company name',
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('presentation', () => {
    it('should display GbTextarea component with props for company presentation', () => {
      // given
      const expected: object = {
        textareaId: 'presentation',
        textareaPlaceholderKey: 'company.presentationPlaceholder',
        textareaRequired: true,
        textareaMaxLength: 500,
        textareaIsLarge: true,
        textareaErrors: undefined,
      };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const textareas: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbTextarea).filter(
        (textarea: Wrapper<Vue>) =>
          // @ts-ignore
          textarea.vm.options.textareaId === expected.textareaId
      );
      // @ts-ignore
      expect(textareas.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbTextarea component with textarea errors for company presentation', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.aPresentationError'],
        messages: ['A presentation error'],
      };
      propsData.options.domainErrors = { presentation: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const textareas: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbTextarea).filter(
        (textarea: Wrapper<Vue>) =>
          // @ts-ignore
          textarea.vm.options.textareaId === 'presentation'
      );
      // @ts-ignore
      expect(textareas.at(0).vm.options.textareaErrors).toStrictEqual(expected);
    });

    it('should bind company presentation property to GbTextarea input prop', () => {
      // given
      const presentation: string = 'company presentation';
      propsData.company.presentation = presentation;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const textareas: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbTextarea).filter(
        (textarea: Wrapper<Vue>) =>
          // @ts-ignore
          textarea.vm.value === presentation
      );
      expect(textareas).toHaveLength(1);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      const textareaField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#presentation');
      const textareaValue: string = 'company presentation';

      // when
      (textareaField.element as HTMLTextAreaElement).value = textareaValue;
      textareaField.trigger('input');

      // then
      const expected: { fieldName: string; fieldValue: string } = {
        fieldName: 'presentation',
        fieldValue: 'company presentation',
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('revenue', () => {
    it('should display GbElementWithIcon component with props for company revenue', () => {
      // given
      const expected: object = { inputWithIconPosition: 'OUTSIDE' };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      expect(
        gbCompanyCreateHeaderWrapper
          .find('.gb-company-create-header-right')
          .findAll(GbElementWithIcon)
          // @ts-ignore
          .at(2).vm.options
      ).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for company revenue', () => {
      // given
      const expected: object = { iconName: 'money_blue' };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelect component with props for company revenue', () => {
      // given
      const expected: object = {
        selectId: 'revenue',
        selectPlaceholderKey: 'company.revenuePlaceholder',
        selectRequired: true,
        selectOptions: [
          { optionLabelKey: 'company.revenueLessThan100K', optionValue: CompanyEnums.CompanyRevenue.REVENUE_LESS_THAN_100K },
          { optionLabelKey: 'company.revenueBetween100KAnd1M', optionValue: CompanyEnums.CompanyRevenue.REVENUE_BETWEEN_100k_AND_1M },
          { optionLabelKey: 'company.revenueBetween1MAnd5M', optionValue: CompanyEnums.CompanyRevenue.REVENUE_BETWEEN_1M_AND_5M },
          { optionLabelKey: 'company.revenueBetween5MAnd20M', optionValue: CompanyEnums.CompanyRevenue.REVENUE_BETWEEN_5M_AND_20M },
          { optionLabelKey: 'company.revenueMoreThan20M', optionValue: CompanyEnums.CompanyRevenue.REVENUE_MORE_THAN_20M },
        ],
        selectErrors: undefined,
      };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const selects: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === expected.selectId
      );
      // @ts-ignore
      expect(selects.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelect component with props for company revenue', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.aRevenueError'],
        messages: ['A revenue error'],
      };
      propsData.options.domainErrors = { revenue: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const selects: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === 'revenue'
      );
      // @ts-ignore
      expect(selects.at(0).vm.options.selectErrors).toStrictEqual(expected);
    });

    it('should bind company revenue property to GbSelect input prop', () => {
      // given
      const revenue: CompanyEnums.CompanyRevenue = CompanyEnums.CompanyRevenue.REVENUE_BETWEEN_100k_AND_1M;
      propsData.company.revenue = revenue;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const selects: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.value === revenue
      );
      expect(selects).toHaveLength(1);
    });

    it('should bind select value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      const selectField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#revenue');
      const selectValue: CompanyEnums.CompanyRevenue = CompanyEnums.CompanyRevenue.REVENUE_BETWEEN_1M_AND_5M;

      // when
      (selectField.find(`option[value="${selectValue}"]`).element as HTMLOptionElement).selected = true;
      selectField.trigger('change');

      // then
      const expected: { fieldName: string; fieldValue: string } = {
        fieldName: 'revenue',
        fieldValue: CompanyEnums.CompanyRevenue.REVENUE_BETWEEN_1M_AND_5M,
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('size', () => {
    it('should display GbElementWithIcon component with props for company size', () => {
      // given
      const expected: object = { inputWithIconPosition: 'OUTSIDE' };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      expect(
        gbCompanyCreateHeaderWrapper
          .find('.gb-company-create-header-right')
          .findAll(GbElementWithIcon)
          // @ts-ignore
          .at(4).vm.options
      ).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for company size', () => {
      // given
      const expected: object = { iconName: 'people_blue' };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelect component with props for company size', () => {
      // given
      const expected: object = {
        selectId: 'size',
        selectPlaceholderKey: 'company.sizePlaceholder',
        selectRequired: true,
        selectOptions: [
          { optionLabelKey: 'company.zeroToNine', optionValue: CompanyEnums.CompanySize.ZERO_TO_NINE },
          { optionLabelKey: 'company.tenToNineteen', optionValue: CompanyEnums.CompanySize.TEN_TO_NINETEEN },
          { optionLabelKey: 'company.twentyToFortyNine', optionValue: CompanyEnums.CompanySize.TWENTY_TO_FORTY_NINE },
          { optionLabelKey: 'company.fiftyToSeventyNine', optionValue: CompanyEnums.CompanySize.FIFTY_TO_SEVENTY_NINE },
          { optionLabelKey: 'company.eightyToHundredNineteen', optionValue: CompanyEnums.CompanySize.EIGHTY_TO_HUNDRED_NINETEEN },
          {
            optionLabelKey: 'company.hundredTwentyToTwoHundredNinetyNine',
            optionValue: CompanyEnums.CompanySize.HUNDRED_TWENTY_TO_TWO_HUNDRED_NINETY_NINE,
          },
          { optionLabelKey: 'company.moreThanThreeHundred', optionValue: CompanyEnums.CompanySize.MORE_THAN_THREE_HUNDRED },
        ],
        selectErrors: undefined,
      };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      const selects: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === expected.selectId
      );
      // @ts-ignore
      expect(selects.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelect component with select errors for company size', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.aSizeError'],
        messages: ['A size error'],
      };
      propsData.options.domainErrors = { size: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      const selects: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === 'size'
      );
      // @ts-ignore
      expect(selects.at(0).vm.options.selectErrors).toStrictEqual(expected);
    });

    it('should bind company size property to GbSelect input prop', () => {
      // given
      const size: CompanyEnums.CompanySize = CompanyEnums.CompanySize.HUNDRED_TWENTY_TO_TWO_HUNDRED_NINETY_NINE;
      propsData.company.size = size;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const selects: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.value === size
      );
      expect(selects).toHaveLength(1);
    });

    it('should bind select value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      const selectField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#size');
      const selectValue: CompanyEnums.CompanySize = CompanyEnums.CompanySize.EIGHTY_TO_HUNDRED_NINETEEN;

      // when
      (selectField.find(`option[value="${selectValue}"]`).element as HTMLOptionElement).selected = true;
      selectField.trigger('change');

      // then
      const expected: { fieldName: string; fieldValue: string } = {
        fieldName: 'size',
        fieldValue: CompanyEnums.CompanySize.EIGHTY_TO_HUNDRED_NINETEEN,
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('website', () => {
    it('should display GbElementWithIcon component with props for company website', () => {
      // given
      const expected: object = { inputWithIconPosition: 'INSIDE', inputWithIconIsRounded: true };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      expect(
        gbCompanyCreateHeaderWrapper
          .findAll('.gb-company-create-header > .columns')
          .at(0)
          .findAll(GbElementWithIcon)
          // @ts-ignore
          .at(0).vm.options
      ).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for company website', () => {
      // given
      const expected: object = { iconName: 'website_white' };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputText component with props for company website', () => {
      // given
      const expected: object = {
        inputId: 'website',
        inputPlaceholderKey: 'company.websitePlaceholder',
        inputMaxLength: 50,
        inputRequired: true,
        inputErrors: undefined,
      };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.options.inputId === expected.inputId
      );

      // @ts-ignore
      expect(inputTexts.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputText component with input errors for company website', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['company.aWebsiteError'],
        messages: ['A website error'],
      };
      propsData.options.domainErrors = { website: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.options.inputId === 'website'
      );

      // @ts-ignore
      expect(inputTexts.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });

    it('should bind company website property to GbInputText input prop', () => {
      // given
      const website: string = 'company website';
      propsData.company.website = website;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.value === website
      );
      expect(inputTexts).toHaveLength(1);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      const inputField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#website');
      const inputValue: string = 'company website';

      // when
      (inputField.element as HTMLInputElement).value = inputValue;
      inputField.trigger('input');

      // then
      const expected: { fieldName: string; fieldValue: string } = {
        fieldName: 'website',
        fieldValue: 'company website',
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('socialNetworks', () => {
    it('should display GbCompanySocialNetworks component with props for company values', () => {
      // given
      propsData.options.domainErrors = undefined;

      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // when
      const gbCompanySocialNetworksWrapperArray: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbCompanySocialNetworks);

      // then
      expect(gbCompanySocialNetworksWrapperArray).toHaveLength(1);
      // @ts-ignore
      expect(gbCompanySocialNetworksWrapperArray.at(0).vm.options).toStrictEqual({ domainErrors: {} });
    });

    it('should bind company values property to GbCompanySocialNetworks input prop', async () => {
      // given
      const socialNetworks: PostSocialNetworksRequestInterface = {
        facebook: 'a-facebook-url',
        linkedIn: 'a-linkedin-url',
      } as PostSocialNetworksRequestInterface;
      propsData.company.socialNetworks = socialNetworks;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateHeaderWrapper.findAll(GbCompanySocialNetworks).at(0).vm.value).toStrictEqual(socialNetworks);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbCompanyCreateHeader, { mocks: { $t, $modal }, stubs, propsData });
      const gbCompanySocialNetworksWrapper: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find(GbCompanySocialNetworks);

      const inputValue: PostSocialNetworksRequestInterface = { linkedIn: 'http://a.linkedin.link' };
      // when
      gbCompanySocialNetworksWrapper.vm.$emit('input', inputValue);

      // then
      const expected: { fieldName: string; fieldValue: PostSocialNetworksRequestInterface } = {
        fieldName: 'socialNetworks',
        fieldValue: inputValue,
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });
});

import { mount, shallowMount, Stubs, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbErrorMessages from '~/components/base-components/gb-error-messages.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbInputImageFile from '~/components/base-components/gb-input-image-file.vue';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import { TemporaryFileResponseInterface } from '../../../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';
import GbCompanyCreateAddLogo from '../gb-company-create-add-logo.vue';

describe('components/company/create/gb-company-create-add-logo', () => {
  let $t: jest.Mock;
  let propsData: {
    value: TemporaryFileResponseInterface;
  };
  let stubs: Stubs;

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: {} as TemporaryFileResponseInterface,
    };
    stubs = {
      'file-upload': true,
      'client-only': true,
    };
  });

  describe('close button', () => {
    it('should display GbIcon component with props for close button', () => {
      // given
      const expected: object = { iconName: 'close_window_black', iconAlternativeTextKey: 'logo.close' };

      // when
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });

      // then
      // @ts-ignore
      expect(gbGbCompanyCreateAddLogoWrapper.find(GbIcon).vm.options).toStrictEqual(expected);
    });

    it('should emit close event when clicking on close button', () => {
      // given
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });

      // when
      gbGbCompanyCreateAddLogoWrapper.find('.gb-company-create-add-logo--close-button-container').trigger('click');

      // then
      expect(gbGbCompanyCreateAddLogoWrapper.emitted().close).toBeTruthy();
    });
  });

  describe('title', () => {
    it('should display title', () => {
      // given
      $t.mockImplementation((key: string) => {
        return key === 'logo.title' ? 'Logo title' : null;
      });

      // when
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });

      // then
      expect(gbGbCompanyCreateAddLogoWrapper.find('h2').text()).toBe('Logo title');
    });
  });

  describe('specifications', () => {
    it('should display specifications subtitle', () => {
      // given
      $t.mockImplementation((key: string) => {
        return key === 'logo.specifications' ? 'Logo specifications' : null;
      });

      // when
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });

      // then
      expect(
        gbGbCompanyCreateAddLogoWrapper
          .findAll('h3')
          .at(0)
          .text()
      ).toBe('Logo specifications');
    });

    it('should display each specification in list', () => {
      // given
      $t.mockImplementation((key: string) => {
        if (key === 'logo.specification1') return 'Specification 1';
        if (key === 'logo.specification2') return 'Specification 2';
      });

      // when
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });

      // then
      const specificationItems: WrapperArray<Vue> = gbGbCompanyCreateAddLogoWrapper.findAll('li');
      expect(specificationItems.at(0).text()).toBe('Specification 1');
      expect(specificationItems.at(1).text()).toBe('Specification 2');
    });
  });

  describe('recommendations', () => {
    it('should display recommendations subtitle', () => {
      // given
      $t.mockImplementation((key: string) => {
        return key === 'logo.recommendations' ? 'Logo recommendations' : null;
      });

      // when
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });

      // then
      expect(
        gbGbCompanyCreateAddLogoWrapper
          .findAll('h3')
          .at(1)
          .text()
      ).toBe('Logo recommendations');
    });

    it('should display each recommendation in list', () => {
      // given
      $t.mockImplementation((key: string) => {
        if (key === 'logo.recommendation1') return 'Specification 1';
      });

      // when
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });

      // then
      const recommendationItems: WrapperArray<Vue> = gbGbCompanyCreateAddLogoWrapper.findAll('li');
      expect(recommendationItems.at(2).text()).toBe('Specification 1');
    });
  });

  describe('file upload', () => {
    it('should display GbInputImageFile component with value for file upload', () => {
      // given
      const uploadedLogo: TemporaryFileResponseInterface = {
        mimeType: 'image/whatever',
        base64: 'an-image-as-base64',
      } as TemporaryFileResponseInterface;
      propsData.value = uploadedLogo;

      // when
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });

      // then
      // @ts-ignore
      expect(gbGbCompanyCreateAddLogoWrapper.find(GbInputImageFile).vm.value).toBe(uploadedLogo);
    });

    it('should display GbInputImageFile component with props for file upload constraints', () => {
      // when
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });

      // then
      // @ts-ignore
      expect(gbGbCompanyCreateAddLogoWrapper.find(GbInputImageFile).vm.options).toStrictEqual({
        inputFileAcceptedMimeTypes: ['image/jpeg', 'image/png'],
        inputFileAcceptedExtensions: ['jpg', 'jpeg', 'png'],
      });
    });

    it('should emit input event with uploaded logo when logo is uploaded', () => {
      // given
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = mount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });
      const expectedUploadedLogo: TemporaryFileResponseInterface = {
        mimeType: 'image/whatever',
        base64: 'an-image-as-base64',
      } as TemporaryFileResponseInterface;

      // when
      gbGbCompanyCreateAddLogoWrapper.find(GbInputImageFile).vm.$emit('input', expectedUploadedLogo);

      // then
      expect(gbGbCompanyCreateAddLogoWrapper.emitted().input[0]).toStrictEqual([expectedUploadedLogo]);
    });
  });

  describe('file upload errors', () => {
    it('should display GbErrorMessages component with file upload sizeInBytes error as props', async () => {
      // given
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });
      const sizeInBytesFieldErrors: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['An i18n message key'],
        messages: ['A message'],
      };
      const domainErrors: DomainErrorsResponseInterface = {
        sizeInBytes: sizeInBytesFieldErrors,
      };

      // when
      gbGbCompanyCreateAddLogoWrapper.setData({ domainErrors });
      await gbGbCompanyCreateAddLogoWrapper.vm.$nextTick();

      // then
      // @ts-ignore
      expect(gbGbCompanyCreateAddLogoWrapper.find(GbErrorMessages).vm.options).toStrictEqual({ domainFieldErrors: sizeInBytesFieldErrors });
    });

    it('should not display any GbErrorMessages component when no file upload sizeInBytes error', () => {
      // given
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });
      const domainErrors: DomainErrorsResponseInterface = {
        anotherFieldName: {
          i18nMessageKeys: ['An i18n message key'],
          messages: ['A message'],
        },
      };

      // when
      gbGbCompanyCreateAddLogoWrapper.setData({ domainErrors });

      // then
      expect(gbGbCompanyCreateAddLogoWrapper.findAll(GbErrorMessages)).toHaveLength(0);
    });

    it('should not display any GbErrorMessages component when no error', () => {
      // given
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });
      const domainErrors: DomainErrorsResponseInterface = {};

      // when
      gbGbCompanyCreateAddLogoWrapper.setData({ domainErrors });

      // then
      expect(gbGbCompanyCreateAddLogoWrapper.findAll('.gb-company-create-add-logo--gb-error-messages-container')).toHaveLength(0);
      expect(gbGbCompanyCreateAddLogoWrapper.findAll(GbErrorMessages)).toHaveLength(0);
    });

    it('should bind domainErrors from GbInputImageFile when an error event has been emitted', () => {
      // given
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });
      const domainErrors: DomainErrorsResponseInterface = {
        aFieldName: {
          i18nMessageKeys: ['An i18n message key'],
          messages: ['A message'],
        },
      };

      // when
      gbGbCompanyCreateAddLogoWrapper.find(GbInputImageFile).vm.$emit('error', domainErrors);

      // then
      expect(gbGbCompanyCreateAddLogoWrapper.vm.$data.domainErrors).toBe(domainErrors);
    });

    it('should reset domainErrors when logo has successfully been uploaded', () => {
      // given
      const gbGbCompanyCreateAddLogoWrapper: Wrapper<Vue> = shallowMount(GbCompanyCreateAddLogo, { mocks: { $t }, stubs, propsData });
      const uploadedLogo: TemporaryFileResponseInterface = {
        mimeType: 'image/whatever',
        base64: 'an-image-as-base64',
      } as TemporaryFileResponseInterface;

      // when
      gbGbCompanyCreateAddLogoWrapper.find(GbInputImageFile).vm.$emit('input', uploadedLogo);

      // then
      expect(gbGbCompanyCreateAddLogoWrapper.vm.$data.domainErrors).toStrictEqual({});
    });
  });
});

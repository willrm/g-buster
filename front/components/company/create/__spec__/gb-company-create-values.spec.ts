import { mount, Stubs, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbCard from '~/components/base-components/gb-card.vue';
import GbErrorMessages from '~/components/base-components/gb-error-messages.vue';
import { ValueResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/company-response.interface';
import { PostValueRequestInterface } from '../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { DomainErrorsResponseInterface } from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbCompanyCreateValue from '../gb-company-create-value.vue';
import GbCompanyCreateValues from '../gb-company-create-values.vue';

describe('components/company/create/gb-company-create-values', () => {
  let $t: jest.Mock;
  let propsData: {
    companyValues: PostValueRequestInterface[];
    options: {
      domainErrors?: DomainErrorsResponseInterface;
    };
  };

  let stubs: Stubs;

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      companyValues: [],
      options: {},
    };

    stubs = { 'client-only': true, 'date-picker': true, 'v-popover': true };
  });

  it('should display header', () => {
    // given
    $t.mockReturnValue('Values');

    // when
    const gbCompanyCreateValuesWrapper: Wrapper<Vue> = mount(GbCompanyCreateValues, { stubs, mocks: { $t }, propsData });

    // then
    expect($t).toHaveBeenCalledWith('company.values');
    expect(gbCompanyCreateValuesWrapper.text()).toContain('Values');
  });

  it('should display values explanation', () => {
    // given
    $t.mockReturnValue('Values explanation');

    // when
    const gbCompanyCreateValuesWrapper: Wrapper<Vue> = mount(GbCompanyCreateValues, { stubs, mocks: { $t }, propsData });

    // then
    expect($t).toHaveBeenCalledWith('company.valuesExplanation');
    expect(gbCompanyCreateValuesWrapper.find('p').text()).toBe('Values explanation');
  });

  it('should not display error message component when domain errors is empty', () => {
    // given
    propsData.options.domainErrors = undefined;

    // when
    const gbCompanyCreateValuesWrapper: Wrapper<Vue> = mount(GbCompanyCreateValues, { stubs, mocks: { $t }, propsData });

    // then
    expect(gbCompanyCreateValuesWrapper.findAll(GbErrorMessages).length).toBe(0);
  });

  it('should display card component with error flag', () => {
    // given
    propsData.options.domainErrors = {
      meaning: {
        i18nMessageKeys: ['company.aMeaningError'],
        messages: ['A meaning error'],
      },
    };

    // when
    const gbCompanyCreateValuesWrapper: Wrapper<Vue> = mount(GbCompanyCreateValues, { stubs, mocks: { $t }, propsData });

    // then
    // @ts-ignore
    expect(gbCompanyCreateValuesWrapper.findAll(GbCard).at(0).vm.options).toStrictEqual({ cardHasError: true });
  });

  describe('gb-company-create-value', () => {
    it('should display as many GbCompanyCreateValue as value meanings', () => {
      // given
      propsData.companyValues = [{} as PostValueRequestInterface, {} as PostValueRequestInterface];

      // when
      const gbCompanyCreateValuesWrapper: Wrapper<Vue> = mount(GbCompanyCreateValues, { stubs, mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCreateValuesWrapper.findAll(GbCompanyCreateValue).at(0).vm.options).toStrictEqual({
        index: 0,
      });
      // @ts-ignore
      expect(gbCompanyCreateValuesWrapper.findAll(GbCompanyCreateValue).at(1).vm.options).toStrictEqual({
        index: 1,
      });
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      propsData.companyValues = [{} as PostValueRequestInterface];
      const gbCompanyCreateValuesWrapper: Wrapper<Vue> = mount(GbCompanyCreateValues, { stubs, mocks: { $t }, propsData });
      const gbCompanyCreateValueWrapper: Wrapper<Vue> = gbCompanyCreateValuesWrapper.findAll(GbCompanyCreateValue).at(0);

      const inputValue: PostValueRequestInterface = { meaning: 'A meaning', justification: 'A justification' };
      // when
      gbCompanyCreateValueWrapper.vm.$emit('input', { index: 0, value: inputValue });

      // then
      const expected: PostValueRequestInterface[] = [inputValue];

      expect(gbCompanyCreateValuesWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('button add', () => {
    it('should display button to add a new value', () => {
      // given
      $t.mockReturnValue('Add a new value');

      // when
      const gbCompanyCreateValuesWrapper: Wrapper<Vue> = mount(GbCompanyCreateValues, { stubs, mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('company.addValue');
      expect(
        gbCompanyCreateValuesWrapper
          .findAll('button[type="button"]')
          .at(0)
          .text()
      ).toBe('Add a new value');
    });

    it('should hide button to add a new value when 4 values added', () => {
      // given
      propsData.companyValues = [
        {} as ValueResponseInterface,
        {} as ValueResponseInterface,
        {} as ValueResponseInterface,
        {} as ValueResponseInterface,
      ];

      // when
      const gbCompanyCreateValuesWrapper: Wrapper<Vue> = mount(GbCompanyCreateValues, { stubs, mocks: { $t }, propsData });

      // then
      expect(gbCompanyCreateValuesWrapper.findAll('button')).toHaveLength(0);
    });

    it('should emit an event with new value when click on button', () => {
      // given
      propsData.companyValues = [{ meaning: 'A meaning' } as ValueResponseInterface];
      const gbCompanyCreateValuesWrapper: Wrapper<Vue> = mount(GbCompanyCreateValues, { stubs, mocks: { $t }, propsData });

      // when
      gbCompanyCreateValuesWrapper
        .findAll('button')
        .at(0)
        .trigger('click');

      // then
      const expected: PostValueRequestInterface[] = [{ meaning: 'A meaning' } as PostValueRequestInterface, {} as PostValueRequestInterface];
      expect(gbCompanyCreateValuesWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });
});

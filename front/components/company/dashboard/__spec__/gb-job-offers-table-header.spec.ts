import { mount, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbJobOffersTableHeader from '../gb-job-offers-table-header.vue';

describe('components/company/dashboard/gb-job-offers-table-header', () => {
  let $t: jest.Mock;
  let propsData: {
    isCollapsed: boolean;
    options: {
      labelKey: string;
      jobOffersCount: number;
      isHighlighted?: boolean;
    };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      isCollapsed: true,
      options: {
        labelKey: '',
        jobOffersCount: 0,
      },
    };
  });

  describe('title', () => {
    it('should display title from label key in options', () => {
      // given
      propsData.options.labelKey = 'companyDashboard.jobOffersTitle';
      $t.mockReturnValue('A header');

      // when
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = mount(GbJobOffersTableHeader, {
        mocks: { $t },
        propsData,
      });

      // then
      expect($t).toHaveBeenCalledWith('companyDashboard.jobOffersTitle');
      expect(gbJobOffersTableHeaderWrapper.text()).toBe('A header (0)');
    });
  });

  describe('job offers count', () => {
    it('should display job offers count from  options', () => {
      // given
      propsData.options.jobOffersCount = 12;
      $t.mockReturnValue('');

      // when
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = mount(GbJobOffersTableHeader, {
        mocks: { $t },
        propsData,
      });

      // then
      expect(gbJobOffersTableHeaderWrapper.text()).toBe('(12)');
    });
  });

  describe('is highlighted', () => {
    it('should add highlighted class to parent', () => {
      // given
      propsData.options.isHighlighted = true;

      // when
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = mount(GbJobOffersTableHeader, {
        mocks: { $t },
        propsData,
      });

      // then
      expect(gbJobOffersTableHeaderWrapper.find('.gb-job-offers-table-header').element.classList.contains('highlighted')).toBe(true);
    });

    it('should not add highlighted class to parent', () => {
      // given
      propsData.options.isHighlighted = false;

      // when
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = mount(GbJobOffersTableHeader, {
        mocks: { $t },
        propsData,
      });

      // then
      expect(gbJobOffersTableHeaderWrapper.find('.gb-job-offers-table-header').element.classList.contains('highlighted')).toBe(false);
    });
  });

  describe('collapse button', () => {
    it('should have a button with a grey arrow in it if not highlighted', () => {
      // given
      propsData.options.isHighlighted = false;
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = shallowMount(GbJobOffersTableHeader, {
        mocks: { $t },
        propsData,
      });

      // when
      const collapseButton: Wrapper<Vue> = gbJobOffersTableHeaderWrapper.find('.gb-job-offers-table-header--collapse-button');

      // then
      // @ts-ignore
      expect(collapseButton.find(GbIcon).vm.options).toStrictEqual({ iconName: 'arrow_grey' });
    });
    it('should have a button with a white arrow in it if it is highlighted', () => {
      // given
      propsData.options.isHighlighted = true;
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = shallowMount(GbJobOffersTableHeader, {
        mocks: { $t },
        propsData,
      });

      // when
      const collapseButton: Wrapper<Vue> = gbJobOffersTableHeaderWrapper.find('.gb-job-offers-table-header--collapse-button');

      // then
      // @ts-ignore
      expect(collapseButton.find(GbIcon).vm.options).toStrictEqual({ iconName: 'arrow_white' });
    });

    it('should have a button with class collapsed if prop is true', () => {
      // given
      propsData.isCollapsed = true;
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = shallowMount(GbJobOffersTableHeader, {
        mocks: { $t },
        propsData,
      });

      // when
      const collapseButton: Wrapper<Vue> = gbJobOffersTableHeaderWrapper.find('.gb-job-offers-table-header--collapse-button');

      // then
      expect(collapseButton.element.classList.contains('is-collapsed')).toBe(true);
    });
  });
  describe('header', () => {
    it('should emit a collapse event on click on div', () => {
      // given
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = shallowMount(GbJobOffersTableHeader, {
        mocks: { $t },
        propsData,
      });
      const collapseButton: Wrapper<Vue> = gbJobOffersTableHeaderWrapper.find('.gb-job-offers-table-header');

      // when
      collapseButton.trigger('click');

      // then
      expect(gbJobOffersTableHeaderWrapper.emitted().collapse).toStrictEqual([[]]);
    });
  });
});

import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import { JobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import GbJobOffersArchived from '../gb-job-offers-archived.vue';
import GbJobOffersTableHeader from '../gb-job-offers-table-header.vue';
import GbJobOffersTableRow from '../gb-job-offers-table-row.vue';

describe('components/company/dashboard/gb-job-offers-archived', () => {
  let $t: jest.Mock;
  let propsData: {
    jobOffers: JobOfferResponseInterface[];
  };
  let data: () => {
    isCollapsed: boolean;
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffers: [],
    };
    data = () => ({
      isCollapsed: false,
    });
  });

  describe('header', () => {
    it('should contain a gb job offer table header component with options', () => {
      // given
      propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

      // when
      const gbJobOffersArchivedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersArchived, {
        mocks: { $t },
        propsData,
        data,
      });

      // then
      // @ts-ignore
      expect(gbJobOffersArchivedWrapper.find(GbJobOffersTableHeader).vm.options).toStrictEqual({
        labelKey: 'companyDashboardJobOffers.jobOffersArchivedTitle',
        jobOffersCount: 3,
        isHighlighted: false,
      });
    });

    describe('is collapsed', () => {
      it('should contain a gb job offer table header component with is collapsed as false from data', () => {
        // given
        propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

        // when
        const gbJobOffersArchivedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersArchived, {
          mocks: { $t },
          propsData,
        });
        gbJobOffersArchivedWrapper.setData({ isCollapsed: false });

        // then
        // @ts-ignore
        expect(gbJobOffersArchivedWrapper.find(GbJobOffersTableHeader).vm.isCollapsed).toBe(false);
      });

      it('should contain a gb job offer table header component with is collapsed as true from data', async () => {
        // given
        propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

        // when
        const gbJobOffersArchivedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersArchived, {
          mocks: { $t },
          propsData,
        });
        gbJobOffersArchivedWrapper.setData({ isCollapsed: true });
        await gbJobOffersArchivedWrapper.vm.$nextTick();

        // then
        // @ts-ignore
        expect(gbJobOffersArchivedWrapper.find(GbJobOffersTableHeader).vm.isCollapsed).toBe(true);
      });
    });

    it('should set is collapsed data to false on header click event', () => {
      // given
      propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];
      const gbJobOffersArchivedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersArchived, {
        mocks: { $t },
        propsData,
      });
      gbJobOffersArchivedWrapper.setData({ isCollapsed: true });

      // when
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = gbJobOffersArchivedWrapper.find(GbJobOffersTableHeader);
      gbJobOffersTableHeaderWrapper.vm.$emit('collapse');

      // then
      // @ts-ignore
      expect(gbJobOffersArchivedWrapper.vm.isCollapsed).toBe(false);
    });

    it('should set is collapsed data to true on header click event', () => {
      // given
      propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

      const gbJobOffersArchivedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersArchived, {
        mocks: { $t },
        propsData,
      });
      gbJobOffersArchivedWrapper.setData({ isCollapsed: false });

      // when
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = gbJobOffersArchivedWrapper.find(GbJobOffersTableHeader);
      gbJobOffersTableHeaderWrapper.vm.$emit('collapse');

      // then
      // @ts-ignore
      expect(gbJobOffersArchivedWrapper.vm.isCollapsed).toBe(true);
    });
  });

  describe('content', () => {
    it('should not contain a div with rows if it is collapsed', async () => {
      // given
      const gbJobOffersArchivedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersArchived, {
        mocks: { $t },
        propsData,
      });

      // when
      gbJobOffersArchivedWrapper.setData({ isCollapsed: true });
      await gbJobOffersArchivedWrapper.vm.$nextTick();

      // then
      const rows: Wrapper<Vue> = gbJobOffersArchivedWrapper.find('.gb-job-offers-archived--rows');
      expect(rows.exists()).toBe(false);
    });
    it('should contain a div with rows if it is collapsed', () => {
      // given
      const gbJobOffersArchivedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersArchived, {
        mocks: { $t },
        propsData,
      });

      // when
      gbJobOffersArchivedWrapper.setData({ isCollapsed: false });

      // then
      const rows: Wrapper<Vue> = gbJobOffersArchivedWrapper.find('.gb-job-offers-archived--rows');
      expect(rows.exists()).toBe(true);
    });
    it('should contain as many gb job offer table row component as job offers', () => {
      // given
      propsData.jobOffers = [
        {
          title: 'A first job offer',
        } as JobOfferResponseInterface,
        {
          title: 'A second job offer',
        } as JobOfferResponseInterface,
      ];

      // when
      const gbJobOffersArchivedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersArchived, {
        mocks: { $t },
        propsData,
        data,
      });

      // then
      // @ts-ignore
      expect(gbJobOffersArchivedWrapper.findAll(GbJobOffersTableRow).at(0).vm.jobOffer).toStrictEqual({
        title: 'A first job offer',
      });
      // @ts-ignore
      expect(gbJobOffersArchivedWrapper.findAll(GbJobOffersTableRow).at(1).vm.jobOffer).toStrictEqual({
        title: 'A second job offer',
      });
    });
  });
});

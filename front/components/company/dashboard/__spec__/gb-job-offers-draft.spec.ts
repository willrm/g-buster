import { mount, shallowMount, Stubs, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { JobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import GbJobOffersDraft from '../gb-job-offers-draft.vue';
import GbJobOffersTableHeader from '../gb-job-offers-table-header.vue';
import GbJobOffersTableRow from '../gb-job-offers-table-row.vue';

describe('components/company/dashboard/gb-job-offers-draft', () => {
  let $t: jest.Mock;
  let $route: {
    params: {};
  };
  let propsData: {
    jobOffers: JobOfferResponseInterface[];
  };
  let data: () => {
    isCollapsed: boolean;
  };
  let stubs: Stubs;

  beforeEach(() => {
    $t = jest.fn();
    $route = {
      params: {
        lang: 'fr-ca',
      },
    };
    propsData = {
      jobOffers: [],
    };
    data = () => ({
      isCollapsed: false,
    });
    stubs = { 'nuxt-link': true };
  });

  describe('header', () => {
    it('should contain a gb job offer table header component with options', () => {
      // given
      propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

      // when
      const gbJobOffersDraftWrapper: Wrapper<Vue> = shallowMount(GbJobOffersDraft, {
        mocks: { $t },
        propsData,
        data,
      });

      // then
      // @ts-ignore
      expect(gbJobOffersDraftWrapper.find(GbJobOffersTableHeader).vm.options).toStrictEqual({
        labelKey: 'companyDashboardJobOffers.jobOffersDraftTitle',
        jobOffersCount: 3,
        isHighlighted: false,
      });
    });

    describe('is collapsed', () => {
      it('should contain a gb job offer table header component with is collapsed as false from data', () => {
        // given
        propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

        // when
        const gbJobOffersDraftWrapper: Wrapper<Vue> = shallowMount(GbJobOffersDraft, {
          mocks: { $t },
          propsData,
        });
        gbJobOffersDraftWrapper.setData({ isCollapsed: false });

        // then
        // @ts-ignore
        expect(gbJobOffersDraftWrapper.find(GbJobOffersTableHeader).vm.isCollapsed).toBe(false);
      });

      it('should contain a gb job offer table header component with is collapsed as true from data', async () => {
        // given
        propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

        // when
        const gbJobOffersDraftWrapper: Wrapper<Vue> = shallowMount(GbJobOffersDraft, {
          mocks: { $t },
          propsData,
        });
        gbJobOffersDraftWrapper.setData({ isCollapsed: true });
        await gbJobOffersDraftWrapper.vm.$nextTick();

        // then
        // @ts-ignore
        expect(gbJobOffersDraftWrapper.find(GbJobOffersTableHeader).vm.isCollapsed).toBe(true);
      });
    });

    it('should set is collapsed data to false on header click event', () => {
      // given
      propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];
      const gbJobOffersDraftWrapper: Wrapper<Vue> = shallowMount(GbJobOffersDraft, {
        mocks: { $t },
        propsData,
      });
      gbJobOffersDraftWrapper.setData({ isCollapsed: true });

      // when
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = gbJobOffersDraftWrapper.find(GbJobOffersTableHeader);
      gbJobOffersTableHeaderWrapper.vm.$emit('collapse');

      // then
      // @ts-ignore
      expect(gbJobOffersDraftWrapper.vm.isCollapsed).toBe(false);
    });

    it('should set is collapsed data to true on header click event', () => {
      // given
      propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

      const gbJobOffersDraftWrapper: Wrapper<Vue> = shallowMount(GbJobOffersDraft, {
        mocks: { $t },
        propsData,
      });
      gbJobOffersDraftWrapper.setData({ isCollapsed: false });

      // when
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = gbJobOffersDraftWrapper.find(GbJobOffersTableHeader);
      gbJobOffersTableHeaderWrapper.vm.$emit('collapse');

      // then
      // @ts-ignore
      expect(gbJobOffersDraftWrapper.vm.isCollapsed).toBe(true);
    });
  });

  describe('content', () => {
    it('should not contain a div with rows if it is collapsed', async () => {
      // given
      const gbJobOffersDraftWrapper: Wrapper<Vue> = shallowMount(GbJobOffersDraft, {
        mocks: { $t },
        propsData,
      });

      // when
      gbJobOffersDraftWrapper.setData({ isCollapsed: true });
      await gbJobOffersDraftWrapper.vm.$nextTick();

      // then
      const rows: Wrapper<Vue> = gbJobOffersDraftWrapper.find('.gb-job-offers-draft--rows');
      expect(rows.exists()).toBe(false);
    });
    it('should contain a div with rows if it is collapsed', () => {
      // given
      const gbJobOffersDraftWrapper: Wrapper<Vue> = shallowMount(GbJobOffersDraft, {
        mocks: { $t },
        propsData,
      });

      // when
      gbJobOffersDraftWrapper.setData({ isCollapsed: false });

      // then
      const rows: Wrapper<Vue> = gbJobOffersDraftWrapper.find('.gb-job-offers-draft--rows');
      expect(rows.exists()).toBe(true);
    });
    it('should contain as many gb job offer table row component as job offers', () => {
      // given
      propsData.jobOffers = [
        {
          title: 'A first job offer',
        } as JobOfferResponseInterface,
        {
          title: 'A second job offer',
        } as JobOfferResponseInterface,
      ];

      // when
      const gbJobOffersDraftWrapper: Wrapper<Vue> = shallowMount(GbJobOffersDraft, {
        mocks: { $t },
        propsData,
        data,
      });

      // then
      // @ts-ignore
      expect(gbJobOffersDraftWrapper.findAll(GbJobOffersTableRow).at(0).vm.jobOffer).toStrictEqual({
        title: 'A first job offer',
      });
      // @ts-ignore
      expect(gbJobOffersDraftWrapper.findAll(GbJobOffersTableRow).at(0).vm.options).toStrictEqual({
        hideCandidatesNumber: true,
      });
      // @ts-ignore
      expect(gbJobOffersDraftWrapper.findAll(GbJobOffersTableRow).at(1).vm.jobOffer).toStrictEqual({
        title: 'A second job offer',
      });
      // @ts-ignore
      expect(gbJobOffersDraftWrapper.findAll(GbJobOffersTableRow).at(1).vm.options).toStrictEqual({
        hideCandidatesNumber: true,
      });
    });

    describe('edit draft link', () => {
      it('should contain a link to create job offer', () => {
        // given
        propsData.jobOffers = [
          {
            id: 'aJobOffer',
            title: 'A first job offer',
            publishedAt: new Date(),
          } as JobOfferResponseInterface,
        ];
        const gbJobOffersDraftWrapper: Wrapper<Vue> = mount(GbJobOffersDraft, {
          stubs,
          mocks: { $t, $route },
          propsData,
          data,
        });

        // when
        const nuxtLinkStubWrapper: Wrapper<Vue> = gbJobOffersDraftWrapper.findAll('nuxt-link-stub').at(0);

        // then
        expect(nuxtLinkStubWrapper.attributes()).toHaveProperty('to', '/fr-ca/company/job-offer/create/aJobOffer');
      });

      it('should contain edit icon link in gb job offer table row component', () => {
        // given
        propsData.jobOffers = [
          {
            title: 'A first job offer',
            publishedAt: new Date(),
          } as JobOfferResponseInterface,
        ];
        const gbJobOffersDraftWrapper: Wrapper<Vue> = mount(GbJobOffersDraft, {
          stubs,
          mocks: { $t, $route },
          propsData,
          data,
        });

        // when
        const gbJobOffersTableRowWrapper: Wrapper<Vue> = gbJobOffersDraftWrapper.findAll(GbJobOffersTableRow).at(0);

        // then
        // @ts-ignore
        expect(gbJobOffersTableRowWrapper.findAll(GbIcon).at(0).vm.options).toStrictEqual({
          iconName: 'edit_blue',
        });
      });
    });
  });
});

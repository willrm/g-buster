import { mount, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { JobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import GbJobOffersPendingValidation from '../gb-job-offers-pending-validation.vue';
import GbJobOffersTableHeader from '../gb-job-offers-table-header.vue';
import GbJobOffersTableRow from '../gb-job-offers-table-row.vue';

describe('components/company/dashboard/gb-job-offers-pending-validation', () => {
  let $t: jest.Mock;
  let propsData: {
    jobOffers: JobOfferResponseInterface[];
  };
  let data: () => {
    isCollapsed: boolean;
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffers: [],
    };
    data = () => ({
      isCollapsed: false,
    });
  });

  describe('header', () => {
    it('should contain a gb job offer table header component with options', () => {
      // given
      propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

      // when
      const gbJobOffersPendingValidationWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPendingValidation, {
        mocks: { $t },
        propsData,
        data,
      });

      // then
      // @ts-ignore
      expect(gbJobOffersPendingValidationWrapper.find(GbJobOffersTableHeader).vm.options).toStrictEqual({
        labelKey: 'companyDashboardJobOffers.jobOffersPendingValidationTitle',
        jobOffersCount: 3,
        isHighlighted: false,
      });
    });

    describe('is collapsed', () => {
      it('should contain a gb job offer table header component with is collapsed as false from data', () => {
        // given
        propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

        // when
        const gbJobOffersPendingValidationWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPendingValidation, {
          mocks: { $t },
          propsData,
        });
        gbJobOffersPendingValidationWrapper.setData({ isCollapsed: false });

        // then
        // @ts-ignore
        expect(gbJobOffersPendingValidationWrapper.find(GbJobOffersTableHeader).vm.isCollapsed).toBe(false);
      });

      it('should contain a gb job offer table header component with is collapsed as true from data', async () => {
        // given
        propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

        // when
        const gbJobOffersPendingValidationWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPendingValidation, {
          mocks: { $t },
          propsData,
        });
        gbJobOffersPendingValidationWrapper.setData({ isCollapsed: true });
        await gbJobOffersPendingValidationWrapper.vm.$nextTick();

        // then
        // @ts-ignore
        expect(gbJobOffersPendingValidationWrapper.find(GbJobOffersTableHeader).vm.isCollapsed).toBe(true);
      });
    });

    it('should set is collapsed data to false on header click event', () => {
      // given
      propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];
      const gbJobOffersPendingValidationWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPendingValidation, {
        mocks: { $t },
        propsData,
      });
      gbJobOffersPendingValidationWrapper.setData({ isCollapsed: true });

      // when
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = gbJobOffersPendingValidationWrapper.find(GbJobOffersTableHeader);
      gbJobOffersTableHeaderWrapper.vm.$emit('collapse');

      // then
      // @ts-ignore
      expect(gbJobOffersPendingValidationWrapper.vm.isCollapsed).toBe(false);
    });

    it('should set is collapsed data to true on header click event', () => {
      // given
      propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

      const gbJobOffersPendingValidationWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPendingValidation, {
        mocks: { $t },
        propsData,
      });
      gbJobOffersPendingValidationWrapper.setData({ isCollapsed: false });

      // when
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = gbJobOffersPendingValidationWrapper.find(GbJobOffersTableHeader);
      gbJobOffersTableHeaderWrapper.vm.$emit('collapse');

      // then
      // @ts-ignore
      expect(gbJobOffersPendingValidationWrapper.vm.isCollapsed).toBe(true);
    });
  });

  describe('content', () => {
    it('should not contain a div with rows if it is collapsed', async () => {
      // given
      const gbJobOffersPendingValidationWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPendingValidation, {
        mocks: { $t },
        propsData,
      });

      // when
      gbJobOffersPendingValidationWrapper.setData({ isCollapsed: true });
      await gbJobOffersPendingValidationWrapper.vm.$nextTick();

      // then
      const rows: Wrapper<Vue> = gbJobOffersPendingValidationWrapper.find('.gb-job-offers-pending-validation--rows');
      expect(rows.exists()).toBe(false);
    });

    it('should contain a div with rows if it is not collapsed', () => {
      // given
      const gbJobOffersPendingValidationWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPendingValidation, {
        mocks: { $t },
        propsData,
      });

      // when
      gbJobOffersPendingValidationWrapper.setData({ isCollapsed: false });

      // then
      const rows: Wrapper<Vue> = gbJobOffersPendingValidationWrapper.find('.gb-job-offers-pending-validation--rows');
      expect(rows.exists()).toBe(true);
    });
    it('should contain as many gb job offer table row component as job offers', () => {
      // given
      propsData.jobOffers = [
        {
          title: 'A first job offer',
        } as JobOfferResponseInterface,
        {
          title: 'A second job offer',
        } as JobOfferResponseInterface,
      ];

      // when
      const gbJobOffersPendingValidationWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPendingValidation, {
        mocks: { $t },
        propsData,
        data,
      });

      // then
      // @ts-ignore
      expect(gbJobOffersPendingValidationWrapper.findAll(GbJobOffersTableRow).at(0).vm.jobOffer).toStrictEqual({
        title: 'A first job offer',
      });
      // @ts-ignore
      expect(gbJobOffersPendingValidationWrapper.findAll(GbJobOffersTableRow).at(0).vm.options).toStrictEqual({
        hideCandidatesNumber: true,
      });
      // @ts-ignore
      expect(gbJobOffersPendingValidationWrapper.findAll(GbJobOffersTableRow).at(1).vm.jobOffer).toStrictEqual({
        title: 'A second job offer',
      });
      // @ts-ignore
      expect(gbJobOffersPendingValidationWrapper.findAll(GbJobOffersTableRow).at(1).vm.options).toStrictEqual({
        hideCandidatesNumber: true,
      });
    });

    it('should contain boost icon link in gb job offer table row component', () => {
      // given
      propsData.jobOffers = [
        {
          title: 'A first job offer',
        } as JobOfferResponseInterface,
      ];
      const gbJobOffersPendingValidationWrapper: Wrapper<Vue> = mount(GbJobOffersPendingValidation, {
        mocks: { $t },
        propsData,
        data,
      });

      // when
      const gbJobOffersTableRowWrapper: Wrapper<Vue> = gbJobOffersPendingValidationWrapper.findAll(GbJobOffersTableRow).at(0);

      // then
      // @ts-ignore
      expect(gbJobOffersTableRowWrapper.findAll(GbIcon).at(0).vm.options).toStrictEqual({
        iconName: 'rocket_blue',
      });
    });
  });
});

import { mount, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { JobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import GbJobOffersPublished from '../gb-job-offers-published.vue';
import GbJobOffersTableHeader from '../gb-job-offers-table-header.vue';
import GbJobOffersTableRow from '../gb-job-offers-table-row.vue';

describe('components/company/dashboard/gb-job-offers-published', () => {
  let $t: jest.Mock;
  let propsData: {
    jobOffers: JobOfferResponseInterface[];
  };
  let data: () => {
    isCollapsed: boolean;
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffers: [],
    };
    data = () => ({
      isCollapsed: false,
    });
  });

  describe('header', () => {
    it('should contain a gb job offer table header component with options', () => {
      // given
      propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

      // when
      const gbJobOffersPublishedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPublished, {
        mocks: { $t },
        propsData,
        data,
      });

      // then
      // @ts-ignore
      expect(gbJobOffersPublishedWrapper.find(GbJobOffersTableHeader).vm.options).toStrictEqual({
        labelKey: 'companyDashboardJobOffers.jobOffersPublishedTitle',
        jobOffersCount: 3,
        isHighlighted: true,
      });
    });

    describe('is collapsed', () => {
      it('should contain a gb job offer table header component with is collapsed as false from data', () => {
        // given
        propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

        // when
        const gbJobOffersPublishedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPublished, {
          mocks: { $t },
          propsData,
        });
        gbJobOffersPublishedWrapper.setData({ isCollapsed: false });

        // then
        // @ts-ignore
        expect(gbJobOffersPublishedWrapper.find(GbJobOffersTableHeader).vm.isCollapsed).toBe(false);
      });

      it('should contain a gb job offer table header component with is collapsed as true from data', async () => {
        // given
        propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

        // when
        const gbJobOffersPublishedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPublished, {
          mocks: { $t },
          propsData,
        });
        gbJobOffersPublishedWrapper.setData({ isCollapsed: true });
        await gbJobOffersPublishedWrapper.vm.$nextTick();

        // then
        // @ts-ignore
        expect(gbJobOffersPublishedWrapper.find(GbJobOffersTableHeader).vm.isCollapsed).toBe(true);
      });
    });

    it('should set is collapsed data to false on header click event', () => {
      // given
      propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];
      const gbJobOffersPublishedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPublished, {
        mocks: { $t },
        propsData,
      });
      gbJobOffersPublishedWrapper.setData({ isCollapsed: true });

      // when
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = gbJobOffersPublishedWrapper.find(GbJobOffersTableHeader);
      gbJobOffersTableHeaderWrapper.vm.$emit('collapse');

      // then
      // @ts-ignore
      expect(gbJobOffersPublishedWrapper.vm.isCollapsed).toBe(false);
    });

    it('should set is collapsed data to true on header click event', () => {
      // given
      propsData.jobOffers = [{} as JobOfferResponseInterface, {} as JobOfferResponseInterface, {} as JobOfferResponseInterface];

      const gbJobOffersPublishedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPublished, {
        mocks: { $t },
        propsData,
      });
      gbJobOffersPublishedWrapper.setData({ isCollapsed: false });

      // when
      const gbJobOffersTableHeaderWrapper: Wrapper<Vue> = gbJobOffersPublishedWrapper.find(GbJobOffersTableHeader);
      gbJobOffersTableHeaderWrapper.vm.$emit('collapse');

      // then
      // @ts-ignore
      expect(gbJobOffersPublishedWrapper.vm.isCollapsed).toBe(true);
    });
  });

  describe('content', () => {
    it('should not contain a div with rows if it is collapsed', async () => {
      // given
      const gbJobOffersPublishedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPublished, {
        mocks: { $t },
        propsData,
      });

      // when
      gbJobOffersPublishedWrapper.setData({ isCollapsed: true });
      await gbJobOffersPublishedWrapper.vm.$nextTick();

      // then
      const rows: Wrapper<Vue> = gbJobOffersPublishedWrapper.find('.gb-job-offers-published--rows');
      expect(rows.exists()).toBe(false);
    });
    it('should contain a div with rows if it is collapsed', () => {
      // given
      const gbJobOffersPublishedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPublished, {
        mocks: { $t },
        propsData,
      });

      // when
      gbJobOffersPublishedWrapper.setData({ isCollapsed: false });

      // then
      const rows: Wrapper<Vue> = gbJobOffersPublishedWrapper.find('.gb-job-offers-published--rows');
      expect(rows.exists()).toBe(true);
    });
    it('should contain as many gb job offer table row component as job offers', () => {
      // given
      propsData.jobOffers = [
        {
          title: 'A first job offer',
        } as JobOfferResponseInterface,
        {
          title: 'A second job offer',
        } as JobOfferResponseInterface,
      ];

      // when
      const gbJobOffersPublishedWrapper: Wrapper<Vue> = shallowMount(GbJobOffersPublished, {
        mocks: { $t },
        propsData,
        data,
      });

      // then
      // @ts-ignore
      expect(gbJobOffersPublishedWrapper.findAll(GbJobOffersTableRow).at(0).vm.jobOffer).toStrictEqual({
        title: 'A first job offer',
      });
      // @ts-ignore
      expect(gbJobOffersPublishedWrapper.findAll(GbJobOffersTableRow).at(1).vm.jobOffer).toStrictEqual({
        title: 'A second job offer',
      });
    });
    it('should contain extend icon link in gb job offer table row component', () => {
      // given
      propsData.jobOffers = [
        {
          title: 'A first job offer',
          publishedAt: new Date(),
        } as JobOfferResponseInterface,
      ];
      const gbJobOffersPublishedWrapper: Wrapper<Vue> = mount(GbJobOffersPublished, {
        mocks: { $t },
        propsData,
        data,
      });

      // when
      const gbJobOffersTableRowWrapper: Wrapper<Vue> = gbJobOffersPublishedWrapper.findAll(GbJobOffersTableRow).at(0);

      // then
      // @ts-ignore
      expect(gbJobOffersTableRowWrapper.findAll(GbIcon).at(0).vm.options).toStrictEqual({
        iconName: 'clock_blue',
      });
    });

    it('should contain boost icon link in gb job offer table row component', () => {
      // given
      propsData.jobOffers = [
        {
          title: 'A first job offer',
          publishedAt: new Date(),
        } as JobOfferResponseInterface,
      ];
      const gbJobOffersPublishedWrapper: Wrapper<Vue> = mount(GbJobOffersPublished, {
        mocks: { $t },
        propsData,
        data,
      });

      // when
      const gbJobOffersTableRowWrapper: Wrapper<Vue> = gbJobOffersPublishedWrapper.findAll(GbJobOffersTableRow).at(0);

      // then
      // @ts-ignore
      expect(gbJobOffersTableRowWrapper.findAll(GbIcon).at(1).vm.options).toStrictEqual({
        iconName: 'rocket_blue',
      });
    });
  });
});

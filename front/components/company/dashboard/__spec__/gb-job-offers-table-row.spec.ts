import { mount, Slots, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import { JobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import GbJobOffersTableRow from '../gb-job-offers-table-row.vue';

describe('components/company/dashboard/gb-job-offers-table-row', () => {
  let $t: jest.Mock;

  let propsData: {
    jobOffer: JobOfferResponseInterface;
    options: {
      hideCandidatesNumber?: boolean;
    };
  };

  beforeEach(() => {
    $t = jest.fn();

    propsData = {
      jobOffer: {} as JobOfferResponseInterface,
      options: {
        hideCandidatesNumber: false,
      },
    };
  });

  describe('title', () => {
    it('should display job offer title', () => {
      // given
      const expected: string = 'A job offer title';
      propsData.jobOffer.title = expected;

      // when
      const gbJobOffersTableRowWrapper: Wrapper<Vue> = mount(GbJobOffersTableRow, {
        mocks: { $t },
        propsData,
      });

      // then
      expect(gbJobOffersTableRowWrapper.find('.gb-job-offers-table-row--title').text()).toBe(expected);
    });
  });

  describe('publication date', () => {
    it('should display an empty string if no job offer published at date', () => {
      // when
      const gbJobOffersTableRowWrapper: Wrapper<Vue> = mount(GbJobOffersTableRow, {
        mocks: { $t },
        propsData,
      });

      // then
      expect(gbJobOffersTableRowWrapper.find('.gb-job-offers-table-row--published-at').text()).toBe('');
    });

    it('should display job offer published at date in format YYYY-MM-DD', () => {
      // given
      const fixedDate: Date = new Date(2019, 11, 23);
      propsData.jobOffer.publishedAt = fixedDate;

      const expected: string = '2019-12-23';

      // when
      const gbJobOffersTableRowWrapper: Wrapper<Vue> = mount(GbJobOffersTableRow, {
        mocks: { $t },
        propsData,
      });

      // then
      expect(gbJobOffersTableRowWrapper.find('.gb-job-offers-table-row--published-at').text()).toBe(expected);
    });
  });

  describe('city', () => {
    it('should display job offer city', () => {
      // given
      const expected: string = 'Montreal';
      propsData.jobOffer.city = expected;

      // when
      const gbJobOffersTableRowWrapper: Wrapper<Vue> = mount(GbJobOffersTableRow, {
        mocks: { $t },
        propsData,
      });

      // then
      expect(gbJobOffersTableRowWrapper.find('.gb-job-offers-table-row--city').text()).toBe(expected);
    });
  });

  describe('number of candidates', () => {
    it('should not display text if there is option to not hide candidates number', () => {
      // given
      propsData.options.hideCandidatesNumber = true;

      // when
      const gbJobOffersTableRowWrapper: Wrapper<Vue> = mount(GbJobOffersTableRow, {
        mocks: { $t },
        propsData,
      });

      // then
      expect(gbJobOffersTableRowWrapper.find('.gb-job-offers-table-row--candidates').text()).toBe('');
    });
    it('should display a hyphen character if there is no candidates', () => {
      // when
      const gbJobOffersTableRowWrapper: Wrapper<Vue> = mount(GbJobOffersTableRow, {
        mocks: { $t },
        propsData,
      });

      // then
      expect(gbJobOffersTableRowWrapper.find('.gb-job-offers-table-row--candidates').text()).toBe('-');
    });
  });

  describe('actions slot', () => {
    it('should match snapshot', () => {
      // given
      const slots: Slots = {
        actions: '<div><button>Extend</button><button>Boost</button></div>',
      };

      // when
      const gbJobOffersTableRowWrapper: Wrapper<Vue> = mount(GbJobOffersTableRow, {
        slots,
        mocks: { $t },
        propsData,
      });

      // then
      expect(gbJobOffersTableRowWrapper.element).toMatchSnapshot();
    });
  });
});

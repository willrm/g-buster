import { mount, Stubs, Wrapper, WrapperArray } from '@vue/test-utils';
import GbCompanyLogo from '~/components/base-components/gb-company-logo.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbCompanyCardValue from '~/components/company/card/gb-company-card-value.vue';
import { CompanyEnums } from '../../../../../back/src/infrastructure/rest/career/models/company-constraints-and-enums';
import {
  CompanyResponseInterface,
  LogoResponseInterface,
  ValueResponseInterface,
} from '../../../../../back/src/infrastructure/rest/career/models/company-response.interface';
import GbCompanyCard from '../gb-company-card.vue';

describe('components/company/card/gb-company-card', () => {
  let propsData: {
    company: CompanyResponseInterface;
  };

  let $t: jest.Mock;
  let $tc: jest.Mock;

  let stubs: Stubs;

  beforeEach(() => {
    propsData = {
      company: {
        name: 'Company name',
        size: CompanyEnums.CompanySize.EIGHTY_TO_HUNDRED_NINETEEN,
        bachelorsOfEngineeringNumber: 4444,
        values: [
          {
            justification: 'A default value',
            meaning: 'A default meaning',
          },
          {
            justification: 'An other value',
            meaning: 'An other meaning',
          },
        ] as ValueResponseInterface[],
        logo: {
          base64: 'base64 image',
          mimeType: 'mime type image',
        } as LogoResponseInterface,
      } as CompanyResponseInterface,
    };

    $t = jest.fn();
    $tc = jest.fn();

    // @ts-ignore
    $tc.mockImplementation((key: string, count: number) => {
      return count > 1 ? 'bachelors' : 'bachelor';
    });

    stubs = { 'nuxt-link': true };
  });

  describe('logo', () => {
    it('should display gb-company-logo company', () => {
      // Given
      const gbCompanyCardWrapper: Wrapper<Vue> = mount(GbCompanyCard, { stubs, mocks: { $t, $tc }, propsData });

      // When
      const gbCompanyLogoItems: WrapperArray<Vue> = gbCompanyCardWrapper.findAll(GbCompanyLogo);

      // then
      expect(gbCompanyLogoItems.length).toBe(1);
    });
    it('should display company logo from input', () => {
      // Given
      const expected: LogoResponseInterface = {
        base64: 'base64 image',
        mimeType: 'mime type image',
      } as LogoResponseInterface;
      const gbCompanyCardWrapper: Wrapper<Vue> = mount(GbCompanyCard, { stubs, mocks: { $t, $tc }, propsData });

      // When
      const gbCompanyLogoItems: WrapperArray<Vue> = gbCompanyCardWrapper.findAll(GbCompanyLogo);

      // then
      // @ts-ignore
      expect(gbCompanyLogoItems.at(0).vm.options.logo).toStrictEqual(expected);
    });
    it('should have company name as logo alternative', () => {
      // given
      const expected: string = 'Company name';
      const gbCompanyCardWrapper: Wrapper<Vue> = mount(GbCompanyCard, { stubs, mocks: { $t, $tc }, propsData });

      // When
      const gbCompanyLogoItems: WrapperArray<Vue> = gbCompanyCardWrapper.findAll(GbCompanyLogo);

      // then
      // @ts-ignore
      expect(gbCompanyLogoItems.at(0).vm.options.alt).toStrictEqual(expected);
    });
  });

  describe('size', () => {
    it('should contain gb icon for company size', () => {
      // given
      const gbCompanyCardWrapper: Wrapper<Vue> = mount(GbCompanyCard, { stubs, mocks: { $t, $tc }, propsData });

      // when
      const gbIcon: Wrapper<Vue> = gbCompanyCardWrapper.findAll(GbIcon).at(0);

      // then
      // @ts-ignore
      expect(gbIcon.vm.options).toStrictEqual({
        iconName: 'people_yellow',
      });
    });
    it('should contain i18n label for company size ', () => {
      // given
      const expected: string = '80-119 persons';
      $t.mockImplementation((key: string) => {
        return key === 'company.people' ? 'persons' : '80-119';
      });

      // when
      const gbCompanyCardWrapper: Wrapper<Vue> = mount(GbCompanyCard, { stubs, mocks: { $t, $tc }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('company.eightyToHundredNineteen');
      expect($t).toHaveBeenCalledWith('company.people');
      expect(gbCompanyCardWrapper.find('.gb-company-overview-block--size').text()).toBe(expected);
    });
  });

  describe('bachelors', () => {
    it('should contain tag for number of bachelors in company', () => {
      // given
      const gbCompanyCardWrapper: Wrapper<Vue> = mount(GbCompanyCard, { stubs, mocks: { $t, $tc }, propsData });

      // when
      const bachelorsNumber: Wrapper<Vue> = gbCompanyCardWrapper.find('.gb-company-overview-block--bachelor .left-element');

      // then
      // @ts-ignore
      expect(bachelorsNumber.text()).toBe(propsData.company.bachelorsOfEngineeringNumber.toString());
    });

    it('should display a singular label when there is no bachelor in company', () => {
      // given
      propsData.company.bachelorsOfEngineeringNumber = 0;
      const gbCompanyCardWrapper: Wrapper<Vue> = mount(GbCompanyCard, { stubs, mocks: { $t, $tc }, propsData });

      // when
      const bachelorLabelWrapper: Wrapper<Vue> = gbCompanyCardWrapper.find('.gb-company-overview-block--bachelor .right-element');

      // then
      expect(bachelorLabelWrapper.text()).toBe('bachelor');
    });

    it('should display a singular label when there is a single bachelor in company', () => {
      // given
      // @ts-ignore
      propsData.company.bachelorsOfEngineeringNumber = 1;
      const gbCompanyCardWrapper: Wrapper<Vue> = mount(GbCompanyCard, { stubs, mocks: { $t, $tc }, propsData });

      // when
      const bachelorLabelWrapper: Wrapper<Vue> = gbCompanyCardWrapper.find('.gb-company-overview-block--bachelor .right-element');

      // then
      expect(bachelorLabelWrapper.text()).toBe('bachelor');
    });

    it('should display a plural label when there is two or more bachelors in company', () => {
      // given
      // @ts-ignore
      const gbCompanyCardWrapper: Wrapper<Vue> = mount(GbCompanyCard, { stubs, mocks: { $t, $tc }, propsData });

      // when
      const bachelorLabelWrapper: Wrapper<Vue> = gbCompanyCardWrapper.find('.gb-company-overview-block--bachelor .right-element');

      // then
      expect(bachelorLabelWrapper.text()).toBe('bachelors');
    });
  });

  describe('value', () => {
    it('should display gbCompanyCardValue component', () => {
      // when
      const gbCompanyCardWrapper: Wrapper<Vue> = mount(GbCompanyCard, { stubs, mocks: { $t, $tc }, propsData });

      // then
      expect(gbCompanyCardWrapper.findAll(GbCompanyCardValue).length).toBe(1);
    });

    it('should have call gbCompanyCardValue component with a value from company', () => {
      // given
      const gbCompanyCardWrapper: Wrapper<Vue> = mount(GbCompanyCard, { stubs, mocks: { $t, $tc }, propsData });

      // when
      const gbCompanyCardValue: Wrapper<Vue> = gbCompanyCardWrapper.findAll(GbCompanyCardValue).at(0);

      // then
      // @ts-ignore
      expect(propsData.company.values).toContainEqual(gbCompanyCardValue.vm.value);
    });
  });

  describe('learn more link', () => {
    it('should display nuxt-link to go on company view', () => {
      // given
      $t.mockReturnValue('Learn more');

      // when
      const gbCompanyCardWrapper: Wrapper<Vue> = mount(GbCompanyCard, { stubs, mocks: { $t, $tc }, propsData });

      // then
      const nuxtLinkStubWrapper: Wrapper<Vue> = gbCompanyCardWrapper.find('nuxt-link-stub');
      expect($t).toHaveBeenCalledWith('learnMore');
      expect(nuxtLinkStubWrapper.attributes()).toHaveProperty('to', '/');
      expect(nuxtLinkStubWrapper.text()).toBe('Learn more');
    });
  });
});

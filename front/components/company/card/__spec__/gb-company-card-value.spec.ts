import { Wrapper, shallowMount } from '@vue/test-utils';
import { ValueResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/company-response.interface';
import GbCompanyCardValue from '../gb-company-card-value.vue';
import GbBadge from '~/components/base-components/gb-badge.vue';

describe('components/company/card/gb-company-card-value', () => {
  let propsData: {
    value: ValueResponseInterface;
  };
  let $t: jest.Mock;

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: {
        justification: 'Value justification',
        meaning: 'Value meaning',
      } as ValueResponseInterface,
    };
  });
  describe('Main title', () => {
    it('should contain i18n label for title', () => {
      // given
      const expected: string = 'Value title';
      $t.mockReturnValue(expected);

      // when
      const gbCompanyCardValueWrapper: Wrapper<Vue> = shallowMount(GbCompanyCardValue, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('company.value');
      expect(gbCompanyCardValueWrapper.find('.gb-company-value-block--title').text()).toBe(expected);
    });
  });

  describe('Badge', () => {
    it('should display a gbBadge component', () => {
      // when
      const gbCompanyCardValueWrapper: Wrapper<Vue> = shallowMount(GbCompanyCardValue, { mocks: { $t }, propsData });

      // then
      expect(gbCompanyCardValueWrapper.findAll(GbBadge).length).toBe(1);
    });

    it('should display a gbBadge component with no highlight', () => {
      // when
      const gbCompanyCardValueWrapper: Wrapper<Vue> = shallowMount(GbCompanyCardValue, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbCompanyCardValueWrapper.findAll(GbBadge).at(0).vm.options).toStrictEqual({
        badgeIsHighlighted: false,
      });
    });

    it('should display a gbBadge component with meaning value', () => {
      // when
      const gbCompanyCardValueWrapper: Wrapper<Vue> = shallowMount(GbCompanyCardValue, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(
        gbCompanyCardValueWrapper
          .findAll(GbBadge)
          .at(0)
          .text()
      ).toBe(propsData.value.meaning);
    });
  });

  describe('Justification', () => {
    it('should display value justification', () => {
      // when
      const gbCompanyCardValueWrapper: Wrapper<Vue> = shallowMount(GbCompanyCardValue, { mocks: { $t }, propsData });

      // then
      expect(gbCompanyCardValueWrapper.find('.gb-company-value-block--justification').text()).toBe(propsData.value.justification);
    });
  });
});

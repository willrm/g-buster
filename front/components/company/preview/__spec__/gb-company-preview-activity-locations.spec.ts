import { mount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { PostActivityLocationsRequestInterface } from '../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import GbCompanyPreviewActivityLocations from '../gb-company-preview-activity-locations.vue';

describe('components/company/preview/gb-company-preview-activity-locations', () => {
  let $t: jest.Mock;
  let $tc: jest.Mock;
  let propsData: {
    activityLocations: PostActivityLocationsRequestInterface;
  };
  beforeEach(() => {
    $t = jest.fn();
    $tc = jest.fn();
    propsData = {
      activityLocations: {} as PostActivityLocationsRequestInterface,
    };
  });
  describe('title', () => {
    beforeEach(() => {});
    it('should contain the title in h2', () => {
      // given
      const expected: string = 'Activity locations';
      $t.mockReturnValue(expected);

      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find('h2');

      // then
      expect(result.text()).toBe(expected);
    });
  });

  describe('quebec', () => {
    it('should not contain GbElementWithIcon if there is no quebec value', () => {
      // given
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find(GbElementWithIcon);

      // then
      expect(result.element).toBeUndefined();
    });
    it('should contain GbElementWithIcon with options if there is quebec value', () => {
      // given
      propsData.activityLocations.quebec = 12;
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find(GbElementWithIcon);

      // then
      // @ts-ignore
      expect(result.vm.options.inputWithIconPosition).toBe('OUTSIDE');
    });
    it('should contain GbIcon with options if there is quebec value with options', () => {
      // given
      propsData.activityLocations.quebec = 12;
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options.iconName).toBe('quebec_flag');
    });
    it('should contain translations with singular for more than one site in quebec', () => {
      // given
      propsData.activityLocations.quebec = 1;
      $tc.mockReturnValue('site');
      $t.mockReturnValue('in Québec');
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find('span');

      // then
      // @ts-ignore
      expect(result.text()).toBe('1 site in Québec');
    });
    it('should contain translations with plural for more than one site in quebec', () => {
      // given
      propsData.activityLocations.quebec = 12;
      $tc.mockReturnValue('sites');
      $t.mockReturnValue('in Québec');
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find('span');

      // then
      // @ts-ignore
      expect(result.text()).toBe('12 sites in Québec');
    });
  });

  describe('canada', () => {
    it('should not contain GbElementWithIcon if there is no canada value', () => {
      // given
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find(GbElementWithIcon);

      // then
      expect(result.element).toBeUndefined();
    });
    it('should contain GbElementWithIcon with options if there is canada value', () => {
      // given
      propsData.activityLocations.canada = 12;
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find(GbElementWithIcon);

      // then
      // @ts-ignore
      expect(result.vm.options.inputWithIconPosition).toBe('OUTSIDE');
    });
    it('should contain GbIcon with options if there is canada value with options', () => {
      // given
      propsData.activityLocations.canada = 12;
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options.iconName).toBe('canada_flag');
    });
    it('should contain translations with singular for more than one site in canada', () => {
      // given
      propsData.activityLocations.canada = 1;
      $tc.mockReturnValue('site');
      $t.mockReturnValue('in Canada');
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find('span');

      // then
      // @ts-ignore
      expect(result.text()).toBe('1 site in Canada');
    });
    it('should contain translations with plural for more than one site in canada', () => {
      // given
      propsData.activityLocations.canada = 12;
      $tc.mockReturnValue('sites');
      $t.mockReturnValue('in Canada');
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find('span');

      // then
      // @ts-ignore
      expect(result.text()).toBe('12 sites in Canada');
    });
  });

  describe('usa', () => {
    it('should not contain GbElementWithIcon if there is no usa value', () => {
      // given
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find(GbElementWithIcon);

      // then
      expect(result.element).toBeUndefined();
    });
    it('should contain GbElementWithIcon with options if there is usa value', () => {
      // given
      propsData.activityLocations.usa = 12;
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find(GbElementWithIcon);

      // then
      // @ts-ignore
      expect(result.vm.options.inputWithIconPosition).toBe('OUTSIDE');
    });
    it('should contain GbIcon with options if there is usa value with options', () => {
      // given
      propsData.activityLocations.usa = 12;
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options.iconName).toBe('usa_flag');
    });
    it('should contain translations with singular for more than one site in usa', () => {
      // given
      propsData.activityLocations.usa = 1;
      $tc.mockReturnValue('site');
      $t.mockReturnValue('in USA');
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find('span');

      // then
      // @ts-ignore
      expect(result.text()).toBe('1 site in USA');
    });
    it('should contain translations with plural for more than one site in usa', () => {
      // given
      propsData.activityLocations.usa = 12;
      $tc.mockReturnValue('sites');
      $t.mockReturnValue('in USA');
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find('span');

      // then
      // @ts-ignore
      expect(result.text()).toBe('12 sites in USA');
    });
  });

  describe('international', () => {
    it('should not contain GbElementWithIcon if there is no international value', () => {
      // given
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find(GbElementWithIcon);

      // then
      expect(result.element).toBeUndefined();
    });
    it('should contain GbElementWithIcon with options if there is international value', () => {
      // given
      propsData.activityLocations.international = 12;
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find(GbElementWithIcon);

      // then
      // @ts-ignore
      expect(result.vm.options.inputWithIconPosition).toBe('OUTSIDE');
    });
    it('should contain GbIcon with options if there is international value with options', () => {
      // given
      propsData.activityLocations.international = 12;
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options.iconName).toBe('international_flag');
    });
    it('should contain translations with singular for more than one site in international', () => {
      // given
      propsData.activityLocations.international = 1;
      $tc.mockReturnValue('site');
      $t.mockReturnValue('in the world');
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find('span');

      // then
      // @ts-ignore
      expect(result.text()).toBe('1 site in the world');
    });
    it('should contain translations with plural for more than one site in international', () => {
      // given
      propsData.activityLocations.international = 12;
      $tc.mockReturnValue('sites');
      $t.mockReturnValue('in the world');
      const gbCompanyPreviewActivityLocationsWrapper: Wrapper<Vue> = mount(GbCompanyPreviewActivityLocations, {
        mocks: { $t, $tc },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewActivityLocationsWrapper.find('span');

      // then
      // @ts-ignore
      expect(result.text()).toBe('12 sites in the world');
    });
  });
});

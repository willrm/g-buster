import { mount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBadge from '~/components/base-components/gb-badge.vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { CompanyEnums } from '../../../../../back/src/infrastructure/rest/career/models/company-constraints-and-enums';
import {
  PostActivityLocationsRequestInterface,
  PostCompanyRequestInterface,
} from '../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import GbCompanyPreviewActivityLocations from '../gb-company-preview-activity-locations.vue';
import GbCompanyPreviewContent from '../gb-company-preview-content.vue';
import GbCompanyPreviewValues from '../gb-company-preview-values.vue';

describe('components/company/preview/gb-company-preview-content', () => {
  let $t: jest.Mock;
  let $tc: jest.Mock;
  let propsData: {
    company: PostCompanyRequestInterface;
  };

  beforeEach(() => {
    $t = jest.fn();
    $tc = jest.fn();
    propsData = {
      company: {
        activityLocations: {} as PostActivityLocationsRequestInterface,
      } as PostCompanyRequestInterface,
    };
  });

  describe('GbCompanyPreviewValues', () => {
    it('should contain GbCompanyPreviewValues component with props', () => {
      // given
      propsData.company.values = [{ meaning: 'A value', justification: 'A value justification' }];
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = mount(GbCompanyPreviewContent, { propsData, mocks: { $t, $tc } });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewContentWrapper.find(GbCompanyPreviewValues);

      // then
      // @ts-ignore
      expect(result.vm.values).toStrictEqual([{ meaning: 'A value', justification: 'A value justification' }]);
    });
  });

  describe('geniusTypes', () => {
    it('should contain geniusTypes card title in h2', () => {
      // given
      $t.mockReturnValue('Genius types');
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = mount(GbCompanyPreviewContent, { propsData, mocks: { $t } });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewContentWrapper.find('#genius-types h2');

      // then
      expect(result.text()).toBe('Genius types');
    });
    it('should contain as many rounded GbBadge components as geniusTypes', () => {
      // given
      propsData.company.geniusTypes = [
        CompanyEnums.CompanyGeniusType.GENIUS_TYPE_CIVIL_ENGINEERING,
        CompanyEnums.CompanyGeniusType.GENIUS_TYPE_CHEMICAL_ENGINEERING,
      ];
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = mount(GbCompanyPreviewContent, { propsData, mocks: { $t } });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewContentWrapper.findAll(GbBadge);

      // then
      expect(result).toHaveLength(2);
      // @ts-ignore
      expect(result.at(0).vm.options).toStrictEqual({ badgeIsRounded: true });
      // @ts-ignore
      expect(result.at(1).vm.options).toStrictEqual({ badgeIsRounded: true });
    });
    it('should be in GbBadge slots', () => {
      // given
      $t.mockImplementation((key: string) => {
        return key === 'company.geniusTypeCivilEngineering'
          ? 'Civil Engineering'
          : key === 'company.geniusTypeAerospaceEngineering'
          ? 'Aerospace Engineering'
          : null;
      });
      propsData.company.geniusTypes = [
        CompanyEnums.CompanyGeniusType.GENIUS_TYPE_CIVIL_ENGINEERING,
        CompanyEnums.CompanyGeniusType.GENIUS_TYPE_AEROSPACE_ENGINEERING,
      ];
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = mount(GbCompanyPreviewContent, { propsData, mocks: { $t } });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewContentWrapper.findAll(GbBadge);

      // then
      expect(result.at(0).text()).toBe('Civil Engineering');
      expect(result.at(1).text()).toBe('Aerospace Engineering');
    });
  });
  describe('bachelorsOfEngineeringNumber', () => {
    it('should contain bachelorsOfEngineeringNumber card title in h2', () => {
      // given
      $t.mockReturnValue('Bachelors Of Engineering Number');
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = mount(GbCompanyPreviewContent, { propsData, mocks: { $t } });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewContentWrapper.find('#bachelors-of-engineering-number h2');

      // then
      expect(result.text()).toBe('Bachelors Of Engineering Number');
    });
    it('should contain bachelorsOfEngineeringNumber in a span', () => {
      // given
      propsData.company.bachelorsOfEngineeringNumber = 12;
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = mount(GbCompanyPreviewContent, { propsData, mocks: { $t } });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewContentWrapper.find('#bachelors-of-engineering-number span');

      // then
      expect(result.text()).toBe('12');
    });
  });
  describe('revenueForRD', () => {
    it('should not contain revenueForRD card when no data', () => {
      // given
      propsData.company.revenueForRD = undefined;

      // when
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = mount(GbCompanyPreviewContent, { propsData, mocks: { $t } });

      // then
      expect(gbCompanyPreviewContentWrapper.findAll('#revenue-for-rd h2')).toHaveLength(0);
    });
    it('should contain revenueForRD card title in h2', () => {
      // given
      propsData.company.revenueForRD = CompanyEnums.CompanyRevenueForRD.REVENUE_RD_BETWEEN_100k_AND_1M;
      $t.mockReturnValue('Revenue for R&D');
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = mount(GbCompanyPreviewContent, { propsData, mocks: { $t } });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewContentWrapper.find('#revenue-for-rd h2');

      // then
      expect(result.text()).toBe('Revenue for R&D');
    });
    it('should contain GbElementWithIcon', () => {
      // given
      propsData.company.revenueForRD = CompanyEnums.CompanyRevenueForRD.REVENUE_RD_BETWEEN_100k_AND_1M;
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = mount(GbCompanyPreviewContent, { propsData, mocks: { $t } });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewContentWrapper.find(GbElementWithIcon);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ inputWithIconPosition: 'OUTSIDE' });
    });
    it('should contain GbIcon', () => {
      // given
      propsData.company.revenueForRD = CompanyEnums.CompanyRevenueForRD.REVENUE_RD_BETWEEN_100k_AND_1M;
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = mount(GbCompanyPreviewContent, { propsData, mocks: { $t } });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewContentWrapper.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ iconName: 'money_blue' });
    });
    it('should contain translation for revenueForRd', () => {
      // given
      $t.mockReturnValue('Revenue between 1m and 5m');
      propsData.company.revenueForRD = CompanyEnums.CompanyRevenueForRD.REVENUE_RD_BETWEEN_1M_AND_5M;
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = mount(GbCompanyPreviewContent, { propsData, mocks: { $t } });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewContentWrapper.find('#revenue-for-rd span');

      // then
      expect(result.text()).toBe('Revenue between 1m and 5m');
    });
  });
  describe('GbCompanyPreviewActivityLocations', () => {
    it('should contain GbCompanyPreviewActivityLocations component with props', () => {
      // given
      propsData.company.activityLocations = { quebec: 12 };
      const gbCompanyPreviewContentWrapper: Wrapper<Vue> = mount(GbCompanyPreviewContent, { propsData, mocks: { $t, $tc } });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewContentWrapper.find(GbCompanyPreviewActivityLocations);

      // then
      // @ts-ignore
      expect(result.vm.activityLocations).toStrictEqual({ quebec: 12 });
    });
  });
});

import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbCompanyPreviewSocialNetworks from '../gb-company-preview-social-networks.vue';

describe('components/company/preview/gb-company-preview-social-networks', () => {
  it('should not contain links if there is neither linkedIn or Facebook fields', () => {
    // given
    const gbCompanyPreviewSocialNetworksWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewSocialNetworks, { propsData: { socialNetworks: {} } });

    // when
    const results: WrapperArray<Vue> = gbCompanyPreviewSocialNetworksWrapper.findAll('a');

    // then
    expect(results.length).toBe(0);
  });

  describe('linkedIn', () => {
    it('should contain a linkedIn link if there is a linkedIn field', () => {
      // given
      const gbCompanyPreviewSocialNetworksWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewSocialNetworks, {
        propsData: { socialNetworks: { linkedIn: 'http://linkedIn.link' } },
      });

      // when
      const results: WrapperArray<Vue> = gbCompanyPreviewSocialNetworksWrapper.findAll('a');

      // then
      expect(results.length).toBe(1);
      expect(results.at(0).attributes('href')).toBe('http://linkedIn.link');
    });

    it('should contain a linkedIn GbIcon if there is a linkedIn field', () => {
      // given
      const gbCompanyPreviewSocialNetworksWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewSocialNetworks, {
        propsData: { socialNetworks: { linkedIn: 'http://linkedIn.link' } },
      });

      // when
      const results: WrapperArray<Vue> = gbCompanyPreviewSocialNetworksWrapper.findAll(GbIcon);

      // then
      expect(results.length).toBe(1);
      // @ts-ignore
      expect(results.at(0).vm.options).toStrictEqual({ iconName: 'linkedin_circle' });
    });
  });

  describe('facebook', () => {
    it('should contain a facebook link if there is a facebook field', () => {
      // given
      const gbCompanyPreviewSocialNetworksWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewSocialNetworks, {
        propsData: { socialNetworks: { facebook: 'http://facebook.link' } },
      });

      // when
      const results: WrapperArray<Vue> = gbCompanyPreviewSocialNetworksWrapper.findAll('a');

      // then
      expect(results.length).toBe(1);
      expect(results.at(0).attributes('href')).toBe('http://facebook.link');
    });
    it('should contain a facebook GbIcon if there is a facebook field', () => {
      // given
      const gbCompanyPreviewSocialNetworksWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewSocialNetworks, {
        propsData: { socialNetworks: { facebook: 'http://facebook.link' } },
      });

      // when
      const results: WrapperArray<Vue> = gbCompanyPreviewSocialNetworksWrapper.findAll(GbIcon);

      // then
      expect(results.length).toBe(1);
      // @ts-ignore
      expect(results.at(0).vm.options).toStrictEqual({ iconName: 'facebook_circle' });
    });
  });
});

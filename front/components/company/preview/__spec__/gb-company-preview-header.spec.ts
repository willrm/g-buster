import { mount, shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { CompanyEnums } from '../../../../../back/src/infrastructure/rest/career/models/company-constraints-and-enums';
import {
  PostActivityLocationsRequestInterface,
  PostCompanyRequestInterface,
  PostSocialNetworksRequestInterface,
  PostValueRequestInterface,
} from '../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { TemporaryFileResponseInterface } from '../../../../../back/src/infrastructure/rest/upload/models/temporary-file-response.interface';
import GbCompanyPreviewHeader from '../gb-company-preview-header.vue';
import GbCompanyPreviewSocialNetworks from '../gb-company-preview-social-networks.vue';

describe('components/company/preview/gb-company-preview-header', () => {
  let $t: jest.Mock;
  let propsData: {
    company: PostCompanyRequestInterface;
    companyLogo: TemporaryFileResponseInterface;
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      company: {
        activityLocations: {} as PostActivityLocationsRequestInterface,
        geniusTypes: [] as CompanyEnums.CompanyGeniusType[],
        socialNetworks: {} as PostSocialNetworksRequestInterface,
        values: [{} as PostValueRequestInterface],
      } as PostCompanyRequestInterface,
      companyLogo: {} as TemporaryFileResponseInterface,
    };
  });

  describe('logo', () => {
    it('should display company logo in img with attributes', () => {
      // given
      propsData.company.name = 'A company';
      propsData.companyLogo = { base64: 'an-image-as-base64', mimeType: 'mime/type' } as TemporaryFileResponseInterface;
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewHeader, { propsData });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll('img');

      // then
      expect(result.length).toBe(1);
      expect(result.at(0).attributes('src')).toBe('data:mime/type;base64,an-image-as-base64');
      expect(result.at(0).attributes('alt')).toBe('A company logo');
    });
  });

  describe('name', () => {
    it('should display company name in h1', () => {
      // given
      propsData.company.name = 'A company name';
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewHeader, { propsData });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll('h1');

      // then
      expect(result.length).toBe(1);
      expect(result.at(0).text()).toBe('A company name');
    });
  });
  describe('website', () => {
    it('should display company website in a tag', () => {
      // given
      propsData.company.website = 'http://a.company.website';
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewHeader, { propsData });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll('a');

      // then
      expect(result.length).toBe(1);
      expect(result.at(0).text()).toBe('http://a.company.website');
    });
    it('should have a href to website', () => {
      // given
      propsData.company.website = 'http://a.company.website';
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewHeader, { propsData });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll('a');

      // then
      expect(result.length).toBe(1);
      expect(result.at(0).attributes('href')).toBe('http://a.company.website');
    });
    it('should have a target to _blank', () => {
      // given
      propsData.company.website = 'http://a.company.website';
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewHeader, { propsData });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll('a');

      // then
      expect(result.length).toBe(1);
      expect(result.at(0).attributes('target')).toBe('_blank');
    });
  });
  describe('GbCompanyPreviewSocialNetworks', () => {
    it('should not contain GbCompanyPreviewSocialNetworks if there is no socialNetworks', () => {
      // given
      propsData.company.socialNetworks = undefined;
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewHeader, { propsData });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll(GbCompanyPreviewSocialNetworks);

      // then
      expect(result.length).toBe(0);
    });
    it('should contain GbCompanyPreviewSocialNetworks if there is socialNetworks with it as a prop', () => {
      // given
      propsData.company.socialNetworks = { facebook: 'http://a.facebook.link' };
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewHeader, { propsData });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll(GbCompanyPreviewSocialNetworks);

      // then
      expect(result.length).toBe(1);
      // @ts-ignore
      expect(result.at(0).vm.socialNetworks).toStrictEqual({ facebook: 'http://a.facebook.link' });
    });
  });
  describe('presentation', () => {
    it('should display company presentation in p tag', () => {
      // given
      propsData.company.presentation = 'A company presentation';
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = shallowMount(GbCompanyPreviewHeader, { propsData });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll('p');

      // then
      expect(result.length).toBe(1);
      expect(result.at(0).text()).toBe('A company presentation');
    });
  });
  describe('address', () => {
    it('should display GbIcon component with props for company address', () => {
      // given
      const expected: object = { iconName: 'address_blue' };

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = mount(GbCompanyPreviewHeader, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display span with company address', () => {
      // given
      propsData.company.address = 'A company address';

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = mount(GbCompanyPreviewHeader, { mocks: { $t }, propsData });

      // then
      const span: Wrapper<Vue> = gbCompanyPreviewHeaderWrapper.find('span#address');

      expect(span.text()).toBe(propsData.company.address);
    });
  });
  describe('creationDate', () => {
    it('should display GbElementWithIcon component with options props', () => {
      // given
      const expected: object = { inputWithIconPosition: 'OUTSIDE' };

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = mount(GbCompanyPreviewHeader, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll(GbElementWithIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.inputWithIconPosition === expected.inputWithIconPosition
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbIcon component with props for company creationDate', () => {
      // given
      const expected: object = { iconName: 'years_blue' };

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = mount(GbCompanyPreviewHeader, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display span with company address', () => {
      // given
      const fixedDate: Date = new Date();
      propsData.company.creationDate = fixedDate;

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = mount(GbCompanyPreviewHeader, { mocks: { $t }, propsData });

      // then
      const span: Wrapper<Vue> = gbCompanyPreviewHeaderWrapper.find('span#creation-date');

      expect(span.text()).toBe(propsData.company.creationDate.getFullYear().toString());
    });
  });
  describe('revenue', () => {
    it('should display GbIcon component with props for company revenue', () => {
      // given
      const expected: object = { iconName: 'money_blue' };

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = mount(GbCompanyPreviewHeader, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display span with company revenue with only years', () => {
      // given
      $t.mockImplementation((key: string) => {
        return key === 'company.revenueBetween1MAnd5M' ? 'between 1m and 5m' : key === 'company.annual' ? 'annual' : null;
      });
      propsData.company.revenue = CompanyEnums.CompanyRevenue.REVENUE_BETWEEN_1M_AND_5M;

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = mount(GbCompanyPreviewHeader, { mocks: { $t }, propsData });

      // then
      const span: Wrapper<Vue> = gbCompanyPreviewHeaderWrapper.find('span#revenue');

      expect(span.text()).toBe('between 1m and 5m / annual');
    });
  });
  describe('businessSegment', () => {
    beforeEach(() => {
      $t.mockReturnValue('business segment environment');
    });
    it('should display GbIcon component with props for company businessSegment', () => {
      // given
      const expected: object = { iconName: 'square_blue' };

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = mount(GbCompanyPreviewHeader, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display span with company businessSegment with only years', () => {
      // given
      propsData.company.businessSegment = CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_ENVIRONMENT;

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = mount(GbCompanyPreviewHeader, { mocks: { $t }, propsData });

      // then
      const span: Wrapper<Vue> = gbCompanyPreviewHeaderWrapper.find('span#business-segment');

      expect(span.text()).toBe('business segment environment');
    });
  });
  describe('size', () => {
    beforeEach(() => {
      $t.mockReturnValueOnce('ten to nineteen');
      $t.mockReturnValueOnce('people');
    });
    it('should display GbIcon component with props for company size', () => {
      // given
      const expected: object = { iconName: 'people_blue' };

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = mount(GbCompanyPreviewHeader, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyPreviewHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display span with company size with only years', () => {
      // given
      $t.mockImplementation((key: string) => {
        return key === 'company.tenToNineteen' ? 'ten to nineteen' : key === 'company.people' ? 'people' : null;
      });
      propsData.company.size = CompanyEnums.CompanySize.TEN_TO_NINETEEN;

      // when
      const gbCompanyPreviewHeaderWrapper: Wrapper<Vue> = mount(GbCompanyPreviewHeader, { mocks: { $t }, propsData });

      // then
      const span: Wrapper<Vue> = gbCompanyPreviewHeaderWrapper.find('span#size');

      expect(span.text()).toBe('ten to nineteen people');
    });
  });
});

import { mount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBadge from '~/components/base-components/gb-badge.vue';
import GbCard from '~/components/base-components/gb-card.vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { PostValueRequestInterface } from '../../../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import GbCompanyPreviewValues from '../gb-company-preview-values.vue';

describe('components/company/preview/gb-company-preview-values', () => {
  let $t: jest.Mock;
  let propsData: {
    values: PostValueRequestInterface[];
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      values: [] as PostValueRequestInterface[],
    };
  });

  describe('GbCard', () => {
    it('should contain a card without options', () => {
      // given
      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewValuesWrapper.find(GbCard);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({});
    });
  });

  describe('title', () => {
    beforeEach(() => {});
    it('should contain the title in h2', () => {
      // given
      const expected: string = 'Company Values';
      $t.mockReturnValue(expected);

      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewValuesWrapper.find('h2');

      // then
      expect(result.text()).toBe(expected);
    });
  });

  describe('GbElementWithIcon', () => {
    it('should not contain GbElementWithIcon if there is no values', () => {
      // given
      propsData.values = [];
      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewValuesWrapper.findAll(GbElementWithIcon);

      // then
      // @ts-ignore
      expect(result.length).toBe(0);
    });
    it('should contain as many GbElementWithIcon as values', () => {
      // given
      propsData.values = [{} as PostValueRequestInterface, {} as PostValueRequestInterface];
      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewValuesWrapper.findAll(GbElementWithIcon);

      // then
      // @ts-ignore
      expect(result.length).toBe(2);
    });
    it('should contain GbElementWithIcon with options', () => {
      // given
      propsData.values = [{} as PostValueRequestInterface];
      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewValuesWrapper.find(GbElementWithIcon);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ inputWithIconPosition: 'OUTSIDE', inputWithIconIsLarge: true });
    });
  });
  describe('GbIcon', () => {
    it('should not contain GbIcon if there is no values', () => {
      // given
      propsData.values = [];
      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewValuesWrapper.findAll(GbIcon);

      // then
      // @ts-ignore
      expect(result.length).toBe(0);
    });
    it('should contain as many GbIcon as values', () => {
      // given
      propsData.values = [{} as PostValueRequestInterface, {} as PostValueRequestInterface];
      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewValuesWrapper.findAll(GbIcon);

      // then
      // @ts-ignore
      expect(result.length).toBe(2);
    });
    it('should contain GbIcon with options', () => {
      // given
      propsData.values = [{} as PostValueRequestInterface];
      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewValuesWrapper.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ iconName: 'number_1_blue' });
    });
  });
  describe('meaning', () => {
    it('should not contain a GbBadge with meaning if there is no values', () => {
      // given
      propsData.values = [];
      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewValuesWrapper.findAll(GbBadge);

      // then
      // @ts-ignore
      expect(result.length).toBe(0);
    });
    it('should contain as many rounded GbBadge components with meaning, as values', () => {
      // given
      propsData.values = [{} as PostValueRequestInterface, {} as PostValueRequestInterface];
      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewValuesWrapper.findAll(GbBadge);

      // then
      expect(result).toHaveLength(2);
      // @ts-ignore
      expect(result.at(0).vm.options).toStrictEqual({ badgeIsRounded: true });
      // @ts-ignore
      expect(result.at(1).vm.options).toStrictEqual({ badgeIsRounded: true });
    });
    it('should be in GbBadge slots', () => {
      // given
      propsData.values = [{ meaning: 'A meaning' } as PostValueRequestInterface];
      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbCompanyPreviewValuesWrapper.find(GbBadge);

      // then
      // @ts-ignore
      expect(result.text()).toBe('A meaning');
    });
  });
  describe('justification', () => {
    it('should not contain a span with justification if there is no values', () => {
      // given
      propsData.values = [];
      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewValuesWrapper.findAll('span#justification-0');

      // then
      // @ts-ignore
      expect(result.length).toBe(0);
    });
    it('should contain as many GbIcon as values', () => {
      // given
      propsData.values = [{} as PostValueRequestInterface, {} as PostValueRequestInterface];
      const gbCompanyPreviewValuesWrapper: Wrapper<Vue> = mount(GbCompanyPreviewValues, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: WrapperArray<Vue> = gbCompanyPreviewValuesWrapper.findAll('span#justification-0,span#justification-1');

      // then
      // @ts-ignore
      expect(result.length).toBe(2);
    });
  });
});

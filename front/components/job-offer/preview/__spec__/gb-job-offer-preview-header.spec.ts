import { mount, shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBadge from '~/components/base-components/gb-badge.vue';
import { JobOfferEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { PutJobOfferRequestInterface } from '../../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import GbJobOfferPreviewHeader from '../gb-job-offer-preview-header.vue';

describe('components/job-offer/preview/gb-company-preview-header', () => {
  let $t: jest.Mock;
  let propsData: {
    jobOffer: PutJobOfferRequestInterface;
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffer: {} as PutJobOfferRequestInterface,
    };
  });

  describe('title', () => {
    it('should display job offer title in h1', () => {
      // given
      propsData.jobOffer.title = 'A job offer title';
      const gbJobOfferPreviewHeaderWrapper: Wrapper<Vue> = shallowMount(GbJobOfferPreviewHeader, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferPreviewHeaderWrapper.findAll('h1');

      // then
      expect(result.length).toBe(1);
      expect(result.at(0).text()).toBe('A job offer title');
    });
  });

  describe('jobStatus', () => {
    it('should display GbBadge with job offer job status translation', () => {
      // given
      $t.mockReturnValue('A job status');
      propsData.jobOffer.jobStatus = JobOfferEnums.JobOfferJobStatus.JOB_STATUS_PART_TIME;

      // when
      const gbJobOfferPreviewHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferPreviewHeader, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.jobStatusPartTime');
      const gbBadgeWrapper: Wrapper<Vue> = gbJobOfferPreviewHeaderWrapper.findAll(GbBadge).at(0);
      expect(gbBadgeWrapper.text()).toBe('A job status');
      // @ts-ignore
      expect(gbBadgeWrapper.vm.options).toStrictEqual({ badgeIsHighlighted: true });
    });
  });

  describe('jobType', () => {
    it('should display important GbBadge with job offer job type translation', () => {
      // given
      $t.mockReturnValue('A job type');
      propsData.jobOffer.jobType = JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR;

      // when
      const gbJobOfferPreviewHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferPreviewHeader, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.jobTypeRegular');
      const gbBadgeWrapper: Wrapper<Vue> = gbJobOfferPreviewHeaderWrapper.findAll(GbBadge).at(1);
      expect(gbBadgeWrapper.text()).toBe('A job type');
      // @ts-ignore
      expect(gbBadgeWrapper.vm.options).toStrictEqual({ badgeIsHighlighted: true });
    });
  });
});

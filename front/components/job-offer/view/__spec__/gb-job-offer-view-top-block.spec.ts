import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import { repeat } from 'lodash';
import Vue from 'vue';
import GbButton from '~/components/base-components/gb-button.vue';
import GbLoginBlock from '~/components/base-components/gb-login-block.vue';
import GbCompanyCard from '~/components/company/card/gb-company-card.vue';
import GbJobOfferViewDetails from '~/components/job-offer/view/gb-job-offer-view-details.vue';
import { JobOfferWithCompanyResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import GbJobOfferViewTopBlock from '../gb-job-offer-view-top-block.vue';

describe('components/job-offer/view/gb-job-offer-view-top-block', () => {
  let $t: jest.Mock;

  let propsData: {
    jobOffer: JobOfferWithCompanyResponseInterface;
    isLoggedAsCandidate: boolean;
  };

  beforeEach(() => {
    $t = jest.fn();

    propsData = {
      jobOffer: {} as JobOfferWithCompanyResponseInterface,
      isLoggedAsCandidate: false,
    };
  });

  describe('GbJobOfferViewDetails', () => {
    it('should have component with job-offer from store through getters', () => {
      // given
      propsData.jobOffer = { title: 'A job title' } as JobOfferWithCompanyResponseInterface;
      const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

      // when
      const gbJobOfferDetailsWrapper: Wrapper<Vue> = gbJobOfferViewTopBlockWrapper.find(GbJobOfferViewDetails);

      // then
      // @ts-ignore
      expect(gbJobOfferDetailsWrapper.vm.jobOffer).toStrictEqual({
        title: 'A job title',
      });
    });
  });

  describe('info', () => {
    it('should display it on 6 columns when connected as candidate', () => {
      // given
      propsData.jobOffer = { title: 'A job title' } as JobOfferWithCompanyResponseInterface;
      propsData.isLoggedAsCandidate = true;
      const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

      // when
      const gbJobOfferViewTopBlockInfoWrapper: Wrapper<Vue> = gbJobOfferViewTopBlockWrapper.find('.gb-job-offer-view-top-block--info');

      // then
      expect(gbJobOfferViewTopBlockInfoWrapper.element.classList).toContain('is-6');
    });

    it('should display it on full columns not when connected as candidate', () => {
      // given
      propsData.jobOffer = { title: 'A job title' } as JobOfferWithCompanyResponseInterface;
      propsData.isLoggedAsCandidate = false;
      const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

      // when
      const gbJobOfferViewTopBlockInfoWrapper: Wrapper<Vue> = gbJobOfferViewTopBlockWrapper.find('.gb-job-offer-view-top-block--info');

      // then
      expect(gbJobOfferViewTopBlockInfoWrapper.element.classList).toContain('is-full');
    });
  });

  describe('apply button', () => {
    it('should not have an apply button component when not connected as candidate', () => {
      // given
      propsData.jobOffer = { title: 'A job title' } as JobOfferWithCompanyResponseInterface;
      propsData.isLoggedAsCandidate = false;
      const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

      // when
      const gbButtonWrapper: WrapperArray<Vue> = gbJobOfferViewTopBlockWrapper.findAll(GbButton);

      // then
      expect(gbButtonWrapper.length).toBe(0);
    });
    it('should have an apply button component when connected as candidate', () => {
      // given
      propsData.jobOffer = { title: 'A job title' } as JobOfferWithCompanyResponseInterface;
      propsData.isLoggedAsCandidate = true;
      const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

      // when
      const gbButtonWrapper: Wrapper<Vue> = gbJobOfferViewTopBlockWrapper.find(GbButton);

      // then
      // @ts-ignore
      expect(gbButtonWrapper.vm.options).toStrictEqual({
        buttonType: 'button',
        buttonLabelKey: 'apply',
      });
    });
  });

  describe('job offer description', () => {
    describe('header', () => {
      it('should display section header translation', () => {
        // given
        const expected: string = 'Job offer description';
        $t.mockReturnValue(expected);

        // when
        const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

        // then
        expect($t).toHaveBeenCalledWith('jobOffer.descriptionLabelKey');
        expect(gbJobOfferViewTopBlockWrapper.find('.gb-job-offer-view-top-block--description--header').text()).toBe(expected);
      });
    });

    describe('content', () => {
      it('should display description text with not logged class', () => {
        // given
        const expected: string = repeat('*', 500);
        propsData.jobOffer.description = expected;
        propsData.isLoggedAsCandidate = false;

        // when
        const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

        // then
        const jobOfferDescriptionContentWrapper: Wrapper<Vue> = gbJobOfferViewTopBlockWrapper.find(
          '.gb-job-offer-view-top-block--description--content'
        );
        expect(jobOfferDescriptionContentWrapper.text()).toBe(expected);
        expect(jobOfferDescriptionContentWrapper.element.classList).toContain('not-logged');
      });

      it('should display description text without not logged class', () => {
        // given
        const expected: string = repeat('*', 500);
        propsData.jobOffer.description = expected;
        propsData.isLoggedAsCandidate = true;

        // when
        const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

        // then
        const jobOfferDescriptionContentWrapper: Wrapper<Vue> = gbJobOfferViewTopBlockWrapper.find(
          '.gb-job-offer-view-top-block--description--content'
        );
        expect(jobOfferDescriptionContentWrapper.text()).toBe(expected);
        expect(jobOfferDescriptionContentWrapper.element.classList).not.toContain('not-logged');
      });
    });
  });

  describe('job offer specificities', () => {
    it('should not display specificities text when not logged as candidate', () => {
      // given
      propsData.isLoggedAsCandidate = false;

      // when
      const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

      // then
      expect(gbJobOfferViewTopBlockWrapper.findAll('.gb-job-offer-view-top-block--specificities').length).toBe(0);
    });

    it('should not display specificities text when there is no specificities text', () => {
      // given
      propsData.isLoggedAsCandidate = true;
      propsData.jobOffer.specificities = undefined;

      // when
      const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

      // then
      expect(gbJobOfferViewTopBlockWrapper.findAll('.gb-job-offer-view-top-block--specificities').length).toBe(0);
    });

    describe('header', () => {
      it('should display section header translation', () => {
        // given
        propsData.isLoggedAsCandidate = true;
        propsData.jobOffer.specificities = 'specificities';

        const expected: string = 'Job offer specificities';
        $t.mockReturnValue(expected);

        // when
        const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

        // then
        expect($t).toHaveBeenCalledWith('jobOffer.specificitiesLabelKey');
        expect(gbJobOfferViewTopBlockWrapper.find('.gb-job-offer-view-top-block--specificities--header').text()).toBe(expected);
      });
    });

    describe('content', () => {
      it('should display specificities text', () => {
        // given
        propsData.isLoggedAsCandidate = true;
        const expected: string = repeat('*', 500);
        propsData.jobOffer.specificities = expected;

        // when
        const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

        // then
        const jobOfferDescriptionContentWrapper: Wrapper<Vue> = gbJobOfferViewTopBlockWrapper.find(
          '.gb-job-offer-view-top-block--specificities--content'
        );
        expect(jobOfferDescriptionContentWrapper.text()).toBe(expected);
      });
    });
  });

  describe('GbLoginBlock', () => {
    it('should not display login block when connected as candidate', () => {
      // when
      propsData.isLoggedAsCandidate = true;
      const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

      // then
      expect(gbJobOfferViewTopBlockWrapper.findAll(GbLoginBlock).length).toBe(0);
    });
    it('should display login block when not connected as candidate', () => {
      // when
      propsData.isLoggedAsCandidate = false;
      const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

      // then
      // @ts-ignore
      expect(gbJobOfferViewTopBlockWrapper.find(GbLoginBlock).vm.options).toStrictEqual({ isLarge: true });
    });
  });

  describe('GbCompanyCard', () => {
    it('should display company card when user is logged as candidate', () => {
      // given
      propsData.isLoggedAsCandidate = true;

      // when
      const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

      // then
      expect(gbJobOfferViewTopBlockWrapper.findAll(GbCompanyCard).length).toBe(1);
    });

    it('should not display company card when user is not logged as candidate', () => {
      // when
      const gbJobOfferViewTopBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } });

      // then
      expect(gbJobOfferViewTopBlockWrapper.findAll(GbCompanyCard).length).toBe(0);
    });

    it('should call gbCompanyCard with job offer company', () => {
      // given
      propsData.isLoggedAsCandidate = true;

      // when
      const gbCompanyCard: Wrapper<Vue> = shallowMount(GbJobOfferViewTopBlock, { propsData, mocks: { $t } }).find(GbCompanyCard);

      // then
      // @ts-ignore
      expect(gbCompanyCard.vm.company).toStrictEqual(propsData.jobOffer.company);
    });
  });
});

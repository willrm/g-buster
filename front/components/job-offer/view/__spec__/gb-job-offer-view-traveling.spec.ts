import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBadge from '~/components/base-components/gb-badge.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbJobOfferViewTraveling from '~/components/job-offer/view/gb-job-offer-view-traveling.vue';
import { GetJobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/get-job-offer-response.interface';
import { TravelingEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';

describe('components/job-offer/view/gb-job-offer-view-travelling', () => {
  let $t: jest.Mock;

  let propsData: {
    jobOffer: GetJobOfferResponseInterface;
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffer: {
        traveling: {},
      } as GetJobOfferResponseInterface,
    };
  });

  describe('title', () => {
    it('should contain a title for traveling', () => {
      // given
      const expected: string = 'Advantages';
      $t.mockReturnValue(expected);

      // when
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.travelingLabel');
      expect(gbJobOfferViewTravelingWrapper.find('.title').text()).toBe(expected);
    });
  });

  describe('no traveling', () => {
    it('should contain a no traveling text when there is no traveling', () => {
      // given
      const expected: string = 'No traveling';
      $t.mockImplementation((domainKey: string) => {
        if ('jobOffer.travelingNoTravelingLabel' === domainKey) {
          return expected;
        }
      });

      propsData.jobOffer.traveling = {
        isTraveling: false,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewTravelingWrapper.find('.gb-job-offer-view-traveling--no-traveling');

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.travelingNoTravelingLabel');
      expect(result.text()).toBe(expected);
    });
  });

  describe('quebec', () => {
    it('should not contain an element for Quebec flag when there is no Quebec as traveling destination', () => {
      // given
      propsData.jobOffer.traveling = {
        quebec: undefined,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferViewTravelingWrapper.findAll('.gb-job-offer-view-traveling--quebec');

      // then
      expect(result.length).toBe(0);
    });
    it('should contain a GbIcon with Quebec flag when there is Quebec as traveling destination', () => {
      // given
      propsData.jobOffer.traveling = {
        quebec: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewTravelingWrapper.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ iconName: 'quebec_flag' });
    });

    it('should contain a label for Quebec when there is Quebec as traveling destination', () => {
      // given
      $t.mockReturnValue('Québec');
      propsData.jobOffer.traveling = {
        quebec: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewTravelingWrapper.find('.gb-job-offer-view-traveling--label');

      // then
      expect($t).toHaveBeenNthCalledWith(2, 'jobOffer.travelingQuebecLabel');
      expect(result.text()).toBe('Québec');
    });

    it('should contain a GbBadge for Quebec value', () => {
      // given
      $t.mockReturnValue('Fréquents');
      propsData.jobOffer.traveling = {
        quebec: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewTravelingWrapper.find(GbBadge);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ badgeColor: 'B2C_COLOR' });
      expect($t).toHaveBeenCalledWith('jobOffer.travelingTravelingFrequencyFrequent');
      expect(result.text()).toBe('Fréquents');
    });
  });

  describe('canada', () => {
    it('should not contain an element for Canada flag when there is no Canada as traveling destination', () => {
      // given
      propsData.jobOffer.traveling = {
        canada: undefined,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferViewTravelingWrapper.findAll('.gb-job-offer-view-traveling--canada');

      // then
      expect(result.length).toBe(0);
    });
    it('should contain a GbIcon with Canada flag when there is Canada as traveling destination', () => {
      // given
      propsData.jobOffer.traveling = {
        canada: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewTravelingWrapper.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ iconName: 'canada_flag' });
    });

    it('should contain a label for Canada when there is Canada as traveling destination', () => {
      // given
      $t.mockReturnValue('Canada');
      propsData.jobOffer.traveling = {
        canada: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewTravelingWrapper.find('.gb-job-offer-view-traveling--label');

      // then
      expect($t).toHaveBeenNthCalledWith(2, 'jobOffer.travelingCanadaLabel');
      expect(result.text()).toBe('Canada');
    });

    it('should contain a GbBadge for Canada value', () => {
      // given
      $t.mockReturnValue('Fréquents');
      propsData.jobOffer.traveling = {
        canada: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewTravelingWrapper.find(GbBadge);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ badgeColor: 'B2C_COLOR' });
      expect($t).toHaveBeenCalledWith('jobOffer.travelingTravelingFrequencyFrequent');
      expect(result.text()).toBe('Fréquents');
    });
  });

  describe('international', () => {
    it('should not contain an element for International flag when there is no International as traveling destination', () => {
      // given
      propsData.jobOffer.traveling = {
        international: undefined,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferViewTravelingWrapper.findAll('.gb-job-offer-view-traveling--international');

      // then
      expect(result.length).toBe(0);
    });
    it('should contain a GbIcon with International flag when there is International as traveling destination', () => {
      // given
      propsData.jobOffer.traveling = {
        international: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewTravelingWrapper.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ iconName: 'international_flag' });
    });

    it('should contain a label for International when there is International as traveling destination', () => {
      // given
      $t.mockReturnValue('International');
      propsData.jobOffer.traveling = {
        international: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewTravelingWrapper.find('.gb-job-offer-view-traveling--label');

      // then
      expect($t).toHaveBeenNthCalledWith(2, 'jobOffer.travelingInternationalLabel');
      expect(result.text()).toBe('International');
    });

    it('should contain a GbBadge for International value', () => {
      // given
      $t.mockReturnValue('Fréquents');
      propsData.jobOffer.traveling = {
        international: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      };
      const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewTravelingWrapper.find(GbBadge);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ badgeColor: 'B2C_COLOR' });
      expect($t).toHaveBeenCalledWith('jobOffer.travelingTravelingFrequencyFrequent');
      expect(result.text()).toBe('Fréquents');
    });
  });

  it('should respect order Quebec, Canada, International', () => {
    // given
    propsData.jobOffer.traveling = {
      international: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      quebec: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      canada: TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL,
    };
    const gbJobOfferViewTravelingWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewTraveling, { mocks: { $t }, propsData });

    // when
    const result: WrapperArray<Vue> = gbJobOfferViewTravelingWrapper.findAll(
      '.gb-job-offer-view-traveling--international, .gb-job-offer-view-traveling--canada, .gb-job-offer-view-traveling--quebec'
    );

    // then
    expect(result.length).toBe(3);
    expect(result.at(0).element.classList).toContain('gb-job-offer-view-traveling--quebec');
    expect(result.at(1).element.classList).toContain('gb-job-offer-view-traveling--canada');
    expect(result.at(2).element.classList).toContain('gb-job-offer-view-traveling--international');
  });
});

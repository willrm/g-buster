import { mount, shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { GetJobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/get-job-offer-response.interface';
import GbJobOfferViewDatesBlock from '../gb-job-offer-view-dates-block.vue';

describe('components/job-offer/view/gb-job-offer-view-dates-block', () => {
  let $t: jest.Mock;
  let $d: jest.Mock;

  let propsData: {
    jobOffer: GetJobOfferResponseInterface;
  };
  beforeEach(() => {
    $t = jest.fn();
    $d = jest.fn();

    propsData = {
      jobOffer: {
        startDateOfEmployment: {},
      } as GetJobOfferResponseInterface,
    };
  });
  describe('end date of application', () => {
    describe('title', () => {
      it('should contain title for end date of application', () => {
        // given
        const expected: string = 'End date of application';
        $t.mockImplementation((domainKey: string) => {
          if ('jobOffer.endDateOfApplication' === domainKey) {
            return expected;
          }
        });

        // when
        const gbJobOfferViewDatesBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewDatesBlock, { mocks: { $t, $d }, propsData });

        // then
        expect($t).toHaveBeenCalledWith('jobOffer.endDateOfApplication');
        expect(gbJobOfferViewDatesBlockWrapper.find('.gb-job-offer-view-dates-block--end-date-of-application .title').text()).toBe(expected);
      });

      it('should contain a GbIcon', () => {
        // given
        const gbJobOfferViewDatesBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewDatesBlock, { mocks: { $t, $d }, propsData });

        // when
        const gbIconWrapper: WrapperArray<Vue> = gbJobOfferViewDatesBlockWrapper.findAll(GbIcon);

        // then
        // @ts-ignore
        expect(gbIconWrapper.at(0).vm.options).toStrictEqual({ iconName: 'calendar_yellow' });
      });

      it('should contain end date of application formatted as d MMMM yyyy', () => {
        // given
        const expected: string = '23 janvier 2019';
        $d.mockReturnValue(expected);

        const fixedDate: Date = new Date(2019, 0, 23);
        propsData.jobOffer.endDateOfApplication = fixedDate;

        const gbJobOfferViewDatesBlockWrapper: Wrapper<Vue> = mount(GbJobOfferViewDatesBlock, { mocks: { $t, $d }, propsData });

        // when
        const result: Wrapper<Vue> = gbJobOfferViewDatesBlockWrapper.find('.gb-job-offer-view-dates-block--end-date-of-application .date');

        // then
        expect($d).toHaveBeenCalledWith(fixedDate, 'medium');
        expect(result.text()).toContain(expected);
      });
    });
  });
  describe('start date of employment', () => {
    describe('title', () => {
      it('should contain title for start date of employment', () => {
        // given
        const expected: string = 'Start date of employment';
        $t.mockImplementation((domainKey: string) => {
          if ('jobOffer.startDateOfEmployment' === domainKey) {
            return expected;
          }
        });

        // when
        const gbJobOfferViewDatesBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewDatesBlock, { mocks: { $t, $d }, propsData });

        // then
        expect($t).toHaveBeenCalledWith('jobOffer.startDateOfEmployment');
        expect(gbJobOfferViewDatesBlockWrapper.find('.gb-job-offer-view-dates-block--start-date-of-employment .title').text()).toBe(expected);
      });

      it('should contain a GbIcon when it is a date', () => {
        // given
        propsData.jobOffer.startDateOfEmployment = {
          startDate: new Date(2019, 0, 23),
        };
        const gbJobOfferViewDatesBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewDatesBlock, { mocks: { $t, $d }, propsData });

        // when
        const gbIconWrapper: WrapperArray<Vue> = gbJobOfferViewDatesBlockWrapper.findAll(GbIcon);

        // then
        // @ts-ignore
        expect(gbIconWrapper.at(1).vm.options).toStrictEqual({ iconName: 'calendar_yellow' });
      });

      it('should not contain a GbIcon when it is asap', () => {
        // given
        propsData.jobOffer.startDateOfEmployment = {
          asSoonAsPossible: true,
        };
        const gbJobOfferViewDatesBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewDatesBlock, { mocks: { $t, $d }, propsData });

        // when
        const gbIconWrapper: WrapperArray<Vue> = gbJobOfferViewDatesBlockWrapper.findAll(GbIcon);

        // then
        // @ts-ignore
        expect(gbIconWrapper.length).toBe(1);
      });

      it('should contain start date of employment formatted as d MMMM yyyy when it is a date', () => {
        // given
        const expected: string = '23 janvier 2019';
        $d.mockReturnValue(expected);

        const fixedDate: Date = new Date(2019, 0, 23);
        propsData.jobOffer.startDateOfEmployment = { startDate: fixedDate };

        const gbJobOfferViewDatesBlockWrapper: Wrapper<Vue> = mount(GbJobOfferViewDatesBlock, { mocks: { $t, $d }, propsData });

        // when
        const result: Wrapper<Vue> = gbJobOfferViewDatesBlockWrapper.find('.gb-job-offer-view-dates-block--start-date-of-employment .date');

        // then
        expect($d).toHaveBeenCalledWith(fixedDate, 'medium');
        expect(result.text()).toContain(expected);
      });

      it('should contain an asap text when it is asap', () => {
        // given
        const expected: string = 'As soon as possible';
        $t.mockImplementation((domainKey: string) => {
          if ('jobOffer.startDateOfEmploymentAsSoonAsPossibleLabel' === domainKey) {
            return expected;
          }
        });

        propsData.jobOffer.startDateOfEmployment = {
          asSoonAsPossible: true,
        };
        const gbJobOfferViewDatesBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewDatesBlock, { mocks: { $t, $d }, propsData });

        // when
        const result: Wrapper<Vue> = gbJobOfferViewDatesBlockWrapper.find('.gb-job-offer-view-dates-block--start-date-of-employment .asap');

        // then
        expect($t).toHaveBeenCalledWith('jobOffer.startDateOfEmploymentAsSoonAsPossibleLabel');
        expect(result.text()).toBe(expected);
      });
    });
  });
});

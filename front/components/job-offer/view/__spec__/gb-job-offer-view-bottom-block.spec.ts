import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbButton from '~/components/base-components/gb-button.vue';
import { GetJobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/get-job-offer-response.interface';
import { AdvantagesEnums, JobOfferEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { JobOfferWithCompanyResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import GbJobOfferViewAdvantages from '../gb-job-offer-view-advantages.vue';
import GbJobOfferViewAnnualSalaryRange from '../gb-job-offer-view-annual-salary-range.vue';
import GbJobOfferViewBottomBlock from '../gb-job-offer-view-bottom-block.vue';
import GbJobOfferViewDatesBlock from '../gb-job-offer-view-dates-block.vue';
import GbJobOfferViewTraveling from '../gb-job-offer-view-traveling.vue';
import GbJobOfferViewWantedPersonality from '../gb-job-offer-view-wanted-personality.vue';

describe('components/job-offer/view/gb-job-offer-view-bottom-block', () => {
  let $t: jest.Mock;

  let propsData: {
    jobOffer: GetJobOfferResponseInterface;
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffer: {} as GetJobOfferResponseInterface,
    };
  });

  describe('header', () => {
    it('should contain header for block in h2', () => {
      // given
      const expected: string = 'Job employment conditions';
      $t.mockReturnValue(expected);

      // when
      const gbJobOfferViewBottomBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewBottomBlock, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.conditionsOfEmployment');
      expect(gbJobOfferViewBottomBlockWrapper.find('h2').text()).toBe(expected);
    });
  });

  describe('end date of application and start date of application', () => {
    it('should contain a GbJobOfferViewDatesBlock component', () => {
      // given
      const gbJobOfferViewBottomBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewBottomBlock, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewBottomBlockWrapper.find(GbJobOfferViewDatesBlock);

      // then
      // @ts-ignore
      expect(result.vm.jobOffer).toStrictEqual(propsData.jobOffer);
    });
  });

  describe('advantages', () => {
    it('should not contain a GbJobOfferViewAdvantages component when there is no advantages', () => {
      // given
      const gbJobOfferViewBottomBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewBottomBlock, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferViewBottomBlockWrapper.findAll(GbJobOfferViewAdvantages);

      // then
      expect(result.length).toBe(0);
    });

    it('should contain a GbJobOfferViewAdvantages component', () => {
      // given
      propsData.jobOffer.advantages = { social: [AdvantagesEnums.AdvantageSocial.SOCIAL_CAFETERIA] };
      const gbJobOfferViewBottomBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewBottomBlock, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewBottomBlockWrapper.find(GbJobOfferViewAdvantages);

      // then
      // @ts-ignore
      expect(result.vm.jobOffer).toStrictEqual(propsData.jobOffer);
    });
  });

  describe('annual salary range', () => {
    it('should not contain a GbJobOfferViewAnnualSalaryRange component when there is no annual salary range', () => {
      // given
      propsData.jobOffer.annualSalaryRange = {};
      const gbJobOfferViewBottomBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewBottomBlock, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferViewBottomBlockWrapper.findAll(GbJobOfferViewAnnualSalaryRange);

      // then
      expect(result.length).toBe(0);
    });

    it('should contain a GbJobOfferViewAnnualSalaryRange component', () => {
      // given
      propsData.jobOffer.annualSalaryRange = { minimumSalary: 19, maximumSalary: 1000 };
      const gbJobOfferViewBottomBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewBottomBlock, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewBottomBlockWrapper.find(GbJobOfferViewAnnualSalaryRange);

      // then
      // @ts-ignore
      expect(result.vm.jobOffer).toStrictEqual(propsData.jobOffer);
    });
  });

  describe('traveling', () => {
    it('should not contain a GbJobOfferViewTraveling component when there is no traveling', () => {
      // given
      propsData.jobOffer.traveling = {};
      const gbJobOfferViewBottomBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewBottomBlock, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferViewBottomBlockWrapper.findAll(GbJobOfferViewTraveling);

      // then
      expect(result.length).toBe(0);
    });

    it('should contain a GbJobOfferViewTraveling component', () => {
      // given
      propsData.jobOffer.traveling = { isTraveling: true };
      const gbJobOfferViewBottomBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewBottomBlock, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewBottomBlockWrapper.find(GbJobOfferViewTraveling);

      // then
      // @ts-ignore
      expect(result.vm.jobOffer).toStrictEqual(propsData.jobOffer);
    });
  });

  describe('wanted personality', () => {
    it('should not contain a GbJobOfferViewTraveling component when there is no wanted personality', () => {
      // given
      propsData.jobOffer.wantedPersonalities = [];
      const gbJobOfferViewBottomBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewBottomBlock, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferViewBottomBlockWrapper.findAll(GbJobOfferViewWantedPersonality);

      // then
      expect(result.length).toBe(0);
    });

    it('should contain a GbJobOfferViewWantedPersonality component', () => {
      // given
      propsData.jobOffer.wantedPersonalities = [JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS];
      const gbJobOfferViewBottomBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewBottomBlock, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewBottomBlockWrapper.find(GbJobOfferViewWantedPersonality);

      // then
      // @ts-ignore
      expect(result.vm.jobOffer).toStrictEqual(propsData.jobOffer);
    });
  });

  describe('apply button', () => {
    it('should have an apply button component', () => {
      // given
      propsData.jobOffer = { title: 'A job title' } as JobOfferWithCompanyResponseInterface;
      const gbJobOfferViewBottomBlockWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewBottomBlock, { mocks: { $t }, propsData });

      // when
      const gbButtonWrapper: Wrapper<Vue> = gbJobOfferViewBottomBlockWrapper.find(GbButton);

      // then
      // @ts-ignore
      expect(gbButtonWrapper.vm.options).toStrictEqual({
        buttonType: 'button',
        buttonLabelKey: 'apply',
      });
    });
  });
});

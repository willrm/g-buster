import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { GetJobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/get-job-offer-response.interface';
import { JobOfferEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import GbJobOfferViewWantedPersonality from '../gb-job-offer-view-wanted-personality.vue';

describe('components/job-offer/view/gb-job-offer-view-wanted-personality', () => {
  let $t: jest.Mock;

  let propsData: {
    jobOffer: GetJobOfferResponseInterface;
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffer: {
        traveling: {},
      } as GetJobOfferResponseInterface,
    };
  });

  describe('title', () => {
    it('should contain a title for wanted personality', () => {
      // given
      const expected: string = 'Wanted personality';
      $t.mockReturnValue(expected);

      // when
      const gbJobOfferViewWantedPersonalityWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewWantedPersonality, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.wantedPersonalitiesLabel');
      expect(gbJobOfferViewWantedPersonalityWrapper.find('h2').text()).toBe(expected);
    });
  });

  describe('personality trait', () => {
    it('should contain as many GbIcon as wanted personality trait', () => {
      // given
      propsData.jobOffer.wantedPersonalities = [
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS,
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS,
      ];
      const gbJobOfferViewWantedPersonalityWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewWantedPersonality, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferViewWantedPersonalityWrapper.findAll(GbIcon);

      // then
      expect(result.length).toBe(2);
    });

    it('should contain as many label as wanted personality trait', () => {
      // given
      $t.mockImplementation((optionLabelKey: string) => {
        if (optionLabelKey === 'jobOffer.wantedPersonalityAmbitious') return 'Ambitious';
        if (optionLabelKey === 'jobOffer.wantedPersonalityCurious') return 'Curious';

        return null;
      });
      propsData.jobOffer.wantedPersonalities = [
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS,
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS,
      ];
      const gbJobOfferViewWantedPersonalityWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewWantedPersonality, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferViewWantedPersonalityWrapper.findAll('.gb-job-offer-view-wanted-personality--item .label');

      // then
      expect($t).toHaveBeenNthCalledWith(2, 'jobOffer.wantedPersonalityAmbitious');
      expect($t).toHaveBeenNthCalledWith(3, 'jobOffer.wantedPersonalityCurious');
      expect(result.at(0).text()).toBe('Ambitious');
      expect(result.at(1).text()).toBe('Curious');
    });
  });
});

import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbJobOfferViewSalaryRange from '~/components/job-offer/view/gb-job-offer-view-annual-salary-range.vue';
import GbBadge from '~/components/base-components/gb-badge.vue';
import { GetJobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/get-job-offer-response.interface';

describe('components/job-offer/view/gb-job-offer-view-annual-salary-range', () => {
  let $t: jest.Mock;
  let $n: jest.Mock;

  let propsData: {
    jobOffer: GetJobOfferResponseInterface;
  };
  beforeEach(() => {
    $t = jest.fn();
    $n = jest.fn();
    propsData = {
      jobOffer: {
        annualSalaryRange: {},
      } as GetJobOfferResponseInterface,
    };
  });

  describe('title', () => {
    it('should contain a title for annual salary range', () => {
      // given
      const expected: string = 'Salary Range';
      $t.mockReturnValue(expected);

      // when
      const gbJobOfferViewSalaryRangeWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewSalaryRange, { mocks: { $t, $n }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.annualSalaryRangeLabel');
      expect(gbJobOfferViewSalaryRangeWrapper.find('.title').text()).toBe(expected);
    });
  });

  describe('minimum salary', () => {
    it('should contain a header for minimum salary', () => {
      // given
      const expected: string = 'Minimum';
      $t.mockReturnValue(expected);

      // when
      const gbJobOfferViewSalaryRangeWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewSalaryRange, { mocks: { $t, $n }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.annualSalaryRangeMinimumLabel');
      expect(gbJobOfferViewSalaryRangeWrapper.find('.gb-job-offer-view-annual-salary-range--minimum h3').text()).toBe(expected);
    });

    it('should contain a badge for minimum salary', () => {
      // given
      propsData.jobOffer.annualSalaryRange = {
        minimumSalary: 10,
      };
      const gbJobOfferViewSalaryRangeWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewSalaryRange, { mocks: { $t, $n }, propsData });

      // when
      const gbBadgeWrapper: WrapperArray<Vue> = gbJobOfferViewSalaryRangeWrapper.findAll(GbBadge);

      // then
      // @ts-ignore
      expect(gbBadgeWrapper.at(0).vm.options).toStrictEqual({ badgeIsRounded: true, badgeColor: 'NEUTRAL_COLOR' });
    });

    it('should contain a formatted minimum salary', () => {
      // given
      $n.mockReturnValue('$10 CAD');
      propsData.jobOffer.annualSalaryRange = {
        minimumSalary: 10,
      };
      const gbJobOfferViewSalaryRangeWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewSalaryRange, { mocks: { $t, $n }, propsData });

      // when
      const gbBadgeWrapper: WrapperArray<Vue> = gbJobOfferViewSalaryRangeWrapper.findAll(GbBadge);

      // then
      expect($n).toHaveBeenNthCalledWith(1, 10, 'currency');
      expect(gbBadgeWrapper.at(0).text()).toBe('$10 CAD');
    });
  });
  describe('maximum salary', () => {
    it('should contain a header for maximum salary', () => {
      // given
      const expected: string = 'Maximum';
      $t.mockReturnValue(expected);

      // when
      const gbJobOfferViewSalaryRangeWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewSalaryRange, { mocks: { $t, $n }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.annualSalaryRangeMaximumLabel');
      expect(gbJobOfferViewSalaryRangeWrapper.find('.gb-job-offer-view-annual-salary-range--maximum h3').text()).toBe(expected);
    });

    it('should contain a badge for maximum salary', () => {
      // given
      propsData.jobOffer.annualSalaryRange = {
        maximumSalary: 100,
      };
      const gbJobOfferViewSalaryRangeWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewSalaryRange, { mocks: { $t, $n }, propsData });

      // when
      const gbBadgeWrapper: WrapperArray<Vue> = gbJobOfferViewSalaryRangeWrapper.findAll(GbBadge);

      // then
      // @ts-ignore
      expect(gbBadgeWrapper.at(1).vm.options).toStrictEqual({ badgeIsRounded: true, badgeColor: 'NEUTRAL_COLOR' });
    });

    it('should contain a formatted maximum salary', () => {
      // given
      $n.mockReturnValue('$100 CAD');
      propsData.jobOffer.annualSalaryRange = {
        maximumSalary: 100,
      };
      const gbJobOfferViewSalaryRangeWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewSalaryRange, { mocks: { $t, $n }, propsData });

      // when
      const gbBadgeWrapper: WrapperArray<Vue> = gbJobOfferViewSalaryRangeWrapper.findAll(GbBadge);

      // then
      expect($n).toHaveBeenNthCalledWith(2, 100, 'currency');
      expect(gbBadgeWrapper.at(0).text()).toBe('$100 CAD');
    });
  });
});

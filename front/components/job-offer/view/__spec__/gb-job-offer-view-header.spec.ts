import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbJobOfferHighlight from '~/components/job-offer/gb-job-offer-highlight.vue';
import { GetJobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/get-job-offer-response.interface';
import GbJobOfferViewHeader from '../gb-job-offer-view-header.vue';

describe('components/job-offer/view/gb-job-offer-view-header', () => {
  let $t: jest.Mock;
  let $d: jest.Mock;

  let propsData: {
    jobOffer: GetJobOfferResponseInterface;
  };
  beforeEach(() => {
    $t = jest.fn();
    $d = jest.fn();
    propsData = {
      jobOffer: {} as GetJobOfferResponseInterface,
    };
  });

  describe('title', () => {
    it('should display job offer title in h1', () => {
      // given
      propsData.jobOffer.title = 'A job offer title';
      const gbJobOfferViewHeaderWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewHeader, { mocks: { $t, $d }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferViewHeaderWrapper.findAll('h1');

      // then
      expect(result.length).toBe(1);
      expect(result.at(0).text()).toBe('A job offer title');
    });
  });

  describe('highlight', () => {
    it('should contain a job offer highlight component', () => {
      // given
      propsData.jobOffer.title = 'A job offer title';

      // when
      const gbJobOfferViewHeaderWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewHeader, {
        propsData,
        mocks: { $d, $t },
      });

      // then
      // @ts-ignore
      expect(gbJobOfferViewHeaderWrapper.find(GbJobOfferHighlight).vm.jobOffer).toStrictEqual(propsData.jobOffer);
    });
  });
});

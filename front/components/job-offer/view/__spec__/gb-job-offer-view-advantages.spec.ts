import { shallowMount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBadge from '~/components/base-components/gb-badge.vue';
import { GetJobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/get-job-offer-response.interface';
import { AdvantagesEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import GbJobOfferViewAdvantages from '../gb-job-offer-view-advantages.vue';

describe('components/job-offer/view/gb-job-offer-view-advantages', () => {
  let $t: jest.Mock;

  let propsData: {
    jobOffer: GetJobOfferResponseInterface;
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffer: {} as GetJobOfferResponseInterface,
    };
  });

  describe('title', () => {
    it('should contain a title for advantages', () => {
      // given
      const expected: string = 'Advantages';
      $t.mockReturnValue(expected);

      // when
      const gbJobOfferViewAdvantagesWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewAdvantages, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.advantagesLabel');
      expect(gbJobOfferViewAdvantagesWrapper.find('.title').text()).toBe(expected);
    });
  });

  describe('advantages', () => {
    it('should contain as many GbBadge as advantages', () => {
      // given
      propsData.jobOffer.advantages = {
        health: [AdvantagesEnums.AdvantageHealth.HEALTH_LIFE_INSURANCE, AdvantagesEnums.AdvantageHealth.HEALTH_DENTAL_INSURANCE],
        social: [AdvantagesEnums.AdvantageSocial.SOCIAL_CAFETERIA],
      };

      // when
      const gbJobOfferViewAdvantagesWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewAdvantages, { mocks: { $t }, propsData });

      // then
      const gbBadgeWrapperArray: WrapperArray<Vue> = gbJobOfferViewAdvantagesWrapper.findAll(GbBadge);
      expect(gbBadgeWrapperArray.length).toBe(3);
      for (let i: number = 0; i < gbBadgeWrapperArray.length; i++) {
        // @ts-ignore
        expect(gbBadgeWrapperArray.at(i).vm.options).toStrictEqual({ badgeColor: 'B2C_COLOR' });
      }
    });
    it('should make translation for each advantage', () => {
      // given
      $t.mockImplementation((key: string) => {
        if (key === 'jobOffer.advantagesHealthHealthLifeInsurance') return 'Health Life Insurance';
        if (key === 'jobOffer.advantageHealthHealthDentalInsurance') return 'Health Dental Insurance';
        if (key === 'jobOffer.advantageSocialSocialCafeteria') return 'Social Cafeteria';
      });
      propsData.jobOffer.advantages = {
        health: [AdvantagesEnums.AdvantageHealth.HEALTH_LIFE_INSURANCE, AdvantagesEnums.AdvantageHealth.HEALTH_DENTAL_INSURANCE],
        social: [AdvantagesEnums.AdvantageSocial.SOCIAL_CAFETERIA],
      };

      // when
      const gbJobOfferViewAdvantagesWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewAdvantages, { mocks: { $t }, propsData });

      // then
      expect($t).toHaveBeenNthCalledWith(2, 'jobOffer.advantagesHealthHealthLifeInsurance');
      expect($t).toHaveBeenNthCalledWith(3, 'jobOffer.advantageHealthHealthDentalInsurance');
      expect($t).toHaveBeenNthCalledWith(4, 'jobOffer.advantageSocialSocialCafeteria');
      const gbBadgeWrapperArray: WrapperArray<Vue> = gbJobOfferViewAdvantagesWrapper.findAll(GbBadge);
      expect(gbBadgeWrapperArray.at(0).text()).toBe('Health Life Insurance');
      expect(gbBadgeWrapperArray.at(1).text()).toBe('Health Dental Insurance');
      expect(gbBadgeWrapperArray.at(2).text()).toBe('Social Cafeteria');
    });
  });
});

import { mount, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbBadge from '~/components/base-components/gb-badge.vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { GetJobOfferResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/get-job-offer-response.interface';
import { JobOfferEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import GbJobOfferViewDetail from '../gb-job-offer-view-details.vue';

describe('components/job-offer/view/gb-job-offer-view-details', () => {
  let $t: jest.Mock;

  let propsData: {
    jobOffer: GetJobOfferResponseInterface;
  };
  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffer: {} as GetJobOfferResponseInterface,
    };
  });
  describe('city', () => {
    it('should display a GbElementWithIcon for city', () => {
      // when
      const gbJobOfferViewDetailWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewDetail, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferViewDetailWrapper.findAll(GbElementWithIcon).at(0).vm.options).toStrictEqual({ inputWithIconPosition: 'OUTSIDE' });
    });

    it('should display a GbIcon for city in GbElementWithIcon', () => {
      // when
      const gbJobOfferViewDetailWrapper: Wrapper<Vue> = mount(GbJobOfferViewDetail, { mocks: { $t }, propsData });

      const gbElementWithIconWrapper: Wrapper<Vue> = gbJobOfferViewDetailWrapper.findAll(GbElementWithIcon).at(0);

      // then
      // @ts-ignore
      expect(gbElementWithIconWrapper.find(GbIcon).vm.options).toStrictEqual({ iconName: 'address_yellow' });
    });

    it('should display job offer city', () => {
      // given
      propsData.jobOffer.city = 'Montreal';

      // when
      const gbJobOfferViewDetailWrapper: Wrapper<Vue> = mount(GbJobOfferViewDetail, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewDetailWrapper.find('.gb-job-offer-view-details--city');

      // then
      expect(result.text()).toBe('Montreal');
    });
  });

  describe('requested specialities', () => {
    it('should display a GbElementWithIcon for target customers', () => {
      // when
      const gbJobOfferViewDetailWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewDetail, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferViewDetailWrapper.findAll(GbElementWithIcon).at(1).vm.options).toStrictEqual({ inputWithIconPosition: 'OUTSIDE' });
    });

    it('should display a GbIcon for target customers in GbElementWithIcon', () => {
      // when
      const gbJobOfferViewDetailWrapper: Wrapper<Vue> = mount(GbJobOfferViewDetail, { mocks: { $t }, propsData });

      const gbElementWithIconWrapper: Wrapper<Vue> = gbJobOfferViewDetailWrapper.findAll(GbElementWithIcon).at(1);

      // then
      // @ts-ignore
      expect(gbElementWithIconWrapper.find(GbIcon).vm.options).toStrictEqual({ iconName: 'wheels_yellow' });
    });

    it('should display as many GbBadge as target customers with translation', () => {
      // given
      propsData.jobOffer.requestedSpecialities = [
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CIVIL,
      ];
      $t.mockImplementation((domainKey: string) => {
        if ('jobOffer.requestedSpecialityAgriculture' === domainKey) {
          return 'Agriculture';
        } else if ('jobOffer.requestedSpecialityCivil' === domainKey) {
          return 'Civil';
        }
      });
      const gbJobOfferViewDetailWrapper: Wrapper<Vue> = mount(GbJobOfferViewDetail, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewDetailWrapper.findAll(GbElementWithIcon).at(1);

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.requestedSpecialityAgriculture');
      expect($t).toHaveBeenCalledWith('jobOffer.requestedSpecialityCivil');

      const badge1: Wrapper<Vue> = result.findAll(GbBadge).at(0);
      expect(badge1.text()).toBe('Agriculture');
      // @ts-ignore
      expect(badge1.vm.options).toStrictEqual({ badgeIsRounded: true, badgeColor: 'NEUTRAL_COLOR' });

      const badge2: Wrapper<Vue> = result.findAll(GbBadge).at(1);
      expect(badge2.text()).toBe('Civil');
      // @ts-ignore
      expect(badge2.vm.options).toStrictEqual({ badgeIsRounded: true, badgeColor: 'NEUTRAL_COLOR' });
    });
  });

  describe('target customers', () => {
    it('should display a GbElementWithIcon for target customers', () => {
      // when
      const gbJobOfferViewDetailWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewDetail, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferViewDetailWrapper.findAll(GbElementWithIcon).at(2).vm.options).toStrictEqual({ inputWithIconPosition: 'OUTSIDE' });
    });

    it('should display a GbIcon for target customers in GbElementWithIcon', () => {
      // when
      const gbJobOfferViewDetailWrapper: Wrapper<Vue> = mount(GbJobOfferViewDetail, { mocks: { $t }, propsData });

      const gbElementWithIconWrapper: Wrapper<Vue> = gbJobOfferViewDetailWrapper.findAll(GbElementWithIcon).at(2);

      // then
      // @ts-ignore
      expect(gbElementWithIconWrapper.find(GbIcon).vm.options).toStrictEqual({ iconName: 'target_yellow' });
    });

    it('should display as many GbBadge as target customers with translation', () => {
      // given
      propsData.jobOffer.targetCustomers = [
        JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP,
        JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS,
      ];
      $t.mockImplementation((domainKey: string) => {
        if ('jobOffer.targetCustomerJuniorEngineersCep' === domainKey) {
          return 'Junior Engineers / Cep';
        } else if ('jobOffer.targetCustomerEngineers' === domainKey) {
          return 'Engineers';
        }
      });
      const gbJobOfferViewDetailWrapper: Wrapper<Vue> = mount(GbJobOfferViewDetail, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewDetailWrapper.findAll(GbElementWithIcon).at(2);

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.targetCustomerJuniorEngineersCep');
      expect($t).toHaveBeenCalledWith('jobOffer.targetCustomerEngineers');

      const badge1: Wrapper<Vue> = result.findAll(GbBadge).at(0);
      expect(badge1.text()).toBe('Junior Engineers / Cep');
      // @ts-ignore
      expect(badge1.vm.options).toStrictEqual({
        badgeIsRounded: true,
        badgeColor: 'NEUTRAL_COLOR',
      });

      const badge2: Wrapper<Vue> = result.findAll(GbBadge).at(1);
      expect(badge2.text()).toBe('Engineers');
      // @ts-ignore
      expect(badge2.vm.options).toStrictEqual({
        badgeIsRounded: true,
        badgeColor: 'NEUTRAL_COLOR',
      });
    });
  });

  describe('required experience', () => {
    it('should display a GbElementWithIcon for required experience', () => {
      // when
      const gbJobOfferViewDetailWrapper: Wrapper<Vue> = shallowMount(GbJobOfferViewDetail, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferViewDetailWrapper.findAll(GbElementWithIcon).at(3).vm.options).toStrictEqual({ inputWithIconPosition: 'OUTSIDE' });
    });

    it('should display a GbIcon for required experience in GbElementWithIcon', () => {
      // when
      const gbJobOfferViewDetailWrapper: Wrapper<Vue> = mount(GbJobOfferViewDetail, { mocks: { $t }, propsData });

      const gbElementWithIconWrapper: Wrapper<Vue> = gbJobOfferViewDetailWrapper.findAll(GbElementWithIcon).at(3);

      // then
      // @ts-ignore
      expect(gbElementWithIconWrapper.find(GbIcon).vm.options).toStrictEqual({ iconName: 'timer_yellow' });
    });

    it('should display as many GbBadge as required experience with translation', () => {
      // given
      propsData.jobOffer.requiredExperiences = [
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS,
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
      ];
      $t.mockImplementation((domainKey: string) => {
        if ('jobOffer.requiredExperienceZeroToTwoYears' === domainKey) {
          return '0 to 2 years';
        } else if ('jobOffer.requiredExperienceThreeToFiveYears' === domainKey) {
          return '3 to 5 years';
        }
      });
      const gbJobOfferViewDetailWrapper: Wrapper<Vue> = mount(GbJobOfferViewDetail, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferViewDetailWrapper.findAll(GbElementWithIcon).at(3);

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.requiredExperienceZeroToTwoYears');
      expect($t).toHaveBeenCalledWith('jobOffer.requiredExperienceThreeToFiveYears');

      const badge1: Wrapper<Vue> = result.findAll(GbBadge).at(0);
      expect(badge1.text()).toBe('0 to 2 years');
      // @ts-ignore
      expect(badge1.vm.options).toStrictEqual({ badgeIsRounded: true, badgeColor: 'NEUTRAL_COLOR' });

      const badge2: Wrapper<Vue> = result.findAll(GbBadge).at(1);
      expect(badge2.text()).toBe('3 to 5 years');
      // @ts-ignore
      expect(badge2.vm.options).toStrictEqual({ badgeIsRounded: true, badgeColor: 'NEUTRAL_COLOR' });
    });
  });
});

import { mount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbBlock from '~/components/base-components/gb-block.vue';
import GbInputEmail from '~/components/base-components/gb-input-email.vue';
import { JobOfferConstraints } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbJobOfferCreateEmailAddressForReceiptOfApplications from '../gb-job-offer-create-email-address-for-receipt-of-applications.vue';

describe('components/job-offer/create/gb-job-offer-create-email-address-for-receipt-of-applications', () => {
  let $t: jest.Mock;
  let propsData: {
    value: string;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: '',
      options: {},
    };
  });

  describe('label', () => {
    it('should have a GbBlock with label in header', () => {
      // given
      const expected: string = 'Email address for receipt of applications';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper: Wrapper<Vue> = mount(GbJobOfferCreateEmailAddressForReceiptOfApplications, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper.find(GbBlock);

      // then
      expect(result.find('.gb-block--header').text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(1, 'jobOffer.emailAddressForReceiptOfApplicationsLabel');
    });
  });
  describe('email address for receipt of applications', () => {
    it('should display GbInputEmail component with props for job offer email address for receipt of applications', () => {
      // given
      const expected: object = {
        inputId: 'email-address-for-receipt-of-applications',
        inputPlaceholderKey: 'jobOffer.emailAddressForReceiptOfApplicationsPlaceholder',
        inputMaxLength: JobOfferConstraints.JOB_OFFER_EMAIL_ADDRESS_FOR_RECEIPT_OF_APPLICATIONS_MAX_LENGTH,
        inputRequired: true,
        inputIsLarge: true,
        inputErrors: undefined,
      };

      // when
      const gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper: Wrapper<Vue> = mount(GbJobOfferCreateEmailAddressForReceiptOfApplications, {
        mocks: { $t },
        propsData,
      });

      // then
      const inputText: Wrapper<Vue> = gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper.find(GbInputEmail);

      // @ts-ignore
      expect(inputText.vm.options).toStrictEqual(expected);
    });

    it('should bind job offer email address for receipt of applications to GbInputEmail input', () => {
      // given
      propsData.value = 'apply@genium360.ca';

      // when
      const gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper: Wrapper<Vue> = mount(GbJobOfferCreateEmailAddressForReceiptOfApplications, {
        mocks: { $t },
        propsData,
      });

      // then
      const inputText: Wrapper<Vue> = gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper.find(GbInputEmail);

      // @ts-ignore
      expect(inputText.vm.value).toBe('apply@genium360.ca');
    });
    it('should set inputErrors when there is a domain errors for this field', async () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        messages: ['An email address for receipt of applications error'],
        i18nMessageKeys: ['jobOffer.anEmailAddressForReceiptOfApplicationsError'],
      };
      propsData.options = { domainErrors: { emailAddressForReceiptOfApplications: expected } };
      const gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper: Wrapper<Vue> = mount(GbJobOfferCreateEmailAddressForReceiptOfApplications, {
        mocks: { $t },
        propsData,
      });

      // then
      const inputText: Wrapper<Vue> = gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper.find(GbInputEmail);

      // @ts-ignore
      expect(inputText.vm.options.inputErrors).toStrictEqual(expected);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper: Wrapper<Vue> = mount(GbJobOfferCreateEmailAddressForReceiptOfApplications, {
        mocks: { $t },
        propsData,
      });

      const inputField: Wrapper<Vue> = gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper.find('#email-address-for-receipt-of-applications');

      // when
      (inputField.element as HTMLInputElement).value = 'apply@genium360.ca';
      inputField.trigger('input');

      // then
      expect(gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper.emitted().input).toStrictEqual([['apply@genium360.ca']]);
    });
  });
});

import { mount, Stubs, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBlock from '~/components/base-components/gb-block.vue';
import GbInputCheckboxMultiple from '~/components/base-components/gb-input-checkbox-multiple.vue';
import { AdvantagesEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { PutAdvandagesRequestInterface } from '../../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbJobOfferCreateAdvantages from '../gb-job-offer-create-advantages.vue';

describe('components/job-offer/create/gb-job-offer-create-advantages', () => {
  let $t: jest.Mock;
  let propsData: {
    value: PutAdvandagesRequestInterface;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };
  let stubs: Stubs;

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: {} as PutAdvandagesRequestInterface,
      options: {},
    };

    stubs = { 'client-only': true, 'date-picker': true, 'v-popover': true };
  });

  describe('label', () => {
    it('should have a GbBlock with label in header', () => {
      // given
      const expected: string = 'Advantages';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateAdvantagesWrapper.find(GbBlock);

      // then
      expect(result.find('.gb-block--header').text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(1, 'jobOffer.advantagesLabel');
    });
  });

  describe('subheader', () => {
    it('should have a GbBlock with explanation in subheader', () => {
      // given
      const expected: string = 'Advantages subheader';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateAdvantagesWrapper.find(GbBlock);

      // then
      expect(result.find('.gb-block--subheader').text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(2, 'jobOffer.advantagesExplanation');
    });
  });

  describe('content', () => {
    describe('health advantages', () => {
      const inputId: string = 'advantage-health';
      it('should display GbInputCheckboxMultiple component with props for job offer health advantages', () => {
        // given
        const expected: object = {
          inputId,
          inputName: 'advantages-health',
          inputLabelKey: 'jobOffer.advantagesHealthLabel',
          inputOptions: [
            {
              optionLabelKey: 'jobOffer.advantageHealthHealthDentalInsurance',
              optionValue: AdvantagesEnums.AdvantageHealth.HEALTH_DENTAL_INSURANCE,
            },
            {
              optionLabelKey: 'jobOffer.advantagesHealthHealthMedicalExpensesInsurance',
              optionValue: AdvantagesEnums.AdvantageHealth.HEALTH_MEDICAL_EXPENSES_INSURANCE,
            },
            {
              optionLabelKey: 'jobOffer.advantagesHealthHealthSightInsurance',
              optionValue: AdvantagesEnums.AdvantageHealth.HEALTH_SIGHT_INSURANCE,
            },
            { optionLabelKey: 'jobOffer.advantagesHealthHealthLifeInsurance', optionValue: AdvantagesEnums.AdvantageHealth.HEALTH_LIFE_INSURANCE },
            {
              optionLabelKey: 'jobOffer.advantagesHealthHealthTravelInsurance',
              optionValue: AdvantagesEnums.AdvantageHealth.HEALTH_TRAVEL_INSURANCE,
            },
            {
              optionLabelKey: 'jobOffer.advantagesHealthHealthSalaryInsurance',
              optionValue: AdvantagesEnums.AdvantageHealth.HEALTH_SALARY_INSURANCE,
            },
            {
              optionLabelKey: 'jobOffer.advantagesHealthHealthShortTermDisabilityInsurance',
              optionValue: AdvantagesEnums.AdvantageHealth.HEALTH_SHORT_TERM_DISABILITY_INSURANCE,
            },
            {
              optionLabelKey: 'jobOffer.advantagesHealthHealthLongTermDisabilityInsurance',
              optionValue: AdvantagesEnums.AdvantageHealth.HEALTH_LONG_TERM_DISABILITY_INSURANCE,
            },
          ],
          inputErrors: undefined,
        };

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const inputs: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (input: Wrapper<Vue>) =>
            // @ts-ignore
            input.vm.options.inputId === inputId
        );
        // @ts-ignore
        expect(inputs.at(0).vm.options).toStrictEqual(expected);
      });

      it('should display GbInputCheckboxMultiple component with input errors for job offer health advantages', () => {
        // given
        const expected: DomainFieldErrorsResponseInterface = {
          i18nMessageKeys: ['jobOffer.aHealthAdvantagesError'],
          messages: ['A health advantages error'],
        };
        propsData.options.domainErrors = { advantages: expected };

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const inputs: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (input: Wrapper<Vue>) =>
            // @ts-ignore
            input.vm.options.inputId === inputId
        );
        // @ts-ignore
        expect(inputs.at(0).vm.options.inputErrors).toStrictEqual(expected);
      });

      it('should bind job offer health advantages property to GbInputCheckboxMultiple value prop', () => {
        // given
        const advantages: PutAdvandagesRequestInterface = {
          health: [AdvantagesEnums.AdvantageHealth.HEALTH_LIFE_INSURANCE, AdvantagesEnums.AdvantageHealth.HEALTH_DENTAL_INSURANCE],
          professional: [AdvantagesEnums.AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM],
        };
        propsData.value = advantages;
        // @ts-ignore
        const expected: AdvantagesEnums.AdvantageHealth[] = advantages.health;

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const inputs: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (input: Wrapper<Vue>) =>
            // @ts-ignore
            input.vm.options.inputId === inputId
        );

        // @ts-ignore
        expect(inputs.at(0).vm.value).toStrictEqual(expected);
      });
      it('should emit input event to parent when change event is triggered on select', () => {
        // given
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        const inputField: Wrapper<Vue> = gbJobOfferCreateAdvantagesWrapper.find(`#${inputId}_1`);

        // when
        // @ts-ignore
        inputField.setChecked();

        // then
        const expected: PutAdvandagesRequestInterface = {
          health: [AdvantagesEnums.AdvantageHealth.HEALTH_DENTAL_INSURANCE],
        };

        expect(gbJobOfferCreateAdvantagesWrapper.emitted().input).toStrictEqual([[expected]]);
      });
    });

    describe('social advantages', () => {
      const inputId: string = 'advantage-social';
      it('should display GbInputCheckboxMultiple component with props for job offer social advantages', () => {
        // given
        const expected: object = {
          inputId,
          inputName: 'advantages-social',
          inputLabelKey: 'jobOffer.advantagesSocialLabel',

          inputOptions: [
            {
              optionLabelKey: 'jobOffer.advantageSocialSocialSocialActivities',
              optionValue: AdvantagesEnums.AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES,
            },
            {
              optionLabelKey: 'jobOffer.advantageSocialSocialTravelAndHolidays',
              optionValue: AdvantagesEnums.AdvantageSocial.SOCIAL_TRAVEL_AND_HOLIDAYS,
            },
            { optionLabelKey: 'jobOffer.advantageSocialSocialCafeteria', optionValue: AdvantagesEnums.AdvantageSocial.SOCIAL_CAFETERIA },
            {
              optionLabelKey: 'jobOffer.advantageSocialSocialFreshFruitAndVegetables',
              optionValue: AdvantagesEnums.AdvantageSocial.SOCIAL_FRESH_FRUIT_AND_VEGETABLES,
            },
            {
              optionLabelKey: 'jobOffer.advantageSocialSocialRelaxationRoom',
              optionValue: AdvantagesEnums.AdvantageSocial.SOCIAL_RELAXATION_ROOM,
            },
            {
              optionLabelKey: 'jobOffer.advantageSocialSocialWellnessProgram',
              optionValue: AdvantagesEnums.AdvantageSocial.SOCIAL_WELLNESS_PROGRAM,
            },
            {
              optionLabelKey: 'jobOffer.advantageSocialSocialReferencingProgram',
              optionValue: AdvantagesEnums.AdvantageSocial.SOCIAL_REFERENCING_PROGRAM,
            },
          ],
          inputErrors: undefined,
        };

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const selects: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (select: Wrapper<Vue>) =>
            // @ts-ignore
            select.vm.options.inputId === expected.inputId
        );
        // @ts-ignore
        expect(selects.at(0).vm.options).toStrictEqual(expected);
      });
      it('should display GbInputCheckboxMultiple component with input errors for job offer social advantages', () => {
        // given
        const expected: DomainFieldErrorsResponseInterface = {
          i18nMessageKeys: ['jobOffer.aSocialAdvantagesError'],
          messages: ['A social advantages error'],
        };
        propsData.options.domainErrors = { advantages: expected };

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const inputs: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (input: Wrapper<Vue>) =>
            // @ts-ignore
            input.vm.options.inputId === inputId
        );
        // @ts-ignore
        expect(inputs.at(0).vm.options.inputErrors).toStrictEqual(expected);
      });

      it('should bind job offer social advantages property to GbInputCheckboxMultiple value prop', () => {
        // given
        const advantages: PutAdvandagesRequestInterface = {
          social: [AdvantagesEnums.AdvantageSocial.SOCIAL_FRESH_FRUIT_AND_VEGETABLES, AdvantagesEnums.AdvantageSocial.SOCIAL_RELAXATION_ROOM],
          professional: [AdvantagesEnums.AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM],
        };
        propsData.value = advantages;
        // @ts-ignore
        const expected: AdvantagesEnums.AdvantageSocial[] = advantages.social;

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const inputs: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (input: Wrapper<Vue>) =>
            // @ts-ignore
            input.vm.options.inputId === inputId
        );

        // @ts-ignore
        expect(inputs.at(0).vm.value).toStrictEqual(expected);
      });
      it('should emit input event to parent when change event is triggered on select', () => {
        // given
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        const inputField: Wrapper<Vue> = gbJobOfferCreateAdvantagesWrapper.find(`#${inputId}_1`);

        // when
        // @ts-ignore
        inputField.setChecked();

        // then
        const expected: PutAdvandagesRequestInterface = {
          social: [AdvantagesEnums.AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES],
        };

        expect(gbJobOfferCreateAdvantagesWrapper.emitted().input).toStrictEqual([[expected]]);
      });
    });

    describe('life balance advantages', () => {
      const inputId: string = 'advantage-life-balance';
      it('should display GbInputCheckboxMultiple component with props for job offer life balance advantages', () => {
        // given
        const expected: object = {
          inputId,
          inputName: 'advantages-life-balance',
          inputLabelKey: 'jobOffer.advantagesLifeBalanceLabel',
          inputOptions: [
            {
              optionLabelKey: 'jobOffer.advantageLifeBalanceLifeBalanceReconcilingFamilyAndWork',
              optionValue: AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK,
            },
            {
              optionLabelKey: 'jobOffer.advantageLifeBalanceLifeBalancePaidHolidays',
              optionValue: AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_PAID_HOLIDAYS,
            },
            {
              optionLabelKey: 'jobOffer.advantageLifeBalanceLifeBalanceFlexibleSchedules',
              optionValue: AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_FLEXIBLE_SCHEDULES,
            },
            {
              optionLabelKey: 'jobOffer.advantageLifeBalanceLifeBalanceSickLeave',
              optionValue: AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_SICK_LEAVE,
            },
            {
              optionLabelKey: 'jobOffer.advantageLifeBalanceLifeBalanceSummerHours',
              optionValue: AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_SUMMER_HOURS,
            },
            {
              optionLabelKey: 'jobOffer.advantageLifeBalanceLifeBalanceMobileHolidays',
              optionValue: AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_MOBILE_HOLIDAYS,
            },
          ],
          inputErrors: undefined,
        };

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const selects: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (select: Wrapper<Vue>) =>
            // @ts-ignore
            select.vm.options.inputId === inputId
        );
        // @ts-ignore
        expect(selects.at(0).vm.options).toStrictEqual(expected);
      });

      it('should display GbInputCheckboxMultiple component with input errors for job offer life balance advantages', () => {
        // given
        const expected: DomainFieldErrorsResponseInterface = {
          i18nMessageKeys: ['jobOffer.aLifeBalanceAdvantagesError'],
          messages: ['A life balance advantages error'],
        };
        propsData.options.domainErrors = { advantages: expected };

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const inputs: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (input: Wrapper<Vue>) =>
            // @ts-ignore
            input.vm.options.inputId === inputId
        );
        // @ts-ignore
        expect(inputs.at(0).vm.options.inputErrors).toStrictEqual(expected);
      });

      it('should bind job offer lifeBalance advantages property to GbInputCheckboxMultiple value prop', () => {
        // given
        const advantages: PutAdvandagesRequestInterface = {
          lifeBalance: [
            AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_PAID_HOLIDAYS,
            AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_FLEXIBLE_SCHEDULES,
          ],
          professional: [AdvantagesEnums.AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM],
        };
        propsData.value = advantages;
        // @ts-ignore
        const expected: AdvantagesEnums.AdvantageLifeBalance[] = advantages.lifeBalance;

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const inputs: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (input: Wrapper<Vue>) =>
            // @ts-ignore
            input.vm.options.inputId === inputId
        );

        // @ts-ignore
        expect(inputs.at(0).vm.value).toStrictEqual(expected);
      });
      it('should emit input event to parent when change event is triggered on select', () => {
        // given
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        const inputField: Wrapper<Vue> = gbJobOfferCreateAdvantagesWrapper.find(`#${inputId}_1`);

        // when
        // @ts-ignore
        inputField.setChecked();

        // then
        const expected: PutAdvandagesRequestInterface = {
          lifeBalance: [AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK],
        };

        expect(gbJobOfferCreateAdvantagesWrapper.emitted().input).toStrictEqual([[expected]]);
      });
    });

    describe('financial advantages', () => {
      const inputId: string = 'advantage-financial';
      it('should display GbInputCheckboxMultiple component with props for job offer financial advantages', () => {
        // given
        const expected: object = {
          inputId,
          inputName: 'advantages-financial',
          inputLabelKey: 'jobOffer.advantagesFinancialLabel',
          inputOptions: [
            {
              optionLabelKey: 'jobOffer.advantageFinancialFinancialEmployeeDiscount',
              optionValue: AdvantagesEnums.AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT,
            },
            {
              optionLabelKey: 'jobOffer.advantageFinancialFinancialCompetitiveCompensation',
              optionValue: AdvantagesEnums.AdvantageFinancial.FINANCIAL_COMPETITIVE_COMPENSATION,
            },
            {
              optionLabelKey: 'jobOffer.advantageFinancialFinancialPensionPlan',
              optionValue: AdvantagesEnums.AdvantageFinancial.FINANCIAL_PENSION_PLAN,
            },
            {
              optionLabelKey: 'jobOffer.advantageFinancialFinancialDeferredProfitSharingPlanDpsp',
              optionValue: AdvantagesEnums.AdvantageFinancial.FINANCIAL_DEFERRED_PROFIT_SHARING_PLAN_DPSP,
            },
            { optionLabelKey: 'jobOffer.advantageFinancialFinancialEap', optionValue: AdvantagesEnums.AdvantageFinancial.FINANCIAL_EAP },
            {
              optionLabelKey: 'jobOffer.advantageFinancialFinancialRecognitionOfSeniority',
              optionValue: AdvantagesEnums.AdvantageFinancial.FINANCIAL_RECOGNITION_OF_SENIORITY,
            },
            {
              optionLabelKey: 'jobOffer.advantageFinancialFinancialRefundOfDuesToAProfessionalAssociationOrOrder',
              optionValue: AdvantagesEnums.AdvantageFinancial.FINANCIAL_REFUND_OF_DUES_TO_A_PROFESSIONAL_ASSOCIATION_OR_ORDER,
            },
            {
              optionLabelKey: 'jobOffer.advantageFinancialFinancialPerformanceBonus',
              optionValue: AdvantagesEnums.AdvantageFinancial.FINANCIAL_PERFORMANCE_BONUS,
            },
            {
              optionLabelKey: 'jobOffer.advantageFinancialFinancialParkingLot',
              optionValue: AdvantagesEnums.AdvantageFinancial.FINANCIAL_PARKING_LOT,
            },
            {
              optionLabelKey: 'jobOffer.advantageFinancialFinancialTransportSubscription',
              optionValue: AdvantagesEnums.AdvantageFinancial.FINANCIAL_TRANSPORT_SUBSCRIPTION,
            },
          ],
          inputErrors: undefined,
        };

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const selects: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (select: Wrapper<Vue>) =>
            // @ts-ignore
            select.vm.options.inputId === expected.inputId
        );
        // @ts-ignore
        expect(selects.at(0).vm.options).toStrictEqual(expected);
      });
      it('should display GbInputCheckboxMultiple component with input errors for job offer financial advantages', () => {
        // given
        const expected: DomainFieldErrorsResponseInterface = {
          i18nMessageKeys: ['jobOffer.aFinancialAdvantagesError'],
          messages: ['A financial advantages error'],
        };
        propsData.options.domainErrors = { advantages: expected };

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const inputs: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (input: Wrapper<Vue>) =>
            // @ts-ignore
            input.vm.options.inputId === inputId
        );
        // @ts-ignore
        expect(inputs.at(0).vm.options.inputErrors).toStrictEqual(expected);
      });

      it('should bind job offer lifeBalance advantages property to GbInputCheckboxMultiple value prop', () => {
        // given
        const advantages: PutAdvandagesRequestInterface = {
          financial: [
            AdvantagesEnums.AdvantageFinancial.FINANCIAL_DEFERRED_PROFIT_SHARING_PLAN_DPSP,
            AdvantagesEnums.AdvantageFinancial.FINANCIAL_EAP,
          ],
          professional: [AdvantagesEnums.AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM],
        };
        propsData.value = advantages;
        // @ts-ignore
        const expected: AdvantagesEnums.AdvantageFinancial[] = advantages.financial;

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const inputs: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (input: Wrapper<Vue>) =>
            // @ts-ignore
            input.vm.options.inputId === inputId
        );

        // @ts-ignore
        expect(inputs.at(0).vm.value).toStrictEqual(expected);
      });
      it('should emit input event to parent when change event is triggered on select', () => {
        // given
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        const inputField: Wrapper<Vue> = gbJobOfferCreateAdvantagesWrapper.find(`#${inputId}_1`);

        // when
        // @ts-ignore
        inputField.setChecked();

        // then
        const expected: PutAdvandagesRequestInterface = {
          financial: [AdvantagesEnums.AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT],
        };

        expect(gbJobOfferCreateAdvantagesWrapper.emitted().input).toStrictEqual([[expected]]);
      });
    });

    describe('professional advantages', () => {
      const inputId: string = 'advantage-professional';
      it('should display GbInputCheckboxMultiple component with props for job offer professional advantages', () => {
        // given
        const expected: object = {
          inputId,
          inputName: 'advantages-professional',
          inputLabelKey: 'jobOffer.advantagesProfessionalLabel',
          inputOptions: [
            {
              optionLabelKey: 'jobOffer.advantageProfessionalProfessionalTrainingAndDevelopmentProgram',
              optionValue: AdvantagesEnums.AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM,
            },
            {
              optionLabelKey: 'jobOffer.advantageProfessionalProfessionalRecognitionProgram',
              optionValue: AdvantagesEnums.AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM,
            },
          ],
          inputErrors: undefined,
        };

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const selects: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (select: Wrapper<Vue>) =>
            // @ts-ignore
            select.vm.options.inputId === expected.inputId
        );
        // @ts-ignore
        expect(selects.at(0).vm.options).toStrictEqual(expected);
      });
      it('should display GbInputCheckboxMultiple component with input errors for job offer professional advantages', () => {
        // given
        const expected: DomainFieldErrorsResponseInterface = {
          i18nMessageKeys: ['jAdvantageProfessionalAdvantagesError'],
          messages: ['A professional advantages error'],
        };
        propsData.options.domainErrors = { advantages: expected };

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const inputs: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (input: Wrapper<Vue>) =>
            // @ts-ignore
            input.vm.options.inputId === inputId
        );
        // @ts-ignore
        expect(inputs.at(0).vm.options.inputErrors).toStrictEqual(expected);
      });

      it('should bind job offer lifeBalance advantages property to GbInputCheckboxMultiple value prop', () => {
        // given
        const advantages: PutAdvandagesRequestInterface = {
          professional: [
            AdvantagesEnums.AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM,
            AdvantagesEnums.AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM,
          ],
          lifeBalance: [AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK],
        };
        propsData.value = advantages;
        // @ts-ignore
        const expected: AdvantagesEnums.AdvantageProfessional[] = advantages.professional;

        // when
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        // then
        const inputs: WrapperArray<Vue> = gbJobOfferCreateAdvantagesWrapper.findAll(GbInputCheckboxMultiple).filter(
          (input: Wrapper<Vue>) =>
            // @ts-ignore
            input.vm.options.inputId === inputId
        );

        // @ts-ignore
        expect(inputs.at(0).vm.value).toStrictEqual(expected);
      });
      it('should emit input event to parent when change event is triggered on select', () => {
        // given
        const gbJobOfferCreateAdvantagesWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdvantages, { stubs, mocks: { $t }, propsData });

        const inputField: Wrapper<Vue> = gbJobOfferCreateAdvantagesWrapper.find(`#${inputId}_1`);

        // when
        // @ts-ignore
        inputField.setChecked();

        // then
        const expected: PutAdvandagesRequestInterface = {
          professional: [AdvantagesEnums.AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM],
        };

        expect(gbJobOfferCreateAdvantagesWrapper.emitted().input).toStrictEqual([[expected]]);
      });
    });
  });
});

import { mount, Stubs, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBlock from '~/components/base-components/gb-block.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbInputCheckbox from '~/components/base-components/gb-input-checkbox.vue';
import GbInputDate from '~/components/base-components/gb-input-date.vue';
import { PutStartDateOfEmploymentInterface } from '../../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbJobOfferCreateStartDateOfEmployment from '../gb-job-offer-create-start-date-of-employment.vue';

describe('components/job-offer/create/gb-job-offer-create-start-date-of-employment', () => {
  let $t: jest.Mock;
  let stubs: Stubs;
  let propsData: {
    value: PutStartDateOfEmploymentInterface;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };
  let fixedDate: Date;
  // @ts-ignore
  let dateSpy: SpyInstance;

  beforeEach(() => {
    $t = jest.fn();
    stubs = { 'client-only': true, 'date-picker': true, 'v-popover': true };
    propsData = {
      value: {} as PutStartDateOfEmploymentInterface,
      options: {},
    };

    fixedDate = new Date('2017-06-13T04:41:20');
    dateSpy = jest.spyOn(global, 'Date');
    dateSpy.mockImplementation(() => fixedDate);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('label', () => {
    it('should have a GbBlock with label in header', () => {
      // given
      const expected: string = 'Start date of employment';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find(GbBlock);

      // then
      expect(result.find('.gb-block--header').text()).toStrictEqual(`${expected}*`);
      expect($t).toHaveBeenNthCalledWith(1, 'jobOffer.startDateOfEmployementLabel');
    });
  });

  describe('as soon as possible', () => {
    it('should display a GbInputCheckbox for as soon as possible with value', () => {
      // given
      propsData.value.asSoonAsPossible = true;
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find(GbInputCheckbox);

      // then
      // @ts-ignore
      expect(result.vm.value).toBe(true);
    });
    it('should display a GbInputCheckbox for as soon as possible with options', () => {
      // given
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find(GbInputCheckbox);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({
        inputId: 'as-soon-as-possible',
        inputLabelKey: 'jobOffer.startDateOfEmploymentAsSoonAsPossibleLabel',
        inputName: 'asSoonAsPossible',
      });
    });
    it('should display a GbInputCheckbox for as soon as possible with true value from prop', () => {
      // given
      propsData.value = {
        asSoonAsPossible: true,
      };
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find(GbInputCheckbox);

      // then
      // @ts-ignore
      expect(result.vm.value).toBe(true);
    });
    it('should display a GbInputCheckbox for as soon as possible with false value if no value prop', () => {
      // given
      propsData.value = (undefined as unknown) as PutStartDateOfEmploymentInterface;
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find(GbInputCheckbox);

      // then
      // @ts-ignore
      expect(result.vm.value).toStrictEqual(false);
    });
    it('should display a GbInputCheckbox for as soon as possible with false value if not in prop', () => {
      // given
      propsData.value = {};
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find(GbInputCheckbox);

      // then
      // @ts-ignore
      expect(result.vm.value).toStrictEqual(false);
    });
    it('should add a disabled css class to start date', () => {
      // given
      propsData.value = {
        asSoonAsPossible: true,
      };
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find('.gb-job-offer-create-start-date-of-employment--content');

      // then
      expect(result.element.classList.contains('disabled')).toBe(true);
    });
    it('should emit input event to parent when change event is triggered on input with cleaned values for other fields when it is true', () => {
      // given
      propsData.value = {
        asSoonAsPossible: false,
        startDate: new Date(),
      };
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      const inputCheckboxField: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find('#as-soon-as-possible');

      // when
      inputCheckboxField.trigger('click');

      // then
      const expected: PutStartDateOfEmploymentInterface = {
        asSoonAsPossible: true,
      };

      expect(gbJobOfferCreateStartDateOfEmploymentWrapper.emitted().input).toStrictEqual([[expected]]);
    });

    it('should emit input event to parent when change event is triggered on input with other fields when it is false', () => {
      // given
      propsData.value = {
        asSoonAsPossible: true,
        startDate: fixedDate,
      };
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      const inputCheckboxField: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find('#as-soon-as-possible');

      // when
      inputCheckboxField.trigger('click');

      // then
      const expected: PutStartDateOfEmploymentInterface = {
        asSoonAsPossible: false,
        startDate: fixedDate,
      };

      expect(gbJobOfferCreateStartDateOfEmploymentWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('start date of employment', () => {
    it('should contain a GbIcon with options', () => {
      // given
      const expected: object = { iconName: 'calendar_blue' };
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // when
      const result: WrapperArray<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.findAll(GbIcon);

      // then
      // @ts-ignore
      expect(result.at(0).vm.options).toStrictEqual(expected);
    });
    it('should display GbInputDate component with props for job offer start date of employment', () => {
      // given
      const expected: object = {
        inputId: 'start-date-of-employment',
        inputMin: fixedDate,
        inputPattern: 'yyyy-MM-dd',
        inputPlaceholderKey: 'jobOffer.startDateOfEmploymentPlaceholder',
        inputRequired: false,
        inputIsLarge: true,
        inputErrors: undefined,
      };

      // when
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // then
      const inputDate: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find(GbInputDate);

      // @ts-ignore
      expect(inputDate.vm.options).toStrictEqual(expected);
    });

    it('should set inputErrors when there is a domain errors for this field', async () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        messages: ['A start date of employment error'],
        i18nMessageKeys: ['jobOffer.aStartDateOfEmploymentError'],
      };
      propsData.options = { domainErrors: { startDate: expected } };
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // then
      const inputDate: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find(GbInputDate);

      // @ts-ignore
      expect(inputDate.vm.options.inputErrors).toStrictEqual(expected);
    });

    it('should bind job offer start date of employment to GbInputDate input', () => {
      // given
      const startDateOfEmployement: Date = new Date('2019-10-23T00:00:00.000Z');
      propsData.value = {
        startDate: startDateOfEmployement,
      };

      // when
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // then
      const inputDate: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find(GbInputDate);

      // @ts-ignore
      expect(inputDate.vm.value).toStrictEqual(startDateOfEmployement);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const startDateOfEmployement: Date = new Date('2019-10-23T00:00:00.000Z');
      propsData.value = {
        startDate: startDateOfEmployement,
      };
      const gbJobOfferCreateStartDateOfEmploymentWrapper: Wrapper<Vue> = mount(GbJobOfferCreateStartDateOfEmployment, {
        stubs,
        mocks: { $t },
        propsData,
      });

      const inputField: Wrapper<Vue> = gbJobOfferCreateStartDateOfEmploymentWrapper.find('date-picker-stub');

      // when
      inputField.vm.$emit('input', startDateOfEmployement);

      // then
      const inputExpected: PutStartDateOfEmploymentInterface = {
        asSoonAsPossible: false,
        startDate: startDateOfEmployement,
      };

      expect(gbJobOfferCreateStartDateOfEmploymentWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });
});

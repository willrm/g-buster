import { mount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbTextarea from '~/components/base-components/gb-textarea.vue';
import { JobOfferEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { PutJobOfferRequestInterface } from '../../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbJobOfferCreateDescriptionAndSpecificities from '../gb-job-offer-create-description-and-specificities.vue';

describe('components/job-offer/create/gb-job-offer-create-description-and-specificities', () => {
  let $t: jest.Mock;
  let propsData: {
    jobOffer: PutJobOfferRequestInterface;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffer: ({
        targetCustomers: [] as JobOfferEnums.JobOfferTargetCustomer[],
        requiredExperience: [] as JobOfferEnums.JobOfferRequiredExperience[],
        requestedSpecialities: [] as JobOfferEnums.JobOfferRequestedSpeciality[],
      } as unknown) as PutJobOfferRequestInterface,
      options: {},
    };
  });

  describe('description', () => {
    it('should display GbTextarea component with props for job offer description', () => {
      // given
      const expected: object = {
        textareaId: 'description',
        textareaLabelKey: 'jobOffer.descriptionLabelKey',
        textareaRequired: true,
        textareaMaxLength: 32000,
        textareaIsLarge: true,
        textareaErrors: undefined,
      };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateDescriptionAndSpecificities, { mocks: { $t }, propsData });

      // then
      const textareas: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbTextarea).filter(
        (textarea: Wrapper<Vue>) =>
          // @ts-ignore
          textarea.vm.options.textareaId === expected.textareaId
      );
      // @ts-ignore
      expect(textareas.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbTextarea component with textarea errors for job offer description', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aDescriptionError'],
        messages: ['A description error'],
      };
      propsData.options.domainErrors = { description: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateDescriptionAndSpecificities, { mocks: { $t }, propsData });

      // then
      const textareas: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbTextarea).filter(
        (textarea: Wrapper<Vue>) =>
          // @ts-ignore
          textarea.vm.options.textareaId === 'description'
      );
      // @ts-ignore
      expect(textareas.at(0).vm.options.textareaErrors).toStrictEqual(expected);
    });

    it('should bind job offer description property to GbTextarea input prop', () => {
      // given
      const description: string = 'job offer description';
      propsData.jobOffer.description = description;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateDescriptionAndSpecificities, { mocks: { $t }, propsData });

      // then
      const textareas: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbTextarea).filter(
        (textarea: Wrapper<Vue>) =>
          // @ts-ignore
          textarea.vm.value === description
      );
      expect(textareas).toHaveLength(1);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateDescriptionAndSpecificities, { mocks: { $t }, propsData });

      const textareaField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#description');
      const textareaValue: string = 'job offer description';

      // when
      (textareaField.element as HTMLTextAreaElement).value = textareaValue;
      textareaField.trigger('input');

      // then
      const expected: { fieldName: string; fieldValue: string } = {
        fieldName: 'description',
        fieldValue: 'job offer description',
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('specificities', () => {
    it('should display GbTextarea component with props for job offer specificities', () => {
      // given
      const expected: object = {
        textareaId: 'specificities',
        textareaLabelKey: 'jobOffer.specificitiesLabelKey',
        textareaPlaceholderKey: 'jobOffer.specificitiesPlaceholder',
        textareaMaxLength: 3000,
        textareaIsLarge: true,
        textareaErrors: undefined,
      };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateDescriptionAndSpecificities, { mocks: { $t }, propsData });

      // then
      const textareas: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbTextarea).filter(
        (textarea: Wrapper<Vue>) =>
          // @ts-ignore
          textarea.vm.options.textareaId === expected.textareaId
      );
      // @ts-ignore
      expect(textareas.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbTextarea component with textarea errors for job offer specificities', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aSpecificitiesError'],
        messages: ['A specificities error'],
      };
      propsData.options.domainErrors = { specificities: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateDescriptionAndSpecificities, { mocks: { $t }, propsData });

      // then
      const textareas: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbTextarea).filter(
        (textarea: Wrapper<Vue>) =>
          // @ts-ignore
          textarea.vm.options.textareaId === 'specificities'
      );
      // @ts-ignore
      expect(textareas.at(0).vm.options.textareaErrors).toStrictEqual(expected);
    });

    it('should bind job offer specificities property to GbTextarea input prop', () => {
      // given
      const specificities: string = 'job offer specificities';
      propsData.jobOffer.specificities = specificities;

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateDescriptionAndSpecificities, { mocks: { $t }, propsData });

      // then
      const textareas: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbTextarea).filter(
        (textarea: Wrapper<Vue>) =>
          // @ts-ignore
          textarea.vm.value === specificities
      );
      expect(textareas).toHaveLength(1);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateDescriptionAndSpecificities, { mocks: { $t }, propsData });

      const textareaField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#specificities');
      const textareaValue: string = 'job offer specificities';

      // when
      (textareaField.element as HTMLTextAreaElement).value = textareaValue;
      textareaField.trigger('input');

      // then
      const expected: { fieldName: string; fieldValue: string } = {
        fieldName: 'specificities',
        fieldValue: 'job offer specificities',
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });
});

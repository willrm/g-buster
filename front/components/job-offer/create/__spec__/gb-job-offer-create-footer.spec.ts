import { mount, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import { JobOfferEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import {
  PutAdvandagesRequestInterface,
  PutAnnualSalaryRangeInterface,
  PutJobOfferRequestInterface,
  PutStartDateOfEmploymentInterface,
  PutTravelingRequestInterface,
} from '../../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import { DomainErrorsResponseInterface } from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbJobOfferCreateFooter from '../gb-job-offer-create-footer.vue';
import GbJobOfferCreateInternalReferenceNumber from '../gb-job-offer-create-internal-reference-number.vue';

describe('components/job-offer/create/gb-job-offer-create-header', () => {
  let $t: jest.Mock;
  let propsData: {
    jobOffer: PutJobOfferRequestInterface;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffer: {
        annualSalaryRange: {} as PutAnnualSalaryRangeInterface,
        emailAddressForReceiptOfApplications: '',
        endDateOfApplication: new Date(),
        internalReferenceNumber: '',
        specificities: '',
        startDateOfEmployment: {} as PutStartDateOfEmploymentInterface,
        wantedPersonalities: [],
        city: '',
        description: '',
        jobStatus: JobOfferEnums.JobOfferJobStatus.JOB_STATUS_FULL_TIME,
        jobType: JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP,
        positionType: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_ADVISOR,
        region: JobOfferEnums.JobOfferRegion.REGION_ABITIBI_TEMISCAMINGUE,
        requiredExperiences: [] as JobOfferEnums.JobOfferRequiredExperience[],
        title: '',
        targetCustomers: [] as JobOfferEnums.JobOfferTargetCustomer[],
        requiredExperience: [] as JobOfferEnums.JobOfferRequiredExperience[],
        requestedSpecialities: [] as JobOfferEnums.JobOfferRequestedSpeciality[],
        advantages: {} as PutAdvandagesRequestInterface,
        traveling: {} as PutTravelingRequestInterface,
      } as PutJobOfferRequestInterface,
      options: {},
    };
  });

  describe('internal reference number', () => {
    it('should bind jobOffer prop to GbJobOfferCreateInternalReferenceNumber prop', () => {
      // given
      propsData.jobOffer.internalReferenceNumber = 'AB_12345-D';

      // when
      const gbJobOfferCreateFooterWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateFooter, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateFooterWrapper.find(GbJobOfferCreateInternalReferenceNumber).vm.value).toStrictEqual('AB_12345-D');
    });

    it('should set GbJobOfferCreateInternalReferenceNumber options prop', () => {
      // given
      const expected: {} = {
        domainErrors: {},
      };

      // when
      const gbJobOfferCreateFooterWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateFooter, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateFooterWrapper.find(GbJobOfferCreateInternalReferenceNumber).vm.options).toStrictEqual(expected);
    });

    it('should set GbJobOfferCreateInternalReferenceNumber options prop when domainErrors is defined', () => {
      // given
      const expected: DomainErrorsResponseInterface = {
        traveling: {
          i18nMessageKeys: ['jobOffer.anEmailAddressForReceiptOfApplicationsError'],
          messages: ['An internal reference number range error'],
        },
      };
      propsData.options.domainErrors = expected;

      // when
      const gbJobOfferCreateFooterWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateFooter, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateFooterWrapper.find(GbJobOfferCreateFooter).vm.options).toStrictEqual({
        domainErrors: expected,
      });
    });

    it('should emit input event when input is triggered from child', () => {
      // given
      const gbJobOfferCreateFooterWrapper: Wrapper<Vue> = mount(GbJobOfferCreateFooter, { mocks: { $t }, propsData });

      const gbJobOfferCreateInternalReferenceNumberWrapper: Wrapper<Vue> = gbJobOfferCreateFooterWrapper.find(
        GbJobOfferCreateInternalReferenceNumber
      );
      const inputExpected: { fieldName: string; fieldValue: string } = {
        fieldName: 'internalReferenceNumber',
        fieldValue: 'AB_12345-D',
      };

      // when
      gbJobOfferCreateInternalReferenceNumberWrapper.vm.$emit('input', 'AB_12345-D');

      // then
      expect(gbJobOfferCreateFooterWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });
});

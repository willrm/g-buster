import { mount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBlock from '~/components/base-components/gb-block.vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbInputNumber from '~/components/base-components/gb-input-number.vue';
import { AnnualSalaryRangeConstraints } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { PutAnnualSalaryRangeInterface } from '../../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbJobOfferCreateAnnualSalaryRange from '../gb-job-offer-create-annual-salary-range.vue';

describe('components/job-offer/create/gb-job-offer-create-annual-salary-range', () => {
  let $t: jest.Mock;
  let propsData: {
    value: PutAnnualSalaryRangeInterface;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: {} as PutAnnualSalaryRangeInterface,
      options: {},
    };
  });

  describe('label', () => {
    it('should have a GbBlock with label in header', () => {
      // given
      const expected: string = 'Annual salary range';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.find(GbBlock);

      // then
      expect(result.find('.gb-block--header').text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(1, 'jobOffer.annualSalaryRangeLabel');
    });
  });

  describe('minimum salary', () => {
    it('should have a label for minimum', () => {
      // given
      const expected: string = 'Minimum';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.findAll('.gb-offer-create-annual-salary-range--label');

      // then
      expect(result.at(0).text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(2, 'jobOffer.annualSalaryRangeMinimumLabel');
    });
    it('should contain a GbElementWithIcon with options', () => {
      // given
      const expected: object = { inputWithIconPosition: 'INSIDE', inputWithIconIsLarge: true };
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.findAll(GbElementWithIcon);

      // then
      // @ts-ignore
      expect(result.at(0).vm.options).toStrictEqual(expected);
    });

    it('should contain a GbIcon with options', () => {
      // given
      const expected: object = { iconName: 'dollar_grey' };
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.findAll(GbIcon);

      // then
      // @ts-ignore
      expect(result.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputNumber component with props for job offer minimum salary', () => {
      // given
      const expected: object = {
        inputId: 'annual-salary-range-minimum',
        inputRequired: false,
        inputMin: AnnualSalaryRangeConstraints.ANNUAL_SALARY_RANGE_MINIMUM_SALARY,
        inputIsLarge: true,
        inputErrors: undefined,
      };

      // when
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // then
      const inputNumbers: WrapperArray<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.findAll(GbInputNumber).filter(
        (inputNumber: Wrapper<Vue>) =>
          // @ts-ignore
          inputNumber.vm.options.inputId === expected.inputId
      );
      // @ts-ignore
      expect(inputNumbers.at(0).vm.options).toStrictEqual(expected);
    });

    it('should bind job offer annual salary range minimum property to GbInputNumber input', () => {
      // given
      const minimumSalary: number = 12345;
      propsData.value.minimumSalary = minimumSalary;

      // when
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // then
      const inputNumbers: WrapperArray<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.findAll(GbInputNumber).filter(
        (inputNumber: Wrapper<Vue>) =>
          // @ts-ignore
          inputNumber.vm.value === minimumSalary
      );
      expect(inputNumbers).toHaveLength(1);
    });
    it('should set inputErrors when there is a domain errors for this field', async () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        messages: ['A minimum annual salary range error'],
        i18nMessageKeys: ['jobOffer.aMinimumAnnualSalaryRangeError'],
      };
      propsData.options = { domainErrors: { minimumSalary: expected } };
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // then
      const inputNumbers: WrapperArray<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.findAll(GbInputNumber).filter(
        (inputNumber: Wrapper<Vue>) =>
          // @ts-ignore
          inputNumber.vm.options.inputId === 'annual-salary-range-minimum'
      );

      // @ts-ignore
      expect(inputNumbers.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      propsData.value = { maximumSalary: 1234567 };
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      const inputField: Wrapper<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.find('#annual-salary-range-minimum');
      const inputValue: string = '12345';

      // when
      (inputField.element as HTMLInputElement).value = inputValue;
      inputField.trigger('input');

      // then
      const inputExpected: PutAnnualSalaryRangeInterface = {
        maximumSalary: propsData.value.maximumSalary,
        minimumSalary: parseInt(inputValue, 10),
      };

      expect(gbJobOfferCreateAnnualSalaryRangeWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });

  describe('maximum salary', () => {
    it('should have a label for minimum', () => {
      // given
      const expected: string = 'Maximum';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.findAll('.gb-offer-create-annual-salary-range--label');

      // then
      expect(result.at(0).text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(3, 'jobOffer.annualSalaryRangeMaximumLabel');
    });
    it('should contain a GbElementWithIcon with options', () => {
      // given
      const expected: object = { inputWithIconPosition: 'INSIDE', inputWithIconIsLarge: true };
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.findAll(GbElementWithIcon);

      // then
      // @ts-ignore
      expect(result.at(0).vm.options).toStrictEqual(expected);
    });

    it('should contain a GbIcon with options', () => {
      // given
      const expected: object = { iconName: 'dollar_grey' };
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.findAll(GbIcon);

      // then
      // @ts-ignore
      expect(result.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputNumber component with props for job offer minimum salary', () => {
      // given
      const expected: object = {
        inputId: 'annual-salary-range-maximum',
        inputRequired: false,
        inputMax: AnnualSalaryRangeConstraints.ANNUAL_SALARY_RANGE_MAXIMUM_SALARY,
        inputIsLarge: true,
        inputErrors: undefined,
      };

      // when
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // then
      const inputNumbers: WrapperArray<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.findAll(GbInputNumber).filter(
        (inputNumber: Wrapper<Vue>) =>
          // @ts-ignore
          inputNumber.vm.options.inputId === expected.inputId
      );
      // @ts-ignore
      expect(inputNumbers.at(0).vm.options).toStrictEqual(expected);
    });

    it('should bind job offer annual salary range maximum property to GbInputNumber input', () => {
      // given
      const maximumSalary: number = 12345;
      propsData.value.maximumSalary = maximumSalary;

      // when
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // then
      const inputNumbers: WrapperArray<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.findAll(GbInputNumber).filter(
        (inputNumber: Wrapper<Vue>) =>
          // @ts-ignore
          inputNumber.vm.value === maximumSalary
      );
      expect(inputNumbers).toHaveLength(1);
    });
    it('should set inputErrors when there is a domain errors for this field', async () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        messages: ['A maximum annual salary range error'],
        i18nMessageKeys: ['jobOffer.aMaximumAnnualSalaryRangeError'],
      };
      propsData.options = { domainErrors: { maximumSalary: expected } };
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      // then
      const inputNumbers: WrapperArray<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.findAll(GbInputNumber).filter(
        (inputNumber: Wrapper<Vue>) =>
          // @ts-ignore
          inputNumber.vm.options.inputId === 'annual-salary-range-maximum'
      );

      // @ts-ignore
      expect(inputNumbers.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      propsData.value = { minimumSalary: 12345 };
      const gbJobOfferCreateAnnualSalaryRangeWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAnnualSalaryRange, { mocks: { $t }, propsData });

      const inputField: Wrapper<Vue> = gbJobOfferCreateAnnualSalaryRangeWrapper.find('#annual-salary-range-maximum');
      const inputValue: string = '1234567';

      // when
      (inputField.element as HTMLInputElement).value = inputValue;
      inputField.trigger('input');

      // then
      const inputExpected: PutAnnualSalaryRangeInterface = {
        maximumSalary: parseInt(inputValue, 10),
        minimumSalary: propsData.value.minimumSalary,
      };

      expect(gbJobOfferCreateAnnualSalaryRangeWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });
});

import { mount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbInputRadio from '~/components/base-components/gb-input-radio.vue';
import GbInputText from '~/components/base-components/gb-input-text.vue';
import GbSelectMultiple from '~/components/base-components/gb-select-multiple.vue';
import GbSelect from '~/components/base-components/gb-select.vue';
import { JobOfferEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { PutJobOfferRequestInterface } from '../../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbJobOfferCreateHeader from '../gb-job-offer-create-header.vue';

describe('components/job-offer/create/gb-job-offer-create-header', () => {
  let $t: jest.Mock;
  let propsData: {
    jobOffer: PutJobOfferRequestInterface;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffer: {
        targetCustomers: [] as JobOfferEnums.JobOfferTargetCustomer[],
        requiredExperiences: [] as JobOfferEnums.JobOfferRequiredExperience[],
        requestedSpecialities: [] as JobOfferEnums.JobOfferRequestedSpeciality[],
      } as PutJobOfferRequestInterface,
      options: {
        domainErrors: {} as DomainErrorsResponseInterface,
      },
    };
  });
  describe('title', () => {
    it('should display GbInputText component with props for job offer title', () => {
      // given
      const expected: object = {
        inputId: 'title',
        inputPlaceholderKey: 'jobOffer.titlePlaceholder',
        inputRequired: true,
        inputMaxLength: 150,
        inputIsLarge: true,
        inputErrors: undefined,
      };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.options.inputId === expected.inputId
      );
      // @ts-ignore
      expect(inputTexts.at(0).vm.options).toStrictEqual(expected);
    });
    it('should bind jobOffer title property to input prop', () => {
      // given
      const title: string = 'A job offer title';
      propsData.jobOffer.title = title;

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.value === title
      );
      expect(inputTexts).toHaveLength(1);
      // @ts-ignore
      expect(inputTexts.at(0).vm.value).toBe(title);
    });
    it('should display GbInputText component with input errors for jobOffer title', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aTitleError'],
        messages: ['A title error'],
      };
      propsData.options.domainErrors = { title: expected };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.options.inputId === 'title'
      );
      // @ts-ignore
      expect(inputTexts.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });
    it('should not display GbInputText component with input errors for jobOffer title', () => {
      // given
      propsData.options = {};

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { stubs: { 'client-only': true }, mocks: { $t }, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.options.inputId === 'title'
      );
      // @ts-ignore
      expect(inputTexts.at(0).vm.options.inputErrors).toBeUndefined();
    });
    it('should emit input event when GbInputText input event is triggered', () => {
      // given
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { stubs: { 'client-only': true }, mocks: { $t }, propsData });

      const inputField: Wrapper<Vue> = gbJobOfferCreateHeaderWrapper.find('#title');
      const inputValue: string = 'A job offer title';

      // when
      (inputField.element as HTMLInputElement).value = inputValue;
      inputField.trigger('input');

      // then
      const expected: { fieldName: string; fieldValue: string } = {
        fieldName: 'title',
        fieldValue: inputValue,
      };

      expect(gbJobOfferCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('jobStatus', () => {
    it('should display GbInputRadio with props for job status', () => {
      // given
      const expected: object = {
        inputId: 'job-status',
        inputName: 'job-status-group',
        inputLabelKey: 'jobOffer.jobStatusLabel',
        inputOptions: [
          { optionLabelKey: 'jobOffer.jobStatusFullTime', optionValue: JobOfferEnums.JobOfferJobStatus.JOB_STATUS_FULL_TIME },
          { optionLabelKey: 'jobOffer.jobStatusPartTime', optionValue: JobOfferEnums.JobOfferJobStatus.JOB_STATUS_PART_TIME },
        ],
        inputRequired: true,
        inputErrors: undefined,
      };

      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // when
      const inputs: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputRadio).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.inputId === expected.inputId
      );

      // then
      // @ts-ignore
      expect(inputs.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputRadio component with select errors', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aJobStatusError'],
        messages: ['A job status error'],
      };
      propsData.options.domainErrors = { jobStatus: expected };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const inputs: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputRadio).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.inputId === 'job-status'
      );
      // @ts-ignore
      expect(inputs.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });

    it('should bind jobOffer jobStatus property input prop', () => {
      // given
      const jobStatus: JobOfferEnums.JobOfferJobStatus = JobOfferEnums.JobOfferJobStatus.JOB_STATUS_PART_TIME;
      propsData.jobOffer.jobStatus = jobStatus;

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const inputs: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputRadio).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.value === jobStatus
      );
      expect(inputs).toHaveLength(1);
    });

    it('should emit input event to parent when change event is triggered on input', () => {
      // given
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      const inputRadioField: Wrapper<Vue> = gbJobOfferCreateHeaderWrapper.find('#job-status_1');
      const inputValue: JobOfferEnums.JobOfferJobStatus = JobOfferEnums.JobOfferJobStatus.JOB_STATUS_FULL_TIME;

      // when
      inputRadioField.trigger('change');

      // then
      const expected: { fieldName: string; fieldValue: JobOfferEnums.JobOfferJobStatus } = {
        fieldName: 'jobStatus',
        fieldValue: inputValue,
      };

      expect(gbJobOfferCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('jobType', () => {
    it('should display GbInputRadio with props for job type', () => {
      // given
      const expected: object = {
        inputId: 'job-type',
        inputName: 'job-type-group',
        inputLabelKey: 'jobOffer.jobTypeLabel',
        inputOptions: [
          { optionLabelKey: 'jobOffer.jobTypeRegular', optionValue: JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR },
          { optionLabelKey: 'jobOffer.jobTypeInternship', optionValue: JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP },
          { optionLabelKey: 'jobOffer.jobTypeTemporary', optionValue: JobOfferEnums.JobOfferJobType.JOB_TYPE_TEMPORARY },
        ],
        inputRequired: true,
        inputErrors: undefined,
      };

      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // when
      const inputs: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputRadio).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.inputId === expected.inputId
      );

      // then
      // @ts-ignore
      expect(inputs.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputRadio component with select errors', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aJobTypeError'],
        messages: ['A job type error'],
      };
      propsData.options.domainErrors = { jobType: expected };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const inputs: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputRadio).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.inputId === 'job-type'
      );
      // @ts-ignore
      expect(inputs.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });

    it('should bind jobOffer jobType property input prop', () => {
      // given
      const jobType: JobOfferEnums.JobOfferJobType = JobOfferEnums.JobOfferJobType.JOB_TYPE_TEMPORARY;
      propsData.jobOffer.jobType = jobType;

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const inputs: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputRadio).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.value === jobType
      );
      expect(inputs).toHaveLength(1);
    });

    it('should emit input event to parent when change event is triggered on input', () => {
      // given
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      const inputRadioField: Wrapper<Vue> = gbJobOfferCreateHeaderWrapper.find('#job-type_1');
      const inputValue: JobOfferEnums.JobOfferJobType = JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR;

      // when
      inputRadioField.trigger('change');

      // then
      const expected: { fieldName: string; fieldValue: JobOfferEnums.JobOfferJobType } = {
        fieldName: 'jobType',
        fieldValue: inputValue,
      };

      expect(gbJobOfferCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('city', () => {
    it('should display GbIcon component with props for job offer city', () => {
      // given
      const expected: object = { iconName: 'address_blue' };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputText component with props for job offer city', () => {
      // given
      const expected: object = {
        inputId: 'city',
        inputPlaceholderKey: 'jobOffer.cityPlaceholder',
        inputRequired: true,
        inputMaxLength: 100,
        inputIsLarge: true,
        inputErrors: undefined,
      };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.options.inputId === expected.inputId
      );
      // @ts-ignore
      expect(inputTexts.at(0).vm.options).toStrictEqual(expected);
    });
    it('should bind jobOffer city property to input prop', () => {
      // given
      const city: string = 'A job offer city';
      propsData.jobOffer.city = city;

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.value === city
      );
      expect(inputTexts).toHaveLength(1);
      // @ts-ignore
      expect(inputTexts.at(0).vm.value).toBe(city);
    });
    it('should display GbInputText component with input errors for jobOffer city', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aCityError'],
        messages: ['A city error'],
      };
      propsData.options.domainErrors = { city: expected };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.options.inputId === 'city'
      );
      // @ts-ignore
      expect(inputTexts.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });
    it('should not display GbInputText component with input errors for jobOffer city', () => {
      // given
      propsData.options = {};

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const inputTexts: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbInputText).filter(
        (inputText: Wrapper<Vue>) =>
          // @ts-ignore
          inputText.vm.options.inputId === 'city'
      );
      // @ts-ignore
      expect(inputTexts.at(0).vm.options.inputErrors).toBeUndefined();
    });
    it('should emit input event when GbInputText input event is triggered', () => {
      // given
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      const inputField: Wrapper<Vue> = gbJobOfferCreateHeaderWrapper.find('#city');
      const inputValue: string = 'A job offer city';

      // when
      (inputField.element as HTMLInputElement).value = inputValue;
      inputField.trigger('input');

      // then
      const expected: { fieldName: string; fieldValue: string } = {
        fieldName: 'city',
        fieldValue: inputValue,
      };

      expect(gbJobOfferCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('region', () => {
    it('should display GbSelect component with props for job offer regions', () => {
      // given
      const expected: object = {
        selectId: 'region',
        selectPlaceholderKey: 'jobOffer.regionPlaceholder',
        selectRequired: true,
        selectOptions: [
          { optionLabelKey: 'jobOffer.regionAbitibiTemiscamingue', optionValue: JobOfferEnums.JobOfferRegion.REGION_ABITIBI_TEMISCAMINGUE },
          { optionLabelKey: 'jobOffer.regionBasStLaurent', optionValue: JobOfferEnums.JobOfferRegion.REGION_BAS_ST_LAURENT },
          { optionLabelKey: 'jobOffer.regionCapitaleNationale', optionValue: JobOfferEnums.JobOfferRegion.REGION_CAPITALE_NATIONALE },
          { optionLabelKey: 'jobOffer.regionCentreDuQuebec', optionValue: JobOfferEnums.JobOfferRegion.REGION_CENTRE_DU_QUEBEC },
          { optionLabelKey: 'jobOffer.regionChaudieresAppalaches', optionValue: JobOfferEnums.JobOfferRegion.REGION_CHAUDIERES_APPALACHES },
          { optionLabelKey: 'jobOffer.regionCoteNord', optionValue: JobOfferEnums.JobOfferRegion.REGION_COTE_NORD },
          { optionLabelKey: 'jobOffer.regionEstrie', optionValue: JobOfferEnums.JobOfferRegion.REGION_ESTRIE },
          {
            optionLabelKey: 'jobOffer.regionGaspesieIlesDeLaMadeleine',
            optionValue: JobOfferEnums.JobOfferRegion.REGION_GASPESIE_ILES_DE_LA_MADELEINE,
          },
          { optionLabelKey: 'jobOffer.regionLanaudiere', optionValue: JobOfferEnums.JobOfferRegion.REGION_LANAUDIERE },
          { optionLabelKey: 'jobOffer.regionLaurentides', optionValue: JobOfferEnums.JobOfferRegion.REGION_LAURENTIDES },
          { optionLabelKey: 'jobOffer.regionLaval', optionValue: JobOfferEnums.JobOfferRegion.REGION_LAVAL },
          { optionLabelKey: 'jobOffer.regionMauricie', optionValue: JobOfferEnums.JobOfferRegion.REGION_MAURICIE },
          { optionLabelKey: 'jobOffer.regionMonteregie', optionValue: JobOfferEnums.JobOfferRegion.REGION_MONTEREGIE },
          { optionLabelKey: 'jobOffer.regionMontreal', optionValue: JobOfferEnums.JobOfferRegion.REGION_MONTREAL },
          { optionLabelKey: 'jobOffer.regionNordDuQuebec', optionValue: JobOfferEnums.JobOfferRegion.REGION_NORD_DU_QUEBEC },
          { optionLabelKey: 'jobOffer.regionOutaouais', optionValue: JobOfferEnums.JobOfferRegion.REGION_OUTAOUAIS },
          { optionLabelKey: 'jobOffer.regionSaguenayLacStJean', optionValue: JobOfferEnums.JobOfferRegion.REGION_SAGUENAY_LAC_ST_JEAN },
        ],
        selectIsLarge: true,
        selectErrors: undefined,
      };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === expected.selectId
      );
      // @ts-ignore
      expect(selects.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelect component with select errors for job offer regions', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aRegionError'],
        messages: ['A region error'],
      };
      propsData.options.domainErrors = { region: expected };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === 'region'
      );
      // @ts-ignore
      expect(selects.at(0).vm.options.selectErrors).toStrictEqual(expected);
    });

    it('should bind job offer region property to GbSelect input prop', () => {
      // given
      const region: JobOfferEnums.JobOfferRegion = JobOfferEnums.JobOfferRegion.REGION_ESTRIE;
      propsData.jobOffer.region = region;

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.value === region
      );
      expect(selects).toHaveLength(1);
    });
    it('should emit input event to parent when change event is triggered on select', () => {
      // given
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      const selectField: Wrapper<Vue> = gbJobOfferCreateHeaderWrapper.find('#region');
      const selectValue: JobOfferEnums.JobOfferRegion = JobOfferEnums.JobOfferRegion.REGION_COTE_NORD;

      // when
      (selectField.find(`option[value="${selectValue}"]`).element as HTMLOptionElement).selected = true;
      selectField.trigger('change');

      // then
      const expected: { fieldName: string; fieldValue: JobOfferEnums.JobOfferRegion } = {
        fieldName: 'region',
        fieldValue: selectValue,
      };

      expect(gbJobOfferCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('targetCustomers', () => {
    it('should display GbIcon component with props for job offer target customers', () => {
      // given
      const expected: object = { iconName: 'target_blue' };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelectMultiple component with props for job offer targetCustomerss', () => {
      // given
      const expected: object = {
        selectId: 'target-customers',
        selectPlaceholderKey: 'jobOffer.targetCustomersPlaceholder',
        selectRequired: true,
        selectOptions: [
          { optionLabelKey: 'jobOffer.targetCustomerEngineers', optionValue: JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS },
          {
            optionLabelKey: 'jobOffer.targetCustomerJuniorEngineersCep',
            optionValue: JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP,
          },
          {
            optionLabelKey: 'jobOffer.targetCustomerEngineeringGraduates',
            optionValue: JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERING_GRADUATES,
          },
          {
            optionLabelKey: 'jobOffer.targetCustomerStudentsAndGraduates',
            optionValue: JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_STUDENTS_AND_GRADUATES,
          },
        ],
        selectErrors: undefined,
      };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelectMultiple).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === expected.selectId
      );
      // @ts-ignore
      expect(selects.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelectMultiple component with select errors for job offer targetCustomerss', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aRegionError'],
        messages: ['A targetCustomers error'],
      };
      propsData.options.domainErrors = { targetCustomers: expected };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelectMultiple).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === 'target-customers'
      );
      // @ts-ignore
      expect(selects.at(0).vm.options.selectErrors).toStrictEqual(expected);
    });

    it('should bind job offer targetCustomers property to GbSelectMultiple input prop', () => {
      // given
      const targetCustomers: JobOfferEnums.JobOfferTargetCustomer[] = [
        JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS,
        JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP,
      ];
      propsData.jobOffer.targetCustomers = targetCustomers;

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelectMultiple).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.value === targetCustomers
      );
      expect(selects).toHaveLength(1);
    });

    it('should emit input event to parent when change event is triggered on select', () => {
      // given
      const selectValues: JobOfferEnums.JobOfferTargetCustomer[] = [
        JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS,
        JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERING_GRADUATES,
      ];
      propsData.jobOffer.targetCustomers = selectValues;

      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });
      const targetCustomersGbSelectMultiple: Wrapper<Vue> = gbJobOfferCreateHeaderWrapper
        .findAll(GbSelectMultiple)
        .filter(
          (select: Wrapper<Vue>) =>
            // @ts-ignore
            select.vm.value === propsData.jobOffer.targetCustomers
        )
        .at(0);

      // when
      targetCustomersGbSelectMultiple.vm.$emit('input', selectValues);

      // then
      const expected: { fieldName: string; fieldValue: JobOfferEnums.JobOfferTargetCustomer[] } = {
        fieldName: 'targetCustomers',
        fieldValue: selectValues,
      };
      expect(gbJobOfferCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('requiredExperiences', () => {
    it('should display GbIcon component with props for job offer target customers', () => {
      // given
      const expected: object = { iconName: 'timer_blue' };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelectMultiple component with props for job offer required experience', () => {
      // given
      const expected: object = {
        selectId: 'required-experience',
        selectPlaceholderKey: 'jobOffer.requiredExperiencesPlaceholder',
        selectRequired: true,
        selectOptions: [
          {
            optionLabelKey: 'jobOffer.requiredExperienceZeroToTwoYears',
            optionValue: JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS,
          },
          {
            optionLabelKey: 'jobOffer.requiredExperienceThreeToFiveYears',
            optionValue: JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
          },
          {
            optionLabelKey: 'jobOffer.requiredExperienceSixToTenYears',
            optionValue: JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
          },
          {
            optionLabelKey: 'jobOffer.requiredExperienceElevenAndMore',
            optionValue: JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
          },
        ],
        selectErrors: undefined,
      };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelectMultiple).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === expected.selectId
      );
      // @ts-ignore
      expect(selects.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelectMultiple component with select errors for job offer required experience', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aRequiredExperiencesError'],
        messages: ['A required experience error'],
      };
      propsData.options.domainErrors = { requiredExperiences: expected };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelectMultiple).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === 'required-experience'
      );
      // @ts-ignore
      expect(selects.at(0).vm.options.selectErrors).toStrictEqual(expected);
    });

    it('should bind job offer requiredExperiences property to GbSelectMultiple input prop', () => {
      // given
      const requiredExperiences: JobOfferEnums.JobOfferRequiredExperience[] = [
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
      ];
      propsData.jobOffer.requiredExperiences = requiredExperiences;

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelectMultiple).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.value === requiredExperiences
      );
      expect(selects).toHaveLength(1);
    });

    it('should emit input event to parent when input event is triggered on select', () => {
      // given
      const selectValues: JobOfferEnums.JobOfferRequiredExperience[] = [
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
      ];
      propsData.jobOffer.requiredExperiences = selectValues;

      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });
      const requiredExperiencesGbSelectMultiple: Wrapper<Vue> = gbJobOfferCreateHeaderWrapper
        .findAll(GbSelectMultiple)
        .filter(
          (select: Wrapper<Vue>) =>
            // @ts-ignore
            select.vm.value === propsData.jobOffer.requiredExperiences
        )
        .at(0);

      // when
      requiredExperiencesGbSelectMultiple.vm.$emit('input', selectValues);

      // then
      const expected: { fieldName: string; fieldValue: JobOfferEnums.JobOfferRequiredExperience[] } = {
        fieldName: 'requiredExperiences',
        fieldValue: selectValues,
      };
      expect(gbJobOfferCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('positionType', () => {
    it('should display GbIcon component with props for job offer position type', () => {
      // given
      const expected: object = { iconName: 'person_blue' };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelect component with props for job offer position type', () => {
      // given
      const expected: object = {
        selectId: 'position-type',
        selectPlaceholderKey: 'jobOffer.positionTypePlaceholder',
        selectRequired: true,
        selectOptions: [
          { optionLabelKey: 'jobOffer.positionTypeAnalyst', optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_ANALYST },
          { optionLabelKey: 'jobOffer.positionTypeOthers', optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_OTHERS },
          {
            optionLabelKey: 'jobOffer.positionTypeProjectManager',
            optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_PROJECT_MANAGER,
          },
          {
            optionLabelKey: 'jobOffer.positionTypeHeadOfDepartment',
            optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_HEAD_OF_DEPARTMENT,
          },
          { optionLabelKey: 'jobOffer.positionTypeResearcher', optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_RESEARCHER },
          { optionLabelKey: 'jobOffer.positionTypeDesigner', optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_DESIGNER },
          { optionLabelKey: 'jobOffer.positionTypeAdvisor', optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_ADVISOR },
          { optionLabelKey: 'jobOffer.positionTypeConsultant', optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_CONSULTANT },
          { optionLabelKey: 'jobOffer.positionTypeCoordinator', optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_COORDINATOR },
          {
            optionLabelKey: 'jobOffer.positionTypeManagingDirector',
            optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_MANAGING_DIRECTOR,
          },
          {
            optionLabelKey: 'jobOffer.positionTypeDirectorOfOperations',
            optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_DIRECTOR_OF_OPERATIONS,
          },
          {
            optionLabelKey: 'jobOffer.positionTypeDirectorDirector',
            optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_DIRECTOR_DIRECTOR,
          },
          {
            optionLabelKey: 'jobOffer.positionTypeDeveloperProgrammer',
            optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_DEVELOPER_PROGRAMMER,
          },
          {
            optionLabelKey: 'jobOffer.positionTypeForestryEngineer',
            optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_FORESTRY_ENGINEER,
          },
          {
            optionLabelKey: 'jobOffer.positionTypeJuniorEngineersCep',
            optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_JUNIOR_ENGINEERS_CEP,
          },
          { optionLabelKey: 'jobOffer.positionTypeEngineer', optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_ENGINEER },
          {
            optionLabelKey: 'jobOffer.positionTypeProjectEngineer',
            optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_PROJECT_ENGINEER,
          },
          { optionLabelKey: 'jobOffer.positionTypeProfessor', optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_PROFESSOR },
          {
            optionLabelKey: 'jobOffer.positionTypePresidentOfTheBoardOfDirectors',
            optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_PRESIDENT_OF_THE_BOARD_OF_DIRECTORS,
          },
          {
            optionLabelKey: 'jobOffer.positionTypeViceChairperson',
            optionValue: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_VICE_CHAIRPERSON,
          },
        ],
        selectIsLarge: true,
        selectErrors: undefined,
      };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === expected.selectId
      );
      // @ts-ignore
      expect(selects.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelect component with select errors for job offer positionTypes', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aPositionTypeError'],
        messages: ['A position type error'],
      };
      propsData.options.domainErrors = { positionType: expected };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === 'position-type'
      );
      // @ts-ignore
      expect(selects.at(0).vm.options.selectErrors).toStrictEqual(expected);
    });

    it('should bind job offer positionType property to GbSelect input prop', () => {
      // given
      const positionType: JobOfferEnums.JobOfferPositionType = JobOfferEnums.JobOfferPositionType.POSITION_TYPE_CONSULTANT;
      propsData.jobOffer.positionType = positionType;

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelect).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.value === positionType
      );
      expect(selects).toHaveLength(1);
    });
    it('should emit input event to parent when change event is triggered on select', () => {
      // given
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      const selectField: Wrapper<Vue> = gbJobOfferCreateHeaderWrapper.find('#position-type');
      const selectValue: JobOfferEnums.JobOfferPositionType = JobOfferEnums.JobOfferPositionType.POSITION_TYPE_CONSULTANT;

      // when
      (selectField.find(`option[value="${selectValue}"]`).element as HTMLOptionElement).selected = true;
      selectField.trigger('change');

      // then
      const expected: { fieldName: string; fieldValue: JobOfferEnums.JobOfferPositionType } = {
        fieldName: 'positionType',
        fieldValue: selectValue,
      };

      expect(gbJobOfferCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('requestedSpecialities', () => {
    it('should display GbIcon component with props for job offer target customers', () => {
      // given
      const expected: object = { iconName: 'wheels_blue' };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const icons: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbIcon).filter(
        (icon: Wrapper<Vue>) =>
          // @ts-ignore
          icon.vm.options.iconName === expected.iconName
      );

      // @ts-ignore
      expect(icons.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelectMultiple component with props for job offer requested specialities', () => {
      // given
      const expected: object = {
        selectId: 'requested-specialities',
        selectPlaceholderKey: 'jobOffer.requestedSpecialitiesPlaceholder',
        selectRequired: true,
        selectOptions: [
          {
            optionLabelKey: 'jobOffer.requestedSpecialityAeronauticsAerospace',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityAgriculture',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityNoneInParticular',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NONE_IN_PARTICULAR,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityOtherSpeciality',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OTHER_SPECIALITY,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityBiomedical',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityBiotechnology',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOTECHNOLOGY,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityChemistry',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CHEMISTRY,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityCivil',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CIVIL,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityConstruction',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CONSTRUCTION,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityDocumentation',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_DOCUMENTATION,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityElectrical',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ELECTRICAL,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityEnvironment',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ENVIRONMENT,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityGeology',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_GEOLOGY,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityQualityManagement',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_QUALITY_MANAGEMENT,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityIndustrialManufacturer',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INDUSTRIAL_MANUFACTURER,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityInformationTechnology',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INFORMATION_TECHNOLOGY,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityInstrumentation',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INSTRUMENTATION,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialitySoftware',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_SOFTWARE,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityMaterialHandlingDistribution',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MATERIAL_HANDLING_DISTRIBUTION,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityMaritimeNaval',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MARITIME_NAVAL,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityNonMetallicMaterials',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NON_METALLIC_MATERIALS,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityMechanics',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MECHANICS,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityMetallurgyMetals',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_METALLURGY_METALS,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityMining',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MINING,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityNuclearPower',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NUCLEAR_POWER,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityPulpAndPaper',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PULP_AND_PAPER,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityPhysical',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PHYSICAL,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityAutomatedProduction',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityRobotics',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ROBOTICS,
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityOccupationalHealthAndSafety',
            optionValue: JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OCCUPATIONAL_HEALTH_AND_SAFETY,
          },
        ],
        selectErrors: undefined,
      };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelectMultiple).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === expected.selectId
      );
      // @ts-ignore
      expect(selects.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbSelectMultiple component with select errors for job offer requested specialities', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aRequestedSpecialitiesError'],
        messages: ['A required experience error'],
      };
      propsData.options.domainErrors = { requestedSpecialities: expected };

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelectMultiple).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.selectId === 'requested-specialities'
      );
      // @ts-ignore
      expect(selects.at(0).vm.options.selectErrors).toStrictEqual(expected);
    });

    it('should bind company requestedSpecialities property to GbSelectMultiple input prop', () => {
      // given
      const requestedSpecialities: JobOfferEnums.JobOfferRequestedSpeciality[] = [
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL,
      ];
      propsData.jobOffer.requestedSpecialities = requestedSpecialities;

      // when
      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

      // then
      const selects: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbSelectMultiple).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.value === requestedSpecialities
      );
      expect(selects).toHaveLength(1);
    });

    it('should emit input event to parent when input event is triggered on select', () => {
      // given
      const selectValues: JobOfferEnums.JobOfferRequestedSpeciality[] = [
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL,
      ];
      propsData.jobOffer.requestedSpecialities = selectValues;

      const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });
      const requestedSpecialitiesGbSelectMultiple: Wrapper<Vue> = gbJobOfferCreateHeaderWrapper
        .findAll(GbSelectMultiple)
        .filter(
          (select: Wrapper<Vue>) =>
            // @ts-ignore
            select.vm.value === propsData.jobOffer.requestedSpecialities
        )
        .at(0);

      // when
      requestedSpecialitiesGbSelectMultiple.vm.$emit('input', selectValues);

      // then
      const expected: { fieldName: string; fieldValue: JobOfferEnums.JobOfferRequestedSpeciality[] } = {
        fieldName: 'requestedSpecialities',
        fieldValue: selectValues,
      };
      expect(gbJobOfferCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  it('should display 5 GbElementWithIcon components with options', () => {
    // given
    const expected: object = { inputWithIconPosition: 'OUTSIDE', inputWithIconIsLarge: true };

    // when
    const gbJobOfferCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateHeader, { mocks: { $t }, propsData });

    // then
    const elementsWithIcon: WrapperArray<Vue> = gbJobOfferCreateHeaderWrapper.findAll(GbElementWithIcon);

    // @ts-ignore
    expect(elementsWithIcon.length).toBe(5);

    for (let i: number = 0; i < 5; i++) {
      // @ts-ignore
      expect(elementsWithIcon.at(i).vm.options).toStrictEqual(expected);
    }
  });
});

import { mount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbBlock from '~/components/base-components/gb-block.vue';
import GbInputText from '~/components/base-components/gb-input-text.vue';
import { JobOfferConstraints } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbJobOfferCreateInternalReferenceNumber from '../gb-job-offer-create-internal-reference-number.vue';

describe('components/job-offer/create/gb-job-offer-create-internal-reference-number', () => {
  let $t: jest.Mock;
  let propsData: {
    value: string;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: '',
      options: {},
    };
  });

  describe('label', () => {
    it('should have a GbBlock with label in header', () => {
      // given
      const expected: string = 'Internal reference number';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateInternalReferenceNumberWrapper: Wrapper<Vue> = mount(GbJobOfferCreateInternalReferenceNumber, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateInternalReferenceNumberWrapper.find(GbBlock);

      // then
      expect(result.find('.gb-block--header').text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(1, 'jobOffer.internalReferenceNumberLabel');
    });
  });
  describe('internal reference number', () => {
    it('should display GbInputText component with props for job offer internal reference number', () => {
      // given
      const expected: object = {
        inputId: 'internal-reference-number',
        inputPlaceholderKey: 'jobOffer.internalReferenceNumberPlaceholder',
        inputMaxLength: JobOfferConstraints.JOB_OFFER_INTERNAL_REFERENCE_NUMBER_MAX_LENGTH,
        inputRequired: false,
        inputIsLarge: true,
        inputErrors: undefined,
      };

      // when
      const gbJobOfferCreateInternalReferenceNumberWrapper: Wrapper<Vue> = mount(GbJobOfferCreateInternalReferenceNumber, {
        mocks: { $t },
        propsData,
      });

      // then
      const inputText: Wrapper<Vue> = gbJobOfferCreateInternalReferenceNumberWrapper.find(GbInputText);

      // @ts-ignore
      expect(inputText.vm.options).toStrictEqual(expected);
    });

    it('should bind job offer internal reference number to GbInputText input', () => {
      // given
      propsData.value = 'AB_12345-D';

      // when
      const gbJobOfferCreateInternalReferenceNumberWrapper: Wrapper<Vue> = mount(GbJobOfferCreateInternalReferenceNumber, {
        mocks: { $t },
        propsData,
      });

      // then
      const inputText: Wrapper<Vue> = gbJobOfferCreateInternalReferenceNumberWrapper.find(GbInputText);

      // @ts-ignore
      expect(inputText.vm.value).toBe('AB_12345-D');
    });
    it('should set inputErrors when there is a domain errors for this field', async () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        messages: ['An internal reference number error'],
        i18nMessageKeys: ['jobOffer.anInternalReferenceNumberError'],
      };
      propsData.options = { domainErrors: { internalReferenceNumber: expected } };
      const gbJobOfferCreateInternalReferenceNumberWrapper: Wrapper<Vue> = mount(GbJobOfferCreateInternalReferenceNumber, {
        mocks: { $t },
        propsData,
      });

      // then
      const inputText: Wrapper<Vue> = gbJobOfferCreateInternalReferenceNumberWrapper.find(GbInputText);

      // @ts-ignore
      expect(inputText.vm.options.inputErrors).toStrictEqual(expected);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const gbJobOfferCreateInternalReferenceNumberWrapper: Wrapper<Vue> = mount(GbJobOfferCreateInternalReferenceNumber, {
        mocks: { $t },
        propsData,
      });

      const inputField: Wrapper<Vue> = gbJobOfferCreateInternalReferenceNumberWrapper.find('#internal-reference-number');

      // when
      (inputField.element as HTMLInputElement).value = 'AB_12345-D';
      inputField.trigger('input');

      // then
      expect(gbJobOfferCreateInternalReferenceNumberWrapper.emitted().input).toStrictEqual([['AB_12345-D']]);
    });
  });
});

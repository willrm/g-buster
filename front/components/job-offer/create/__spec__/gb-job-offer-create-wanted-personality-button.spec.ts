import { mount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbJobOfferCreateWantedPersonalityButton from '../gb-job-offer-create-wanted-personality-button.vue';

describe('components/job-offer/create/gb-job-offer-create-wanted-personality-button', () => {
  let $t: jest.Mock;
  let propsData: {
    value: boolean;
    options: {
      iconName: string;
      labelKey: string;
    };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: false,
      options: {
        iconName: '',
        labelKey: '',
      },
    };
  });

  describe('icon ON', () => {
    beforeEach(() => {
      propsData.value = true;
    });
    it('should contain a GbIcon component with ON version of icon', () => {
      // given
      propsData.options.iconName = 'medal';
      const gbJobOfferCreateWantedPersonalityButtonWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalityButton, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateWantedPersonalityButtonWrapper.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ iconName: 'medal_on' });
    });

    it('should contain a span with label and ON class', () => {
      // given
      propsData.options.labelKey = 'jobOffer.aWantedPersonalityLabel';
      const expected: string = 'Ambitious';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateWantedPersonalityButtonWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalityButton, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateWantedPersonalityButtonWrapper.find('span');

      // then
      expect(result.text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(1, 'jobOffer.aWantedPersonalityLabel');
    });

    it('should contain a div with ON class', () => {
      // given
      const gbJobOfferCreateWantedPersonalityButtonWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalityButton, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateWantedPersonalityButtonWrapper.find('.gb-job-offer-create-wanted-personality-button');

      // then
      expect(result.element.classList).toContain('on');
    });

    it('should emit event with false to set button to OFF on click on button', () => {
      // given
      const gbJobOfferCreateWantedPersonalityButtonWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalityButton, {
        mocks: { $t },
        propsData,
      });
      const buttonWrapper: Wrapper<Vue> = gbJobOfferCreateWantedPersonalityButtonWrapper.find('button');

      // when
      buttonWrapper.trigger('click');

      // then
      expect(gbJobOfferCreateWantedPersonalityButtonWrapper.emitted().input).toStrictEqual([[false]]);
    });

    it('should emit event with false to set button to OFF on click on span', () => {
      // given
      const gbJobOfferCreateWantedPersonalityButtonWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalityButton, {
        mocks: { $t },
        propsData,
      });
      const spanWrapper: Wrapper<Vue> = gbJobOfferCreateWantedPersonalityButtonWrapper.find('span');

      // when
      spanWrapper.trigger('click');

      // then
      expect(gbJobOfferCreateWantedPersonalityButtonWrapper.emitted().input).toStrictEqual([[false]]);
    });
  });
  describe('icon OFF', () => {
    beforeEach(() => {
      propsData.value = false;
    });
    it('should contain a GbIcon component with OFF version of icon', () => {
      // given
      propsData.options.iconName = 'medal';
      const gbJobOfferCreateWantedPersonalityButtonWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalityButton, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateWantedPersonalityButtonWrapper.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ iconName: 'medal_off' });
    });

    it('should contain a span with label', () => {
      // given
      propsData.options.labelKey = 'jobOffer.aWantedPersonalityLabel';
      const expected: string = 'Ambitious';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateWantedPersonalityButtonWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalityButton, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateWantedPersonalityButtonWrapper.find('span');

      // then
      expect(result.text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(1, 'jobOffer.aWantedPersonalityLabel');
    });

    it('should contain a div without ON class', () => {
      // given
      const gbJobOfferCreateWantedPersonalityButtonWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalityButton, {
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateWantedPersonalityButtonWrapper.find('.gb-job-offer-create-wanted-personality-button');

      // then
      expect(result.element.classList).not.toContain('on');
    });

    it('should emit event with true to set button to ON on click on button', () => {
      // given
      const gbJobOfferCreateWantedPersonalityButtonWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalityButton, {
        mocks: { $t },
        propsData,
      });
      const buttonWrapper: Wrapper<Vue> = gbJobOfferCreateWantedPersonalityButtonWrapper.find('button');

      // when
      buttonWrapper.trigger('click');

      // then
      expect(gbJobOfferCreateWantedPersonalityButtonWrapper.emitted().input).toStrictEqual([[true]]);
    });

    it('should emit event with true to set button to ON on click on span', () => {
      // given
      const gbJobOfferCreateWantedPersonalityButtonWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalityButton, {
        mocks: { $t },
        propsData,
      });
      const spanWrapper: Wrapper<Vue> = gbJobOfferCreateWantedPersonalityButtonWrapper.find('span');

      // when
      spanWrapper.trigger('click');

      // then
      expect(gbJobOfferCreateWantedPersonalityButtonWrapper.emitted().input).toStrictEqual([[true]]);
    });
  });
});

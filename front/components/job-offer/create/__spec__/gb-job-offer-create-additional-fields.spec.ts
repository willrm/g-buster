import { mount, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import {
  AdvantagesEnums,
  JobOfferEnums,
  TravelingEnums,
} from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import {
  PutAdvandagesRequestInterface,
  PutAnnualSalaryRangeInterface,
  PutJobOfferRequestInterface,
  PutStartDateOfEmploymentInterface,
  PutTravelingRequestInterface,
} from '../../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import { DomainErrorsResponseInterface } from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbJobOfferCreateAdditionalFields from '../gb-job-offer-create-additional-fields.vue';
import GbJobOfferCreateAdvantages from '../gb-job-offer-create-advantages.vue';
import GbJobOfferCreateAnnualSalaryRange from '../gb-job-offer-create-annual-salary-range.vue';
import GbJobOfferCreateEmailAddressForReceiptOfApplications from '../gb-job-offer-create-email-address-for-receipt-of-applications.vue';
import GbJobOfferCreateEndDateOfApplication from '../gb-job-offer-create-end-date-of-application.vue';
import GbJobOfferCreateStartDateOfEmployment from '../gb-job-offer-create-start-date-of-employment.vue';
import GbJobOfferCreateTraveling from '../gb-job-offer-create-traveling.vue';
import GbJobOfferCreateWantedPersonalities from '../gb-job-offer-create-wanted-personalities.vue';

describe('components/job-offer/create/gb-job-offer-create-content', () => {
  let $t: jest.Mock;
  let propsData: {
    jobOffer: PutJobOfferRequestInterface;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffer: {
        annualSalaryRange: {} as PutAnnualSalaryRangeInterface,
        emailAddressForReceiptOfApplications: '',
        endDateOfApplication: new Date(),
        internalReferenceNumber: '',
        specificities: '',
        startDateOfEmployment: {} as PutStartDateOfEmploymentInterface,
        wantedPersonalities: [],
        city: '',
        description: '',
        jobStatus: JobOfferEnums.JobOfferJobStatus.JOB_STATUS_FULL_TIME,
        jobType: JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP,
        positionType: JobOfferEnums.JobOfferPositionType.POSITION_TYPE_ADVISOR,
        region: JobOfferEnums.JobOfferRegion.REGION_ABITIBI_TEMISCAMINGUE,
        requiredExperiences: [] as JobOfferEnums.JobOfferRequiredExperience[],
        title: '',
        targetCustomers: [] as JobOfferEnums.JobOfferTargetCustomer[],
        requiredExperience: [] as JobOfferEnums.JobOfferRequiredExperience[],
        requestedSpecialities: [] as JobOfferEnums.JobOfferRequestedSpeciality[],
        advantages: {} as PutAdvandagesRequestInterface,
        traveling: {} as PutTravelingRequestInterface,
      } as PutJobOfferRequestInterface,
      options: {},
    };
  });

  describe('title', () => {
    it('should contain a title', () => {
      // given
      $t.mockReturnValue('Additional fields');
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateAdditionalFieldsWrapper.find('h2');

      // then
      expect($t).toHaveBeenNthCalledWith(1, 'jobOffer.additionalFieldsTitle');
      expect(result.text()).toBe('Additional fields');
    });
  });

  describe('advantages', () => {
    it('should bind job offer advantages prop to GbJobOfferCreateAdvantages input prop', () => {
      // given
      propsData.jobOffer = ({
        advantages: { health: [] },
      } as unknown) as PutJobOfferRequestInterface;
      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateAdvantages).vm.value).toStrictEqual({ health: [] });
    });

    it('should set GbJobOfferCreateAdvantages options prop', () => {
      // given
      const expected: {} = {
        domainErrors: {},
      };

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateAdvantages).vm.options).toStrictEqual(expected);
    });

    it('should set GbJobOfferCreateAdvantages options prop when domainErrors is defined', () => {
      // given
      const expected: DomainErrorsResponseInterface = {
        traveling: {
          i18nMessageKeys: ['jobOffer.anAdvantagesError'],
          messages: ['An advantages error'],
        },
      };
      propsData.options.domainErrors = expected;

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateAdvantages).vm.options).toStrictEqual({ domainErrors: expected });
    });

    it('should emit input event when input is triggered from child', () => {
      // given
      propsData.jobOffer.advantages = { health: [] };
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdditionalFields, {
        stubs: { 'client-only': true, 'date-picker': true },
        mocks: { $t },
        propsData,
      });

      const gbJobOfferCreateTravelingWrapper: Wrapper<Vue> = gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateAdvantages);
      const inputExpected: { fieldName: string; fieldValue: PutAdvandagesRequestInterface } = {
        fieldName: 'advantages',
        fieldValue: { health: [AdvantagesEnums.AdvantageHealth.HEALTH_DENTAL_INSURANCE] },
      };

      // when
      gbJobOfferCreateTravelingWrapper.vm.$emit('input', { health: [AdvantagesEnums.AdvantageHealth.HEALTH_DENTAL_INSURANCE] });

      // then
      expect(gbJobOfferCreateAdditionalFieldsWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });

  describe('traveling', () => {
    it('should bind jobOffer prop to GbJobOfferCreateTraveling input prop', () => {
      // given
      propsData.jobOffer = {
        traveling: { isTraveling: true },
      } as PutJobOfferRequestInterface;
      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateTraveling).vm.value).toStrictEqual({ isTraveling: true });
    });

    it('should set GbJobOfferCreateTraveling options prop', () => {
      // given
      const expected: {} = {
        domainErrors: {},
      };

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateTraveling).vm.options).toStrictEqual(expected);
    });

    it('should set GbJobOfferCreateTraveling options prop when domainErrors is defined', () => {
      // given
      const expected: DomainErrorsResponseInterface = {
        traveling: {
          i18nMessageKeys: ['jobOffer.aTravelingError'],
          messages: ['A traveling error'],
        },
      };
      propsData.options.domainErrors = expected;

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateTraveling).vm.options).toStrictEqual({ domainErrors: expected });
    });

    it('should emit input event when input is triggered from child', () => {
      // given
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdditionalFields, {
        stubs: { 'client-only': true, 'date-picker': true },
        mocks: { $t },
        propsData,
      });

      const gbJobOfferCreateTravelingWrapper: Wrapper<Vue> = gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateTraveling);
      const inputExpected: { fieldName: string; fieldValue: PutTravelingRequestInterface } = {
        fieldName: 'traveling',
        fieldValue: { canada: TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL },
      };

      // when
      gbJobOfferCreateTravelingWrapper.vm.$emit('input', { canada: TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL });

      // then
      expect(gbJobOfferCreateAdditionalFieldsWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });

  describe('annual salary range', () => {
    it('should bind jobOffer prop to GbJobOfferCreateAnnualSalaryRange input prop', () => {
      // given
      propsData.jobOffer.annualSalaryRange = { minimumSalary: 12345 };

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateAnnualSalaryRange).vm.value).toStrictEqual({ minimumSalary: 12345 });
    });

    it('should set GbJobOfferCreateAnnualSalaryRange options prop', async () => {
      // given
      const expected: {} = {
        domainErrors: {},
      };

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateAnnualSalaryRange).vm.options).toStrictEqual(expected);
    });

    it('should set GbJobOfferCreateAnnualSalaryRange options prop when domainErrors is defined', () => {
      // given
      const expected: DomainErrorsResponseInterface = {
        traveling: {
          i18nMessageKeys: ['jobOffer.anAnnualSalaryRangeError'],
          messages: ['An annual salary range error'],
        },
      };
      propsData.options.domainErrors = expected;

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateAnnualSalaryRange).vm.options).toStrictEqual({ domainErrors: expected });
    });

    it('should emit input event when input is triggered from child', () => {
      // given
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdditionalFields, {
        stubs: { 'client-only': true, 'date-picker': true },
        mocks: { $t },
        propsData,
      });

      const gbJobOfferCreateTravelingWrapper: Wrapper<Vue> = gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateAnnualSalaryRange);
      const inputExpected: { fieldName: string; fieldValue: PutAnnualSalaryRangeInterface } = {
        fieldName: 'annualSalaryRange',
        fieldValue: { minimumSalary: 12345 },
      };

      // when
      gbJobOfferCreateTravelingWrapper.vm.$emit('input', { minimumSalary: 12345 });

      // then
      expect(gbJobOfferCreateAdditionalFieldsWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });

  describe('end date of application', () => {
    it('should bind jobOffer prop to GbJobOfferCreateEndDateOfApplication prop', () => {
      // given
      const fixedDate: Date = new Date();
      propsData.jobOffer.endDateOfApplication = fixedDate;

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateEndDateOfApplication).vm.value).toStrictEqual(fixedDate);
    });

    it('should set GbJobOfferCreateEndDateOfApplication options prop', () => {
      // given
      const expected: {} = {
        domainErrors: {},
      };

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateEndDateOfApplication).vm.options).toStrictEqual(expected);
    });

    it('should set GbJobOfferCreateEndDateOfApplication options prop when domainErrors is defined', () => {
      // given
      const expected: DomainErrorsResponseInterface = {
        traveling: {
          i18nMessageKeys: ['jobOffer.anEndDateOfApplicationError'],
          messages: ['An end date of application range error'],
        },
      };
      propsData.options.domainErrors = expected;

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateEndDateOfApplication).vm.options).toStrictEqual({ domainErrors: expected });
    });

    it('should emit input event when input is triggered from child', () => {
      // given
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdditionalFields, {
        stubs: { 'client-only': true, 'date-picker': true },
        mocks: { $t },
        propsData,
      });

      const gbJobOfferCreateTravelingWrapper: Wrapper<Vue> = gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateEndDateOfApplication);
      const fixedDate: Date = new Date();
      const inputExpected: { fieldName: string; fieldValue: Date } = {
        fieldName: 'endDateOfApplication',
        fieldValue: fixedDate,
      };

      // when
      gbJobOfferCreateTravelingWrapper.vm.$emit('input', fixedDate);

      // then
      expect(gbJobOfferCreateAdditionalFieldsWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });

  describe('start date of employment', () => {
    it('should bind jobOffer prop to GbJobOfferCreateStartDateOfEmployment prop', () => {
      // given
      const fixedDate: Date = new Date();
      propsData.jobOffer.startDateOfEmployment = {
        startDate: fixedDate,
      };

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateStartDateOfEmployment).vm.value).toStrictEqual({
        startDate: fixedDate,
      });
    });

    it('should set GbJobOfferCreateStartDateOfEmployment options prop', () => {
      // given
      const expected: {} = {
        domainErrors: {},
      };

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateStartDateOfEmployment).vm.options).toStrictEqual(expected);
    });

    it('should set GbJobOfferCreateStartDateOfEmployment options prop when domainErrors is defined', () => {
      // given
      const expected: DomainErrorsResponseInterface = {
        traveling: {
          i18nMessageKeys: ['jobOffer.anEndDateOfApplicationError'],
          messages: ['An start date of employment range error'],
        },
      };
      propsData.options.domainErrors = expected;

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateStartDateOfEmployment).vm.options).toStrictEqual({
        domainErrors: expected,
      });
    });

    it('should emit input event when input is triggered from child', () => {
      // given
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdditionalFields, {
        stubs: { 'client-only': true, 'date-picker': true },
        mocks: { $t },
        propsData,
      });

      const gbJobOfferCreateTravelingWrapper: Wrapper<Vue> = gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateStartDateOfEmployment);
      const fixedDate: Date = new Date();
      const inputExpected: { fieldName: string; fieldValue: PutStartDateOfEmploymentInterface } = {
        fieldName: 'startDateOfEmployment',
        fieldValue: {
          startDate: fixedDate,
        },
      };

      // when
      gbJobOfferCreateTravelingWrapper.vm.$emit('input', {
        startDate: fixedDate,
      });

      // then
      expect(gbJobOfferCreateAdditionalFieldsWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });

  describe('wanted personality', () => {
    it('should bind jobOffer prop to GbJobOfferCreateWantedPersonalities prop', () => {
      // given
      propsData.jobOffer.wantedPersonalities = [
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS,
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AUTONOMOUS,
      ];

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateWantedPersonalities).vm.value).toStrictEqual([
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS,
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AUTONOMOUS,
      ]);
    });

    it('should set GbJobOfferCreateWantedPersonalities options prop', () => {
      // given
      const expected: {} = {
        domainErrors: {},
      };

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateWantedPersonalities).vm.options).toStrictEqual(expected);
    });

    it('should set GbJobOfferCreateWantedPersonalities options prop when domainErrors is defined', () => {
      // given
      const expected: DomainErrorsResponseInterface = {
        traveling: {
          i18nMessageKeys: ['jobOffer.aWantedPersonalityError'],
          messages: ['A wanted personality range error'],
        },
      };
      propsData.options.domainErrors = expected;

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateWantedPersonalities).vm.options).toStrictEqual({
        domainErrors: expected,
      });
    });

    it('should emit input event when input is triggered from child', () => {
      // given
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdditionalFields, {
        stubs: { 'client-only': true, 'date-picker': true },
        mocks: { $t },
        propsData,
      });

      const gbJobOfferCreateTravelingWrapper: Wrapper<Vue> = gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateWantedPersonalities);

      const inputExpected: { fieldName: string; fieldValue: JobOfferEnums.JobOfferWantedPersonality[] } = {
        fieldName: 'wantedPersonalities',
        fieldValue: [
          JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS,
          JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AUTONOMOUS,
        ],
      };

      // when
      gbJobOfferCreateTravelingWrapper.vm.$emit('input', [
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS,
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AUTONOMOUS,
      ]);

      // then
      expect(gbJobOfferCreateAdditionalFieldsWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });

  describe('email address for receipt of applications', () => {
    it('should bind jobOffer prop to GbJobOfferCreateEmailAddressForReceiptOfApplications prop', () => {
      // given
      propsData.jobOffer.emailAddressForReceiptOfApplications = 'apply@genium360.ca';

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateEmailAddressForReceiptOfApplications).vm.value).toStrictEqual(
        'apply@genium360.ca'
      );
    });

    it('should set GbJobOfferCreateEmailAddressForReceiptOfApplications options prop', () => {
      // given
      const expected: {} = {
        domainErrors: {},
      };

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateEmailAddressForReceiptOfApplications).vm.options).toStrictEqual(expected);
    });

    it('should set GbJobOfferCreateEmailAddressForReceiptOfApplications options prop when domainErrors is defined', () => {
      // given
      const expected: DomainErrorsResponseInterface = {
        traveling: {
          i18nMessageKeys: ['jobOffer.anEmailAddressForReceiptOfApplicationsError'],
          messages: ['An email address for receipt of applications range error'],
        },
      };
      propsData.options.domainErrors = expected;

      // when
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = shallowMount(GbJobOfferCreateAdditionalFields, { mocks: { $t }, propsData });

      // then
      // @ts-ignore
      expect(gbJobOfferCreateAdditionalFieldsWrapper.find(GbJobOfferCreateEmailAddressForReceiptOfApplications).vm.options).toStrictEqual({
        domainErrors: expected,
      });
    });

    it('should emit input event when input is triggered from child', () => {
      // given
      const gbJobOfferCreateAdditionalFieldsWrapper: Wrapper<Vue> = mount(GbJobOfferCreateAdditionalFields, {
        stubs: { 'client-only': true, 'date-picker': true },
        mocks: { $t },
        propsData,
      });

      const gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper: Wrapper<Vue> = gbJobOfferCreateAdditionalFieldsWrapper.find(
        GbJobOfferCreateEmailAddressForReceiptOfApplications
      );
      const inputExpected: { fieldName: string; fieldValue: string } = {
        fieldName: 'emailAddressForReceiptOfApplications',
        fieldValue: 'apply@genium360.ca',
      };

      // when
      gbJobOfferCreateEmailAddressForReceiptOfApplicationsWrapper.vm.$emit('input', 'apply@genium360.ca');

      // then
      expect(gbJobOfferCreateAdditionalFieldsWrapper.emitted().input).toStrictEqual([[inputExpected]]);
    });
  });
});

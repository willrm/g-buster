import { mount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBlock from '~/components/base-components/gb-block.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbInputCheckbox from '~/components/base-components/gb-input-checkbox.vue';
import GbInputRadio from '~/components/base-components/gb-input-radio.vue';
import { TravelingEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { PutTravelingRequestInterface } from '../../../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbJobOfferCreateTraveling from '../gb-job-offer-create-traveling.vue';

describe('components/job-offer/create/gb-job-offer-create-traveling', () => {
  let $t: jest.Mock;
  let propsData: {
    value: PutTravelingRequestInterface;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: {} as PutTravelingRequestInterface,
      options: {},
    };
  });

  describe('label', () => {
    it('should have a GbBlock with label in header', () => {
      // given
      const expected: string = 'Travelings';
      $t.mockReturnValue(expected);
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find(GbBlock);

      // then
      expect(result.find('.gb-block--header').text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(1, 'jobOffer.travelingLabel');
    });
  });

  describe('subheader', () => {
    it('should have a GbBlock with explanation in subheader', () => {
      // given
      const expected: string = 'Travelings subheader';
      $t.mockReturnValue(expected);
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find(GbBlock);

      // then
      expect(result.find('.gb-block--header').text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(2, 'jobOffer.travelingExplanation');
    });
  });

  describe('no traveling checkbox', () => {
    it('should display a GbInputCheckbox for no traveling disabled when isTraveling is true', () => {
      // given
      propsData.value.isTraveling = true;
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find(GbInputCheckbox);

      // then
      // @ts-ignore
      expect(result.vm.value).toBe(false);
    });
    it('should display a GbInputCheckbox for no traveling enabled when isTraveling is false', () => {
      // given
      propsData.value.isTraveling = false;
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find(GbInputCheckbox);

      // then
      // @ts-ignore
      expect(result.vm.value).toBe(true);
    });
    it('should display a GbInputCheckbox for no traveling disabled when isTraveling is undefined', () => {
      // given
      propsData.value.isTraveling = undefined;
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find(GbInputCheckbox);

      // then
      // @ts-ignore
      expect(result.vm.value).toBe(false);
    });
    it('should display a GbInputCheckbox for no traveling with options', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find(GbInputCheckbox);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({
        inputId: 'no-traveling',
        inputLabelKey: 'jobOffer.travelingNoTravelingLabel',
        inputName: 'noTraveling',
      });
    });
    it('should add a disabled css class to countries when isTraveling is false', () => {
      // given
      propsData.value.isTraveling = false;
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('.gb-job-offer-create-traveling--traveling-countries');

      // then
      expect(result.element.classList.contains('disabled')).toBe(true);
    });
    it('should not add a disabled css class to countries when isTraveling is true', () => {
      // given
      propsData.value.isTraveling = true;
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('.gb-job-offer-create-traveling--traveling-countries');

      // then
      expect(result.element.classList.contains('disabled')).toBe(false);
    });
    it('should not add a disabled css class to countries when isTraveling is undefined', () => {
      // given
      propsData.value.isTraveling = undefined;
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('.gb-job-offer-create-traveling--traveling-countries');

      // then
      expect(result.element.classList.contains('disabled')).toBe(false);
    });
    it('should emit input event to parent when change event is triggered on input with cleaned values for other fields when it is true', () => {
      // given
      propsData.value = {
        isTraveling: true,
        quebec: TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL,
        canada: TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL,
        international: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      };
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      const inputCheckboxField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#no-traveling');

      // when
      inputCheckboxField.trigger('click');

      // then
      const expected: PutTravelingRequestInterface = {
        isTraveling: false,
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });

    it('should emit input event to parent when change event is triggered on input with other fields when it is false', () => {
      // given
      propsData.value = {
        isTraveling: false,
        quebec: TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL,
        canada: TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL,
        international: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      };
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      const inputCheckboxField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#no-traveling');

      // when
      inputCheckboxField.trigger('click');

      // then
      const expected: PutTravelingRequestInterface = {
        isTraveling: true,
        quebec: TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL,
        canada: TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL,
        international: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('quebec traveling', () => {
    it('should contain a GbIcon for Québec ', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const quebecDiv: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('.gb-job-offer-create-traveling--quebec');
      const result: Wrapper<Vue> = quebecDiv.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ iconName: 'quebec_flag' });
    });
    it('should contain label for Québec', () => {
      // given
      $t.mockReturnValue('Québec');
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('.gb-job-offer-create-traveling--quebec--label');

      // then
      expect($t).toHaveBeenNthCalledWith(4, 'jobOffer.travelingQuebecLabel');
      expect(result.text()).toBe('Québec');
    });
    it('should display GbInputRadio with props for job status', () => {
      // given
      const expected: object = {
        inputId: 'traveling-quebec',
        inputName: 'traveling-quebec-group',
        inputOptions: [
          {
            optionLabelKey: 'jobOffer.travelingTravelingFrequencyOccasional',
            optionValue: TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL,
          },
          {
            optionLabelKey: 'jobOffer.travelingTravelingFrequencyFrequent',
            optionValue: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
          },
        ],
        inputRequired: false,
        inputErrors: undefined,
      };

      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const inputs: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputRadio).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.inputId === expected.inputId
      );

      // then
      // @ts-ignore
      expect(inputs.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputRadio component with select errors', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aTravelingFrequencyError'],
        messages: ['A traveling quebec error'],
      };
      propsData.options.domainErrors = { quebec: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // then
      const inputs: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputRadio).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.inputId === 'traveling-quebec'
      );
      // @ts-ignore
      expect(inputs.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });
    it('should emit input event to parent when change event is triggered on input', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      const inputRadioField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#traveling-quebec_1');
      const inputValue: TravelingEnums.TravelingFrequency = TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL;

      // when
      inputRadioField.trigger('change');

      // then
      const expected: PutTravelingRequestInterface = {
        isTraveling: true,
        quebec: inputValue,
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('canada traveling', () => {
    it('should contain a GbIcon for Canada ', async () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const canadaDiv: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('.gb-job-offer-create-traveling--canada');
      const result: Wrapper<Vue> = canadaDiv.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ iconName: 'canada_flag' });
    });
    it('should contain a label for Canada', () => {
      // given
      $t.mockReturnValue('Canada');
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('.gb-job-offer-create-traveling--canada--label');

      // then
      expect($t).toHaveBeenNthCalledWith(5, 'jobOffer.travelingCanadaLabel');
      expect(result.text()).toBe('Canada');
    });
    it('should display GbInputRadio with props for job status', () => {
      // given
      const expected: object = {
        inputId: 'traveling-canada',
        inputName: 'traveling-canada-group',
        inputOptions: [
          {
            optionLabelKey: 'jobOffer.travelingTravelingFrequencyOccasional',
            optionValue: TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL,
          },
          {
            optionLabelKey: 'jobOffer.travelingTravelingFrequencyFrequent',
            optionValue: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
          },
        ],
        inputRequired: false,
        inputErrors: undefined,
      };

      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const inputs: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputRadio).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.inputId === expected.inputId
      );

      // then
      // @ts-ignore
      expect(inputs.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputRadio component with select errors', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aTravelingFrequencyError'],
        messages: ['A traveling canada error'],
      };
      propsData.options.domainErrors = { canada: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // then
      const inputs: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputRadio).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.inputId === 'traveling-canada'
      );
      // @ts-ignore
      expect(inputs.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });
    it('should emit input event to parent when change event is triggered on input', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      const inputRadioField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#traveling-canada_1');
      const inputValue: TravelingEnums.TravelingFrequency = TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL;

      // when
      inputRadioField.trigger('change');

      // then
      const expected: PutTravelingRequestInterface = {
        isTraveling: true,
        canada: inputValue,
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });

  describe('international traveling', () => {
    it('should contain a GbIcon for International ', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const internationalDiv: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('.gb-job-offer-create-traveling--international');
      const result: Wrapper<Vue> = internationalDiv.find(GbIcon);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({ iconName: 'international_flag' });
    });
    it('should contain label for International', () => {
      // given
      $t.mockReturnValue('International');
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('.gb-job-offer-create-traveling--international--label');

      // then
      expect($t).toHaveBeenNthCalledWith(6, 'jobOffer.travelingInternationalLabel');
      expect(result.text()).toBe('International');
    });
    it('should display GbInputRadio with props for job status', () => {
      // given
      const expected: object = {
        inputId: 'traveling-international',
        inputName: 'traveling-international-group',
        inputOptions: [
          {
            optionLabelKey: 'jobOffer.travelingTravelingFrequencyOccasional',
            optionValue: TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL,
          },
          {
            optionLabelKey: 'jobOffer.travelingTravelingFrequencyFrequent',
            optionValue: TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
          },
        ],
        inputRequired: false,
        inputErrors: undefined,
      };

      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // when
      const inputs: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputRadio).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.inputId === expected.inputId
      );

      // then
      // @ts-ignore
      expect(inputs.at(0).vm.options).toStrictEqual(expected);
    });

    it('should display GbInputRadio component with select errors', () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        i18nMessageKeys: ['jobOffer.aTravelingFrequencyError'],
        messages: ['A traveling international error'],
      };
      propsData.options.domainErrors = { international: expected };

      // when
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      // then
      const inputs: WrapperArray<Vue> = gbCompanyCreateHeaderWrapper.findAll(GbInputRadio).filter(
        (select: Wrapper<Vue>) =>
          // @ts-ignore
          select.vm.options.inputId === 'traveling-international'
      );
      // @ts-ignore
      expect(inputs.at(0).vm.options.inputErrors).toStrictEqual(expected);
    });
    it('should emit input event to parent when change event is triggered on input', () => {
      // given
      const gbCompanyCreateHeaderWrapper: Wrapper<Vue> = mount(GbJobOfferCreateTraveling, { mocks: { $t }, propsData });

      const inputRadioField: Wrapper<Vue> = gbCompanyCreateHeaderWrapper.find('#traveling-international_1');
      const inputValue: TravelingEnums.TravelingFrequency = TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL;

      // when
      inputRadioField.trigger('change');

      // then
      const expected: PutTravelingRequestInterface = {
        isTraveling: true,
        international: inputValue,
      };

      expect(gbCompanyCreateHeaderWrapper.emitted().input).toStrictEqual([[expected]]);
    });
  });
});

import { mount, Stubs, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBlock from '~/components/base-components/gb-block.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import GbInputDate from '~/components/base-components/gb-input-date.vue';
import {
  DomainErrorsResponseInterface,
  DomainFieldErrorsResponseInterface,
} from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbJobOfferCreateEndDateOfApplication from '../gb-job-offer-create-end-date-of-application.vue';

describe('components/job-offer/create/gb-job-offer-create-end-date-of-application', () => {
  let $t: jest.Mock;
  let stubs: Stubs;
  let propsData: {
    value: Date;
    options: { domainErrors?: DomainErrorsResponseInterface };
  };
  let fixedDate: Date;
  // @ts-ignore
  let dateSpy: SpyInstance;

  beforeEach(() => {
    $t = jest.fn();
    stubs = { 'client-only': true, 'date-picker': true };
    propsData = {
      value: new Date() as Date,
      options: {},
    };

    fixedDate = new Date('2017-06-13T04:41:20');
    dateSpy = jest.spyOn(global, 'Date');
    dateSpy.mockImplementation(() => fixedDate);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('label', () => {
    it('should have a GbBlock with label in header', () => {
      // given
      const expected: string = 'End date of application';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateEndDateOfApplicationWrapper: Wrapper<Vue> = mount(GbJobOfferCreateEndDateOfApplication, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateEndDateOfApplicationWrapper.find(GbBlock);

      // then
      expect(result.find('.gb-block--header').text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(1, 'jobOffer.endDateOfApplicationLabel');
    });
  });
  describe('end date of application', () => {
    it('should contain a GbIcon with options', () => {
      // given
      const expected: object = { iconName: 'calendar_blue' };
      const gbJobOfferCreateEndDateOfApplicationWrapper: Wrapper<Vue> = mount(GbJobOfferCreateEndDateOfApplication, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // when
      const result: WrapperArray<Vue> = gbJobOfferCreateEndDateOfApplicationWrapper.findAll(GbIcon);

      // then
      // @ts-ignore
      expect(result.at(0).vm.options).toStrictEqual(expected);
    });
    it('should display GbInputDate component with props for job offer minimum salary', () => {
      // given
      const expected: object = {
        inputId: 'end-date-of-application',
        inputRequired: false,
        inputMin: fixedDate,
        inputPattern: 'yyyy-MM-dd',
        inputPlaceholderKey: 'jobOffer.endDateOfApplicationPlaceholder',
        inputIsLarge: true,
        inputErrors: undefined,
      };

      // when
      const gbJobOfferCreateEndDateOfApplicationWrapper: Wrapper<Vue> = mount(GbJobOfferCreateEndDateOfApplication, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // then
      const inputDate: Wrapper<Vue> = gbJobOfferCreateEndDateOfApplicationWrapper.find(GbInputDate);

      // @ts-ignore
      expect(inputDate.vm.options).toStrictEqual(expected);
    });

    it('should bind job offer end date of application to GbInputDate input', () => {
      // given
      const endDateOfApplication: Date = new Date('2019-10-23T00:00:00.000Z');
      propsData.value = endDateOfApplication;

      // when
      const gbJobOfferCreateEndDateOfApplicationWrapper: Wrapper<Vue> = mount(GbJobOfferCreateEndDateOfApplication, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // then
      const inputDate: Wrapper<Vue> = gbJobOfferCreateEndDateOfApplicationWrapper.find(GbInputDate);

      // @ts-ignore
      expect(inputDate.vm.value).toStrictEqual(endDateOfApplication);
    });
    it('should set inputErrors when there is a domain errors for this field', async () => {
      // given
      const expected: DomainFieldErrorsResponseInterface = {
        messages: ['A end date of application error'],
        i18nMessageKeys: ['jobOffer.aEndDateOfApplicationError'],
      };
      propsData.options = { domainErrors: { endDateOfApplication: expected } };
      const gbJobOfferCreateEndDateOfApplicationWrapper: Wrapper<Vue> = mount(GbJobOfferCreateEndDateOfApplication, {
        stubs,
        mocks: { $t },
        propsData,
      });

      // then
      const inputDate: Wrapper<Vue> = gbJobOfferCreateEndDateOfApplicationWrapper.find(GbInputDate);

      // @ts-ignore
      expect(inputDate.vm.options.inputErrors).toStrictEqual(expected);
    });

    it('should bind input value to parent model when input event is triggered', () => {
      // given
      const endDateOfApplication: Date = new Date('2019-10-23T00:00:00.000Z');
      propsData.value = endDateOfApplication;
      const gbJobOfferCreateEndDateOfApplicationWrapper: Wrapper<Vue> = mount(GbJobOfferCreateEndDateOfApplication, {
        stubs,
        mocks: { $t },
        propsData,
      });

      const inputField: Wrapper<Vue> = gbJobOfferCreateEndDateOfApplicationWrapper.find('date-picker-stub');

      // when
      inputField.vm.$emit('input', endDateOfApplication);

      // then
      expect(gbJobOfferCreateEndDateOfApplicationWrapper.emitted().input).toStrictEqual([[endDateOfApplication]]);
    });
  });
});

import { mount, Wrapper, WrapperArray } from '@vue/test-utils';
import Vue from 'vue';
import GbBlock from '~/components/base-components/gb-block.vue';
import { JobOfferEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { DomainErrorsResponseInterface } from '../../../../../back/src/infrastructure/rest/filters/models/domain-validation-error-response.interface';
import GbJobOfferCreateWantedPersonalities from '../gb-job-offer-create-wanted-personalities.vue';
import GbJobOfferCreateWantedPersonalityButton from '../gb-job-offer-create-wanted-personality-button.vue';

describe('components/job-offer/create/gb-job-offer-create-wanted-personalities', () => {
  let $t: jest.Mock;
  let propsData: {
    value: JobOfferEnums.JobOfferWantedPersonality[];
    options: { domainErrors?: DomainErrorsResponseInterface };
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      value: [],
      options: {},
    };
  });

  describe('label', () => {
    it('should have a GbBlock with label in header', () => {
      // given
      const expected: string = 'Wanted personality';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateWantedPersonalityWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalities, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateWantedPersonalityWrapper.find(GbBlock);

      // then
      expect(result.find('.gb-block--header').text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(1, 'jobOffer.wantedPersonalitiesLabel');
    });
  });

  describe('subheader', () => {
    it('should have a GbBlock with explanation in subheader', () => {
      // given
      const expected: string = 'Wanted personality subheader';
      $t.mockReturnValue(expected);
      const gbJobOfferCreateWantedPersonalityWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalities, { mocks: { $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferCreateWantedPersonalityWrapper.find(GbBlock);

      // then
      expect(result.find('.gb-block--subheader').text()).toStrictEqual(expected);
      expect($t).toHaveBeenNthCalledWith(2, 'jobOffer.wantedPersonalitiesExplanation');
    });
  });

  describe('content', () => {
    it('should have as many GbJobOfferCreateWantedPersonalityButton elements than array configured in computed', () => {
      // given
      const gbJobOfferCreateWantedPersonalityWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalities, { mocks: { $t }, propsData });

      // when
      const result: WrapperArray<Vue> = gbJobOfferCreateWantedPersonalityWrapper.findAll(GbJobOfferCreateWantedPersonalityButton);

      // then
      // @ts-ignore
      expect(result.length).toBe(gbJobOfferCreateWantedPersonalityWrapper.vm.wantedPersonalityOptions.length);
    });

    it('should bind wanted personality prop to GbJobOfferCreateWantedPersonalityButton input prop with true value for ones in value prop of parent', () => {
      // given
      propsData.value = [
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AUTONOMOUS,
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS,
      ];
      const gbJobOfferCreateWantedPersonalityWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalities, { mocks: { $t }, propsData });

      // when
      const results: WrapperArray<Vue> = gbJobOfferCreateWantedPersonalityWrapper.findAll(GbJobOfferCreateWantedPersonalityButton);

      // then
      // @ts-ignore
      expect(results.at(2).vm.value).toBe(true);
      // @ts-ignore
      expect(results.at(4).vm.value).toBe(true);
    });

    it('should bind wanted personality prop to GbJobOfferCreateWantedPersonalityButton input prop with false value for ones not in value prop of parent', () => {
      // given
      propsData.value = [
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AUTONOMOUS,
        JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS,
      ];
      const gbJobOfferCreateWantedPersonalityWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalities, { mocks: { $t }, propsData });

      // when
      const results: WrapperArray<Vue> = gbJobOfferCreateWantedPersonalityWrapper.findAll(GbJobOfferCreateWantedPersonalityButton);

      // then
      // @ts-ignore
      expect(results.at(0).vm.value).toBe(false);
      // @ts-ignore
      expect(results.at(1).vm.value).toBe(false);
      // @ts-ignore
      expect(results.at(3).vm.value).toBe(false);
    });

    it('should emit input event when input is triggered from child', async () => {
      // given
      const gbJobOfferCreateWantedPersonalityWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalities, { mocks: { $t }, propsData });

      const gbJobOfferCreateTravelingWrappers: WrapperArray<Vue> = gbJobOfferCreateWantedPersonalityWrapper.findAll(
        GbJobOfferCreateWantedPersonalityButton
      );
      const inputExpected1: JobOfferEnums.JobOfferWantedPersonality = JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS;
      const inputExpected2: JobOfferEnums.JobOfferWantedPersonality = JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS;

      // when
      gbJobOfferCreateTravelingWrappers.at(0).vm.$emit('input', true);
      gbJobOfferCreateWantedPersonalityWrapper.setProps({ value: [inputExpected1] });

      gbJobOfferCreateTravelingWrappers.at(1).vm.$emit('input', true);

      // then
      expect(gbJobOfferCreateWantedPersonalityWrapper.emitted().input[0]).toStrictEqual([[inputExpected1]]);
      expect(gbJobOfferCreateWantedPersonalityWrapper.emitted().input[1]).toStrictEqual([[inputExpected1, inputExpected2]]);
    });

    it('should emit input event when input is triggered from child', () => {
      // given
      const gbJobOfferCreateWantedPersonalityWrapper: Wrapper<Vue> = mount(GbJobOfferCreateWantedPersonalities, { mocks: { $t }, propsData });

      const gbJobOfferCreateTravelingWrappers: WrapperArray<Vue> = gbJobOfferCreateWantedPersonalityWrapper.findAll(
        GbJobOfferCreateWantedPersonalityButton
      );
      const inputExpected1: JobOfferEnums.JobOfferWantedPersonality = JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS;
      const inputExpected2: JobOfferEnums.JobOfferWantedPersonality = JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS;

      // when
      gbJobOfferCreateTravelingWrappers.at(0).vm.$emit('input', true);
      gbJobOfferCreateWantedPersonalityWrapper.setProps({ value: [inputExpected1] });

      gbJobOfferCreateTravelingWrappers.at(1).vm.$emit('input', true);
      gbJobOfferCreateWantedPersonalityWrapper.setProps({ value: [inputExpected1, inputExpected2] });

      gbJobOfferCreateTravelingWrappers.at(0).vm.$emit('input', true);

      // then
      expect(gbJobOfferCreateWantedPersonalityWrapper.emitted().input[0]).toStrictEqual([[inputExpected1]]);
      expect(gbJobOfferCreateWantedPersonalityWrapper.emitted().input[1]).toStrictEqual([[inputExpected1, inputExpected2]]);
      expect(gbJobOfferCreateWantedPersonalityWrapper.emitted().input[2]).toStrictEqual([[inputExpected2]]);
    });
  });
});

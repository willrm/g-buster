import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbSelectMultiple from '~/components/base-components/gb-select-multiple.vue';
import GbJobOfferSearch from '~/components/job-offer/search/gb-job-offer-search.vue';
import { JobOfferEnums } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';

describe('components/job-offer/search/gb-job-offer-search', () => {
  let propsData: {
    value: {
      requestedSpecialities: JobOfferEnums.JobOfferRequestedSpeciality[];
      jobTypes: JobOfferEnums.JobOfferJobType[];
      requiredExperiences: JobOfferEnums.JobOfferRequiredExperience[];
      regions: JobOfferEnums.JobOfferRegion[];
    };
  };

  beforeEach(() => {
    propsData = {
      value: {
        requestedSpecialities: [],
        jobTypes: [],
        requiredExperiences: [],
        regions: [],
      },
    };
  });

  describe('requestedSpecialities', () => {
    it('should display GbSelectMultiple component with existing requestedSpecialities value', () => {
      // given
      const expected: JobOfferEnums.JobOfferRequestedSpeciality[] = [
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION,
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
      ];
      propsData.value.requestedSpecialities = expected;

      // when
      const gbJobOfferSearchWrapper: Wrapper<Vue> = shallowMount(GbJobOfferSearch, { propsData });

      // then
      // @ts-ignore
      expect(findGbSelectMultipleById(gbJobOfferSearchWrapper, 'filter-requested-specialities').vm.value).toStrictEqual(expected);
    });

    it('should display GbSelectMultiple component with props for job offer requestedSpecialities', () => {
      // given
      const expected: object = {
        selectId: 'filter-requested-specialities',
        selectPlaceholderKey: 'jobOffer.requestedSpecialities',
        selectOptions: [
          {
            optionLabelKey: 'jobOffer.requestedSpecialityAeronauticsAerospace',
            optionValue: 'REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityAgriculture',
            optionValue: 'REQUESTED_SPECIALITY_AGRICULTURE',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityNoneInParticular',
            optionValue: 'REQUESTED_SPECIALITY_NONE_IN_PARTICULAR',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityOtherSpeciality',
            optionValue: 'REQUESTED_SPECIALITY_OTHER_SPECIALITY',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityBiomedical',
            optionValue: 'REQUESTED_SPECIALITY_BIOMEDICAL',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityBiotechnology',
            optionValue: 'REQUESTED_SPECIALITY_BIOTECHNOLOGY',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityChemistry',
            optionValue: 'REQUESTED_SPECIALITY_CHEMISTRY',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityCivil',
            optionValue: 'REQUESTED_SPECIALITY_CIVIL',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityConstruction',
            optionValue: 'REQUESTED_SPECIALITY_CONSTRUCTION',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityDocumentation',
            optionValue: 'REQUESTED_SPECIALITY_DOCUMENTATION',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityElectrical',
            optionValue: 'REQUESTED_SPECIALITY_ELECTRICAL',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityEnvironment',
            optionValue: 'REQUESTED_SPECIALITY_ENVIRONMENT',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityGeology',
            optionValue: 'REQUESTED_SPECIALITY_GEOLOGY',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityQualityManagement',
            optionValue: 'REQUESTED_SPECIALITY_QUALITY_MANAGEMENT',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityIndustrialManufacturer',
            optionValue: 'REQUESTED_SPECIALITY_INDUSTRIAL_MANUFACTURER',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityInformationTechnology',
            optionValue: 'REQUESTED_SPECIALITY_INFORMATION_TECHNOLOGY',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityInstrumentation',
            optionValue: 'REQUESTED_SPECIALITY_INSTRUMENTATION',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialitySoftware',
            optionValue: 'REQUESTED_SPECIALITY_SOFTWARE',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityMaterialHandlingDistribution',
            optionValue: 'REQUESTED_SPECIALITY_MATERIAL_HANDLING_DISTRIBUTION',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityMaritimeNaval',
            optionValue: 'REQUESTED_SPECIALITY_MARITIME_NAVAL',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityNonMetallicMaterials',
            optionValue: 'REQUESTED_SPECIALITY_NON_METALLIC_MATERIALS',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityMechanics',
            optionValue: 'REQUESTED_SPECIALITY_MECHANICS',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityMetallurgyMetals',
            optionValue: 'REQUESTED_SPECIALITY_METALLURGY_METALS',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityMining',
            optionValue: 'REQUESTED_SPECIALITY_MINING',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityNuclearPower',
            optionValue: 'REQUESTED_SPECIALITY_NUCLEAR_POWER',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityPulpAndPaper',
            optionValue: 'REQUESTED_SPECIALITY_PULP_AND_PAPER',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityPhysical',
            optionValue: 'REQUESTED_SPECIALITY_PHYSICAL',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityAutomatedProduction',
            optionValue: 'REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityRobotics',
            optionValue: 'REQUESTED_SPECIALITY_ROBOTICS',
          },
          {
            optionLabelKey: 'jobOffer.requestedSpecialityOccupationalHealthAndSafety',
            optionValue: 'REQUESTED_SPECIALITY_OCCUPATIONAL_HEALTH_AND_SAFETY',
          },
        ],
        selectIsLarge: true,
        selectBoxColor: 'B2C_COLOR',
      };

      // when
      const gbJobOfferSearchWrapper: Wrapper<Vue> = shallowMount(GbJobOfferSearch, { propsData });

      // then
      // @ts-ignore
      expect(findGbSelectMultipleById(gbJobOfferSearchWrapper, expected.selectId).vm.options).toStrictEqual(expected);
    });

    it('should emit input event with selected requestedSpecialities when input event from GbSelectMultiple ', async () => {
      // given
      const expected: JobOfferEnums.JobOfferRequestedSpeciality[] = [
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION,
        JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
      ];
      const gbJobOfferSearchWrapper: Wrapper<Vue> = shallowMount(GbJobOfferSearch, { propsData });

      // when
      findGbSelectMultipleById(gbJobOfferSearchWrapper, 'filter-requested-specialities').vm.$emit('input', expected);
      await gbJobOfferSearchWrapper.vm.$nextTick();

      // then
      // @ts-ignore
      expect(gbJobOfferSearchWrapper.emitted().input).toStrictEqual([[{ fieldName: 'requestedSpecialities', fieldValue: expected }]]);
    });
  });

  describe('jobTypes', () => {
    it('should display GbSelectMultiple component with existing jobTypes value', () => {
      // given
      const expected: JobOfferEnums.JobOfferJobType[] = [
        JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR,
        JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP,
      ];
      propsData.value.jobTypes = expected;

      // when
      const gbJobOfferSearchWrapper: Wrapper<Vue> = shallowMount(GbJobOfferSearch, { propsData });

      // then
      // @ts-ignore
      expect(findGbSelectMultipleById(gbJobOfferSearchWrapper, 'filter-job-types').vm.value).toStrictEqual(expected);
    });

    it('should display GbSelectMultiple component with props for job offer jobTypes', () => {
      // given
      const expected: object = {
        selectId: 'filter-job-types',
        selectPlaceholderKey: 'jobOffer.jobType',
        selectOptions: [
          {
            optionLabelKey: 'jobOffer.jobTypeRegular',
            optionValue: 'JOB_TYPE_REGULAR',
          },
          {
            optionLabelKey: 'jobOffer.jobTypeInternship',
            optionValue: 'JOB_TYPE_INTERNSHIP',
          },
          {
            optionLabelKey: 'jobOffer.jobTypeTemporary',
            optionValue: 'JOB_TYPE_TEMPORARY',
          },
        ],
        selectIsLarge: true,
        selectBoxColor: 'B2C_COLOR',
      };

      // when
      const gbJobOfferSearchWrapper: Wrapper<Vue> = shallowMount(GbJobOfferSearch, { propsData });

      // then
      // @ts-ignore
      expect(findGbSelectMultipleById(gbJobOfferSearchWrapper, expected.selectId).vm.options).toStrictEqual(expected);
    });

    it('should emit input event with selected jobTypes when input event from GbSelectMultiple ', async () => {
      // given
      const expected: JobOfferEnums.JobOfferJobType[] = [
        JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR,
        JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP,
      ];
      const gbJobOfferSearchWrapper: Wrapper<Vue> = shallowMount(GbJobOfferSearch, { propsData });

      // when
      findGbSelectMultipleById(gbJobOfferSearchWrapper, 'filter-job-types').vm.$emit('input', expected);
      await gbJobOfferSearchWrapper.vm.$nextTick();

      // then
      // @ts-ignore
      expect(gbJobOfferSearchWrapper.emitted().input).toStrictEqual([[{ fieldName: 'jobTypes', fieldValue: expected }]]);
    });
  });

  describe('requiredExperiences', () => {
    it('should display GbSelectMultiple component with existing requiredExperiences value', () => {
      // given
      const expected: JobOfferEnums.JobOfferRequiredExperience[] = [
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
      ];
      propsData.value.requiredExperiences = expected;

      // when
      const gbJobOfferSearchWrapper: Wrapper<Vue> = shallowMount(GbJobOfferSearch, { propsData });

      // then
      // @ts-ignore
      expect(findGbSelectMultipleById(gbJobOfferSearchWrapper, 'filter-required-experiences').vm.value).toStrictEqual(expected);
    });

    it('should display GbSelectMultiple component with props for job offer requiredExperiences', () => {
      // given
      const expected: object = {
        selectId: 'filter-required-experiences',
        selectPlaceholderKey: 'jobOffer.requiredExperiences',
        selectOptions: [
          {
            optionLabelKey: 'jobOffer.requiredExperienceZeroToTwoYears',
            optionValue: 'REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS',
          },
          {
            optionLabelKey: 'jobOffer.requiredExperienceThreeToFiveYears',
            optionValue: 'REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS',
          },
          {
            optionLabelKey: 'jobOffer.requiredExperienceSixToTenYears',
            optionValue: 'REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS',
          },
          {
            optionLabelKey: 'jobOffer.requiredExperienceElevenAndMore',
            optionValue: 'REQUIRED_EXPERIENCE_ELEVEN_AND_MORE',
          },
        ],
        selectIsLarge: true,
        selectBoxColor: 'B2C_COLOR',
      };

      // when
      const gbJobOfferSearchWrapper: Wrapper<Vue> = shallowMount(GbJobOfferSearch, { propsData });

      // then
      // @ts-ignore
      expect(findGbSelectMultipleById(gbJobOfferSearchWrapper, expected.selectId).vm.options).toStrictEqual(expected);
    });

    it('should emit input event with selected requiredExperiences when input event from GbSelectMultiple ', async () => {
      // given
      const expected: JobOfferEnums.JobOfferRequiredExperience[] = [
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
        JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
      ];
      const gbJobOfferSearchWrapper: Wrapper<Vue> = shallowMount(GbJobOfferSearch, { propsData });

      // when
      findGbSelectMultipleById(gbJobOfferSearchWrapper, 'filter-required-experiences').vm.$emit('input', expected);
      await gbJobOfferSearchWrapper.vm.$nextTick();

      // then
      // @ts-ignore
      expect(gbJobOfferSearchWrapper.emitted().input).toStrictEqual([[{ fieldName: 'requiredExperiences', fieldValue: expected }]]);
    });
  });

  describe('regions', () => {
    it('should display GbSelectMultiple component with existing regions value', () => {
      // given
      const expected: JobOfferEnums.JobOfferRegion[] = [
        JobOfferEnums.JobOfferRegion.REGION_CENTRE_DU_QUEBEC,
        JobOfferEnums.JobOfferRegion.REGION_GASPESIE_ILES_DE_LA_MADELEINE,
      ];
      propsData.value.regions = expected;

      // when
      const gbJobOfferSearchWrapper: Wrapper<Vue> = shallowMount(GbJobOfferSearch, { propsData });

      // then
      // @ts-ignore
      expect(findGbSelectMultipleById(gbJobOfferSearchWrapper, 'filter-regions').vm.value).toStrictEqual(expected);
    });

    it('should display GbSelectMultiple component with props for job offer regions', () => {
      // given
      const expected: object = {
        selectId: 'filter-regions',
        selectPlaceholderKey: 'jobOffer.region',
        selectOptions: [
          {
            optionLabelKey: 'jobOffer.regionAbitibiTemiscamingue',
            optionValue: 'REGION_ABITIBI_TEMISCAMINGUE',
          },
          {
            optionLabelKey: 'jobOffer.regionBasStLaurent',
            optionValue: 'REGION_BAS_ST_LAURENT',
          },
          {
            optionLabelKey: 'jobOffer.regionCapitaleNationale',
            optionValue: 'REGION_CAPITALE_NATIONALE',
          },
          {
            optionLabelKey: 'jobOffer.regionCentreDuQuebec',
            optionValue: 'REGION_CENTRE_DU_QUEBEC',
          },
          {
            optionLabelKey: 'jobOffer.regionChaudieresAppalaches',
            optionValue: 'REGION_CHAUDIERES_APPALACHES',
          },
          {
            optionLabelKey: 'jobOffer.regionCoteNord',
            optionValue: 'REGION_COTE_NORD',
          },
          {
            optionLabelKey: 'jobOffer.regionEstrie',
            optionValue: 'REGION_ESTRIE',
          },
          {
            optionLabelKey: 'jobOffer.regionGaspesieIlesDeLaMadeleine',
            optionValue: 'REGION_GASPESIE_ILES_DE_LA_MADELEINE',
          },
          {
            optionLabelKey: 'jobOffer.regionLanaudiere',
            optionValue: 'REGION_LANAUDIERE',
          },
          {
            optionLabelKey: 'jobOffer.regionLaurentides',
            optionValue: 'REGION_LAURENTIDES',
          },
          {
            optionLabelKey: 'jobOffer.regionLaval',
            optionValue: 'REGION_LAVAL',
          },
          {
            optionLabelKey: 'jobOffer.regionMauricie',
            optionValue: 'REGION_MAURICIE',
          },
          {
            optionLabelKey: 'jobOffer.regionMonteregie',
            optionValue: 'REGION_MONTEREGIE',
          },
          {
            optionLabelKey: 'jobOffer.regionMontreal',
            optionValue: 'REGION_MONTREAL',
          },
          {
            optionLabelKey: 'jobOffer.regionNordDuQuebec',
            optionValue: 'REGION_NORD_DU_QUEBEC',
          },
          {
            optionLabelKey: 'jobOffer.regionOutaouais',
            optionValue: 'REGION_OUTAOUAIS',
          },
          {
            optionLabelKey: 'jobOffer.regionSaguenayLacStJean',
            optionValue: 'REGION_SAGUENAY_LAC_ST_JEAN',
          },
        ],
        selectIsLarge: true,
        selectBoxColor: 'B2C_COLOR',
      };

      // when
      const gbJobOfferSearchWrapper: Wrapper<Vue> = shallowMount(GbJobOfferSearch, { propsData });

      // then
      // @ts-ignore
      expect(findGbSelectMultipleById(gbJobOfferSearchWrapper, expected.selectId).vm.options).toStrictEqual(expected);
    });

    it('should emit input event with selected regions when input event from GbSelectMultiple ', async () => {
      // given
      const expected: JobOfferEnums.JobOfferRegion[] = [
        JobOfferEnums.JobOfferRegion.REGION_CENTRE_DU_QUEBEC,
        JobOfferEnums.JobOfferRegion.REGION_GASPESIE_ILES_DE_LA_MADELEINE,
      ];
      const gbJobOfferSearchWrapper: Wrapper<Vue> = shallowMount(GbJobOfferSearch, { propsData });

      // when
      findGbSelectMultipleById(gbJobOfferSearchWrapper, 'filter-regions').vm.$emit('input', expected);
      await gbJobOfferSearchWrapper.vm.$nextTick();

      // then
      // @ts-ignore
      expect(gbJobOfferSearchWrapper.emitted().input).toStrictEqual([[{ fieldName: 'regions', fieldValue: expected }]]);
    });
  });
});
const findGbSelectMultipleById = (wrapper: Wrapper<Vue>, expectedSelectId: string): Wrapper<Vue> => {
  return wrapper
    .findAll(GbSelectMultiple)
    .filter(
      (select: Wrapper<Vue>) =>
        // @ts-ignore
        select.vm.options.selectId === expectedSelectId
    )
    .at(0);
};

import { mount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbElementWithIcon from '~/components/base-components/gb-element-with-icon.vue';
import GbIcon from '~/components/base-components/gb-icon.vue';
import { JobOfferEnums } from '../../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import { JobOfferWithCompanyResponseInterface } from '../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import GbJobOfferHighlight from '../gb-job-offer-highlight.vue';

describe('components/job-offer/gb-job-offer-highlight', () => {
  let $t: jest.Mock;
  let $d: jest.Mock;

  let propsData: {
    jobOffer: JobOfferWithCompanyResponseInterface;
  };

  beforeEach(() => {
    $t = jest.fn();
    $d = jest.fn();

    propsData = {
      jobOffer: {} as JobOfferWithCompanyResponseInterface,
    };
  });

  describe('published at date', () => {
    it('should display job offer published at date in format d MMMM', () => {
      // given
      const expected: string = '23 janvier';
      $d.mockReturnValue(expected);

      const fixedDate: Date = new Date(2019, 0, 23);
      propsData.jobOffer.publishedAt = fixedDate;

      const gbJobOfferHighlightWrapper: Wrapper<Vue> = mount(GbJobOfferHighlight, { mocks: { $d, $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferHighlightWrapper.find('.gb-job-offer-highlight--published-at');

      // then
      expect($d).toHaveBeenCalledWith(fixedDate, 'short');
      expect(result.text()).toContain(expected);
    });

    it('should contain a GbElementWithIcon for published at date', () => {
      $d.mockReturnValue('23 janvier');
      propsData.jobOffer.publishedAt = new Date(2019, 0, 23);

      const gbJobOfferHighlightWrapper: Wrapper<Vue> = mount(GbJobOfferHighlight, { mocks: { $d, $t }, propsData });

      // when
      const elementWithIcon: Wrapper<Vue> = gbJobOfferHighlightWrapper.findAll(GbElementWithIcon).at(0);

      // then
      // @ts-ignore
      expect(elementWithIcon.vm.options).toStrictEqual({
        inputWithIconPosition: 'OUTSIDE',
      });
    });

    it('should contain a GbIcon for published at date', () => {
      // given
      $d.mockReturnValue('23 janvier');
      propsData.jobOffer.publishedAt = new Date(2019, 0, 23);

      const gbJobOfferHighlightWrapper: Wrapper<Vue> = mount(GbJobOfferHighlight, { mocks: { $d, $t }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferHighlightWrapper.findAll(GbIcon).at(0);

      // then
      // @ts-ignore
      expect(result.vm.options).toStrictEqual({
        iconName: 'clock_grey',
      });
    });
  });

  describe('job status/job type', () => {
    it('should display job offer type', () => {
      // given
      const expected: string = 'A job status / A job type';

      $t.mockImplementation((domainKey: string) => {
        if ('jobOffer.jobTypeRegular' === domainKey) {
          return 'A job type';
        } else if ('jobOffer.jobStatusFullTime' === domainKey) {
          return 'A job status';
        } else {
          return 'unexpected';
        }
      });

      propsData.jobOffer.jobType = JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR;
      propsData.jobOffer.jobStatus = JobOfferEnums.JobOfferJobStatus.JOB_STATUS_FULL_TIME;

      const gbJobOfferHighlightWrapper: Wrapper<Vue> = mount(GbJobOfferHighlight, { mocks: { $t, $d }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferHighlightWrapper.find('.gb-job-offer-highlight--job-type-and-status');

      // then

      expect($t).toHaveBeenCalledWith('jobOffer.jobTypeRegular');
      expect($t).toHaveBeenCalledWith('jobOffer.jobStatusFullTime');
      expect(result.text()).toBe(expected);
    });

    it('should contain a GbElementWithIcon for job type', () => {
      // given
      $t.mockReturnValue('A job type');

      propsData.jobOffer.jobType = JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR;

      const gbJobOfferHighlightWrapper: Wrapper<Vue> = mount(GbJobOfferHighlight, { mocks: { $t, $d }, propsData });

      // when
      const elementWithIcon: Wrapper<Vue> = gbJobOfferHighlightWrapper.findAll(GbElementWithIcon).at(1);

      // then
      // @ts-ignore
      expect(elementWithIcon.vm.options).toStrictEqual({
        inputWithIconPosition: 'OUTSIDE',
      });
    });

    it('should contain a GbIcon for job type', () => {
      // given
      $t.mockReturnValue('A job type');

      propsData.jobOffer.jobType = JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR;

      const gbJobOfferHighlightWrapper: Wrapper<Vue> = mount(GbJobOfferHighlight, { mocks: { $t, $d }, propsData });

      // when
      const elementWithIcon: Wrapper<Vue> = gbJobOfferHighlightWrapper.findAll(GbIcon).at(1);

      // then
      // @ts-ignore
      expect(elementWithIcon.vm.options).toStrictEqual({
        iconName: 'work_grey',
      });
    });
  });

  describe('job region', () => {
    it('should display job offer region', () => {
      // given
      const expected: string = 'Montreal';
      $t.mockReturnValue(expected);
      propsData.jobOffer.region = JobOfferEnums.JobOfferRegion.REGION_MONTREAL;

      const gbJobOfferHighlightWrapper: Wrapper<Vue> = mount(GbJobOfferHighlight, { mocks: { $t, $d }, propsData });

      // when
      const result: Wrapper<Vue> = gbJobOfferHighlightWrapper.find('.gb-job-offer-highlight--region');

      // then
      expect($t).toHaveBeenCalledWith('jobOffer.regionMontreal');
      expect(result.text()).toBe(expected);
    });

    it('should contain a GbElementWithIcon for region', () => {
      // given
      $t.mockReturnValue('Montreal');
      propsData.jobOffer.region = JobOfferEnums.JobOfferRegion.REGION_MONTREAL;

      const gbJobOfferHighlightWrapper: Wrapper<Vue> = mount(GbJobOfferHighlight, { mocks: { $t, $d }, propsData });

      // when
      const elementWithIcon: Wrapper<Vue> = gbJobOfferHighlightWrapper.findAll(GbElementWithIcon).at(2);

      // then
      // @ts-ignore
      expect(elementWithIcon.vm.options).toStrictEqual({
        inputWithIconPosition: 'OUTSIDE',
      });
    });

    it('should contain a GbIcon for region', () => {
      // given
      $t.mockReturnValue('Montreal');
      propsData.jobOffer.region = JobOfferEnums.JobOfferRegion.REGION_MONTREAL;

      const gbJobOfferHighlightWrapper: Wrapper<Vue> = mount(GbJobOfferHighlight, { mocks: { $t, $d }, propsData });

      // when
      const elementWithIcon: Wrapper<Vue> = gbJobOfferHighlightWrapper.findAll(GbIcon).at(2);

      // then
      // @ts-ignore
      expect(elementWithIcon.vm.options).toStrictEqual({
        iconName: 'address_grey',
      });
    });
  });
});

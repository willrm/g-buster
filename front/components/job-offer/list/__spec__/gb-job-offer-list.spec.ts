import { shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbJobOfferListItem from '~/components/job-offer/list/gb-job-offer-list-item.vue';
import GbJobOfferList from '~/components/job-offer/list/gb-job-offer-list.vue';
import { JobOfferWithCompanyResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';

describe('components/job-offers/gb-job-offer-list', () => {
  let $t: jest.Mock;
  let propsData: {
    jobOffers: JobOfferWithCompanyResponseInterface[];
    isLoggedAsCandidate: boolean;
  };

  beforeEach(() => {
    $t = jest.fn();
    propsData = {
      jobOffers: [],
      isLoggedAsCandidate: false,
    };
  });

  it('should contain as many gb job offers table row components as job offers', () => {
    // given
    propsData.jobOffers = [
      {
        title: 'A first job offer',
      } as JobOfferWithCompanyResponseInterface,
      {
        title: 'A second job offer',
      } as JobOfferWithCompanyResponseInterface,
    ];
    propsData.isLoggedAsCandidate = true;

    // when
    const gbJobOfferListWrapper: Wrapper<Vue> = shallowMount(GbJobOfferList, {
      mocks: { $t },
      propsData,
    });

    // then
    // @ts-ignore
    expect(gbJobOfferListWrapper.findAll(GbJobOfferListItem).at(0).vm.jobOffer).toStrictEqual({
      title: 'A first job offer',
    });
    // @ts-ignore
    expect(gbJobOfferListWrapper.findAll(GbJobOfferListItem).at(0).vm.isLoggedAsCandidate).toBe(true);

    // @ts-ignore
    expect(gbJobOfferListWrapper.findAll(GbJobOfferListItem).at(1).vm.jobOffer).toStrictEqual({
      title: 'A second job offer',
    });
    // @ts-ignore
    expect(gbJobOfferListWrapper.findAll(GbJobOfferListItem).at(1).vm.isLoggedAsCandidate).toBe(true);
  });

  it('should contain a div with gb offers rows', () => {
    // when
    const gbJobOfferListWrapper: Wrapper<Vue> = shallowMount(GbJobOfferList, {
      mocks: { $t },
      propsData,
    });

    // then
    const rows: Wrapper<Vue> = gbJobOfferListWrapper.find('.gb-job-offer-list');
    expect(rows.exists()).toBe(true);
  });
});

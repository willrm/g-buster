import { mount, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import GbCompanyLogo from '~/components/base-components/gb-company-logo.vue';
import GbJobOfferHighlight from '~/components/job-offer/gb-job-offer-highlight.vue';
import GbJobOfferListItem from '~/components/job-offer/list/gb-job-offer-list-item.vue';
import { CompanyResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/company-response.interface';
import { JobOfferWithCompanyResponseInterface } from '../../../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';

describe('components/job-offers/gb-job-offer-list-item', () => {
  let $t: jest.Mock;
  let $d: jest.Mock;
  let $router: object;
  let pushMock: jest.Mock;

  const company: CompanyResponseInterface = {
    name: 'Company name',
    logo: {
      base64: 'Image base64',
      mimeType: 'Image mime type',
    },
  } as CompanyResponseInterface;

  let propsData: {
    jobOffer: JobOfferWithCompanyResponseInterface;
    isLoggedAsCandidate: boolean;
  };

  beforeEach(() => {
    $t = jest.fn();
    $d = jest.fn();

    pushMock = jest.fn();
    $router = {
      push: pushMock,
    };

    propsData = {
      jobOffer: {
        title: 'A job offer title',
        company: company as CompanyResponseInterface,
      } as JobOfferWithCompanyResponseInterface,
      isLoggedAsCandidate: false,
    };
  });

  describe('company logo', () => {
    it('should contains gbCompanyLogo component', () => {
      // given
      propsData.isLoggedAsCandidate = true;

      // when
      const gbJobOfferListItemWrapper: Wrapper<Vue> = shallowMount(GbJobOfferListItem, {
        propsData,
        mocks: { $d, $t },
      });

      // then
      expect(gbJobOfferListItemWrapper.findAll(GbCompanyLogo).length).toBe(1);
    });

    it('should contain gbCompanyLogo called with jobOffer company data', () => {
      // given
      propsData.isLoggedAsCandidate = true;

      // when
      const gbJobOfferListItemWrapper: Wrapper<Vue> = shallowMount(GbJobOfferListItem, {
        propsData,
        mocks: { $d, $t },
      });
      const gbCompanyLogo: Wrapper<Vue> = gbJobOfferListItemWrapper.find(GbCompanyLogo);

      // then
      // @ts-ignore
      expect(gbCompanyLogo.vm.options.logo).toStrictEqual(propsData.jobOffer.company.logo);
      // @ts-ignore
      expect(gbCompanyLogo.vm.options.isLocked).toBe(!propsData.isLoggedAsCandidate);
      // @ts-ignore
      expect(gbCompanyLogo.vm.options.alt).toBe(company.name);
    });

    it('should contain gbCompanyLogo with property isLocked at false when user is logged as candidate', () => {
      // given
      propsData.isLoggedAsCandidate = true;

      // when
      const gbJobOfferListItemWrapper: Wrapper<Vue> = shallowMount(GbJobOfferListItem, {
        propsData,
        mocks: { $d, $t },
      });
      const gbCompanyLogo: Wrapper<Vue> = gbJobOfferListItemWrapper.find(GbCompanyLogo);

      // then
      // @ts-ignore
      expect(gbCompanyLogo.vm.options.isLocked).toBe(false);
    });

    it('should contain gbCompanyLogo with property isLocked at true when user is not logged as candidate', () => {
      // when
      const gbJobOfferListItemWrapper: Wrapper<Vue> = shallowMount(GbJobOfferListItem, {
        propsData,
        mocks: { $d, $t },
      });
      const gbCompanyLogo: Wrapper<Vue> = gbJobOfferListItemWrapper.find(GbCompanyLogo);

      // then
      // @ts-ignore
      expect(gbCompanyLogo.vm.options.isLocked).toBe(true);
    });
  });

  describe('highlight', () => {
    it('should contain a job offer highlight component', () => {
      // given
      propsData.jobOffer.title = 'A job offer title';

      // when
      const gbJobOfferListItemWrapper: Wrapper<Vue> = shallowMount(GbJobOfferListItem, {
        propsData,
        mocks: { $d, $t },
      });

      // then
      // @ts-ignore
      expect(gbJobOfferListItemWrapper.find(GbJobOfferHighlight).vm.jobOffer).toStrictEqual(propsData.jobOffer);
    });
  });

  it('should redirect to job offer page with slug on click', async () => {
    // given
    propsData.jobOffer.id = 'aJobOfferId';
    propsData.jobOffer.title = 'A job offer title';

    const gbJobOfferListItemWrapper: Wrapper<Vue> = mount(GbJobOfferListItem, { mocks: { $router, $t, $d }, propsData });

    // when
    gbJobOfferListItemWrapper.find('.gb-job-offer-list-item').trigger('click');
    await gbJobOfferListItemWrapper.vm.$nextTick();

    // then
    expect(pushMock).toHaveBeenCalledWith({ path: 'job-offers/aJobOfferId--a-job-offer-title' });
  });
});

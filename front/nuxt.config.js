export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'G-Buster',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#ff3300' },
  /*
   ** Global CSS
   */
  css: ['@/assets/scss/index.scss', 'vue-multiselect/dist/vue-multiselect.min.css'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/api-service.plugin' },
    { src: '~/plugins/i18n.plugin' },
    { src: '~/plugins/vue-js-modal.plugin', ssr: false },
    { src: '~/plugins/vue-upload-component.plugin', ssr: false },
    { src: '~/plugins/vuejs-datepicker.plugin', ssr: false },
    { src: '~/plugins/v-tooltip.plugin', ssr: false },
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module',
    '@nuxt/typescript-build',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    [
      'nuxt-i18n',
      {
        locales: ['fr-ca', 'en-ca'],
        defaultLocale: 'fr-ca',
        vuex: false,
        strategy: 'no_prefix',
        vueI18n: {
          fallbackLocale: 'fr-ca',
          dateTimeFormats: {
            'fr-ca': {
              short: {
                day: 'numeric',
                month: 'long',
              },
              medium: {
                day: 'numeric',
                month: 'long',
                year: 'numeric',
              },
              long: {
                year: 'numeric',
                month: 'short',
                day: 'numeric',
                weekday: 'short',
                hour: 'numeric',
                minute: 'numeric',
              },
            },
          },
          numberFormats: {
            'fr-ca': {
              currency: {
                style: 'currency',
                currency: 'CAD',
                maximumSignificantDigits: 1,
              },
            },
          },
        },
      },
    ],
    '@nuxtjs/toast',
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true,
    /**
     * baseURL is used in SSR mode and will be overridden if API_URL environment variable if present
     */
    baseURL: 'http://localhost:3001/',
    /**
     * browserBaseURL is used in client browser and will be proxified if API_URL environment variable is present (see proxy configuration below)
     */
    browserBaseURL: '/',
  },
  proxy: {
    /**
     * Configure proxy to dynamically route /api/** requests to API_URL if present
     */
    '/api': { target: process.env.API_URL || 'http://localhost:3001' },
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    // extend(config, ctx) {}
    loaders: {
      scss: { sourceMap: false },
    },
  },
  server: {
    port: process.env.PORT || 3000, // default: 3000
    host: process.env.HOST || 'localhost', // default: localhost
  },
  ignore: ['**/*.spec.*', 'test-stubs/*.*'],
  toast: {
    position: 'bottom-right',
  },
};

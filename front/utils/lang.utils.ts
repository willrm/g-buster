import { Locale } from '~/enums/locale.enum';
import { CommonTypeAliases } from '../../back/src/infrastructure/rest/common-models/common-type-aliases';

const DEFAULT_LANG: string = Locale.CANADIAN_FRENCH;
const WHITELISTED_LANGS: string[] = [Locale.CANADIAN_FRENCH, Locale.CANADIAN_ENGLISH];

export const sanitizeLang = (lang: string): CommonTypeAliases.Lang => {
  return WHITELISTED_LANGS.includes(lang) ? lang : DEFAULT_LANG;
};

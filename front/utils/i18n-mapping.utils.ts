import { CompanyEnums } from '../../back/src/infrastructure/rest/career/models/company-constraints-and-enums';
import { AdvantagesEnums, JobOfferEnums, TravelingEnums } from '../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';

export interface I18nMapping<T> {
  [key: string]: T;
}
export type I18nKey = string | undefined;

export const getI18nKey = <T>(i18nMapping: I18nMapping<T>, searchedValue: T): I18nKey => {
  for (const [key, value] of Object.entries(i18nMapping)) {
    if (value === searchedValue) {
      return key;
    }
  }

  return undefined;
};

export const COMPANY_REVENUE_I18N_MAPPING: I18nMapping<CompanyEnums.CompanyRevenue> = {
  'company.revenueLessThan100K': CompanyEnums.CompanyRevenue.REVENUE_LESS_THAN_100K,
  'company.revenueBetween100KAnd1M': CompanyEnums.CompanyRevenue.REVENUE_BETWEEN_100k_AND_1M,
  'company.revenueBetween1MAnd5M': CompanyEnums.CompanyRevenue.REVENUE_BETWEEN_1M_AND_5M,
  'company.revenueBetween5MAnd20M': CompanyEnums.CompanyRevenue.REVENUE_BETWEEN_5M_AND_20M,
  'company.revenueMoreThan20M': CompanyEnums.CompanyRevenue.REVENUE_MORE_THAN_20M,
};

export const COMPANY_BUSINESS_SEGMENT_I18N_MAPPING: I18nMapping<CompanyEnums.CompanyBusinessSegment> = {
  'company.businessSegmentAeronauticsAerospace': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_AERONAUTICS_AEROSPACE,
  'company.businessSegmentAgriculture': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_AGRICULTURE,
  'company.businessSegmentOtherSpeciality': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_OTHER_SPECIALITY,
  'company.businessSegmentBiomedical': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_BIOMEDICAL,
  'company.businessSegmentChemistry': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_CHEMISTRY,
  'company.businessSegmentCivil': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_CIVIL,
  'company.businessSegmentDocumentation': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_DOCUMENTATION,
  'company.businessSegmentElectrical': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_ELECTRICAL,
  'company.businessSegmentEnvironment': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_ENVIRONMENT,
  'company.businessSegmentGeology': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_GEOLOGY,
  'company.businessSegmentQualityManagement': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_QUALITY_MANAGEMENT,
  'company.businessSegmentIndustrialManufacturer': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_INDUSTRIAL_MANUFACTURER,
  'company.businessSegmentInformationTechnology': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_INFORMATION_TECHNOLOGY,
  'company.businessSegmentInstrumentation': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_INSTRUMENTATION,
  'company.businessSegmentMaterialHandlingDistribution': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_MATERIAL_HANDLING_DISTRIBUTION,
  'company.businessSegmentMaritimeNaval': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_MARITIME_NAVAL,
  'company.businessSegmentNonMetallicMaterials': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_NON_METALLIC_MATERIALS,
  'company.businessSegmentMechanics': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_MECHANICS,
  'company.businessSegmentMetallurgyMetals': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_METALLURGY_METALS,
  'company.businessSegmentMining': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_MINING,
  'company.businessSegmentNuclearPower': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_NUCLEAR_POWER,
  'company.businessSegmentPulpAndPaper': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_PULP_AND_PAPER,
  'company.businessSegmentPhysical': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_PHYSICAL,
  'company.businessSegmentRobotics': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_ROBOTICS,
  'company.businessSegmentOccupationalHealthAndSafety': CompanyEnums.CompanyBusinessSegment.BUSINESS_SEGMENT_OCCUPATIONAL_HEALTH_AND_SAFETY,
};

export const COMPANY_SIZE_I18N_MAPPING: I18nMapping<CompanyEnums.CompanySize> = {
  'company.zeroToNine': CompanyEnums.CompanySize.ZERO_TO_NINE,
  'company.tenToNineteen': CompanyEnums.CompanySize.TEN_TO_NINETEEN,
  'company.twentyToFortyNine': CompanyEnums.CompanySize.TWENTY_TO_FORTY_NINE,
  'company.fiftyToSeventyNine': CompanyEnums.CompanySize.FIFTY_TO_SEVENTY_NINE,
  'company.eightyToHundredNineteen': CompanyEnums.CompanySize.EIGHTY_TO_HUNDRED_NINETEEN,
  'company.hundredTwentyToTwoHundredNinetyNine': CompanyEnums.CompanySize.HUNDRED_TWENTY_TO_TWO_HUNDRED_NINETY_NINE,
  'company.moreThanThreeHundred': CompanyEnums.CompanySize.MORE_THAN_THREE_HUNDRED,
};

export const COMPANY_GENIUS_TYPES_I18N_MAPPING: I18nMapping<CompanyEnums.CompanyGeniusType> = {
  'company.geniusTypeMechanicalEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_MECHANICAL_ENGINEERING,
  'company.geniusTypeCivilEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_CIVIL_ENGINEERING,
  'company.geniusTypeBuildingEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_BUILDING_ENGINEERING,
  'company.geniusTypeConstructionEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_CONSTRUCTION_ENGINEERING,
  'company.geniusTypeElectricalEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_ELECTRICAL_ENGINEERING,
  'company.geniusTypeChemicalEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_CHEMICAL_ENGINEERING,
  'company.geniusTypeIndustrialEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_INDUSTRIAL_ENGINEERING,
  'company.geniusTypeComputerAndSoftwareEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_COMPUTER_AND_SOFTWARE_ENGINEERING,
  'company.geniusTypeGeologicalEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_GEOLOGICAL_ENGINEERING,
  'company.geniusTypeAutomatedProductionEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_AUTOMATED_PRODUCTION_ENGINEERING,
  'company.geniusTypeForestEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_FOREST_ENGINEERING,
  'company.geniusTypeMaterialsEngineeringAndMetallurgy': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_MATERIALS_ENGINEERING_AND_METALLURGY,
  'company.geniusTypePhysicalEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_PHYSICAL_ENGINEERING,
  'company.geniusTypeAerospaceEngineering': CompanyEnums.CompanyGeniusType.GENIUS_TYPE_AEROSPACE_ENGINEERING,
};

export const COMPANY_REVENUE_FOR_RD_I18N_MAPPING: I18nMapping<CompanyEnums.CompanyRevenueForRD> = {
  'company.revenueRdLessThan100K': CompanyEnums.CompanyRevenueForRD.REVENUE_RD_LESS_THAN_100K,
  'company.revenueRdBetween100KAnd1M': CompanyEnums.CompanyRevenueForRD.REVENUE_RD_BETWEEN_100k_AND_1M,
  'company.revenueRdBetween1MAnd5M': CompanyEnums.CompanyRevenueForRD.REVENUE_RD_BETWEEN_1M_AND_5M,
  'company.revenueRdBetween5MAnd20M': CompanyEnums.CompanyRevenueForRD.REVENUE_RD_BETWEEN_5M_AND_20M,
  'company.revenueRdMoreThan20M': CompanyEnums.CompanyRevenueForRD.REVENUE_RD_MORE_THAN_20M,
};

export const JOB_OFFER_JOB_STATUS_I18N_MAPPING: I18nMapping<JobOfferEnums.JobOfferJobStatus> = {
  'jobOffer.jobStatusFullTime': JobOfferEnums.JobOfferJobStatus.JOB_STATUS_FULL_TIME,
  'jobOffer.jobStatusPartTime': JobOfferEnums.JobOfferJobStatus.JOB_STATUS_PART_TIME,
};

export const JOB_OFFER_JOB_TYPE_I18N_MAPPING: I18nMapping<JobOfferEnums.JobOfferJobType> = {
  'jobOffer.jobTypeRegular': JobOfferEnums.JobOfferJobType.JOB_TYPE_REGULAR,
  'jobOffer.jobTypeInternship': JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP,
  'jobOffer.jobTypeTemporary': JobOfferEnums.JobOfferJobType.JOB_TYPE_TEMPORARY,
};

export const JOB_OFFER_REGION_I18N_MAPPING: I18nMapping<JobOfferEnums.JobOfferRegion> = {
  'jobOffer.regionAbitibiTemiscamingue': JobOfferEnums.JobOfferRegion.REGION_ABITIBI_TEMISCAMINGUE,
  'jobOffer.regionBasStLaurent': JobOfferEnums.JobOfferRegion.REGION_BAS_ST_LAURENT,
  'jobOffer.regionCapitaleNationale': JobOfferEnums.JobOfferRegion.REGION_CAPITALE_NATIONALE,
  'jobOffer.regionCentreDuQuebec': JobOfferEnums.JobOfferRegion.REGION_CENTRE_DU_QUEBEC,
  'jobOffer.regionChaudieresAppalaches': JobOfferEnums.JobOfferRegion.REGION_CHAUDIERES_APPALACHES,
  'jobOffer.regionCoteNord': JobOfferEnums.JobOfferRegion.REGION_COTE_NORD,
  'jobOffer.regionEstrie': JobOfferEnums.JobOfferRegion.REGION_ESTRIE,
  'jobOffer.regionGaspesieIlesDeLaMadeleine': JobOfferEnums.JobOfferRegion.REGION_GASPESIE_ILES_DE_LA_MADELEINE,
  'jobOffer.regionLanaudiere': JobOfferEnums.JobOfferRegion.REGION_LANAUDIERE,
  'jobOffer.regionLaurentides': JobOfferEnums.JobOfferRegion.REGION_LAURENTIDES,
  'jobOffer.regionLaval': JobOfferEnums.JobOfferRegion.REGION_LAVAL,
  'jobOffer.regionMauricie': JobOfferEnums.JobOfferRegion.REGION_MAURICIE,
  'jobOffer.regionMonteregie': JobOfferEnums.JobOfferRegion.REGION_MONTEREGIE,
  'jobOffer.regionMontreal': JobOfferEnums.JobOfferRegion.REGION_MONTREAL,
  'jobOffer.regionNordDuQuebec': JobOfferEnums.JobOfferRegion.REGION_NORD_DU_QUEBEC,
  'jobOffer.regionOutaouais': JobOfferEnums.JobOfferRegion.REGION_OUTAOUAIS,
  'jobOffer.regionSaguenayLacStJean': JobOfferEnums.JobOfferRegion.REGION_SAGUENAY_LAC_ST_JEAN,
};

export const JOB_OFFER_TARGET_CUSTOMER_I18N_MAPPING: I18nMapping<JobOfferEnums.JobOfferTargetCustomer> = {
  'jobOffer.targetCustomerEngineers': JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERS,
  'jobOffer.targetCustomerJuniorEngineersCep': JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_JUNIOR_ENGINEERS_CEP,
  'jobOffer.targetCustomerEngineeringGraduates': JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_ENGINEERING_GRADUATES,
  'jobOffer.targetCustomerStudentsAndGraduates': JobOfferEnums.JobOfferTargetCustomer.TARGET_CUSTOMER_STUDENTS_AND_GRADUATES,
};

export const JOB_OFFER_REQUIRED_EXPERIENCE_I18N_MAPPING: I18nMapping<JobOfferEnums.JobOfferRequiredExperience> = {
  'jobOffer.requiredExperienceZeroToTwoYears': JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ZERO_TO_TWO_YEARS,
  'jobOffer.requiredExperienceThreeToFiveYears': JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_THREE_TO_FIVE_YEARS,
  'jobOffer.requiredExperienceSixToTenYears': JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_SIX_TO_TEN_YEARS,
  'jobOffer.requiredExperienceElevenAndMore': JobOfferEnums.JobOfferRequiredExperience.REQUIRED_EXPERIENCE_ELEVEN_AND_MORE,
};

export const JOB_OFFER_POSITION_TYPE_I18N_MAPPING: I18nMapping<JobOfferEnums.JobOfferPositionType> = {
  'jobOffer.positionTypeAnalyst': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_ANALYST,
  'jobOffer.positionTypeOthers': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_OTHERS,
  'jobOffer.positionTypeProjectManager': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_PROJECT_MANAGER,
  'jobOffer.positionTypeHeadOfDepartment': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_HEAD_OF_DEPARTMENT,
  'jobOffer.positionTypeResearcher': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_RESEARCHER,
  'jobOffer.positionTypeDesigner': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_DESIGNER,
  'jobOffer.positionTypeAdvisor': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_ADVISOR,
  'jobOffer.positionTypeConsultant': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_CONSULTANT,
  'jobOffer.positionTypeCoordinator': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_COORDINATOR,
  'jobOffer.positionTypeManagingDirector': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_MANAGING_DIRECTOR,
  'jobOffer.positionTypeDirectorOfOperations': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_DIRECTOR_OF_OPERATIONS,
  'jobOffer.positionTypeDirectorDirector': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_DIRECTOR_DIRECTOR,
  'jobOffer.positionTypeDeveloperProgrammer': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_DEVELOPER_PROGRAMMER,
  'jobOffer.positionTypeForestryEngineer': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_FORESTRY_ENGINEER,
  'jobOffer.positionTypeJuniorEngineersCep': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_JUNIOR_ENGINEERS_CEP,
  'jobOffer.positionTypeEngineer': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_ENGINEER,
  'jobOffer.positionTypeProjectEngineer': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_PROJECT_ENGINEER,
  'jobOffer.positionTypeProfessor': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_PROFESSOR,
  'jobOffer.positionTypePresidentOfTheBoardOfDirectors': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_PRESIDENT_OF_THE_BOARD_OF_DIRECTORS,
  'jobOffer.positionTypeViceChairperson': JobOfferEnums.JobOfferPositionType.POSITION_TYPE_VICE_CHAIRPERSON,
};

export const JOB_OFFER_REQUESTED_SPECIALITY_I18N_MAPPING: I18nMapping<JobOfferEnums.JobOfferRequestedSpeciality> = {
  'jobOffer.requestedSpecialityAeronauticsAerospace': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AERONAUTICS_AEROSPACE,
  'jobOffer.requestedSpecialityAgriculture': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AGRICULTURE,
  'jobOffer.requestedSpecialityNoneInParticular': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NONE_IN_PARTICULAR,
  'jobOffer.requestedSpecialityOtherSpeciality': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OTHER_SPECIALITY,
  'jobOffer.requestedSpecialityBiomedical': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOMEDICAL,
  'jobOffer.requestedSpecialityBiotechnology': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_BIOTECHNOLOGY,
  'jobOffer.requestedSpecialityChemistry': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CHEMISTRY,
  'jobOffer.requestedSpecialityCivil': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CIVIL,
  'jobOffer.requestedSpecialityConstruction': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_CONSTRUCTION,
  'jobOffer.requestedSpecialityDocumentation': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_DOCUMENTATION,
  'jobOffer.requestedSpecialityElectrical': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ELECTRICAL,
  'jobOffer.requestedSpecialityEnvironment': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ENVIRONMENT,
  'jobOffer.requestedSpecialityGeology': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_GEOLOGY,
  'jobOffer.requestedSpecialityQualityManagement': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_QUALITY_MANAGEMENT,
  'jobOffer.requestedSpecialityIndustrialManufacturer': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INDUSTRIAL_MANUFACTURER,
  'jobOffer.requestedSpecialityInformationTechnology': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INFORMATION_TECHNOLOGY,
  'jobOffer.requestedSpecialityInstrumentation': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_INSTRUMENTATION,
  'jobOffer.requestedSpecialitySoftware': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_SOFTWARE,
  'jobOffer.requestedSpecialityMaterialHandlingDistribution':
    JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MATERIAL_HANDLING_DISTRIBUTION,
  'jobOffer.requestedSpecialityMaritimeNaval': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MARITIME_NAVAL,
  'jobOffer.requestedSpecialityNonMetallicMaterials': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NON_METALLIC_MATERIALS,
  'jobOffer.requestedSpecialityMechanics': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MECHANICS,
  'jobOffer.requestedSpecialityMetallurgyMetals': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_METALLURGY_METALS,
  'jobOffer.requestedSpecialityMining': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_MINING,
  'jobOffer.requestedSpecialityNuclearPower': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_NUCLEAR_POWER,
  'jobOffer.requestedSpecialityPulpAndPaper': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PULP_AND_PAPER,
  'jobOffer.requestedSpecialityPhysical': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_PHYSICAL,
  'jobOffer.requestedSpecialityAutomatedProduction': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_AUTOMATED_PRODUCTION,
  'jobOffer.requestedSpecialityRobotics': JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_ROBOTICS,
  'jobOffer.requestedSpecialityOccupationalHealthAndSafety':
    JobOfferEnums.JobOfferRequestedSpeciality.REQUESTED_SPECIALITY_OCCUPATIONAL_HEALTH_AND_SAFETY,
};

export const JOB_OFFER_ADVANTAGE_HEALTH_I18N_MAPPING: I18nMapping<AdvantagesEnums.AdvantageHealth> = {
  'jobOffer.advantageHealthHealthDentalInsurance': AdvantagesEnums.AdvantageHealth.HEALTH_DENTAL_INSURANCE,
  'jobOffer.advantagesHealthHealthMedicalExpensesInsurance': AdvantagesEnums.AdvantageHealth.HEALTH_MEDICAL_EXPENSES_INSURANCE,
  'jobOffer.advantagesHealthHealthSightInsurance': AdvantagesEnums.AdvantageHealth.HEALTH_SIGHT_INSURANCE,
  'jobOffer.advantagesHealthHealthLifeInsurance': AdvantagesEnums.AdvantageHealth.HEALTH_LIFE_INSURANCE,
  'jobOffer.advantagesHealthHealthTravelInsurance': AdvantagesEnums.AdvantageHealth.HEALTH_TRAVEL_INSURANCE,
  'jobOffer.advantagesHealthHealthSalaryInsurance': AdvantagesEnums.AdvantageHealth.HEALTH_SALARY_INSURANCE,
  'jobOffer.advantagesHealthHealthShortTermDisabilityInsurance': AdvantagesEnums.AdvantageHealth.HEALTH_SHORT_TERM_DISABILITY_INSURANCE,
  'jobOffer.advantagesHealthHealthLongTermDisabilityInsurance': AdvantagesEnums.AdvantageHealth.HEALTH_LONG_TERM_DISABILITY_INSURANCE,
};

export const JOB_OFFER_ADVANTAGE_SOCIAL_I18N_MAPPING: I18nMapping<AdvantagesEnums.AdvantageSocial> = {
  'jobOffer.advantageSocialSocialSocialActivities': AdvantagesEnums.AdvantageSocial.SOCIAL_SOCIAL_ACTIVITIES,
  'jobOffer.advantageSocialSocialTravelAndHolidays': AdvantagesEnums.AdvantageSocial.SOCIAL_TRAVEL_AND_HOLIDAYS,
  'jobOffer.advantageSocialSocialCafeteria': AdvantagesEnums.AdvantageSocial.SOCIAL_CAFETERIA,
  'jobOffer.advantageSocialSocialFreshFruitAndVegetables': AdvantagesEnums.AdvantageSocial.SOCIAL_FRESH_FRUIT_AND_VEGETABLES,
  'jobOffer.advantageSocialSocialRelaxationRoom': AdvantagesEnums.AdvantageSocial.SOCIAL_RELAXATION_ROOM,
  'jobOffer.advantageSocialSocialWellnessProgram': AdvantagesEnums.AdvantageSocial.SOCIAL_WELLNESS_PROGRAM,
  'jobOffer.advantageSocialSocialReferencingProgram': AdvantagesEnums.AdvantageSocial.SOCIAL_REFERENCING_PROGRAM,
};

export const JOB_OFFER_ADVANTAGE_LIFE_BALANCE_I18N_MAPPING: I18nMapping<AdvantagesEnums.AdvantageLifeBalance> = {
  'jobOffer.advantageLifeBalanceLifeBalanceReconcilingFamilyAndWork': AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_RECONCILING_FAMILY_AND_WORK,
  'jobOffer.advantageLifeBalanceLifeBalancePaidHolidays': AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_PAID_HOLIDAYS,
  'jobOffer.advantageLifeBalanceLifeBalanceFlexibleSchedules': AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_FLEXIBLE_SCHEDULES,
  'jobOffer.advantageLifeBalanceLifeBalanceSickLeave': AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_SICK_LEAVE,
  'jobOffer.advantageLifeBalanceLifeBalanceSummerHours': AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_SUMMER_HOURS,
  'jobOffer.advantageLifeBalanceLifeBalanceMobileHolidays': AdvantagesEnums.AdvantageLifeBalance.LIFE_BALANCE_MOBILE_HOLIDAYS,
};

export const JOB_OFFER_ADVANTAGE_FINANCIAL_I18N_MAPPING: I18nMapping<AdvantagesEnums.AdvantageFinancial> = {
  'jobOffer.advantageFinancialFinancialEmployeeDiscount': AdvantagesEnums.AdvantageFinancial.FINANCIAL_EMPLOYEE_DISCOUNT,
  'jobOffer.advantageFinancialFinancialCompetitiveCompensation': AdvantagesEnums.AdvantageFinancial.FINANCIAL_COMPETITIVE_COMPENSATION,
  'jobOffer.advantageFinancialFinancialPensionPlan': AdvantagesEnums.AdvantageFinancial.FINANCIAL_PENSION_PLAN,
  'jobOffer.advantageFinancialFinancialDeferredProfitSharingPlanDpsp': AdvantagesEnums.AdvantageFinancial.FINANCIAL_DEFERRED_PROFIT_SHARING_PLAN_DPSP,
  'jobOffer.advantageFinancialFinancialEap': AdvantagesEnums.AdvantageFinancial.FINANCIAL_EAP,
  'jobOffer.advantageFinancialFinancialRecognitionOfSeniority': AdvantagesEnums.AdvantageFinancial.FINANCIAL_RECOGNITION_OF_SENIORITY,
  'jobOffer.advantageFinancialFinancialRefundOfDuesToAProfessionalAssociationOrOrder':
    AdvantagesEnums.AdvantageFinancial.FINANCIAL_REFUND_OF_DUES_TO_A_PROFESSIONAL_ASSOCIATION_OR_ORDER,
  'jobOffer.advantageFinancialFinancialPerformanceBonus': AdvantagesEnums.AdvantageFinancial.FINANCIAL_PERFORMANCE_BONUS,
  'jobOffer.advantageFinancialFinancialParkingLot': AdvantagesEnums.AdvantageFinancial.FINANCIAL_PARKING_LOT,
  'jobOffer.advantageFinancialFinancialTransportSubscription': AdvantagesEnums.AdvantageFinancial.FINANCIAL_TRANSPORT_SUBSCRIPTION,
};

export const JOB_OFFER_ADVANTAGE_PROFESSIONAL_I18N_MAPPING: I18nMapping<AdvantagesEnums.AdvantageProfessional> = {
  'jobOffer.advantageProfessionalProfessionalTrainingAndDevelopmentProgram':
    AdvantagesEnums.AdvantageProfessional.PROFESSIONAL_TRAINING_AND_DEVELOPMENT_PROGRAM,
  'jobOffer.advantageProfessionalProfessionalRecognitionProgram': AdvantagesEnums.AdvantageProfessional.PROFESSIONAL_RECOGNITION_PROGRAM,
};

export const JOB_OFFER_TRAVELING_FREQUENCY_I18N_MAPPING: I18nMapping<TravelingEnums.TravelingFrequency> = {
  'jobOffer.travelingTravelingFrequencyOccasional': TravelingEnums.TravelingFrequency.TRAVELING_OCCASIONAL,
  'jobOffer.travelingTravelingFrequencyFrequent': TravelingEnums.TravelingFrequency.TRAVELING_FREQUENT,
};

export const JOB_OFFER_WANTED_PERSONALITY_I18N_MAPPING: I18nMapping<JobOfferEnums.JobOfferWantedPersonality> = {
  'jobOffer.wantedPersonalityCommunicators': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_COMMUNICATORS,
  'jobOffer.wantedPersonalityAmbitious': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AMBITIOUS,
  'jobOffer.wantedPersonalityAutonomous': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_AUTONOMOUS,
  'jobOffer.wantedPersonalityInnovative': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_INNOVATIVE,
  'jobOffer.wantedPersonalityCurious': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_CURIOUS,
  'jobOffer.wantedPersonalityDecisionMakers': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_DECISION_MAKERS,
  'jobOffer.wantedPersonalityStrategists': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_STRATEGISTS,
  'jobOffer.wantedPersonalityFlexible': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_FLEXIBLE,
  'jobOffer.wantedPersonalityLeaders': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_LEADERS,
  'jobOffer.wantedPersonalityNegotiators': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_NEGOTIATORS,
  'jobOffer.wantedPersonalityOrganized': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_ORGANIZED,
  'jobOffer.wantedPersonalityRigorous': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_RIGOROUS,
  'jobOffer.wantedPersonalityResourceful': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_RESOURCEFUL,
  'jobOffer.wantedPersonalityTeamwork': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_TEAMWORK,
  'jobOffer.wantedPersonalitySpeakersAtTheConference': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_SPEAKERS_AT_THE_CONFERENCE,
  'jobOffer.wantedPersonalityPonderator': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_PONDERATOR,
  'jobOffer.wantedPersonalityDiplomats': JobOfferEnums.JobOfferWantedPersonality.WANTED_PERSONALITY_DIPLOMATS,
};

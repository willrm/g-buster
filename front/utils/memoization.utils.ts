import memoize from 'memoizee';
import ApiService from '~/services/api.service';
import { UserResponseInterface } from '../../back/src/infrastructure/rest/user/models/user-response.interface';

const getUserProfileCacheMaxAge: number = 5 * 60 * 1000;
const getUserProfile = (apiService: ApiService): Promise<UserResponseInterface> => apiService.getUserProfile();
export const memoizedGetUserProfile: (apiService: ApiService) => UserResponseInterface = memoize(getUserProfile, {
  promise: true,
  maxAge: getUserProfileCacheMaxAge,
});

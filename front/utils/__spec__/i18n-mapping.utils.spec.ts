import { getI18nKey, I18nKey, I18nMapping } from '~/utils/i18n-mapping.utils';

describe('utils/i18n-mapping.utils', () => {
  describe('getI18nKey()', () => {
    it('should return key corresponding to given value', () => {
      // given
      const i18nMapping: I18nMapping<string> = {
        key1: 'value1',
        key2: 'value2',
        key3: 'value3',
      };

      // when
      const result: I18nKey = getI18nKey(i18nMapping, 'value2');

      // then
      expect(result).toBe('key2');
    });

    it('should return undefined when no key corresponding to given value', () => {
      // given
      const i18nMapping: I18nMapping<string> = {
        key1: 'value1',
        key2: 'value2',
        key3: 'value3',
      };

      // when
      const result: I18nKey = getI18nKey(i18nMapping, 'value4');

      // then
      expect(result).toBeUndefined();
    });
  });
});

import { buildSlugUrl, extractIdFromUrl } from '~/utils/url-builder.utils';

describe('utils/url-builder.utils', () => {
  describe('buildSlugUrl()', () => {
    it('should return an url starting with id', () => {
      // given
      const expected: string = 'anId';

      // when
      const result: string = buildSlugUrl(expected, 'A slug text');

      // then
      expect(result.startsWith(expected)).toBe(true);
    });

    it('should have separator after url', () => {
      // given
      const expected: string = 'anId--';

      // when
      const result: string = buildSlugUrl('anId', 'A slug text');

      // then
      expect(result.startsWith(expected)).toBe(true);
    });

    it('should have slug text slugified', () => {
      // given
      const expected: string = 'a-slug-text';

      // when
      const result: string = buildSlugUrl('anId', 'A slug Text');

      // then
      expect(result.split('--')[1]).toBe(expected);
    });
  });

  describe('extractIdFromUrl()', () => {
    it('should return id part from url', () => {
      // given
      const expected: string = 'anId';

      // when
      const result: string = extractIdFromUrl('anId--a-slug-text');

      // then
      expect(result).toBe(expected);
    });
  });
});

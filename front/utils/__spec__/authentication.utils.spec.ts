import { getLoginUrl } from '~/utils/authentication.utils';

describe('utils/authentication.utils', () => {
  describe('getLoginUrl()', () => {
    it('should return an url to login endpoint with given redirect url as query string', () => {
      // given
      const redirectUrl: string = '/url/to/redirect/after/login';

      // when
      const result: string = getLoginUrl(redirectUrl);

      // then
      expect(result).toBe('/api/user/login?redirectUrl=/url/to/redirect/after/login');
    });
  });
});

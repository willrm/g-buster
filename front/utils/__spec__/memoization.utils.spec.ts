import memoize from 'memoizee';
import ApiService from '~/services/api.service';
import { memoizedGetUserProfile } from '~/utils/memoization.utils';

jest.mock('memoizee', () => {
  return jest.fn().mockImplementation((functionToMemoize: unknown) => {
    return functionToMemoize;
  });
});

describe('utils/memoization.utils', () => {
  describe('memoizedGetUserProfile()', () => {
    let apiService: ApiService;
    beforeEach(() => {
      apiService = {} as ApiService;
      apiService.getUserProfile = jest.fn();
    });

    it('should memoize user profile call', async () => {
      // when
      memoizedGetUserProfile(apiService);

      // then
      expect(apiService.getUserProfile).toHaveBeenCalled();
    });
    it('should configure memoization as a promise call with a 5 minutes max age', async () => {
      // then
      expect((memoize as jest.Mock).mock.calls[0][1]).toStrictEqual({
        maxAge: 300000,
        promise: true,
      });
    });
  });
});

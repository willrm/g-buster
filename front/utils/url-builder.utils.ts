import slug from 'slug';

export const URL_SLUG_SEPARATOR: string = '--';

export const buildSlugUrl: (id: string, textToSlug: string) => string = (id: string, textToSlug: string) => {
  return `${id}${URL_SLUG_SEPARATOR}${slug(textToSlug, { lower: true })}`;
};

export const extractIdFromUrl: (url: string) => string = (url: string) => {
  return url.split(URL_SLUG_SEPARATOR)[0];
};

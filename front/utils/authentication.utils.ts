export const getLoginUrl = (redirectUrl: string) => {
  return `/api/user/login?redirectUrl=${redirectUrl}`;
};

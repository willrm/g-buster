declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module 'vuejs-datepicker';
declare module 'v-tooltip';
declare module 'memoizee';

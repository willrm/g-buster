import { createLocalVue, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import Vuex, { ModuleTree, Store } from 'vuex';
import GbFooter from '~/components/layout/gb-footer.vue';
import GbHeader from '~/components/layout/gb-header.vue';
import { RootStateInterface } from '~/store';
import { FooterResponseInterface, HeaderResponseInterface } from '../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';
import Default from '../default.vue';

describe('layouts/default', () => {
  let localVue: typeof Vue;
  let defaultWrapper: Wrapper<Vue>;

  let store: Store<RootStateInterface>;
  let getHeaderMock: jest.Mock;
  let getFooterMock: jest.Mock;

  let $route: { path: string };

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
  });

  beforeEach(() => {
    getHeaderMock = jest.fn();
    getFooterMock = jest.fn();

    store = new Vuex.Store({
      modules: {
        'layout-module': {
          namespaced: true,
          getters: {
            getHeader: getHeaderMock,
            getFooter: getFooterMock,
          },
        },
      } as ModuleTree<RootStateInterface>,
    });

    $route = {
      path: '',
    };
  });

  describe('header', () => {
    it('should bind getHeader getter to GbHeader value prop', () => {
      // given
      const expected: HeaderResponseInterface = { title: 'A header' } as HeaderResponseInterface;
      getHeaderMock.mockReturnValue(expected);

      // when
      defaultWrapper = shallowMount(Default, { store, localVue, stubs: { nuxt: true }, mocks: { $route } });

      // then
      // @ts-ignore
      expect(defaultWrapper.find(GbHeader).vm.value).toStrictEqual(expected);
    });
  });

  describe('main', () => {
    it('should have no class', () => {
      // when
      defaultWrapper = shallowMount(Default, { store, localVue, stubs: { nuxt: true }, mocks: { $route } });

      // then
      expect(defaultWrapper.find('main').element.classList.length).toEqual(0);
    });

    describe('company creation workflow', () => {
      it('should have company-creation-background class on company/create route', async () => {
        // given
        defaultWrapper = shallowMount(Default, { store, localVue, stubs: { nuxt: true }, mocks: { $route } });

        // when
        $route.path = '/en-ca/company/profile/create';
        await defaultWrapper.vm.$nextTick();

        // then
        expect(defaultWrapper.find('main').element.classList).toContain('company-creation-background');
      });

      it('should have company-preview-background class on company/preview route', async () => {
        // given
        defaultWrapper = shallowMount(Default, { store, localVue, stubs: { nuxt: true }, mocks: { $route } });

        // when
        $route.path = '/en-ca/company/profile/preview';
        await defaultWrapper.vm.$nextTick();

        // then
        expect(defaultWrapper.find('main').element.classList).toContain('company-preview-background');
      });

      it('should have company-creation-confirmation-background class on company/preview route', async () => {
        // given
        defaultWrapper = shallowMount(Default, { store, localVue, stubs: { nuxt: true }, mocks: { $route } });

        // when
        $route.path = '/en-ca/company/profile/creation-confirmation';
        await defaultWrapper.vm.$nextTick();

        // then
        expect(defaultWrapper.find('main').element.classList).toContain('company-creation-confirmation-background');
      });
    });
    describe('job-offer creation workflow', () => {
      it('should have job-offer-creation-background class on job-offer/create route', async () => {
        // given
        defaultWrapper = shallowMount(Default, { store, localVue, stubs: { nuxt: true }, mocks: { $route } });

        // when
        $route.path = '/en-ca/career/job-offer/create';
        await defaultWrapper.vm.$nextTick();

        // then
        expect(defaultWrapper.find('main').element.classList).toContain('job-offer-creation-background');
      });

      it('should have job-offer-preview-background class on job-offer/preview route', async () => {
        // given
        defaultWrapper = shallowMount(Default, { store, localVue, stubs: { nuxt: true }, mocks: { $route } });

        // when
        $route.path = '/en-ca/career/job-offer/preview';
        await defaultWrapper.vm.$nextTick();

        // then
        expect(defaultWrapper.find('main').element.classList).toContain('job-offer-preview-background');
      });

      it('should have job-offer-creation-confirmation-background class on job-offer/preview route', async () => {
        // given
        defaultWrapper = shallowMount(Default, { store, localVue, stubs: { nuxt: true }, mocks: { $route } });

        // when
        $route.path = '/en-ca/career/job-offer/creation-confirmation';
        await defaultWrapper.vm.$nextTick();

        // then
        expect(defaultWrapper.find('main').element.classList).toContain('job-offer-creation-confirmation-background');
      });
    });
  });

  describe('footer', () => {
    it('should bind getFooter getter to GbFooter value prop', () => {
      // given
      const expected: FooterResponseInterface = { copyright: 'A footer copyright' } as FooterResponseInterface;
      getFooterMock.mockReturnValue(expected);

      // when
      defaultWrapper = shallowMount(Default, { store, localVue, stubs: { nuxt: true }, mocks: { $route } });

      // then
      // @ts-ignore
      expect(defaultWrapper.find(GbFooter).vm.value).toStrictEqual(expected);
    });
  });
});

import { createLocalVue, shallowMount, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import Vuex, { ModuleTree, Store } from 'vuex';
import GbFooter from '~/components/layout/gb-footer.vue';
import GbHeader from '~/components/layout/gb-header.vue';
import { RootStateInterface } from '~/store';
import { FooterResponseInterface, HeaderResponseInterface } from '../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';
import CompanyDashboard from '../company-dashboard.vue';

describe('layouts/company-dashboard', () => {
  let localVue: typeof Vue;
  let companyDashboardWrapper: Wrapper<Vue>;

  let store: Store<RootStateInterface>;
  let getHeaderMock: jest.Mock;
  let getFooterMock: jest.Mock;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
  });

  beforeEach(() => {
    getHeaderMock = jest.fn();
    getFooterMock = jest.fn();

    store = new Vuex.Store({
      modules: {
        'layout-module': {
          namespaced: true,
          getters: {
            getHeader: getHeaderMock,
            getFooter: getFooterMock,
          },
        },
      } as ModuleTree<RootStateInterface>,
    });
  });

  describe('middleware', () => {
    it('should use authenticatedAsCompany middleware', () => {
      // when
      companyDashboardWrapper = shallowMount(CompanyDashboard, { store, localVue, stubs: { nuxt: true } });

      // then
      expect(companyDashboardWrapper.vm.$options.middleware).toBe('authenticatedAsCompany.middleware');
    });
  });

  describe('header', () => {
    it('should bind getHeader getter to GbHeader value prop', () => {
      // given
      const expected: HeaderResponseInterface = { title: 'A header' } as HeaderResponseInterface;
      getHeaderMock.mockReturnValue(expected);

      // when
      companyDashboardWrapper = shallowMount(CompanyDashboard, { store, localVue, stubs: { nuxt: true } });

      // then
      // @ts-ignore
      expect(companyDashboardWrapper.find(GbHeader).vm.value).toStrictEqual(expected);
    });
  });
  describe('footer', () => {
    it('should bind getFooter getter to GbFooter value prop', () => {
      // given
      const expected: FooterResponseInterface = { copyright: 'A footer copyright' } as FooterResponseInterface;
      getFooterMock.mockReturnValue(expected);

      // when
      companyDashboardWrapper = shallowMount(CompanyDashboard, { store, localVue, stubs: { nuxt: true } });

      // then
      // @ts-ignore
      expect(companyDashboardWrapper.find(GbFooter).vm.value).toStrictEqual(expected);
    });
  });
});

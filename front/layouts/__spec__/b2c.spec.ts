import { createLocalVue, mount, shallowMount, Stubs, Wrapper } from '@vue/test-utils';
import Vue from 'vue';
import Vuex, { ModuleTree, Store } from 'vuex';
import GbActionBar from '~/components/base-components/gb-action-bar.vue';
import GbButton from '~/components/base-components/gb-button.vue';
import GbFooter from '~/components/layout/gb-footer.vue';
import GbHeader from '~/components/layout/gb-header.vue';
import B2c from '~/layouts/b2c.vue';
import { RootStateInterface } from '~/store';
import { FooterResponseInterface, HeaderResponseInterface } from '../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';

describe('layouts/b2c', () => {
  let localVue: typeof Vue;
  let b2cWrapper: Wrapper<Vue>;

  let store: Store<RootStateInterface>;
  let getHeaderMock: jest.Mock;
  let getFooterMock: jest.Mock;
  let isLoggedAsCandidateMock: jest.Mock;

  let $t: jest.Mock;
  let $router: object;
  let $route: object;
  let pushMock: jest.Mock;

  let stubs: Stubs;

  let header: HeaderResponseInterface;
  let footer: FooterResponseInterface;

  beforeAll(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);
  });

  beforeEach(() => {
    window.location.replace = jest.fn();
    getHeaderMock = jest.fn();
    getFooterMock = jest.fn();
    isLoggedAsCandidateMock = jest.fn();

    $t = jest.fn();

    pushMock = jest.fn();
    $router = {
      push: pushMock,
    };
    $route = {
      params: { lang: '' },
      fullPath: '',
    };

    stubs = { nuxt: true, 'nuxt-link': true };

    header = {
      title: 'A header',
      logo: {},
    } as HeaderResponseInterface;

    footer = {
      copyright: 'A footer copyright',
    } as FooterResponseInterface;

    store = new Vuex.Store({
      modules: {
        'layout-module': {
          namespaced: true,
          getters: {
            getHeader: getHeaderMock,
            getFooter: getFooterMock,
          },
        },
        'user-profile-module': {
          namespaced: true,
          getters: {
            isLoggedAsCandidate: isLoggedAsCandidateMock,
          },
        },
      } as ModuleTree<RootStateInterface>,
    });
  });

  describe('header', () => {
    it('should bind getHeader getter to GbHeader value prop', () => {
      // given
      getHeaderMock.mockReturnValue(header);

      // when
      b2cWrapper = shallowMount(B2c, { store, localVue, stubs, mocks: { $t, $router, $route } });

      // then
      // @ts-ignore
      expect(b2cWrapper.find(GbHeader).vm.value).toStrictEqual(header);
    });
  });
  describe('footer', () => {
    it('should bind getFooter getter to GbFooter value prop', () => {
      // given
      getFooterMock.mockReturnValue(footer);

      // when
      b2cWrapper = shallowMount(B2c, { store, localVue, stubs, mocks: { $t, $router, $route } });

      // then
      // @ts-ignore
      expect(b2cWrapper.find(GbFooter).vm.value).toStrictEqual(footer);
    });
  });

  describe('action-bar', () => {
    it('should display action bar when user is not logged in', () => {
      // given
      isLoggedAsCandidateMock.mockReturnValue(false);

      // when
      b2cWrapper = shallowMount(B2c, { store, localVue, stubs, mocks: { $t, $router, $route } });

      // then
      expect(b2cWrapper.findAll(GbActionBar)).toHaveLength(1);
    });
    it('should not display action bar when user is logged in', () => {
      // given
      isLoggedAsCandidateMock.mockReturnValue(true);

      // when
      b2cWrapper = shallowMount(B2c, { store, localVue, stubs, mocks: { $t, $router, $route } });

      // then
      expect(b2cWrapper.findAll(GbActionBar)).toHaveLength(0);
    });

    it('should contain action-bar component', () => {
      // when
      b2cWrapper = shallowMount(B2c, { store, localVue, stubs, mocks: { $t, $router, $route } });

      // then
      // @ts-ignore
      expect(b2cWrapper.findAll(GbActionBar)).toHaveLength(1);
    });

    describe('Redirect link in action-bar', () => {
      it('should contain link to go to company dashboard', () => {
        // given
        // @ts-ignore
        $route.params.lang = 'fr-ca';
        getHeaderMock.mockReturnValue(header);
        getFooterMock.mockReturnValue(footer);
        $t.mockReturnValue('Link to company view');

        // when
        b2cWrapper = mount(B2c, { store, localVue, stubs, mocks: { $t, $router, $route } });

        // then
        const nuxtLinkStubWrapper: Wrapper<Vue> = b2cWrapper.find('nuxt-link-stub');
        expect($t).toHaveBeenCalledWith('companyView');
        expect(nuxtLinkStubWrapper.attributes()).toHaveProperty('to', '/fr-ca/company/dashboard');
        expect(nuxtLinkStubWrapper.text()).toBe('Link to company view');
      });
    });

    describe('Log in button in action-bar', () => {
      it('should display gb-button component for log in action', () => {
        // given
        getHeaderMock.mockReturnValue(header);
        getFooterMock.mockReturnValue(footer);
        $t.mockReturnValue('Log in');

        const expected: object = {
          buttonLabelKey: 'login',
        };

        // when
        b2cWrapper = mount(B2c, { store, localVue, stubs, mocks: { $t, $router, $route } });
        const gbButton: Wrapper<Vue> = b2cWrapper.findAll(GbButton).at(0);

        // then
        // @ts-ignore
        expect(gbButton.vm.options).toMatchObject(expected);
      });

      it('should redirect on login endpoint with current route as redirect url when click on it', () => {
        // given
        // @ts-ignore
        $route.fullPath = '/route/full/path';

        getHeaderMock.mockReturnValue(header);
        getFooterMock.mockReturnValue(footer);
        $t.mockReturnValue('Log in');
        b2cWrapper = mount(B2c, { store, localVue, stubs, mocks: { $t, $router, $route } });

        // when
        b2cWrapper
          .findAll(GbButton)
          .at(0)
          .trigger('click');

        // then
        expect(window.location.replace).toHaveBeenCalledWith('/api/user/login?redirectUrl=/route/full/path');
      });

      describe('Sign up button in action-bar', () => {
        it('should display gb-button component for sign up action', () => {
          // given
          getHeaderMock.mockReturnValue(header);
          getFooterMock.mockReturnValue(footer);
          $t.mockReturnValue('Sign up');

          const expected: object = {
            buttonLabelKey: 'signup',
          };

          // when
          b2cWrapper = mount(B2c, { store, localVue, stubs, mocks: { $t, $router, $route } });
          const gbButton: Wrapper<Vue> = b2cWrapper.findAll(GbButton).at(1);

          // then
          // @ts-ignore
          expect(gbButton.vm.options).toMatchObject(expected);
        });

        it('should call router to redirect on sign up page when click on it', async () => {
          // given
          getHeaderMock.mockReturnValue(header);
          getFooterMock.mockReturnValue(footer);
          $t.mockReturnValue('Sign up');
          b2cWrapper = mount(B2c, { store, localVue, stubs, mocks: { $t, $router, $route } });

          // when
          b2cWrapper
            .findAll(GbButton)
            .at(1)
            .trigger('click');
          await b2cWrapper.vm.$nextTick();

          // then
          expect(pushMock).toHaveBeenCalledWith('/');
        });
      });
    });
  });
});

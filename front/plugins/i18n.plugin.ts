import { Context } from '@nuxt/types';
import { sanitizeLang } from '~/utils/lang.utils';
import { CommonTypeAliases } from '../../back/src/infrastructure/rest/common-models/common-type-aliases';
import { I18nMessagesResponseInterface } from '../../back/src/infrastructure/rest/i18n/models/i18n-messages-response.interface';

export default async (context: Context) => {
  const lang: CommonTypeAliases.Lang = sanitizeLang(context.route.params.lang);
  context.app.i18n.setLocale(lang);
  const i18nMessages: I18nMessagesResponseInterface = await context.app.$apiService.getI18nMessages(lang);
  context.app.i18n.setLocaleMessage(lang, i18nMessages);
};

import { Context } from '@nuxt/types';
import { Route } from 'vue-router';
import ApiService from '~/services/api.service';
import { I18nMessagesResponseInterface } from '../../../back/src/infrastructure/rest/i18n/models/i18n-messages-response.interface';

import i18nPlugin from '../i18n.plugin';

jest.mock('~/services/api.service');

describe('plugins/I18nPlugin', () => {
  let mockSetLocale: jest.Mock;
  let mockSetLocaleMessage: jest.Mock;
  let mockContext: Context;
  let route: Route;
  let mockApiService: ApiService;

  beforeEach(() => {
    mockContext = {} as Context;

    mockSetLocale = jest.fn();
    mockSetLocaleMessage = jest.fn();

    mockApiService = {} as ApiService;
    mockApiService.getI18nMessages = jest.fn();

    route = {} as Route;
    route.params = {} as { [key: string]: string };

    Object.assign(mockContext, {
      ...mockContext,
      app: {
        i18n: {
          setLocale: mockSetLocale,
          setLocaleMessage: mockSetLocaleMessage,
        },
        $apiService: mockApiService,
      },
      route,
    });
  });

  it('should set locale to the one in route params', () => {
    // given
    route.params.lang = 'fr-ca';

    // when
    i18nPlugin(mockContext);

    // then
    expect(mockSetLocale).toHaveBeenCalledWith('fr-ca');
  });

  it('should set locale to with default one if one in params is not whitelisted', () => {
    // given
    route.params.lang = 'a-lang';

    // when
    i18nPlugin(mockContext);

    // then
    expect(mockSetLocale).toHaveBeenCalledWith('fr-ca');
  });

  it('should call api i18n messages endpoint with a lang', () => {
    // given
    route.params.lang = 'fr-ca';

    // when
    i18nPlugin(mockContext);

    // then
    expect(mockApiService.getI18nMessages).toHaveBeenCalledWith('fr-ca');
  });

  it('should call setLocale with lang and messages from api', async () => {
    // given
    route.params.lang = 'fr-ca';
    const expected: I18nMessagesResponseInterface = { welcome: 'A welcome message' };
    (mockApiService.getI18nMessages as jest.Mock).mockReturnValue(Promise.resolve(expected));

    // when
    await i18nPlugin(mockContext);

    // then
    expect(mockSetLocaleMessage).toHaveBeenCalledWith('fr-ca', expected);
  });
});

import { NuxtAxiosInstance } from '@nuxtjs/axios';
import { AxiosError, AxiosResponse } from 'axios';
import ApiService from '~/services/api.service';
import { GetJobOffersRequestInterface } from '../../../back/src/infrastructure/rest/career/models/get-job-offers-request.interface';
import { JobOfferEnums } from '../../../back/src/infrastructure/rest/career/models/job-offer-constraints-and-enums';
import {
  JobOfferResponseInterface,
  JobOfferWithCompanyResponseInterface,
} from '../../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import { PostCompanyRequestInterface } from '../../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { PostCompanyResponseInterface } from '../../../back/src/infrastructure/rest/career/models/post-company-response.interface';
import { PostJobOfferRequestInterface } from '../../../back/src/infrastructure/rest/career/models/post-job-offer-request.interface';
import { PostJobOfferResponseInterface } from '../../../back/src/infrastructure/rest/career/models/post-job-offer-response.interface';
import { PutJobOfferRequestInterface } from '../../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import { CommonTypeAliases } from '../../../back/src/infrastructure/rest/common-models/common-type-aliases';
import { DummyPageResponseInterface } from '../../../back/src/infrastructure/rest/editorial/models/dummy-page-response.interface';
import {
  FooterResponseInterface,
  HeaderResponseInterface,
  LayoutResponseInterface,
} from '../../../back/src/infrastructure/rest/editorial/models/layout-response.interface';
import { I18nMessagesResponseInterface } from '../../../back/src/infrastructure/rest/i18n/models/i18n-messages-response.interface';
import { UserResponseInterface } from '../../../back/src/infrastructure/rest/user/models/user-response.interface';

describe('services/ApiService', () => {
  let apiService: ApiService;
  let $get: jest.Mock;
  let $post: jest.Mock;
  let $put: jest.Mock;
  let useMock: jest.Mock;

  beforeEach(() => {
    $get = jest.fn();
    $post = jest.fn();
    $put = jest.fn();
    useMock = jest.fn();
    const $axios: object = { $get, $post, $put, interceptors: { response: { use: useMock } } };
    const mockAxios: NuxtAxiosInstance = $axios as NuxtAxiosInstance;
    apiService = new ApiService(mockAxios);
  });

  describe('constructor()', () => {
    it('should call use methods of response interceptor with undefined onFulfilled callback', () => {
      // then
      expect(useMock).toHaveBeenCalled();
      expect(useMock.mock.calls[0][0]).toBeUndefined();
    });
    it('should call use methods of response interceptor with defined onRejected callback that return data from error', () => {
      // given
      const rejectCallback: (error: AxiosError) => Promise<unknown> = useMock.mock.calls[0][1];
      const error: AxiosError = {} as AxiosError;
      error.response = { data: 'An error data' } as AxiosResponse;

      // when
      const result: Promise<unknown> = rejectCallback(error);

      // then
      expect(result).rejects.toBe('An error data');
    });
  });

  describe('getDummyPageContent()', () => {
    it('should call api with lang and uid', async () => {
      // given
      const expected: DummyPageResponseInterface = {
        title: 'a-title',
        content: 'a-content',
        image: {
          alternativeText: 'an-awesome-image',
          source: 'http://an-awesome.png',
        },
        generatedDate: new Date(),
      };

      const lang: string = 'a-lang';
      const uid: string = 'an-uid';

      $get.mockReturnValue(Promise.resolve(expected));

      // when
      const result: DummyPageResponseInterface = await apiService.getDummyPageContent(lang, uid);

      // then
      expect(result).toBe(expected);
      expect($get).toHaveBeenCalledWith(`/api/${lang}/dummy-page/${uid}`);
    });
  });

  describe('getLayout()', () => {
    it('should should call api with lang', async () => {
      // given
      const expected: LayoutResponseInterface = {
        header: {} as HeaderResponseInterface,
        footer: {} as FooterResponseInterface,
      };

      const lang: string = 'a-lang';

      $get.mockReturnValue(Promise.resolve(expected));

      // when
      const result: LayoutResponseInterface = await apiService.getLayout(lang);

      // then
      expect(result).toBe(expected);
      expect($get).toHaveBeenCalledWith(`/api/${lang}/layout`);
    });
  });

  describe('getI18nMessages()', () => {
    it('should call api with lang', async () => {
      // given
      const expected: I18nMessagesResponseInterface = { hello: 'Bon matin!' };

      $get.mockReturnValue(Promise.resolve(expected));

      // when
      const result: I18nMessagesResponseInterface = await apiService.getI18nMessages('fr-ca');

      // then
      expect(result).toBe(expected);
      expect($get).toHaveBeenCalledWith('/api/i18n/messages/fr-ca');
    });
  });

  describe('getJobOffer()', () => {
    it('should get a job offer', async () => {
      // given
      const id: CommonTypeAliases.JobOfferId = 'aJobOfferId';
      const expected: JobOfferWithCompanyResponseInterface = { title: 'A job offer' } as JobOfferWithCompanyResponseInterface;

      $get.mockReturnValue(Promise.resolve(expected));

      // when
      const result: JobOfferWithCompanyResponseInterface = await apiService.getJobOffer(id);

      // then
      expect(result).toBe(expected);
      expect($get).toHaveBeenCalledWith('/api/career/job-offers/aJobOfferId');
    });
  });

  describe('getJobOffers()', () => {
    it('should call api with empty param when no filter given', async () => {
      // when
      await apiService.getJobOffers();

      // then
      expect($get).toHaveBeenCalledWith('/api/career/job-offers', { params: {} });
    });

    it('should call api with filter param when given', async () => {
      // given
      const filter: GetJobOffersRequestInterface = {
        jobTypes: [JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP],
      };

      // when
      await apiService.getJobOffers(filter);

      // then
      expect($get).toHaveBeenCalledWith('/api/career/job-offers', { params: { jobTypes: [JobOfferEnums.JobOfferJobType.JOB_TYPE_INTERNSHIP] } });
    });

    it('should return job offers', async () => {
      // given
      const expected: JobOfferWithCompanyResponseInterface[] = [
        { title: 'A job offer' },
        { title: 'An other job' },
      ] as JobOfferWithCompanyResponseInterface[];

      $get.mockReturnValue(Promise.resolve(expected));

      // when
      const result: JobOfferWithCompanyResponseInterface[] = await apiService.getJobOffers();

      // then
      expect(result).toBe(expected);
    });
  });

  describe('createNewCompany()', () => {
    it('should post company to api', async () => {
      // given
      const companyToCreate: PostCompanyRequestInterface = { name: 'New company' } as PostCompanyRequestInterface;

      // when
      await apiService.createNewCompany(companyToCreate);

      // then
      expect($post).toHaveBeenCalledWith('/api/career/companies', companyToCreate, { params: { dryRun: false } });
    });
    it('should make a dryRun call when dryRun is true', async () => {
      // given
      const companyToCreate: PostCompanyRequestInterface = { name: 'New company' } as PostCompanyRequestInterface;

      // when
      await apiService.createNewCompany(companyToCreate, true);

      // then
      expect($post).toHaveBeenCalledWith('/api/career/companies', companyToCreate, { params: { dryRun: true } });
    });
    it('should return company id', async () => {
      // given
      const companyToCreate: PostCompanyRequestInterface = { name: 'New company' } as PostCompanyRequestInterface;
      const expected: PostCompanyResponseInterface = { id: 'aCompanyId' };
      $post.mockReturnValue(expected);

      // when
      const result: PostCompanyResponseInterface = await apiService.createNewCompany(companyToCreate, false);

      // then
      expect(result).toStrictEqual(expected);
    });
  });

  describe('createNewJobOffer()', () => {
    it('should post job offer to api', async () => {
      // given
      const jobOfferToCreate: PostJobOfferRequestInterface = { title: 'New job offer' } as PostJobOfferRequestInterface;

      // when
      await apiService.createNewJobOffer(jobOfferToCreate);

      // then
      expect($post).toHaveBeenCalledWith('/api/career/job-offers', jobOfferToCreate, { params: { dryRun: false } });
    });
    it('should make a dryRun call when dryRun is true', async () => {
      // given
      const jobOfferToCreate: PostJobOfferRequestInterface = { title: 'New job offer' } as PostJobOfferRequestInterface;

      // when
      await apiService.createNewJobOffer(jobOfferToCreate, true);

      // then
      expect($post).toHaveBeenCalledWith('/api/career/job-offers', jobOfferToCreate, { params: { dryRun: true } });
    });
    it('should return job offer id', async () => {
      // given
      const jobOfferToCreate: PostJobOfferRequestInterface = { title: 'New job offer' } as PostJobOfferRequestInterface;
      const expected: PostJobOfferResponseInterface = { id: 'aJobOfferId' };
      $post.mockReturnValue(expected);

      // when
      const result: PostJobOfferResponseInterface = await apiService.createNewJobOffer(jobOfferToCreate, false);

      // then
      expect(result).toStrictEqual(expected);
    });
  });

  describe('updateDraftJobOffer()', () => {
    it('should put job offer to api', async () => {
      // given
      const id: CommonTypeAliases.JobOfferId = 'aJobOfferId';
      const jobOfferToUpdate: PutJobOfferRequestInterface = { title: 'Draft job offer' } as PutJobOfferRequestInterface;

      // when
      await apiService.updateDraftJobOffer(id, jobOfferToUpdate);

      // then
      expect($put).toHaveBeenCalledWith(`/api/career/job-offers/${id}`, jobOfferToUpdate);
    });
  });

  describe('getUserProfile()', () => {
    it('should get user profile', async () => {
      // given
      const expected: UserResponseInterface = { firstName: 'A user' } as UserResponseInterface;

      $get.mockReturnValue(Promise.resolve(expected));

      // when
      const result: UserResponseInterface = await apiService.getUserProfile();

      // then
      expect(result).toBe(expected);
      expect($get).toHaveBeenCalledWith('/api/user/profile');
    });
  });

  describe('getUserJobOffers()', () => {
    it('should get user job offers', async () => {
      // given
      const expected: JobOfferResponseInterface[] = [{ title: 'A job offer' } as JobOfferResponseInterface];

      $get.mockReturnValue(Promise.resolve(expected));

      // when
      const result: JobOfferResponseInterface[] = await apiService.getUserJobOffers();

      // then
      expect(result).toBe(expected);
      expect($get).toHaveBeenCalledWith('/api/user/job-offers');
    });
  });

  describe('getUserJobOffer()', () => {
    it('should get a job offer', async () => {
      // given
      const id: CommonTypeAliases.JobOfferId = 'aJobOfferId';
      const expected: JobOfferResponseInterface = { title: 'A job offer' } as JobOfferResponseInterface;

      $get.mockReturnValue(Promise.resolve(expected));

      // when
      const result: JobOfferResponseInterface = await apiService.getUserJobOffer(id);

      // then
      expect(result).toBe(expected);
      expect($get).toHaveBeenCalledWith('/api/user/job-offers/aJobOfferId');
    });
  });
});

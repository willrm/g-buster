import { NuxtAxiosInstance } from '@nuxtjs/axios';
import { AxiosError } from 'axios';
import { GetJobOffersRequestInterface } from '../../back/src/infrastructure/rest/career/models/get-job-offers-request.interface';
import {
  JobOfferResponseInterface,
  JobOfferWithCompanyResponseInterface,
} from '../../back/src/infrastructure/rest/career/models/job-offer-response.interface';
import { PostCompanyRequestInterface } from '../../back/src/infrastructure/rest/career/models/post-company-request.interface';
import { PostCompanyResponseInterface } from '../../back/src/infrastructure/rest/career/models/post-company-response.interface';
import { PostJobOfferRequestInterface } from '../../back/src/infrastructure/rest/career/models/post-job-offer-request.interface';
import { PostJobOfferResponseInterface } from '../../back/src/infrastructure/rest/career/models/post-job-offer-response.interface';
import { PutJobOfferRequestInterface } from '../../back/src/infrastructure/rest/career/models/put-job-offer-request.interface';
import { CommonTypeAliases } from '../../back/src/infrastructure/rest/common-models/common-type-aliases';
import { DummyPageResponseInterface } from '../../back/src/infrastructure/rest/editorial/models/dummy-page-response.interface';
import { LayoutResponseInterface } from '../../back/src/infrastructure/rest/editorial/models/layout-response.interface';
import { I18nMessagesResponseInterface } from '../../back/src/infrastructure/rest/i18n/models/i18n-messages-response.interface';
import { UserResponseInterface } from '../../back/src/infrastructure/rest/user/models/user-response.interface';

export default class ApiService {
  private readonly $axios: NuxtAxiosInstance;

  constructor($axios: NuxtAxiosInstance) {
    this.attachResponseInterceptors($axios);
    this.$axios = $axios;
  }

  getDummyPageContent(lang: CommonTypeAliases.Lang, uid: CommonTypeAliases.Uid): Promise<DummyPageResponseInterface> {
    return this.$axios.$get(`/api/${lang}/dummy-page/${uid}`);
  }

  getLayout(lang: CommonTypeAliases.Lang): Promise<LayoutResponseInterface> {
    return this.$axios.$get(`/api/${lang}/layout`);
  }

  getI18nMessages(lang: CommonTypeAliases.Lang): Promise<I18nMessagesResponseInterface> {
    return this.$axios.$get(`/api/i18n/messages/${lang}`);
  }

  getJobOffer(id: CommonTypeAliases.JobOfferId): Promise<JobOfferWithCompanyResponseInterface> {
    return this.$axios.$get(`/api/career/job-offers/${id}`);
  }

  getJobOffers(filter: GetJobOffersRequestInterface = {}): Promise<JobOfferWithCompanyResponseInterface[]> {
    return this.$axios.$get('/api/career/job-offers', { params: { ...filter } });
  }

  createNewCompany(company: PostCompanyRequestInterface, dryRun: boolean = false): Promise<PostCompanyResponseInterface> {
    return this.$axios.$post('/api/career/companies', company, { params: { dryRun } });
  }

  createNewJobOffer(jobOffer: PostJobOfferRequestInterface, dryRun: boolean = false): Promise<PostJobOfferResponseInterface> {
    return this.$axios.$post('/api/career/job-offers', jobOffer, { params: { dryRun } });
  }

  updateDraftJobOffer(id: CommonTypeAliases.JobOfferId, jobOffer: PutJobOfferRequestInterface): Promise<void> {
    return this.$axios.$put(`/api/career/job-offers/${id}`, jobOffer);
  }

  getUserProfile(): Promise<UserResponseInterface> {
    return this.$axios.$get(`/api/user/profile`);
  }

  getUserJobOffers(): Promise<JobOfferResponseInterface[]> {
    return this.$axios.$get(`/api/user/job-offers`);
  }

  getUserJobOffer(id: CommonTypeAliases.JobOfferId): Promise<JobOfferResponseInterface> {
    return this.$axios.$get(`/api/user/job-offers/${id}`);
  }

  // PRIVATE METHODS

  private attachResponseInterceptors($axios: NuxtAxiosInstance): void {
    $axios.interceptors.response.use(undefined, (error: AxiosError) => {
      return Promise.reject(error && error.response && error.response.data);
    });
  }
}

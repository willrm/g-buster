import { Context, Middleware } from '@nuxt/types';
import { getLoginUrl } from '~/utils/authentication.utils';
import { memoizedGetUserProfile } from '~/utils/memoization.utils';
import { UserEnums } from '../../back/src/infrastructure/rest/user/models/user-constraints-and-enums';
import { UserResponseInterface } from '../../back/src/infrastructure/rest/user/models/user-response.interface';

const authenticatedAsCompanyMiddleware: Middleware = async (ctx: Context): Promise<void> => {
  try {
    const user: UserResponseInterface = await memoizedGetUserProfile(ctx.app.$apiService);
    if (user.role !== UserEnums.UserRole.COMPANY) {
      return ctx.error({
        statusCode: 403,
        message: 'Forbidden',
      });
    }
  } catch (e) {
    if (process.server) {
      return ctx.redirect(`/api/user/login?redirectUrl=${ctx.route.fullPath}`);
    } else {
      window.location.replace(getLoginUrl(ctx.route.fullPath));
    }
  }
};

export default authenticatedAsCompanyMiddleware;

import { Context } from '@nuxt/types';
import ApiService from '~/services/api.service';
import { memoizedGetUserProfile } from '~/utils/memoization.utils';
import { UserEnums } from '../../../back/src/infrastructure/rest/user/models/user-constraints-and-enums';
import { UserResponseInterface } from '../../../back/src/infrastructure/rest/user/models/user-response.interface';

import authenticatedAsCompanyMiddleware from '../authenticatedAsCompany.middleware';

jest.mock('~/utils/memoization.utils');

describe('middleware/authenticatedAsCompanyMiddleware', () => {
  let ctx: Context;

  beforeEach(() => {
    (memoizedGetUserProfile as jest.Mock).mockClear();

    window.location.replace = jest.fn();

    ctx = ({
      route: {
        fullPath: '',
      },
      redirect: jest.fn(),
      error: jest.fn(),
      app: { $apiService: {} as ApiService },
    } as unknown) as Context;
  });

  it('should fetch memoized user profile', async () => {
    // when
    // @ts-ignore
    await authenticatedAsCompanyMiddleware(ctx);

    // then
    expect(memoizedGetUserProfile).toHaveBeenCalledWith(ctx.app.$apiService);
  });

  it('should redirect to error page with forbidden status code when user is authenticated but has a different role than Company', async () => {
    const user: UserResponseInterface = { role: UserEnums.UserRole.CANDIDATE } as UserResponseInterface;
    (memoizedGetUserProfile as jest.Mock).mockReturnValue(Promise.resolve(user));

    // when
    // @ts-ignore
    await authenticatedAsCompanyMiddleware(ctx);

    // then
    expect(ctx.error).toHaveBeenCalledWith({
      statusCode: 403,
      message: 'Forbidden',
    });
  });

  it('should redirect to login page with current path as redirectUrl when user is not authenticated and in server mode', async () => {
    (memoizedGetUserProfile as jest.Mock).mockReturnValue(Promise.reject(new Error('403 Forbidden')));
    ctx.route.fullPath = '/route/full/path';
    process.server = true;

    // when
    // @ts-ignore
    await authenticatedAsCompanyMiddleware(ctx);

    // then
    expect(ctx.redirect).toHaveBeenCalledWith('/api/user/login?redirectUrl=/route/full/path');
  });

  it('should redirect to login page with current path as redirectUrl when user is not authenticated and in client mode', async () => {
    (memoizedGetUserProfile as jest.Mock).mockReturnValue(Promise.reject(new Error('403 Forbidden')));
    ctx.route.fullPath = '/route/full/path';
    process.server = false;

    // when
    // @ts-ignore
    await authenticatedAsCompanyMiddleware(ctx);

    // then
    expect(window.location.replace).toHaveBeenCalledWith('/api/user/login?redirectUrl=/route/full/path');
  });

  it('should not redirect anywhere when user is authenticated with Company role', async () => {
    const user: UserResponseInterface = { role: UserEnums.UserRole.COMPANY } as UserResponseInterface;
    (memoizedGetUserProfile as jest.Mock).mockReturnValue(Promise.resolve(user));

    // when
    // @ts-ignore
    await authenticatedAsCompanyMiddleware(ctx);

    // then
    expect(window.location.replace).not.toHaveBeenCalled();
    expect(ctx.redirect).not.toHaveBeenCalled();
    expect(ctx.error).not.toHaveBeenCalled();
  });
});

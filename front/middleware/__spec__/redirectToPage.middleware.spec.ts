import { Context } from '@nuxt/types';

import redirectToPageMiddleware from '../redirectToPage.middleware';

describe('middleware/redirectToPageMiddleware', () => {
  let ctx: Context;

  beforeEach(() => {
    ctx = ({
      params: {},
      route: {
        path: '',
      },
      redirect: jest.fn(),
      req: {},
    } as unknown) as Context;
  });

  describe('lang', () => {
    it('should redirect to French homepage when client first accept-language is fr', () => {
      // given
      ctx.req.headers = {
        'accept-language': 'fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7',
      };

      // when
      // @ts-ignore
      redirectToPageMiddleware(ctx);

      // then
      expect(ctx.redirect).toHaveBeenCalledWith('/fr-ca/');
    });

    it('should redirect to English homepage when client first accept-language is en', () => {
      // given
      ctx.req.headers = {
        'accept-language': 'en;q=0.9,en-US;q=0.8,en;q=0.7',
      };

      // when
      // @ts-ignore
      redirectToPageMiddleware(ctx);

      // then
      expect(ctx.redirect).toHaveBeenCalledWith('/en-ca/');
    });

    it('should redirect to French homepage when client first accept-language is unknown', () => {
      // given
      ctx.req.headers = {
        'accept-language': 'OTHER-LANGUAGE,en;q=0.9,en-US;q=0.8,en;q=0.7',
      };

      // when
      // @ts-ignore
      redirectToPageMiddleware(ctx);

      // then
      expect(ctx.redirect).toHaveBeenCalledWith('/fr-ca/');
    });

    it('should redirect to French homepage when client first accept-language does not exists', () => {
      // given
      ctx.req.headers = {};

      // when
      // @ts-ignore
      redirectToPageMiddleware(ctx);

      // then
      expect(ctx.redirect).toHaveBeenCalledWith('/fr-ca/');
    });
  });

  describe('company dashboard', () => {
    it('should redirect to company dashboard job offers on dashboard url', () => {
      // given
      ctx.params.lang = 'fr-ca';
      ctx.route.path = '/fr-ca/company/dashboard';

      // when
      // @ts-ignore
      redirectToPageMiddleware(ctx);

      // then
      expect(ctx.redirect).toHaveBeenCalledWith('/fr-ca/company/dashboard/job-offers');
    });
  });
});

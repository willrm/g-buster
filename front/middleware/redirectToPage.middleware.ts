import { Context, Middleware } from '@nuxt/types';
import { Locale } from '~/enums/locale.enum';

const redirectToPageMiddleware: Middleware = (ctx: Context): void => {
  if (ctx.route.path.includes('company/dashboard')) {
    return ctx.redirect(`/${ctx.params.lang}/company/dashboard/job-offers`);
  }

  const acceptLanguageHeader: string = ctx.req.headers['accept-language'] as string;
  if (acceptLanguageHeader && acceptLanguageHeader.startsWith('en')) {
    return ctx.redirect(`/${Locale.CANADIAN_ENGLISH}/`);
  }

  return ctx.redirect(`/${Locale.CANADIAN_FRENCH}/`);
};

export default redirectToPageMiddleware;

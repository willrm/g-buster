# INSTALL

## Environment variables

### front

  * __`APP_FOLDER`__
    * Description: used only when deploying on Clever Cloud to specify which folder to scan when building and deploying application
    * Example: `front`

  * __`API_URL`__
    * Description: base url for API calls to back application 
    * Example: `https://g-buster-back-dev.cleverapps.io`

  * __`HOST`__
    * Description: the host address that will be accessible to connections outside of the host machine 
    * Example: `0.0.0.0`

  * __`PORT`__
    * Description: the port on which the application will listen
    * Example: `8080`

#### How to use them in front
Environment variables are accessible in `front` application using [Nuxt env property](https://nuxtjs.org/api/configuration-env/).

### back

  * __`APP_FOLDER`__
    * Description: used only when deploying on Clever Cloud to specify which folder to scan when building and deploying application
    * Example: `back`

  * __`AUTH0_CALLBACK_URL`__
    * Description: the url where to redirect after a successful Auth0 login
    * Example: `https://g-buster-front-dev.cleverapps.io/api/user/login/callback`

  * __`AUTH0_CLIENT_ID`__
    * Description: Auth0 application client id
    * Example: `a-super-secret-client-id`

  * __`AUTH0_CLIENT_SECRET`__
    * Description: Auth0 application client secret
    * Example: `a-super-secret-client-secret`

  * __`AUTH0_DOMAIN`__
    * Description: Auth0 application domain
    * Example: `webisoft.auth0.com`

  * __`DATABASE_TYPE`__
    * Description: database type, can be `postgres` or `sqlite`
    * Example: `postgres`

  * __`DATABASE_NAME`__
    * Description: database name
    * Example: `my-db-name` (or `my-local-db.sqlite` when `DATABASE_TYPE` is `sqlite`)

  * __`DATABASE_HOST`__
    * Description: database host, **not needed when `DATABASE_TYPE` is `sqlite`**
    * Example: `postgres`

  * __`DATABASE_PORT`__
    * Description: database port, **not needed when `DATABASE_TYPE` is `sqlite`**
    * Example: `5432`

  * __`DATABASE_USERNAME`__
    * Description: database username, **not needed when `DATABASE_TYPE` is `sqlite`**
    * Example: `db-username`

  * __`DATABASE_PASSWORD`__
    * Description: database password, **not needed when `DATABASE_TYPE` is `sqlite`**
    * Example: `a-super-secret-password`

  * __`GBUSTER_ADMIN_EMAIL_ADDRESSES`__
    * Description: the email addresses, comma-separated, of the accounts that should have admin role
    * Example: `test@example.org,test2@example.org`

  * __`GBUSTER_SESSION_SECRET`__
    * Description: the secret used to encrypt session
    * Example: `a-session-secret-different-for-every-environments`

  * __`PORT`__
    * Description: the port on which the application will listen
    * Example: `8080`

  * __`PRISMIC_API_ACCESS_TOKEN`__
    * Description: the secret access token used for Prismic API calls
    * Example: `a-secret-prismic-access-token`

  * __`PRISMIC_API_URL`__
    * Description: the API base url corresponding to the Prismic repository where the content is 
    * Example: `https://g-buster.cdn.prismic.io/api/v2`

  * __`ZOHO_API_ACCESS_TOKEN`__
    * Description: the secret access token used for Zoho API calls 
    * Example: `a-secret-zoho-access-token`

  * __`ZOHO_API_URL`__
    * Description: the Zoho Recruit API base url 
    * Example: `https://recruit.zoho.com/recruit/private/xml`

  * __`ZOHO_API_CACHE_TTL_IN_SECONDS`__
    * Description: Zoho API calls cache time to live in seconds 
    * Example: `60`

  * __`GENIUM360_API_URL`__
    * Description: The Genium360 API base url 
    * Example: `https://www.genium360.ca/api/v1`

  * __`GENIUM360_API_ACCESS_TOKEN`__
    * Description: the secret access token used for Genium360 API calls 
    * Example: `a-secret-genium360-access-token`

#### How to use them in back
Environment variables are accessible in `back` application using the `EnvironmentConfigService`, and are validated on startup using [Joi](https://github.com/hapijs/joi).

# 2. Back and Front application versioning

Date: 2019-09-26

## Status

Accepted

## Context

We want to define a way to handle properly versioning for both `back` and `front` applications in order to be consistent.

## Decision

3 digits versioning (x.y.z).
- First digit: major release. Example: the MVP will be a version 0, and once the MVP is done and the project continues, then we will bump to version 1.
- Second digit: sprint release. Example: the MVP sprint 5 will be a version 0.5.
- Third digit: patch, in case of hotfixes. Example: second hotfix for MVP sprint 5 will be a version 0.5.2.

## Consequences

To keep track of changes and releases, we will have to create a git tag for each new release when a sprint is done (or a hotfix is implemented).

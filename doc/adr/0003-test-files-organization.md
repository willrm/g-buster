# 3. Test files organization

Date: 2019-09-26

## Status

Accepted

## Context

We want to define a convention to organize the test files, because both Nuxt and Nest frameworks are flexible about that.

## Decision

Keep the test files close to the tested classes/pages/components, in a subdirectory, to ease maintainability.

## Consequences

Test files will be in a `__spec__` subdirectory for each production code directory.
This `__spec__` subdirectory structure will be the same for both `back` and `front` applications.

Because end-to-end tests in `back` application are not related to a specific class and are run separately from unit tests, there are keep in a specific `e2e` directory.

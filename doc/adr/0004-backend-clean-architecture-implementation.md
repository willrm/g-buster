# 4. Backend Clean Architecture Implementation

Date: 2019-09-26

## Status

Accepted

## Context

We want to properly distinct business domain from technical implementation in order to ease maintainability. 

## Decision

We use a Clean Architecture pattern (domain/use cases/infrastructure) 

## Consequences

Create three main directories:
- `domain` for business logic and rules.
- `use_cases` for domain orchestration in order to fit our business features.
- `infrastructure` for technical implementation and configuration. Every related to framework will only be in this directory.

To ease Dependency Injection of use cases in framework controllers, we made a proxy layer (`use_cases_proxy`) that benefits the Dependency Injection mechanism of NestJs. 

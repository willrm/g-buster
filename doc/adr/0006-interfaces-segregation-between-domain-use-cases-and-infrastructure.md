# 6. Interfaces segregation between domain, use_cases and infrastructure

Date: 2019-10-29

## Status

Accepted

## Context

It was hard which interface to use depending on the layer of the application (domain, use_cases, infrastructure).
We had too many interfaces with mixed responsibilities.
And obviously we don't want domain to depend on infrastructure.

## Decision

Define interfaces for input and output for each layer.

## Consequences

- In domain:
  - A domain entity is created using:
    - a `<Domain>RawInterface`: this interface corresponds to raw data needed to construct the domain entity
    - many `<Domain>Interface`: when a domain entity is an aggregate
  - A domain entity implements an interface `<Domain>Interface`: this interface corresponds to a valid and consistent domain entity structure in the internal system, which means it has passed all the validation steps and therefore cannot be inconsistent or invalid
  - A domain repository use only `<Domain>Interface` as inputs and outputs
  - Both `<Domain>RawInterface` and `<Domain>Interface` are defined in domain layer

- In use_cases:
  - A use case is still the orchestration layer between domain entities and domain repositories

- In infrastructure:
  - A controller endpoint use a `<HttpMethod><Domain>RequestInterface` as input  
  - A controller endpoint use a `<Domain>ResponseInterface` as output
  - To avoid create new interfaces: 
    - `<HttpMethod><Domain>RequestInterface` is an alias of `<Domain>RawInterface`
    - `<Domain>ResponseInterface` is an alias of `<Domain>Interface`
  - Therefore, the controller endpoint is responsible to:
     - cast `<HttpMethod><Domain>RequestInterface` to `<Domain>RawInterface`
     - cast `<Domain>Interface` to `<Domain>ResponseInterface`
  - Both `<HttpMethod><Domain>RequestInterface` and `<Domain>ResponseInterface` are defined in infrastructure layer
  - Constraints and Enums defined in domain are exposed through barrels
  - Swagger classes implements `<HttpMethod><Domain>RequestInterface` and `<Domain>ResponseInterface` interfaces
 
- In front application:
  - The interfaces, constraints and enums used in code are those defined in infrastructure layer
  - Therefore, front should not directly depend on domain layer

- Across all layers:
  - No more `Shared` interfaces

# 5. Usage of shared interfaces

Date: 2019-10-15

## Status

Accepted

## Context

- In order to share interfaces between front and back, we introduced a notion of "shared" interfaces to avoid strong coupling between front codebase and implementation details in back
- Every domain entity implements a shared interface (example: `Company` implements a `CompanySharedInterface`)
- It's unclear where to use and not use shared interfaces in back

## Decision

- Shared interfaces should only be used for controller outputs: a controller method should always return a `Promise` of a shared interface
- For every other layers (domain, use cases, repositories, etc.), use implementation and not a shared interface

## Consequences

- Be aware to respect the same rules in test files
- A shared interface should have __only primitive or shared interface__ properties 
- A domain entity should have __only primitive or other domain entity__ properties 
